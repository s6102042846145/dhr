﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API;
using DHR.HR.API.Shared;
using ESS.EMPLOYEE.CONFIG.PA;
using DHR.HR.API.Model;
using ESS.DATA.EXCEPTION;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;

namespace ESS.EMPLOYEE.DHR
{
    public class EmployeeDataServiceImpl : AbstractEmployeeDataService
    {
        #region Constructor
        public EmployeeDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }
        #endregion Constructor

        #region Member
        private static string ModuleID = "ESS.EMPLOYEE.DHR";

        private static string ModuleID_DB = "ESS.EMPLOYEE.DB";

        private string CompanyCode { get; set; }
        CultureInfo oCL = new CultureInfo("en-US");

        private string BaseConnStr_DB
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID_DB, "BASECONNSTR");
            }
        }

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        #region GET
        private string DefaultDateGet
        {
            get
            {
                //ddMMyyyy:00:00:00
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATGET");
            }
        }
        private string DefaultEndDateGet
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        #endregion GET

        #region POST
        private string DefaultDatePost
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATPOST");
            }
        }
        private string DefaultEndDatePost
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATPOST");
            }
        }
        #endregion POST
        #endregion Member

        public override DateSpecificData GetDateSpecific(string EmployeeID)
        {
            DateSpecificData dateSpecificData = new DateSpecificData();
            ResponseModel<PAImportanceDateInf> responseModel = new ResponseModel<PAImportanceDateInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAImportanceDateInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                if(responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    dateSpecificData.EmployeeID = responseModel.Data[0].empCode;
                    //dateSpecificData.HiringDate = DateTime.ParseExact(responseModel.Data[0].employedDate, DefaultReturnDateFormat, oCL);
                    dateSpecificData.HiringDate = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[0].employedDate);
                    //dateSpecificData.StartWorkingDate = DateTime.ParseExact(responseModel.Data[0].startWorkDate, DefaultReturnDateFormat, oCL);
                    dateSpecificData.StartWorkingDate = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[0].startWorkDate);
                    //dateSpecificData.RetirementDate = DateTime.ParseExact(responseModel.Data[0].retireDate, DefaultReturnDateFormat, oCL);
                    dateSpecificData.RetirementDate = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[0].retireDate);
                    //dateSpecificData.StartPF = DateTime.ParseExact(responseModel.Data[0].registFundDate, DefaultReturnDateFormat, oCL);
                    dateSpecificData.StartPF = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[0].registFundDate);
                    if (responseModel.Data[0].leavePfFundDate1 == null)
                        responseModel.Data[0].leavePfFundDate1 = DefaultEndDateGet;
                    //dateSpecificData.EndPF = DateTime.ParseExact(responseModel.Data[0].leavePfFundDate1, DefaultReturnDateFormat, oCL);
                    dateSpecificData.EndPF = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[0].leavePfFundDate1);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("EMPLOYEE", "GetDateSpecific Error", responseModel.message + " " + ex.Message);
            }

            return dateSpecificData;
        }

        #region JOB

        public override string JobALL_DHR_TO_INFOTYPE0001(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0001 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE0001", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE0001
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }
               
            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE0002(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0002 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE0002", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE0002
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE0007(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0007 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE0007", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE0007
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE0027(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0027 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_INFOTYPE0027", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE0030(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0030 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_INFOTYPE0030", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE0105(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0105 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE0105", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE0105
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);
                        
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE0182(string EmployeeID1, string EmployeeID2)
        {
            string result = "Job Copy INFOTYPE0182 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE0182", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE0182
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_employeeid1", EmployeeID1);
                        oCommand.Parameters.AddWithValue("@p_employeeid2", EmployeeID2);
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobAll_DHR_TO_DateSpecificData()
        {
            string result = "Job Copy DateSpecificData Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_DateSpecificData", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_DateSpecificData
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }
        public override string JobALL_DHR_TO_INFOTYPE0105_Mapping(out string oMessage)
        {
            oMessage = "Job Copy INFOTYPE0105_Mapping Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobMTData_TO_INFOTYPE0105_Mapping", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.Add("@pMessage", SqlDbType.VarChar, 1000);
                        oCommand.Parameters["@pMessage"].Direction = ParameterDirection.Output;

                        oCommand.ExecuteNonQuery();
                        oMessage = oCommand.Parameters["@pMessage"].Value.ToString();

                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        oMessage = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return oMessage;
        }
        #endregion JOB

    }
}
