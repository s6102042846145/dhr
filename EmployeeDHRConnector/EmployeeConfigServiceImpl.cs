﻿using DHR.HR.API;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.DHR
{
    class EmployeeConfigServiceImpl : AbstractEmployeeConfigService
    {
        #region Constructor
        public EmployeeConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
       
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.EMPLOYEE.DHR";

        private CultureInfo DefaultCultureInfo
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new CultureInfo("en-US");
                }
                return WorkflowPrinciple.Current.UserSetting.CultureInfo;

            }
        }

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        #region GET
        private string DefaultDateGet
        {
            get
            {
                //ddMMyyyy:00:00:00
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATGET");
            }
        }
        private string DefaultEndDateGet
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        #endregion GET

        #endregion Member

        #region ApiKey Request
        //public override string GetApiKeyRequest()
        //{
        //    string ApiKkey = string.Empty;
        //    try
        //    {
        //        ApiKkey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
        //    }
        //    catch (Exception ex)
        //    {

        //        return "";
        //    }

        //    return ApiKkey;
        //}
        #endregion ApiKey Request

    }
}
