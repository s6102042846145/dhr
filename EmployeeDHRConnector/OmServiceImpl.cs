﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.DHR
{
    public class OmServiceImpl : AbstractOMService
    {
        #region Constructor
        public OmServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //oSqlManage["BaseConnStr"] = this.BaseConnStr;
        }
        #endregion Constructor

        #region Property
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }
        private static string ModuleID = "ESS.EMPLOYEE.DHR";

        private static string ModuleID_DB = "ESS.EMPLOYEE.DB";

        //private string BaseConnStr
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
        //    }
        //}

        private string BaseConnStr_DB
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID_DB, "BASECONNSTR");
            }
        }

        #endregion Member

        #region JOB

        public override string JobALL_DHR_TO_INFOTYPE1000()
        {
            string result = "Job Copy INFOTYPE1000 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE1000", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE1000
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE1001()
        {
            string result = "Job Copy INFOTYPE1001 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_TO_INFOTYPE1001", oConnection, oTransaction);//sp_JobEmployee_DHR_TO_INFOTYPE1001
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_companycode", CompanyCode);

                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE1010()
        {
            string result = "Job Copy INFOTYPE1010 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_INFOTYPE1010", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_INFOTYPE1013()
        {
            string result = "Job Copy INFOTYPE1013 Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_INFOTYPE1013", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }
        #endregion JOB

    }
}
