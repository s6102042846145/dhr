﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Shared;
using DHR.HR.API.Model;
using ESS.DATA.EXCEPTION;
using ESS.HR.OM.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.HR.OM.INTERFACE;
using ESS.SHAREDATASERVICE;
using DHR.HR.API;

namespace ESS.HR.OM.DHR
{
    public class HROMConfigServiceImpl : AbstractHROMConfigService
    {
        #region Constructor
        public HROMConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }
        #endregion

        #region Member
        private static string ModuleID = "ESS.HR.OM.DHR";
        private static string ModuleID_DB = "ESS.HR.OM.DB";
        private string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private string BaseConnStr_DB
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID_DB, "BASECONNSTR");
            }
        }

        #endregion

        public override List<OrganizationCenter> getOMUnitByPeriodDate(DateTime oBegdate, DateTime oEndDate)
        {
            List<OrganizationCenter> oReturn = new List<OrganizationCenter>();
            ResponseModel<Unit> responseModel = new ResponseModel<Unit>();
            try
            {
                //responseModel = APIOrgManagement.CreateInstance(CompanyCode).GetDHRUnit(oBegdate.ToString("ddMMyyyy"), oEndDate.ToString("ddMMyyyy"));
                //if (responseModel.Data != null && responseModel.Data.Count > 0)
                //{
                //    foreach (var item in responseModel.Data)
                //    {
                //        OrganizationMasterData temp = new OrganizationMasterData();

                //        temp.BeginDate = DateTime.ParseExact(item.startDate, "ddMmyyyy", null);
                //        temp.EndDate = DateTime.ParseExact(item.endDate, "ddMmyyyy", null);
                //        temp.CostCenterID = item.costCenterValue;
                //        temp.ShortNameEN = item.unitShortnameEn;
                //        temp.ShortNameTH = item.unitShortname;
                //        temp.FullNameEN = item.unitNameEn;
                //        temp.FullNameTH = item.unitName;
                //        temp.OrderNo = "0";
                //        temp.OrganizationID = item.unitCode;
                //        temp.OrganizationLevelID = item.unitLevelValue;
                //        temp.HeadOfOrganizationID = "";
                //        temp.HeadOfOrganizationName = "";
                //        oReturn.Add(temp);
                //    }

                //}
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HROM", "getOMUnitByPeriodDate Error", responseModel.message);
            }


            return oReturn;
        }

      
    }
}
