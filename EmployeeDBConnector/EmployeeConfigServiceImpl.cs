using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.CONFIG.WF;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.EXTENSION;
using ESS.UTILITY.LOG;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;

namespace ESS.EMPLOYEE.DB
{
    public class EmployeeConfigServiceImpl : AbstractEmployeeConfigService
    {
        #region Constructor
        public EmployeeConfigServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private string CompanyCode { get; set; }

        private static string ModuleID = "ESS.EMPLOYEE.DB";



        #endregion Member

        private CultureInfo oCL = new CultureInfo("th-TH");
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();

        #region " Private Data "
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        #endregion " Private Data "

        #region " ConfigData "

        private delegate void GetData<T>(DataTable Table, List<T> Data);

        private delegate List<T> ParseObject<T>(DataTable dt);

        private void SaveConfig<T>(string ConfigName, List<T> Data, GetData<T> Method)
        {
            SaveConfig<T>(ConfigName, Data, Method, "BaseConnStr");
        }

        private void SaveConfig<T>(string ConfigName, List<T> Data, GetData<T> Method, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from {0}", ConfigName), oConnection);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from {0}", ConfigName);
                oCommand.Transaction = tx;
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                Method(oTable, Data);
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save {0} Error", ConfigName), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }

        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method)
        {
            return LoadConfig<T>(ConfigName, Method, false);
        }

        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method, bool CaseSensitive)
        {
            return LoadConfig<T>(ConfigName, Method, CaseSensitive, "");
        }

        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method, bool CaseSensitive, string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            List<T> oReturn = new List<T>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oTable.CaseSensitive = CaseSensitive;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0}", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0}", ConfigName), oConnection);
            }
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oReturn.AddRange(Method(oTable));
            oTable.Dispose();
            return oReturn;
        }

        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code)
        {
            return LoadConfigSingle<T>(ConfigName, Method, Code, "");
        }

        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code, string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            T oReturn = default(T);
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName);
            oTable.CaseSensitive = false;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0} Where {0}Key = @Key", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0} Where {0}Key = @Key", ConfigName), oConnection);
            }
            SqlParameter oParam = new SqlParameter("@Key", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<T> list = Method(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " ConfigData "

        #region IHRPAConfig Members

        #region " PersonalSubAreaSetting "

        public override List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile)
        {
            return this.LoadConfig<PersonalSubAreaSetting>("PersonalSubAreaSetting", new ParseObject<PersonalSubAreaSetting>(ParseToPersonalSubAreaSetting));
        }

        public override PersonalArea GetPersonalAreaSetting(string PersonalArea)
        {
            PersonalArea oReturn = new PersonalArea();
            if (PersonalArea == null)
            {
                return null;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalArea");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalArea Where PersonalAreaCode = @Key1", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = PersonalArea;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<PersonalArea> list = oTable.ToList<PersonalArea>();
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }

        public override PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea)
        {
            PersonalSubAreaSetting oReturn = new PersonalSubAreaSetting();
            if (PersonalArea == null || PersonalSubArea == null)
            {
                return null;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubAreaSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalSubAreaSetting Where PersonalArea = @Key1 and PersonalSubArea = @Key2", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = PersonalArea;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = PersonalSubArea;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<PersonalSubAreaSetting> list = ParseToPersonalSubAreaSetting(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea)
        {
            List<PersonalSubAreaSetting> oReturn = new List<PersonalSubAreaSetting>();
            if (String.IsNullOrEmpty(strArea))
            {
                return null;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubAreaSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalSubAreaSetting Where PersonalArea = @Key1", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = strArea;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oReturn = ParseToPersonalSubAreaSetting(oTable);
            return oReturn;
        }

        private List<PersonalSubAreaSetting> ParseToPersonalSubAreaSetting(DataTable dt)
        {
            List<PersonalSubAreaSetting> oReturn = new List<PersonalSubAreaSetting>();
            foreach (DataRow dr in dt.Rows)
            {
                PersonalSubAreaSetting Item = new PersonalSubAreaSetting();
                Item.PersonalArea = (string)dr["PersonalArea"];
                Item.PersonalSubArea = (string)dr["PersonalSubArea"];
                Item.Description = (string)dr["Description"];
                Item.CountryGrouping = (string)dr["CountryGrouping"];
                Item.AbsAttGrouping = (string)dr["AbsAttGrouping"];
                Item.TimeQuotaGrouping = (string)dr["TimeQuotaGrouping"];
                Item.HolidayCalendar = (string)dr["HolidayCalendar"];
                Item.WorkScheduleGrouping = (string)dr["WorkScheduleGrouping"];
                Item.DailyWorkScheduleGrouping = (string)dr["DailyWorkScheduleGrouping"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        public override void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> data, string Profile)
        {
            this.SaveConfig<PersonalSubAreaSetting>("PersonalSubAreaSetting", data, new GetData<PersonalSubAreaSetting>(ParsePersonalSubAreaSettingToTable), Profile);
        }

        private void ParsePersonalSubAreaSettingToTable(DataTable oTable, List<PersonalSubAreaSetting> Data)
        {
            foreach (PersonalSubAreaSetting item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["PersonalArea"] = item.PersonalArea;
                oNewRow["PersonalSubArea"] = item.PersonalSubArea;
                oNewRow["Description"] = item.Description;
                oNewRow["CountryGrouping"] = item.CountryGrouping;
                oNewRow["AbsAttGrouping"] = item.AbsAttGrouping;
                oNewRow["TimeQuotaGrouping"] = item.TimeQuotaGrouping;
                oNewRow["HolidayCalendar"] = item.HolidayCalendar;
                oNewRow["WorkScheduleGrouping"] = item.WorkScheduleGrouping;
                oNewRow["DailyWorkScheduleGrouping"] = item.DailyWorkScheduleGrouping;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " PersonalSubAreaSetting "

        #region " PersonalSubGroupSetting "

        public override List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string Profile)
        {
            return this.LoadConfig<PersonalSubGroupSetting>("PersonalSubGroupSetting", new ParseObject<PersonalSubGroupSetting>(ParseToPersonalSubGroupSetting));
        }

        public override PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup)
        {
            PersonalSubGroupSetting oReturn = new PersonalSubGroupSetting();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubGroupSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalSubGroupSetting Where EmpGroup = @Key1 and EmpSubGroup = @Key2", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = EmpGroup;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = EmpSubGroup;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<PersonalSubGroupSetting> list = ParseToPersonalSubGroupSetting(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }

        private List<PersonalSubGroupSetting> ParseToPersonalSubGroupSetting(DataTable dt)
        {
            List<PersonalSubGroupSetting> oReturn = new List<PersonalSubGroupSetting>();
            foreach (DataRow dr in dt.Rows)
            {
                PersonalSubGroupSetting Item = new PersonalSubGroupSetting();
                Item.EmpGroup = (string)dr["EmpGroup"];
                Item.EmpSubGroup = (string)dr["EmpSubGroup"];
                Item.WorkScheduleGrouping = (string)dr["WorkScheduleGrouping"];
                Item.TimeQuotaTypeGrouping = (string)dr["TimeQuotaTypeGrouping"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        public override void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> data, string Profile)
        {
            this.SaveConfig<PersonalSubGroupSetting>("PersonalSubGroupSetting", data, new GetData<PersonalSubGroupSetting>(ParsePersonalSubGroupSettingToTable), Profile);
        }

        private void ParsePersonalSubGroupSettingToTable(DataTable oTable, List<PersonalSubGroupSetting> Data)
        {
            foreach (PersonalSubGroupSetting item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["EmpGroup"] = item.EmpGroup;
                oNewRow["EmpSubGroup"] = item.EmpSubGroup;
                oNewRow["WorkScheduleGrouping"] = item.WorkScheduleGrouping;
                oNewRow["TimeQuotaTypeGrouping"] = item.TimeQuotaTypeGrouping;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " PersonalSubGroupSetting "

        #region " MonthlyWorkSchedule "

        public override void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            SqlParameter oParam;
            DataTable oTable = new DataTable("MonthlyWorkSchedule");
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from MonthlyWorkSchedule where WS_Year = @Year"), oConnection);
            oParam = new SqlParameter("@Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from MonthlyWorkSchedule where WS_Year = @Year");
                oCommand.Transaction = tx;
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                ParseMonthlyWorkscheduleToTable(oTable, Data);
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save MonthlyWorkSchedule Error"), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }

        private void ParseMonthlyWorkscheduleToTable(DataTable oTable, List<MonthlyWS> Data)
        {
            foreach (MonthlyWS item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                foreach (PropertyInfo oProp in item.GetType().GetProperties())
                {
                    if (oTable.Columns.Contains(oProp.Name))
                    {
                        oNewRow[oProp.Name] = oProp.GetValue(item, null);
                    }
                }
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        private List<MonthlyWS> ParseToMonthlyWS(DataTable dt)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();
            Type oType = typeof(MonthlyWS);
            PropertyInfo oProp;
            foreach (DataRow dr in dt.Rows)
            {
                MonthlyWS Item = new MonthlyWS();
                Item.EmpSubGroupForWorkSchedule = (string)dr["EmpSubGroupForWorkSchedule"];
                Item.PublicHolidayCalendar = (string)dr["PublicHolidayCalendar"];
                Item.EmpSubAreaForWorkSchedule = (string)dr["EmpSubAreaForWorkSchedule"];
                Item.WorkScheduleRule = (string)dr["WorkScheduleRule"];
                Item.ValuationClass = (string)dr["ValuationClass"];
                Item.WS_Year = (int)dr["WS_Year"];
                Item.WS_Month = (int)dr["WS_Month"];
                string cDayName;
                for (int index = 1; index <= 31; index++)
                {
                    cDayName = string.Format("Day{0}", index.ToString("00"));
                    oProp = oType.GetProperty(cDayName);
                    oProp.SetValue(Item, dr[cDayName], null);

                    cDayName = string.Format("Day{0}_H", index.ToString("00"));
                    oProp = oType.GetProperty(cDayName);
                    oProp.SetValue(Item, dr[cDayName], null);
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        public override MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month)
        {
            MonthlyWS oReturn = new MonthlyWS();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("MonthlyWS");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from MonthlyWorkSchedule Where EmpSubGroupForWorkSchedule = @Key1 and PublicHolidayCalendar = @Key2 and EmpSubAreaForWorkSchedule = @Key3 and WorkScheduleRule = @Key4 and WS_Year = @Key5 and WS_Month = @Key6", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = EmpSubGroupForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = PublicHolidayCalendar;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key3", SqlDbType.VarChar);
            oParam.Value = EmpSubAreaForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key4", SqlDbType.VarChar);
            oParam.Value = WorkScheduleRule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key5", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key6", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<MonthlyWS> list = ParseToMonthlyWS(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " MonthlyWorkSchedule "

        #region " SaveDailyWorkscheduleList "

        public override void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable("DailyWorkschedule");
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from DailyWorkschedule"), oConnection);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from DailyWorkschedule");
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                foreach (DailyWS item in Data)
                {
                    item.LoadDataToTable(oTable);
                }
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save DailyWorkschedule Error"), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }

        #endregion " SaveDailyWorkscheduleList "

        #region " GetDailyWorkschedule "

        public override DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate)
        {
            DailyWS oReturn = new DailyWS();
            if (DailyCode.Contains("-"))
            {
                oReturn.WorkBeginTime = DateTime.ParseExact(DailyCode.Split('-')[0], "HH:mm:ss", oCL).TimeOfDay;
                oReturn.WorkEndTime = DateTime.ParseExact(DailyCode.Split('-')[1], "HH:mm:ss", oCL).TimeOfDay;
                oReturn.DailyWorkscheduleCode = String.Empty;
                oReturn.BeginDate = CheckDate;
                oReturn.EndDate = CheckDate;
                oReturn.IsHoliday = false;
                oReturn.WorkscheduleClass = "1";
                oReturn.WorkHours = 8;
            }
            else
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand;
                SqlDataAdapter oAdapter;
                DataTable oTable = new DataTable("DailyWS");
                oTable.CaseSensitive = false;
                oCommand = new SqlCommand("select * from DailyWorkSchedule Where DailyWorkscheduleGrouping = @DailyGroup and DailyWorkscheduleCode = @DailyCode and @CheckDate between BeginDate and EndDate", oConnection);
                SqlParameter oParam;
                oParam = new SqlParameter("@DailyGroup", SqlDbType.VarChar);
                oParam.Value = DailyGroup;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@DailyCode", SqlDbType.VarChar);
                oParam.Value = DailyCode;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                oParam.Value = CheckDate;
                oCommand.Parameters.Add(oParam);

                oAdapter = new SqlDataAdapter(oCommand);
                oConnection.Open();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                foreach (DataRow dr in oTable.Rows)
                {
                    oReturn.ParseToObject(dr);
                    break;
                }
                oTable.Dispose();
            }
            return oReturn;
        }

        public override DailyWS GetDailyWorkschedule(string EmployeeID, DateTime CheckDate)
        {
            DailyWS oReturn = new DailyWS();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable();
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("sp_GetWorkSchedule", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            string Day=string.Empty;
            string Day_H=string.Empty;
            string EmpSubAreaForWorkSchedule;
            if (oTable.Rows.Count > 0)
            {
                Day = oTable.Rows[0][String.Format("Day{0:00}", CheckDate.Day)].ToString();
                Day_H = oTable.Rows[0][String.Format("Day{0:00}_H", CheckDate.Day)].ToString();
                EmpSubAreaForWorkSchedule = oTable.Rows[0]["EmpSubAreaForWorkSchedule"].ToString();

                oTable = new DataTable("DailyWS");
                oTable.CaseSensitive = false;
                oCommand = new SqlCommand("sp_GetDailyWorkSchedule", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                oParam = new SqlParameter("@DailyWorkscheduleCode", SqlDbType.VarChar);
                if (!string.IsNullOrEmpty(Day_H))
                    oParam.Value = "OFF";
                else
                    oParam.Value = Day;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@DailyWorkscheduleGrouping", SqlDbType.VarChar);
                oParam.Value = EmpSubAreaForWorkSchedule;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                oParam.Value = CheckDate;
                oCommand.Parameters.Add(oParam);

                oAdapter = new SqlDataAdapter(oCommand);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
            }
            else { //throw new Exception(string.Format("Cannot found DialyWorkschedule for {0} on {1}", EmployeeID, CheckDate)); 
            }


            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.ParseToObject(dr);
                oReturn.IsHoliday = !string.IsNullOrEmpty(Day_H);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetDailyWorkschedule "

        public override BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            BreakPattern oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("BreakPattern");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from BreakPattern Where DailyWorkscheduleGrouping = @DWSGroup and BreakCode = @BreakCode", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@DWSGroup", SqlDbType.VarChar);
            oParam.Value = DWSGroup;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BreakCode", SqlDbType.VarChar);
            oParam.Value = BreakCode;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = new BreakPattern();
                oReturn.ParseToObject(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion IHRPAConfig Members

        public override WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            WFRuleSetting oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("WFRuleSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from WFRuleSetting Where WFRule = @WFRule", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@WFRule", SqlDbType.VarChar);
            oParam.Value = WFRule;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = new WFRuleSetting();
                oReturn.ParseToObject(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar)
        {
            return GetMonthlyWorkscheduleGroup(EmpSubGroupForWorkSchedule, EmpSubAreaForWorkSchedule, PublicHolidayCalendar, false, false);
        }

        public override List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("MonthlyWS");
            oTable.CaseSensitive = false;
            string Include = string.Empty;
            if (IncludeMonth)
                Include += ",WS_Month";
            if (IncludeYear)
                Include += ",WS_Year";
            oCommand = new SqlCommand(string.Format("SELECT distinct WorkScheduleRule {0} from MonthlyWorkSchedule Where  EmpSubGroupForWorkSchedule= @Key1 and EmpSubAreaForWorkSchedule = @Key2 and PublicHolidayCalendar = @Key3", Include), oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = EmpSubGroupForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = EmpSubAreaForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key3", SqlDbType.VarChar);
            oParam.Value = PublicHolidayCalendar;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return ParseToMonthlyWorkScheduleRule(oTable);
        }

        private List<MonthlyWS> ParseToMonthlyWorkScheduleRule(DataTable dt)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();
            MonthlyWS item;
            foreach (DataRow dr in dt.Rows)
            {
                item = new MonthlyWS();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            return oReturn;
        }

        public override List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();
            MonthlyWS item;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("MonthlyWS");
            string sql = "SELECT * from MonthlyWorkSchedule Where  EmpSubGroupForWorkSchedule= @EmpSubGroupForWorkSchedule and PublicHolidayCalendar = @PublicHolidayCalendar and EmpSubAreaForWorkSchedule = @EmpSubAreaForWorkSchedule and WS_Year = @WS_Year and WS_Month = @WS_Month";
            foreach (KeyValuePair<string, string> pair in DayOption)
                sql += String.Format(" and {0} = @{0}", pair.Value.Split('|')[0]);

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpSubGroupForWorkSchedule", SqlDbType.VarChar);
            oParam.Value = EmpSubGroupForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@PublicHolidayCalendar", SqlDbType.VarChar);
            oParam.Value = PublicHolidayCalendar;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpSubAreaForWorkSchedule", SqlDbType.VarChar);
            oParam.Value = EmpSubAreaForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@WS_Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@WS_Month", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);

            string[] stringSeparators = new string[] { "[and]" };
            foreach (KeyValuePair<string, string> pair in DayOption)
            {
                oParam = new SqlParameter("@" + pair.Value.Split('|')[0], SqlDbType.VarChar);
                oParam.Value = pair.Value.Split('|')[1];
                oCommand.Parameters.Add(oParam);
            }

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }

            foreach (DataRow row in oTable.Rows)
            {
                item = new MonthlyWS();
                item.ParseToObject(row);
                oReturn.Add(item);
            }

            return oReturn;
        }

        public override List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate)
        {
            List<DailyWS> oReturn = new List<DailyWS>();
            DailyWS oitem = new DailyWS();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("DailyWS");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from DailyWorkSchedule Where DailyWorkscheduleGrouping = @DailyWorkscheduleGrouping and @CheckDate between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@DailyWorkscheduleGrouping", SqlDbType.VarChar);
            oParam.Value = WorkScheduleGrouping;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oitem = new DailyWS();
                oitem.ParseToObject(dr);
                oReturn.Add(oitem);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<string> GetPersonalSubAreaWSRMapping(string strSubArea)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubAreaWSRMapping");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("SELECT * FROM PersonalSubAreaWSRMapping WHERE PersonalSubArea = @SubArea", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@SubArea", SqlDbType.VarChar);
            oParam.Value = strSubArea;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(dr["WorkScheduleRule"].ToString());
            }
            oTable.Dispose();
            return oReturn;
        }

        #region " Bind Selector "
        public override List<INFOTYPE1000> GetGenderCheckBoxData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT GenderKey as ObjectID,GenderName as ShortText,GenderName as Text,GenderName as ShortTextEn,GenderName as TextEn FROM Gender";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetPrefixDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT TitleNameKey as ObjectID,TitleAbbreviation as ShortText,TitleName as Text,TitleAbbreviation as ShortTextEn,TitleName as TextEn FROM TitleName";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetMaritalStatusDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT MaritalStatusKey as ObjectID,MaritalStatusName as ShortText,MaritalStatusName as Text,MaritalStatusName as ShortTextEn,MaritalStatusName as TextEn FROM MaritalStatus";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetNationalityDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT NationalityKey as ObjectID,TextDescription as ShortText,TextDescription as Text,NationalityName as ShortTextEn,NationalityName as TextEn FROM Nationality left join TextDescription on NationalityKey = TextCode and CategoryCode = 'COUNTRY' and LanguageCode = 'TH' order by case when NationalityKey = 'TH' then 1 else 2 END,NationalityKey";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetLanguageDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT l.LanguageKey as ObjectID,t.TextDescription as ShortText,t.TextDescription as Text"
                            + ",l.LanguageName as ShortTextEn,l.LanguageName as TextEn FROM Language l "
                            + "left join TextDescription t on CAST((l.LanguageKey COLLATE Thai_CI_AS) + ':' + (l.LanguageCode COLLATE Thai_CI_AS) as varchar) = CAST(t.TextCode as varchar) and t.CategoryCode = 'LANGUAGE' and t.LanguageCode = 'TH' order by case when l.LanguageKey = '2' then 1 when l.LanguageKey = 'E' then 2 else 3 END,l.LanguageKey";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetReligionDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT ReligionKey as ObjectID,ReligionName as ShortText,ReligionName as Text,ReligionName as ShortTextEn,ReligionName as TextEn FROM Religion";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetEmpGroupDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT EmpGroup as ObjectID,EmpGroupText as ShortText,EmpGroupText as Text,EmpGroupText as ShortTextEn,EmpGroupText as TextEn FROM EmpGroup";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetEmpSubGroupDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT EmpSubGroup as ObjectID,EmpSubGroupText as ShortText,EmpSubGroupText as Text,EmpSubGroupText as ShortTextEn,EmpSubGroupText as TextEn FROM EmpSubGroup";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetSubTypeDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT DISTINCT CategoryCode as ObjectID,CategoryCode as ShortText,CategoryCode as Text,CategoryCode as ShortTextEn,CategoryCode as TextEn FROM INFOTYPE0105_Mapping --WHERE SubType != '0001'";
            //string cmdStr = "SELECT DISTINCT CategoryCode as ObjectID,CategoryCode as ShortText,CategoryCode as Text,CategoryCode as ShortTextEn,CategoryCode as TextEn FROM PA_Communication_Mapping --WHERE SubType != '0001'";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetCostCenterByOrganize(string Organize)
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT DISTINCT a.NextObjectID as ObjectID,a.BeginDate,a.EndDate " +
                            ",b.ShortText as ShortText,b.Text as Text,b.ShortTextEn as ShortTextEn,b.TextEn as TextEn " +
                            "FROM INFOTYPE1001 a left join INFOTYPE1000 b on b.ObjectType = 'K' and b.ObjectID = a.NextObjectID " +
                            "and GETDATE() between b.BeginDate and b.EndDate " +
                            "WHERE a.NextObjectType = 'K' and a.ObjectType = 'O' and a.Relation = 'A011' and a.ObjectID = @p_Organize " +
                            "and GETDATE() BETWEEN a.BeginDate AND a.EndDate";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oCommand.Parameters.AddWithValue("@p_Organize", Organize);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate)
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT DISTINCT CostCenterCode as ObjectID,BeginDate,EndDate,ShortDesc as ShortText,LongDesc as Text,ShortDesc as ShortTextEn,LongDesc as TextEn FROM CO_CostCenter WHERE @p_CheckDate BETWEEN BeginDate AND EndDate";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oCommand.Parameters.AddWithValue("@p_CheckDate", CheckDate);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetAreaDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT DISTINCT PersonalAreaCode as ObjectID,[Description] as ShortText,[Description] as Text,[Description] as ShortTextEn,[Description] as TextEn FROM PersonalArea";//"SELECT DISTINCT AreaID as ObjectID,Name as ShortText,Name as Text,Name as ShortTextEn,Name as TextEn FROM PersonalArea";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetSubAreaDropdownData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT DISTINCT SubArea as ObjectID,SubAreaText as ShortText,SubAreaText as Text,SubAreaText as ShortTextEn,Area as TextEn FROM SubArea";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetRelationSelectData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT RelationID as ObjectID,RelationText as ShortText,RelationText as Text,RelationText as ShortTextEn,RelationText as TextEn FROM Relation";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<string> GetRelationValidationByRelation(string relation)
        {
            List<string> oReturn = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "select * from RelationValidation where Relation = @relation";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oCommand.Parameters.AddWithValue("@relation", relation);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                string temp = dr.ItemArray[1].ToString() + "," + dr.ItemArray[2].ToString();
                oReturn.Add(temp);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1000> GetRelationValidationSelectData()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "SELECT r.Relation+r.ObjectType1+r.ObjectType2 as ObjectID,o.ObjectTypeText as ShortText,o.ObjectTypeText+' '+a.RelationText+' '+t.ObjectTypeText as Text,t.ObjectTypeText as ShortTextEn,o.ObjectTypeText+' '+a.RelationText+' '+t.ObjectTypeText as TextEn " +
                            "FROM RelationValidation r LEFT JOIN Relation a ON a.RelationID=r.Relation LEFT JOIN ObjectType o ON o.ObjectTypeID=r.ObjectType1 LEFT JOIN ObjectType t ON t.ObjectTypeID=r.ObjectType2";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 obj = new INFOTYPE1000();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region Common
        private IList<T> ExecuteQuery<T>(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            IList<T> oResult = new List<T>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oAdapter.Fill(oTable);
                        if (oTable != null && oTable.Rows.Count > 0)
                        {
                            oResult = Convert<T>.ConvertFrom(oTable);
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private bool ExecuteNoneQuery(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            bool oResult = false;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        if (oCommand.ExecuteNonQuery() > 0)
                        {
                            oResult = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private string ExecuteScalar(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            string oResult = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        oResult = Convert.ToString(oCommand.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        #endregion

        public override DataTable GetMonthlyWorkscheduleGetByPeriod(string EmployeeID, int iYear, int iMonth)
        {
            oSqlManage["ProcedureName"] = "sp_MonthlyWorkscheduleGetByPeriod";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oParamRequest["@Year"] = iYear;
            oParamRequest["@Month"] = iMonth;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }
        private bool InsertActionLog(object objOld, object objNew, string oAction, bool oStatus)
        {
            bool flg = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            flg = InsertActionLog(oActionLog);
            return flg;
        }
        public override bool InsertActionLog(ActionLog oActionLog)
        {
            bool flg = false;
            oSqlManage["ProcedureName"] = "sp_ActionLogSet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LogAction"] = oActionLog.LogAction;
            oParamRequest["@p_LogData"] = oActionLog.LogData;
            oParamRequest["@p_LogActionBy"] = oActionLog.LogActionBy;
            oParamRequest["@p_LogStatus"] = oActionLog.LogStatus;
            oParamRequest["@p_RequestNo"] = oActionLog.RequestNo;
            flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            return flg;
        }

        public override bool InsertJobActionLog(JobActionLog oJobActionLog)
        {
            bool flg = false;
            oSqlManage["ProcedureName"] = "sp_Job_LogInsert";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_JobID"] = oJobActionLog.JobID;
            oParamRequest["@p_JobTypeID"] = oJobActionLog.JobTypeID;
            oParamRequest["@p_LogData"] = oJobActionLog.LogData;
            oParamRequest["@p_LogDate"] = oJobActionLog.LogDate;
            oParamRequest["@p_LogRemark"] = oJobActionLog.LogRemark;
            flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            return flg;
        }

        public override string GetBusinessPlace(string oBusinessArea)
        {
            oSqlManage["ProcedureName"] = "sp_BusinessPlaceGetByBusinessArea";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_BusinessArea"] = oBusinessArea;
            string oResult = string.Empty;
            try
            {
                oResult = DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {
            }
            return oResult;
        }

        public override List<AttachedFileConfiguration> GetAttachedFileConfigurationList(string CategoryGroup, string CategoryType)
        {
            List<AttachedFileConfiguration> oReturn = new List<AttachedFileConfiguration>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AttachedFileConfigurationGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_CategoryGroup", CategoryGroup);
            oCommand.Parameters.AddWithValue("@p_CategoryType", CategoryType);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oAdapter.Fill(oTable);

            foreach (DataRow dr in oTable.Rows)
            {
                AttachedFileConfiguration AttachedFileConfiguration = new AttachedFileConfiguration();
                AttachedFileConfiguration.ParseToObject(dr);
                oReturn.Add(AttachedFileConfiguration);
            }
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();

            return oReturn;
        }

        public override DataTable GetOffCycleTypeList(string oLang)
        {
            DataTable returnValue = new DataTable();
            returnValue.Columns.Add("TypeID", typeof(string));
            returnValue.Columns.Add("Description", typeof(string));
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOffCycleType", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_Lang", oLang);


            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OFFCYCLETYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                DataRow nRow = returnValue.NewRow();

                nRow["TypeID"] = dr["TypeID"];
                nRow["Description"] = dr["Description"];

                returnValue.Rows.Add(nRow);
            }
            oTable.Dispose();
            return returnValue;
        }

        #region JOB

        public override string JobALL_DHR_TO_PersonalSubAreaSetting()
        {
            string result = "Job Copy PersonalSubAreaSetting Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_PersonalSubAreaSetting", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_PersonalSubGroupSetting()
        {
            string result = "Job Copy PersonalSubGroupSetting Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_PersonalSubGroupSetting", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_MonthlyWorkschedule(int Year)
        {
            string result = "Job Copy MonthlyWorkschedule Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_MonthlyWorkschedule", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_Year", Year);
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        public override string JobALL_DHR_TO_DailyWorkschedule()
        {
            string result = "Job Copy DailyWorkschedule Completed.";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    try
                    {
                        oCommand = new SqlCommand("sp_JobEmployee_DHR_TO_DailyWorkschedule", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.ExecuteNonQuery();
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        result = "Error:" + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oConnection.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return result;
        }

        #endregion JOB

        #region New Workflow
        public override List<RequestTypeKeyCodeSetting> GetRequestFlowSettingByCriteria(int oRequestTypeID, int oVersion)
        {
            List<RequestTypeKeyCodeSetting> oReturn = new List<RequestTypeKeyCodeSetting>();
            oSqlManage["ProcedureName"] = "sp_GetRequestTypeKeyCodeSetting";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestTypeID"] = oRequestTypeID;
            oParamRequest["@p_VersionID"] = oVersion;
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<RequestTypeKeyCodeSetting>(oSqlManage,oParamRequest));// = DatabaseHelper.(oSqlManage, oParamRequest);
            return oReturn;
        }
        #endregion
    }
}