﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace DHR.HR.API
{
    public class OrganizationRoute
    {
        public OrganizationRoute()
        {

        }
        private static string ModuleID = "DHR.ROUTE.OM";

        private string CompanyCode { get; set; }

        public static OrganizationRoute Route(string oCompanyCode)
        {
            OrganizationRoute oOrganizationRoute = new OrganizationRoute()
            {
                CompanyCode = oCompanyCode
            };

            return oOrganizationRoute;
        }
        #region   #region API Unused
        //public string OrganizationRoute_WsSentBelongTo
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMBELONGTO");
        //    }
        //}

        //public string OrganizationRoute_WsSentComSecondment
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMCOMSECONDMENT");
        //    }
        //}
        //public string OrganizationRoute_WsSentCompany
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMCOMPANY");
        //    }
        //}

        //public string OrganizationRoute_WsSentFiscalYear
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMFISCALYEAR");
        //    }
        //}

        //public string OrganizationRoute_WsSentPosition
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMPOSITION");
        //    }
        //}

        //public string OrganizationRoute_WsSentReportTo
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMREPORTTO");
        //    }
        //}

        //public string OrganizationRoute_WsSentUnit
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMUNIT");
        //    }
        //}

        //public string OrganizationRoute_WsSentSolidLine
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMSOLIDLINE");
        //    }
        //}

        //public string OrganizationRoute_WsSentDotedLine
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMDOTEDLINE");
        //    }
        //}
        #endregion
        #region OM_POSITION
        public string OM_PositionRoute_UIGETOMPOSITION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIGETOMPOSITION");
            }
        }
        public string OM_PositionRoute_UIPOSTOMPOSITION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIPOSTOMPOSITION");
            }
        }
        public string OM_PositionRoute_UIPOSTOMDOTEDLINE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIPOSTDOTEDLINE");
            }
        }
        #endregion

        public string OrganizationRoute_WsSentUnit
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMUNIT");
            }
        }
       
        //public string OrganizationRoute_WsSentSolidLine
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMSOLIDLINE");
        //    }
        //}

        //public string OrganizationRoute_WsSentDotedLine
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTOMDOTEDLINE");
        //    }
        //}
        public string OrganizationRoute_WsSentOMUnit_UI
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIGETOMUNIT");
            }
        }
        public string OrganizationRoute_WsReceiveOMUnitInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIPOSTOMUNIT");
            }
        }
    }
}
