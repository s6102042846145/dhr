﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class OrganizationModel
    {
      public int? id { get; set; }
      public string companyCode { get; set; }
      public string unitCode { get; set; }
      public string unitShortname { get; set; }
      public string unitName { get; set; }
      public string unitShortnameEn { get; set; }
      public string unitNameEn { get; set; }
      public string unitLevelCode { get; set; }
      public string unitLevelValue { get; set; }
      public string unitLevelTextTH { get; set; }
      public string unitLevelTextEN { get; set; }
      public string costCenterCode { get; set; }
      public string costCenterValue { get; set; }
      public string costCenterTextTH { get; set; }
      public string costCenterTextEN { get; set; }
      public string upperUnitCode { get; set; }
      public string startDate { get; set; }
      public string endDate { get; set; }
      public string createDate { get; set; }
      public string createBy { get; set; }
      public string updateDate { get; set; }
      public string updateBy { get; set; }
      public string headNameTh { get; set; }
      public string headNameEn { get; set; }
      public string headPositionTh { get; set; }
      public string headPositionEn { get; set; }
      public string priority { get; set; }
      public string actiontype { get; set; }
      public string servicetype { get; set; }
    }
}
