﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class Company
    {
        public string companyCode { get; set; }
        public string companyName { get; set; }
        public string addressCode { get; set; }
        public string building { get; set; }
        public string floor { get; set; }
        public string addressNo { get; set; }
        public string street { get; set; }
        public string subdistrictCode { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string postcode { get; set; }
        public string companyTaxid { get; set; }
        public string companyAccount { get; set; }
        public string country { get; set; }
        public string companyNameEn { get; set; }
        public string buildingEn { get; set; }
        public string floorEn { get; set; }
        public string addressNoEn { get; set; }
        public string streetEn { get; set; }
        public string primaryLang { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
