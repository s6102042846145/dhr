﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class BelongTo
    {
        public string companyCode { get; set; }
        public string posCode { get; set; }
        public string unitCode{ get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
