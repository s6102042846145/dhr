﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class SolidLineModel
    {
        public string id { get; set; }
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string posCode { get; set; }
        public string posShortname { get; set; }
        public string posName { get; set; }
        public string posShortnameEn { get; set; }
        public string posNameEn { get; set; }
        public string bandCode { get; set; }
        public string bandValue { get; set; }
        public string headOfUnitF { get; set; }
        public string unitCode { get; set; }
        public string headCode { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }

    }
}

