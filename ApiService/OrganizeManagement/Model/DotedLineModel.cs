﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class DotedLineModel
    {
        public string id { get; set; }
        public string companyCode { get; set; }
        public string posCode { get; set; }
        public string headPosition { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }

    }
}
