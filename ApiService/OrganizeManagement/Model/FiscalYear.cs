﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class FiscalYear
    {
        public string companyCode { get; set; }
        public string startMonth { get; set; }
        public string endMonth { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
