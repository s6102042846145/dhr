﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using System.Web;

namespace DHR.HR.API
{
    public class APIOrgManagement
    {
        #region "Constructor"
        public APIOrgManagement()
        {

        }
        #endregion "Contructor"

        #region "Multicompany Framework"
        private Dictionary<string, APIOrgManagement> cache = new Dictionary<string, APIOrgManagement>();

        private static string ModuleID = "DHR.HR";

        private string CompanyCode { get; set; }

        private string _ApiKey = string.Empty;

        private string _empCode = string.Empty;

        public static APIOrgManagement CreateInstance(string oCompanyCode)
        {
            APIOrgManagement oAPIOrgManagement = new APIOrgManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAPIOrgManagement;

        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        private string ApiUser
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIUSER");
            }
        }

        private string ApiPassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIPASSWORD");
            }
        }

        public string ApiKey
        {
            get
            {
                string ApiKeyRequest = string.Format("ApiKeyRequest_{0}_{1}", CompanyCode, _empCode);
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                    {
                        _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                    }
                    else

                    {
                        _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                        HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                    }
                }
                else
                {
                    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                }
                //if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                //{
                //    _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                //}
                //else

                //{
                //    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                //    HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                //}

                return _ApiKey;

            }
            set
            {
                _ApiKey = value;
            }
        }

        public int Period_Time_Expired
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERIODTIMEEXPIRED"));
            }
        }

        #endregion "Multicompany Framework"

        #region API Unused
        //public ResponseModel<BelongTo> GetDHRBelongTo(string startDate, string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRBelongTo(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<ComSecondment> GetDHRComSecondment(string startDate , string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRComSecondment(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<Company> GetDHRCompany (string startDate, string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRCompany(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<FiscalYear> GetDHRFiscalYear(string startDate, string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRFiscalYear(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<Position> GetDHRPosition(string startDate,string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPosition(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<ReportTo> GetDHRReportTo(string startDate, string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRReportTo(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<SolidLine> GetDHRSolidLine(string startDate, string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRSolidLine(startDate, endDate, ApiKey);
        //}

        //public ResponseModel<DotedLine> GetDHRDotedLine(string startDate, string endDate)
        //{
        //    return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRDotedLine(startDate, endDate, ApiKey);
        //}
        #endregion
        #region OM_POSITION
        public ResponseModel<PositionModel> GetDHRPosition(string startDate, string endDate,string unitCode=null,string levelCode=null,string posCode=null)
        {
            return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPosition(ApiKey, startDate, endDate, unitCode, levelCode, posCode);
        }
        public PostResult PostDHROMSolidLine_UI(SolidLineModel oSolidData)
        {
            return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHROMSolidLine_UI(oSolidData, ApiKey);
        }
        public PostResult PostDHROMDotedLine_UI(DotedLineModel oDotedData)
        {
            return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHROMDotedLine_UI(oDotedData, ApiKey);
        }
        #endregion

        public ResponseModel<Unit> GetDHRUnit(string startDate, string endDate)
        {
            return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRUnit(startDate, endDate, ApiKey);
        }

        public ResponseModel<OrganizationModel> GetDHROMUnit_UI(string startDate, string endDate, string unitCode, string unitLevel)
        {
            return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHROMUnit_UI(startDate, endDate, unitCode, unitLevel, ApiKey);
        }
        public PostResult PostDHROMUnit_UI(OrganizationModel oOMData)
        {
            return OrgServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHROMUnit_UI(oOMData, ApiKey);
        }

    }
}
