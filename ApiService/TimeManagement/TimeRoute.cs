﻿using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class TimeRoute
    {
        public TimeRoute()
        {

        }
        private static string ModuleID = "DHR.ROUTE.TM";
        private string CompanyCode { get; set; }

        public static TimeRoute Route (string oCompanyCode)
        {
            TimeRoute oTimeRoute = new TimeRoute()
            {
                CompanyCode = oCompanyCode
            };
            return oTimeRoute;
        }
        
        public string TimeRoute_WsSentTMCalenHoliday
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMCALENHOLIDAY");
            }
        }
        
        public string TimeRoute_WsSentTMSetupHoliday
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMSETUPHOLIDAY");
            }
        }
        
        public string TimeRoute_WsSentTMPlanWorkTime
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMPLANWORKTIME");
            }
        }
        
        public string TimeRoute_WsSentTMTimeRecInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMTIMERECINF");
            }
        }
        
        public string TimeRoute_WsSentTMTimeRecType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMTIMERECTYPE");
            }
        }
        
        public string TimeRoute_WsSentTMLeaveInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMLEAVEINF");
            }
        }
        
        public string TimeRoute_WsSentTMTimeAttend
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMTIMEATTEND");
            }
        }
        
        public string TimeRoute_WsSentTMLeaveQuota
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMLEAVEQUOTA");
            }
        }
        
        public string TimeRoute_WsSentTMTimeRecInOut
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMTIMERECINOUT");
            }
        }
        
        public string TimeRoute_WsSentTMChaWorkSch
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMCHAWORKSCH");
            }
        }
        
        public string TimeRoute_WsSentTMOTTypeInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMOTTYPEINF");
            }
        }
        
        public string TimeRoute_WsSentTMLeaveType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMLEAVETYPE");
            }
        }
        
        public string TimeRoute_WsSentTMWorkingType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMWORKINGTYPE");
            }
        }
        
        public string TimeRoute_WsSentTMLeaveQuotaType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMLEAVEQUOTATYPE");
            }
        }
        
        public string TimeRoute_WsSentTMEmpGroup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMEMPGROUP");
            }
        }
        
        public string TimeRoute_WsSentTMPersonalArea
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMPERSONALAREA");
            }
        }
        
        public string TimeRoute_WsSentTMBreakSchedule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMBREAKSCHEDULE");
            }
        }
        
        public string TimeRoute_WsSentTMDailyWorkSchedule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMDAILYWORKSCHEDULE");
            }
        }
        
        public string TimeRoute_WsSentTMPeriodWorkSchedule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMPERIODWORKSCHEDULE");
            }
        }
        
        public string TimeRoute_WsSentTMWorkSchedule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMWORKSCHEDULE");
            }
        }
        
        public string TimeRoute_WsSentTMWorkScheduleRule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMWORKSCHEDULERULE");
            }
        }
        
        public string TimeRoute_WsSentTMOTType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMOTTYPE");
            }
        }
        
        public string TimeRoute_WsSentTMSubsititutionType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMSUBSITITUTIONTYPE");
            }
        }

        public string TimeRoute_WsSentTMCalculateLeaveData
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMCALCULATELEAVEDATA");
            }
        }
        public string TimeRoute_WsSentTMCalculateAttendData
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMCALCULATEATTENDANCEDATA");
            }
        }

        public string TimeRoute_WsSentTMCollisionCheck
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTTMCOLLISIONCHECK");
            }
        }
        #region POST

        public string TimeRoute_WsReceiveTMLeaveInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVETMLEAVEINF");
            }
        }
        
        public string TimeRoute_WsReceiveTMTimeAttend
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVETMTIMEATTEND");
            }
        }
        
        public string TimeRoute_WsReceiveTMChaWorkSch
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVETMCHAWORKSCH");
            }
        }
       
        public string TimeRoute_WsReceiveTMOTTypeInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVETMOTTYPEINF");
            }
        }

        public string TimeRoute_WsReceiveTMTimeRecInOut
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVETMTIMERECINOUT");
            }
        }
        #endregion POST
    }
}









