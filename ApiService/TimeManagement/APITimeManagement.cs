﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DHR.HR.API
{
    public class APITimeManagement
    {
        #region Constructor

        public APITimeManagement()
        {

        }

        #endregion Constructor

        #region Multicompany Framework

        private string ModuleID = "DHR.HR";

        private string CompanyCode { get; set; }

        private string _ApiKey = string.Empty;

        private string _empCode = string.Empty;

        public static APITimeManagement CreateInstance(string oCompanyCode)
        {
            APITimeManagement oAPITimeManagement = new APITimeManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPITimeManagement;
        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        private string ApiUser
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIUSER");
            }
        }

        private string ApiPassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIPASSWORD");
            }
        }

        public string ApiKey
        {
            get
            {
                string ApiKeyRequest = string.Format("ApiKeyRequest_{0}_{1}", CompanyCode, _empCode);
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                    {
                        _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                    }
                    else

                    {
                        _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                        HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                    }
                }
                else
                {
                    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                }
                //if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                //{
                //    _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                //}
                //else

                //{
                //    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                //    HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                //}

                return _ApiKey;

            }
            set
            {
                _ApiKey = value;
            }
        }

        public int Period_Time_Expired
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERIODTIMEEXPIRED"));
            }
        }
        #endregion

        #region GET ALL DATA

        public ResponseModel<TMCalenHoliday> GetDHRTMCalenHoliday(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMCalenHoliday(startDate, endDate, ApiKey);
        }


        public ResponseModel<TMSetupHoliday> GetDHRTMSetupHoliday(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMSetupHoliday(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMPlanWorkTime> GetDHRTMPlanWorkTime(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMPlanWorkTime(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMTimeRecInf> GetDHRTMTimeRecInf(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeRecInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMTimeRecType> GetDHRTMTimeRecType(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeRecType(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMLeaveInf> GetDHRTMLeaveInf(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMLeaveInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMTimeAttend> GetDHRTMTimeAttend(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeAttend(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMLeaveQuota> GetDHRTMLeaveQuota(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMLeaveQuota(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMTimeRecInOut> GetDHRTMTimeRecInOut(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeRecInOut(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMChaWorkSch> GetDHRTMChaWorkSch(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMChaWorkSch(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMOTTypeInf> GetDHRTMOTTypeInf(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMOTTypeInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMLeaveType> GetDHRTMLeaveType(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMLeaveType(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMWorkingType> GetDHRTMWorkingType(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMWorkingType(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMLeaveQuotaType> GetDHRTMLeaveQuotaType(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMLeaveQuotaType(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMEmpGroup> GetDHRTMEmpGroup(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMEmpGroup(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMPersonalArea> GetDHRTMPersonalArea(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMPersonalArea(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMBreakSchedule> GetDHRTMBreakSchedule(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMBreakSchedule(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMDailyWorkSchedule> GetDHRTMDailyWorkSchedule(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMDailyWorkSchedule(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMPeriodWorkSchedule> GetDHRTMPeriodWorkSchedule(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMPeriodWorkSchedule(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMWorkSchedule> GetDHRTMWorkSchedule(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMWorkSchedule(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMWorkScheduleRule> GetDHRTMWorkScheduleRule(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMWorkScheduleRule(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMOTType> GetDHRTMOTType(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMOTType(startDate, endDate, ApiKey);
        }

        public ResponseModel<TMSubsititutionType> GetDHRTMSubsititutionType(string startDate, string endDate)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMSubsititutionType(startDate, endDate, ApiKey);
        }

        #endregion GET ALL DATA

        #region GET BY EMPCODE


        public ResponseModel<TMTimeRecInf> GetDHRTMTimeRecInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeRecInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMLeaveInf> GetDHRTMLeaveInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMLeaveInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMTimeAttend> GetDHRTMTimeAttendByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeAttendByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMLeaveQuota> GetDHRTMLeaveQuotaByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMLeaveQuotaByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMTimeRecInOut> GetDHRTMTimeRecInOutByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMTimeRecInOutByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMCollisionCheck> GetDHRTMCollisionCheck(string startDate, string endDate, string leaveStartTime, string leaveEndTime, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMCollisionCheck(startDate, endDate, leaveStartTime, leaveEndTime, ApiKey, EmpCode);
        }
        public ResponseModel<TMChaWorkSch> GetDHRTMChaWorkSchByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMChaWorkSchByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMOTTypeInf> GetDHRTMOTTypeInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMOTTypeInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<TMCalculateLeaveDataReceive> GetDHRTMCalculateLeaveData(string startDate, string endDate, string EmpCode, TMCalculateLeaveDataInf oData)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMCalculateLeaveData(startDate, endDate, ApiKey, EmpCode, oData);
        }

        public ResponseModel<TMTimeAttendCalculateDataReceive> GetDHRTMCalculateAttendanceData(string startDate, string endDate, string EmpCode, TMTimeAttendCalculateDataInf oData)
        {
            _empCode = EmpCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRTMCalculateAttendanceData(startDate, endDate, ApiKey, EmpCode, oData);
        }

        #endregion GET BY EMPCODE

        #region POST DATA

        public PostResult PostDHRTMLeaveInf(TMLeaveInf leaveInf)
        {
            _empCode = leaveInf.empCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRTMLeaveInf(leaveInf, ApiKey);
        }

        public PostResult PostDHRTMTimeAttend(TMTimeAttend timeAttend)
        {
            _empCode = timeAttend.empCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRTMTimeAttend(timeAttend, ApiKey);
        }

        public PostResult PostDHRTMChaWorkSch(TMChaWorkSch chaWorkSch)
        {
            _empCode = chaWorkSch.empCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRTMChaWorkSch(chaWorkSch, ApiKey);
        }

        public PostResult PostDHRTMOTTypeInf(TMOTTypeInf otTypeInf)
        {
            _empCode = otTypeInf.empCode;
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRTMOTTypeInf(otTypeInf, ApiKey);
        }

        public PostResult PostDHRTMTimeRecInOut(TMTimeRecInOutPost timeRecInOut)
        {
            return TimeServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRTMTimeRecInOut(timeRecInOut, ApiKey);
        }
        #endregion POST DATA
    }
}
