﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Interfaces
{
    public interface ITimeService
    {
        #region GET ALL

        ResponseModel<TMCalenHoliday> GetDHRTMCalenHoliday(string startDate, string endDate, string ApiKey);
        ResponseModel<TMSetupHoliday> GetDHRTMSetupHoliday(string startDate, string endDate, string ApiKey);
        ResponseModel<TMPlanWorkTime> GetDHRTMPlanWorkTime(string startDate, string endDate, string ApiKey);
        ResponseModel<TMTimeRecInf> GetDHRTMTimeRecInf(string startDate, string endDate, string ApiKey);
        ResponseModel<TMTimeRecType> GetDHRTMTimeRecType(string startDate, string endDate, string ApiKey);
        ResponseModel<TMLeaveInf> GetDHRTMLeaveInf(string startDate, string endDate, string ApiKey);
        ResponseModel<TMTimeAttend> GetDHRTMTimeAttend(string startDate, string endDate, string ApiKey);
        ResponseModel<TMLeaveQuota> GetDHRTMLeaveQuota(string startDate, string endDate, string ApiKey);
        ResponseModel<TMTimeRecInOut> GetDHRTMTimeRecInOut(string startDate, string endDate, string ApiKey);
        ResponseModel<TMChaWorkSch> GetDHRTMChaWorkSch(string startDate, string endDate, string ApiKey);
        ResponseModel<TMOTTypeInf> GetDHRTMOTTypeInf(string startDate, string endDate, string ApiKey);
        ResponseModel<TMLeaveType> GetDHRTMLeaveType(string startDate, string endDate, string ApiKey);
        ResponseModel<TMWorkingType> GetDHRTMWorkingType(string startDate, string endDate, string ApiKey);
        ResponseModel<TMLeaveQuotaType> GetDHRTMLeaveQuotaType(string startDate, string endDate, string ApiKey);
        ResponseModel<TMEmpGroup> GetDHRTMEmpGroup(string startDate, string endDate, string ApiKey);
        ResponseModel<TMPersonalArea> GetDHRTMPersonalArea(string startDate, string endDate, string ApiKey);
        ResponseModel<TMBreakSchedule> GetDHRTMBreakSchedule(string startDate, string endDate, string ApiKey);
        ResponseModel<TMDailyWorkSchedule> GetDHRTMDailyWorkSchedule(string startDate, string endDate, string ApiKey);
        ResponseModel<TMPeriodWorkSchedule> GetDHRTMPeriodWorkSchedule(string startDate, string endDate, string ApiKey);
        ResponseModel<TMWorkSchedule> GetDHRTMWorkSchedule(string startDate, string endDate, string ApiKey);
        ResponseModel<TMWorkScheduleRule> GetDHRTMWorkScheduleRule(string startDate, string endDate, string ApiKey);
        ResponseModel<TMOTType> GetDHRTMOTType(string startDate, string endDate, string ApiKey);
        ResponseModel<TMSubsititutionType> GetDHRTMSubsititutionType(string startDate, string endDate, string ApiKey);

        #endregion GET ALL

        #region GET BY EMPCODE

        
        ResponseModel<TMTimeRecInf> GetDHRTMTimeRecInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMLeaveInf> GetDHRTMLeaveInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMTimeAttend> GetDHRTMTimeAttendByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMLeaveQuota> GetDHRTMLeaveQuotaByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMTimeRecInOut> GetDHRTMTimeRecInOutByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMChaWorkSch> GetDHRTMChaWorkSchByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMOTTypeInf> GetDHRTMOTTypeInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<TMCalculateLeaveDataReceive> GetDHRTMCalculateLeaveData(string startDate, string endDate, string ApiKey, string EmpCode, TMCalculateLeaveDataInf oData);
        ResponseModel<TMTimeAttendCalculateDataReceive> GetDHRTMCalculateAttendanceData(string startDate, string endDate, string ApiKey, string EmpCode, TMTimeAttendCalculateDataInf oData);
        ResponseModel<TMCollisionCheck> GetDHRTMCollisionCheck(string startDate, string endDate, string leaveStartTime, string leaveEndTime, string ApiKey,string EmpCode);
        #endregion GET BY EMPCODE

        #region POST DATA


        PostResult PostDHRTMLeaveInf(TMLeaveInf leaveInf, string ApiKey);
        PostResult PostDHRTMTimeAttend(TMTimeAttend timeAttend, string ApiKey);
        PostResult PostDHRTMChaWorkSch(TMChaWorkSch chaWorkSch, string ApiKey);
        PostResult PostDHRTMOTTypeInf(TMOTTypeInf otTypeInf, string ApiKey);
        PostResult PostDHRTMTimeRecInOut(TMTimeRecInOutPost timeRecInOut, string ApiKey);
       
        #endregion POST DATA
    }
}









