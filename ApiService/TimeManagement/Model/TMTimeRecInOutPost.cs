﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMTimeRecInOutPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string recTypeCode { get; set; }
        public string recTime { get; set; }
        public string recFlag { get; set; }
        public string recSourceCode { get; set; }
        public string recSourceValue { get; set; }
        public string actionType { get; set; }
    }
}
