﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMCalenHoliday
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string empGrpGrouping { get; set; }
        public string psnAreaGrouping { get; set; }
        public string holiCalenCode { get; set; }
        public string holiCalenName { get; set; }
        public string year { get; set; }
        public string holidayCode { get; set; }
    }
}
