﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMLeaveInf
    {
        public string tmLeaveID { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string leaveTypeCode { get; set; }
        public string leaveStartTime { get; set; }
        public string leaveEndTime { get; set; }
        public double leaveHour { get; set; }
        public double leaveDay { get; set; }
        public double leaveDayCalen { get; set; }
        public double useQuota { get; set; }
        public string actionType { get; set; }
    }
}