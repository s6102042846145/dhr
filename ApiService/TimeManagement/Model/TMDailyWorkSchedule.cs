﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMDailyWorkSchedule
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string dayWorkSchCode { get; set; }
        public string dayWorkSchName { get; set; }
        public double planWorkHour { get; set; }
        public string startPlanTime { get; set; }
        public string endPlanTime { get; set; }
        public string breakSchCode { get; set; }
        public string toleranceStartTime { get; set; }
        public string toleranceEndTime { get; set; }
    }
}
