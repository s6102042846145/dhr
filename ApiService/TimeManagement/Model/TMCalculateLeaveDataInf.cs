﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMCalculateLeaveDataInf
    {

        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string leaveTypeCode { get; set; }
        public string checkQuota { get; set; }
        public string leaveStartTime { get; set; }
        public string leaveEndTime { get; set; }
        public string skipDayoff { get; set; }

    }
}
