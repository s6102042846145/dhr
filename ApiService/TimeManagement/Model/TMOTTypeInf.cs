﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMOTTypeInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string otTypeCode { get; set; }
        public string otStartTime { get; set; }
        public string otEndTime { get; set; }
        public double otHour { get; set; }
        public string bridgeDay { get; set; }
        public string actionType { get; set; }
    }
}
   