﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMOTType
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string otTypeCode { get; set; }
        public string otTypeName { get; set; }
        public double amount { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
        public double quantity { get; set; }
        public string unitCode { get; set; }
        public string unitValue { get; set; }
    }
}
