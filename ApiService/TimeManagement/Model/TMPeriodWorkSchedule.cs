﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMPeriodWorkSchedule
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string periodWorkSchCode { get; set; }
        public string periodWorkSchName { get; set; }
        public int week { get; set; }
        public string dayWorkSchCode1 { get; set; }
        public string dayWorkSchCode2 { get; set; }
        public string dayWorkSchCode3 { get; set; }
        public string dayWorkSchCode4 { get; set; }
        public string dayWorkSchCode5 { get; set; }
        public string dayWorkSchCode6 { get; set; }
        public string dayWorkSchCode7 { get; set; }
    }
}
