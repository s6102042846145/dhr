﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMEmpGroup
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string empGrp { get; set; }
        public string empSubGrp { get; set; }
        public string empGrpGrouping { get; set; }
    }
}
