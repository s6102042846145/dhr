﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAPositionHis
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string posStartDate { get; set; }
        public string actionCode { get; set; }
        public string actionValue { get; set; }
        public string actionTextTH { get; set; }
        public string actionTextEN { get; set; }
        public string reasonCode { get; set; }
        public string reasonValue { get; set; }
        public string reasonTextTH { get; set; }
        public string reasonTextEN { get; set; }
        public string commandNo { get; set; }
        public string cmdTypeCode { get; set; }
        public string cmdTypeValue { get; set; }
        public string cmdTypeTextTH { get; set; }
        public string cmdTypeTextEN { get; set; }
        public string cmdName { get; set; }
        public string cmdNameEn { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }
        public string id { get; set; }


    }
}

