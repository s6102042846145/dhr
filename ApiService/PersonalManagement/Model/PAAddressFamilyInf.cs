﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAAddressFamilyInf : PAAddressFamilyInfPost
    {
       
        public string relateCodeTextTh { get; set; }
        public string relateCodeTextEn { get; set; }
        public string addressTypeTextTH { get; set; }
        public string addressTypeTextEN { get; set; } 
        public string subdistrictTextTH { get; set; }
        public string subdistrictTextEN { get; set; }
        public string districtTextTH { get; set; }
        public string districtTextEN { get; set; }
        public string provinceTextTH { get; set; }
        public string provinceTextEN { get; set; }
        public string countryTextTH { get; set; }
        public string countryTextEN { get; set; }
    }
}
