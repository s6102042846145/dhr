﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAMedicalCheckup
    {
        public string paMeId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string sequence { get; set; }
        public string checkupCode { get; set; }
        public string checkupValue { get; set; }
        public string checkupTextTH { get; set; }
        public string checkupTextEN { get; set; }
        public string normCheckResult { get; set; }
        public string abnormCheckResult { get; set; }
        public string doctorComment { get; set; }
        public string standard { get; set; }
        public string actionType { get; set; }
    }
}
