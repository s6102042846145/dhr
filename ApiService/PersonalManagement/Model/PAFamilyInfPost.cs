﻿namespace DHR.HR.API.Model
{
    public class PAFamilyInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int familyId { get; set; }
        public string relateCode { get; set; }
        public string relateValue { get; set; }
        public string inameCode { get; set; }
        public string inameValue { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string birthDate { get; set; }
        public string sexCode { get; set; }
        public string sexValue { get; set; }
        public string provinceOfBirth { get; set; }
        public string countryBirthCode { get; set; }
        public string countryBirthValue { get; set; }
        public string nationalityCode { get; set; }
        public string nationalityValue { get; set; }
        public string passAwayF { get; set; }
        public string passAwayDate { get; set; }
        public string idCard { get; set; }
        public string maritalDate { get; set; }
        public string divorceDate { get; set; }
        public string telNumber { get; set; }
        public string famEmpCode { get; set; }
        public string idCardFatherSpouse { get; set; }
        public string idCardMotherSpouse { get; set; }
        public string actionType { get; set; }

        public PAFamilyInfPost()
        {
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            familyId = default(int);
            relateCode = string.Empty;
            relateValue = string.Empty;
            inameCode = string.Empty;
            inameValue = string.Empty;
            fname = string.Empty;
            lname = string.Empty;
            birthDate = string.Empty;
            sexCode = string.Empty;
            sexValue = string.Empty;
            provinceOfBirth = string.Empty;
            countryBirthCode = string.Empty;
            countryBirthValue = string.Empty;
            nationalityCode = string.Empty;
            nationalityValue = string.Empty;
            passAwayF = string.Empty;
            passAwayDate = string.Empty;
            idCard = string.Empty;
            maritalDate = string.Empty;
            divorceDate = string.Empty;
            telNumber = string.Empty;
            famEmpCode = string.Empty;
            idCardFatherSpouse = string.Empty;
            idCardMotherSpouse = string.Empty;
            actionType = string.Empty;
        }
    }
}
