﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.HR.API.Model
{
    public class PAWorkCalendar
    {
        public string companyCode { get; set; }
        public string calendarCode { get; set; }
        public string empCode { get; set; }
        public string psnAreaCode { get; set; }
        public string psnAreaValue { get; set; }
        public string psnAreaText { get; set; }
        public string psnlSubAreaCode { get; set; }
        public string psnlSubAreaValue { get; set; }
        public string psnlSubAreaText { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }

}

     