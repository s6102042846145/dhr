﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PASecondmentInf
    {
        public string paSeId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string scmTypeCode { get; set; }
        public string scmTypeValue { get; set; }
        public string scmTypeTextTH { get; set; }
        public string scmTypeTextEN { get; set; }
        public string scmOutCode { get; set; }
        public string scmOutTextTH { get; set; }
        public string scmOutTextEN { get; set; }
        public string levelCode { get; set; }
        public string levelValue { get; set; }
        public string levelTextTH { get; set; }
        public string levelTextEN { get; set; }
        public string knowHow { get; set; }
        public string posName { get; set; }
        public string posNameEn { get; set; }
        public string unitName { get; set; }
        public string unitNameEn { get; set; }
        public string actionType { get; set; }
    }
}
