﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAPropationUpdateInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string updateResult { get; set; }
        public int updateTime { get; set; }
        public string actionType { get; set; }

        public PAPropationUpdateInf()
        {
            companyCode= string.Empty;
            empCode= string.Empty;
            startDate= string.Empty;
            endDate= string.Empty;
            updateResult= string.Empty;
            updateTime = default(int);
            actionType = string.Empty;
        }
    }
}