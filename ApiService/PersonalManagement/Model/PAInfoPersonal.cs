﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAInfoPersonal
    {
        public string employeeID { get; set; }
        public string subType { get; set; }
        public string beginDate { get; set; }
        public string endDate { get; set; }
        public string companyCode { get; set; }
        public string name { get; set; }
        public string area { get; set; }
        public string areaTextTH { get; set; }
        public string areaTextEN { get; set; }
        public string subArea { get; set; }
        public string subAreaTextTH { get; set; }
        public string subAreaTextEN { get; set; }
        public string empGroup { get; set; }
        public string empGroupTextTH { get; set; }
        public string empGroupTextEN { get; set; }
        public string empSubGroup { get; set; }
        public string orgUnit { get; set; }
        public string orgUnitTextTH { get; set; }
        public string orgUnitTextEN { get; set; }
        public string position { get; set; }
        public string positionTextTH { get; set; }
        public string positionTextEN { get; set; }
        public string adminGroup { get; set; }
        public string costCenter { get; set; }
        public string businessArea { get; set; }
        public string payrollArea { get; set; }
        public string secondaryCompanyCode { get; set; }
    }
}
