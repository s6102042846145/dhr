﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PADiseaseInf : PADiseaseInfPost
    {
        public string diseaseTypeCode { get; set; }
        public string diseaseTypeValue { get; set; }
        public string diseaseTypeTextTH { get; set; }
        public string diseaseTypeTextEN { get; set; }
        public string diseaseCode { get; set; }
        public string diseaseValue { get; set; }
        public string diseaseTextTH { get; set; }
        public string diseaseTextEN { get; set; }
    }
}