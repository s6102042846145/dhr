﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PADocumentInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string documentCode { get; set; }
        public string documentValue { get; set; }
        public string documentId { get; set; }
        public string issuingAuthority { get; set; }
        public string placeOfIssue { get; set; }
        public string countryOfIssueCode { get; set; }
        public string countryOfIssueValue { get; set; }
        public string dateOfIssue { get; set; }
        public string effectiveDate { get; set; }
        public string expireDate { get; set; }
        public string validityCode { get; set; }
        public string validityValue { get; set; }
        public string remark { get; set; }
        public string actionType { get; set; }


        public PADocumentInfPost()
        {
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            documentCode = string.Empty;
            documentValue = string.Empty;
            documentId = string.Empty;
            issuingAuthority = string.Empty;
            placeOfIssue = string.Empty;
            countryOfIssueCode = string.Empty;
            countryOfIssueValue = string.Empty;
            dateOfIssue = string.Empty;
            effectiveDate = string.Empty;
            expireDate = string.Empty;
            validityCode = string.Empty;
            validityValue = string.Empty;
            actionType = string.Empty;
        }
    }
}
