﻿namespace DHR.HR.API.Model
{
    public class PAPersonalInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string inameCode { get; set; }
        public string inameValue { get; set; }
        public string iname1Code { get; set; }
        public string iname1Value { get; set; }
        public string iname2Code { get; set; }
        public string iname2Value { get; set; }
        public string iname3Code { get; set; }
        public string iname3Value { get; set; }
        public string iname4Code { get; set; }
        public string iname4Value { get; set; }
        public string iname5Code { get; set; }
        public string iname5Value { get; set; }
        public string fname { get; set; }
        public string mname { get; set; }
        public string lname { get; set; }
        public string fullName { get; set; }
        public string inameEnCode { get; set; }
        public string inameEnValue { get; set; }
        public string fnameEn { get; set; }
        public string mnameEn { get; set; }
        public string lnameEn { get; set; }
        public string nickName { get; set; }
        public string countryBirthCode { get; set; }
        public string countryBirthValue { get; set; }
        public string provinceBirthCode { get; set; }
        public string raceCode { get; set; }
        public string raceValue { get; set; }
        public string nationalityCode { get; set; }
        public string nationalityValue { get; set; }
        public string religionCode { get; set; }
        public string religionValue { get; set; }
        public string maritalStatusCode { get; set; }
        public string maritalStatusValue { get; set; }
        public string sexCode { get; set; }
        public string sexValue { get; set; }
        public string actionType { get; set; }
        public string Id { get; set; }

        public PAPersonalInfPost()
        {
            Id = string.Empty;
            companyCode = string.Empty;
            empCode = string.Empty;
            inameCode = string.Empty;
            inameValue = string.Empty;
            iname1Code = string.Empty;
            iname1Value = string.Empty;
            iname2Code = string.Empty;
            iname2Value = string.Empty;
            iname3Code = string.Empty;
            iname3Value = string.Empty;
            iname4Code = string.Empty;
            iname4Value = string.Empty;
            iname5Code = string.Empty;
            iname5Value = string.Empty;
            fname = string.Empty;
            mname = string.Empty;
            lname = string.Empty;
            fullName = string.Empty;
            inameEnCode = string.Empty;
            inameEnValue = string.Empty;
            fnameEn = string.Empty;
            mnameEn = string.Empty;
            lnameEn = string.Empty;
            nickName = string.Empty;
            countryBirthCode = string.Empty;
            countryBirthValue = string.Empty;
            provinceBirthCode = string.Empty;
            raceCode = string.Empty;
            raceValue = string.Empty;
            nationalityCode = string.Empty;
            nationalityValue = string.Empty;
            religionCode = string.Empty;
            religionValue = string.Empty;
            maritalStatusCode = string.Empty;
            maritalStatusValue = string.Empty;
            sexCode = string.Empty;
            sexValue = string.Empty;
            actionType = string.Empty;
        }

    }
}
