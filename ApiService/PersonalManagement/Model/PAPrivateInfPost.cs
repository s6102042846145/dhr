﻿namespace DHR.HR.API.Model
{
    public class PAPrivateInfPost
    {
        public string Id { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string birthDate { get; set; }
        public string idCard { get; set; }
        public string bloodGrpCode { get; set; }
        public string bloodGrpValue { get; set; }
        public string taxId { get; set; }
        public string actionType { get; set; }

        public PAPrivateInfPost()
        {
            Id = string.Empty;
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            birthDate = string.Empty;
            idCard = string.Empty;
            bloodGrpCode = string.Empty;
            bloodGrpValue = string.Empty;
            taxId = string.Empty;
            actionType = string.Empty;
        }
    }
}
