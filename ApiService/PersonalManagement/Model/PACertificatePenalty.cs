﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PACertificatePenalty 
    {
        public string paPuId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string statusCode { get; set; }
        public string statusValue { get; set; }
        public string startCode { get; set; }
        public string startValue { get; set; }
        public string evidenceCode { get; set; }
        public string evidenceValue { get; set; }
        public string punishmentCode { get; set; }
        public string punishmentValue { get; set; }
        public string detail { get; set; }
        public string commandNo { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
