﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAWorkingHisInf
    {
        public string paWoId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string employerName { get; set; }
        public string position { get; set; }
        public string workCountryCode { get; set; }
        public string workCountryValue { get; set; }
        public string workCountryTextTH { get; set; }
        public string workCountryTextEN { get; set; }
        public string buCode { get; set; }
        public string buValue { get; set; }
        public string buTextTH { get; set; }
        public string buTextEN { get; set; }
        public string jobGrpCode { get; set; }
        public string jobGrpValue { get; set; }
        public string jobGrpTextTH { get; set; }
        public string jobGrpTextEN { get; set; }
        public object actionType { get; set; }
        public object serviceType { get; set; }
        public string createDate { get; set; }
        public string createBy { get; set; }
        public string updateDate { get; set; }
        public string updateBy { get; set; }
    }
}
 