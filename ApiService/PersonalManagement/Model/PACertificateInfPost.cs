﻿namespace DHR.HR.API.Model
{
    public class PACertificateInfPost
    {
        public string paCeId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string certificateCode { get; set; }
        public string certificateValue { get; set; }
        public string cerStartDate { get; set; }
        public string cerEndDate { get; set; }
        public int certificateTime { get; set; }
        public int score { get; set; }
        public string actionType { get; set; }

        public PACertificateInfPost()
        {
            paCeId = string.Empty;
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            certificateCode = string.Empty;
            certificateValue = string.Empty;
            cerStartDate = string.Empty;
            cerEndDate = string.Empty;
            certificateTime = default(int);
            score = default(int);
            actionType = string.Empty;
        }
    }
}
