﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PATalentInf : PATalentInfPost
    {
        public string talentTextTH { get; set; }
        public string talentTextEN { get; set; }
    }
}
