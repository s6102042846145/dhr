﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAContactInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string contactCode { get; set; }
        public string contactValue { get; set; }
        public string contactData { get; set; }
        public string actionType { get; set; }

        public PAContactInfPost()
        {
            companyCode= string.Empty;
            empCode= string.Empty;
            startDate= string.Empty;
            endDate= string.Empty;
            contactCode= string.Empty;
            contactValue= string.Empty;
            contactData= string.Empty;
            actionType = string.Empty;
        }
    }
}
