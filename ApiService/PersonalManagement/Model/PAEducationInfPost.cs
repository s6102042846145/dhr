﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAEducationInfPost
    {
        public string paEduID { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string eduLevelCode { get; set; }
        public string eduLevelValue { get; set; }
        public string instituteCode { get; set; }
        public string instituteValue { get; set; }
        public string countryCode { get; set; }
        public string countryValue { get; set; }
        public string certificateCode { get; set; }
        public string certificateValue { get; set; }
        public string grade { get; set; }
        public string majorCode { get; set; }
        public string majorValue { get; set; }
        public string minorCode { get; set; }
        public string minorValue { get; set; }
        public string specialCode { get; set; }
        public string specialValue { get; set; }
        public string eduGroupCode { get; set; }
        public string eduGroupValue { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }
        public  PAEducationInfPost()
        {
            paEduID = string.Empty;
            companyCode  = string.Empty;
            empCode  = string.Empty;
            startDate  = string.Empty;
            endDate  = string.Empty;
            eduLevelCode  = string.Empty;
            eduLevelValue  = string.Empty;
            instituteCode  = string.Empty;
            instituteValue  = string.Empty;
            countryCode  = string.Empty;
            countryValue  = string.Empty;
            certificateCode  = string.Empty;
            certificateValue  = string.Empty;
            grade  = string.Empty;
            majorCode  = string.Empty;
            majorValue  = string.Empty;
            minorCode  = string.Empty;
            minorValue  = string.Empty;
            specialCode  = string.Empty;
            specialValue  = string.Empty;
            eduGroupCode  = string.Empty;
            eduGroupValue  = string.Empty;
            actionType = string.Empty;
            serviceType = string.Empty;
        }
    }
}