﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAClothingSize
    {
        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ClothingSizeCode { get; set; }
        public string ClothingSizeValue { get; set; }
        public string ClothingSizeTextTH { get; set; }
        public string ClothingSizeTextEN { get; set; }
        public string ChestCode { get; set; }
        public string ChestValue { get; set; }
        public string ChestTextTH { get; set; }
        public string ChestTextEN { get; set; }
        public string UnitCode { get; set; }
        public string UnitValue { get; set; }
        public string UnitTextTH { get; set; }
        public string UnitTextEN { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Remark { get; set; }
        public string ActionType { get; set; }
        public string ServiceType { get; set; }
        public string Id { get; set; }
        public string CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateBy { get; set; }


        public PAClothingSize()
        {
            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            StartDate = string.Empty;
            EndDate = string.Empty;
            ClothingSizeCode = string.Empty;
            ClothingSizeValue = string.Empty;
            ClothingSizeTextTH = string.Empty;
            ClothingSizeTextEN = string.Empty;
            ChestCode = string.Empty;
            ChestValue = string.Empty;
            ChestTextTH = string.Empty;
            ChestTextEN = string.Empty;
            UnitCode = string.Empty;
            UnitValue = string.Empty;
            UnitTextTH = string.Empty;
            UnitTextEN = string.Empty;
            Weight = string.Empty;
            Height = string.Empty;
            Remark = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
            Id = string.Empty;


        }
    }
}
