﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAPosition
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string posCode { get; set; }
        public string posTextTH { get; set; }
        public string posTextEN { get; set; }
        public string actionType { get; set; }
    }
}
 