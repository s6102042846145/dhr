﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYWageType
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string wageTypeCode { get; set; }
        public string wageTypeNameEn { get; set; }
        public string wageTypeNameTh { get; set; }
        public string otBaseCode { get; set; }
        public string prorateCode { get; set; }
        public string calculateType { get; set; }
        public string totalGross { get; set; }
        public string netDeduction { get; set; }
        public string calRegTax { get; set; }
        public string calNonregTax { get; set; }
        public string calOnetimeTax { get; set; }
        public string calAlltimeTax { get; set; }
        public string reportTaxCode { get; set; }
        public string reportTaxValue { get; set; }
        public string calSsoBase { get; set; }
        public string calPfBase { get; set; }
        public string calAmount { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
        public string calQuantity { get; set; }
        public string unitCode { get; set; }
        public string unitValue { get; set; }
        public string wageTypeRef { get; set; }
        public double wageTypeRate { get; set; }
        public double wageTypeMin { get; set; }
        public double wageTypeMax { get; set; }
    }
}
