﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPayslipGroup
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string wageTypeGroup { get; set; }
        public int wageTypeGroupOrder { get; set; }
        public string wageTypeGroupNameTh { get; set; }
        public string wageTypeGroupNameEn { get; set; }
    }
}
