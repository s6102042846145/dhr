﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYAccountPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string accountNo { get; set; }
        public string bankCode { get; set; }
        public string bankValue { get; set; }
        public string accountTypeCode { get; set; }
        public string accountTypeValue { get; set; }
        public string accountName { get; set; }
        public string provinceCode { get; set; }
        public string postcode { get; set; }
        public string countryCode { get; set; }
        public string countryValue { get; set; }
        public string paymentTypeCode { get; set; }
        public string paymentTypeValue { get; set; }
        public string paymentPercenCode { get; set; }
        public string paymentPercenValue { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
        public string actionType { get; set; }

        public PYAccountPost()
        {
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            accountNo = string.Empty;
            bankCode = string.Empty;
            bankValue = string.Empty;
            accountTypeCode = string.Empty;
            accountTypeValue = string.Empty;
            accountName = string.Empty;
            provinceCode = string.Empty;
            postcode = string.Empty;
            countryCode = string.Empty;
            paymentTypeCode = string.Empty;
            paymentTypeValue = string.Empty;
            paymentPercenCode = string.Empty;
            paymentPercenValue = string.Empty;
            currencyCode = string.Empty;
            currencyValue = string.Empty;
            actionType = string.Empty;
        }
    }
}
