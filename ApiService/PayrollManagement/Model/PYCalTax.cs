﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYCalTax
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string taxLevel { get; set; }
        public double minIncome { get; set; }
        public double maxIncome { get; set; }
        public double rate { get; set; }
    }
}
