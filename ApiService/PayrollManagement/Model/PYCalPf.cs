﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYCalPf
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string pfBaseCode { get; set; }
        public string refCountWork { get; set; }
        public double pfCumulateRate { get; set; }
        public double pfContributeRate { get; set; }
        public string optionFund { get; set; }
    }
}
