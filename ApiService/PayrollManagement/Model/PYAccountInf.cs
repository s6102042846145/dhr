﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYAccountInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string accountNo { get; set; }
        public string bankCode { get; set; }
        public string bankValue { get; set; }
        public string bankTextTh { get; set; }
        public string bankTextEn { get; set; }
        public string accountTypeCode { get; set; }
        public string accountTypeValue { get; set; }
        public string accountTypeTextTh { get; set; }
        public string accountTypeTextEn { get; set; }
        public string accountName { get; set; }
        public string provinceCode { get; set; }
        public string provinceTextTh { get; set; }
        public string provinceTextEn { get; set; }
        public string postcode { get; set; }
        public string countryCode { get; set; }
        public string countryValue { get; set; }
        public string countryTextTh { get; set; }
        public string countryTextEn { get; set; }
        public string paymentTypeCode { get; set; }
        public string paymentTypeValue { get; set; }
        public string paymentTypeTextTh { get; set; }
        public string paymentTypeTextEn { get; set; }
        public string paymentPercenCode { get; set; }
        public string paymentPercenValue { get; set; }
        public string paymentPercenTextTh { get; set; }
        public string paymentPercenTextEn { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
        public string currencyTextTh { get; set; }
        public string currencyTextEn { get; set; }
        public string actionType { get; set; }
    }
}
