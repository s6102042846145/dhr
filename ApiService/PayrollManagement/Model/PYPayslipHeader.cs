﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPayslipHeader
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int period { get; set; }
        public string year { get; set; }
        public string periodTypeCode { get; set; }
        public string periodTypeValue { get; set; }
        public string payDate { get; set; }
        public string accountNo { get; set; }
    }
}
