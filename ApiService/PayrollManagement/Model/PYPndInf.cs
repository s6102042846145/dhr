﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPndInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string taxYear { get; set; }
        public decimal incomeSection401 { get; set; }
        public decimal taxSection401 { get; set; }
        public decimal incomeSection402 { get; set; }
        public decimal taxSection402 { get; set; }
        public decimal otherIncome { get; set; }
        public decimal taxOther { get; set; }
        public decimal gbf { get; set; }
        public decimal pfAmount { get; set; }
        public decimal ssoAmount { get; set; }
        public string payerCode { get; set; }
        public string payerValue { get; set; }
    }
}
