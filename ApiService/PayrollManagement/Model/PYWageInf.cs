﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYWageInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string wageTypeCode01 { get; set; }
        public string majorSalary { get; set; }
        public string minorSalary { get; set; }
        public string majorSalaryBase { get; set; }
        public string levelWage { get; set; }
        public double? amount01 { get; set; }
        public string currencyCode01 { get; set; }
        public string currencyValue01 { get; set; }
        public int? quantity01 { get; set; }
        public string unitCode01 { get; set; }
        public string unitValue01 { get; set; }
        public string wageTypeCode02 { get; set; }
        public double? amount02 { get; set; }
        public string currencyCode02 { get; set; }
        public string currencyValue02 { get; set; }
        public int? quantity02 { get; set; }
        public string unitCode02 { get; set; }
        public string unitValue02 { get; set; }
        public string wageTypeCode03 { get; set; }
        public double? amount03 { get; set; }
        public string currencyCode03 { get; set; }
        public string currencyValue03 { get; set; }
        public int? quantity03 { get; set; }
        public string unitCode03 { get; set; }
        public string unitValue03 { get; set; }
        public string wageTypeCode04 { get; set; }
        public double? amount04 { get; set; }
        public string currencyCode04 { get; set; }
        public string currencyValue04 { get; set; }
        public int? quantity04 { get; set; }
        public string unitCode04 { get; set; }
        public string unitValue04 { get; set; }
        public string wageTypeCode05 { get; set; }
        public double? amount05 { get; set; }
        public string currencyCode05 { get; set; }
        public string currencyValue05 { get; set; }
        public int? quantity05 { get; set; }
        public string unitCode05 { get; set; }
        public string unitValue05 { get; set; }
    }
}
 