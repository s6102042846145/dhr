﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYTaxInf
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string taxIdentifiNo { get; set; }
        public string personAreaGrop { get; set; }
        public string name { get; set; }
        public string branchNo { get; set; }
        public string building { get; set; }
        public string roomNo { get; set; }
        public string floor { get; set; }
        public string village { get; set; }
        public string addressNo { get; set; }
        public int? moo { get; set; }
        public string soi { get; set; }
        public string separate { get; set; }
        public string street { get; set; }
        public string subdistrictCode { get; set; }
        public string districtCode { get; set; }
        public string provinceCode { get; set; }
        public string postcode { get; set; }
    }
}
