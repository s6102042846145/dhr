﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPersonalArea
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string personArea { get; set; }
        public string personSubarea { get; set; }
        public string personAreaGrop { get; set; }
    }
}
