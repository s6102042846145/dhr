﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYNonRegIncDed
    {
        public string pyNoId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string wageTypeCode { get; set; }
        public double amount { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
        public int quantity { get; set; }
        public string unitCode { get; set; }
        public string unitValue { get; set; }
        public string remark { get; set; }
        public string actionType { get; set; }
        public int? seqNo { get; set; }
    }
}

