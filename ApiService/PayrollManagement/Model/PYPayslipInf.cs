﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPayslipInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int period { get; set; }
        public string year { get; set; }
        public string periodTypeCode { get; set; }
        public string periodTypeValue { get; set; }
        public string wageTypeCode { get; set; }
        public double amount { get; set; }
        public int quantity { get; set; }
        public string unitCode { get; set; }
        public string unitValue { get; set; }
        public string periodTypeTextTh { get; set; }
        public string periodTypeTextEn { get; set; }
        public string wageTypeNameTh { get; set; }
        public string wageTypeNameEn { get; set; }
    }
}
