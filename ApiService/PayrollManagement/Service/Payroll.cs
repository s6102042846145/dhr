﻿using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class Payroll : IPayroll
    {
        #region Constructor
        public Payroll(string oCompanyCode , string oBaseUrl, string oGateway)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateway;
        }
        #endregion Constructor

        #region Member
        private string ModuleID = "DHR.HR";
        private string  CompanyCode { get; set; }
        private string  BaseUrl { get; set; }
        private string Gateway { get; set; }
        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }

        }

        private string Scheme
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SCHEME");
            }
        }

        private string HeaderContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "HEADERCONTENTTYPE");
            }
        }

        private int MaxReconnectLimit
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXRECONNECTIONATTEMPTLIMIT"));
            }
        }

        #endregion Member

        public ResponseModel<PYPayslipInf> GetDHRPYPayslipInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPayslipInf> responseModel = new ResponseModel<PYPayslipInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).PayrollRoute_WsSentPYPayslipInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayslipInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPayslipInf> GetDHRPYPayslipInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYPayslipInf> responseModel = new ResponseModel<PYPayslipInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).PayrollRoute_WsSentPYPayslipInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayslipInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYWageInf> GetDHRPYWageInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYWageInf> responseModel = new ResponseModel<PYWageInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).PayrollRoute_WsSentPYWageInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYWageInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYWageInf> GetDHRPYWageInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYWageInf> responseModel = new ResponseModel<PYWageInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).PayrollRoute_WsSentPYWageInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYWageInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYRegIncDed> GetDHRPYRegIncDed(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYRegIncDed> responseModel = new ResponseModel<PYRegIncDed>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).PayrollRoute_WsSentPYRegIncDed;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYRegIncDed>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYRegIncDed> GetDHRPYRegIncDedByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYRegIncDed> responseModel = new ResponseModel<PYRegIncDed>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).PayrollRoute_WsSentPYRegIncDed;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYRegIncDed>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYNonRegIncDed> GetDHRPYNonRegIncDed(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYNonRegIncDed> responseModel = new ResponseModel<PYNonRegIncDed>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYNonRegIncDed;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYNonRegIncDed>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYNonRegIncDed> GetDHRPYNonRegIncDedByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYNonRegIncDed> responseModel = new ResponseModel<PYNonRegIncDed>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYNonRegIncDed;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYNonRegIncDed>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYSpecialIncDed> GetDHRPYSpecialIncDed(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYSpecialIncDed> responseModel = new ResponseModel<PYSpecialIncDed>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYSpecialIncDed;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYSpecialIncDed>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYSpecialIncDed> GetDHRPYSpecialIncDedByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYSpecialIncDed> responseModel = new ResponseModel<PYSpecialIncDed>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYSpecialIncDed;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYSpecialIncDed>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYSsoInf> GetDHRPYSsoInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYSsoInf> responseModel = new ResponseModel<PYSsoInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYSsoInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYSsoInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYSsoInf> GetDHRPYSsoInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYSsoInf> responseModel = new ResponseModel<PYSsoInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYSsoInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYSsoInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYAccountInf> GetDHRPYAccountInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYAccountInf> responseModel = new ResponseModel<PYAccountInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYAccountInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYAccountInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYAccountInf> GetDHRPYAccountInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYAccountInf> responseModel = new ResponseModel<PYAccountInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYAccountInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYAccountInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPndInf> GetDHRPYPndInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPndInf> responseModel = new ResponseModel<PYPndInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPndInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPndInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPndInf> GetDHRPYPndInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYPndInf> responseModel = new ResponseModel<PYPndInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPndInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPndInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYTaxDedInf> GetDHRPYTaxDedInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYTaxDedInf> responseModel = new ResponseModel<PYTaxDedInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYTaxDedInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYTaxDedInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYTaxDedInf> GetDHRPYTaxDedInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYTaxDedInf> responseModel = new ResponseModel<PYTaxDedInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYTaxDedInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYTaxDedInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPersonalArea> GetDHRPYPersonalArea(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPersonalArea> responseModel = new ResponseModel<PYPersonalArea>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPersonalArea;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPersonalArea>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYTaxInf> GetDHRPYTaxInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYTaxInf> responseModel = new ResponseModel<PYTaxInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYTaxInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYTaxInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYWageType> GetDHRPYWageType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYWageType> responseModel = new ResponseModel<PYWageType>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYWageType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYWageType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYCalOt> GetDHRPYCalOt(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYCalOt> responseModel = new ResponseModel<PYCalOt>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYCalOt;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYCalOt>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYCalProrate> GetDHRPYCalProrate(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYCalProrate> responseModel = new ResponseModel<PYCalProrate>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYCalProrate;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYCalProrate>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYSsoRate> GetDHRPYSsoRate(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYSsoRate> responseModel = new ResponseModel<PYSsoRate>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYSsoRate;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYSsoRate>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYCalTax> GetDHRPYCalTax(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYCalTax> responseModel = new ResponseModel<PYCalTax>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYCalTax;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYCalTax>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYCalPf> GetDHRPYCalPf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYCalPf> responseModel = new ResponseModel<PYCalPf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYCalPf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYCalPf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPfInf> GetDHRPYPfInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPfInf> responseModel = new ResponseModel<PYPfInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPfInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPfInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPfInf> GetDHRPYPfInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYPfInf> responseModel = new ResponseModel<PYPfInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPfInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPfInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPayslipWt> GetDHRPYPayslipWt(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPayslipWt> responseModel = new ResponseModel<PYPayslipWt>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPayslipWt;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayslipWt>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPayslipGroup> GetDHRPYPayslipGroup(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPayslipGroup> responseModel = new ResponseModel<PYPayslipGroup>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPayslipGroup;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayslipGroup>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPayslipHeader> GetDHRPYPayslipHeader(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPayslipHeader> responseModel = new ResponseModel<PYPayslipHeader>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPayslipHeader;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayslipHeader>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPayslipHeader> GetDHRPYPayslipHeaderByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYPayslipHeader> responseModel = new ResponseModel<PYPayslipHeader>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPayslipHeader;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayslipHeader>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYBankMaster> GetDHRPYBankMaster(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYBankMaster> responseModel = new ResponseModel<PYBankMaster>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYBankMaster;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYBankMaster>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public ResponseModel<PYPayStructure> GetDHRPYPayStructure(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PYPayStructure> responseModel = new ResponseModel<PYPayStructure>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsSentPYPayStructure;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PYPayStructure>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
                
            return responseModel;
        }

        public PostResult PostDHRPAPYTaxDedInf(PYTaxDedInf taxDedInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsReceivePYTaxDedInf;
                    string dataPost = JsonConvert.SerializeObject(taxDedInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, taxDedInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPYNonregIncDed(PYNonRegIncDed oNonRegIncDed , string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsReceivePYNonregIncDed;
                    string dataPost = JsonConvert.SerializeObject(oNonRegIncDed);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, oNonRegIncDed.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }
    
        public PostResult PostDHRPYAccountInf(PYAccountPost oAccountInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PayrollRoute.Route(CompanyCode).Payroll_WsReceivePYAccountInf;
                    string dataPost = JsonConvert.SerializeObject(oAccountInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, oAccountInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }
            return postResult;
        }
    }
}
