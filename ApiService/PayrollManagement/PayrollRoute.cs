﻿using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class PayrollRoute
    {
        public PayrollRoute()
        {

        }
        private static string ModuleID = "DHR.ROUTE.PY";
        private string CompanyCode { get; set; }

        public static PayrollRoute Route (string oCompanyCode)
        {
            PayrollRoute oPaymentRoute = new PayrollRoute()
            {
                CompanyCode = oCompanyCode
            };
            return oPaymentRoute;
        }
        



        #region example variable
        public string PayrollRoute_WsSentPYTaxAllowance
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYTAXALLOWANCE");
            }
        }

        public string PayrollRoute_WsSentPYGetProvidentfund
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYGETPROVIDENTFUND");
            }
        }

        #endregion
        
        public string PayrollRoute_WsSentPYPayslipInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPAYSLIPINF");
            }
        }

        public string PayrollRoute_WsSentPYWageInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYWAGEINF");
            }
        }

        public string PayrollRoute_WsSentPYRegIncDed
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYREGINCDED");
            }
        }

        public string Payroll_WsSentPYNonRegIncDed
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYNONREGINCDED");
            }
        }

        public string Payroll_WsSentPYSpecialIncDed
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYSPECIALINCDED");
            }
        }

        public string Payroll_WsSentPYSsoInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYSSOINF");
            }
        }

        public string Payroll_WsSentPYAccountInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYACCOUNTINF");
            }
        }

        public string Payroll_WsSentPYPndInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPNDINF");
            }
        }

        public string Payroll_WsSentPYTaxDedInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYTAXDEDINF");
            }
        }

        public string Payroll_WsSentPYPersonalArea
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPERSONALAREA");
            }
        }

        public string Payroll_WsSentPYTaxInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYTAXINF");
            }
        }

        public string Payroll_WsSentPYWageType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYWAGETYPE");
            }
        }

        public string Payroll_WsSentPYCalOt
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYCALOT");
            }
        }

        public string Payroll_WsSentPYCalProrate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYCALPRORATE");
            }
        }

        public string Payroll_WsSentPYSsoRate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYSSORATE");
            }
        }

        public string Payroll_WsSentPYCalTax
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYCALTAX");
            }
        }

        public string Payroll_WsSentPYCalPf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYCALPF");
            }
        }

        public string Payroll_WsSentPYPfInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPFINF");
            }
        }

        public string Payroll_WsSentPYPayslipWt
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPAYSLIPWT");
            }
        }

        public string Payroll_WsSentPYPayslipGroup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPAYSLIPGROUP");
            }
        }

        public string Payroll_WsSentPYPayslipHeader
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPAYSLIPHEADER");
            }
        }

        public string Payroll_WsSentPYBankMaster
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYBANKMASTER");
            }
        }

        public string Payroll_WsSentPYPayStructure
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYPAYSTRUCTURE");
            }
        }

        #region POST DATA
       
        public string Payroll_WsReceivePYTaxDedInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPYTAXDEDINF");
            }
        }

        public string Payroll_WsReceivePYNonregIncDed
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPYNONREGINCDED");
            }
        }

        public string Payroll_WsReceivePYSpecialIncDed
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPYSPECIALINCDED");
            }
        }

        public string Payroll_WsReceivePYAccountInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPYACCOUNTINF");
            }
        }
        #endregion POST DATA
    }
}
