﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Shared
{
    public class PostResult
    {
        public string requestId { get; set; }
        public string refNo { get; set; }
        public int returnCode { get; set; }
        public string returnDesc { get; set; }
        public string returnBusinessCode { get; set; }
        public string returnParam { get; set; }
        public List<ReturnData> returnDatas { get; set; }
        public string data { get; set; }
    }
    public class ReturnData
    {
        public int? returnCode { get; set; }
        public string returnDesc { get; set; }
    }
}
