﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace DHR.HR.API
{
    public class AuthenRoute
    {
        public AuthenRoute()
        {

        }
        private static string ModuleID = "DHR.ROUTE.AUTHEN";
        
        public string CompanyCode { get; set; }

        public static AuthenRoute Route(string oCompanyCode)
        {
            AuthenRoute oAuthenRoute = new AuthenRoute()
            {
                CompanyCode = oCompanyCode
            };
            return oAuthenRoute;
        }

        public string Authentication_request
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "REQUESTAPIKEY");
            }
        }

        public string Authentication_refresh
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "REFRESHAPIKEY");
            }
        }
    }
}
