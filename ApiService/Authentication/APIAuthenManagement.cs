﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;

namespace DHR.HR.API
{
    public class APIAuthenManagement
    {

        #region Constructor
        public APIAuthenManagement()
        {

        }
        #endregion Constructor

        #region Multicompany Framework

        private Dictionary<string, APIAuthenManagement> cache = new Dictionary<string, APIAuthenManagement>();

        private static string ModuleID = "DHR.HR";

        public string CompanyCode { get; set; }

        public static APIAuthenManagement CreateInstance(string oCompanyCode)
        {
            APIAuthenManagement oAPIAuthenManagement = new APIAuthenManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIAuthenManagement;
        }
        

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }
        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");

            }
        }

        private string ApiUser
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIUSER");
            }
        }

        private string ApiPassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIPASSWORD");
            }
        }
        #endregion Multicompany Framework


        public string RequestApiToken()
        {
            return AuthenServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.RequestApiToken(ApiUser, ApiPassword);
        }

        public string RefreshApiToken(string token)
        {
            return AuthenServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.RequestApiTokenRefresh(token);
        }
    }
}
