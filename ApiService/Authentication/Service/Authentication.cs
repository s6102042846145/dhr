﻿using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API;
using ESS.SHAREDATASERVICE;
using System.Net;

namespace DHR.HR.API
{
    public class Authentication : IAuthentication
    {
        #region Constructor
        public Authentication(string oCompanyCode, string oBaseUrl, string oGateway)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateway;
        }
        #endregion Constructor
        #region Member
        private string ModuleID = "DHR.HR";
        public string CompanyCode { get; set; }
        public string BaseUrl { get; set; }
        public string Gateway { get; set; }

        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }

        }


        #endregion Member

        public string RequestApiToken(string username, string password)
        {
            TokenRequest token = new TokenRequest();
            token.username = username;
            token.password = password;
            token.companycode = CompanyCode;

            string ApiKey = string.Empty;
            using (var client = new HttpClient())
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                string oRoute = AuthenRoute.Route(CompanyCode).Authentication_request;
                string dataRequest = JsonConvert.SerializeObject(token);
                var contentRequest = new StringContent(dataRequest, Encoding.UTF8, ContentType);
                //string ApiGateWay = Gateway + "/" + oRoute;
                string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);
                HttpResponseMessage response = client.PostAsync(ApiGateWay, contentRequest).Result;
                if (response.IsSuccessStatusCode)
                {
                    ApiKey = response.Content.ReadAsAsync<string>().Result;
                }
            }
            return ApiKey;
        }

        public string RequestApiTokenRefresh(string token)
        {
            string ApiKey = string.Empty;
            using (var client = new HttpClient())
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                string oRoute = AuthenRoute.Route(CompanyCode).Authentication_refresh;
                //string ApiGateWay = Gateway + "/" + oRoute+"?token="+token;
                string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute) + "?token=" + token;
                HttpResponseMessage response = client.PostAsJsonAsync(ApiGateWay, "").Result;
                if (response.IsSuccessStatusCode)
                {
                    ApiKey = response.Content.ReadAsAsync<string>().Result;
                }
            }
            return ApiKey;
        }
    }
}
