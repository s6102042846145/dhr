﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TokenRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string companycode { get; set; }

    }
}
