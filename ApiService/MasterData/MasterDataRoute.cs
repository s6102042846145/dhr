﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace DHR.HR.API
{
    public class MasterDataRoute
    {
        public MasterDataRoute()
        {

        }
        private static string ModuleID = "DHR.ROUTE.MT";

        public string CompanyCode { get; set; }

        public static MasterDataRoute Route(string oCompanyCode)
        {
            MasterDataRoute oMasterDataRoute = new MasterDataRoute()
            {
                CompanyCode = oCompanyCode
            };
            return oMasterDataRoute;
        }

        public string MasterDataRoute_WsSentDataCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTMTDATACODE");
            }
        }

        public string MasterDataRoute_WsSentDataValue
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTMTDATAVALUE");
            }
        }

        public string MasterDataRoute_WsSentDistrictProvince
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTMTDISTRICTPROVINCE");
            }
        }


        public string MasterDataRoute_WsSentMTValidateCodeValue
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTMTVALIDATECODEVALUE");
            }
        }

        public string MasterDataRoute_WsSentMTValidateTitle
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTMTVALIDATETITLE");
            }
        }
    }
}
