﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class MasterDataValue
    {
        public string dataCode { get; set; }
        public string valueCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string valueName { get; set; }
        public string valueName_EN { get; set; }
        public int? sequence { get; set; }
        public string text1 { get; set; }
        public string text2 { get; set; }
        public string text3 { get; set; }
        public string text4 { get; set; }
        public string text5 { get; set; }
        public double? num1 { get; set; }
        public double? num2 { get; set; }
        public double? num3 { get; set; }
        public double? num4 { get; set; }
        public double? num5 { get; set; }
  
    }
}

