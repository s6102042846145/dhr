﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class DistrictProvice 
    {
        public string provinceCode { get; set; }
        public string provinceName { get; set; }
        public string provinceNameEn { get; set; }
        public string districtCode { get; set; }
        public string districtName { get; set; }
        public string districtNameEn { get; set; }
        public string subdistrictCode { get; set; }
        public string subdistrictName { get; set; }
        public string subdistrictNameEn { get; set; }
        public string postcode { get; set; }

       
    }
}
