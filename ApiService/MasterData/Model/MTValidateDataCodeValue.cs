﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
   public class MTValidateDataCodeValue
    {
        public string dataCode { get; set; }
        public string valueCode { get; set; }
        public string valueName { get; set; }
        public string valueNameEn { get; set; }
        public string mapDataCode { get; set; }
        public string mapValueCode { get; set; }
        public string mapValueName { get; set; }
        public string mapValueNameEn { get; set; }
    }
} 