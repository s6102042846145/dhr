﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class MasterDataCode
    {
        public string dataCode { get; set; }
        public string dataName { get; set; }
        public string module { get; set; }
        public string remark { get; set; }
        public string recordStatus { get; set; }
    }

}
