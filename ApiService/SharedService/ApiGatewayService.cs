﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class ApiGatewayService
    {
        public static string ApiGateWay(string Gateway, string oRoute, string Params=null)
        {
            string strGateway = string.Empty;
            if (!string.IsNullOrEmpty(Gateway))
            {
                strGateway = Gateway + "/";
            }
            strGateway = strGateway + oRoute ;

            if (!string.IsNullOrEmpty(Params))
            {
                strGateway = strGateway + "/" + Params;
            }

            return strGateway;
        }
    }
}
