﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
namespace DHR.HR.API
{
    public class DateTimeService
    {
        public static DateTime ConvertDateTime(string CompanyCode , string strDateTime)
        {
            string formats = APIServices.SVC(CompanyCode).MultipleFormatDate;

            var Dateformats = formats.Split('|');
            DateTime parsedDate;
            DateTime.TryParseExact(strDateTime, Dateformats, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate);
            return parsedDate;
        }
      
    }
}
