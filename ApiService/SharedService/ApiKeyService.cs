﻿using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DHR.HR.API
{
    public class ApiKeyService
    {

        // Add
        //Calls to this method will fail if an item with the same key parameter is already stored in the Cache. 
        //To overwrite an existing Cache item using the same key parameter, use the Insert method.

        //Insert
        //This method will overwrite an existing cache item whose key matches the key parameter.

        public static string ApiKeyInsertCache(string CompanyCode, string ApiKeyCache, string token)
        {

            string apiKey = string.Empty;
            try
            {
                apiKey = APIAuthenManagement.CreateInstance(CompanyCode).RefreshApiToken(token);

                int periodTimeExpired = APIServices.SVC(CompanyCode).Period_Time_Expired; //Minutes
                HttpContext.Current.Cache.Insert(ApiKeyCache, apiKey, null, DateTime.Now.AddMinutes(periodTimeExpired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);

            }
            catch (Exception ex)
            {
                throw new DataServiceException("APISERVICE", "Get API Key Error", ex.Message);
            }

            return apiKey;
        }

        //public void ApiKeyRemoveCache()
        //{

        //    if (HttpContext.Current.Cache.Get(_apiKeyRequest) != null)
        //    {
        //        HttpContext.Current.Cache.Remove(_apiKeyRequest);
        //    }

        //}

        public static string ApiKeyRefreshManageMent(string CompanyCode, string EmployeeID, string token)
        {
            string apiKeyRequest = string.Format("ApiKeyRequest_{0}_{1}", CompanyCode, EmployeeID);
            return ApiKeyInsertCache(CompanyCode,apiKeyRequest, token);
        }

        public static bool CheckTokenIsExpired(HttpResponseMessage response)
        {
            bool isExpired = false;
            List<AuthenticationHeaderValue> authValue = response.Headers.WwwAuthenticate.ToList();
            if (authValue.Count > 0)
            {
                if (response.StatusCode.ToString() == "Unauthorized" && authValue[0].Scheme == "Bearer" && authValue[0].Parameter.Contains("The token is expired"))
                {
                    isExpired = true;
                }
            }
            return isExpired;
        }
    }
}
