﻿using DESS.HR.API.Model;
using DESS.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.HR.API
{
    public class APIPaymentManagement
    {
        #region Constructor
        public APIPaymentManagement()
        {

        }
        #endregion Constructor
        #region Multicompany Framework
       // private Dictionary<string, APIPersonalManagement> cache = new Dictionary<string, APIPersonalManagement>();

        private string ModuleID = "DESS.HR";

        private string CompanyCode { get; set; }

        public static APIPaymentManagement CreateInstance(string oCompanyCode)
        {
            APIPaymentManagement oAPIPaymentManagement = new APIPaymentManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIPaymentManagement;
        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        private string ApiUser
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIUSER");
            }
        }

        private string ApiPassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIPASSWORD");
            }
        }
        private string ApiKey
        {
            get
            {
                return APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken(ApiUser, ApiPassword);
            }
        }

        #endregion Multicompany Framework

        #region GET ALL DATA
       
        public ResponseModel<PYProvidentfundInf> GetDHRPYPaymentGetProvidentFund(string startDate, string endDate, string EmpCode)
        {
            return PaymentServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPaymentGetProvidentFund(startDate, endDate, ApiKey, EmpCode);
        }

        #endregion GET ALL DATA

        #region GET BY EMPCODE


        public ResponseModel<PYTaxAllowanceInf> GetDHRPYPaymentTaxAllowanceByEmpcode(string startDate, string endDate, string EmpCode)
        {
            return PaymentServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPaymentTaxAllowanceByEmpcode(startDate, endDate, ApiKey, EmpCode);
        }

       


        #endregion GET BY EMPCODE
    }
}
