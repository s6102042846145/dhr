﻿using DESS.HR.API.Interfaces;
using DESS.HR.API.Model;
using DESS.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DESS.HR.API
{
    public class Payment : IPayment
    {
        #region Constructor
        public Payment(string oCompanyCode , string oBaseUrl, string oGateway)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateway;
        }
        #endregion Constructor

        #region Member
        private string ModuleID = "DESS.HR";
        private string  CompanyCode { get; set; }
        private string  BaseUrl { get; set; }
        private string Gateway { get; set; }
        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }

        }

        private string Scheme
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SCHEME");
            }
        }
        #endregion Member

        public ResponseModel<PYTaxAllowanceInf> GetDHRPYPaymentTaxAllowanceByEmpcode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYTaxAllowanceInf> responseModel = new ResponseModel<PYTaxAllowanceInf>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme,ApiKey);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                string oRoute = PaymentRoute.Route(CompanyCode).PaymentRoute_WsSentPYTaxAllowance;
                //string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                //test
                string strParams = startDate + "/" + endDate + "/" + "1000" + "/" + EmpCode;
                string ApiGateWay = Gateway + "/" + oRoute + "/" + strParams;
                HttpResponseMessage response = client.GetAsync(ApiGateWay).Result; ;
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    responseModel = JsonConvert.DeserializeObject<ResponseModel<PYTaxAllowanceInf>>(content);
                }
                else
                {
                    responseModel.message = response.ReasonPhrase;
                }
            }
            return responseModel;
        }
        
        public ResponseModel<PYProvidentfundInf> GetDHRPYPaymentGetProvidentFund(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PYProvidentfundInf> responseModel = new ResponseModel<PYProvidentfundInf>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                string oRoute = PaymentRoute.Route(CompanyCode).PaymentRoute_WsSentPYGetProvidentfund;
                //string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                //test
                string strParams = startDate + "/" + endDate + "/" + "1000" + "/" + EmpCode;
                string ApiGateWay = Gateway + "/" + oRoute + "/" + strParams;
                HttpResponseMessage response = client.GetAsync(ApiGateWay).Result; ;
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    responseModel = JsonConvert.DeserializeObject<ResponseModel<PYProvidentfundInf>>(content);
                }
                else
                {
                    responseModel.message = response.ReasonPhrase;
                }
            }
            return responseModel;
        }




    }
}
