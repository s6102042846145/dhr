﻿using DHR.HR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.API.DB
{
    public interface IPersonalDataService
    {
        #region JOB  
        string SaveAllPAPersonalInf(List<PAPersonalInf> data);
        //INFOTYPE0001
        string SaveAllPAInfoPersonal(List<PAInfoPersonal> data);
        string SaveAllPAOrganizationInf(List<PAOrganizationInf> data);
        //string SaveAllWorkCalendar(List<PAWorkCalendar> data);
        string SaveAllSecondmentInf(List<PASecondmentInf> data);
        string SaveAllContactInf(List<PAContactInf> data);
        string SaveAllPosition(List<PAPosition> data);
        string SaveAllActingPosition(List<PAActingPosition> data);
        string SaveAllImportanceDateInf(List<PAImportanceDateInf> data);
        #endregion JOB
    }
}
