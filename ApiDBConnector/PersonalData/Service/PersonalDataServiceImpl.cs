﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Model;
using DHR.HR.API;
using ESS.SHAREDATASERVICE;

namespace DESS.API.DB 
{
    public class PersonalDataServiceImpl : IPersonalDataService
    {
        public PersonalDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #region Property
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private string CompanyCode { get; set; }
        private static string ModuleID = "DHR.JOB.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion
        public string SaveAllPAPersonalInf(List<PAPersonalInf> data)
        {
            string ErrMsg = string.Empty;
            using(SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;

                    try
                    {
                        //oCommandDel = new SqlCommand("sp_DHR_PA_Delete_PersonalInf", oConnection, oTransaction);
                        //oCommandDel.CommandType = CommandType.StoredProcedure;
                        //oCommandDel.ExecuteNonQuery();
                        foreach (PAPersonalInf item in data)
                        {
                            oCommandDel = new SqlCommand("sp_DHR_PA_Delete_PersonalInf", oConnection, oTransaction);
                            oCommandDel.CommandType = CommandType.StoredProcedure;
                            oCommandDel.Parameters.AddWithValue("@p_employeeID", item.empCode);
                            oCommandDel.ExecuteNonQuery();
                        }

                        foreach (PAPersonalInf item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_PersonalInf", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode",item.empCode);
                            oCommand.Parameters.AddWithValue("@p_inameCode",item.inameCode);
                            oCommand.Parameters.AddWithValue("@p_inameValue",item.inameValue);
                            oCommand.Parameters.AddWithValue("@p_inameTextTH", item.inameTextTH);
                            oCommand.Parameters.AddWithValue("@p_inameTextEN", item.inameTextEN);
                            oCommand.Parameters.AddWithValue("@p_iname1Code",item.iname1Code);
                            oCommand.Parameters.AddWithValue("@p_iname1Value",item.iname1Value);
                            oCommand.Parameters.AddWithValue("@p_iname1TextTH", item.iname1TextTH);
                            oCommand.Parameters.AddWithValue("@p_iname1TextEN", item.iname1TextEN);
                            oCommand.Parameters.AddWithValue("@p_iname2Code" ,item.iname2Code);
                            oCommand.Parameters.AddWithValue("@p_iname2Value" , item.iname2Value);
                            oCommand.Parameters.AddWithValue("@p_iname2TextTH", item.iname2TextTH);
                            oCommand.Parameters.AddWithValue("@p_iname2TextEN", item.iname2TextEN);
                            oCommand.Parameters.AddWithValue("@p_iname3Code" ,item.iname3Code);
                            oCommand.Parameters.AddWithValue("@p_iname3Value" ,item.iname3Value);
                            oCommand.Parameters.AddWithValue("@p_iname3TextTH", item.iname3TextTH);
                            oCommand.Parameters.AddWithValue("@p_iname3TextEN", item.iname3TextEN);
                            oCommand.Parameters.AddWithValue("@p_iname4Code" ,item.iname4Code);
                            oCommand.Parameters.AddWithValue("@p_iname4Value",item.iname4Value);
                            oCommand.Parameters.AddWithValue("@p_iname4TextTH", item.iname4TextTH);
                            oCommand.Parameters.AddWithValue("@p_iname4TextEN", item.iname4TextEN);
                            oCommand.Parameters.AddWithValue("@p_iname5Code" ,item.iname5Code);
                            oCommand.Parameters.AddWithValue("@p_iname5Value",item.iname5Value);
                            oCommand.Parameters.AddWithValue("@p_iname5TextTH", item.iname5TextTH);
                            oCommand.Parameters.AddWithValue("@p_iname5TextEN", item.iname5TextEN);
                            oCommand.Parameters.AddWithValue("@p_fname" ,item.fname);
                            oCommand.Parameters.AddWithValue("@p_mname" ,item.mname);
                            oCommand.Parameters.AddWithValue("@p_lname" ,item.lname);
                            oCommand.Parameters.AddWithValue("@p_fullName" ,item.fullName);
                            oCommand.Parameters.AddWithValue("@p_inameEnCode" ,item.inameEnCode);
                            oCommand.Parameters.AddWithValue("@p_inameEnValue" ,item.inameEnValue);
                            oCommand.Parameters.AddWithValue("@p_inameEnText", item.inameEnText);
                            oCommand.Parameters.AddWithValue("@p_fnameEn" ,item.fnameEn);
                            oCommand.Parameters.AddWithValue("@p_mnameEn" ,item.mnameEn);
                            oCommand.Parameters.AddWithValue("@p_lnameEn" ,item.lnameEn);
                            oCommand.Parameters.AddWithValue("@p_fullNameEnText", item.fullNameEnText);
                            oCommand.Parameters.AddWithValue("@p_nickName" , item.nickName);
                            oCommand.Parameters.AddWithValue("@p_countryBirthCode",item.countryBirthCode);
                            oCommand.Parameters.AddWithValue("@p_countryBirthValue" ,item.countryBirthValue);
                            oCommand.Parameters.AddWithValue("@p_countryBirthTextTH", item.countryBirthTextTH);
                            oCommand.Parameters.AddWithValue("@p_countryBirthTextEN", item.countryBirthTextEN);
                            oCommand.Parameters.AddWithValue("@p_provinceBirthCode", item.provinceBirthCode);
                            oCommand.Parameters.AddWithValue("@p_provinceBirthTextTH", item.provinceBirthTextTH);
                            oCommand.Parameters.AddWithValue("@p_provinceBirthTextEN", item.provinceBirthTextEN);
                            oCommand.Parameters.AddWithValue("@p_raceCode",item.raceCode);
                            oCommand.Parameters.AddWithValue("@p_raceValue" ,item.raceValue);
                            oCommand.Parameters.AddWithValue("@p_raceTextTH", item.raceTextTH);
                            oCommand.Parameters.AddWithValue("@p_raceTextEN", item.raceTextEN);
                            oCommand.Parameters.AddWithValue("@p_nationalityCode",item.nationalityCode);
                            oCommand.Parameters.AddWithValue("@p_nationalityValue",item.nationalityValue);
                            oCommand.Parameters.AddWithValue("@p_nationalityTextTH", item.nationalityTextTH);
                            oCommand.Parameters.AddWithValue("@p_nationalityTextEN", item.nationalityTextEN);
                            oCommand.Parameters.AddWithValue("@p_religionCode" ,item.religionCode);
                            oCommand.Parameters.AddWithValue("@p_religionValue" ,item.religionValue);
                            oCommand.Parameters.AddWithValue("@p_religionTextTH", item.religionTextTH);
                            oCommand.Parameters.AddWithValue("@p_religionTextEN", item.religionTextEN);
                            oCommand.Parameters.AddWithValue("@p_maritalStatusCode" ,item.maritalStatusCode);
                            oCommand.Parameters.AddWithValue("@p_maritalStatusValue",item.maritalStatusValue);
                            oCommand.Parameters.AddWithValue("@p_maritalStatusTextTH", item.maritalStatusTextTH);
                            oCommand.Parameters.AddWithValue("@p_maritalStatusTextEN", item.maritalStatusTextEN);
                            oCommand.Parameters.AddWithValue("@p_sexCode" ,item.sexCode);
                            oCommand.Parameters.AddWithValue("@p_sexValue",item.sexValue);
                            oCommand.Parameters.AddWithValue("@p_sexTextTH", item.sexTextTH);
                            oCommand.Parameters.AddWithValue("@p_sexTextEN", item.sexTextEN);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate" , DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return ErrMsg;
        }

        public string SaveAllPAOrganizationInf(List<PAOrganizationInf> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_PA_Delete_OrganizationInf", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (PAOrganizationInf item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_OrganizationInf",oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode",item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode",item.empCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                            oCommand.Parameters.AddWithValue("@p_empStatusCode",item.empStatusCode);
                            oCommand.Parameters.AddWithValue("@p_empStatusValue", item.empStatusValue);
                            oCommand.Parameters.AddWithValue("@p_empStatusTextTH", item.empStatusTextTH);
                            oCommand.Parameters.AddWithValue("@p_empStatusTextEN", item.empStatusTextEN);
                            oCommand.Parameters.AddWithValue("@p_empTypeCode",item.empTypeCode);
                            oCommand.Parameters.AddWithValue("@p_empTypeValue",item.empTypeValue);
                            oCommand.Parameters.AddWithValue("@p_empTypeTextTH", item.empTypeTextTH);
                            oCommand.Parameters.AddWithValue("@p_empTypeTextEN", item.empTypeTextEN);
                            oCommand.Parameters.AddWithValue("@p_posCode" ,item.posCode);
                            oCommand.Parameters.AddWithValue("@p_posTextTH", item.posTextTH);
                            oCommand.Parameters.AddWithValue("@p_posTextEN", item.posTextEN);
                            oCommand.Parameters.AddWithValue("@p_spcPosName" ,item.spcPosName);
                            oCommand.Parameters.AddWithValue("@p_unitCode" ,item.unitCode);
                            oCommand.Parameters.AddWithValue("@p_unitTextTH", item.unitTextTH);
                            oCommand.Parameters.AddWithValue("@p_unitTextEN", item.unitTextEN);
                            oCommand.Parameters.AddWithValue("@p_costCenterCode",item.costCenterCode);
                            oCommand.Parameters.AddWithValue("@p_costCenterValue", item.costCenterValue);
                            oCommand.Parameters.AddWithValue("@p_costCenterTextTH", item.costCenterTextTH);
                            oCommand.Parameters.AddWithValue("@p_costCenterTextEN", item.costCenterTextEN);
                            oCommand.Parameters.AddWithValue("@p_levelCode" ,item.levelCode);
                            oCommand.Parameters.AddWithValue("@p_levelValue",item.levelValue);
                            oCommand.Parameters.AddWithValue("@p_levelText", item.levelText);
                            oCommand.Parameters.AddWithValue("@p_actingLevelCode",item.actingLevelCode);
                            oCommand.Parameters.AddWithValue("@p_actingLevelValue" ,item.actingLevelValue);
                            oCommand.Parameters.AddWithValue("@p_actingLevelText", item.actingLevelText);
                            oCommand.Parameters.AddWithValue("@p_bandCode" ,item.bandCode);
                            oCommand.Parameters.AddWithValue("@p_bandValue" ,item.bandValue);
                            oCommand.Parameters.AddWithValue("@p_bandTextTH", item.bandTextTH);
                            oCommand.Parameters.AddWithValue("@p_bandTextEN", item.bandTextEN);
                            oCommand.Parameters.AddWithValue("@p_actingBandCode" ,item.actingBandCode);
                            oCommand.Parameters.AddWithValue("@p_actingBandValue",item.actingBandValue);
                            oCommand.Parameters.AddWithValue("@p_actingBandTextTH", item.actingBandTextTH);
                            oCommand.Parameters.AddWithValue("@p_actingBandTextEN", item.actingBandTextEN);
                            oCommand.Parameters.AddWithValue("@p_countryWorkCode",item.countryWorkCode);
                            oCommand.Parameters.AddWithValue("@p_countryWorkValue",item.countryWorkValue);
                            oCommand.Parameters.AddWithValue("@p_countryWorkTextTH", item.countryWorkTextTH);
                            oCommand.Parameters.AddWithValue("@p_countryWorkTextEN", item.countryWorkTextEN);
                            oCommand.Parameters.AddWithValue("@p_workPlaceCode",item.workPlaceCode);
                            oCommand.Parameters.AddWithValue("@p_workPlaceValue",item.workPlaceValue);
                            oCommand.Parameters.AddWithValue("@p_workPlaceTextTH", item.workPlaceTextTH);
                            oCommand.Parameters.AddWithValue("@p_workPlaceTextEN", item.workPlaceTextEN);
                            oCommand.Parameters.AddWithValue("@p_workAreaCode" ,item.workAreaCode);
                            oCommand.Parameters.AddWithValue("@p_workAreaValue" ,item.workAreaValue);
                            oCommand.Parameters.AddWithValue("@p_workAreaTextTH", item.workAreaTextTH);
                            oCommand.Parameters.AddWithValue("@p_workAreaTextEN", item.workAreaTextEN);
                            oCommand.Parameters.AddWithValue("@p_workTimeCode",item.workTimeCode);
                            oCommand.Parameters.AddWithValue("@p_workTimeValue",item.workTimeValue);
                            oCommand.Parameters.AddWithValue("@p_workTimeTextTH", item.workTimeTextTH);
                            oCommand.Parameters.AddWithValue("@p_workTimeTextEN", item.workTimeTextEN);
                            oCommand.Parameters.AddWithValue("@p_reportToPosCode" ,item.reportToPosCode);
                            oCommand.Parameters.AddWithValue("@p_reportToPosTextTH", item.reportToPosTextTH);
                            oCommand.Parameters.AddWithValue("@p_reportToPosTextEN", item.reportToPosTextEN);
                            oCommand.Parameters.AddWithValue("@p_reportToBandCode" ,item.reportToBandCode);
                            oCommand.Parameters.AddWithValue("@p_reportToBandValue",item.reportToBandValue);
                            oCommand.Parameters.AddWithValue("@p_reportToBandTextTH", item.reportToBandTextTH);
                            oCommand.Parameters.AddWithValue("@p_reportToBandTextEN", item.reportToBandTextEN);
                            oCommand.Parameters.AddWithValue("@p_hrAdmin",item.hrAdmin);
                            oCommand.Parameters.AddWithValue("@p_spcPosNameEn",item.spcPosNameEn);                           
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        //public string SaveAllWorkCalendar(List<PAWorkCalendar> data)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_PA_Delete_WorkCalendar", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();
        //                foreach (PAWorkCalendar item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_PA_Save_WorkCalendar", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_calendarCode", item.calendarCode);
        //                    oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
        //                    oCommand.Parameters.AddWithValue("@p_psnAreaCode", item.psnAreaCode);
        //                    oCommand.Parameters.AddWithValue("@p_psnAreaValue", item.psnAreaValue);
        //                    oCommand.Parameters.AddWithValue("@p_psnAreaText", item.psnAreaText);
        //                    oCommand.Parameters.AddWithValue("@p_psnlSubAreaCode", item.psnlSubAreaCode);
        //                    oCommand.Parameters.AddWithValue("@p_psnlSubAreaValue", item.psnlSubAreaValue);
        //                    oCommand.Parameters.AddWithValue("@p_psnlSubAreaText", item.psnlSubAreaText);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTime.ParseExact(item.startDate, DefaultReturnDateFormat, null));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTime.ParseExact(item.endDate, DefaultReturnDateFormat, null));
                           
        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();

        //            }
        //            catch (Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        public string SaveAllSecondmentInf(List<PASecondmentInf> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_PA_Delete_SecondmentInf", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (PASecondmentInf item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_SecondmentInf", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_scmTypeCode", item.scmTypeCode);
                            oCommand.Parameters.AddWithValue("@p_scmTypeValue", item.scmTypeValue);
                            oCommand.Parameters.AddWithValue("@p_scmTypeTextTH", item.scmTypeTextTH);
                            oCommand.Parameters.AddWithValue("@p_scmTypeTextEN", item.scmTypeTextEN);
                            oCommand.Parameters.AddWithValue("@p_scmOutCode", item.scmOutCode);
                            oCommand.Parameters.AddWithValue("@p_scmOutTextTH", item.scmOutTextTH);
                            oCommand.Parameters.AddWithValue("@p_scmOutTextEN", item.scmOutTextEN);
                            oCommand.Parameters.AddWithValue("@p_levelCode", item.levelCode);
                            oCommand.Parameters.AddWithValue("@p_levelvalue", item.levelValue);
                            oCommand.Parameters.AddWithValue("@p_levelTextTH", item.levelTextTH);
                            oCommand.Parameters.AddWithValue("@p_levelTextEN", item.levelTextEN);
                            oCommand.Parameters.AddWithValue("@p_knowHow", item.knowHow);
                            oCommand.Parameters.AddWithValue("@p_posName", item.posName);
                            oCommand.Parameters.AddWithValue("@p_posNameEn", item.posNameEn);
                            oCommand.Parameters.AddWithValue("@p_unitName", item.unitName);
                            oCommand.Parameters.AddWithValue("@p_unitNameEn", item.unitNameEn);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                     
                    
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllContactInf(List<PAContactInf> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        //oCommandDel = new SqlCommand("sp_DHR_PA_Delete_ContactInf", oConnection, oTransaction);
                        //oCommandDel.CommandType = CommandType.StoredProcedure;
                        //oCommandDel.ExecuteNonQuery();
                        foreach (PAContactInf item in data)
                        {

                            oCommandDel = new SqlCommand("sp_DHR_PA_Delete_ContactInf", oConnection, oTransaction);
                            oCommandDel.CommandType = CommandType.StoredProcedure;
                            oCommandDel.Parameters.AddWithValue("@p_employeeID", item.empCode);
                            oCommandDel.ExecuteNonQuery();

                        }

                        foreach (PAContactInf item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_ContactInf", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_contactCode", item.contactCode);
                            oCommand.Parameters.AddWithValue("@p_contactValue", item.contactValue);
                            oCommand.Parameters.AddWithValue("@p_contactTextTH", item.contactTextTH);
                            oCommand.Parameters.AddWithValue("@p_contactTextEN", item.contactTextEN);
                            oCommand.Parameters.AddWithValue("@p_contactData", item.contactData);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();


                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllPosition(List<PAPosition> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_PA_Delete_Position", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (PAPosition item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_Position", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_posCode", item.posCode);
                            oCommand.Parameters.AddWithValue("@p_posTextTH", item.posTextTH);
                            oCommand.Parameters.AddWithValue("@p_posTextEN", item.posTextEN);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllActingPosition(List<PAActingPosition> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_PA_Delete_ActingPosition", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (PAActingPosition item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_ActingPosition", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_actingPosCode", item.actingPosCode);
                            oCommand.Parameters.AddWithValue("@p_actingPosTextTH", item.actingPosTextTH);
                            oCommand.Parameters.AddWithValue("@p_actingPosTextEN", item.actingPosTextEN);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        //INFOTYPE0001
        public string SaveAllPAInfoPersonal(List<PAInfoPersonal> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;

                    try
                    {
                        //oCommandDel = new SqlCommand("sp_DHR_PA_Delete_InfoPersonal", oConnection, oTransaction);
                        //oCommandDel.CommandType = CommandType.StoredProcedure;
                        //oCommandDel.ExecuteNonQuery();
                        foreach (PAInfoPersonal item in data)
                        {

                            oCommandDel = new SqlCommand("sp_DHR_PA_Delete_InfoPersonal", oConnection, oTransaction);
                            oCommandDel.CommandType = CommandType.StoredProcedure;
                            oCommandDel.Parameters.AddWithValue("@p_employeeID", item.employeeID);
                            oCommandDel.ExecuteNonQuery();

                        }

                        foreach (PAInfoPersonal item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PA_Save_InfoPersonal", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_employeeID", item.employeeID);
                            oCommand.Parameters.AddWithValue("@p_subType",item.subType);
                            oCommand.Parameters.AddWithValue("@p_beginDate", DateTimeService.ConvertDateTime(CompanyCode, item.beginDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_name", item.name);
                            oCommand.Parameters.AddWithValue("@p_area", item.area);
                            oCommand.Parameters.AddWithValue("@p_areaTextTH", item.areaTextTH);
                            oCommand.Parameters.AddWithValue("@p_areaTextEN", item.areaTextEN);
                            oCommand.Parameters.AddWithValue("@p_subArea", item.subArea);
                            oCommand.Parameters.AddWithValue("@p_subAreaTextTH", item.subAreaTextTH);
                            oCommand.Parameters.AddWithValue("@p_subAreaTextEN", item.subAreaTextEN);
                            oCommand.Parameters.AddWithValue("@p_empGroup", item.empGroup);
                            oCommand.Parameters.AddWithValue("@p_empGroupTextTH", item.empGroupTextTH);
                            oCommand.Parameters.AddWithValue("@p_empGroupTextEN", item.empGroupTextEN);
                            oCommand.Parameters.AddWithValue("@p_empSubGroup", item.empSubGroup);
                            oCommand.Parameters.AddWithValue("@p_orgUnit", item.orgUnit);
                            oCommand.Parameters.AddWithValue("@p_orgUnitTextTH", item.orgUnitTextTH);
                            oCommand.Parameters.AddWithValue("@p_orgUnitTextEN", item.orgUnitTextEN);
                            oCommand.Parameters.AddWithValue("@p_position", item.position);
                            oCommand.Parameters.AddWithValue("@p_positionTextTH", item.positionTextTH);
                            oCommand.Parameters.AddWithValue("@p_positionTextEN", item.positionTextEN);
                            oCommand.Parameters.AddWithValue("@p_adminGroup", item.adminGroup);
                            oCommand.Parameters.AddWithValue("@p_costCenter", item.costCenter);
                            oCommand.Parameters.AddWithValue("@p_businessArea", item.businessArea);
                            oCommand.Parameters.AddWithValue("@p_payrollArea", item.payrollArea);
                            oCommand.Parameters.AddWithValue("@p_secondaryCompanyCode", item.secondaryCompanyCode);
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return ErrMsg;
        }

        public string SaveAllImportanceDateInf(List<PAImportanceDateInf> data)
        {
            string ErrMsg = string.Empty;
            string ttt = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_PA_Delete_ImportanceDateInf", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (PAImportanceDateInf item in data)
                        {
                            DateTime minDate = DateTime.MinValue;
                            ttt = item.empCode;
                            oCommand = new SqlCommand("sp_DHR_PA_Save_ImportanceDateInf", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                           
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.startWorkDate))
                                oCommand.Parameters.AddWithValue("@p_startWorkDate", DateTimeService.ConvertDateTime(CompanyCode, item.startWorkDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.employedDate))
                                oCommand.Parameters.AddWithValue("@p_employedDate",  DateTimeService.ConvertDateTime(CompanyCode, item.employedDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countLeaveAgeDate))
                                oCommand.Parameters.AddWithValue("@p_countLeaveAgeDate", DateTimeService.ConvertDateTime(CompanyCode, item.countLeaveAgeDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.registFundDate))
                                oCommand.Parameters.AddWithValue("@p_registFundDate",  DateTimeService.ConvertDateTime(CompanyCode, item.registFundDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countFundAgeDate))
                                oCommand.Parameters.AddWithValue("@p_countFundAgeDate",  DateTimeService.ConvertDateTime(CompanyCode, item.countFundAgeDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.startWorkGroupDate))
                                oCommand.Parameters.AddWithValue("@p_startWorkGroupDate",DateTimeService.ConvertDateTime(CompanyCode, item.startWorkGroupDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate1))
                                oCommand.Parameters.AddWithValue("@p_leavePfFundDate1", DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate1));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate2))
                                oCommand.Parameters.AddWithValue("@p_leavePfFundDate2", DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate2));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate3))
                                oCommand.Parameters.AddWithValue("@p_leavePfFundDate3", DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate3));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.retireDate))
                                oCommand.Parameters.AddWithValue("@p_retireDate",  DateTimeService.ConvertDateTime(CompanyCode, item.retireDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countLevelAgeDate))
                                oCommand.Parameters.AddWithValue("@p_countLevelAgeDate",  DateTimeService.ConvertDateTime(CompanyCode, item.countLevelAgeDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countLevelAgeGroupDate))
                                oCommand.Parameters.AddWithValue("@p_countLevelAgeGroupDate",  DateTimeService.ConvertDateTime(CompanyCode, item.countLevelAgeGroupDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countPosAgeDate))
                                oCommand.Parameters.AddWithValue("@p_countPosAgeDate",  DateTimeService.ConvertDateTime(CompanyCode, item.countPosAgeDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countUnitAgeDate))
                                oCommand.Parameters.AddWithValue("@p_countUnitAgeDate", DateTimeService.ConvertDateTime(CompanyCode, item.countUnitAgeDate));
                            if(minDate != DateTimeService.ConvertDateTime(CompanyCode, item.countRootJobAgeDate))
                                oCommand.Parameters.AddWithValue("@p_countRootJobAgeDate",  DateTimeService.ConvertDateTime(CompanyCode, item.countRootJobAgeDate));
                           
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ttt + ' ' + ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }
    }
}
