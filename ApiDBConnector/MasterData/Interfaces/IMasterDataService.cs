﻿using DHR.HR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.API.DB
{
    public interface IMasterDataService
    {
        #region JOB

        string SaveAllMasterDataCode(List<MasterDataCode> data);
        string SaveAllMasterDataValue(List<MasterDataValue> data);

        #endregion JOB


    }
}
