﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API;
using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;

namespace DESS.API.DB
{
    public class MasterDataServiceImpl : IMasterDataService
    {
        public MasterDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #region Property
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }
        private static string ModuleID = "DHR.JOB.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Property
        public string SaveAllMasterDataCode(List<MasterDataCode> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;

                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_MS_Delete_DataCode", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (MasterDataCode item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_MS_Save_DataCode", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_dataCode", item.dataCode);
                            oCommand.Parameters.AddWithValue("@p_dataName", item.dataName);
                            oCommand.Parameters.AddWithValue("@p_module", item.module);
                            oCommand.Parameters.AddWithValue("@p_remark", item.remark);
                            oCommand.Parameters.AddWithValue("@p_recordStatus", item.recordStatus);
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();
                    }
                    catch(Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }

            return ErrMsg;
        }

        public string  SaveAllMasterDataValue(List<MasterDataValue> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;

                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_MS_Delete_DataValue", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();

                        foreach (MasterDataValue item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_MS_Save_DataValue", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_dataCode", item.dataCode);
                            oCommand.Parameters.AddWithValue("@p_valueCode", item.valueCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                            oCommand.Parameters.AddWithValue("@p_valueName", item.valueName);
                            oCommand.Parameters.AddWithValue("@p_valueName_EN", item.valueName_EN);
                            oCommand.Parameters.AddWithValue("@p_sequence", item.sequence);
                            oCommand.Parameters.AddWithValue("@p_text1", item.text1);
                            oCommand.Parameters.AddWithValue("@p_text2", item.text2);
                            oCommand.Parameters.AddWithValue("@p_text3", item.text3);
                            oCommand.Parameters.AddWithValue("@p_text4", item.text4);
                            oCommand.Parameters.AddWithValue("@p_text5", item.text5);
                            oCommand.Parameters.AddWithValue("@p_num1", item.num1);
                            oCommand.Parameters.AddWithValue("@p_num2", item.num2);
                            oCommand.Parameters.AddWithValue("@p_num3", item.num3);
                            oCommand.Parameters.AddWithValue("@p_num4", item.num4);
                            oCommand.Parameters.AddWithValue("@p_num5", item.num5);
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();
				    
                    }
                    catch(Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
                return ErrMsg;
            }
        }
    }
}
