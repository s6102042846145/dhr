﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API;
using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;

namespace DESS.API.DB
{
    public class TimeDataServiceImpl : ITimeDataService
    {
        public TimeDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #region Member

        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private string CompanyCode { get; set; }
        private static string ModuleID = "DHR.JOB.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        
        #endregion Member
        public string SaveAllTMBreakSchedule(List<TMBreakSchedule> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_BreakSchedule", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMBreakSchedule item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_BreakSchedule", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_breakSchCode", item.breakSchCode);
                            oCommand.Parameters.AddWithValue("@p_breakSchName", item.breakSchName);
                            oCommand.Parameters.AddWithValue("@p_startBreakTime", item.startBreakTime);
                            oCommand.Parameters.AddWithValue("@p_endBreakTime", item.endBreakTime);
                            oCommand.Parameters.AddWithValue("@p_paidHour",item.paidHour);
                            oCommand.Parameters.AddWithValue("@p_unpaidHour",item.unpaidHour);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMCalenHoliday(List<TMCalenHoliday> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_CalenHoliday", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMCalenHoliday item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_CalenHoliday", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empGrpGrouping", item.empGrpGrouping);
                            oCommand.Parameters.AddWithValue("@p_psnAreaGrouping", item.psnAreaGrouping);
                            oCommand.Parameters.AddWithValue("@p_holiCalenCode", item.holiCalenCode);
                            oCommand.Parameters.AddWithValue("@p_holiCalenName", item.holiCalenName);
                            oCommand.Parameters.AddWithValue("@p_holidayCode", item.holidayCode);
                            oCommand.Parameters.AddWithValue("@p_year", item.year);	
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMDailyWorkSchedule(List<TMDailyWorkSchedule> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_DailyWorkSchedule", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMDailyWorkSchedule item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_DailyWorkSchedule", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode", item.dayWorkSchCode);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchName", item.dayWorkSchName);
                            oCommand.Parameters.AddWithValue("@p_planWorkHour", item.planWorkHour);
                            oCommand.Parameters.AddWithValue("@p_startPlanTime", item.startPlanTime);
                            oCommand.Parameters.AddWithValue("@p_endPlanTime", item.endPlanTime);
                            oCommand.Parameters.AddWithValue("@p_breakSchCode", item.breakSchCode);
                            oCommand.Parameters.AddWithValue("@p_toleranceStartTime", item.toleranceStartTime);
                            oCommand.Parameters.AddWithValue("@p_toleranceEndTime", item.toleranceEndTime);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMEmpGroup(List<TMEmpGroup> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_EmpGroup", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMEmpGroup item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_EmpGroup", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empGrp", item.empGrp);
                            oCommand.Parameters.AddWithValue("@p_empSubGrp", item.empSubGrp);
                            oCommand.Parameters.AddWithValue("@p_empGrpGrouping", item.empGrpGrouping);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMLeaveQuotaType(List<TMLeaveQuotaType> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_LeaveQuotaType", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMLeaveQuotaType item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_LeaveQuotaType", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empGrpGrouping", item.empGrpGrouping);
                            oCommand.Parameters.AddWithValue("@p_psnAreaGrouping", item.psnAreaGrouping);
                            oCommand.Parameters.AddWithValue("@p_leaveQoutaCode", item.leaveQoutaCode);
                            oCommand.Parameters.AddWithValue("@p_leaveQoutaName", item.leaveQoutaName);
                            oCommand.Parameters.AddWithValue("@p_unitCode", item.unitCode);
                            oCommand.Parameters.AddWithValue("@p_unitValue", item.unitValue);
                            oCommand.Parameters.AddWithValue("@p_timeConst", item.timeConst);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMLeaveType(List<TMLeaveType> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_LeaveType", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMLeaveType item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_LeaveType", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empGrpGrouping", item.empGrpGrouping);
                            oCommand.Parameters.AddWithValue("@p_psnAreaGrouping", item.psnAreaGrouping);
                            oCommand.Parameters.AddWithValue("@p_leaveTypeCode", item.leaveTypeCode);
                            oCommand.Parameters.AddWithValue("@p_leaveTypeName", item.leaveTypeName);
                            oCommand.Parameters.AddWithValue("@p_firstDateDayoffFlag", item.firstDateDayoffFlag);
                            oCommand.Parameters.AddWithValue("@p_endDateDayoffFlag", item.endDateDayoffFlag);
                            oCommand.Parameters.AddWithValue("@p_nonWorkPeriodFlag", item.nonWorkPeriodFlag);
                            oCommand.Parameters.AddWithValue("@p_dayTypeFlag", item.dayTypeFlag);
                            oCommand.Parameters.AddWithValue("@p_countHoliFlag", item.countHoliFlag);
                            oCommand.Parameters.AddWithValue("@p_leaveQuotaCode", item.leaveQuotaCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMPeriodWorkSchedule(List<TMPeriodWorkSchedule> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_PeriodWorkSchedule", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMPeriodWorkSchedule item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_PeriodWorkSchedule", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_periodWorkSchCode", item.periodWorkSchCode);
                            oCommand.Parameters.AddWithValue("@p_periodWorkSchName", item.periodWorkSchName);
                            oCommand.Parameters.AddWithValue("@p_week", item.week);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode1", item.dayWorkSchCode1);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode2", item.dayWorkSchCode2);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode3", item.dayWorkSchCode3);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode4", item.dayWorkSchCode4);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode5", item.dayWorkSchCode5);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode6", item.dayWorkSchCode6);
                            oCommand.Parameters.AddWithValue("@p_dayWorkSchCode7", item.dayWorkSchCode7);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMPersonalArea(List<TMPersonalArea> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_PersonalArea", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMPersonalArea item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_PersonalArea", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_psnArea", item.psnArea);
                            oCommand.Parameters.AddWithValue("@p_psnSubArea", item.psnSubArea);
                            oCommand.Parameters.AddWithValue("@p_psnAreaGrouping", item.psnAreaGrouping);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMSetupHoliday(List<TMSetupHoliday> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_SetupHoliday", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMSetupHoliday item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_SetupHoliday", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_holidayCode", item.holidayCode);
                            oCommand.Parameters.AddWithValue("@p_holidayName", item.holidayName);
                            oCommand.Parameters.AddWithValue("@p_holidayNameEN", item.holidayNameEN);
                            oCommand.Parameters.AddWithValue("@p_year", item.year);
                            oCommand.Parameters.AddWithValue("@p_date", DateTimeService.ConvertDateTime(CompanyCode, item.date));
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMSubsititutionType(List<TMSubsititutionType> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_SubsititutionType", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMSubsititutionType item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_SubsititutionType", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_subsTypeCode", item.subsTypeCode);
                            oCommand.Parameters.AddWithValue("@p_subsTypeName", item.subsTypeName);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMTimeRecType(List<TMTimeRecType> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_TimeRecType", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMTimeRecType item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_TimeRecType", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_timeRecTypeCode", item.timeRecTypeCode);
                            oCommand.Parameters.AddWithValue("@p_timeRecTypeName", item.timeRecTypeName);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMWorkingType(List<TMWorkingType> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_WorkingType", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMWorkingType item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_WorkingType", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_workingTypeCode", item.workingTypeCode);
                            oCommand.Parameters.AddWithValue("@p_workingTypeName", item.workingTypeName);
                            oCommand.Parameters.AddWithValue("@p_minDuration",item.minDuration);
                            oCommand.Parameters.AddWithValue("@p_maxDuration",item.maxDuration);
                            oCommand.Parameters.AddWithValue("@p_dayTypeFlag", item.dayTypeFlag);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMWorkSchedule(List<TMWorkSchedule> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_WorkSchedule", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMWorkSchedule item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_WorkSchedule", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_psnAreaGrouping", item.psnAreaGrouping);
                            oCommand.Parameters.AddWithValue("@p_empGrpGrouping", item.empGrpGrouping);
                            oCommand.Parameters.AddWithValue("@p_workSchCode", item.workSchCode);
                            oCommand.Parameters.AddWithValue("@p_holiCalenCode", item.holiCalenCode);
                            oCommand.Parameters.AddWithValue("@p_year", item.year);
                            oCommand.Parameters.AddWithValue("@p_month", item.month);
                            oCommand.Parameters.AddWithValue("@p_day01", item.day01);
                            oCommand.Parameters.AddWithValue("@p_day02", item.day02);
                            oCommand.Parameters.AddWithValue("@p_day03", item.day03);
                            oCommand.Parameters.AddWithValue("@p_day04", item.day04);
                            oCommand.Parameters.AddWithValue("@p_day05", item.day05);
                            oCommand.Parameters.AddWithValue("@p_day06", item.day06);
                            oCommand.Parameters.AddWithValue("@p_day07", item.day07);
                            oCommand.Parameters.AddWithValue("@p_day08", item.day08);
                            oCommand.Parameters.AddWithValue("@p_day09", item.day09);
                            oCommand.Parameters.AddWithValue("@p_day10", item.day10);
                            oCommand.Parameters.AddWithValue("@p_day11", item.day11);
                            oCommand.Parameters.AddWithValue("@p_day12", item.day12);
                            oCommand.Parameters.AddWithValue("@p_day13", item.day13);
                            oCommand.Parameters.AddWithValue("@p_day14", item.day14);
                            oCommand.Parameters.AddWithValue("@p_day15", item.day15);
                            oCommand.Parameters.AddWithValue("@p_day16", item.day16);
                            oCommand.Parameters.AddWithValue("@p_day17", item.day17);
                            oCommand.Parameters.AddWithValue("@p_day18", item.day18);
                            oCommand.Parameters.AddWithValue("@p_day19", item.day19);
                            oCommand.Parameters.AddWithValue("@p_day20", item.day20);
                            oCommand.Parameters.AddWithValue("@p_day21", item.day21);
                            oCommand.Parameters.AddWithValue("@p_day22", item.day22);
                            oCommand.Parameters.AddWithValue("@p_day23", item.day23);
                            oCommand.Parameters.AddWithValue("@p_day24", item.day24);
                            oCommand.Parameters.AddWithValue("@p_day25", item.day25);
                            oCommand.Parameters.AddWithValue("@p_day26", item.day26);
                            oCommand.Parameters.AddWithValue("@p_day27", item.day27);
                            oCommand.Parameters.AddWithValue("@p_day28", item.day28);
                            oCommand.Parameters.AddWithValue("@p_day29", item.day29);
                            oCommand.Parameters.AddWithValue("@p_day30", item.day30);
                            oCommand.Parameters.AddWithValue("@p_day31", item.day31);

                            oCommand.Parameters.AddWithValue("@p_day01_H", item.day01_H);
                            oCommand.Parameters.AddWithValue("@p_day02_H", item.day02_H);
                            oCommand.Parameters.AddWithValue("@p_day03_H", item.day03_H);
                            oCommand.Parameters.AddWithValue("@p_day04_H", item.day04_H);
                            oCommand.Parameters.AddWithValue("@p_day05_H", item.day05_H);
                            oCommand.Parameters.AddWithValue("@p_day06_H", item.day06_H);
                            oCommand.Parameters.AddWithValue("@p_day07_H", item.day07_H);
                            oCommand.Parameters.AddWithValue("@p_day08_H", item.day08_H);
                            oCommand.Parameters.AddWithValue("@p_day09_H", item.day09_H);
                            oCommand.Parameters.AddWithValue("@p_day10_H", item.day10_H);
                            oCommand.Parameters.AddWithValue("@p_day11_H", item.day11_H);
                            oCommand.Parameters.AddWithValue("@p_day12_H", item.day12_H);
                            oCommand.Parameters.AddWithValue("@p_day13_H", item.day13_H);
                            oCommand.Parameters.AddWithValue("@p_day14_H", item.day14_H);
                            oCommand.Parameters.AddWithValue("@p_day15_H", item.day15_H);
                            oCommand.Parameters.AddWithValue("@p_day16_H", item.day16_H);
                            oCommand.Parameters.AddWithValue("@p_day17_H", item.day17_H);
                            oCommand.Parameters.AddWithValue("@p_day18_H", item.day18_H);
                            oCommand.Parameters.AddWithValue("@p_day19_H", item.day19_H);
                            oCommand.Parameters.AddWithValue("@p_day20_H", item.day20_H);
                            oCommand.Parameters.AddWithValue("@p_day21_H", item.day21_H);
                            oCommand.Parameters.AddWithValue("@p_day22_H", item.day22_H);
                            oCommand.Parameters.AddWithValue("@p_day23_H", item.day23_H);
                            oCommand.Parameters.AddWithValue("@p_day24_H", item.day24_H);
                            oCommand.Parameters.AddWithValue("@p_day25_H", item.day25_H);
                            oCommand.Parameters.AddWithValue("@p_day26_H", item.day26_H);
                            oCommand.Parameters.AddWithValue("@p_day27_H", item.day27_H);
                            oCommand.Parameters.AddWithValue("@p_day28_H", item.day28_H);
                            oCommand.Parameters.AddWithValue("@p_day29_H", item.day29_H);
                            oCommand.Parameters.AddWithValue("@p_day30_H", item.day30_H);
                            oCommand.Parameters.AddWithValue("@p_day31_H", item.day31_H);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMWorkScheduleRule(List<TMWorkScheduleRule> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_WorkScheduleRule", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMWorkScheduleRule item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_WorkScheduleRule", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_psnAreaGrouping", item.psnAreaGrouping);
                            oCommand.Parameters.AddWithValue("@p_workSchCode", item.workSchCode);
                            oCommand.Parameters.AddWithValue("@p_workSchName", item.workSchName);
                            oCommand.Parameters.AddWithValue("@p_dailyWorkHour",item.dailyWorkHour);
                            oCommand.Parameters.AddWithValue("@p_weekWorkHour",item.weekWorkHour);
                            oCommand.Parameters.AddWithValue("@p_weekWorkDay",item.weekWorkDay);
                            oCommand.Parameters.AddWithValue("@p_monthWorkHour",item.monthWorkHour);
                            oCommand.Parameters.AddWithValue("@p_pwsCode", item.pwsCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMPlanWorkTime(List<TMPlanWorkTime> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_PlanWorkTime", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMPlanWorkTime item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_PlanWorkTime", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_timeMgmtCode", item.timeMgmtCode);
                            oCommand.Parameters.AddWithValue("@p_workSchCode", item.workSchCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMLeaveInf(List<TMLeaveInf> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_LeaveInf", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMLeaveInf item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_LeaveInf", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                            oCommand.Parameters.AddWithValue("@p_leaveTypeCode", item.leaveTypeCode);
                            oCommand.Parameters.AddWithValue("@p_leaveStartTime", item.leaveStartTime);
                            oCommand.Parameters.AddWithValue("@p_leaveEndTime", item.leaveEndTime);
                            oCommand.Parameters.AddWithValue("@p_leaveHour", item.leaveHour);
                            oCommand.Parameters.AddWithValue("@p_leaveDay", item.leaveDay);
                            oCommand.Parameters.AddWithValue("@p_leaveDayCalen", item.leaveDayCalen);
                            oCommand.Parameters.AddWithValue("@p_useQuota", item.useQuota);
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public string SaveAllTMTimeAttend(List<TMTimeAttend> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_TM_Delete_TimeAttend", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (TMTimeAttend item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_TM_Save_TimeAttend", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_empCode", item.empCode);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
                            oCommand.Parameters.AddWithValue("@p_workingTypeCode", item.workingTypeCode);
                            oCommand.Parameters.AddWithValue("@p_timeStart", item.timeStart);
                            oCommand.Parameters.AddWithValue("@p_endTime", item.endTime);
                            oCommand.Parameters.AddWithValue("@p_hour", item.hour);
                            oCommand.Parameters.AddWithValue("@p_day", item.day);
                            oCommand.Parameters.AddWithValue("@p_prevDay", item.prevDay);
                            oCommand.Parameters.AddWithValue("@p_fullDay", item.fullDay);
                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }
    }
}
