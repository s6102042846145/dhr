﻿using DHR.HR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.API.DB
{
    public interface IPayrollDataService
    {
        #region JOB  
        string SaveAllBankMaster(List<PYBankMaster> data);
        #endregion
    }
}
