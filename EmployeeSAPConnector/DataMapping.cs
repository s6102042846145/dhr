using System.Collections.Generic;

namespace ESS.EMPLOYEE.SAP
{
    public class DataMapping
    {
        private static Dictionary<string, string> EMPGROUP_MAPPING = new Dictionary<string, string>();
        private static Dictionary<string, string> EMPSUBGROUP_MAPPING = new Dictionary<string, string>();

        static DataMapping()
        {
            EMPGROUP_MAPPING["A"] = "0";
            EMPGROUP_MAPPING["B"] = "1";
            EMPGROUP_MAPPING["C"] = "2";
            EMPGROUP_MAPPING["D"] = "3";
            EMPGROUP_MAPPING["E"] = "4";
            EMPGROUP_MAPPING["F"] = "5";
            EMPGROUP_MAPPING["G"] = "6";
            EMPGROUP_MAPPING["H"] = "7";
            EMPGROUP_MAPPING["I"] = "8";
            EMPGROUP_MAPPING["J"] = "9";
            EMPGROUP_MAPPING["K"] = "10";
            EMPGROUP_MAPPING["L"] = "11";

            EMPSUBGROUP_MAPPING["Z0"] = "0";
            EMPSUBGROUP_MAPPING["Z8"] = "1";
            EMPSUBGROUP_MAPPING["Z9"] = "2";
            EMPSUBGROUP_MAPPING["ZA"] = "3";
            EMPSUBGROUP_MAPPING["ZB"] = "4";
            EMPSUBGROUP_MAPPING["ZC"] = "5";
            EMPSUBGROUP_MAPPING["ZD"] = "6";
            EMPSUBGROUP_MAPPING["ZE"] = "7";
            EMPSUBGROUP_MAPPING["ZF"] = "8";
            EMPSUBGROUP_MAPPING["ZG"] = "9";
            EMPSUBGROUP_MAPPING["ZH"] = "10";
            EMPSUBGROUP_MAPPING["ZI"] = "11";
            EMPSUBGROUP_MAPPING["ZJ"] = "12";
            EMPSUBGROUP_MAPPING["ZK"] = "13";
            EMPSUBGROUP_MAPPING["ZL"] = "14";
            EMPSUBGROUP_MAPPING["ZM"] = "15";
            EMPSUBGROUP_MAPPING["ZN"] = "16";
            EMPSUBGROUP_MAPPING["ZO"] = "17";
            EMPSUBGROUP_MAPPING["ZP"] = "18";
            EMPSUBGROUP_MAPPING["ZR"] = "0";
            //EMPSUBGROUP_MAPPING["ZS"] = "-1";
            EMPSUBGROUP_MAPPING["ZT"] = "0";
            EMPSUBGROUP_MAPPING["ZX"] = "19";
            EMPSUBGROUP_MAPPING["ZZ"] = "20";
        }

        public static string LookupEmpGroup(string sapEmpGroup)
        {
            return EMPGROUP_MAPPING[sapEmpGroup.Trim().ToUpper()];
        }

        public static string LookupEmpGroup(string sapEmpGroup, string defaultValue)
        {
            string returnValue;
            try
            {
                returnValue = LookupEmpGroup(sapEmpGroup);
            }
            catch
            {
                if (defaultValue == "$LookupKey")
                {
                    returnValue = sapEmpGroup;
                }
                else
                {
                    returnValue = defaultValue;
                }
                System.Console.WriteLine("Could not map EmpGroup {0} to inteter value. return {1}", sapEmpGroup, returnValue);
            }
            return returnValue;
        }

        public static string LookupEmpSubGroup(string sapEmpSubGroup)
        {
            return EMPSUBGROUP_MAPPING[sapEmpSubGroup.Trim().ToUpper()];
        }

        public static string LookupEmpSubGroup(string sapEmpSubGroup, string defaultValue)
        {
            string returnValue;
            try
            {
                returnValue = LookupEmpSubGroup(sapEmpSubGroup);
            }
            catch
            {
                if (defaultValue == "$LookupKey")
                {
                    returnValue = sapEmpSubGroup;
                }
                else
                {
                    returnValue = defaultValue;
                }
                System.Console.WriteLine("Could not map EmpSubGroup {0} to integer value. return {1}", sapEmpSubGroup, returnValue);
            }
            return returnValue;
        }
    }
}