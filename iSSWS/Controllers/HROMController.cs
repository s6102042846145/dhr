﻿using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.HR.PA;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.PORTALENGINE;
using ESS.UTILITY.CONVERT;
using ESS.WORKFLOW;
using iSSWS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using QRCoder;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.FILE;
using System.Web;
using ESS.HR;
using Newtonsoft.Json.Linq;
using ESS.HR.OM;
using ESS.HR.OM.DATACLASS;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]


    public class HROMController : ApiController
    {
        private readonly CultureInfo thCL = new CultureInfo("th-TH");
        private readonly CultureInfo enCL = new CultureInfo("en-US");
        #region  Get DDL Search content

        [HttpPost]
        public object GetOrgLevelDDL([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            List<ControlDropdownlistData> oReturn = new List<ControlDropdownlistData>();
            oReturn = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetOrgLevelDDL(oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);
            return oReturn;
        }

        [HttpPost]
        public object GetOrgUnitDDL([FromBody] RequestParameter oRequestParameter)
        {
            string LevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["LevelSelected"].ToString()) ? oRequestParameter.InputParameter["LevelSelected"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetOrgUnitDDL(LevelSelected, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetPositionDDL([FromBody] RequestParameter oRequestParameter)
        {
            string UnitSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitSelected"].ToString()) ? oRequestParameter.InputParameter["UnitSelected"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionDDL(UnitSelected, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetCostCenterDDL([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCostCenterDDL(oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetHeadOfOrgUnitDDL([FromBody] RequestParameter oRequestParameter)
        {
            string oOrgLevel = !string.IsNullOrEmpty(oRequestParameter.InputParameter["OrgLevel"].ToString()) ? oRequestParameter.InputParameter["OrgLevel"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetHeadUnitDDL(oOrgLevel, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetBandDDL([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBandDDL(oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }
        #endregion

        #region Get Data Content
        [HttpPost]
        public object GetPositionDataContent([FromBody] RequestParameter oRequestParameter)
        {
            List<PositionCenter> oData = new List<PositionCenter>();
            int requestTypeID_1201 = 1201;
            int requestTypeID_1202 = 1202;
            int requestTypeID_1203 = 1203;
            int requestTypeID_1204 = 1204;
            int requestTypeID_1205 = 1205;

            DataTable oCreateDocEdit = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID_1201, oRequestParameter.CurrentEmployee.Language);
            DataTable oCreateDocDelete = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID_1202, oRequestParameter.CurrentEmployee.Language);
            DataTable oCreateDocPerDelete = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID_1203, oRequestParameter.CurrentEmployee.Language);
            DataTable oCreateDocInactive = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID_1204, oRequestParameter.CurrentEmployee.Language);
            DataTable oCreateDocNew = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID_1205, oRequestParameter.CurrentEmployee.Language);

            string LevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["LevelSelected"].ToString()) ? oRequestParameter.InputParameter["LevelSelected"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string UnitSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitSelected"].ToString()) ? oRequestParameter.InputParameter["UnitSelected"].ToString() : string.Empty;
            string PositionSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PositionSelected"].ToString()) ? oRequestParameter.InputParameter["PositionSelected"].ToString() : string.Empty;

            oData = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionDataContent(BeginDate, EndDate, LevelSelected, UnitSelected, PositionSelected);

            object oReturn = new
            {
                oData,
                oCreateDocEdit,
                oCreateDocDelete,
                oCreateDocPerDelete,
                oCreateDocInactive,
                oCreateDocNew
            };
            return oReturn;
        }

        [HttpPost]
        public object GetPositionDotedDataContent([FromBody] RequestParameter oRequestParameter)
        {
            string UnitSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitSelected"].ToString()) ? oRequestParameter.InputParameter["UnitSelected"].ToString() : string.Empty;
            string PositionSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PositionSelected"].ToString()) ? oRequestParameter.InputParameter["PositionSelected"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

           var oData = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionDataContent(BeginDate, EndDate,string.Empty,UnitSelected,PositionSelected);
            object oReturn = new
            {
                oData
            };
            return oReturn;
        }

        [HttpPost]
        public object GetPositionDataDetail([FromBody] RequestParameter oRequestParameter)
        {
            string UnitCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitCode"].ToString()) ? oRequestParameter.InputParameter["UnitCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oData = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionDataDetail(UnitCode, oRequestParameter.Requestor.Language, BeginDate, EndDate);
            object oReturn = new
            {
                oData
            };
            return oReturn;

        }

        [HttpPost]
        public object GetPositionUnderSupervisorDDL([FromBody] RequestParameter oRequestParameter)
        {
            string HeadPosCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PosCode"].ToString()) ? oRequestParameter.InputParameter["PosCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionUnderSupervisorDDL(HeadPosCode, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);
            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetPositionUnderSupervisorNewDDL([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionUnderSupervisorNewDDL(string.Empty, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate);
            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetPositionDotedDetail([FromBody] RequestParameter oRequestParameter)
        {
            string HeadPosCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["HeadPosCode"].ToString()) ? oRequestParameter.InputParameter["HeadPosCode"].ToString() : string.Empty;
            string PosCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PosCode"].ToString()) ? oRequestParameter.InputParameter["PosCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oData = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionDotedDetail(HeadPosCode, PosCode, oRequestParameter.Requestor.Language, BeginDate, EndDate);
            oData = oData.OrderBy(x => x.StartDate).ThenByDescending(x => x.EndDate).ToList();
            object oReturn = new
            {
                oData
            };
            return oReturn;

        }

        //[HttpPost]
        //public object GetPositonDataByPositionCode([FromBody] RequestParameter oRequestParameter)
        //{
        //    string PositionSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PositionSelected"].ToString()) ? oRequestParameter.InputParameter["PositionSelected"].ToString() : string.Empty;
        //    DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
        //    DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

        //    var oData = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionByPosCode(PositionSelected, BeginDate, EndDate);

        //    object oReturn = new
        //    {
        //        oData
        //    };
        //    return oReturn;

        //}

        [HttpPost]
        public List<OrganizationCenter> GetOrganizationDataContent([FromBody] RequestParameter oRequestParameter)
        {
            List<OrganizationCenter> oReturn = new List<OrganizationCenter>();
            string LevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["LevelSelected"].ToString()) ? oRequestParameter.InputParameter["LevelSelected"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string UnitSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitSelected"].ToString()) ? oRequestParameter.InputParameter["UnitSelected"].ToString() : string.Empty;

            oReturn = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetOrganizationDataContent(LevelSelected, UnitSelected, BeginDate, EndDate);


            return oReturn;
        }
        #endregion

        #region Get Org Level
        [HttpPost]
        public object GetOrganizationLevelUnit([FromBody] RequestParameter oRequestParameter)
        {

            string OrgType = oRequestParameter.InputParameter["OrgType"].ToString();
            string AsOfDate = oRequestParameter.InputParameter["AsOfDate"].ToString();
            // DataSet PAConfig = HRPAManagement.CreateInstance(CompanyCode).GetPAConfigurationListByCategoryName(CategoryName);
            var Gender = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllGender("TH");
            DataTable ConfigData = new DataTable("Config");
            // ConfigData = PAConfig.Tables[0];

            object DataConfig = new
            {
                Gender
            };

            return Gender;
        }
        #endregion

        [HttpPost]
        public List<string> getOrganizationLevelChart([FromBody] RequestParameter oRequestParameter)
        {
            string oOrgID = String.IsNullOrEmpty(oRequestParameter.InputParameter["UNIT_ID"].ToString()) ? "" : oRequestParameter.InputParameter["UNIT_ID"].ToString(),
                oLevelCode = String.IsNullOrEmpty(oRequestParameter.InputParameter["LEVEL_ID"].ToString()) ? "" : oRequestParameter.InputParameter["LEVEL_ID"].ToString(),
                oHeadOrg = String.IsNullOrEmpty(oRequestParameter.InputParameter["HEADORG_ID"].ToString()) ? "" : oRequestParameter.InputParameter["HEADORG_ID"].ToString(),
                oCompanyCode = oRequestParameter.Requestor.CompanyCode,
                oLanguage = oRequestParameter.Requestor.Language;
            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<string> oOrgChartByHeadID = HROMManagement.CreateInstance(oCompanyCode).getOrganizationChartByHeadOfOrg(oOrgID, oLevelCode, oHeadOrg, oBeginDate, oEndDate, oLanguage);

            return oOrgChartByHeadID;
        }

        [HttpPost]
        public List<OrganizationCenter> getOrganizationByID([FromBody] RequestParameter oRequestParameter)
        {
            string oOrgID = oRequestParameter.InputParameter["ORG_ID"].ToString();
            string oLang = oRequestParameter.InputParameter["LANGUAGE"].ToString();
            string oCompany = oRequestParameter.Requestor.CompanyCode;

            List<OrganizationCenter> oReturn = new List<OrganizationCenter>();

            try
            {
                oReturn = HROMManagement.CreateInstance(oCompany).getOrganizationByOrgID(oOrgID, oLang);
            }
            catch (Exception ex)
            {

            }

            return oReturn;
        }

        [HttpPost]
        public DataTable getPriorityListByOrganizationID([FromBody] RequestParameter oRequestParameter)
        {
            string oHeadOrg = oRequestParameter.InputParameter["ORG_ID"].ToString();
            string oLang = oRequestParameter.CurrentEmployee.Language;
            string oCompany = oRequestParameter.Requestor.CompanyCode;
            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null),
             oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            DataTable oReturn = new DataTable();
            try
            {
                oReturn = HROMManagement.CreateInstance(oCompany).getPriorityListByOrganizationID(oHeadOrg, oBeginDate, oEndDate, oLang);
            }
            catch (Exception ex)
            {

            }

            return oReturn;
        }

        [HttpPost]
        public string getExampleOMUnitID([FromBody] RequestParameter oRequestParameter)
        {
            OrganizationCenter oData = JsonConvert.DeserializeObject<OrganizationCenter>(oRequestParameter.InputParameter["organization"].ToString());
            string oReturn = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).getExampleOMUnitID(oData, "I","V");
            return oReturn;
        }


        #region Get SolidLine
        [HttpPost]
        public object GetSolidLine([FromBody] RequestParameter oRequestParameter)
        {
            //var oSolidLineLists = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetSolidLine();
            //object oReturn = new
            //{
            //    oSolidLineLists

            //};
            return null;
        }
        #endregion

        #region Get DotedLine
        [HttpPost]
        public object GetDottedLine([FromBody] RequestParameter oRequestParameter)
        {
            //var oSolidLineLists = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDottedLine();
            //object oReturn = new
            //{
            //    oSolidLineLists

            //};
            return null;
        }
        #endregion

        #region MarkData
        [HttpPost]
        public List<MarkData> GetMarkDataAll([FromBody] RequestParameter oRequestParameter)
        {
            var markDataList = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetMarkDataAll();
            return markDataList;
        }

        [HttpPost]
        public List<MarkData> GetMarkDataWithRequestType([FromBody] RequestParameter oRequestParameter)
        {
            var markDataList = HROMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetMarkDataWithRequestType();
            return markDataList;
        }
        [HttpPost]
        public object GetRequestDocumentDetailByRequestNo([FromBody] RequestParameter oRequestParameter)
        {
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode.ToString(),
                oRequestNo = oRequestParameter.InputParameter["REQUESTNO"].ToString();

            DataTable oRequestDocument = HROMManagement.CreateInstance(oCompanyCode).GetRequestDocumentData(oRequestNo);

            object oReturn = new
            {
                RequestDocument = oRequestDocument,
            };
            return oReturn;
        }

        [HttpPost]
        public string getRootOrganizationUnit([FromBody] RequestParameter oRequestParameter)
        {
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode.ToString();
            string oReturn;
            try
            {
                oReturn = HROMManagement.CreateInstance(oCompanyCode).getRootOrganizationUnit();
            }
            catch
            {
                oReturn = "";
            }
            return oReturn;
        }


        [HttpPost]
        public OrganizationChief getLeaderByHeadOfOrganization([FromBody] RequestParameter oRequestParameter)
        {
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode.ToString(),
                oHeadOfOrg = oRequestParameter.InputParameter["HEAD_ORG"].ToString();
            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null),
                oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            OrganizationChief oReturn;
            try
            {
                oReturn = HROMManagement.CreateInstance(oCompanyCode).getLeaderByHeadOfOrganization(oHeadOfOrg, oBeginDate, oEndDate);
            }
            catch
            {
                oReturn = new OrganizationChief();
            }
            return oReturn;
        }
        [HttpPost]
        public bool isLastRecordOfOrganization([FromBody] RequestParameter oRequestParameter)
        {
            bool oReturn = false;
            string oOrgID = oRequestParameter.InputParameter["ORG_ID"].ToString(),
                oCompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null),
                oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            OrganizationCenter oOrgData;
            try
            {
                oOrgData = HROMManagement.CreateInstance(oCompanyCode).GetOrganizationDataContent("", oOrgID, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31)).LastOrDefault();
                if (oOrgData.BeginDate == oBeginDate && oOrgData.EndDate == oEndDate)
                    oReturn = true;
            }
            catch
            {
                //Nothing to do;
            }
            return oReturn;
        }

        #endregion

    }
}