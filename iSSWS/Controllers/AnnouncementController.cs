﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.ANNOUNCEMENT;
using iSSWS.Models;
//using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.ANNOUNCEMENT.DATACLASS;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AnnouncementController : ApiController
    {    
        
         //Nattawat S. Added 2019-08-02
        [HttpPost]
        public void AcceptAnnouncement([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            string EmployeeID = oRequestParameter.Requestor.EmployeeID;
            string announcementID = oRequestParameter.InputParameter["announcementID"].ToString();
            string statusType = oRequestParameter.InputParameter["statusType"].ToString();

            AnnouncementManagement.CreateInstance(CompanyCode).acceptAnnouncement(EmployeeID, announcementID, statusType);
        }

        [HttpPost]
        public List<AnnouncementData> getAnnouncement([FromBody] RequestParameter oRequestParameter)
        {
            return AnnouncementManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAnnoncementList();
        }
    }


}