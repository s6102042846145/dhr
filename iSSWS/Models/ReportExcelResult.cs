﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class ReportExcelResult
    {
        public int Code { get; set; }
        public string ErrorMessage { get; set; }
        public string PathDownload { get; set; }
    }
}