﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE.DATACLASS;

namespace iSSWS.Models
{
    public class EmployeeDataForMobile 
    {
        #region Constructor
        public EmployeeDataForMobile()
        {
        }
        #endregion

        #region Member

        public string EmployeeID { get; set; }
        public string PositionID { get; set; }

        public bool IsExternalUser { get; set; }

        public string Name { get; set; }
        private string oCompanyCode = string.Empty;
        public string CompanyCode
        {
            get
            {
                return oCompanyCode.Trim().PadLeft(4, '0');
            }
            set
            {
                oCompanyCode = value.Trim().PadLeft(4, '0');
            }
        }
        public Company CompanyDetail { get; set; }
        public string ImageUrl { get; set; }
        public string Area { get; set; }
        public PersonalArea AreaSetting { get; set; }
        public string SubArea { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public string OrgUnit { get; set; }
        public string OrgUnitName { get; set; }
        public string Position { get; set; }

        public string AdminGroup { get; set; }
        public string CostCenter { get; set; }
        public string Language { get; set; }
        public bool ReceiveMail { get; set; }
        public string RequesterEmployeeID { get; set; }
        public string RequesterPositionID { get; set; }
        public string RequesterCompanyCode { get; set; } //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
        public string EmailAddress { get; set; }
        public string OfficeTelephoneNo { get; set; }
        public string MobileNo { get; set; }
        public string HomeTelephoneNo { get; set; }
        public bool IsViewPost { get; set; }
        public bool IsViewEmployeeLevel { get; set; }
        public List<INFOTYPE1000> AllPosition { get; set; }

        public DateTime HiringDate { get; set; }
        public DateTime RetirementDate { get; set; }
        public DateTime StartWorkingDate { get; set; }

        public List<string> UserRoles { get; set; }

        //For get API Key
        public string tokenRequests { get; set; }
        #endregion
    }
}
