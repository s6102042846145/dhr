﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace iSSWS.Models.Common
{
    public class GridTemplate
    {
        public GridTemplate()
        {
        }
        public string Title { get; set; }
        public DataTable DataTable { get; set; }
        public Dictionary<string, Button> Button { get; set; }
        public string NoData { get; set; }

    }

}