using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace iSSWS.Models
{
    public class ContentSettingForMobile 
    {
        public bool IsContent { get; set; }
        public int SubjectID { get; set; }

        public string Header { get; set; }
        public bool HaveActionInsteadOf { get; set; }

        public string ContentTemplate { get; set; }

        public string ContentParam { get; set; }

        public bool IsShowCounter { get; set; }

        public string CounterTemplate { get; set; }

        public EmployeeDataForMobile Requestor { get; set; }

        public EmployeeDataForMobile Creator { get; set; }

        public List<EmployeeInResponse> ManualEmployeeList { get; set; }
    }

    public class EmployeeInResponse : AbstractObject
    {
        public bool IsHeader { get; set; }
        public string OrgUnit { get; set; }
        public string OrgName { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string PositionID { get; set; }
        public string PositionName { get; set; }
        public string CompanyCode { get; set; }
        public string RedirectURL { get; set; }
        
    }
}
