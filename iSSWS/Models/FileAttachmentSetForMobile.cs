﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSSWS.Models
{
    public class FileAttachmentSetForMobile
    {
        public FileAttachmentSetForMobile()
        {
        }
        public int FileSetID { get; set; }
        public int RequestID { get; set; }
        public string RequestNo { get; set; }
        public string CompanyCode { get; set; }

        public List<FileAttachmentForMobile> Attachments { get; set; }
    }
}
