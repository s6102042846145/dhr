using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public delegate void MenuSelectedEventHandler(object sender, MenuSelectedEventArgs e);
public class MenuSelectedEventArgs : EventArgs
{
    private string __code;
    public MenuSelectedEventArgs()
    {
    }
    public MenuSelectedEventArgs(string Code)
    {
        __code = Code;
    }
    public string Code
    {
        get
        {
            return __code;
        }
        set
        {
            __code = value;
        }
    }
}
