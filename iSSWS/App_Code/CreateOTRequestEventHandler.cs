using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public delegate void CreateOTRequestEventHandler(object sender, CreateOTRequestEventArgs e);
public class CreateOTRequestEventArgs : EventArgs
{
    private OTRequestType __requestType;
    public CreateOTRequestEventArgs(OTRequestType requestType)
    {
        __requestType = requestType;
    }

    public OTRequestType OTRequestType
    {
        get
        {
            return __requestType;
        }
    }

}
