﻿using ESS.EMPLOYEE;
using ESS.JOB;
using ESS.JOB.DATACLASS;
using ESS.JOB.INTERFACE;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace iSSWS.Client
{
    public partial class runjobs : System.Web.UI.Page
    {
        private static IJobWorker CreateWorker(string CompanyCode, JobDetail oJobDetail)
        {
            IJobWorker worker = null;
            Type oReturn = null;
            Assembly oAssembly;
            oAssembly = Assembly.Load(oJobDetail.JobType.AssemblyName);
            if (oAssembly != null)
            {
                oReturn = oAssembly.GetType(oJobDetail.JobType.ClassName);
                if (oReturn != null)
                {
                    worker = (IJobWorker)Activator.CreateInstance(oReturn);
                }
            }

            worker.Params = JobManagement.CreateInstance(CompanyCode).GetJobParam(oJobDetail.JobID);
            worker.CompanyCode = CompanyCode;

            return worker;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = (string)this.Request["id"];
            string CompanyCode = (string)this.Request["cc"];

            JobDetail oJobDetail = JobManagement.CreateInstance(CompanyCode).GetJob(int.Parse(id));
            IJobWorker worker = CreateWorker(CompanyCode, oJobDetail);
            System.IO.TextWriter save = Console.Out;
            Console.SetOut(Response.Output);

            try
            {
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(int.Parse(id), int.Parse(id), "Start", string.Empty);
                Response.Write("Job " + oJobDetail.JobID + "<br/><br/>");
                List<ITaskWorker> taskList = worker.LoadTasks();
                foreach (ITaskWorker task in taskList)
                {
                    Response.Write("Task " + task.TaskID + "<br/><pre>");
                    task.TaskID = !string.IsNullOrEmpty(id) ? Convert.ToInt32(id) : 0; //JobID
                    task.CompanyCode = CompanyCode;
                    task.Run();
                    Response.Write("</pre>Run task complete.<br/><br/>");
                }
                Response.Write("Run job complete.<br/>");
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(int.Parse(id), int.Parse(id), string.Format("End {0} Task", taskList.Count), string.Empty);
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(int.Parse(id), int.Parse(id), "Error:" + ex.Message, string.Empty);
            }

            Console.SetOut(save);
        }

    }
}