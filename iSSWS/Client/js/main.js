$(document).ready(function () {

    /* custom scroll theme */
    //$(window).load(function() {
    //    if (!mobileandtabletcheck()) {
    //        $("body,.forscroll").mcustomscrollbar({
    //            theme: "minimal-dark",
    //            mousewheelpixels: 400,
    //            updateoncontentresize: true,
    //            advanced: {
    //                autoupdatetimeout: 10,
    //            }
    //            //axis:"yx",
    //            //scrollinertia: 500,
    //        });
    //        $(".forscroll-x").mcustomscrollbar({
    //            theme: "minimal-dark",
    //            mousewheelpixels: 400,
    //            axis: "yx",
    //            //scrollinertia: 500,
    //        });
    //    }
    //    //$("body.loginpagebody").mcustomscrollbar("destroy");
    //});

    //window.mobileandtabletcheck = function () {
    //    var check = false;
    //    (function (a) {
    //        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
    //    })(navigator.useragent || navigator.vendor || window.opera);
    //    return check;
    //};

    /* dropdown changing text (bootstrap)*/
    $(".dropdown-menu").on('click', 'li a', function () {
        $(".btnd:first-child").text($(this).text());
        $(".btnd:first-child").append("<span class='icon-arrow_line_down caret'></span>");
        $(".btnd:first-child").val($(this).text());

        $("a.dropdown-toggle.btnd:first-child").text($(this).text());
        $("a.dropdown-toggle.btnd:first-child").append("<span class='caret'></span>");
        $("a.dropdown-toggle.btnd:first-child").val($(this).text());
    });

    $(".contentBody .dropdown-menu").on('click', 'li a', function () {
        $(".btnd:first-child").text($(this).text());
        $(".btnd:first-child").append("<span class='caret'></span>");
        $(".btnd:first-child").val($(this).text());

        $("a.dropdown-toggle.btnd:first-child").text($(this).text());
        $("a.dropdown-toggle.btnd:first-child").append("<span class='caret'></span>");
        $("a.dropdown-toggle.btnd:first-child").val($(this).text());
    });

    /* Set Height for .forheight */
    //var window_height;
    //var forheight_height;
    //var bar_widthtitle;
    //var content_notscroll;
    //bar_widthtitle = ($('.contentHead .bar_title').width());
    //content_notscroll = ($('.content_notscroll').height());
    //setRowHeight(content_notscroll, bar_widthtitle + 10);

    //$(window).resize(function() {
    //    bar_widthtitle = ($('.contentHead .bar_title').width());
    //    content_notscroll = ($('.content_notscroll').height());
    //    setRowHeight(content_notscroll, bar_widthtitle);
    //});

    //function setRowHeight(content_notscroll, bar_width) {
    //    window_height = window.innerHeight;
    //    forheight_height = ((window_height) - (content_notscroll));
    //    $('.forheight').css('height', forheight_height - 2);
    //    $('.forheight_sub').css('height', forheight_height - 15);
    //    $('.longPage .forheight_sub').css('height', forheight_height + 28);
    //    $('.nav-panel_list').css('height', window_height);
    //    $('.mainBlock .contentPagination').css('width', bar_width);
    //}

    /* toggle bar_search */
    //var flagsearch = 1;
    $('body').on('click', ".forsearch", function () {
        $(".bar_search").toggleClass("bar_searchactive");
        $(".forsearch").toggleClass("forsearch_active");
        $(".forsearch").find("span").toggleClass("faa-shake animated");

        //bar_widthtitle = ($('.contentHead .bar_title').width());
        //setTimeout(function() {
        //    content_notscroll = ($('.content_notscroll').height());
        //    setRowHeight((content_notscroll), bar_widthtitle);
        //}, 150);
        /*
        setTimeout(function() {
            if (flagsearch == 1) {
                setRowHeight((bar_heighttitle + bar_heightsearch), bar_widthtitle);
                flagsearch = 0;
            } else {
                setRowHeight(bar_height);
                flagsearch = 1;
            } 
        */
    });

    $('body').on('click', ".high-lvl-search", function () {
        $(".search-visible-xs").css("display", "none");
        $(".search-hide-xs").css("display", "block");
        /*bar_widthtitle = ($('.contentHead .bar_title').width());
        setTimeout(function() {
            content_notscroll = ($('.content_notscroll').height());
            setRowHeight((content_notscroll), bar_widthtitle);
        }, 150);
        
        setTimeout(function() {
            if (flagsearch == 1) {
                setRowHeight((bar_heighttitle + bar_heightsearch), bar_widthtitle);
                flagsearch = 0;
            } else {
                setRowHeight(bar_height);
                flagsearch = 1;
            } 
        */
    });

    /* toggle navbar */
    $('body').on('click', ".profilebar-hamburger", function () {
        $("#menuSection").toggleClass("navActive");
        $("#MenuButtonIcon").toggleClass("eat");
        //$("#MenuButtonIcon").toggleClass("faa-horizontal animated");
        //faa-horizontal animated
        //$(".mainBlock,.contentBody").toggleClass("eventnone");


    });
    /* toggle navProfile */
    //var flagcaret = 1;
    //$('body').on('click', '.profilebar-id a', function() {
    //    $(".mainNav .navBlock .navProfile").toggleClass("navProfileActive");


    //    if (flagcaret == true) {
    //        $(".profilebar-id").find("span.caret").css({ "transform": "rotate(180deg)" });
    //        setTimeout(function() {
    //            $(".profilebar-img").css({ "margin-top": "50%" });
    //            $(".profilebar_img-small img").css({ "top": "300%" });
    //            //$('.profilebar_img-small img').hide(1000);
    //        }, 150);
    //        flagcaret = 0;
    //    } else {
    //        $(".profilebar-id").find("span.caret").css({ "transform": "rotate(0deg)" });
    //        $(".profilebar-img").css({ "margin-top": "0" });
    //        $(".profilebar_img-small img").css({ "top": "0" });
    //        flagcaret = 1;
    //    }


    //});

    /* toggle Task */
    //$(".task_inner").hide();
    //$("body").on('click', '.task_titlehead', function() {
    //    $(".taskBlock").removeClass("taskactive");
    //    //$(".taskBlock").find(".task_inner").hide();
    //    $(this).parent().find(".task_inner").toggle();
    //    $(this).parent(".taskBlock").toggleClass("taskactive");
    //});
    /* toggle Task highlight */
    //$("body").on('click', '.task_inner ul', function () {
    //    $(".task_inner").children("ul").removeClass("-task-highlight");
    //    $(this).addClass("-task-highlight");
    //});
    /* toggle Menu */
    //$("body").on('click', '.task_inner a', function () {
    //    $(".taskBlock").removeClass("taskactive");
    //    $(this).parent(".taskBlock").toggleClass("taskactive");

    //    $(".taskBlock").toggleClass("taskactive");

    //    $('#main-menu').find(".taskactive").find('a').removeClass("-task-highlight");
    //    $(this).addClass("-task-highlight");
    //});

    /* datepicker */
    //$('input[name="leavestart"],input[name="leaveend"]').daterangepicker({
    //    locale: {
    //        format: 'DD/MM/YYYY'
    //    },
    //    singleDatePicker: true,
    //    showDropdowns: false,
    //    opens: 'left',

    //    //autoUpdateInput: false,
    //});

    //$(".ico_datestart").on("click", function() {
    //    $('input[name="leavestart"]').trigger("click");
    //})
    //$(".ico_dateend").on("click", function() {
    //    $('input[name="leaveend"]').trigger("click");
    //})

    /* toggle fyi_expand (comment) */
    //$(".fyi_expand").hide();
    $('body').on('click', ".itemfyi-icon.icodown", function () {
        $(".fyi_expand").slideToggle(100);
        $(".itemfyi-icon.icodown").toggleClass('rotate180');
    });
    /* bar_bottom-expandDetail (document flow) */
    //$(".bar_bottom-expandDetail").hide();
    $("body").on('click', '.bar_left-icon', function () {
        var $element = $(this).parent().parent().find('.bar_bottom-expandDetail');
        var $rootParentElement = $(this).parent().parent().parent().parent();
        $element.slideToggle(100);
        $(this).find('.dummy').toggleClass('rotate180');
        if ($element.hasClass('active-bar')) {
            //$('#document-flow-last .bar_bottom-expandDetail').slideToggle(100);
            //$('#document-flow-last .bar_left-icon').find('.dummy').toggleClass('rotate180');

            $rootParentElement.find('.document-flow-last .bar_bottom-expandDetail').slideToggle(100);
            $rootParentElement.find('.document-flow-last .bar_left-icon').find('.dummy').toggleClass('rotate180');
        } else if ($element.hasClass('active-bar-last')) {
            //$('#document-flow-all .bar_bottom-expandDetail.active-bar').slideToggle(100);
            //$('#document-flow-all .bar_bottom-status.active-bar .bar_left-icon').find('.dummy').toggleClass('rotate180');

            $rootParentElement.find('.document-flow-all .bar_bottom-expandDetail.active-bar').slideToggle(100);
            $rootParentElement.find('.document-flow-all .bar_bottom-status.active-bar .bar_left-icon').find('.dummy').toggleClass('rotate180');
        }
    });
    $("body").on('click', '.document-flow-all .bar_right-icon', function () {
        var $rootParentElement = $(this).parent().parent().parent().parent();
        $rootParentElement.find('.bar_bottom-expandlist').slideToggle(100, function () {
            $rootParentElement.find('.document-flow-last').show();
        });
        //$(this).find('.dummy').toggleClass('rotate180');
        //$('#document-flow-last').find('.dummy').toggleClass('rotate180');
    });
    $("body").on('click', '.document-flow-last .bar_right-icon', function () {
        $(this).parent().parent().hide();
        var $rootParentElement = $(this).parent().parent().parent().parent();
        $rootParentElement.find('.bar_bottom-expandlist').slideToggle(150);
        //$(this).find('.dummy').toggleClass('rotate180');
        //$('#document-flow-all').find('.dummy').toggleClass('rotate180');
    });

    /* work_calendar (date trigger) */
    //$('table.calendar_item-table tbody tr a').on('click', function() {

    //    $('table.calendar_item-table .inCircle').removeClass('-date-blue -date-black');

    //    if ($(this).parent().hasClass('day_weekend')) {
    //        $(this).find('.inCircle').addClass('-date-blue');
    //    } else {
    //        $(this).find('.inCircle').addClass('-date-black');
    //    }
    //    if ($(this).parent().hasClass('day_moredetail')) {
    //        $('.workcalendar_item.forexpand').slideDown(100);
    //    } else {
    //        $('.workcalendar_item.forexpand').slideUp(100);
    //    }

    //});

    $("body").on('click', '.itemtable-itemres .itemtxt-left a', function () {
        $(this).parentsUntil('.itemtable-itemres').find('.itemres-detail_intable').slideToggle(100);
        $(this).find('.dummy').toggleClass('rotate180');
    });



    function isNumber(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // �-� CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // �.� CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function fnToCurrency(data) {
        return $.number(data, 2, '.', ',');
    }

    $(document).on('keypress', ".numeric", function (e) {
        return isNumber(event, this);
    });

    $(document).on('blur', ".numeric", function (e) {
        var val = $(this).val();
        var newval = fnToCurrency(val);
        $(this).val(newval);
    });

    /* detail_item-resexpand toggle in Common02(res content) */
    $("body").on('click', '.common02-res_Block .tableexpand-caret a', function () {
        $(this).parent().parent().find('.detail_item-resexpand').slideToggle(100);
        $(this).parents('.common02-res_Block').toggleClass('open');
        $(this).find('.dummy').toggleClass('rotate180');
    });

    /* detail_item-tableexpand toggle in Travel */
    $("body").on('click', '.traveltableBlock .itemlist_block .tableexpand-caret a', function () {
        $(this).parentsUntil('.traveltableBlock').find('.tableexpand .detail_item-tableexpand').slideToggle(100);
        $(this).find('.dummy').toggleClass('rotate180');
    });

    /* detail_item-resexpand toggle in Travel(res content) */
    $("body").on('click', '.travel-res_Block .tableexpand-caret a', function () {
        $(this).parent().parent().find('.detail_item-resexpand').slideToggle(100);
        $(this).parents('.travel-res_Block').toggleClass('open');
        $(this).find('.dummy').toggleClass('rotate180');
    });
    /* detail_item-resexpand > list_travelsubexpand toggle in Travel(res content) */
    $("body").on('click', '.travel-res_Block .item-res_layertwo .bar_right.barright_icon', function () {
        $(this).parentsUntil('.travel-res_Block').find('.item-res_layertwo .list_travelsubexpand').slideToggle(100);
        $(this).find('.dummy').toggleClass('rotate180');
    });

    /* bar_expand-expandDetail */
    $("body .bar_expand-expandDetail").hide();
    $("body").on('click', '.bar_expandlist .bar_left-icon', function () {
        $(this).parent().parent().find('.bar_expand-expandDetail').slideToggle(100);
    });
    $("body").on('click', '.bar_expandlist .bar_right-icon', function () {
        $(this).parent().parent().find('.bar_expand-expandDetail').first().slideToggle(100);
        //$(this).find('.dummy').toggleClass('rotate180');
    });

});



var convertToDate = function (datePickerModel) {
    if (typeof datePickerModel._d != 'undefined') return datePickerModel._d;
    else return datePickerModel;
}