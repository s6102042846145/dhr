﻿(function () {
    angular.module('DESS')
        .controller('ReportLeaveController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {

            /******************* variable start ********************/

            $scope.Textcategory = "REPORT_LEAVE";
            $scope.employee = getToken(CONFIG.USER);
            var date = new Date();
            $scope.StartDate = date;
            $scope.EndDate = date;
            $scope.DateSelect = date;
            $scope.OrgSelect = '';
            $scope.sumLeave = [];
            $scope.contentLeave = [];
            $scope.treeViewData = [];

            $scope.rootOrgData = {
                id: "",
                name: "",
                level: 0
            };

            $scope.Data = {
                id: '',
                name: '',
                level: '',
                parent: '',
                checked: false
            };

            $scope.levelOrg = [];
            $scope.rootOrg = [];
            $scope.parentOrg = [];
            $scope.Node = [];
            $scope.Node1 = [];
            $scope.Node2 = [];
            $scope.Node3 = [];
            $scope.Node4 = [];
            $scope.Node5 = [];

            $scope.headerColumn = [];
            $scope.dataHeader = {
                type: '',
                id: ''
            }

            $scope.checkStartDate = function () {
                if ($scope.StartDate > $scope.EndDate) {
                    $scope.EndDate = $scope.StartDate;
                }
            }
            $scope.checkEndDate = function () {
                if ($scope.EndDate < $scope.StartDate) {
                    $scope.StartDate = $scope.EndDate;
                }
            }

            $scope.GetLeaveGroupSetting = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetLeaveGroupSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oLeaveGroupSetting = response.data;
                    //console.log('leavegroupsetting = ', $scope.oLeaveGroupSetting);
                    //split for text description
                    for (var i = 0; i < $scope.oLeaveGroupSetting.length; i++) {
                        var str = $scope.oLeaveGroupSetting[i].LeaveTypeGroup;
                        var text = str.split("_");
                        $scope.dataHeader = {
                            type: text[0],
                            id: text[1]
                        }
                        $scope.headerColumn.push($scope.dataHeader);
                    }
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            }
            $scope.GetLeaveGroupSetting();

            $scope.GetReportOrgList = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetReportOrgList';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.OrgList = response.data.OrgList;
                    //console.log('OrgList = ', $scope.OrgList);
                    //filter Level
                    var tempLevel = -1;
                    var count = 0;
                    var index = 0;
                    for (var i = 0; i < $scope.OrgList.length; i++) {
                        if ($scope.OrgList[i].OrgLevel !== tempLevel) {
                            tempLevel = $scope.OrgList[i].OrgLevel;
                            $scope.levelOrg.push(tempLevel);
                        }
                        if ($scope.OrgList[i].OrgLevel == 1) {
                            $scope.Data = {
                                id: $scope.OrgList[i].OrgUnit,
                                name: $scope.OrgList[i].OrgUnitName,
                                level: $scope.OrgList[i].OrgLevel,
                                parent: $scope.OrgList[i].OrgParent,
                                checked: false
                            };
                            $scope.Node1.push($scope.Data);
                        }
                        if ($scope.OrgList[i].OrgLevel == 2) {
                            $scope.Data = {
                                id: $scope.OrgList[i].OrgUnit,
                                name: $scope.OrgList[i].OrgUnitName,
                                level: $scope.OrgList[i].OrgLevel,
                                parent: $scope.OrgList[i].OrgParent,
                                checked: false
                            };
                            $scope.Node2.push($scope.Data);
                        } if ($scope.OrgList[i].OrgLevel == 3) {
                            $scope.Data = {
                                id: $scope.OrgList[i].OrgUnit,
                                name: $scope.OrgList[i].OrgUnitName,
                                level: $scope.OrgList[i].OrgLevel,
                                parent: $scope.OrgList[i].OrgParent,
                                checked: false
                            };
                            $scope.Node3.push($scope.Data);
                        }
                        if ($scope.OrgList[i].OrgLevel == 4) {
                            $scope.Data = {
                                id: $scope.OrgList[i].OrgUnit,
                                name: $scope.OrgList[i].OrgUnitName,
                                level: $scope.OrgList[i].OrgLevel,
                                parent: $scope.OrgList[i].OrgParent,
                                checked: false
                            };
                            $scope.Node4.push($scope.Data);
                        }
                        if ($scope.OrgList[i].OrgLevel == 5) {
                            $scope.Data = {
                                id: $scope.OrgList[i].OrgUnit,
                                name: $scope.OrgList[i].OrgUnitName,
                                level: $scope.OrgList[i].OrgLevel,
                                parent: $scope.OrgList[i].OrgParent,
                                checked: false
                            };
                            $scope.Node5.push($scope.Data);
                        }
                    }

                    //Nattawat S. Add
                    
                    $scope.OrgSorting = $scope.OrgList.sort(function (a, b) { return a.OrgLevel - b.OrgLever });
                    //var allLevel = [];
                    //var currentLevel = $scope.OrgSorting[0].OrgLevel;
                    //for (i = 0; i < $scope.OrgSorting.length; i++) {
                    //    if (i == 0) {
                    //        allLevel[0] = currentLevel;
                    //        currentLevel = $scope.OrgSorting[i].OrgLevel;
                    //    }
                            
                    //    else {
                    //        if ($scope.OrgSorting[i].OrgLevel != currentLevel) {
                    //            allLevel.push($scope.OrgSorting[i].OrgLevel);
                    //            currentLevel = $scope.OrgSorting[i].OrgLevel;
                    //        }
                    //    }
                    //}

                    $scope.nodesFormatWithNoChild = {
                        id: 0,
                        name: '',
                        level: '',
                        parent: '',
                        checked: false
                    }

                    $scope.nodesFormatWithChild = {
                        id: 0,
                        name: '',
                        level: '',
                        parent: '',
                        checked: false,
                        children: []
                    }

                    var listOfCurrentLevel = [];
                    var listOfChild; //is final result of list org 

                    for (oLevel = $scope.OrgSorting[$scope.OrgSorting.length - 1].OrgLevel; oLevel >= 0; oLevel--) {
                        var allOrgInLevel = $scope.OrgSorting.filter(Data => Data.OrgLevel == oLevel);
                        var currentLevel = oLevel;
                        for (index = 0; index < allOrgInLevel.length; index++) {
                            //ถ้าเป็น Node ล่างสุด ไม่ต้องมี Children object
                            var tempNode = $scope.OrgSorting[$scope.OrgSorting.length - 1].OrgLevel == currentLevel ? {
                                id: 0,
                                name: '',
                                level: '',
                                parent: '',
                                checked: false
                            } : {
                                id: 0,
                                name: '',
                                level: '',
                                parent: '',
                                checked: false,
                                children: []
                            };
                            if (angular.isUndefined(tempNode.children)) {
                                tempNode.id = allOrgInLevel[index].OrgUnit;
                                tempNode.name = allOrgInLevel[index].OrgUnitName;
                                tempNode.level = allOrgInLevel[index].OrgLevel;
                                tempNode.parent = allOrgInLevel[index].OrgParent;
                                tempNode.checked = false;
                            } else {
                                tempNode.id = allOrgInLevel[index].OrgUnit;
                                tempNode.name = allOrgInLevel[index].OrgUnitName;
                                tempNode.level = allOrgInLevel[index].OrgLevel;
                                tempNode.parent = allOrgInLevel[index].OrgParent;
                                tempNode.checked = false;
                                children: []
                            }
                            if (listOfCurrentLevel.length == 0)
                                listOfCurrentLevel[0] = tempNode;
                            else
                                listOfCurrentLevel.push(tempNode);
                        }
                        //ไม่ใช่ org Node สุดท้าย
                        if ($scope.OrgSorting[$scope.OrgSorting.length - 1].OrgLevel != oLevel) {
                            for (index = 0; index < listOfCurrentLevel.length; index++) {
                                var allChildNode = listOfChild.filter(Data => Data.parent == listOfCurrentLevel[index].id);
                                if (allChildNode.length > 0)
                                    allChildNode.forEach(element => listOfCurrentLevel[index].children.push(element));
                                else {
                                    delete listOfCurrentLevel[index].children;
                                }

                            }
                        }

                        listOfChild = listOfCurrentLevel;
                        listOfCurrentLevel = [];
                    }

                    //filter Parrent
                    var tempOrgParent = "";
                    var i = 0;
                    for (i = 0; i < $scope.OrgList.length; i++) {
                        if ($scope.OrgList[i].OrgLevel === 0) {
                            $scope.rootOrgData = {
                                id: $scope.OrgList[i].OrgUnit,
                                name: $scope.OrgList[i].OrgUnitName,
                                level: $scope.OrgList[i].OrgLevel
                            }
                            $scope.rootOrg.push($scope.rootOrgData);
                        } else {
                            if ($scope.OrgList[i].OrgParent !== tempOrgParent) {
                                tempOrgParent = $scope.OrgList[i].OrgParent;
                                $scope.parentData = {
                                    id: $scope.OrgList[i].OrgUnit,
                                    name: $scope.OrgList[i].OrgUnitName,
                                    level: $scope.OrgList[i].OrgLevel
                                }
                                $scope.parentOrg.push($scope.parentData);
                            }
                        }
                    }

                    //loop parrent -> child
                    if ($scope.rootOrg.length >= 1) {

                        $scope.nodes = listOfChild; 
                    }
                    
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            }
            $scope.GetReportOrgList();

            $scope.controllerFunction = function (valueFromDirective) {
                console.log('val from directive', valueFromDirective);
            }

            /* ตัวนี้ยังไม่ใช้งาน อาจปิด แต่เอาไว้ก่อน */
            $scope.GetTypeQuota = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetAllLeaveQuotaType';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MasterQuotaType = response.data.oTypeQuota;
                    for (var i = 0; i < $scope.MasterQuotaType.length; i++) {
                        if ($scope.MasterQuotaType[i].LeaveQuotaKey == '0110') {
                            //ลบลาพักผ่อน (ยกมา)
                            $scope.MasterQuotaType.splice(i, 1);
                        }
                    }
                }, function errorCallback(response) {
                });
            }
            $scope.GetTypeQuota();

            $scope.GetTypeLeave = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetLeaveEditorUI';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MasterLeaveType = response.data.oTypeLeave;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            }
            $scope.GetTypeLeave();

            $scope.GetReportLeave = function () {
                if ($scope.OrgSelect == "" || $scope.OrgSelect == null || $scope.OrgSelect == 'undefined') {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['REPORT_LEAVE'].REQUIREDSELECT_ORG_ALERT)
                            .ok('OK')
                    );
                    return;
                }
                var URL = CONFIG.SERVER + 'HRTM/GetReportLeave';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'StartDate': $scope.StartDate, 'EndDate': $scope.EndDate, 'Org': $scope.OrgSelect }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ReportLeaveTmp = response.data;
                    $scope.ReportLeave = response.data;
                    //console.log('report = ', $scope.ReportLeave);
                    $scope.rptLeave = [];
                    if ($scope.ReportLeave.length > 0) {
                        //sumleave by column
                        for (var i = 0; i < $scope.oLeaveGroupSetting.length; i++) {
                            $scope.sumLeave[i] = 0;
                            for (var j = 0; j < $scope.ReportLeave.length; j++) {
                                var index = $scope.oLeaveGroupSetting[i].LeaveTypeGroup;
                                if ($scope.ReportLeave[j][index] !== null) {
                                    $scope.sumLeave[i] = $scope.sumLeave[i] + $scope.ReportLeave[j][index];
                                }
                            }
                        }
                    }
                    $scope.ReportFilter();
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            }
            //$scope.GetReportLeave();

            // 2020-10-19 Chamrat P. Report leave lists to Excel file
            $scope.getExportLeave = function () {
                $('#exportLeaveStatus').html('<i class="fa fa-spinner fa-spin"></i>');
                //set header text
                $scope.reportTitle = $scope.Text[$scope.Textcategory].REPORTTITLE;
                $scope.totalText = $scope.Text[$scope.Textcategory].LEAVE_TOTAL;
                $scope.reportDate = $scope.getDateFormate($scope.StartDate) + ' - ' + $scope.getDateFormate($scope.EndDate);
                $scope.headerText = [];
                $scope.headerData = {
                    value: ''
                }
                $scope.headerText[0] = $scope.Text[$scope.Textcategory].NO;
                $scope.headerText[1] = $scope.Text[$scope.Textcategory].EMPLOYEEID;
                $scope.headerText[2] = $scope.Text[$scope.Textcategory].EMPLOYEENAME;
                for (var i = 0; i < $scope.headerColumn.length; i++) {
                    $scope.headerText.push($scope.Text['LEAVEQUOTATYPE'][$scope.headerColumn[i].id]);
                }

                //set value each column
                $scope.reportValue = [];
                for (var j = 0; j < $scope.ReportLeave.length; j++) {
                    $scope.valueText = [];
                    for (var i = 0; i < $scope.oLeaveGroupSetting.length; i++) {
                        var index = $scope.oLeaveGroupSetting[i].LeaveTypeGroup;
                        var val = 0;
                        if ($scope.ReportLeave[j][index] != null) {
                            val = $scope.ReportLeave[j][index];
                        }
                        $scope.valueText.push(val);
                    }
                    $scope.reportValue[j] = $scope.valueText;
                }
                //console.log('reportleavewithValuesSet = ', $scope.ReportLeave);

                //$scope.loader.enable = true;
                var oRequestParameter = {
                    CurrentEmployee: $scope.employeeData,
                    InputParameter: { 'title': $scope.reportTitle, 'totalText': $scope.totalText, 'reportDate': $scope.reportDate, 'header': $scope.headerText, 'total': $scope.sumLeave, 'row': $scope.ReportLeave, 'reportValue': $scope.reportValue, 'Org': $scope.OrgSelect }
                };
                var URL = CONFIG.SERVER + 'HRTM/ExportLeave';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;
                    if (response.data.reportExcelResult.Code === 0) {
                        //window.open(response.data.PathDownload);
                        Leavedownload(response.data.reportExcelResult.PathDownload, response.data.fileName);
                    } else {
                        console.log('export success: ', response.data);
                    }
                    $('#exportLeaveStatus').html('');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.debug(response);
                    console.debug('Fail to call');
                    console.log('export error: ', response.data);
                });
            };

            function Leavedownload(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.myClick = function (node) {
                if (node.checked) {
                    alert(node.id);
                }

                //alert('Clicked [' + node.name + '] state is [' + node.checked + ']');
                /*var confirm = dialog.confirm('Editar', node);
                confirm.result.then(function (btn) {

                });*/
            };

            $scope.ReportFilter = function () {
                for (i = 0; i <= $scope.OrgList.length - 1; i++) {
                    if ($scope.OrgList[i].Selected === "True") {
                        $scope.ReportLeave = $filter('filter')($scope.ReportLeaveTmp, $scope.OrgList[i].OrgID);
                    }
                }
            }

            $scope.subStringText = function (text) {
                if (text !== undefined) {
                    if (text.length < 20) {
                        return text;
                    } else {
                        var texts = text.substring(0, 20);
                        return texts + '...';
                    }
                }
            }

            $scope.searchText = function (text) {
                if (text !== undefined) {
                    var sText = text.search("ครึ่งวัน");
                    if (sText >= 0) {
                        if ($scope.employee.Language != 'TH') {
                            return '(Half Day)';
                        } else {
                            return '(ครึ่งวัน)';
                        }
                    } else {
                        return '';
                    }
                }
            }

            $scope.ClearData = function () {
                $scope.StartDate = date;
                $scope.EndDate = date;
                for (i = 0; i <= $scope.OrgList.length - 1; i++) {
                    $scope.OrgList[i].Selected = ""; 
                }
                $scope.ReportLeave = null;
                $scope.sumLeave = [];
            }

            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            //get value from directive treeView
            $scope.controllerFunction = function (valueFromDirective) {
                //console.log('directive val = ',valueFromDirective);
                $scope.treeViewData = valueFromDirective;
                var str = '';
                //if (valueFro
                $scope.OrgSelect = '';
                for (var i = 0; i < valueFromDirective.length; i++) {
                    if ((i+1) < valueFromDirective.length) {
                        str = valueFromDirective[i] + ',';
                    } else {
                        str = valueFromDirective[i];
                    }
                    $scope.OrgSelect = $scope.OrgSelect + str;
                }
                //console.log('OrgeSelect = ', $scope.OrgSelect);
            }

        }]);

    angular.module('DESS')
      .directive('treeView-x', function ($compile) {
          return {
              scope: {
                  localNodes: '=model',
                  "myDirectiveFn": "="
              },
              link: function ($scope, tElement, tAttrs, transclude) {
                  var maxLevels = (angular.isUndefined(tAttrs.maxlevels)) ? 10 : tAttrs.maxlevels;
                  var hasCheckBox = (angular.isUndefined(tAttrs.checkbox)) ? false : true;
                  $scope.showItems = [];

                  $scope.showHide = function (ulId) {
                      var hideThis = document.getElementById(ulId);
                      var showHide = angular.element(hideThis).attr('class');
                      angular.element(hideThis).attr('class', (showHide === 'show' ? 'hide' : 'show'));
                  }

                  $scope.showIcon = function (node) {
                      if (!angular.isUndefined(node.children)) return true;
                  }

                  $scope.checkIfChildren = function (node) {
                      if (!angular.isUndefined(node.children)) return true;
                  }

                  /////////////////////////////////////////////////
                  /// SELECT ALL CHILDRENS
                  function parentCheckChange(item) {
                      for (var i in item.children) {
                          item.children[i].checked = item.checked;
                          if (item.children[i].children) {
                              parentCheckChange(item.children[i]);
                          }
                      }
                  }

                  $scope.selected = [];
                  $scope.level = [];

                  $scope.checkChange = function (node) {
                      if (node.children) {
                          parentCheckChange(node);
                      }
                      //alert(node.id + ' | ' + node.name + ' | ' + node.checked);
                      if (node.checked) {
                          console.log(node.checked);
                          $scope.selected.push(node.id);
                          $scope.level.push(node.level);
                          console.log('selected = ', $scope.selected);
                      } else {
                          //spice from arr
                          var index = $scope.selected.indexOf(node.id);
                          $scope.selected.splice(index, 1);
                          $scope.level.splice(index, 1);

                          for (var i = 0; i < $scope.level.length; i++) {
                              if (node.level <= $scope.level[i]) {
                                  var index = $scope.level.indexOf(node.level);
                                  $scope.selected.splice(index, 1);
                                  $scope.level.splice(index, 1);
                              }
                          }
                          //$scope.selected;
                          //console.log('selected = ', $scope.selected);
                          //console.log('level = ', $scope.level);
                      }
                  }

                  //parse selected id to Controller
                  $scope.somethingHappend = function () {
                      $scope.myDirectiveFn($scope.selected);

                  }
                  /////////////////////////////////////////////////

                  function renderTreeView(collection, level, max) {
                      var text = '';
                      text += '<li ng-repeat="n in ' + collection + '" >';
                      text += '<span ng-show=showIcon(n) class="show-hide" ng-click=showHide(n.id)><i class="fa fa-plus-square"></i></span>';
                      text += '<span ng-show=!showIcon(n) style="padding-right: 6px"></span>';

                      if (hasCheckBox) {
                          text += '<input name="checkNode[]" class="tree-checkbox" type="checkbox" ng-model="n.checked" ng-click="somethingHappend()" ng-change="checkChange(n)">';
                      }

                      //text += '<span class="edit" ng-click=localClick({node:n})><i class="fa fa-pencil"></i></span>'

                      text += '<label>{{n.name}}</label>';

                      if (level < max) {
                          text += '<ul id="{{n.id}}" class="hide" ng-if=checkIfChildren(n)>' + renderTreeView('n.children', level + 1, max) + '</ul></li>';
                      } else {
                          text += '</li>';
                      }

                      return text;
                  }// end renderTreeView();

                  try {
                      var text = '<ul class="tree-view-wrapper">';
                      text += renderTreeView('localNodes', 1, maxLevels);
                      text += '</ul>';
                      tElement.html(text);
                      $compile(tElement.contents())($scope); 
                  }
                  catch (err) {
                      tElement.html('<b>ERROR!!!</b> - ' + err);
                      $compile(tElement.contents())($scope);
                  }

              }//end link
          }; //end return
      })

})();

