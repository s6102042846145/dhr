﻿(function () {
    angular.module('DESS')
        .controller('ReportFingerScanController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {

            /******************* variable start ********************/
     
            $scope.Textcategory = "REPORT_FINGER_SCAN";
            var date = new Date();
            $scope.DateSelect = date;//$filter('date')(date, 'd/M/yyyy');

            $scope.GetReportFinger = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetReportFinger';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'Date': $scope.DateSelect }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ReportFinger = response.data.ReportFinger;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            }
            $scope.GetReportFinger();

            /******************* service caller  start ********************/
            //$scope.getDateFormate = function (date) {
            //    return getDateFormate(date);
            //}
            //function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }

        }]);

})();

