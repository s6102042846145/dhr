﻿(function () {
    angular.module('DESS')
        .controller('ReportCheckInCheckOutController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {

            /******************* variable start ********************/
     
            $scope.Textcategory = "REPORT_CHECKIN_CHECKOUT";
            var date = new Date();
            date.setDate(date.getDate()-1);
            $scope.DateSelect = date;
            $scope.employee = getToken(CONFIG.USER);

            $scope.GetCheckInCheckOut = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetCheckInCheckOutReport';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'Date': $scope.DateSelect }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SumNumberOfEmployee = 0;
                    $scope.SumEmployeeInWork = 0;
                    $scope.SumNumberOfTemporary = 0;
                    $scope.SumTemporaryInWork = 0;
                    $scope.SumAttendance = 0;
                    $scope.SumWorkAtHome = 0;
                    $scope.SumLate = 0;
                    $scope.SumEarly = 0;
                    $scope.SumNonwork = 0;
                    $scope.SumSickLeave = 0;
                    $scope.SumLeave = 0;
                    $scope.SumVacation = 0;
                    $scope.SumMaternityLeave = 0;
                    $scope.SumOFF = 0;
                    $scope.SumSubstitute = 0;
                    //$scope.SumOtherLeave = 0;
                    $scope.DataCheckInCheckOut = response.data.Report.EmployeeList;
                    $scope.Summary = response.data.Report.SummaryList;
                    for (i = 0; i <= $scope.Summary.length - 1; i++) {
                        if ($scope.Summary[i].Code === "EMPLOYEE") {
                            $scope.SumNumberOfEmployee = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "EMPLOYEE_WORK") {
                            $scope.SumEmployeeInWork = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "TEMPORARY") {
                            $scope.SumNumberOfTemporary = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "TEMPORARY_WORK") {
                            $scope.SumTemporaryInWork = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "ATTENDANCETYPE_20") {
                            $scope.SumAttendance = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "ATTENDANCETYPE_21") {
                            $scope.SumWorkAtHome = $scope.Summary[i].Amount;
                        }  
                        if ($scope.Summary[i].Code === "CLOCKINLATE") {
                            $scope.SumLate = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "CLOCKOUTEARLY") {
                            $scope.SumEarly = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "CLOCKNONEWORK") {
                            $scope.SumNonwork = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "LEAVETYPE_02") {
                            $scope.SumSickLeave = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "LEAVETYPE_03") {
                            $scope.SumLeave = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "LEAVETYPE_01") {
                            $scope.SumVacation = $scope.Summary[i].Amount;
                        }
                        //if ($scope.Summary[i].Code === "LEAVETYPE_04") {
                        //    $scope.SumMaternityLeave = $scope.Summary[i].Amount;
                        //}
                        if ($scope.Summary[i].Code === "LEAVETYPE_99") {
                            $scope.SumMaternityLeave = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "OFF") {
                            $scope.SumOFF = $scope.Summary[i].Amount;
                        }
                        if ($scope.Summary[i].Code === "SUBSTITUTE") {
                            $scope.SumSubstitute = $scope.Summary[i].Amount;
                        }
                    }

                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            }
            $scope.GetCheckInCheckOut();

            /******************* service caller  start ********************/
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            };
            
            function getDateFormate(date) {
                var tempYear = parseInt($filter('date')(date, 'yyyy'));
                if ($scope.employee.Language === 'TH') {
                    tempYear = tempYear + 543;
                }
                var dateMonth = $filter('date')(date, 'd/M');
                return dateMonth+'/'+tempYear;
            }

            $scope.getTimeFormate = function (date) {
                var minDate = $filter('date')(date, 'yyyy-MM-dd');
                var tempDate = $filter('date')(new Date('0001-01-01'), 'yyyy-MM-dd');
                if (minDate === tempDate) {
                    return '-';
                } else {
                    return $filter('date')(date, 'HH:mm:ss');
                }
            };
        }]);

})();

