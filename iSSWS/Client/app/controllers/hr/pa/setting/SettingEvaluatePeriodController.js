﻿(function () {
    angular.module('DESS')
        .controller('SettingEvaluatePeriodController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {

            /******************* variable start ********************/
            $scope.Textcategory = "SETTING_EVALUATE";
            $scope.EVASettingList = [];
            $scope.YearSetting = [];
            $scope.YearHistory = [];
            $scope.isSave = false;
            $scope.tempEVAList = [];
            $scope.hasNew = false;

            $scope.EVAPeriodSetting = {
                SettingID: 0,
                Year: '',
                BeginDate: '',
                EndDate: ''
            };

            var date = new Date();
            var date2 = new Date();
            //var dateAdd1Day = date.setDate(date.getDate() + 1);

            $scope.Language = $scope.employeeData.Language;

            $scope.init = async function () {
                //Initial Data
                await $scope.GetSettingYear();
                $scope.GetEvaluateSetting($scope.yearToSetting);
            };

            /* Get Year List for Setting */
            $scope.GetSettingYear = function () {
                $scope.currentYear = date.getFullYear();
                $scope.yearToSetting = $scope.currentYear;

                //get config start year
                var URL = CONFIG.SERVER + 'HRPA/GetEvaluateStartYear';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.YearStart = parseInt(response.data);
                    console.log('Get year start = ', $scope.YearStart);
                    var maxYear = date.getFullYear() + 1;
                    for (var i = maxYear; i >= $scope.YearStart; i--) {
                        $scope.YearSetting.push(i);
                    }
                }, function errorCallback(response) {
                    console.log('error Get config start year');
                });
                $scope.employeeData = getToken(CONFIG.USER);

                //get evaluate year from data
                var URL = CONFIG.SERVER + 'HRPA/GetEvaluateSettingYear';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.YearHistory = response.data;
                    var tempKey = null;
                    angular.forEach(response.data, function (item, key) {
                        if (item.Year == 0) {
                            $scope.yearToHistory = item;
                            tempKey = key;
                        }
                    });
                    $scope.YearHistory.splice(tempKey, 1);
                    $scope.YearHistory.unshift($scope.yearToHistory);

                    $scope.GetEvaluateSettingHistory($scope.yearToHistory);
                    $scope.BeginDate = date;
                    $scope.EndDate = date2.setDate(date2.getDate() + 1);
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    console.log('error Get setting year');
                });
            };

            /* Get Year List for Setting and History */
            $scope.GetHistoryYear = function () {
                $scope.employeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetEvaluateSettingYear';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.YearHistory = response.data;
                    var tempKey = null;
                    angular.forEach(response.data, function (item, key) {
                        if (item.Year == 0) {
                            $scope.yearToHistory = item;
                            tempKey = key;
                        }
                    });
                    $scope.YearHistory.splice(tempKey, 1);
                    $scope.YearHistory.unshift($scope.yearToHistory);

                    $scope.loader.enable = false;
                    console.log('success Get year for history ', $scope.YearHistory);
                }, function errorCallback(response) {
                    console.log('error Get year for history');
                });
            };

            /* Get Evaluate Setting Data History */
            $scope.GetEvaluateSettingHistory = function (yearHistory) {
                console.log('selected year history = ', yearHistory);
                var URL = CONFIG.SERVER + 'HRPA/GetEvaluateSettingHistory';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'YearHistory': yearHistory }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EVASettingListHistory = response.data;
                    console.log('EVASettingListHistory = ', $scope.EVASettingListHistory);
                    $scope.loader.enable = false;
                    console.log('success EVASettingListHistory');

                }, function errorCallback(response) {
                    console.log('error EVASettingListHistory');
                    $scope.loader.enable = false;
                });
            };

            /* Change Setting Year */
            $scope.ChangeSettingYear = function (yearToSetting) {
                //check new add row that not yet save to DB before
                $scope.hasNew = false;
                if ($scope.EVASettingList.length > 0) {
                    var checkNewData = false;
                    for (var i = 0; i < $scope.EVASettingList.length; i++) {
                        if ($scope.EVASettingList[i].PeriodID == 0) {
                            checkNewData = true;
                            $scope.yearToSetting = $scope.EVASettingList[i].Year;
                        }
                    }
                    if (checkNewData == true) {
                        $scope.hasNew = true;
                        //alert warning box and show text class color warning in row
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].WARNING)
                                .textContent($scope.Text['SETTING_EVALUATE'].WARNINGSAVEDATA)
                                .ok('OK')
                        );
                        $scope.BeginDate = date.setFullYear(parseInt($scope.yearToSetting));
                        $scope.EndDate = date2.setFullYear(parseInt($scope.yearToSetting));
                    } else {
                        $scope.GetEvaluateSetting(yearToSetting);
                    }
                } else {
                    $scope.GetEvaluateSetting(yearToSetting);
                }
            }

            /* Get Evaluate Setting Data By Year */
            $scope.GetEvaluateSetting = function (yearToSetting) {
                $scope.BeginDate = date;
                console.log('selected year setting = ', yearToSetting);
                var URL = CONFIG.SERVER + 'HRPA/GetEvaluateSettingByYear';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'YearSetting': yearToSetting }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EVASettingList = response.data;
                    $scope.tempEVAList = response.data;
                    //check change year to next year and set new date by selected year
                    $scope.BeginDate = date.setFullYear(parseInt($scope.yearToSetting));
                    $scope.EndDate = date2.setFullYear(parseInt($scope.yearToSetting));
                    console.log('EVASettingList by selected year ', $scope.yearToSetting, ' = ', $scope.EVASettingList);
                    $scope.loader.enable = false;
                    console.log('success EVASettingList');
                    $("#PA_909_divEVASettingDetail").removeClass("border-danger");
                }, function errorCallback(response) {
                    console.log('error EVASettingList');
                    $scope.loader.enable = false;
                });
            };

            /* Add Evaluate Setting List */
            $scope.EvaluateSetting = function () {
                $scope.EVAPeriodSetting.Year = $scope.yearToSetting;
                console.log('PeriodSetting Modify =', $scope.EVAPeriodSetting.Year, ' ', $scope.EVAPeriodSetting);
                var URL = CONFIG.SERVER + 'HRPA/EvaluateSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        'EVAPeriodSetting': $scope.EVAPeriodSetting,
                        'EVASettingList': $scope.EVASettingList !== undefined ? $scope.EVASettingList : [],
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Exception = response.data.oException;
                    if ($scope.Exception !== '') {
                        var strException = $scope.Exception.split('|');
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].WARNING)
                                .textContent($scope.Exception)
                                .ok('OK')
                        );
                    } else {
                        $scope.EVASettingList = response.data.EVASettingList;
                        $scope.EVASettingList = $filter('orderBy')($scope.EVASettingList, ['BeginDate']);
                        $("#PA_909_divEVASettingDetail").removeClass("border-danger");
                        $scope.BeginDate = date;
                        $scope.isSave = true;
                        $scope.hasNew = true;
                    }

                    console.log('EVASettingList after add row = ', $scope.EVASettingList);
                    $scope.loader.enable = false;
                    console.log('success EVASettingList');
                }, function errorCallback(response) {
                    console.log('error EVASettingList');
                    $scope.loader.enable = false;
                });
            }

            /* Confirm Evaluate Setting Save */
            $scope.SaveEvaluateSettingConfirm = function () {
                $("#PA_909_SaveModal").modal('show');
            }

            /* Save Evaluate Setting List To DB */
            $scope.SaveEvaluateSetting = function () {
                //confirm to save data
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/confirm.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    locals: {
                        params: {
                            name: $scope.Text['SYSTEM'].WARNING,
                            placeholder: $scope.Text['SYSTEM'].CONFIRM_SAVE,
                            confirm: $scope.Text['SYSTEM'].SAVE,
                            cancel: $scope.Text['SYSTEM'].BUTTON_CANCEL
                        }
                    },
                }).then(function (answer) {
                    if (answer) {
                        //retrive data list from $scope.EVASettingList
                        var URL = CONFIG.SERVER + 'HRPA/SaveEvaSetting';
                        $scope.employeeData = getToken(CONFIG.USER);
                        var oRequestParameter = {
                            InputParameter: { 'EvaSetting': $scope.EVASettingList }
                            , CurrentEmployee: $scope.employeeData
                            , Requestor: $scope.requesterData
                            , Creator: getToken(CONFIG.USER)
                        };
                        $("#PA_909_savingStatus").html('<i class="fa fa-spinner fa-spin"></i> ' + $scope.Text['SYSTEM'].SAVE_PROCESSING);
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.Exception = response.data.oException;
                            if ($scope.Exception !== '') {
                                var strException = $scope.Exception.split('|');
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['SYSTEM'].WARNING)
                                        .textContent($scope.Exception)
                                        .ok('OK')
                                );
                            } else {
                                $scope.EVASettingList = response.data.EVASettingList;
                                $scope.tempEVAList = response.data.EVASettingList;
                                $scope.isSave = false;
                                $scope.hasNew = false;
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['SYSTEM'].STATUS)
                                        .textContent($scope.Text['SYSTEM'].SAVE_SUCCESS)
                                        .ok('OK')
                                );
                                $scope.GetEvaluateSetting($scope.yearToSetting);
                                $scope.GetHistoryYear();
                                $scope.GetEvaluateSettingHistory($scope.yearToHistory);
                            }
                            $("#PA_909_savingStatus").html('');
                            console.log('EVASettingList after saved = ', $scope.EVASettingList);
                            $scope.loader.enable = false;
                            console.log('success EVASettingList');
                        }, function errorCallback(response) {
                            console.log('error EVASettingList');
                            $scope.loader.enable = false;
                        });
                    }
                }, function () { });
            }

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.EVAPeriodSetting.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd ');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.EVAPeriodSetting.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.checkStartDate = function () {
                if ($scope.BeginDate > $scope.EndDate) {
                    var date = new Date($scope.BeginDate);
                    $scope.EndDate = date.setDate(date.getDate() + 1);
                    $scope.setSelectedEndDate($scope.EndDate);
                }
                if ($scope.BeginDate == $scope.EndDate) {
                    var dateEnd = new Date($scope.BeginDate);
                    $scope.EndDate = dateEnd.setDate(dateEnd.getDate() + 1);
                    $scope.setSelectedEndDate($scope.EndDate);
                }
                $scope.setSelectedBeginDate($scope.BeginDate);
            }
            $scope.checkEndDate = function () {
                if ($scope.EndDate < $scope.BeginDate) {
                    $scope.setSelectedBeginDate($scope.EndDate);
                    var date = new Date($scope.EndDate);
                    $scope.BeginDate = date.setDate(date.getDate() - 1);
                    $scope.setSelectedBeginDate($scope.EndDate);
                }
                if ($scope.EndDate == $scope.BeginDate) {
                    var date = new Date($scope.EndDate);
                    $scope.EndDate = date.setDate(date.getDate() + 1);
                    $scope.setSelectedEndDate($scope.EndDate);
                    console.log('enter end == begin');
                }
                $scope.setSelectedEndDate($scope.EndDate);
            }

            $scope.deleteRow = function (index) {
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/confirm.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    locals: {
                        params: {
                            name: $scope.Text['SYSTEM'].WARNING,
                            placeholder: $scope.Text['SYSTEM'].CONFIRM_DELETE,
                            confirm: $scope.Text['SYSTEM'].DELETE,
                            cancel: $scope.Text['SYSTEM'].BUTTON_CANCEL
                        }
                    },
                }).then(function (answer) {
                    if (answer) {
                        var temp = $scope.tempEVAList.length;
                        $scope.EVASettingList.splice(index, 1);
                        if ($scope.EVASettingList.length < temp) {
                            $scope.isSave = true;
                            $scope.hasNew = true;
                        } else {
                            //compare modify data ?
                            $scope.checkModifyData();
                        }
                    }
                }, function () { });
            };

            $scope.editRow = function (index) {
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/confirm.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    locals: {
                        params: {
                            name: $scope.Text['SYSTEM'].WARNING,
                            placeholder: $scope.Text['SYSTEM'].CONFIRM_EDIT,
                            confirm: $scope.Text['SYSTEM'].EDIT,
                            cancel: $scope.Text['SYSTEM'].BUTTON_CANCEL
                        }
                    },
                }).then(function (answer) {
                    if (answer) {
                        $scope.isEditing = true;
                        $scope.BeginDate = $scope.EVASettingList[index].BeginDate;
                        $scope.EndDate = $scope.EVASettingList[index].EndDate;
                        $scope.yearToSetting = $scope.EVASettingList[index].Year;
                        $scope.EVASettingList.splice(index, 1);
                        $("#PA_909_divEVASettingDetail").addClass("border-danger");
                    }
                }, function () { });
            }

            $scope.checkModifyData = function () {
                var count = 0;
                angular.forEach($scope.tempEVAList, function (value1, key1) {
                    angular.forEach($scope.EVASettingList, function (value2, key2) {
                        if (value1.Year == value2.Year
                            && value1.BeginDate == value2.BeginDate
                            && value1.EndDate == value2.EndDate) {
                            count = count + 1;
                        }
                    })
                })
                if (count == $scope.tempEVAList.length) {
                    $scope.isSave = false;
                    $scope.hasNew = false;
                } else {
                    $scope.isSave = true;
                    $scope.hasNew = true;
                }
            }

            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function (answer) {
                    $mdDialog.cancel(answer);
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };
            }

        }]);

})();

