﻿(function () {
    angular.module('DESS')
        .controller('PersonalSecretContentController', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.loader.enable = false;
                $scope.profile = getToken(CONFIG.USER);
                $scope.LevelSelected = [];
                $scope.UnitSelected = [];
                $scope.PositionSelected = [];
                $scope.EmployeeData = [];
                $scope.isLastRecord = false; // flag สำหรับดุว่าข้อมูลหน่วยงานที่เลือก เป็นอันสุดท้ายรึเปล่า







                /******************* variable start ********************/
                $scope.profile = (getToken(CONFIG.USER));
                $scope.UserImg = $scope.profile.ImageUrl + "?" + new Date().getTime();
                $scope.Textcategory = "PA_EDUCATION_MANAGEMENT";
                $scope.content.isShowHeader = true;
                $scope.PAConfigPersonalTitleFormat = [];
                $scope.Name_EmpID = $scope.requesterData.EmployeeID + ' ' + $scope.requesterData.Name;
                $scope.FirstTabIndex = '';
                $scope.EmpCode = '';
                $scope.EmpNameTH = '';
                $scope.PosCode = '';
                $scope.PositionTextTH = '';
                $scope.UnitCode = '';
                $scope.UnitTextTh = '';
                $scope.BandValue = '';
                $scope.BandTextTH = '';
                $scope.UnitLevelValue = '';
                $scope.UnitLevelTextTH = '';

                $scope.StartDateText = null;
                $scope.EndDateText = null;
                $scope.EducationLevelText = '';
                $scope.CertificateText = '';
                $scope.MajorText = '';
                $scope.MinorText = '';
                $scope.InstitutionText = '';
                $scope.CountryText = '';
                $scope.HonorText = '';
                $scope.GradeText = '';

                $scope.fname = '';
                //$scope.mname = '-';
                //$scope.inameTextTH = '-';
                //$scope.nickName = '-';
                //$scope.iname1TextTH = '-';
                //$scope.iname2TextTH = '-';
                //$scope.iname3TextTH = '-';
                //$scope.iname4TextTH = '-';
                //$scope.iname5TextTH = '-';
                //$scope.fullName = '-';

                $scope.sexTextTH = '';


                $scope.init = function () {
                    init();
                    GetAdminSelectEmp();
                }
                // #endregion ******************* listener end ********************/

                // #region ******************* action start ********************/
                function init() {
                    $scope.GetAddressType();
                    $scope.GetRelationList();
                    getPAConfiguration();
                    //$scope.getAllTextDescription();
                    $scope.GetOrgLevel();
                    $scope.GetOrgUnit();
                    // $scope.GetPosition();
                }



                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                //#region date format

                $scope.getDateFormat = function (date) {
                    return $filter('date')(date, 'dd/MM/yyyy');
                }

                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateFormateData = function (date) {
                    return getDateFormateData(date);
                }

                function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }



                $scope.ConvertDate = function (date) {
                    return ConvertDate(date);
                }
                function ConvertDate(date) {
                    var day = date.substr(4, 4) + "-" + date.substr(2, 2) + "-" + date.substr(0, 2);
                    return $filter('date')(day, 'dd/MM/yyyy');
                }
                //ConvertDate(data.startDate)


                //#endregion

                //#endregion


                function getPAConfiguration() {
                    $scope.loader.enable = true;
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PersonalConfig = response.data.Personal;
                        $scope.AddressConfig = response.data.Address;
                        $scope.BankConfig = response.data.Bank;
                        $scope.FamilyConfig = response.data.Family;
                        $scope.CommuConfig = response.data.Communication;
                        $scope.EducationConfig = response.data.Education;
                        $scope.TaxAllowanceConfig = response.data.TaxAllowance;
                        $scope.TabQualiConfig = response.data.TabPAQuali;

                        console.log('sssssaaa');
                        $scope.TabPersonalConfig = response.data.TabPAInfo;
                        $scope.TabContentConfig = response.data.TabContent;
                        $scope.DocumentConfig = response.data.Document;
                        $scope.AcademicConfig = response.data.Academic;
                        //check Personaldata Title Config
                        for (var i = 0; i < $scope.PersonalConfig.length; i++) {
                            switch ($scope.PersonalConfig[i].FieldName) {
                                case "TitleID":
                                case "AcademicTitle":
                                case "MedicalTitle":
                                case "MilitaryTitle":
                                case "PrefixName":
                                case "SecondTitle"://DR.
                                    if (!$scope.PersonalConfig[i].IsVisible) {
                                        $scope.PAConfigPersonalTitleFormat.push($scope.PersonalConfig[i].FieldName);
                                    }
                                    break;
                                default:
                            }
                        }
                        if ($scope.TabPersonalConfig.length > 0) {
                            console.log('sssssaaa', $scope.TabPersonalConfig[0]);
                            var FirstTabIndex = $scope.TabPersonalConfig[0].FieldName;
                            $scope.getDataByTab(FirstTabIndex);
                            $scope.FirstTabIndex = FirstTabIndex;
                        }
                        //$scope.DateCutoffApproveBankAccount = response.data.DateCutoffApproveBankAccount;
                        // $scope.BankAccountRemark = $scope.Text['REMARK'].BANK_ACCOUNT.replace('{0}', $scope.DateCutoffApproveBankAccount);
                        console.log('getPAConfiguration success.');
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        console.log('getPAConfiguration error.');
                        $scope.loader.enable = false;
                    });
                }


                //$scope.Text = null;
                //$scope.RequestorText = null;
                //$scope.getAllTextDescription = function () {
                //    $scope.Text = null;
                //    var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                //    var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //         Success
                //        if (response.data != null) {
                //            $scope.Text = response.data;
                //            $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                //        }
                //    }, function errorCallback(response) {
                //         Error
                //        $scope.Text = null;
                //        console.log('error MainController TextDescription.', response);
                //    });
                //};
                //$scope.getAllTextDescription();


                $scope.getDataByTab = function (TapClick) {
                    switch (TapClick) {
                        case "TabSecretInfo":
                            break;
                        case "TabSecretAddress":
                            break;
                        case "TabSecretFamily ":
                            break;
                        case "TabSecretContract":
                            break;
                        case "TabSecretDocument":
                            break;
                        case "TabSecretSize":
                            break;
                        case "TabSecretAttachment":
                            break;
                        default:
                    }
                };

                $scope.GetOrgLevel = function () {
                    var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.LevelList = response.data;

                        console.log('success GetOrgLevel', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgLevel', response);
                    });
                }
                $scope.GetOrgLevel();

                $scope.GetOrgUnit = function () {
                    $scope.levelSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';

                    for (var i = 0; i < $scope.LevelSelected.length; i++) {
                        if ($scope.LevelSelected.length > 1) {
                            $scope.levelSearch += $scope.LevelSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.levelSearch = $scope.LevelSelected[0].DLL_VALUE;
                        }
                    }

                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.UnitList = response.data.oDDL;
                        $scope.GetEmployee();
                        console.log('success GetOrgUnit', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgUnit', response);
                    });
                }
                $scope.GetOrgUnit();

                $scope.GetPosition = function (unit) {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDDL';

                    for (var i = 0; i < $scope.UnitSelected.length; i++) {

                        if ($scope.UnitSelected.length > 1) {
                            $scope.unitSearch += $scope.UnitSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.unitSearch = $scope.UnitSelected[0].DLL_VALUE;
                        }
                    }



                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.unitSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionList = response.data.oDDL;;
                        $scope.GetEmployee();
                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetPosition();




                $scope.GetEmployee = function (position) {
                    $scope.PositionSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/GetEducationEmployeeDLL';

                    for (var i = 0; i < $scope.PositionSelected.length; i++) {
                        if ($scope.PositionSelected.length > 1) {
                            $scope.PositionSearch += $scope.PositionSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.PositionSearch = $scope.PositionSelected[0].DLL_VALUE;
                        }
                    }




                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'PAOR',
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31",
                            "UnitLevelValue": $scope.levelSearch,
                            "UnitCode": $scope.unitSearch,
                            "PostCode": $scope.PositionSearch
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeList = response.data.oDDL;;

                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetEmployee();




                $scope.SearchEmployeeData = function () {
                    $scope.loader.enable = true;
                    $scope.EmployeeSearch = '';



                    for (var i = 0; i < $scope.EmployeeSelected.length; i++) {
                        if ($scope.EmployeeSelected.length > 1) {
                            $scope.EmployeeSearch += $scope.EmployeeSelected[i].DLL_VALUE + ':';
                        } else {
                            $scope.EmployeeSearch = $scope.EmployeeSelected[0].DLL_VALUE;
                        }
                    }



                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "UnitSelected": $scope.unitSearch,
                            "PositionSelected": $scope.PositionSearch,
                            "EmpCode": ($scope.EmployeeSearch) ? $scope.EmployeeSearch : "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeDataList = response.data;
                        console.log('success GetDataContent', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetDataContent', response);
                    });
                };
                $scope.SetIsAllPeriod = function () {
                    if ($scope.AllPeriod) {
                        $scope.AsOfDate = '';
                    } else {
                        $scope.AsOfDate = angular.copy($scope.CurrentAsOfDate);
                    }
                }
                $scope.SetCurrentTab = function (currentTab) {
                    $scope.CurrentTab = currentTab;
                    $scope.ResetDataContent();
                }




                $scope.ViewDetailData = function (data) {
                    console.log("bbbbbbbbaaaaaaaaaa", data);
                    //$scope.EmpCode = data.EmpCode;
                    //$scope.EmpNameTH = data.EmpNameTH;
                    //$scope.PosCode = data.PosCode;
                    //$scope.PositionTextTH = data.PositionTextTH;
                    //$scope.UnitCode = data.UnitCode;
                    //$scope.UnitTextTh = data.UnitTextTh;
                    //$scope.BandValue = data.BandValue;
                    //$scope.BandTextTH = data.BandTextTH;
                    //$scope.UnitLevelValue = data.UnitLevelValue;
                    //$scope.UnitLevelTextTH = data.UnitLevelTextTH;

                    $scope.loader.enable = true;
                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": "",
                            "UnitSelected": "",
                            "PositionSelected": data.PosCode,
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        SetLastAdminSelectEmp(response.data[0]);
                        GetPersonalEducationDetailById(response.data[0]);
                        GetPersonalWorkHistoryDetailById(response.data[0]);
                        GetPersonalTrainningDetailById(response.data[0]);

                        GetPersonalInfo(response.data[0]);
                        GetDHRPAAddressInfByEmpCode(response.data[0]);
                        GetDHRPAFamilyInfByEmpCode(response.data[0]);
                        GetDHRPAContractByEmpCode(response.data[0]);
                        GetDHRPADocumentInfByEmpCode(response.data[0]);
                        GetDHRPAClothingSizeByEmpCode(response.data[0]);
                        GetDHRPAAddressFamilyInfByEmpCode(response.data[0]);

                        SetEmployeeDetail(response.data[0]);
                        $scope.loader.enable = false;
                        console.log('success GetMarkDataWithRequestType', response);
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetMarkDataWithRequestType', response);
                    });
                };


                function SetLastAdminSelectEmp(data) {
                    var URL = CONFIG.SERVER + 'HRPA/SetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "SET",
                            "EmpCode": data.EmpCode,
                            "PosCode": data.PosCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                    });
                }


                function GetAdminSelectEmp() {
                    var URL = CONFIG.SERVER + 'HRPA/GetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "GET",
                            "EmpCode": "",
                            "PosCode": "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmpCode = response.data[0].EmpCode;
                        $scope.ViewDetailData(response.data[0]);
                    });
                }

                $scope.ResetDataContent = function () {

                    $scope.PositionSelected = [];
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.EmployeeSelected = [];

                }




                //Start GetDateFromTab
                //get DataPersonal
                function GetPersonalInfo(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAPersonalInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            //"BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31" 
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.DataList = response.data;
                        console.log('DataList', $scope.DataList);

                    });
                }
                //GetPersonalAddress
                function GetDHRPAAddressInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAAddressInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            //"BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.AddressList = response.data;
                        //$scope.AddressListTmp = response.data;
                        console.log('AddressList', $scope.AddressList);

                    });
                }
                //GetPersonalFamily
                
                function GetDHRPAFamilyInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAFamilyInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            //"BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormateData($scope.CurrentAsOfDate),
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.FamilyList = response.data;
                        console.log('FamilyList', $scope.FamilyList);
                        H
                    });
                }


                //GetPersonalContract
                function GetDHRPAContractByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAContractByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            //"BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.ContractList = response.data;
                        console.log('ContractList', $scope.ContractList);

                    });
                }
                //GetPersonalDocument
                function GetDHRPADocumentInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPADocumentInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            //"BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.DocumentList = response.data;
                        console.log('DocumentList', $scope.DocumentList);

                    });
                }

                //GetPersonalShirtSize
                function GetDHRPAClothingSizeByEmpCode(data) {
                    console.log('$scope.ShirtSizeList', data.StartDate);
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAClothingSizeByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            //"BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData(data.StartDate),
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData(data.EndDate)
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.ShirtSizeList = response.data;
                        console.log('$ShirtSizeList', $scope.ShirtSizeList);

                    });
                }










                //GetEducation
                function GetPersonalEducationDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetPersonalEducationDetail';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        //$scope.EducationList = response.data;
                        //console.log('$scope.EducationList', $scope.EducationList);

                    });
                }

                //GetWorkHistory
                function GetPersonalWorkHistoryDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetWorkHistoryDetail';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.WorkHistoryList = response.data;
                        console.log('$scope.WorkHistoryList', $scope.WorkHistoryList);

                    });
                }

                function GetPersonalTrainningDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetTrainingHistory';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.TrainingList = response.data;
                        console.log('$scope.TrainingList', $scope.TrainingList);

                    });
                }

                //$scope.familyData;
                function GetDHRPAAddressFamilyInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAAddressFamilyInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.familyData = response.data;
                        //console.log('$scope.familyData', $scope.familyData);

                    });
                }







                //Start GetDateFromTab
                $scope.ViewAddressDetail = function (data) {
                    console.log('ViewAddressDetail', data);
                    $scope.startAddress = data.BeginDate;
                    $scope.endAddress = data.EndDate;

                    $scope.AddressNo = data.AddressNo;
                    $scope.Building = data.Building;
                    $scope.BuildingEn = data.BuildingEn;
                    $scope.Moo = data.Moo;
                    $scope.Soi = data.Soi;
                    $scope.Street = data.Street;
                    $scope.CountryTextTH = data.CountryTextTH;
                    $scope.CountryTextEN = data.CountryTextEN;

                    $scope.ProvinceTextEN = data.ProvinceTextEN;
                    $scope.ProvinceTextTH = data.ProvinceTextTH;

                    $scope.DistrictTextTH = data.DistrictTextTH;
                    $scope.DistrictTextEN = data.DistrictTextEN;
                    $scope.SubdistrictTextTH = data.SubdistrictTextTH;
                    $scope.SubdistrictTextEN = data.SubdistrictTextEN;
                    $scope.Postcode = data.Postcode;
                    
                    
                    
                    

                    
                    
                }


                $scope.ViewEducationDetail = function (data) {
                    //console.log('ViewEducationDetail', data);
                    $scope.StartDateText = ConvertDate(data.startDate);
                    $scope.EndDateText = ConvertDate(data.endDate);
                    $scope.inameTextTH = data.inameTextTH;
                    $scope.fname = data.fname;
                    $scope.lname = data.lname;
                    $scope.mname = data.mname;
                    $scope.nickName = data.nickName;

                    $scope.iname1TextTH = data.iname1TextTH;
                    $scope.iname2TextTH = data.iname2TextTH;
                    $scope.iname3TextTH = data.iname3TextTH;
                    $scope.iname4TextTH = data.iname4TextTH;
                    $scope.iname5TextTH = data.iname5TextTH;
                    $scope.fullName = data.fullName;
                    $scope.inameEnText = data.inameEnText;
                    $scope.fnameEn = data.fnameEn;
                    $scope.lnameEn = data.lnameEn;
                    $scope.fullNameEnText = data.fullNameEnText;
                    //console.log('xxxx', data.sexTextTH);
                    $scope.sexTextTH = data.sexTextTH;
                    $scope.sexTextEN = data.sexTextEN;
                    $scope.maritalStatusTextTH = data.maritalStatusTextTH;
                    $scope.maritalStatusTextEN = data.maritalStatusTextEN;
                    $scope.nationalityTextTH = data.nationalityTextTH;
                    $scope.nationalityTextEN = data.nationalityTextEN;
                    $scope.provinceBirthTextTH = data.provinceBirthTextTH;
                    $scope.provinceBirthTextEN = data.provinceBirthTextEN;
                    $scope.countryBirthTextTH = data.countryBirthTextTH;
                    $scope.countryBirthTextEN = data.countryBirthTextEN;
                    $scope.religionTextTH = data.religionTextTH;
                    $scope.religionTextEN = data.religionTextEN;
                }
                //$scope.familyId;
                //Tab 3 
                $scope.ViewFamilyDetail = function (data) {
                    //console.log('ViewFamilyDetail', data);
                    $scope.FamilystartDate = data.startDate;
                    $scope.FamilyendDate = data.endDate;
                    $scope.FamilyinameCodeTextTH = data.inameCodeTextTH;
                    $scope.FamilyinameCodeTextEN = data.inameCodeTextEN;
                    $scope.Familyfname = data.fname;
                    $scope.Familylname = data.lname;
                    $scope.FamilysexCodeTextTH = data.sexCodeTextTH;
                    $scope.FamilysexCodeTextEN = data.sexCodeTextEN;
                    $scope.FamilybirthDate = getDateFormateData(data.birthDate);
                    $scope.Age = getAge(getDateFormateData(data.birthDate));
                    $scope.FamilynationalityCodeTextEN = data.nationalityCodeTextEN;
                    $scope.FamilynationalityCodeTextTH = data.nationalityCodeTextTH;
                    $scope.FamilycountryBirthCodeTextEN = data.countryBirthCodeTextEN;
                    $scope.FamilycountryBirthCodeTextTH = data.countryBirthCodeTextTH;
                    $scope.FamilymaritalDate = data.maritalDate;
                    $scope.FamilydivorceDate = data.divorceDate;
                    $scope.FamilypassAwayF = data.passAwayF;//status  DB flag เสียชีวิต    (1 ใช่, 0 ไม่ใช่)
                    $scope.FamilyidCard = data.idCard;
                    $scope.FamilyidtelNumber = data.telNumber;
                    for (var i = 0; i < $scope.familyData.length; i++) {
                        if ($scope.familyData[i].familyId == data.familyId) {
                            $scope.FamilyaddressNo = $scope.familyData[i].addressNo;
                            $scope.FamilybuildingTH = $scope.familyData[i].building;
                            $scope.FamilybuildingEN = $scope.familyData[i].buildingEn;
                            $scope.Familymoo = $scope.familyData[i].moo;
                            $scope.Familysoi = $scope.familyData[i].soi;
                            $scope.Familystreet = $scope.familyData[i].idCard;
                            $scope.FamilycountryTextEN = $scope.familyData[i].countryTextEN;
                            $scope.FamilycountryTextTH = $scope.familyData[i].countryTextTH;
                            $scope.FamilyprovinceTextEN = $scope.familyData[i].provinceTextEN;
                            $scope.FamilyprovinceTextTH = $scope.familyData[i].provinceTextTH;
                            $scope.FamilydistrictTextEN = $scope.familyData[i].districtTextEN;
                            $scope.FamilydistrictTextTH = $scope.familyData[i].districtTextTH;
                            $scope.FamilysubdistrictTextEN = $scope.familyData[i].subdistrictTextEN;
                            $scope.FamilysubdistrictTextTH = $scope.familyData[i].subdistrictTextTH;
                            $scope.Familypostcode = $scope.familyData[i].postcode;
                        }
                    }
                }



                function getAge(bob) {

                    var DOB = new Date(bob);
                    var today = new Date();
                    var age = today.getTime() - DOB.getTime();
                    var elapsed = new Date(age);
                    var year = elapsed.getYear() - 70;
                    var month = elapsed.getMonth();
                    var day = elapsed.getDay();
                    var ageTotal = year + " ปี " + month + " เดือน " + day + " วัน";
                    //console.log("xxx : " + ageTotal)
                    return ageTotal;



                }

                function SetEmployeeDetail(data) {


                    $scope.EmpCode = data.EmpCode;
                    $scope.EmpNameTH = data.EmpNameTH;
                    $scope.PosCode = data.PosCode;
                    $scope.PositionTextTH = data.PositionTextTH;
                    $scope.UnitCode = data.UnitCode;
                    $scope.UnitTextTh = data.UnitTextTh;
                    $scope.BandValue = data.BandValue;
                    $scope.BandTextTH = data.BandTextTH;
                    $scope.UnitLevelValue = data.UnitLevelValue;
                    $scope.UnitLevelTextTH = data.UnitLevelTextTH;





                }



                $scope.ResetDataContent = function () {
                    $scope.AllPeriod = false;
                    $scope.CurrentAsOfDate = new Date();
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.PositionSelected = [];
                    $scope.levelSearch = '';
                    $scope.unitSearch = '';
                    $scope.positionSearch = '';
                    $scope.SolidMasterDataList = [];
                    $scope.DottedMasterDataList = [];

                };


                $scope.GetAddressType = function () {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/AddressTypeDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'OMDL',
                            "BeginDate": "2000-01-01",
                            "EndDate": "9999-12-31",
                            "SUBTYPE": 'ADDRESS_TYPE_CODE'
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.AddressTypeList = response.data.oDDL;
                        console.log('success AddressTypeList', response.data.oDDL);
                    }, function errorCallback(response) {
                        console.log('error AddressType', response);
                    });
                }

                
                $scope.GetRelationList = function () {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/RelationList';
                    var oRequestParameter = {
                        InputParameter: {
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31",
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.RelationList = response.data.oDDL;
                        console.log(1111111111111111);
                        console.log('success RelationList', response.data.oDDL);
                        console.log(response.data.oDDL);
                    }, function errorCallback(response) {
                        console.log('error AddressType', response);
                    });
                }




                $scope.onAddressTypeChange = function () {
                    $scope.addresstype;
                    //console.log("ddd", $scope.AddressListTmp);
                    if ($scope.addresstype == 'All') {
                        $scope.AddressList = $scope.AddressListTmp;
                    } else {
                        $scope.AddressList = $scope.AddressListTmp;
                        $scope.AddressList = $filter('filter')($scope.AddressList, { AddressType: $scope.addresstype });
                        $scope.AddressList = $scope.AddressList.filter(x => x.AddressType == $scope.addresstype);
                    }
                };
                $scope.onRelationChange = function () {
                    //$scope.relation;
                    //console.log("ddd", $scope.AddressListTmp);
                    //if ($scope.relation == 'All') {
                    //    $scope.AddressList = $scope.AddressListTmp;
                    //} else {
                    //    $scope.AddressList = $scope.AddressListTmp;
                    //    $scope.AddressList = $filter('filter')($scope.AddressList, { AddressType: $scope.addresstype });
                    //    $scope.AddressList = $scope.AddressList.filter(x => x.AddressType == $scope.addresstype);
                    //}
                };
                //$scope.ViewDetailData = function (data) {
                //    //get data from API and view in modal
                //    //#region HROMPOSITION_EDIT_xxxxxxxx
                //    $scope.PositionInfoEditRequest = [];
                //    var dataCategory = 'HROMPOSITION_EDIT_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocEdit, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoEditRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_DELETE_xxxxxxxx
                //    $scope.PositionInfoDeleteRequest = [];
                //    var dataCategory = 'HROMPOSITION_DELETE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocDelete, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoDeleteRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_PERDELETE_xxxxxxxx
                //    $scope.PositionInfoPerDeleteRequest = [];
                //    var dataCategory = 'HROMPOSITION_PERDELETE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocPerDelete, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoPerDeleteRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_INACTIVE_xxxxxxxx
                //    $scope.PositionInfoInactiveRequest = [];
                //    var dataCategory = 'HROMPOSITION_INACTIVE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocInactive, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoInactiveRequest.push(result[0]);
                //    }
                //    //#endregion



                //    $scope.PositionDetail = angular.copy(data);
                //    $scope.UnitName = filterTextDescription($scope.UnitList, 'DLL_VALUE', $scope.PositionDetail.UnitCode, 'DLL_DATA');
                //};


                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                $scope.directToRequest = function (requestTypeID, actionType,EmployeeID, PAID) {
                    $scope.fromBeginDate = "1900-01-01";
                    $scope.fromEndDate = "9999-12-31";
                    if (actionType == 'EDIT') {
                        if (requestTypeID == '2301') {
                            $scope.CreateNew('2301', 'EDIT' + '|' + $scope.fromBeginDate + '|' + $scope.fromEndDate  + '|' + EmployeeID + '|' + PAID);
                        }
                        if (requestTypeID == '2302') {
                            $scope.CreateNew('2302', 'EDIT' + '|' + $scope.fromBeginDate + '|' + $scope.fromEndDate  + '|' + EmployeeID + '|' + PAID);
                        }
                        if (requestTypeID == '2203') {
                            $scope.CreateNew('2203', 'EDIT' + '|' + $scope.fromBeginDate + '|' + $scope.fromEndDate  + '|' + EmployeeID + '|' + PAID);
                        }
                        if (requestTypeID == '2205') {
                            $scope.CreateNew('2205', 'EDIT' + '|' + $scope.fromBeginDate + '|' + $scope.fromEndDate  + '|' + EmployeeID + '|' + PAID);
                        }
                    } else if (actionType == 'CANCEL') {
                        if (requestTypeID == '2202') {
                            $scope.CreateNew('2202', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                        if (requestTypeID == '2204') {
                            $scope.CreateNew('2204', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                        if (requestTypeID == '2206') {
                            $scope.CreateNew('2206', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                    }
                };






            }]);
})();