﻿(function () {
    angular.module('DESS')
        .controller('PersonalConfidentialContentController', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.loader.enable = false;
                $scope.profile = getToken(CONFIG.USER);
                $scope.LevelSelected = [];
                $scope.UnitSelected = [];
                $scope.PositionSelected = [];
                $scope.EmployeeData = [];
                $scope.isLastRecord = false; // flag สำหรับดุว่าข้อมูลหน่วยงานที่เลือก เป็นอันสุดท้ายรึเปล่า







                /******************* variable start ********************/
                $scope.profile = (getToken(CONFIG.USER));
                $scope.UserImg = $scope.profile.ImageUrl + "?" + new Date().getTime();
                $scope.Textcategory = "HRPAPERSONALDATA";
                $scope.content.isShowHeader = true;
                //$scope.TitleNameText = '';
                //$scope.TitleNameEnText = '';
                //$scope.PrefixNameText = '';
                //$scope.SecondTitleText = '';
                //$scope.MilitaryTitleText = '';
                //$scope.AcademicTitleText = '';
                //$scope.MedicalTitleText = '';
                //$scope.GenderText = '';
                //$scope.BirthPlaceText = '';
                //$scope.NationalityText = '';
                //$scope.MaritalStatusText = '';
                //$scope.BirthCityText = '';
                //$scope.ReligionText = '';
                //$scope.BirthDateText = '';
                //$scope.FamilyMemberText = '';
                //$scope.createdDoc;
                $scope.PAConfigPersonalTitleFormat = [];
                $scope.Name_EmpID = $scope.requesterData.EmployeeID + ' ' + $scope.requesterData.Name;
                $scope.FirstTabIndex = '';
                $scope.EmpCode = '';
                $scope.EmpNameTH = '';
                $scope.PosCode = '';
                $scope.PositionTextTH = '';
                $scope.UnitCode = '';
                $scope.UnitTextTh = '';
                $scope.BandValue = '';
                $scope.BandTextTH = '';
                $scope.UnitLevelValue = '';
                $scope.UnitLevelTextTH = '';

                $scope.StartDateText = null;
                $scope.EndDateText = null;
                $scope.DateOfBirth = '-';
                $scope.BloodType = '-';
                $scope.IDCard = '-';


                $scope.dStartDate = '1900-01-01';
                $scope.dEndDate = '9999-12-31';
                $scope.EmployeeCode = "";









                $scope.init = function () {
                    init();
                    GetAdminSelectEmp();
                }
                // #endregion ******************* listener end ********************/

                // #region ******************* action start ********************/
                function init() {
                    getPAConfiguration();
                    //$scope.getAllTextDescription();
                    $scope.GetOrgLevel();
                    $scope.GetOrgUnit();
                    //$scope.getPeriodDLL();
                    // $scope.GetPosition();
                    $scope.getPeriodDLL();
                }



                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                //#region date format

                $scope.getDateFormat = function (date) {
                    return $filter('date')(date, 'dd/MM/yyyy');
                }

                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateFormateData = function (date) {
                    return getDateFormateData(date);
                }

                function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

                $scope.ConvertDate = function (date) {
                    return ConvertDate(date);
                }
                function ConvertDate(date) {
                    var day = date.substr(4, 4) + "-" + date.substr(2, 2) + "-" + date.substr(0, 2);
                    return $filter('date')(day, 'dd/MM/yyyy');
                }
                $scope.ConvertDate2 = function (date) {
                    return ConvertDate2(date);
                }
                function ConvertDate2(date) {
                    var day = date.substr(4, 4) + "/" + date.substr(2, 2) + "/" + date.substr(0, 2);
                    return day;
                }

                //#endregion

                //#endregion


                function getPAConfiguration() {
                    $scope.loader.enable = true;
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PersonalConfig = response.data.Personal;
                        //console.log("xxx", response.data.Personal);
                        $scope.AddressConfig = response.data.Address;
                        $scope.BankConfig = response.data.Bank;
                        $scope.FamilyConfig = response.data.Family;
                        $scope.CommuConfig = response.data.Communication;
                        $scope.EducationConfig = response.data.Education;
                        $scope.TaxAllowanceConfig = response.data.TaxAllowance;

                        $scope.TabPAConfidentialConfig = response.data.TabPAConfidential;

                        $scope.TabContentConfig = response.data.TabContent;
                        $scope.DocumentConfig = response.data.Document;
                        $scope.AcademicConfig = response.data.Academic;
                        //check Personaldata Title Config
                        for (var i = 0; i < $scope.PersonalConfig.length; i++) {
                            switch ($scope.PersonalConfig[i].FieldName) {
                                case "TitleID":
                                case "AcademicTitle":
                                case "MedicalTitle":
                                case "MilitaryTitle":
                                case "PrefixName":
                                case "SecondTitle"://DR.
                                    if (!$scope.PersonalConfig[i].IsVisible) {
                                        $scope.PAConfigPersonalTitleFormat.push($scope.PersonalConfig[i].FieldName);
                                    }
                                    break;
                                default:
                            }
                        }
                        if ($scope.TabPAConfidentialConfig.length > 0) {
                            var FirstTabIndex = $scope.TabPAConfidentialConfig[0].FieldName;
                            $scope.getDataByTab(FirstTabIndex);
                            $scope.FirstTabIndex = FirstTabIndex;
                        }
                        //$scope.DateCutoffApproveBankAccount = response.data.DateCutoffApproveBankAccount;
                        // $scope.BankAccountRemark = $scope.Text['REMARK'].BANK_ACCOUNT.replace('{0}', $scope.DateCutoffApproveBankAccount);
                        //console.log('getPAConfiguration success.');
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        //console.log('getPAConfiguration error.');
                        $scope.loader.enable = false;
                    });
                }


                //$scope.Text = null;
                //$scope.RequestorText = null;
                //$scope.getAllTextDescription = function () {
                //    $scope.Text = null;
                //    var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                //    var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //         Success
                //        if (response.data != null) {
                //            $scope.Text = response.data;
                //            $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                //        }
                //    }, function errorCallback(response) {
                //         Error
                //        $scope.Text = null;
                //        console.log('error MainController TextDescription.', response);
                //    });
                //};
                //$scope.getAllTextDescription();


                $scope.getDataByTab = function (TapClick) {
                    switch (TapClick) {
                        case "TabConfidentialInfo":

                            break;
                        case "TabConfidentialAssess":

                            break;
                        case "TabQualiTraininghistory ":

                            break;
                        case "TabQualiresearchMember":

                            break;
                        case "TabQualiAttachment":

                            break;

                        default:
                    }
                };

                $scope.GetOrgLevel = function () {
                    var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.LevelList = response.data;

                        console.log('success GetOrgLevel', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgLevel', response);
                    });
                }
                $scope.GetOrgLevel();

                $scope.GetOrgUnit = function () {
                    $scope.levelSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';

                    for (var i = 0; i < $scope.LevelSelected.length; i++) {
                        if ($scope.LevelSelected.length > 1) {
                            $scope.levelSearch += $scope.LevelSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.levelSearch = $scope.LevelSelected[0].DLL_VALUE;
                        }
                    }

                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.UnitList = response.data.oDDL;

                        console.log('success GetOrgUnit', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgUnit', response);
                    });
                }
                $scope.GetOrgUnit();

                $scope.GetPosition = function (unit) {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDDL';

                    for (var i = 0; i < $scope.UnitSelected.length; i++) {

                        if ($scope.UnitSelected.length > 1) {
                            $scope.unitSearch += $scope.UnitSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.unitSearch = $scope.UnitSelected[0].DLL_VALUE;
                        }
                    }



                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.unitSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionList = response.data.oDDL;;

                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetPosition();




                $scope.GetEmployee = function (position) {
                    $scope.PositionSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/GetEducationEmployeeDLL';

                    for (var i = 0; i < $scope.PositionSelected.length; i++) {
                        if ($scope.PositionSelected.length > 1) {
                            $scope.PositionSearch += $scope.PositionSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.PositionSearch = $scope.PositionSelected[0].DLL_VALUE;
                        }
                    }




                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'PAOR',
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31",
                            "UnitLevelValue": $scope.levelSearch,
                            "UnitCode": $scope.unitSearch,
                            "PostCode": $scope.PositionSearch
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeList = response.data.oDDL;;

                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetEmployee();




                $scope.SearchEmployeeData = function () {
                    $scope.loader.enable = true;
                    $scope.EmployeeSearch = '';



                    for (var i = 0; i < $scope.EmployeeSelected.length; i++) {
                        if ($scope.EmployeeSelected.length > 1) {
                            $scope.EmployeeSearch += $scope.EmployeeSelected[i].DLL_VALUE + ':';
                        } else {
                            $scope.EmployeeSearch = $scope.EmployeeSelected[0].DLL_VALUE;
                        }
                    }



                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "UnitSelected": $scope.unitSearch,
                            "PositionSelected": $scope.PositionSearch,
                            "EmpCode": ($scope.EmployeeSearch) ? $scope.EmployeeSearch : "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeDataList = response.data;
                        console.log('success GetDataContent', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetDataContent', response);
                    });
                };
                $scope.SetIsAllPeriod = function () {
                    if ($scope.AllPeriod) {
                        $scope.AsOfDate = '';
                    } else {
                        $scope.AsOfDate = angular.copy($scope.CurrentAsOfDate);
                    }
                }
                $scope.SetCurrentTab = function (currentTab) {
                    $scope.CurrentTab = currentTab;
                    $scope.ResetDataContent();
                }




                $scope.ViewDetailData = function (data) {
                    //console.log("bbbbbbbbaaaaaaaaaa", data);
                    //$scope.EmpCode = data.EmpCode;
                    //$scope.EmpNameTH = data.EmpNameTH;
                    //$scope.PosCode = data.PosCode;
                    //$scope.PositionTextTH = data.PositionTextTH;
                    //$scope.UnitCode = data.UnitCode;
                    //$scope.UnitTextTh = data.UnitTextTh;
                    //$scope.BandValue = data.BandValue;
                    //$scope.BandTextTH = data.BandTextTH;
                    //$scope.UnitLevelValue = data.UnitLevelValue;
                    //$scope.UnitLevelTextTH = data.UnitLevelTextTH;

                    $scope.loader.enable = true;
                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": "",
                            "UnitSelected": "",
                            "PositionSelected": data.PosCode,
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        SetLastAdminSelectEmp(response.data[0]);
                        //GetPersonalEducationDetailById(response.data[0]);
                        $scope.EmployeeCode = response.data[0].EmpCode;
                        GetDHRPAPrivateInfByEmpCode(response.data[0]);
                        GetDHRPAEvaluationInfByEmpCode(response.data[0]);
                        GetDHRPAMedicalCheckupByEmpCode(response.data[0]);
                        GetDHRPAPunishmentInfByEmpCode(response.data[0]);

                        $scope.EmpCode = response.data[0].EmpCode;
                        //$scope.EmpNameTH = response.data[0].EmpNameTH;
                        //$scope.PosCode = response.data[0].PosCode;
                        //$scope.PositionTextTH = response.data[0].PositionTextTH;
                        //$scope.UnitCode = response.data[0].UnitCode;
                        //$scope.UnitTextTh = response.data[0].UnitTextTh;
                        //$scope.BandValue = response.data[0].BandValue;
                        //$scope.BandTextTH = response.data[0].BandTextTH;
                        //$scope.UnitLevelValue = response.data[0].UnitLevelValue;
                        //$scope.UnitLevelTextTH = response.data[0].UnitLevelTextTH;


                        SetEmployeeDetail(response.data[0]);

                        $scope.loader.enable = false;

                        console.log('success GetMarkDataWithRequestType', response);


                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetMarkDataWithRequestType', response);
                    });
                };


                function SetLastAdminSelectEmp(data) {
                    var URL = CONFIG.SERVER + 'HRPA/SetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "SET",
                            "EmpCode": data.EmpCode,
                            "PosCode": data.PosCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                    });
                }


                function GetAdminSelectEmp() {
                    var URL = CONFIG.SERVER + 'HRPA/GetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "GET",
                            "EmpCode": "",
                            "PosCode": "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmpCode = response.data[0].EmpCode;
                        $scope.ViewDetailData(response.data[0]);
                    });
                }

                

                function GetDHRPAPrivateInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAPrivateInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.InfoList = response.data;
                        //console.log("$scope.InfoList", $scope.InfoList);
                    });
                }


                
                //GetEvaluationInf
                function GetDHRPAEvaluationInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAEvaluationInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            "BeginDate": $scope.dStartDate,
                            "EndDate": $scope.dEndDate
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EvaluationInfList = response.data;
                        console.log('$scope.EvaluationInfList', $scope.EvaluationInfList);

                    });
                }


                //GetDHRPAMedicalCheckupByEmpCode
                function GetDHRPAMedicalCheckupByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAMedicalCheckupByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormateData(data.StartDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData(data.EndDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.MedicalCheckupList = response.data;
                    });
                }
                
                function GetDHRPAPunishmentInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAPunishmentInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormateData(data.StartDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData(data.EndDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PunishmentList = response.data;
                        console.log('$scope.PunishmentList', $scope.PunishmentList);

                    });
                }
                $scope.ViewPunishmentInfDetail = function (data) {
                   
                }













                $scope.ViewEvaluationInfDetail = function (data) {
                    $scope.evaluationTime = data.evaluationTime;
                    $scope.period = data.period;
                    $scope.score = data.score;
                    $scope.average = data.average;
                    $scope.StartDateText = ConvertDate(data.startDate);
                    $scope.EndDateText = ConvertDate(data.endDate);
                    $scope.upSalaryPercentage = data.upSalaryPercentage;
                    $scope.variableBonus = data.variableBonus;
                    $scope.increaseSalary = data.increaseSalary;
                    $scope.compenAmount = data.compenAmount;
                }



                $scope.ViewInfoDetail = function (data) {
                    //console.log('ViewEducationDetail', data);
                    $scope.StartDateText = ConvertDate(data.startDate);
                    $scope.EndDateText = ConvertDate(data.endDate);
                    $scope.DateOfBirth = ConvertDate(data.birthDate);
                    $scope.Age = getAge(new Date(ConvertDate2(data.birthDate)));
                    $scope.BloodTypeTH = data.bloodGrpTextTH;
                    $scope.BloodTypeEN = data.bloodGrpTextEN;
                    $scope.IDCard = data.idCard;
                }

                function getAge(dob) {
                    var today = new Date(),
                        result = {
                            years: 0,
                            months: 0,
                            days: 0,
                            toString: function () {
                                return (this.years ? this.years + ' ปี ' : '')
                                    + (this.months ? this.months + ' เดือน ' : '')
                                    + (this.days ? this.days + ' วัน' : '');
                            }
                        };
                    result.months =
                        ((today.getFullYear() * 12) + (today.getMonth() + 1))
                        - ((dob.getFullYear() * 12) + (dob.getMonth() + 1));
                    if (0 > (result.days = today.getDate() - dob.getDate())) {
                        var y = today.getFullYear(), m = today.getMonth();
                        m = (--m < 0) ? 11 : m;
                        result.days +=
                            [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m]
                            + (((1 == m) && ((y % 4) == 0) && (((y % 100) > 0) || ((y % 400) == 0)))
                                ? 1 : 0);
                        --result.months;
                    }
                    result.years = (result.months - (result.months % 12)) / 12;
                    result.months = (result.months % 12);
                    return result;

                }



                function SetEmployeeDetail(data) {


                    $scope.EmpCode = data.EmpCode;
                    $scope.EmpNameTH = data.EmpNameTH;
                    $scope.PosCode = data.PosCode;
                    $scope.PositionTextTH = data.PositionTextTH;
                    $scope.UnitCode = data.UnitCode;
                    $scope.UnitTextTh = data.UnitTextTh;
                    $scope.BandValue = data.BandValue;
                    $scope.BandTextTH = data.BandTextTH;
                    $scope.UnitLevelValue = data.UnitLevelValue;
                    $scope.UnitLevelTextTH = data.UnitLevelTextTH;





                }



                $scope.ResetDataContent = function () {
                    $scope.AllPeriod = false;
                    $scope.CurrentAsOfDate = new Date();
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.PositionSelected = [];
                    $scope.levelSearch = '';
                    $scope.unitSearch = '';
                    $scope.positionSearch = '';
                    $scope.SolidMasterDataList = [];
                    $scope.DottedMasterDataList = [];

                };



                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion





               

                $scope.getPeriodDLL = function () {
                    var URL = CONFIG.SERVER + 'HRPA/GetPeriodDLL';
                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'PAME',
                            "EmpCode": '581005',
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PeriodList = response.data.oDDL;

                        console.log('success PeriodList', response.data.oDDL);
                    }, function errorCallback(response) {
                        console.log('error PeriodList', response.data.oDDL);
                    });
                }










                $scope.directToRequest = function (requestTypeID, actionType, StartDate, EndDate, EmployeeID, PAID) {
                    //$scope.fromBeginDate = "1900-01-01";
                    //$scope.fromEndDate = "9999-12-31";
                    if (actionType == 'EDIT') {
                        if (requestTypeID == '2401') {
                            $scope.CreateNew('2401', 'EDIT' + '|' + StartDate + '|' + EndDate + '|' + EmployeeID + '|' + PAID);
                        }
                        if (requestTypeID == '2402') {
                            $scope.CreateNew('2402', 'EDIT' + '|' + StartDate + '|' + EndDate + '|' + EmployeeID + '|' + PAID);
                        }
                    } else if (actionType == 'CANCEL') {

                    }
                };






            }]);
})();