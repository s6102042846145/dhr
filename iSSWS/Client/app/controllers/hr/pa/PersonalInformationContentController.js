﻿(function () {
    angular.module('DESS')
        .controller('PersonalInformationContentController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {

            /******************* variable start ********************/
            $scope.profile = (getToken(CONFIG.USER));
            $scope.UserImg = $scope.profile.ImageUrl + "?" + new Date().getTime();
            $scope.Textcategory = "HRPAPERSONALDATA";
            $scope.content.isShowHeader = true;
            $scope.TitleNameText = '';
            $scope.TitleNameEnText = '';
            $scope.PrefixNameText = '';
            $scope.SecondTitleText = '';
            $scope.MilitaryTitleText = '';
            $scope.AcademicTitleText = '';
            $scope.MedicalTitleText = '';
            $scope.GenderText = '';
            $scope.BirthPlaceText = '';
            $scope.NationalityText = '';
            $scope.MaritalStatusText = '';
            $scope.BirthCityText = '';
            $scope.ReligionText = '';
            $scope.BirthDateText = '';
            $scope.FamilyMemberText = '';
            $scope.createdDoc;
            $scope.PAConfigPersonalTitleFormat = [];
            $scope.Name_EmpID = $scope.requesterData.EmployeeID + ' ' + $scope.requesterData.Name;
            /******************* variable end ********************/

            // #region ******************* listener start ********************/
            $scope.init = function () {
                init();
            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
                getDocumentTypeList();
                getVISATypeList();
            }

            function getPAConfiguration() {
                $scope.loader.enable = true;
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PersonalConfig = response.data.Personal;
                    $scope.AddressConfig = response.data.Address;
                    $scope.BankConfig = response.data.Bank;
                    $scope.FamilyConfig = response.data.Family;
                    $scope.CommuConfig = response.data.Communication;
                    $scope.EducationConfig = response.data.Education;
                    $scope.TaxAllowanceConfig = response.data.TaxAllowance;
                    $scope.TabContentConfig = response.data.TabContent;
                    $scope.DocumentConfig = response.data.Document;
                    $scope.AcademicConfig = response.data.Academic;
                    //check Personaldata Title Config
                    for (var i = 0; i < $scope.PersonalConfig.length; i++) {
                        switch ($scope.PersonalConfig[i].FieldName) {
                            case "TitleID":
                            case "AcademicTitle":
                            case "MedicalTitle":
                            case "MilitaryTitle":
                            case "PrefixName":
                            case "SecondTitle"://DR.
                                if (!$scope.PersonalConfig[i].IsVisible) {
                                    $scope.PAConfigPersonalTitleFormat.push($scope.PersonalConfig[i].FieldName);
                                }
                                break;
                            default:
                        }
                    }
                    if ($scope.TabContentConfig.length > 0) {
                        $scope.FirstTabIndex = $scope.TabContentConfig[0].FieldName;
                        $scope.getDataByTab($scope.FirstTabIndex)
                    }
                    $scope.DateCutoffApproveBankAccount = response.data.DateCutoffApproveBankAccount;
                    $scope.BankAccountRemark = $scope.Text['REMARK'].BANK_ACCOUNT.replace('{0}', $scope.DateCutoffApproveBankAccount);
                    console.log('getPAConfiguration success.');
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log('getPAConfiguration error.');
                    $scope.loader.enable = false;
                });
            }

            $scope.getAttactmentFileForEmployeeID = function () {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetAttactmentFile';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAFileSummay = response.data.PAFileSummay;
                    console.log('getAttactmentFileForEmployeeID done.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getAttactmentFileForEmployeeID.', response);
                });
            }
            $scope.getAttactmentFileForEmployeeID();
            $scope.getDistrictProvince = function (provinceCode) {
                var oRequestParameter = {
                    InputParameter: { "ProvinceCode": provinceCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetDistrictProvince/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.MasterDistrict = response.data.MasterDistrict;
                    //Filter District by Province
                    $stopFlag = false;
                    for (i = 0; i < $scope.MasterDistrict.length; i++) {
                        if ($scope.MasterDistrict[i].provinceCode == provinceCode) {
                            if ($scope.profile.Language == "TH") {
                                $scope.BirthPlaceText = $scope.MasterDistrict[i].provinceName;
                            } else {
                                $scope.BirthPlaceText = $scope.MasterDistrict[i].provinceNameEn;
                            }
                            $stopFlag = true;
                        }
                        if ($stopFlag == true) {
                            break;
                        }
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }
            $scope.getDataByTab = function (TapClick) {
                switch (TapClick) {
                    case "TAB_PA":
                        getPersonalData();
                        getRequestTypeFileSet('601');
                        break;
                    case "TAB_ADDRESS":
                        $scope.loader.enable = true;
                        getAllCountryData();
                        getRequestTypeFileSet('602');
                        break;
                    case "TAB_ACCOUNT":
                        getBankDetail();
                        getRequestTypeFileSet('610');
                        break;
                    case "TAB_FAMILY":
                        var oRequestParameter = {
                            InputParameter: { "ProvinceCode": '' }
                            , CurrentEmployee: getToken(CONFIG.USER)
                            , Requestor: $scope.requesterData
                        };
                        var URL = CONFIG.SERVER + 'HRPA/GetDistrictProvince/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.MasterDistrict = response.data.MasterDistrict;
                            getFamilySelectData();
                            console.log('GetDistrictProvince done.');
                        }, function errorCallback(response) {
                            // Error
                            console.log('GetDistrictProvince Error.');
                        });
                        getRequestTypeFileSet('604');
                        break;
                    case "TAB_TAXALLOWANCE":
                        getTimeAwareLink();
                        getSpouseList();
                        getLink();
                        break;
                    case "TAB_EDUCATION":
                        getEducationSelectData();
                        getRequestTypeFileSet('607');
                        break;
                    case "TAB_DOCUMENTINFO":
                        $scope.loader.enable = false;
                        getRequestTypeFileSet('611');
                        break;
                    case "TAB_CONTRACT":
                        getIsViewCommunicationOnly();
                        getCommunicationData();
                        getRequestTypeFileSet('603');
                        break;
                    case "TAB_RESEARCH_PATENT"://ACADEMIC
                        getPersonalAcademicData();
                        getRequestTypeFileSet('612');
                        break;
                    default:
                }
            }

            // #endregion ******************* action end ********************/

            // #region ******************* function start ********************/

            // #region ******************** Personal START ******************* */
            function getPersonalData() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID, "checkDate": checkDate }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    // Success
                    $scope.UserImg = response.data.empImg;
                    $scope.PersonalInformation = response.data.oResult;

                    if ($scope.profile.Language != "TH") {
                        $scope.profile.Name = $scope.PersonalInformation.PersonalData.FirstNameEn + " " + $scope.PersonalInformation.PersonalData.LastNameEn;
                    }
                    $scope.getDistrictProvince($scope.PersonalInformation.PersonalData.BirthPlace);

                    $scope.getPersonSelectData();
                    $scope.createdDoc = response.data.createdDoc;
                    if ($scope.createdDoc.length > 0) {
                        $scope.PersonalInformation.InfoRequest = [];
                        $scope.PersonalInformation.InfoRequest.push($scope.createdDoc[0]);
                    }
                    console.log('getPersonalData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getPersonalData done.');
                });
            }
            $scope.getPersonSelectData = function () {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Language = response.data.Language;
                    $scope.Title = response.data.Title;
                    $scope.TitleEN = response.data.TitleEN;
                    $scope.SecondTitle = response.data.SecondTitle;
                    $scope.SecondTitleEN = response.data.SecondTitleEN;
                    $scope.Prefix = response.data.Prefix;
                    $scope.PrefixEN = response.data.PrefixEN;
                    $scope.MilitaryTitle = response.data.MilitaryTitle;
                    $scope.MilitaryTitleEN = response.data.MilitaryTitleEN;
                    $scope.AcademicTitle = response.data.AcademicTitle;
                    $scope.AcademicTitleEN = response.data.AcademicTitleEN;
                    $scope.MedicalTitle = response.data.MedicalTitle;
                    $scope.MedicalTitleEN = response.data.MedicalTitleEN;
                    $scope.Gender = response.data.Gender;
                    $scope.Nationality = response.data.Nationality;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Religion = response.data.Religion;
                    $scope.Province = response.data.Province;//BirtiPlace-ภูมิลำเนา
                    $scope.Country = response.data.Country;
                    $scope.FamilyMember = response.data.FamilyMember;


                    if ($scope.Title !== 'null' && $scope.Title.length > 0) {
                        $scope.TitleNameText = FilterDescription($scope.Title, 'Key', $scope.PersonalInformation.PersonalData.TitleID, 'Description');
                    }

                    if ($scope.Title !== 'null' && $scope.Title.length > 0) {
                        $scope.TitleNameEnText = FilterDescription($scope.TitleEN, 'Key', $scope.PersonalInformation.PersonalData.TitleIDEn, 'Description');
                    }
                    if ($scope.SecondTitle !== 'null' && $scope.SecondTitle.length > 0) {
                        $scope.SecondTitleText = FilterDescription($scope.SecondTitle, 'Key', $scope.PersonalInformation.PersonalData.SecondTitle, 'Description');
                    }
                    if ($scope.Prefix !== 'null' && $scope.Prefix.length > 0) {
                        $scope.PrefixNameText = FilterDescription($scope.Prefix, 'Key', $scope.PersonalInformation.PersonalData.Prefix, 'Description');
                    }
                    if ($scope.MilitaryTitle !== 'null' && $scope.MilitaryTitle.length > 0) {
                        $scope.MilitaryTitleText = FilterDescription($scope.MilitaryTitle, 'Key', $scope.PersonalInformation.PersonalData.MilitaryTitle, 'Description');
                    }
                    if ($scope.AcademicTitle !== 'null' && $scope.AcademicTitle.length > 0) {
                        $scope.AcademicTitleText = FilterDescription($scope.AcademicTitle, 'Key', $scope.PersonalInformation.PersonalData.AcademicTitle, 'Description');
                    }
                    if ($scope.MedicalTitle !== 'null' && $scope.MedicalTitle.length > 0) {
                        $scope.MedicalTitleText = FilterDescription($scope.MedicalTitle, 'Key', $scope.PersonalInformation.PersonalData.MedicalTitle, 'Description');
                    }
                    if ($scope.Gender !== 'null' && $scope.Gender.length > 0) {
                        $scope.GenderText = FilterDescription($scope.Gender, 'Key', $scope.PersonalInformation.PersonalData.Gender, 'Description');
                    }
                    if ($scope.Nationality !== 'null' && $scope.Nationality.length > 0) {
                        $scope.NationalityText = FilterDescription($scope.Nationality, 'NationalityKey', $scope.PersonalInformation.PersonalData.Nationality, 'NationalityName');
                    }

                    if ($scope.MaritalStatus !== 'null' && $scope.MaritalStatus.length > 0) {
                        $scope.MaritalStatusText = FilterDescription($scope.MaritalStatus, 'Key', $scope.PersonalInformation.PersonalData.MaritalStatus, 'Description');
                    }

                    if ($scope.Religion !== 'null' && $scope.Religion.length > 0) {
                        $scope.ReligionText = FilterDescription($scope.Religion, 'Key', $scope.PersonalInformation.PersonalData.Religion, 'Description');
                    }

                    if ($scope.Country !== 'null' && $scope.Country.length > 0) {
                        $scope.BirthCityText = FilterDescription($scope.Country, 'CountryCode', $scope.PersonalInformation.PersonalData.BirthCity, 'CountryName');
                    }

                    if ($scope.PersonalInformation.PersonalData.DOB != null) {
                        $scope.BirthDateText = getDateFormate($scope.PersonalInformation.PersonalData.DOB);
                    }


                    $scope.EffectiveDate;
                    switch ($scope.PersonalInformation.PersonalData.MaritalStatus) {
                        case "001":
                            break;
                        case "002"://สมรส
                            if ($scope.PersonalInformation.PersonalData.MaritalDate != '0001-01-01T00:00:00' &&
                                $scope.PersonalInformation.PersonalData.MaritalDate != null &&
                                $scope.PersonalInformation.PersonalData.MaritalDate != '') {
                                $scope.EffectiveDate = getDateFormate($scope.PersonalInformation.PersonalData.MaritalDate);
                            }
                            break;
                        case "003"://หย่า
                            if ($scope.PersonalInformation.PersonalData.DivorceDate != '0001-01-01T00:00:00' &&
                                $scope.PersonalInformation.PersonalData.DivorceDate != null &&
                                $scope.PersonalInformation.PersonalData.DivorceDate != '') {
                                $scope.EffectiveDate = getDateFormate($scope.PersonalInformation.PersonalData.DivorceDate);
                            }
                            break;
                        case "004"://หม้าย
                            if ($scope.PersonalInformation.PersonalData.DivorceDate != '0001-01-01T00:00:00' &&
                                $scope.PersonalInformation.PersonalData.PassAwayDate != null &&
                                $scope.PersonalInformation.PersonalData.PassAwayDate != '') {
                                $scope.EffectiveDate = getDateFormate($scope.PersonalInformation.PersonalData.PassAwayDate);
                            }
                            break;
                    }

                    console.log('getPersonSelectData success.');
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    // Error
                    console.log('getPersonSelectData error.');
                    $scope.loader.enable = false;
                });
            }

            $scope.CheckExpiredDate = function (expiredDate, dateNotiBefore) {
                $scope.IsExpired = false;
                if (expiredDate != undefined && expiredDate != null) {
                    var dateExpire = new Date(expiredDate);
                    var dateCurrent = new Date();
                    const diffTime = Math.abs(dateExpire - dateCurrent);
                    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    if (diffDays <= dateNotiBefore) {
                        $scope.IsExpired = true;
                    }
                }
                return $scope.IsExpired;
            }
            // #endregion ******************** Personal END ******************* */

            // #region ******************** Address START ******************* */
            function getPersonalAddress() {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalAddress';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;
                    // Success
                    $scope.AddressInfo = response.data.oResult;
                    $scope.createdDoc = response.data.createdDoc;

                    for (i = 0; i <= $scope.AddressInfo.length - 1; i++) {
                        for (k = 0; k < $scope.createdDoc.length; k++) {
                            $scope.AddressInfo[i].AddressRequest = [];
                            if ($scope.AddressInfo[i].AddressType === $scope.createdDoc[k].subType) {
                                $scope.AddressInfo[i].AddressRequest.push($scope.createdDoc[k]);
                                break;
                            }
                        }
                    }

                    $scope.DictFileAttachmentList = response.data.dictFileAttachmentList;

                    for (i = 0; i < $scope.AddressInfo.length; i++) {
                        $scope.AddressInfo[i].AddressType == '001' ? $scope.AddressInfo[i].CategoryName = 'PersonalAddressPermanent' : $scope.AddressInfo[i].CategoryName = 'PersonalAddress';
                        if ($scope.AddressInfo[i].Country !== null && $scope.AddressInfo[i].Country !== "") {
                            if ($scope.CountryList !== null) {
                                //ประเทศ
                                $scope.AddressInfo[i].Country = FilterDescription($scope.CountryList, 'CountryCode', $scope.AddressInfo[i].Country, 'CountryName');
                            }
                        }
                        if ($scope.AddressInfo[i].Province !== null && $scope.AddressInfo[i].Province !== "") {
                            //จังหวัด อำเภอ ตำบล
                            if ($scope.employeeData.Language == "TH") {
                                $scope.AddressInfo[i].Province = $scope.AddressInfo[i].ProvinceTextTH;
                                $scope.AddressInfo[i].District = $scope.AddressInfo[i].DistrictTextTH;
                                $scope.AddressInfo[i].SubDistrict = $scope.AddressInfo[i].SubdistrictTextTH;
                            } else {
                                $scope.AddressInfo[i].Province = $scope.AddressInfo[i].ProvinceTextEN;
                                $scope.AddressInfo[i].District = $scope.AddressInfo[i].DistrictTextEN;
                                $scope.AddressInfo[i].SubDistrict = $scope.AddressInfo[i].SubdistrictTextEN;
                            }
                        }
                    }
                    $scope.loader.enable = false;
                    console.log('GetPersonalAddress done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('GetPersonalAddress error.');
                });
            }
            function getAllCountryData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetAllCountryData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.CountryList = response.data.Country;
                    getPersonalAddress();
                    console.log('getAllCountryData done.');
                }, function errorCallback(response) {
                    console.log('getAllCountryData error.');
                });
            }
            //#endregion ******************** Address END ******************* */

            //#region ******************** Bank Account START ******************* */     

            function getBankDetail() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetBankAllDetail';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.AccountInfo = response.data.CurrentBankUpdate;
                        $scope.BankDetail = response.data.CurrentBankUpdate;
                        if ($scope.BankDetail != null) {
                            $scope.BankEffectiveDate = getDateFormate($scope.BankDetail.BeginDate);
                            getBank($scope.BankDetail.Bank);
                        }

                    }
                    else
                        $scope.BankName = "-";
                    $scope.createdDoc = response.data.createdDoc;
                    if ($scope.createdDoc.length > 0) {
                        $scope.AccountInfo.AccountRequest = [];
                        $scope.AccountInfo.AccountRequest.push($scope.createdDoc[0]);
                    }

                    console.log('getBankDetail done.');
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getBankDetail error.');
                });
            }
            function getBank(Code) {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetBank';
                var oRequestParameter = {
                    InputParameter: { "Code": Code }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.BankName = response.data.Description;
                    console.log('getBank done.');
                }, function errorCallback(response) {
                    console.log('getBank error.');
                });
            }
            //#endregion ******************** Bank Account END ******************* */

            //#region ******************** Family START ******************* */
            function getFamilyData() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalFamily';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FamilyMemberList = response.data.FamilyData;
                    $scope.DictFileAttachmentList = response.data.dictFileAttachmentList;
                    //580116 Rachatawan
                    $scope.createdDoc = response.data.createdDoc;

                    for (i = 0; i <= $scope.FamilyMemberList.length - 1; i++) {

                        for (k = 0; k < $scope.createdDoc.length; k++) {
                            $scope.FamilyMemberList[i].FamilyRequest = [];
                            if ($scope.FamilyMemberList[i].FamilyMember === $scope.createdDoc[k].subType && $scope.FamilyMemberList[i].ChildNo === $scope.createdDoc[k].childNo) {
                                $scope.FamilyMemberList[i].FamilyRequest.push($scope.createdDoc[k]);
                                break;
                            }
                        }
                        $scope.FamilyMemberList[i].FamilyMemberText = FilterDescription($scope.FamilyMember, 'Key', $scope.FamilyMemberList[i].FamilyMember, 'Description');
                        $scope.FamilyMemberList[i].TitleNameText = FilterDescription($scope.Title, 'Key', $scope.FamilyMemberList[i].TitleName, 'Description');
                        $scope.FamilyMemberList[i].SexText = FilterDescription($scope.Gender, 'Key', $scope.FamilyMemberList[i].Sex, 'Description');
                        //ภูมิลำเนา
                        $scope.FamilyMemberList[i].BirthPlaceText = FilterDescription($scope.Province, 'ProvinceCode', $scope.FamilyMemberList[i].BirthPlace, 'ProvinceName');
                        //ประเทศที่เกิด
                        $scope.FamilyMemberList[i].CityOfBirthText = FilterDescription($scope.Country, 'CountryCode', $scope.FamilyMemberList[i].CityOfBirth, 'CountryName');
                        //สัญชาติ
                        $scope.FamilyMemberList[i].NationalityText = FilterDescription($scope.Nationality, 'NationalityKey', $scope.FamilyMemberList[i].Nationality, 'NationalityName');

                        //check from date
                        if ($scope.FamilyMemberList[i].MaritalDate != null && $scope.FamilyMemberList[i].MaritalDate != '') {
                            $scope.FamilyMemberList[i].EffectiveDate = getDateFormate($scope.FamilyMemberList[i].MaritalDate);
                            $scope.FamilyMemberList[i].MaritalStatus = '002';
                        }
                        if ($scope.FamilyMemberList[i].DivorceDate != null && $scope.FamilyMemberList[i].DivorceDate != '') {
                            $scope.FamilyMemberList[i].EffectiveDate = getDateFormate($scope.FamilyMemberList[i].DivorceDate);
                            $scope.FamilyMemberList[i].MaritalStatus = '003';
                        }

                        if ($scope.FamilyMemberList[i].PassAwayDate != null && $scope.FamilyMemberList[i].PassAwayDate != '') {
                            $scope.FamilyMemberList[i].EffectivePassAwayDate = getDateFormate($scope.FamilyMemberList[i].PassAwayDate);
                            $scope.FamilyMemberList[i].MaritalStatus = '004';
                        }
                        $scope.FamilyMemberList[i].MaritalStatusText = FilterDescription($scope.MaritalStatus, 'Key', $scope.FamilyMemberList[i].MaritalStatus, 'Description');
                        $scope.FamilyMemberList[i].DeadText = $scope.FamilyMemberList[i].Dead == "" ? $scope.Text['FAMILY']['DEADSTATUS0'] : $scope.Text['FAMILY']['DEADSTATUS1'];

                        //Address
                        //ประเทศ
                        $scope.FamilyMemberList[i].CountryText = FilterDescription($scope.Country, 'CountryCode', $scope.FamilyMemberList[i].Country, 'CountryName');
                        //จังหวัด
                        $scope.FamilyMemberList[i].CityText = FilterDescription($scope.Province, 'ProvinceCode', $scope.FamilyMemberList[i].City, 'ProvinceName');
                        //อำเภอ,ตำบล
                        //if ($scope.FamilyMemberList[i].City != null && $scope.FamilyMemberList[i].Country == 'TH') {
                        if ($scope.FamilyMemberList[i].City != null && $scope.profile.Language == 'TH') {
                            if ($scope.MasterDistrict.length > 0) {
                                $scope.FamilyMemberList[i].DistrictText = FilterDescription($scope.MasterDistrict, 'districtCode', $scope.FamilyMemberList[i].District, 'districtName');
                                $scope.FamilyMemberList[i].SubdistrictText = FilterDescription($scope.MasterDistrict, 'subdistrictCode', $scope.FamilyMemberList[i].SubdistrictCode, 'subdistrictName');
                            }
                        } else if ($scope.FamilyMemberList[i].City != null && $scope.profile.Language == 'EN') {
                            if ($scope.MasterDistrict.length > 0) {
                                $scope.FamilyMemberList[i].DistrictText = FilterDescription($scope.MasterDistrict, 'districtCode', $scope.FamilyMemberList[i].District, 'districtNameEn');
                                $scope.FamilyMemberList[i].SubdistrictText = FilterDescription($scope.MasterDistrict, 'subdistrictCode', $scope.FamilyMemberList[i].SubdistrictCode, 'subdistrictNameEn');
                            }
                        } else {
                            $scope.FamilyMemberList[i].CityText = $scope.FamilyMemberList[i].City;
                            $scope.FamilyMemberList[i].DistrictText = $scope.FamilyMemberList[i].District;
                            $scope.FamilyMemberList[i].SubdistrictText = $scope.FamilyMemberList[i].SubdistrictCode;

                        }

                        if ($scope.FamilyMemberList[i].Dead == 'X') {
                            $scope.FamilyMemberList[i].EffectivePassAwayDate = getDateFormate($scope.FamilyMemberList[i].PassAwayDate);
                        } else {
                            $scope.FamilyMemberList[i].EffectivePassAwayDate = null;
                        }
                        switch ($scope.FamilyMemberList[i].FamilyMember) {
                            case "01"://คู่สมรส
                                $scope.FamilyMemberList[i].IDCard = $scope.FamilyMemberList[i].SpouseID != undefined ? $scope.FamilyMemberList[i].SpouseID : "";
                                break;
                            case "11"://บิดา
                                $scope.FamilyMemberList[i].IDCard = $scope.FamilyMemberList[i].FatherID != undefined ? $scope.FamilyMemberList[i].FatherID : "";
                                break;
                            case "12"://มารดา
                                $scope.FamilyMemberList[i].IDCard = $scope.FamilyMemberList[i].MotherID != undefined ? $scope.FamilyMemberList[i].MotherID : "";
                                break;
                            case "13"://บิดาคู่สมรส
                                $scope.FamilyMemberList[i].IDCard = $scope.FamilyMemberList[i].FatherSpouseID != undefined ? $scope.FamilyMemberList[i].FatherSpouseID : "";
                                break;
                            case "14"://มารดาคู่สมรส
                                $scope.FamilyMemberList[i].IDCard = $scope.FamilyMemberList[i].MotherSpouseID != undefined ? $scope.FamilyMemberList[i].MotherSpouseID : "";
                                break;
                            default://บุตร
                                $scope.FamilyMemberList[i].IDCard = $scope.FamilyMemberList[i].ChildID != undefined ? $scope.FamilyMemberList[i].ChildID : "";
                                break;
                        }
                    }
                    $scope.loader.enable = false;
                    console.log('getFamilyData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getFamilyData error.');
                });
            }
            function getFamilySelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    getFamilyData();
                    // Success
                    $scope.Title = response.data.Title;
                    $scope.Gender = response.data.Gender;
                    $scope.Nationality = response.data.Nationality;
                    $scope.Religion = response.data.Religion;
                    $scope.Province = response.data.Province;
                    $scope.Country = response.data.Country;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.FamilyMember = response.data.FamilyMember;
                    console.log('getFamilySelectData done.');
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    // Error
                    console.log('getFamilySelectData error.');
                });
            }
            //#endregion ******************** Family END ******************* */

            //#region ******************** Education START ******************* */
            function getEducationData() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetAllPersonalEducationHistory';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;
                    // Success
                    $scope.EducationList = response.data.oResult;
                    $scope.DictFileAttachmentList = response.data.dictFileAttachmentList;
                    $scope.createdDoc = response.data.createdDoc;

                    for (i = 0; i <= $scope.EducationList.length - 1; i++) {
                        for (k = 0; k < $scope.createdDoc.length; k++) {
                            $scope.EducationList[i].EducationRequest = [];
                            if ($scope.EducationList[i].EducationLevelCode === $scope.createdDoc[k].subType) {
                                $scope.EducationList[i].EducationRequest.push($scope.createdDoc[k]);
                                break;
                            }
                        }
                        $scope.EducationList[i].EducationGroupText = FilterDescription($scope.Educationgrouplist, 'EducationGroupCode', $scope.EducationList[i].EducationGroupCode, 'Description');
                        $scope.EducationList[i].EducationlevelText = FilterDescription($scope.Educationlevellist, 'EducationLevelCode', $scope.EducationList[i].EducationLevelCode, 'EducationLevelText');
                        $scope.EducationList[i].InstituteText = FilterDescription($scope.Institutelist, 'InstituteCode', $scope.EducationList[i].InstituteCode, 'InstituteText');
                        $scope.EducationList[i].CountryText = FilterDescription($scope.Countrylist, 'CountryCode', $scope.EducationList[i].CountryCode, 'CountryName');
                        $scope.EducationList[i].CertificateText = FilterDescription($scope.Certificatelist, 'CertificateCode', $scope.EducationList[i].CertificateCode, 'CertificateDescription');
                        $scope.EducationList[i].BranchText = FilterDescription($scope.Branch, 'BranchCode', $scope.EducationList[i].Branch1, 'BranchText');
                        $scope.EducationList[i].HonorText = FilterDescription($scope.Honorlist, 'HonorCode', $scope.EducationList[i].HonorCode, 'Description');
                        $scope.EducationList[i].BeginDate = $filter('date')($scope.EducationList[i].BeginDate, 'yyyy-MM-dd');
                        $scope.EducationList[i].EndDate = $filter('date')($scope.EducationList[i].EndDate, 'yyyy-MM-dd');
                    }
                    console.log('getEducationData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getEducationData error.');
                });
            }
            function getEducationSelectData() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetEducationSelectData';
                var oRequestParameter = {
                    InputParameter: { EducationLevelCode: '' }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;
                    // Success 
                    getEducationData();
                    $scope.Educationgrouplist = response.data.educationgroup;
                    $scope.Honorlist = response.data.honor;
                    $scope.Educationlevellist = response.data.educationlevel;
                    $scope.Institutelist = response.data.institute;
                    $scope.Countrylist = response.data.country;
                    $scope.Certificatelist = response.data.certificate;
                    $scope.Branch = response.data.branch1;
                    console.log('getEducationSelectData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getEducationSelectData error.');
                });
            }
            //#endregion ******************** Education END ******************* */

            //#region ******************** Communication START ******************* */
            function getIsViewCommunicationOnly() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetIsViewCommunicationOnly';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.IsViewCommunicationOnly = response.data;
                    console.log('getIsViewCommunicationOnly done.');
                }, function errorCallback(response) {
                    console.log('getIsViewCommunicationOnly error.');
                });
            }
            function getCommunicationData() {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetCommunicationData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;
                    // Success
                    $scope.ContactInfo = response.data.Contact;

                    for (var i = 0; i < $scope.ContactInfo.length; i++) {
                        $scope.ContactInfo[i].NoSpecify = $scope.ContactInfo[i].EndDate === '9999-12-31T00:00:00';
                    }

                    $scope.createdDoc = response.data.createdDoc;
                    if ($scope.createdDoc.length > 0) {
                        $scope.ContactInfo.ContactRequest = [];
                        $scope.ContactInfo.ContactRequest.push($scope.createdDoc[0]);
                    }

                    console.log('GetCommunicationData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('GetCommunicationData error.');
                });
            }
            //#endregion ******************** Communication END ******************* */

            //#region ******************** TaxAllowance START ******************* */
            $scope.exportToPdfAllowance = function () {
                $("#exportLYStatus").html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>');
                var str_category_export = "";
                var str_status_export = "";
                var type_download = "PDF";
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download === 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';
                }
                var URL = CONFIG.SERVER + 'HRPY/ExportTaxAllowanceToPDF/';
                $scope.employeeData = getToken(CONFIG.USER);
                console.log('set employee-->' + $scope.employeeData);
                var oRequestParameter = {
                    InputParameter: {
                        Type: str_category_export
                        , Status: str_status_export
                        , ReportName: $scope.Text["APPLICATION"].TAXREPORT
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.requesterData.EmployeeID
                        , EmployeeName: $scope.createHtml($scope.requesterData.Name)
                        , ExportType: type_download //"PDF" //EXCEL
                        , TaxYear: $scope.SelectedTaxYear
                        , PinCode: $scope.vPincode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    var file_name = response.data.FileName;
                    var url = CONFIG.SERVER + response.data.URL;

                    if (response.data.FileName == '' || response.data.URL == '')
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].WARNING)
                                .textContent($scope.Text['HRPA_EXCEPTION'].NODATA_TAXDEDUCTION)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                        );
                    else
                        downloadPDFTaxAllowance(url, file_name);
                    $("#exportLYStatus").html('');
                    console.log('exportToPdfAllowance done.');
                }, function errorCallback(response) {
                    $("#exportLYStatus").html('');
                    console.log('exportToPdfAllowance error.');
                });
            }
            function getTaxAllowanceData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetTaxAllowanceData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TaxAllowanceData = response.data.oTaxAllowance;
                    $scope.TaxCutOffDate = response.data.DateCutoffApproveTax;
                    $scope.TaxCutOffRemark = $scope.Text['TAXALLOWANCE'].PY_DESC_2.replace('{0}', $scope.TaxCutOffDate);
                    if ($scope.TaxAllowanceData.CompanyCode != "" && $scope.TaxAllowanceData.CompanyCode != null) {
                        $scope.TaxAllowance_Effective = $scope.TaxAllowanceData.TaxAllowance_Effective;
                    } else {
                        $scope.TaxAllowance_Effective = "";
                    }
                    $scope.createdDoc = response.data.createdDoc;
                    if ($scope.createdDoc.length > 0) {
                        $scope.TaxAllowanceData.TaxAllowanceRequest = [];
                        $scope.TaxAllowanceData.TaxAllowanceRequest.push($scope.createdDoc[0]);
                    }
                    if ($scope.SpouseAllowance !== 'null' && $scope.SpouseAllowance.length > 0) {
                        $scope.SpouseAllowanceText = FilterDescription($scope.SpouseAllowance, 'Key', $scope.TaxAllowanceData.SpouseAllowance, 'Description');
                    }
                    console.log('getTaxAllowanceData done.');
                }, function errorCallback(response) {
                    console.log('getTaxAllowanceData error.');
                });
            }
            function getSpouseList() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPY/GetSpouseDescSelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SpouseAllowance = response.data.SpouseDesc;
                    getTaxAllowanceData(); //ดึงจาก DHR
                    console.log('getSpouseList done.');
                }, function errorCallback(response) {
                    // Error
                    console.log('getSpouseList error.');
                });
            }
            function getLink() {
                //var oEmployeeData = getToken(CONFIG.USER);
                //var URL = CONFIG.SERVER + 'HRPY/GetLinkTAXDEDUCTIONWEB';
                //var oRequestParameter = {
                //    InputParameter: {}
                //    , CurrentEmployee: oEmployeeData
                //};

                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    $scope.LinkTAXDEDUCTIONWEB = response.data;
                //}, function errorCallback(response) {
                //});
                $scope.LinkTAXDEDUCTIONWEB = '';
            }
            function downloadPDFTaxAllowance(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }
            function getTimeAwareLink() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/ShowTimeAwareLink';
                var oRequestParameter = {
                    InputParameter: { "LinkID": 2 }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.IsShowTimeAwareLink = response.data;
                    console.log('getTimeAwareLink done.');
                }, function errorCallback(response) {
                    console.log('getTimeAwareLink error.');
                });
            }
            //#endregion ******************** TaxAllowance END ******************* */

            //#region ******************** DocumentInfo START ******************* */
            function getDocumentTypeList() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetDocumentTypeAllForDocumentInfo';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.DocumentTypeList = response.data;
                    //getVISATypeList();

                    console.log('getDocumentTypeList done.');
                }, function errorCallback(response) {
                    // Error
                    console.log('getDocumentTypeList error.');
                });
            }
            function getVISATypeList() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetValidityTypeAllForDocumentInfo';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ValidityTypeList = response.data;
                    getPersonalDocumentData();
                    console.log('getVISATypeList done.');
                }, function errorCallback(response) {
                    // Error
                    console.log('getVISATypeList error.');
                });
            }
            function getPersonalDocumentData() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalDocumentData';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //success
                    $scope.PersonalDocumentList = response.data.oPersonalDocumentList;
                    $scope.DocumentValidateList = response.data.oDocumentValidate;
                    $scope.createdDoc = response.data.createdDoc;
                    $scope.ExpiredMessage = '';
                    for (i = 0; i <= $scope.PersonalDocumentList.length - 1; i++) {
                        $scope.PersonalDocumentList[i].isDocExpired = [];

                        for (k = 0; k < $scope.createdDoc.length; k++) {
                            $scope.PersonalDocumentList[i].DocumentRequest = [];
                            if ("HRPAPERSONALDOCUMENT_" + $scope.PersonalDocumentList[i].DocumentType === $scope.createdDoc[k].DataCategory) {
                                $scope.PersonalDocumentList[i].DocumentRequest.push($scope.createdDoc[k]);
                                break;
                            }
                        }
                        $scope.PersonalDocumentList[i].DocumentText = FilterDescription($scope.DocumentTypeList, 'DocumentKey', $scope.PersonalDocumentList[i].DocumentType, 'Description');
                        //ValidityTypeList
                        $scope.PersonalDocumentList[i].NumberOfEntryText = FilterDescription($scope.ValidityTypeList, 'ValidityTypeKey', $scope.PersonalDocumentList[i].ValidityOfEntry, 'Description');

                        if ($scope.PersonalDocumentList[i].Nationality != null) {
                            $scope.PersonalDocumentList[i].NationalityText = FilterDescription($scope.Country, 'CountryCode', $scope.PersonalDocumentList[i].Nationality, 'CountryName');
                        }
                        var validateKey = 'VALIDATELICENCEEXPIRED_' + $scope.PersonalDocumentList[i].DocumentType;
                        if ($scope.DocumentValidateList[validateKey] != undefined && $scope.DocumentValidateList[validateKey] != '999') {
                            /* 01	เลขใบขับขี่,02	เลขหนังสือเดินทาง,03	เลขใบอนุญาตทำงาน,51	Non-B Visa*/
                            if ($scope.PersonalDocumentList[i].ExpiredDate != '' && $scope.PersonalDocumentList[i].ExpiredDate != '') {
                                if ($scope.CheckExpiredDate($scope.PersonalDocumentList[i].ExpiredDate, $scope.DocumentValidateList[validateKey])) {
                                    $scope.ExpiredMessage += '- ' + $scope.Text['DocumentType'][$scope.PersonalDocumentList[i].DocumentType] + '<br>';
                                    $scope.PersonalDocumentList[i].isDocExpired.push(true);
                                }

                            }
                        }
                    }

                    if ($scope.ExpiredMessage != '') {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].DOC_EXPIRED_WARNING)
                                .htmlContent($scope.ExpiredMessage)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                        );
                    }

                    $scope.loader.enable = false;
                    console.log('getFamilyData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getFamilyData error.');
                });
            }
            //#endregion ******************** DocumentInfo END ******************* */

            //#region ******************** AcademicInfo START ******************* */            
            function getPersonalAcademicData() {
                $scope.loader.enable = true;
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalAcademicDataByEmpCode';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PersonalAcademicList = response.data.oPersonalAcademicList;

                    $scope.createdDoc = response.data.createdDoc;

                    for (i = 0; i <= $scope.PersonalAcademicList.length - 1; i++) {
                        for (k = 0; k < $scope.createdDoc.length; k++) {
                            $scope.PersonalAcademicList[i].AcademicRequest = [];
                            if ("HRPAPERSONALACADEMIC_" + $scope.PersonalAcademicList[i].DOI === $scope.createdDoc[k].DataCategory) {
                                $scope.PersonalAcademicList[i].AcademicRequest.push($scope.createdDoc[k]);
                                break;
                            }
                        }
                    }
                    $scope.loader.enable = false;
                    console.log('getPersonalAcademicData done.');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('getPersonalAcademicData error.');
                });
            }
            $scope.getAcademicFileFromPath = function (attachment) {
                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {

                    } else if (attachment.FilePath) {
                        var path = CONFIG.SERVER + 'Client/' + attachment.FilePath + attachment.FileName;

                        $window.open(path);
                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment;
                        $window.open(path);
                    }
                }
                /* !direct */
            }
            $scope.getFileFromPath = function (attachment) {
                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {

                    } else if (attachment.FilePath) {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        console.debug(path);
                        $window.open(path);
                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment;
                        console.debug(path);
                        $window.open(path);

                    }
                }
                /* !direct */
            }
            //#endregion ******************** AcademicInfo END ******************* */

            //#endregion ******************* function end ********************/


            /******************* service caller  start ********************/
            $scope.openUrl = function (url) {
                if (url.toLowerCase().indexOf("http://") == 0 || url.toLowerCase().indexOf("https://") == 0) {
                    window.open(url, '_blank');
                } else {

                    window.open('https://' + url, '_blank');
                }
            }

            function getRequestTypeFileSet(TypeID) {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.requesterData.EmployeeID, "RequestTypeID": TypeID, "RequestSubType": "" }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'Workflow/GetRequestTypeFileSet/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.FileAttachmentList = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getRequestTypeFileSet.', response);
                });
            }
            function FilterDescription(array, attr, value, objReturn) {
                $scope.isData = false;
                if (value == "")
                    return "";
                else {
                    if (array != null && array != undefined) {
                        for (var i = 0; i < array.length; i += 1) {
                            if (array[i][attr] == value) {
                                $scope.isData = true;
                                return array[i][objReturn];
                            }
                        }
                    }

                }

                if (!$scope.isData)
                    return "";
            }

            function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }
            $scope.createHtml = function (htmlCode) {
                var txt = document.createElement("textarea");
                txt.innerHTML = htmlCode;
                return txt.value;
            };

            /******************* service caller  end ********************/
            $scope.OpenActionInsteadOf = function () {
                $location.path('/actionInsteadOf/' + $scope.contentId);
            }

        }]);

})();

