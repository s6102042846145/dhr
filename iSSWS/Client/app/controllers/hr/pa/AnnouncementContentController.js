﻿(function () {
    angular.module('DESS')
        .controller('AnnouncementContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.loader.enable = true;
            $scope.seenCount = 0;
            $scope.acceptCount = 0;
            $scope.CompanyCode = $scope.requesterData.RequesterCompanyCode;
            //$scope.dateVariable = new Date();
            //$scope.loader.enable = true;
            //$scope.years = $scope.dateVariable.getFullYear();
            //$scope.prop = {
            //    "type": "select",
            //    "name": $scope.years,
            //    "value": $scope.years,
            //    "values": $scope.YearsList
            //};
            //$scope.HtmlEditor = '';
            ////$scope.AnnouncementListData = [];
            //$scope.SettingID_Delete = '';
            //$scope.num = 0;


            $scope.AnnouncementListData = getAnnouncement();
            console.log('AnnouncementListData.', $scope.AnnouncementListData);

            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };

            function getAnnouncement() {
                var URL = CONFIG.SERVER + 'Announcement/getAnnouncement';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.AnnouncementListData = response.data;
                    calEmployeeActionToAnnouncement();
                    $scope.loader.enable = false;
                    return response;
                },
                    function errorCallback(response) {
                        console.error(response);
                        $scope.loader.enable = false;
                        return response;
                    });
            };

            function acceptAnnouncement(announcementID, statusType, index) {
                alert("ทดสอบ");
                if (statusType == "ACCEPTED") {
                    //$scope.isButtonVisible = true;
                    //$scope.announcements[index].Acceptable = false;
                    $scope.DataShowAnn.Acceptable = false;
                } else {
                    //$scope.isButtonVisible = false;
                }

                var URL = CONFIG.SERVER + 'Announcement/acceptAnnouncement';
                var oRequestParameter = {
                    InputParameter: { "announcementID": announcementID, "statusType": statusType },
                    CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: URL
                }).then(function successCallback(response) {
                    // Success
                    if (statusType == "ACCEPTED")
                        employeeSeeAlert();
                    //$route.reload();
                }, function errorCallback(response) {
                    // Error
                    console.log('error AnnouncementController.', response);
                });
            };

            function calEmployeeActionToAnnouncement() {
                $scope.seenCount = 0;
                $scope.acceptCount = 0;
                // $scope.TodayDate = getDateFormateData(new Date());
                for (i = 0; i < $scope.AnnouncementListData.length; i++) {
                    $scope.AnnouncementListData[i].IsExpire = false;
                    $scope.AnnouncementListData[i].SeenList = $scope.AnnouncementListData[i].SeenList.split(',').length - 1;
                    $scope.AnnouncementListData[i].AcceptedList = $scope.AnnouncementListData[i].AcceptedList.split(',').length - 1;
                    $scope.AnnouncementListData[i].EmpQuatityAccess = $scope.AnnouncementListData[i].EmpQuatityAccess - $scope.AnnouncementListData[i].SeenList
                    if (getDateFormateData(new Date()) > getDateFormateData($scope.AnnouncementListData[i].EndDate)) {
                        $scope.AnnouncementListData[i].IsExpire = true;
                    }
                }

            };

            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            }
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            }
            $scope.genDatas = function () {
                var datas = $scope.AnnouncementListData;
                setNumOfPage(datas);
                var res = [];
                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    if (datas != null && datas.length > 0) {
                        res = datas.slice();
                    }

                }

                return res;
            }
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
            }
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            }
            $scope.getPages = function () {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            }

            $scope.getFileSetID = function (requestDocumentNo) {
                var oFileSetID = -1;

                if (requestDocumentNo == "aa") {
                    oFileSetID = "5000";
                } else {
                    oFileSetID = "2002";
                }
                $scope.ViewObjAnnounce(requestDocumentNo, $scope.CompanyCode);
            }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
        }]);
})();