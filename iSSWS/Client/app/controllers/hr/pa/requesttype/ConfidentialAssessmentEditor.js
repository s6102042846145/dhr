﻿(function () {
    angular.module('DESS')
        .controller('ConfidentialAssessmentEditor', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);


            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];





            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();

            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
                //$scope.getBloodGrpDLL();
            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
                console.log("Confidential", $scope.document.Additional.Confidential);
                console.log("$scope.CompanyCode", $scope.CurrentEmployee.CompanyCode)
                //$scope.document.Additional.Confidential.companyCode = $scope.CurrentEmployee.CompanyCode;
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //console.log("oooooooooooooooooooo2", $scope.document.Additional.Confidential)
                if ($scope.document.Additional.Confidential.BeginDate == null || $scope.document.Additional.Confidential.BeginDate == '' || $scope.document.Additional.Confidential.BeginDate == undefined) {
                    console.log('enter startdate = null');
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );

                    return false;

                } else {
                    console.log("$scope.ccccc", $scope.CurrentEmployee.CompanyCode)
                    $scope.document.Additional.Confidential.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.document.Additional.Confidential.BeginDate = $scope.getDateFormatSave($scope.document.Additional.Confidential.BeginDate);
                    $scope.document.Additional.Confidential.EndDate = $scope.getDateFormatSave($scope.document.Additional.Confidential.EndDate);
                }
                console.log("$scope.ddd", $scope.CurrentEmployee.CompanyCode)
                $scope.requesterData.EmployeeID = $scope.document.Additional.Confidential.EmployeeID;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }


            //$scope.getBloodGrpDLL = function () {
            //    var URL = CONFIG.SERVER + 'HRPA/GetBloodGrpDLL';
            //    var oRequestParameter = {
            //        InputParameter: {
            //            "Type": 'OMDL',
            //            "Language": 'TH',
            //            "BeginDate": "1900-01-01",
            //            "EndDate": "9999-12-31",
            //            "Code": "BLOOD_GRP_CODE"
            //        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
            //    };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        $scope.BloodList = response.data.oDDL;

            //        console.log('success BloodList', response.data.oDDL);
            //    }, function errorCallback(response) {
            //        console.log('error BloodList', response.data.oDDL);
            //    });
            //}




            $scope.ConvertDate = function (date) {
                return ConvertDate(date);
            }
            function ConvertDate(date) {
                var day = date.substr(4, 4) + "-" + date.substr(2, 2) + "-" + date.substr(0, 2);
                return $filter('date')(day, 'dd/MM/yyyy');
            }


            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

        }]);
})();