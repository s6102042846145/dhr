﻿(function () {
    angular.module('DESS')
        .controller('ConfidentialPenaltyEditor', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);


            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];





            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();

            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
                $scope.getDrpStatus();
                $scope.getDrpInspection();
                $scope.getDrpOffense();
                $scope.getDrpPunishment();
                $scope.getDrpNumber();
            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
                console.log("Confidential", $scope.document.Additional.Confidential);
               // console.log("$scope.CurrentEmployee", $scope.CurrentEmployee);
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //console.log("oooooooooooooooooooo2", $scope.document.Additional.Confidential)
                if ($scope.document.Additional.Confidential.BeginDate == null || $scope.document.Additional.Confidential.BeginDate == '' || $scope.document.Additional.Confidential.BeginDate == undefined) {
                    console.log('enter startdate = null');
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );

                    return false;

                } else {
                    $scope.document.Additional.Confidential.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.document.Additional.Confidential.BeginDate = $scope.getDateFormatSave($scope.document.Additional.Confidential.BeginDate);
                    $scope.document.Additional.Confidential.EndDate = $scope.getDateFormatSave($scope.document.Additional.Confidential.EndDate);
                }
                $scope.requesterData.EmployeeID = $scope.document.Additional.Confidential.EmployeeID;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.', response);
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }

            $scope.dDataType = "OMDL";
            $scope.dStartDate = "1900-01-01";
            $scope.dEndDate = "9999-12-31";

            /* Dropdown */            
            $scope.getDrpStatus = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "STATUS_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.statusList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }
            $scope.getDrpInspection = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "START_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.InspectionList = response.data.oDDL;
                    //console.log("$scope.InspectionList", $scope.InspectionList);
                }, function errorCallback(response) {
                });
            }
            $scope.getDrpOffense = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "EVIDENCE_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.OffenseList = response.data.oDDL;
                    //console.log("$scope.OffenseList", $scope.OffenseList);
                }, function errorCallback(response) {
                });
            }
            $scope.getDrpPunishment = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "PUNISHMENT_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PunishmentList = response.data.oDDL;
                    //console.log("$scope.PunishmentList", $scope.PunishmentList);
                }, function errorCallback(response) {
                });
            }

            $scope.getDrpNumber = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": "PACO",
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "2018-05-01",
                        "param2": "2019-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.NumberList = response.data.oDDL;
                    console.log("$scope.NumberList", $scope.NumberList);
                }, function errorCallback(response) {
                });
            }




            $scope.ConvertDate = function (date) {
                return ConvertDate(date);
            }
            function ConvertDate(date) {
                var day = date.substr(4, 4) + "-" + date.substr(2, 2) + "-" + date.substr(0, 2);
                return $filter('date')(day, 'dd/MM/yyyy');
            }
            $scope.ConvertDate2 = function (date) {
                return ConvertDate2(date);
            }
            function ConvertDate2(date) {
                var day = date.substr(4, 4) + "-" + date.substr(2, 2) + "-" + date.substr(0, 2);
                return $filter('date')(day, 'dd/MM/yyyy');
            }


            $scope.getDateFormat = function (date) {
                date = new Date(date);
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

        }]);
})();