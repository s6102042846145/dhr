﻿(function () {
    angular.module('DESS')
        .controller('PositionPermanentDeleteEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            //$('.modal').modal('hide');
            //$scope.loader.enable = false;
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.Language = oEmployeeData.Language;
            $scope.Textcategory = 'OM_POSITION';
      
            $scope.PositionData = $scope.document.Additional.PositionData;
            $scope.PositionDataOld = $scope.document.Additional.PositionData_OLD;
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {

            };

            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 


            //#region date format
            $scope.getDateFormat = function (date) {
                return getDateFormat(date);
            }
            function getDateFormat(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            $scope.getDateTimeFormat = function (date) {
                return getDateTimeFormat(date);
            }
            function getDateTimeFormat(date) { return $filter('date')(date, 'dd/MM/yyyy HH:mm'); }

            $scope.getDateFormatData = function (date) {
                return getDateFormatData(date);
            }

            function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.getDateFormatURL = function (date) {
                return getDateFormatURL(date);
            }
            function getDateFormatURL(date) { return $filter('date')(date, 'yyyyMMdd'); }

            //#endregion

        }]);
})();