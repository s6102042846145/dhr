﻿(function () {
    angular.module('DESS')
        .controller('OrganizationPermanentDeleteEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentLanguage = $scope.CurrentEmployee.Language;
            $scope.Textcategory = 'OM_UNIT_DELETE';
            $scope.OrganizationUnitDataList = [];
            $scope.ConfirmDelete = '';
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //$scope.OrganizationID = angular.copy($scope.document.Additional.OrganizationID);
                //getOrganizationList();
                $scope.OrganizationList = angular.copy($scope.document.Additional.OrgainaztionList);
                for (i = 0; i < $scope.OrganizationList.length; i++) {
                    $scope.OrganizationList[i].BeginDate = getDateFormat(new Date($scope.OrganizationList[i].BeginDate));
                    $scope.OrganizationList[i].EndDate = getDateFormat(new Date($scope.OrganizationList[i].EndDate));
                }

                $scope.loader.enable = false;
                $scope.loader.enable = false;
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.ValidateData = function () {
                //$scope.ConfirmDelete = '';
                //var confirm = $mdDialog.confirm()
                //    .title($scope.Text['SYSTEM'].WARNING)
                //    .textContent($scope.Text[$scope.Textcategory].CONFIRM_PERMANENT_DELETE)
                //    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                //    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                //$mdDialog.show(confirm).then(function (result) {
                //    $scope.ConfirmDelete =true;
                //}, function () {
                //    $scope.ConfirmDelete =false;
                //});
                // return $scope.ConfirmDelete;
                return true;
            };
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 
            /*
            function getOrganizationList() {
                var oRequestParameter = {
                    InputParameter: {
                        UnitSelected: $scope.OrganizationID,
                        LevelSelected: '',
                        BeginDate: '1900-01-01',
                        EndDate: '9999-12-31'
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetOrganizationDataContent/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrganizationList = response.data;
                    for (i = 0; i < $scope.OrganizationList.length; i++) {
                        $scope.OrganizationList[i].BeginDate = getDateFormat(new Date($scope.OrganizationList[i].BeginDate));
                        $scope.OrganizationList[i].EndDate = getDateFormat(new Date($scope.OrganizationList[i].EndDate));
                    }
                    console.log('success Cost center ddl.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error Cost center ddl.');
                });
            } */

            function getDateFormat(date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }
            $scope.getDateFormat = function (objDate) {
                getDateFormat(objDate);
            };
        }]);
})();