﻿(function () {
    angular.module('DESS')
        .controller('OrganizationContent', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.loader.enable = false;
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.profile = getToken(CONFIG.USER);
                $scope.Textcategory = 'OM_UNIT_MANAGEMENT';
                $scope.LevelSelected = [];
                $scope.UnitSelected = [];
                $scope.RequestDocument = ''; // ข้อมูล requestDocument ในกรณีที่ข้อมูลหน่วยงานที่สนใจมี draft ไว้อยู่
                $scope.mapData = ''; // ถ้าไม่เป็น '' แสดงว่า มีเอกสารบางประเภทใช้งานหน่วยงานที่เราต้องการอยู่
                $scope.isLastRecord = false; // flag สำหรับดุว่าข้อมูลหน่วยงานที่เลือก เป็นอันสุดท้ายรึเปล่า

                $scope.init = function () {
                    getALlOrganizationLevel();
                    //getAllOrganizationUnit();
                };

                $scope.GetTest = function () {
                    var URL = CONFIG.SERVER + 'HROM/GetDataTest';
                    var oRequestParameter = {
                        InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.DataList = response.data.Gender;

                        console.log('success GetTest list', response);
                    }, function errorCallback(response) {
                        console.log('error GetTest list', response);
                    });
                };

                function getALlOrganizationLevel() {
                    var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL/';
                    var oRequestParameter = {
                        InputParameter: {
                            BeginDate: $scope.AsOfDate == '' ? '9999-12-31' : getDateFormateData($scope.CurrentAsOfDate),
                            EndDate: $scope.AsOfDate == '' ? '9999-12-31' : getDateFormateData($scope.CurrentAsOfDate)
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.OrganizationLevelList = response.data;
                        console.log('success Organization level ddl.');
                    }, function errorCallback(response) {
                        // Error
                        $scope.loader.enable = false;
                        console.log('error Organization level ddl.');
                    });
                }

                function getRootOrgByHeadOrg() {
                    var oRequestParameter = {
                        InputParameter: {
                            UNIT_ID: $scope.OrgDetail.OrganizationID ? $scope.OrgDetail.OrganizationID : '',
                            LEVEL_ID: $scope.OrgDetail.OrganizationLevelID ? $scope.OrgDetail.OrganizationLevelID : '',
                            HEADORG_ID: $scope.OrgDetail.HeadOfOrganizationID ? $scope.OrgDetail.HeadOfOrganizationID : '',
                            BeginDate: getDateFormateData(new Date($scope.OrgDetail.BeginDate)),
                            EndDate: getDateFormateData(new Date($scope.OrgDetail.EndDate)),
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HROM/getOrganizationLevelChart/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.OrgLevelByCriteria = response.data;
                        console.log('success loadOrgLevelByCriteria.');
                    }, function errorCallback(response) {
                        // Error
                        console.log('error loadOrgLevelByCriteria.');
                    });
                }

                $scope.SetIsAllPeriod = function () {
                    if ($scope.AllPeriod) {
                        $scope.AsOfDate = '';
                    } else {
                        $scope.AsOfDate = angular.copy($scope.CurrentAsOfDate);
                    }
                    $scope.AllPeriodChange = true;
                }

                //#region Mockup search data
                $scope.GetDataContent = function () {
                    $scope.loader.enable = true;
                    $('#loading_search_1100').html('<i class="fa fa-spinner fa-spin"></i>');
                    var oLevelSelect = '';
                    var oUnitSelect = '';

                    for (i = 0; i < $scope.LevelSelected.length; i++)
                        oLevelSelect += $scope.LevelSelected[i].DLL_VALUE + ':';
                    for (i = 0; i < $scope.UnitSelected.length; i++)
                        oUnitSelect += $scope.UnitSelected[i].DLL_VALUE + ':';

                    var URL = CONFIG.SERVER + 'HROM/GetOrganizationDataContent';
                    var oRequestParameter = {
                        InputParameter: {
                            LevelSelected: oLevelSelect,
                            UnitSelected: oUnitSelect,
                            BeginDate: ($scope.AllPeriod) ? '1900-01-01' : getDateFormateData($scope.CurrentAsOfDate),
                            EndDate: '9999-12-31' //($scope.AllPeriod) ? '9999-12-31' : getDateFormateData($scope.CurrentAsOfDate),
                        }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.OrganizationUnitDataList = response.data;
                        $('#loading_search_1100').html('');
                        $scope.loader.enable = false;
                        console.log('success OrganizationUnitDataList list', response);
                    }, function errorCallback(response) {
                        $('#loading_search_1100').html('');
                        $scope.loader.enable = false;
                        console.log('error OrganizationUnitDataList list', response);
                    });

                }
                $scope.ResetDataContent = function () {
                    $scope.AllPeriod = false;
                    $scope.CurrentAsOfDate = new Date();
                    $scope.OrganizationUnitDataList = [];
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                }
                //#endregion

                //#region Show detail data
                $scope.ViewDetailData = function (data) {
                    var URL = CONFIG.SERVER + 'HROM/GetMarkDataWithRequestType';
                    var oRequestParameter = {
                        InputParameter: {
                        }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.MarkData = response.data;
                        $scope.OrgDetail = angular.copy(data);
                        isLastRecord(); //ส่ง Data ไปตรวจสอบว่าเป็น record สุดท้ายรึเปล่า
                        checkDirectLocation(); // ตรวจสอบว่า มี RequestDocument ค้างอยู่ไหม 
                        getRootOrgByHeadOrg(); // Generate ตารางลำดับสายบังคับบัญชา
                        console.log('success GetMarkDataWithRequestType', response);
                    }, function errorCallback(response) {
                        console.log('error GetMarkDataWithRequestType', response);
                    });
                };

                function isLastRecord() {
                    var URL = CONFIG.SERVER + 'HROM/isLastRecordOfOrganization';
                    var oRequestParameter = {
                        InputParameter: {
                            ORG_ID: $scope.OrgDetail.OrganizationID,
                            BeginDate: getDateFormateData(new Date($scope.OrgDetail.BeginDate)),
                            EndDate: getDateFormateData(new Date($scope.OrgDetail.EndDate))
                        }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.isLastRecord = response.data;
                    }, function errorCallback(response) {
                        $scope.isLastRecord = false;
                        console.log('Error during get isLastOrganization record.')
                    });
                }

                //#endregion

                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '10', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                //#region date format
                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateFormateData = function (date) {
                    return getDateFormateData(date);
                }

                function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

                $scope.dateFormatForURL = function (objDate) {
                    return $filter('date')(objDate, 'ddMMyyyy');
                };
                //#endregion

                //#endregion

                $scope.completedSelectLevel = function (event) {
                    $scope.UnitSelected = [];
                    $scope.levelChange = true;
                }

                $scope.dateChange = function () {
                    $scope.DateChange = true;
                };

                $scope.reloadOrganization = function () {
                    if ($scope.levelChange || $scope.DateChange || $scope.AllPeriodChange) {
                        var allLevelSelected = '';
                        for (i = 0; i < $scope.LevelSelected.length; i++)
                            allLevelSelected += $scope.LevelSelected[i].DLL_VALUE + ':';
                        var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                        var oRequestParameter = {
                            InputParameter: {
                                BeginDate: $scope.AsOfDate == '' ? '1900-01-01' : getDateFormateData($scope.CurrentAsOfDate),
                                EndDate: $scope.AsOfDate == '' ? '9999-12-31' : getDateFormateData($scope.CurrentAsOfDate),
                                LevelSelected: allLevelSelected,
                            }
                            , CurrentEmployee: oEmployeeData
                            , Requestor: oRequestData
                        };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.OrganizationList = response.data.oDDL;
                            $scope.levelChange = false;
                            $scope.DateChange = false;
                            $scope.AllPeriodChange = false;
                            console.log('success OrganizationList list', response);
                        }, function errorCallback(response) {
                            console.log('error OrganizationList list', response);
                        });
                    }
                };

                //#region Redirect Markdata or New request
                function checkDirectLocation() {
                    $scope.mapData = '';
                    for (i = 0; i < $scope.MarkData.length; i++) {
                        var splitedDataCategory = $scope.MarkData[i].DataCategory.split('_');
                        if (splitedDataCategory[0] == 'HROMORGANIZATION' && splitedDataCategory[1] == $scope.OrgDetail.OrganizationID) {
                            var arrObject = {
                                DataCategory: $scope.MarkData[i].DataCategory,
                                RequestNo: $scope.MarkData[i].RequestNo,
                                RequestType: $scope.MarkData[i].RequestTypeID,
                                UnitCode: splitedDataCategory[1],
                                Type: 'ORGANIZATION'
                            };
                            $scope.mapData = arrObject;
                            break;
                        } else if (splitedDataCategory[0] == 'HROMPOSITION' && splitedDataCategory[1] == $scope.OrgDetail.OrganizationID) {
                            var arrObject = {
                                DataCategory: $scope.MarkData[i].DataCategory,
                                RequestNo: $scope.MarkData[i].RequestNo,
                                RequestType: $scope.MarkData[i].RequestTypeID,
                                UnitCode: splitedDataCategory[1],
                                Type: 'POSITION'
                            };
                            $scope.mapData = arrObject;
                            break;
                        } else {
                            continue;
                        }
                    }
                    if ($scope.mapData)
                        getRequestDocumentDetail($scope.mapData.RequestNo);
                }

                $scope.buttonText = function (requestTypeID) {
                    var rqID = $scope.mapData.RequestType;
                    var reName = $scope.mapData.RequestNo;
                    if (requestTypeID == '1101') {
                        return !angular.isUndefined(rqID) && rqID == '1101' ? reName : $scope.Text[$scope.Textcategory].BUTTON_EDIT;
                    } else if (requestTypeID == '1102') {
                        return !angular.isUndefined(rqID) && rqID == '1102' ? reName : $scope.Text[$scope.Textcategory].BUTTON_DELETE;
                    } else if (requestTypeID == '1103') {
                        return !angular.isUndefined(rqID) && rqID == '1103' ? reName : $scope.Text[$scope.Textcategory].BUTTON_PERMANENT_DELETE
                    } else if (requestTypeID == '1104') {
                        return !angular.isUndefined(rqID) && rqID == '1104' ? reName : $scope.Text[$scope.Textcategory].BUTTON_INACTIVE;
                    } else if (requestTypeID == '1105') {
                        return !angular.isUndefined(rqID) && rqID == '1105' ? reName : $scope.Text[$scope.Textcategory].BUTTON_SPLIT;
                    }
                };

                $scope.directToRequest = function (requestTypeID) {
                    $scope.warningText = ''; //set default ให้ warn text
                    if ($scope.mapData == '') {
                        if (requestTypeID == '1101') {
                            $scope.CreateNew('1101', $scope.OrgDetail.OrganizationID + '|' + $scope.dateFormatForURL($scope.OrgDetail.BeginDate) + '|' + $scope.dateFormatForURL($scope.OrgDetail.EndDate));
                        } else if (requestTypeID == '1102') {
                            $scope.CreateNew('1102', $scope.OrgDetail.OrganizationID + '|' + $scope.dateFormatForURL($scope.OrgDetail.BeginDate) + '|' + $scope.dateFormatForURL($scope.OrgDetail.EndDate));
                        } else if (requestTypeID == '1103') {
                            $scope.CreateNew('1103', $scope.OrgDetail.OrganizationID);
                        } else if (requestTypeID == '1104') {
                            $scope.CreateNew('1104', $scope.OrgDetail.OrganizationID);
                        } else if (requestTypeID == '1105') {
                            if ($scope.isLastRecord) {
                                if (validYearSplit($scope.OrgDetail.EndDate))
                                    $scope.CreateNew('1105', $scope.OrgDetail.OrganizationID);
                                else {
                                    $scope.warningText = $scope.Text[$scope.Textcategory].SPLIT_VALIDYEAR9999ONLY;
                                    warningAlert();
                                }
                            } else {
                                $scope.warningText = $scope.Text[$scope.Textcategory].ALLOW_SPLIT_LASTRECORD;
                                warningAlert();
                            }
                        }
                    } else if ($scope.mapData.Type == 'ORGANIZATION') {
                        if ($scope.mapData.RequestType == requestTypeID) {
                            //redirect to document
                            //$('body').removeClass('modal-open');
                            //$('.modal-backdrop').remove();
                            $('#OrganizationModal').modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                            $scope.ViewObjAnnounce($scope.RequestDocument[0].RequestNo, $scope.profile.CompanyCode);
                        } else {
                            $scope.warningText = $scope.Text[$scope.Textcategory].ORGANIZATION_INPROGRESS;
                            warningAlert();
                        }
                    } else if ($scope.mapData.Type == 'POSITION') {
                        $scope.warningText = $scope.Text[$scope.Textcategory].RELATED_TO_POSITION;
                        warningAlert();
                    }
                };

                function warningAlert() {
                    $mdDialog.show({
                        title: $scope.Text['SYSTEM'].WARNING,
                        contentElement: '#myStaticDialog',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        ok: $scope.Text['SYSTEM'].BUTTON_CANCEL
                    });
                }

                $scope.close = function () {
                    $mdDialog.cancel();
                };

                function validYearSplit(date) {
                    var tmpdate = new Date(date);
                    return '9999-12-31' == $filter('date')(tmpdate, 'yyyy-MM-dd');
                }

                function getRequestDocumentDetail(reqNo) {
                    var URL = CONFIG.SERVER + 'HROM/GetRequestDocumentDetailByRequestNo';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { 'REQUESTNO': reqNo }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.RequestDocument = response.data.RequestDocument;
                    }, function errorCallback(response) {
                        console.log('error getRequestDocumentDetail');
                        $scope.loader.enable = false;
                    });
                }
                //#endregion
            }]);
})();