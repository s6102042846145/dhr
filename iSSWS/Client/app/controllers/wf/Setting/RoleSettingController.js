﻿(function () {
    angular.module('DESS')
        .controller('RoleSettingController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {

            /******************* variable start ********************/
            $scope.Textcategory = "ROLE_SETTING";
            $scope.EmployeeID = '';
            $scope.ConfirmDialog = false;
            $scope.iShowDelete = true;
            $scope.NewRole = {
                EmployeeID: '',
                EmployeeName: '',
                SUPERADMIN: false,
                ACCOUNTADMIN: false,
                ACMANAGER: false,
                BEADMIN: false,
                CUSTODIAN: false,
                LETTERADMIN: false,
                OMADMIN: false,
                PAADMIN: false,
                PYADMIN: false,
                SETTINGADMIN: false,
                TEADMIN: false,
                TMADMIN: false,
                FlagDelete : false,
            }

            $scope.loader.enable = true;

            $scope.GetRoleSetting = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetRoleSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mRoleSetting = response.data;
                    console.log('mRoleSetting = ', $scope.mRoleSetting);
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetRoleSetting();

            $scope.GetOrgUnitAll = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetOrgUnitAll';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mOrgUnit = response.data;
                    $scope.mOrgUnits = response.data;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetOrgUnitAll();

            $scope.GetDataSetAdmin = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetUserRoleSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.DataAdmin = response.data;
                    console.log('dataAdmin = ', $scope.DataAdmin);
                    for (i = 0; i <= $scope.DataAdmin.length - 1; i++) {
                        $scope.DataAdmin[i].FlagDelete = false;
                    }
                    if ($scope.DataAdmin.length <= 1)
                        $scope.iShowDelete = false;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetDataSetAdmin();

            $scope.ItemChange = function (item, object) {
                item[object] = !item[object];
            }

            $scope.SaveRoleSetting = function () {
                $scope.iSave = false;
                for (i = 0; i <= $scope.DataAdmin.length - 1; i++) {
                    if ($scope.DataAdmin[i].FlagDelete == false) {
                        if ($scope.DataAdmin[i].SUPERADMIN == true) {
                            $scope.iSave = true;
                            break;
                        }
                    }
                }
                if ($scope.iSave == false) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text[$scope.Textcategory].WARNING)
                            .textContent($scope.Text[$scope.Textcategory].NO_ROLE_SUPERADMIN)
                            .ok('OK')
                    );
                    return;
                }

                $scope.promDialogSaveRole();
            }
            $scope.AddNewRole = function () {
                if ($scope.EmployeeID != "") {
                    $scope.Result = $filter('filter')($scope.DataAdmin, { EmployeeID: $scope.EmployeeID });
                    if ($scope.Result.length > 0) {
                        if ($scope.Result[0].FlagDelete == true) {
                            $scope.Result[0].FlagDelete = false;
                            $scope.ap_pdr_text.orgunit = '';
                            $scope.OrgUnitID = '';
                            $scope.EmployeeID = '';
                        }
                        else {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text[$scope.Textcategory].WARNING)
                                    .textContent(String.format($scope.Text[$scope.Textcategory].DATA_IN_ROLE, $scope.EmployeeID))
                                    .ok('OK')
                            );
                            $scope.ap_pdr_text.orgunit = '';
                            $scope.OrgUnitID = '';
                            $scope.EmployeeID = '';
                            return;
                        }
                    }
                    else {
                        $scope.NewFilter = $filter('filter')($scope.mOrgUnit, { EmployeeID: $scope.EmployeeID });
                        $scope.NewRole.EmployeeID = $scope.NewFilter[0].EmployeeID;
                        $scope.NewRole.EmployeeName = $scope.NewFilter[0].Name;
                        $scope.NewRoleTmp = angular.copy($scope.NewRole);
                        $scope.DataAdmin.push($scope.NewRoleTmp);

                        $scope.NewRole.EmployeeID = "";
                        $scope.NewRole.EmployeeName = "";
                        $scope.NewRole.SUPERADMIN = false;
                        $scope.NewRole.ACCOUNTADMIN = false;
                        $scope.NewRole.ACMANAGER = false;
                        $scope.NewRole.BEADMIN = false;
                        $scope.NewRole.CUSTODIAN = false;
                        $scope.NewRole.LETTERADMIN = false;
                        $scope.NewRole.OMADMIN = false;
                        $scope.NewRole.PAADMIN = false;
                        $scope.NewRole.PYADMIN = false;
                        $scope.NewRole.SETTINGADMIN = false;
                        $scope.NewRole.TEADMIN = false;
                        $scope.NewRole.TMADMIN = false;

                        $scope.ap_pdr_text.orgunit = '';
                        $scope.OrgUnitID = '';
                        $scope.EmployeeID = '';
                    }
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text[$scope.Textcategory].WARNING)
                            .textContent($scope.Text['SYSTEM'].INVALIDEMPLOYEE)
                            .ok('OK')
                    );
                }
            }

            $scope.DeleteRole = function (object) {
                $scope.iCountSupAdmin = 0;
                if (object.SUPERADMIN == 1) {
                    for (i = 0; i <= $scope.DataAdmin.length - 1; i++) {
                        if ($scope.DataAdmin[i].SUPERADMIN == 1 && $scope.DataAdmin[i].FlagDelete == false)
                            $scope.iCountSupAdmin = $scope.iCountSupAdmin + 1;
                    }
                    if ($scope.iCountSupAdmin <= 1) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text[$scope.Textcategory].WARNING)
                                .textContent(String.format($scope.Text[$scope.Textcategory].NO_DELETE_SUPER_ADMIN, object.EmployeeID, object.EmployeeName))
                                .ok('OK')
                        );
                    }
                    else {
                        $scope.promtDialog(true, object);
                    }
                }
                else {
                    object.FlagDelete = true;
                }
            }

            function createFilterForOrgUnit(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeID + " : " + x.Name + "(" + x.OrgUint + ":" + x.Text + ")");
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_orgunit;
            $scope.init_orgunit = function () {
            }
            $scope.querySearchOrgUnit = function (query) {
                if (!$scope.mOrgUnit) return;
                var results = angular.copy(query ? $scope.mOrgUnit.filter(createFilterForOrgUnit(query)) : $scope.mOrgUnit), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemOrgUnitChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.orgunit = item.EmployeeID + " : " + item.Name + "(" + item.OrgUnit + ":" + item.Text + ")";
                    $scope.OrgUnitID = item.EmployeeID;
                    $scope.EmployeeID = item.EmployeeID;
                    tempResult_orgunit = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.orgunit = '';
                }
            };
            $scope.tryToSelect_orgunit = function (text) {
                $scope.ap_pdr_text.orgunit = '';
                $scope.OrgUnitID = '';
                $scope.EmployeeID = '';
            }
            $scope.checkText_orgunit = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.orgunit = '';
                    $scope.OrgUnitID = '';
                    $scope.EmployeeID = '';
                }
            }

            $scope.promtDialog = function (FlagConfirm, objEmployee) {
                var element = FlagConfirm;

                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/confirm.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: $scope.Text[$scope.Textcategory].CONFIRM_DELETE,
                            placeholder: String.format($scope.Text[$scope.Textcategory].DELETE_EMPLOYEE_ROLE, objEmployee.EmployeeID),
                            confirm: $scope.Text[$scope.Textcategory].BTN_CONFIRM,
                            cancel: $scope.Text[$scope.Textcategory].BTN_CANCEL,
                            defaultValue: element,
                        }
                    },
                })
                    .then(function (answer) {
                        if (answer) {
                            $scope.ConfirmDialog = answer;
                            objEmployee.FlagDelete = answer;

                            $scope.iCountSupAdmin = 0;
                            for (i = 0; i <= $scope.DataAdmin.length - 1; i++) {
                                if ($scope.DataAdmin[i].SUPERADMIN == 1 && $scope.DataAdmin[i].FlagDelete == false)
                                    $scope.iCountSupAdmin = $scope.iCountSupAdmin + 1;
                            }
                            $scope.Result = $filter('filter')($scope.DataAdmin, { FlagDelete: false});
                            if ($scope.Result.length <= 1)
                                $scope.iShowDelete = false;

                            if (objEmployee.SUPERADMIN == 1) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text[$scope.Textcategory].WARNING)
                                        .textContent(String.format($scope.Text[$scope.Textcategory].DELETE_SUPER_ADMIN, objEmployee.EmployeeID, objEmployee.EmployeeName, $scope.iCountSupAdmin))
                                        .ok('OK')
                                );
                            }
                        }
                    }, function () {

                    });
            };
            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
                $scope.inputValue = params.defaultValue;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function (answer) {
                    $mdDialog.cancel(answer);
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };
            }

            $scope.promDialogSaveRole = function () {
                $mdDialog.show({
                    controller: promtDialogCtrlSaveRole,
                    templateUrl: 'views/common/dialog/confirm.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: $scope.Text[$scope.Textcategory].WARNING,
                            placeholder: $scope.Text[$scope.Textcategory].CONFIRM_SAVE_ROLE,
                            confirm: $scope.Text[$scope.Textcategory].BTN_CONFIRM,
                            cancel: $scope.Text[$scope.Textcategory].BTN_CANCEL
                        }
                    },
                })
                    .then(function (answer) {
                        if (answer) {
                            $scope.loader.enable = true;
                            var URL = CONFIG.SERVER + 'HRPA/SaveRoleSetting';
                            $scope.employeeData = getToken(CONFIG.USER);
                            var oRequestParameter = {
                                InputParameter: { 'DataRoleSetting': $scope.DataAdmin }
                                , CurrentEmployee: $scope.employeeData
                                , Requestor: $scope.requesterData
                                , Creator: getToken(CONFIG.USER)
                            };
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                $scope.Succes = response.data;
                                $scope.loader.enable = false;

                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text[$scope.Textcategory].WARNING)
                                        .textContent(response.data)
                                        .ok('OK')
                                );
                            }, function errorCallback(response) {
                                $scope.loader.enable = false;
                            });
                        }
                    }, function () {

                    });
            }
            function promtDialogCtrlSaveRole($scope, $mdDialog, params) {
                $scope.params = params;
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function (answer) {
                    $mdDialog.cancel(answer);
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };
            }

            function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

        }]);

})();

