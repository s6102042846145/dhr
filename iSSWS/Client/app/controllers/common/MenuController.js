﻿(function () {
    angular.module('DESS')
        .controller('MenuController', ['$rootScope', '$scope', '$http', '$location', 'CONFIG', function ($rootScope, $scope, $http, $location, CONFIG) {
            var lstExpandedMenu = [];
            $scope.isActiveFastButton = false;

            $scope.toggleFastButton = function () {
                $scope.isActiveFastButton = !$scope.isActiveFastButton;
                if ($('#menuSection').hasClass('navActive')) {
                    $('#menuSection').removeClass('navActive');
                    $("#MenuButtonIcon").toggleClass("eat");
                }
            }

            $scope.init = function () {
                $scope.loadMenu();
            }

            $scope.loadMenu = function () {
                var menuList = (getToken('com.pttdigital.dess.menu'));
                if (menuList == null) {
                    menuList = [];
                }
                // set group class
                var groupClass = 0;
                for (var i = 0; i < menuList.length; i++) {
                    if (lstExpandedMenu.indexOf(menuList[i].groupID) <= -1) {
                        menuList[i].isShow = false;
                    }
                    else {
                        menuList[i].isShow = true;
                    }
                }
                // set initial active
                var newLocation = $location.path();
                var selectedGroupID = '-1';
                var selectedIndex = -1;
                for (var i = 0; i < menuList.length; i++) {
                    if ('/' + menuList[i].url == newLocation || (newLocation == '/frmViewContent/0' && i == 0)) {
                        selectedGroupID = menuList[i].groupID;
                        selectedIndex = i;
                        break;
                    }
                }
                if (selectedGroupID != '-1') {
                    for (var i = 0; i < menuList.length; i++) {
                        menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                        menuList[i].isSelected = false;
                    }
                    menuList[selectedIndex].isSelected = true;
                }
                $rootScope.menus = menuList;

                window.localStorage.removeItem('com.pttdigital.dess.menu');
                window.localStorage.setItem('com.pttdigital.dess.menu', JSON.stringify($rootScope.menus));
            };
           // $scope.loadMenu();

            var onReloadMenuFunction = function (isReloadAfterFinish) {
                var URL = CONFIG.SERVER + 'workflow/GetMenuItem';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    var menuList = response.data;
                    // set group class
                    var groupClass = 0;
                    for (var i = 0; i < menuList.length; i++) {
                        if (lstExpandedMenu.indexOf(menuList[i].groupID) <= -1) {
                            menuList[i].isShow = false;
                        }
                        else {
                            menuList[i].isShow = true;
                        }
                    }

                    menuList = initCurrentUrlData(menuList);

                    // set initial active
                    var newLocation = $location.path();
                    var selectedGroupID = '-1';
                    var selectedIndex = -1;
                    for (var i = 0; i < menuList.length; i++) {
                        if ('/' + menuList[i].url == newLocation || (newLocation == '/frmViewContent/0' && i == 0)) {
                            selectedGroupID = menuList[i].groupID;
                            selectedIndex = i;
                            break;
                        }
                    }
                    if (selectedGroupID != '-1') {
                        for (var i = 0; i < menuList.length; i++) {
                            menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                            menuList[i].isSelected = false;
                        }
                        menuList[selectedIndex].isSelected = true;
                    }

                    $rootScope.menus = menuList;

                    window.localStorage.removeItem(CONFIG.MENU);
                    window.localStorage.setItem(CONFIG.MENU, JSON.stringify($rootScope.menus));

                    if (typeof (isReloadAfterFinish) != 'undefined' && isReloadAfterFinish) {

                        $route.reload();
                    }

                }, function errorCallback(response) {
                    // Error
                    console.log('error reload menu.', response);

                });
            };

            var initCurrentUrlData = function (menuList) {
                // set initial active
                var newLocation = $location.path();
                var isFirstMenu = newLocation === '/frmViewContent/0';



                var isSelected = false;
                for (var m = 0; m < menuList.length; m++) {


                    if (isFirstMenu === true) {
                        menuList[0].isSelected = true;
                    } else {
                        menuList[m].isSelected = false;
                    }

                    if (compareUrl(newLocation, menuList[m].url)) {
                        menuList[m].isSelected = true;
                    } else if (menuList[m].menues && menuList[m].menues.length > 0) {
                        for (var s = 0; s < menuList[m].menues.length; s++) {
                            menuList[m].menues[s].isSelected = false;
                            if (compareUrl(newLocation, menuList[m].menues[s].url)) {
                                menuList[m].isSelected = true;
                                menuList[m].menues[s].isSelected = true;
                                menuList[m].isShow = true;
                                break;
                            }
                        }
                    } else {
                        menuList[m].isSelected = false;
                    }
                }

                var selectHMenu = menuList.filter(function (m) { return m.isShow === true; });

                if (selectHMenu && selectHMenu.length > 0) {
                    menuList.forEach(function (m) {
                        if (m.groupID === selectHMenu[0].groupID && m.menues && m.menues.length > 0) {
                            m.menues.forEach(function (s) {
                                s.isShow = true;
                            });
                        }
                    });
                }

                return menuList;
            };
            var compareUrl = function (currentUrl, menuUrl) {
                return currentUrl === '/' + menuUrl;
            };
            // send function to global
            reloadFunction = onReloadMenuFunction;
            $scope.$on('onReloadMenu', function (event, args) {
                onReloadMenuFunction(false);
            });

            /* route change event */
            $scope.$on('$locationChangeStart', function (event) {

            });
            $scope.$on('$locationChangeSuccess', function (event) {
                var newLocation = $location.path();
                var menuList = $rootScope.menus;
                var selectedGroupID = '-1';
                var selectedIndex = -1;
                for (var i = 0; i < menuList.length; i++) {
                    if ('/' + menuList[i].url == newLocation) {
                        selectedGroupID = menuList[i].groupID;
                        selectedIndex = i;
                        break;
                    }
                }

                if (selectedGroupID != '-1') {
                    for (var i = 0; i < menuList.length; i++) {
                        menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                        menuList[i].isSelected = false;
                    }
                    menuList[selectedIndex].isSelected = true;
                    $rootScope.menus = menuList;

                }
            });
            $scope.currentUrl = function () {
                $location.$$path
            }
            $scope.clickMenu = function (sender, url) {
                closeMenu(sender);
                // Reload Menu
                var args = {};
                $rootScope.$broadcast('onReloadMenu', args);
                // Redirect
                $location.path(url);
            };

            $scope.clickFastButton = function (url) {
                // Redirect
                $location.path('frmViewContent/' + url);
                $scope.isActiveFastButton = false;
            }


            $scope.clickHeadMenu = function (sender) {
                var selectedGroupID = sender.menu.groupID;
                var bIsShow = false;
                for (var i = 0; i < $rootScope.menus.length; i++) {
                    if (selectedGroupID == $rootScope.menus[i].groupID) {
                        $rootScope.menus[i].isShow = !$rootScope.menus[i].isShow;
                        bIsShow = $rootScope.menus[i].isShow;
                    }

                }
                if (bIsShow) {
                    if (lstExpandedMenu.indexOf(selectedGroupID) <= -1) {
                        lstExpandedMenu.push(selectedGroupID);
                    }
                }
                else {
                    var index = lstExpandedMenu.indexOf(selectedGroupID);
                    lstExpandedMenu.splice(index, 1);
                }

            };
            $scope.expandSubMenu = function (menu) {

                if ($scope.menus && $scope.menus.length > 0) {

                    $scope.menus.forEach(function (m) {

                        if (m.groupID === menu.groupID) {
                            m.isShow = !menu.isShow;
                            m.isSelected = !menu.isSelected;

                            if (m.menues && m.menues.length > 0) {
                                m.menues.forEach(function (s) {
                                    s.isShow = m.isShow;
                                    s.isSelected = m.isSelected;
                                });
                            }
                        } else {
                            m.isShow = false;
                            m.isSelected = false;

                            if (m.menues && m.menues.length > 0) {
                                m.menues.forEach(function (s) {
                                    s.isShow = false;
                                    s.isSelected = false;
                                });
                            }
                        }

                    });

                }

            };



            $scope.CreateNew = function (requestTypeId, paramValue) {

                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + +$routeParams.requesterCompanyCode + '/' + paramValue);
                } else {
                    $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + paramValue);
                }
                $scope.isActiveFastButton = false;
            };


        }]);
})();

function closeMenu(obj) {
    $("#menuSection").removeClass("navActive");
    $("#MenuButtonIcon").removeClass("faa-horizontal animated");
    $("#MenuButtonIcon").removeClass("eat");

};


