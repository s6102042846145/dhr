﻿(function () {
    angular.module('DESS')
        .controller('UploadPhotoEmployeeController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {

            /******************* variable start ********************/
            $scope.Textcategory = "UPLOAD_PHOTO_EMPLOYEE";
            $scope.isRadioSelect = 'All';
            $scope.isDisabled = true;
            $scope.EmployeeID = '';
            $scope.chunk = 10;
            $scope.FileType = '';
            $scope.loader.enable = true;
            $scope.currentIndex = 0;
            //All Employee
            $scope.GetEmployeeAll = function () {
                var URL = CONFIG.SERVER + 'HRPY/GetAllEmployee';
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mEmployee = response.data;
                    for (var i = 0; i < $scope.mEmployee.length; i++) {
                        $scope.mEmployee[i].PhotoPath = $scope.mEmployee[i].PhotoPath + "?" + new Date().getTime();
                    }
                    $scope.mEmployees = response.data;
                    for (var i = 0; i < $scope.mEmployees.length; i++) {
                        $scope.mEmployees[i].PhotoPath = $scope.mEmployees[i].PhotoPath + "?" + new Date().getTime();
                    }
                    $scope.FileType = response.data[0].FileType;
                    $scope.SessionEmpID = "";
                    window.localStorage.setItem('tm.search.employee', JSON.stringify($scope.mEmployee));
                    console.log('success GetEmployeeAll.', response);
                }, function errorCallback(response) {
                    console.log('error GetAllEmployee.', response);
                });
            }
            $scope.GetEmployeeAll();

            //For document_comment.html
            $scope.GetPahtFileEmployee = function () {
                var URL = CONFIG.SERVER + 'HRPY/GetPathFileEmployee';
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    for (i = 0; i <= response.data.length - 1; i++) {
                        if (response.data[i].Key == "FILE_TYPE")
                            $scope.FileType = '.' + response.data[i].Value;
                        else if (response.data[i].Key == "PHOTO_PATH")
                            $scope.FilePath = response.data[i].Value;
                    }
                    $scope.loader.enable = false;
                    console.log('success GetPahtFileEmployee.', response);
                }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    console.log('error GetPahtFileEmployee.', response);
                });
            }

            $scope.SearchStorage = window.localStorage.getItem('tm.search.photo');

            if ($scope.SearchStorage != undefined || $scope.SearchStorage != null) {
                $scope.EmployeeID = $scope.SearchStorage;
                $scope.filteredItem = [];
                $scope.data = [];
                window.localStorage.removeItem('tm.search.photo');

                $scope.mEmployee = JSON.parse(window.localStorage.getItem('tm.search.employee'));

                if ($scope.EmployeeID == '') {
                    $scope.ReportData = $scope.mEmployee;
                    $scope.data = angular.copy($scope.ReportData);
                    $scope.filteredItem = $scope.data;
                    $scope.arrSplited = [];

                    var i, j, temparray;
                    for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                        temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                        $scope.arrSplited.push(temparray);
                    }
                    $scope.data.selectedColumn = 0;
                }
                else {
                    $scope.ReportData = $scope.mEmployee;
                    if ($scope.EmployeeID != '') {
                        $scope.ReportData = $filter('filter')($scope.mEmployee, $scope.EmployeeID);
                        $scope.data = angular.copy($scope.ReportData);
                        $scope.filteredItem = $scope.data;

                        $scope.arrSplited = [];
                        var i, j, temparray;
                        for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                            temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                            $scope.arrSplited.push(temparray);
                        }
                        $scope.data.selectedColumn = 0;
                        $scope.ap_pdr_employee = $scope.arrSplited[0][0].EmployeeID + ' : ' + $scope.arrSplited[0][0].Name;
                    }
                }
            }

            function createFilterForEmployee(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeID + " : " + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_employee;
            $scope.init_employee = function () {
            }
            $scope.querySearchEmployee = function (query) {
                if (!$scope.mEmployees) return;
                var results = angular.copy(query ? $scope.mEmployees.filter(createFilterForEmployee(query)) : $scope.mEmployee), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemEmployeeChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_employee = item.EmployeeID + " : " + item.Name;
                    $scope.EmployeeID = item.EmployeeID;
                    tempResult_employee = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_employee = '';
                }
            };
            $scope.tryToSelect_employee = function (text) {
                $scope.ap_pdr_employee = '';
                $scope.EmployeeID = '';
            }
            $scope.checkText_employee = function (text) {
                if (text == '') {
                    $scope.ap_pdr_employee = '';
                    $scope.EmployeeID = '';
                }
                else {
                    for (i = 0; i <= $scope.mEmployee.length - 1; i++) {
                        $scope.EmpCheck = $scope.mEmployees[i].EmployeeID + " : " + $scope.mEmployees[i].Name;
                        if (text == $scope.EmpCheck) {
                            $scope.ap_pdr_employee = text;
                            break;
                        }
                        else
                            $scope.ap_pdr_employee = "";
                    }
                }
            }
            //All Employee

            //All Position
            $scope.GetAllPosition = function () {
               // $scope.loader.enable = true;
                var URL = CONFIG.SERVER + 'HRPY/GetPositionAll';
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mPosition = response.data;
                    $scope.mPositions = response.data;
                    $scope.SessionPositionID = "";
                    $scope.loader.enable = false;
                    console.log('success GetAllPosition.', response);
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('error GetAllPosition.', response);
                });
            }
            $scope.GetAllPosition();
            function createFilterForPosition(query) {

                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.ObjectID + " : " + x.Text + "(" + x.ShortText + ")");
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_position;
            $scope.init_position = function () {
            }
            $scope.querySearchPosition = function (query) {
                if (!$scope.mPositions) return;
                var results = angular.copy(query ? $scope.mPositions.filter(createFilterForPosition(query)) : $scope.mPositions), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemPositionChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.position = item.ObjectID + " : " + item.Text + "(" + item.ShortText + ")";
                    $scope.PositionID = item.ObjectID;
                    tempResult_position = angular.copy(item);
                }
            };
            $scope.tryToSelect_position = function (text) {
                $scope.ap_pdr_text.position = '';
                $scope.PositionID = '';
            }
            $scope.checkText_position = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.position = '';
                    $scope.PositionID = '';
                }
                else {
                    for (i = 0; i <= $scope.mPositions.length - 1; i++) {
                        $scope.PosCheck = $scope.mPositions[i].ObjectID + " : " + $scope.mPositions[i].Text + "(" + $scope.mPositions[i].ShortText + ")";
                        if (text == $scope.PosCheck) {
                            $scope.ap_pdr_text.position = text;
                            break;
                        }
                        else
                            $scope.ap_pdr_text.position = "";
                    }
                }
            }
            //All Position

            //All OrgUnit
            $scope.GetOrgUnitAll = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetOrgUnitAll';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mOrgUnit = response.data;
                    $scope.mOrgUnits = response.data;
                    console.log('success GetOrgUnitAll.', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnitAll.', response);
                });
            }
            $scope.GetOrgUnitAll();

            function createFilterForOrgUnit(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.ObjectID + " : " + x.Text + "(" + x.ShortText + ")");
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_orgunit;
            $scope.init_orgunit = function () {
            }
            $scope.querySearchOrgUnit = function (query) {
                if (!$scope.mOrgUnit) return;
                var results = angular.copy(query ? $scope.mOrgUnit.filter(createFilterForOrgUnit(query)) : $scope.mOrgUnit), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemOrgUnitChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.orgunit = item.ObjectID + " : " + item.Text + "(" + item.ShortText + ")";
                    $scope.OrgUnitID = item.ObjectID;
                    tempResult_orgunit = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.orgunit = '';
                }
            };
            $scope.tryToSelect_orgunit = function (text) {
                $scope.ap_pdr_text.orgunit = '';
                $scope.OrgUnitID = '';
            }
            $scope.checkText_orgunit = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.orgunit = '';
                    $scope.OrgUnitID = '';
                }
            }
            //All OrgUnit

            $scope.SetEnableControl = function (isDisable) {
                $scope.isDisabled = isDisable;
            }

            $scope.filteredItem = [];
            $scope.DataStorage = function () {
                $scope.ReportData = $scope.mEmployee;
            }
            $scope.SearchData = function () {
                if ($scope.ap_pdr_employee == '') {
                    $scope.ReportData = $scope.mEmployee;
                    $scope.data = angular.copy($scope.ReportData);
                    $scope.filteredItem = $scope.data;
                    $scope.arrSplited = [];

                    var i, j, temparray;
                    for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                        temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                        $scope.arrSplited.push(temparray);
                    }
                    $scope.data.selectedColumn = 0;
                    window.localStorage.setItem('tm.search.photo', $scope.ap_pdr_employee);

                }
                else {
                    $scope.ReportData = $scope.mEmployee;
                    if ($scope.ap_pdr_employee != '') {
                        $scope.ReportData = $filter('filter')($scope.ReportData, $scope.EmployeeID);
                        $scope.data = angular.copy($scope.ReportData);
                        $scope.filteredItem = $scope.data;

                        $scope.arrSplited = [];
                        var i, j, temparray;
                        for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                            temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                            $scope.arrSplited.push(temparray);
                        }
                        $scope.data.selectedColumn = 0;
                        window.localStorage.setItem('tm.search.photo', $scope.EmployeeID);
                    }
                }
            }

            $scope.ClearData = function () {
                $scope.ap_pdr_employee = '';

                $scope.EmployeeID = '';
                $scope.PositionID = '';
                $scope.OrgUnitID = '';

                $scope.ReportData = [];
                $scope.arrSplited = [];
            }

            $scope.EditPhotoContent = function (object,Index) {
                $scope.EmpIDForEdit = object.EmployeeID;
                $scope.currentIndex = Index;
            }
            $scope.formInput = {}
            $scope.onAfterValidatePhotoFunc = function (event, fileList) {
                console.log('file validated.', fileList);
                var newFile = {
                    FileID: -1,
                    IsDelete: false,
                    FileSetID: 1,
                    FileName: fileList[0].filename,
                    Data: fileList[0].base64,
                    FileType: fileList[0].filetype,
                    FileSize: fileList[0].filesize
                };
                if ($scope.AllowUploadFile(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (newFile.FileName.substr(newFile.FileName.indexOf('.') + 1, 3) != $scope.FileType) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent(String.format($scope.Text['SYSTEM']['INVALIDFILETYPE_EMPLOYEE'], $scope.FileType))
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else {
                    $scope.EditPhotoByEmployee(newFile);
                }
                newFile = null;
                //$scope.formInput.file = null;
                var idName = '#PA_908_flEmployeePhoto' + $scope.currentIndex;
                angular.element(idName).val(null);//"#document-attachment-file-input"
            };

            $scope.EditPhotoByEmployee = function (objFile) {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'workflow/EditPhotoByEmployee';
                var oRequestParameter = {
                    InputParameter: {
                        'CompanyCode': oEmployeeData.CompanyCode, 'PhotoEmployee': objFile, 'EmployeeID': $scope.EmpIDForEdit
                    },
                    CurrentEmployee: getToken(CONFIG.USER)
                }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    window.location.reload();
                    console.log('Success EditPhotoByEmployee.', response);
                }, function errorCallback(response) {
                        console.log('error EditPhotoByEmployee.', response);
                });
            }

            //Paging
            $scope.currentPage = 0;
            $scope.pageSize = 1;
            $scope.pageLeft = function () {
                if ($scope.data.selectedColumn > 0) {
                    $scope.data.selectedColumn = $scope.currentPage - 1;
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.afterClickPlage = $scope.data.selectedColumn;

                }
            }
            $scope.pageRight = function () {
                if ($scope.data.selectedColumn < $scope.arrSplited.length - 1) {
                    $scope.data.selectedColumn = $scope.currentPage + 1;
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.afterClickPlage = $scope.data.selectedColumn;
                }
            }
            $scope.afterClickPlage = 0;
            $scope.selectPage = function (page) {
                $scope.currentPage = page - 1;
                $scope.afterClickPlage = page;
                $scope.data.selectedColumn = page - 1;
                for (i = 0; i <= $scope.data.length - 1; i++) {
                    $scope.data[i].selected = false;
                }
            }
            //Paging

        }]);

})();

