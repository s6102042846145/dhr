﻿(function () {
    angular.module('DESS')
        .controller('SwitchPositionController', ['$rootScope', '$scope', '$http', '$document', '$window', '$route', '$location', 'CONFIG', '$timeout', '$mdDialog', '$routeParams', function ($rootScope, $scope, $http, $document, $window, $route, $location, CONFIG, $timeout, $mdDialog, $routeParams) {
            //Nattawat S. 2021-01-25
            $scope.PositionSelected = getToken(CONFIG.POSITION_SELECTED);
            $scope.CurrentEmployeeAllPosition = getToken(CONFIG.USER).AllPosition;
            $scope.CurrentEmployeeLanguage = getToken(CONFIG.USER).Language;
            $scope.SelectedPosition = angular.copy($scope.CurrentEmployeeAllPosition[0]);
            //End add

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.PassingDocument = { document: null };
            $scope.MODULE_SETTING = CONFIG.MODULE_SETTING;
            $scope.PAGE_SETTING = CONFIG.PAGE_SETTING;
            $scope.PROFILE_SETTING = { POSITIONID: '' };
            $scope.CONFIG_SERVER = CONFIG.SERVER;

            //Nattawat S. Add Org mapping to position
            $scope.OragnaizationData = [];
            $scope.OrganizationRelationData = [];

            // //Pariyaporn P. Add 2021-03-10 for validate display footer
            //window.localStorage.setItem('currentPageIsSwitchPositon', true);


            $scope.GetRelation = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1001GetAllHistory';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var currentDate = new Date();
                    $scope.OrganizationRelationData = response.data.filter(data => data.ObjectType == 'S' && data.NextObjectType == 'O' && data.Relation == 'Belong To' && (currentDate >= new Date(data.BeginDate) && currentDate <= new Date(data.EndDate)));

                    for (i = 0; i < $scope.CurrentEmployeeAllPosition.length; i++) {
                        var PositionBelongToUnit = angular.copy($scope.OrganizationRelationData.filter(data => data.ObjectID.split(":")[0] == $scope.CurrentEmployeeAllPosition[i].ObjectID));
                        var PositionBelongToName = angular.copy($scope.OragnaizationData.filter(data => data.ObjectID == PositionBelongToUnit[0].NextObjectID.split(':')[0]));
                        PositionBelongToName = $scope.CurrentEmployeeLanguage == 'EN' ? PositionBelongToName[0].TextEn : PositionBelongToName[0].Text;

                        $scope.CurrentEmployeeAllPosition[i].BelongToUnit = PositionBelongToUnit[0].ObjectID.split(':')[0];
                        $scope.CurrentEmployeeAllPosition[i].BelongToName = PositionBelongToName;

                    }

                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            };

            $scope.GetOrganizationDetail = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetAllHistory';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var currentDate = new Date();
                    $scope.OragnaizationData = response.data.filter(data => data.ObjectType == 'O' && (currentDate >= new Date(data.BeginDate) && currentDate <= new Date(data.EndDate)));
                    $scope.GetRelation();
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            };
            $scope.GetOrganizationDetail();

            //Nattawat S. Add 2021-01-25
            $scope.selectPositionAfterLogin = function () {
                var Position = $scope.SelectedPosition;
                var NewPositionID = Position.ObjectID;
                var NewPositionName = $scope.CurrentEmployeeLanguage == 'EN' ? Position.TextEn : Position.Text;

                $scope.PROFILE_SETTING.POSITIONID = NewPositionID;
                $scope.employeeData.PositionID = NewPositionID;
                $scope.employeeData.Position = NewPositionName;
                $scope.employeeData.RequesterPositionID = NewPositionID;

                //$scope.PositionSelected = true;
                window.localStorage.setItem(CONFIG.USER, JSON.stringify($scope.employeeData));
                window.localStorage.setItem(CONFIG.POSITION_SELECTED, true);
                //window.localStorage.setItem('currentPageIsSwitchPositon', false);
                window.location.reload();
                //window.location.replace("index.html");
                //window.location('index.html').replace();

            };

        }]);

})();