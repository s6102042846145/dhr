﻿
var app = angular.module('jobUI', []);
app.controller('JobUIController', function ($http, $scope, $sce) {

    $scope.JobID = 0;
    $scope.selCompany;
    $scope.errorText = 'Please input Job id.';

    $scope.jobLoader = {
        enable: false
    };

    $scope.init = function () {
        $scope.jobLoader.enable = true;
        GetCompanyList();
        $scope.checkJobIDLength();
    }

    function GetCompanyList() {
        console.log("GetCompanyList");
        var MoDule = 'Share/';
        var Functional = 'GetCompanyList';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log(response.data);
            $scope.CompanyList = response.data;
            $scope.CompanyLogo = "";
            $scope.CompanyCodeDefault = "";

            $scope.selCompany = $scope.CompanyList[0];
            $scope.jobLoader.enable = false;
        }, function errorCallback(response) {
            // Error
            console.log('error CompanyController.', response);
        });
    };

    function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

    $scope.clearCriteria = function () {
        $scope.selCompany = $scope.CompanyList[0];
        $scope.JobID = 0;
    }

    $scope.runJob = function () {

        $scope.JobResult = 'Job is in processing...';
        $scope.isJobIDIsBlank = false;
        $scope.jobLoader.enable = true;
        //var params = 'RUNJOB|';
        //params = params + $scope.selCompany.CompanyCode + '|';
        //params = params + $scope.JobID;

        var URL = 'http://localhost:5555/' + 'JOB/runJob';//CONFIG.SERVER
        var oRequestParameter = { InputParameter: { Task: 'RUNJOB', JobID: $scope.JobID, CompanyCode: $scope.selCompany.CompanyCode } };
        return $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.jobLoader.enable = false;
            $scope.JobResult = response.data;
        },
            function errorCallback(response) {
                console.error(response);
                $scope.jobLoader.enable = false;
                return response;
            });

    };

    $scope.renderHtml = function (html_code) {
        return $sce.trustAsHtml(html_code);
    };

    $scope.ifBlankAutoZero = function () {
        if ($scope.JobID == null || $scope.JobID == '' || $scope.JobID == 0) {
            $scope.JobID = 0;
        } 

        document.getElementById("txtJobID").value = document.getElementById("txtJobID").value.replace(/^0+/, '');
    }

});


