﻿(function () {
    angular.module('ESSMobile')
        .controller('AirFareViewController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

            $scope.airfareList = [
                //{
                //    EmployeeID: '245801115',
                //    Name: 'นางจิราวรรณ สงวนสิน',
                //    BeginDate: '15/12/2016',
                //    TravelTime: '10.00',
                //    BookingDesc: 'Air Asia FD-3447 Bangkok-Chaing Mai'
                //},
                //{
                //    EmployeeID: '245801115',
                //    Name: 'นางจิราวรรณ สงวนสิน',
                //    BeginDate: '17/12/2016',
                //    TravelTime: '8.00',
                //    BookingDesc: 'Air Asia FD-3447 Bangkok - Chaing Mai'
                //},
                //{
                //    EmployeeID: '24540222',
                //    Name: 'นาบเอกราช ตัณยะบุตร',
                //    BeginDate: '21/12/2016',
                //    TravelTime: '17.00',
                //    BookingDesc: 'Lion Air SL 520 Chaing Mai - Bangkok '
                //},
                //{
                //    EmployeeID: '24540222',
                //    Name: 'นาบเอกราช ตัณยะบุตร',
                //    BeginDate: '21/12/2016',
                //    TravelTime: '17.00',
                //    BookingDesc: 'Lion Air SL 520 Chaing Mai - Bangkok'
                //} 
                
            ];
            $scope.column_header = [
                 $scope.Text['ACCOUNT_SETTING']['EMPLOYEE_ID'], $scope.Text['EXPENSE']['FULLNAME'], $scope.Text['EXPENSE']['DATEFROM'], $scope.Text['EXPENSE']['DATETO'], $scope.Text['ACCOUNT_SETTING']['DETAIL'],
            ];

            $scope.init = function () {
                
                getAirfareList();
            }

            function getAirfareList() {
                var URL = CONFIG.SERVER + 'HRTR/GetAirfareView';
                var oRequestParameter = { InputParameter: { REQUESTNO: $scope.referRequestNo, TYPE: $scope.TRAVELMODE }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data) {
                        $scope.airfareList = response.data;
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error AirFareViewController InitialConfig.', response);
                    $scope.setBlockAction(false, "");
                });

            }


        }]);
})();