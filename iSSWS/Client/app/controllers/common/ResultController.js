﻿(function () {
    angular.module('DESS')
        .controller('ResultController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', '$mdDialog', 'CONFIG', function ($rootScope, $scope, $http, $routeParams, $location, $mdDialog, CONFIG) {
            $scope.runtime = Date.now();
            //MIN DATE 1970/01/01
            $scope.MinDate = new Date(0);

            $scope.loader.enable = false;

            $scope.document = $scope.$parent.PassingDocument.document;
            

            var oRequestParameter = { InputParameter: { 'RequestNo': $scope.document.RequestNo }, CurrentEmployee: getToken(CONFIG.USER)}
            var URL = CONFIG.SERVER + 'HRPA/GetStatusForResult/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                if (response.data != null) {
                    $scope.document.CurrentFlowItemCode = response.data;
                }
                console.log('----------- GetStatusForResult. -----------', angular.copy($scope.Text));
            }, function errorCallback(response) {
                // Error
                $scope.Text = null;
                console.log('error GetStatusForResult.', response);
            });
            console.log('result document.', $scope.document);

            $scope.isViewResult = true;
            if ($scope.document) {
                if ($scope.document.PopupTitle) {
                    var parentEl = angular.element(document.body);
                    $mdDialog.show({
                        parent: parentEl,
                        template:
                          '<md-dialog aria-label="List dialog" style=" width: 100%; max-width: 700px;height: 100%;max-height: 420px;  padding: 10px;">' +
                          '  <md-dialog-content class="-txt-blue" style="    height: 65px; font-size:20px; padding: 15px;">' +
                          '     {{description.title}}' +
                          '  </md-dialog-content>' +
                          '  <md-dialog-actions style=" border: none; position:relative; ">' +
                          '    <div class="alert-warning" role="alert" style="width: 100%; padding:15px; height:290px; overflow-y:scroll; overflow-x:hidden;">' +
                          '      <div style="font-size:20px; word-wrap: break-word;" ng-bind-html="description.box_title | nl2br"></div>' +
              
                          '     <div class="clearfix"></div>' +
                          '    </div>' +
                          '  </md-dialog-actions>' +

                          '    <md-button ng-click="closeDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 0;  font-size: 20px">' +
                          '      OK' +
                          '    </md-button>' +
                          '</md-dialog>',
                        locals: {
                            description: {
                                title: 'INFORMATION', // get text error to display in the dialog
                                box_title: $scope.document.PopupMessage
                            }
                        },
                        controller: DialogController
                    });
                    function DialogController($scope, $mdDialog, description) {
                        $scope.comment = '';
                        $scope.description = description;

                        $scope.closeDialog = function () {
                            $mdDialog.hide();
                        }
                     
                    }
                }
            }

            $scope.openViewContent = function (requestType) {
                console.log('line 986 openViewContent');
                $location.path('/frmViewContent/' + requestType);
            };
            //$scope.openViewContent = function (requestType) {
            //    console.log('openViewContent');
            //    $location.path('/frmViewContent/' + requestType);
            //};
        }]);
})();