﻿
//var SERVER = CONFIG.SERVER;
//var SERVER = 'https://dessdev.pttdigital.com/';
var SERVER = "http://localhost:5555/";
var adminKey;

function checkAuthorize() {
    if (window.location.hash.indexOf('#/ByPassTicket/') >= 0) {
        return true;
    }
    console.log('checkAuthorize');
    var pageName = location.pathname;
    // if (pageName === '/start.html') {
    if (pageName === '/login_admin.html') {
        setInterval(function () {
            authorize();
        }, 1000);
    } else {
        authorize();
    }

    function authorize() {
        if (checkToken()) {
            // if (pageName === '/start.html') {
            if (pageName === '/login_admin.html') {
                toApplication();
            }
        } else {
            window.localStorage.removeItem('com.pttdigital.dess.authorizeUser');
            window.localStorage.removeItem('com.pttdigital.dess.announcement');
            window.localStorage.removeItem('com.pttdigital.dess.menu');
            toLogin();
        }

        function checkToken() {
            var result = false;
            if (hasToken()) {
                result = checkTokenWithWebService(getEmployeeDataToken());
            }
            return result;

            function checkTokenWithWebService(userData) {
                // check token with webservice
                //
                return true;
            }
        }
    }
}

function login() {
    // add code here test login

    var tryGetAuthen = localStorage.getItem('com.pttdigital.dess.authorizeUser')
    console.debug(tryGetAuthen);
    if (tryGetAuthen !== null) {
        document.getElementById('ErrorMessage').style.visibility = "visible";
        document.getElementById('ErrorMessage').innerHTML = "Another user is logged in, please check. Otherwise, please contact system administrator.";
        return;
    }


    setLoader(true);
    var inputUsername = document.getElementById('CORE_txtInputAccount').value;
    var inputPassword = document.getElementById('CORE_txtInputPassword').value;
    var x = document.querySelector("select");
    var inputCompanyCode = x.options[x.selectedIndex].value;
    var inputCompanyName = x.options[x.selectedIndex].label;
    console.log(inputCompanyCode);

    document.getElementById('ErrorMessage').style.visibility = "hidden";
    if (inputUsername !== null && inputPassword !== null && inputUsername.length > 0 && inputPassword.length > 0
        && inputCompanyCode !== null && inputCompanyCode.length > 0) {
        document.getElementById('ErrorMessage').style.visibility = "hidden";
        var currentLocation = window.location;
        var key = gup('key', currentLocation);
        if (key === adminKey && key !== '') {
            authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
        } else {
            authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
        }

    } else {
        document.getElementById('ErrorMessage').style.visibility = "visible";
        document.getElementById('ErrorMessage').innerHTML = "Please input Username/Password/Company";
        setLoader(false);
    }

    function authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
        //// check username, password with webservice
        var URL = SERVER + 'workflow/authenticate';
        var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            //dataType: 'json',
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data.ErrorMessage === "") {
                        //window.localStorage.setItem('com.pttdigital.ess.authorizeUser', JSON.stringify(data.EmployeeData));
                        //window.localStorage.setItem('com.pttdigital.ess.announcement', JSON.stringify(data.Announcement));
                        //window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify(data.Menu));
                        //toApplication();
                        validateEmployeeData(data);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });

        //$.post(URL, { '': JSON.stringify(x) })
        //.done(function (data) {
        //    alert(data);
        //});

    }

    function authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
        var URL = SERVER + 'workflow/AuthenticateWithOutPassword';
        var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data.ErrorMessage === "") {
                        validateEmployeeData(data);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });
    }


}

function validateEmployeeData(oData) {


    var URL = SERVER + 'Workflow/ValidateData';

    var oRequestParameter = { InputParameter: {}, RequestDocument: {}, CurrentEmployee: {}, Creator: {}, Requestor: oData.EmployeeData };

    $.ajax({
        type: 'POST',
        data: oRequestParameter,
        url: URL,
        success:
            function (data, textStatus, XMLHttpRequest) {
                if (data !== null && data.length > 0) {
                    // error config
                    var errorMsg = '';
                    for (var i = 0; i < data.length; i++) {
                        errorMsg += '<b>' + data[i].TableName + '</b> : ' + data[i].ErrorMessage + '<br>';
                    }
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = errorMsg;
                    setLoader(false);
                }
                else {
                    // config complete

                    window.localStorage.setItem('com.pttdigital.dess.authorizeUser', JSON.stringify(oData.EmployeeData));
                    window.localStorage.setItem('com.pttdigital.dess.announcement', JSON.stringify(oData.Announcement));
                    window.localStorage.setItem('com.pttdigital.dess.menu', JSON.stringify(oData.Menu));
                    toApplication();

                }
            },
        error:
            function (XMLHttpRequest, textStatus, errorThrown) {
                document.getElementById('ErrorMessage').style.visibility = "visible";
                document.getElementById('ErrorMessage').innerHTML = errorThrown;
                setLoader(false);
            }
    });

}

function logout() {
    console.log('logout');
    window.localStorage.removeItem("com.pttdigital.dess.authorizeUser");
    window.localStorage.removeItem("com.pttdigital.dess.announcement");
    window.localStorage.removeItem('com.pttdigital.dess.menu');
    window.localStorage.removeItem('CurrentCompanyTheme');
    window.localStorage.removeItem('CurrentCompanyLogo');
    window.location = 'login_admin.html' + window.localStorage.getItem('com.pttdigital.dess.pathcookie'); //getToken(CONFIG.PATHCOOKIES);//document.cookie;//'start.html';
    localStorage.clear();
}

function toApplication() {
    window.location.replace("index.html");//'index.html';
}

function toLogin() {
    window.location = 'login_admin.html';
}

function hasToken() {
    var jsonIsValid;
    try {
        var jsonResult = JSON.parse(window.localStorage.getItem('com.pttdigital.dess.authorizeUser'));
        jsonIsValid = true;
    } catch (e) {
        console.log('token error')
        window.localStorage.removeItem("com.pttdigital.dess.authorizeUser");
        jsonIsValid = false;
    };
    return window.localStorage.getItem('com.pttdigital.dess.authorizeUser') !== null && jsonIsValid;
}

function getEmployeeDataToken() {
    var objEmployeeData = null;
    try {
        objEmployeeData = JSON.parse(window.localStorage.getItem('com.pttdigital.dess.authorizeUser'));
    } catch (e) {
        console.log('token error')
        window.localStorage.removeItem("com.pttdigital.dess.authorizeUser");
    };
    return objEmployeeData;
}

function getToken(KEY) {
    var obj = null;
    try {
        obj = JSON.parse(window.localStorage.getItem(KEY));
    } catch (e) {
        console.log('token error')
        window.localStorage.removeItem(KEY);
    };
    return obj;
}

function setLoader(enable) {
    if (enable) {
        $('#CORE_txtInputAccount').prop('disabled', true);
        $('#CORE_txtInputPassword').prop('disabled', true);
        $('#login_button').prop('disabled', true);
        $('#inputCompany').prop('disabled', true);
        $('#login_button_icon').hide();
        $('#login_loader').show()
    } else {
        $('#CORE_txtInputAccount').prop('disabled', false);
        $('#CORE_txtInputPassword').prop('disabled', false);
        $('#login_button').prop('disabled', false);
        $('#inputCompany').prop('disabled', false);
        $('#login_loader').hide();
        $('#login_button_icon').show();
    }
}

function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}

var app = angular.module('LoginApp', []);
app.controller('CompanyController', function ($http, $scope) {
    $scope.event = {
        isEvent: false,
        hiddenVisit: true
    }
    $scope.CompanyLogin = 'DEFAULT';

    // single signon
    function AuthenticatewithWindowsAuthen() {
        var currentLocation = window.location;
        var KEY_COMPANY_CODE = gup('cc', currentLocation);
        var isFoundCompany = false;

        if (KEY_COMPANY_CODE == null)
            KEY_COMPANY_CODE = '';

        for (var i = 0; i < $scope.CompanyList.length; i++) {

            var decrypted = CryptoJS.AES.decrypt(KEY_COMPANY_CODE, "pttdigitalsolution");
            var currentCompanyCode = decrypted.toString(CryptoJS.enc.Utf8);

            if (currentCompanyCode.includes($scope.CompanyList[i].CompanyCode)) {
                isFoundCompany = true;
                $scope.CompanyLogin = $scope.CompanyList[i].Name;
                break;
            }

            //if (KEY_COMPANY_CODE === $scope.CompanyList[i].CompanyCode) {
            //    isFoundCompany = true;
            //    $scope.CompanyLogin = $scope.CompanyList[i].Name;
            //    break;
            //}
        }
        if (!isFoundCompany) {
            $scope.CurrentCompanyCode = KEY_COMPANY_CODE;
            return;
        }


        console.log("AuthenticateWindows");

        var MoDule = 'workflow/';
        var Functional = 'AuthenticateWindows';
        //var URL = SERVER + MoDule + Functional + "?CompanyCode=" + KEY_COMPANY_CODE;
        var URL = SERVER + MoDule + Functional;
        var oRequestParameter = { InputParameter: { "CompanyCode": KEY_COMPANY_CODE } };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            var data = response.data;
            console.log('success -- authen');
            if (data.ErrorMessage === "") {
                validateEmployeeData(data);
            }
            else {
                document.getElementById('ErrorMessage').style.visibility = "visible";
                document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage + ' AD1 : ' + data.UserAuthenAD1 + ' AD2 : ' + data.UserAuthenAD2;
                setLoader(false);
            }
            $scope.CompanyList = response.data;

        }, function errorCallback(response) {
            // Error
            console.log('error AuthenticatewithWindowsAuthen.', response);
        });
    };


    // external sign on
    function AuthenticatewithExternalUser() {
        var currentLocation = window.location;
        var KEY_TOKEN = gup('ex', currentLocation);
        var KEY_COMPANY = gup('ex_cc', currentLocation);
        if (!KEY_TOKEN) return;
        if (!KEY_COMPANY) return;
        var MoDule = 'workflow/';
        var Functional = 'AuthenticateForExternalUser';
        var URL = SERVER + MoDule + Functional + "?EncryptKey=" + KEY_TOKEN + "|" + KEY_COMPANY;
        console.log(URL);
        $http({
            method: 'GET',
            url: URL,
            //data: oParams
        }).then(function successCallback(response) {
            var data = response.data;
            console.debug(data);
            console.log('success -- AuthenticateForExternalUser');
            if (data.ErrorMessage === "") {
                window.localStorage.setItem('com.pttdigital.dess.authorizeUser', JSON.stringify(data.EmployeeData));
                window.localStorage.setItem('com.pttdigital.dess.announcement', JSON.stringify(data.Announcement));
                window.localStorage.setItem('com.pttdigital.dess.menu', JSON.stringify(data.Menu));
                toApplication();
            }
            else {
                setLoader(false);
            }

        }, function errorCallback(response) {
            // Error
            console.log('error AuthenticateForExternalUser.', response);
        });
    }


    $scope.GetCompanyList = function () {
        console.log("GetCompanyList");
        var MoDule = 'Share/';
        var Functional = 'GetCompanyList';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log('suAuthenticatewithWindowsAuthenccess');
            console.log(response.data);
            $scope.CompanyList = response.data;
            console.log("$scope.CompanyList", $scope.CompanyList);
            $scope.CompanyLogo = "";
            $scope.CompanyBackground = "images/dess-background.jpg";

            //$scope.CompanyBackground = "";
            $scope.CompanyIndexSel = '';
            $scope.CompanyCodeDefault = gup('cc', window.location); // = null ? "" : gup('cc', window.location);

            if ($scope.CompanyCodeDefault === null) {
                $scope.CompanyCodeDefault = "";
                $scope.CompanyLogo = "";
            }
            //keep params of URL that company
            window.localStorage.setItem('com.pttdigital.dess.pathcookie', '?cc=' + $scope.CompanyCodeDefault);

            window.localStorage.removeItem('CurrentCompanyTheme');
            window.localStorage.removeItem('CurrentCompanyLogo');
            for (i = 0; i < $scope.CompanyList.length; i++) {
                var decrypted = CryptoJS.AES.decrypt($scope.CompanyCodeDefault.toString(), "pttdigitalsolution");
                var currentCompanyCode = decrypted.toString(CryptoJS.enc.Utf8);

                if (currentCompanyCode.includes($scope.CompanyList[i].CompanyCode)) {
                    $scope.CompanyBackground = $scope.CompanyList[i].Background == null ? "images/dess-background.jpg" : $scope.CompanyList[i].Background;
                    $scope.CompanyIndexSel = i;
                    $scope.CompanyLogo = $scope.CompanyList[i].Logo == null ? '' : $scope.CompanyList[i].Logo;
                    window.localStorage.setItem('CurrentCompanyLogo', $scope.CompanyLogo);
                    window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/' + $scope.CompanyList[i].Theme);
                    break;
                } else {
                    window.localStorage.setItem('CurrentCompanyLogo', '');
                    window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/Default.css');
                }
            }

            AuthenticatewithWindowsAuthen();
            AuthenticatewithExternalUser();

        }, function errorCallback(response) {
            // Error
            console.log('error CompanyController.', response);
        });
    };
    $scope.GetCompanyList();

    $scope.GetEnvironment = function () {
        console.log("GenEnvironment");
        var MoDule = 'Share/';
        var Functional = 'GetEnvironment';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log('GetEnvironment Success');
            console.log(response.data['DataBaseLocation']);
            $scope.Environment = response.data['DataBaseLocation'];
        }, function errorCallback(response) {
            // Error
            console.log('error GetEnvironment.', response);
        });
    };
    $scope.GetEnvironment();

    $scope.switchThemeByCompany = function (CompanyInfo) {
        // alert(comp);
        //set css theme
        if (CompanyInfo.Theme !== null && CompanyInfo.Theme !== '') {
            window.localStorage.setItem('CurrentCompanyLogo', CompanyInfo.Logo);
            window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/' + CompanyInfo.Theme);
        } else {
            window.localStorage.setItem('CurrentCompanyLogo', '');
            window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/Default.css');
        }
        //set background 
        if (CompanyInfo.Background !== null && CompanyInfo.Background !== '') {
            $scope.CompanyBackground = CompanyInfo.Background == null ? "images/dess-background.jpg" : CompanyInfo.Background;
        } else {
            $scope.CompanyBackground = "images/dess-background.jpg";
        }
    }

    function getLockInBlockSetting() {
        var MoDule = 'workflow/';
        var Functional = 'GetLoginBlockSetting';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
        }).then(function successCallback(response) {
            $scope.event.isEvent = (response.data.IsEvent.toLowerCase() !== 'false');
            adminKey = response.data.AdminKey;
            if ($scope.event.isEvent) {
                var currentLocation = window.location;
                var key = gup('key', currentLocation);
                if (key === adminKey) {
                    $scope.event.isEvent = false;
                }

            }
        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });
    }

    $scope.RegisterADdminBypass = function RegisterADdminBypass() {
        var inputUsername = document.getElementById('CORE_txtInputAccount').value;
        var inputEmail = document.getElementById('CORE_txtInputEmail').value;
        var CompanyCode = "";
        var CompanyName = "";
        var CurrentCompany = $scope.CompanyList[0].CompanyCode;
        for (i = 0; i < $scope.companySelectedList.length; i++) {

            if ($scope.companySelectedList[i].Selected == true) {

                CompanyCode += ($scope.companySelectedList[i].CompanyCode) + "|";
                CompanyName += ($scope.companySelectedList[i].CompanyName) + "|";
            }

        }
        if (inputUsername == "" || inputEmail == "") {
            $scope.danger = "กรุณากรอก username และ email ให้ครบถ้วน";
            $("#CORE_divErrorModal").modal("show");
            return;
        }


        var MoDule = 'workflow/';
        var Functional = 'Admin_bypass_register';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        var oRequestParameter = {
            InputParameter: { "Username": inputUsername, "password": "", "email": inputEmail, "firstlogin": 'F', "responseid": "", "CompanyCode": CompanyCode, "CompanyName": CompanyName, "CurrentCompany": CurrentCompany }
        };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            if (response.data == "Success") {
                location.replace(SERVER + "Client/login_admin.html");
            }
            else {
                $scope.danger = response.data;
                $("#CORE_divErrorModal").modal("show");
            }
        }, function errorCallback(response) {

            console.log('error blockseting.', response);
        });
    }
    $scope.tipscheck = true;
    $scope.tips = "กรุณากรอก Username ที่ท่านต้องการ reset รหัสผ่าน";
    $scope.Clearmodal = function ClaerModal() {
        document.getElementById('AccountforReset').value = "";
        $scope.tipscheck = true;
        $scope.error = "";
        $scope.danger = "";
    }

    $scope.AdminBypasslogin = function AdminBypasslogin() {
        // add code here test login
        var inputAccount = document.getElementById('CORE_txtInputAccount').value;
        var inputAdminUsername = document.getElementById('CORE_txtInputAccountAdmin').value;
        var inputPassword = document.getElementById('CORE_txtInputPassword').value;
        var x = document.querySelector("select");
        var inputCompanyCode = x.options[x.selectedIndex].value;
        var inputCompanyName = x.options[x.selectedIndex].label;
        var MoDule = 'workflow/';
        var Functional = 'Admin_bypass_login';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        var oRequestParameter = { InputParameter: { "Username": inputAdminUsername, "password": inputPassword, "flag": "T", "Company": inputCompanyCode } };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.status = response.data;
            if ($scope.status == "F") { $("#CORE_divChange_Pass_Modal").modal("show"); }
            else if ($scope.status != "Error" && $scope.status != "Password Expired" && $scope.status != "" && $scope.status != "Wrong Username" && $scope.status != "Wrong Password" && $scope.status != "Company ไม่ตรงกับที่ Register ไว้") {
                authenWebByPassAdmin(inputAdminUsername, inputAccount, inputPassword, inputCompanyCode, inputCompanyName);
            }

            else {
                AdminBypassSaveLog("Fail" + " - " + $scope.status);
                $scope.danger = $scope.status;
                $("#CORE_divErrorModal").modal("show");
            }
        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });

        function authenWebByPassAdmin(inputAdminUsername, inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
            var URL = SERVER + 'workflow/AuthenticateWithOutPassword';
            var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
            $.ajax({
                type: 'POST',
                data: oUserLogin,
                url: URL,
                success:
                    function (data, textStatus, XMLHttpRequest) {
                        if (data.ErrorMessage === "") {
                            validateEmployeeData(data);
                            AdminBypassSaveLog("Pass");
                        }
                        else {

                            //document.getElementById('ErrorMessage').style.visibility = "visible";
                            //document.getElementById('ErrorMessage').innerHTML = "Wrong EmployeeID";
                            $scope.danger = "Wrong EmployeeID";
                            $("#CORE_divErrorModal").modal("show");
                            AdminBypassSaveLog("Fail " + " - " + $scope.danger);
                            setLoader(false);
                        }
                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                        setLoader(false);
                    }
            });
        }

    }
    //$scope.danger = "กรุณาตั้งPasswordใหม่ที่ท่านต้องการ";
    $scope.AdminBypassChangepassword = function AdminBypassChangepassword() {
        // add code here test login
        var inputAdminUsername = document.getElementById('CORE_txtInputAccountAdmin').value;
        var Newpassword = document.getElementById('Newpassword').value;
        var Confirmpassword = document.getElementById('Confirmpassword').value;
        var x = document.querySelector("select");
        var inputCompanyCode = x.options[x.selectedIndex].value;
        var inputCompanyName = x.options[x.selectedIndex].label;
        if (Newpassword != Confirmpassword) {
            $scope.danger = "รหัส Newpassword และ Confirmpassword ไม่ตรงกัน";
            $("#CORE_divChange_Pass_Modal").modal("show");
            return;
        }
        var MoDule = 'workflow/';
        var Functional = 'Admin_bypass_login';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        var oRequestParameter = { InputParameter: { "Username": inputAdminUsername, "password": Newpassword, "flag": "F", "Company": inputCompanyCode } };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.status = response.data;
            if ($scope.status == "T") {
                $("#CORE_divChange_Pass_Modal").modal('hide');
                document.getElementById('Newpassword').value = "";
                document.getElementById('Confirmpassword').value = "";
                $scope.danger = 'ตั้งค่ารหัสผ่านส่วนตัวสำเร็จ!'
                $("#CORE_divErrorModal").modal('show');
            }

        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });

    }

    $scope.AdminBypassResetpassword = function AdminBypassResetpassword() {
        // add code here test login
        var inputAdminUsername = document.getElementById('AccountforReset').value;
        var x = document.querySelector("select");
        var inputCompanyCode = x.options[x.selectedIndex].value;
        var inputCompanyName = x.options[x.selectedIndex].label;
        var MoDule = 'workflow/';
        var Functional = 'AdminBypassResetPassword';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        var oRequestParameter = { InputParameter: { "Username": inputAdminUsername, "flag": "R", "Company": inputCompanyCode, "CompanyName": inputCompanyName } };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.status = response.data;
            if ($scope.status != "Success") {
                $scope.tipscheck = false;
                $scope.error = $scope.status;

            }
            else {
                $("#CORE_divReset_Pass_Modal").modal('hide');
                $scope.danger = "ทำการ Reset รหัสผ่านแล้ว , กรุณาตรวจสอบ email inbox ตามที่ Register ไว้ (หากไม่ได้รับ email กรุณาติดต่อ Admin)";
                $("#CORE_divErrorModal").modal('show');
                document.getElementById('AccountforReset').value.clear;
            }


        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });

    }
    $.getJSON("https://api.ipify.org?format=json",
        function (data) {

            // Setting text of element P with id gfg 
            $scope.data_ip = data.ip;
        })
    function AdminBypassSaveLog(description) {
        // add code here test login

        var client_name = location.host;
        var inputAdminUsername = document.getElementById('CORE_txtInputAccountAdmin').value;
        var inputAccount = document.getElementById('CORE_txtInputAccount').value;
        var description = description;
        var x = document.querySelector("select");
        var inputCompanyCode = x.options[x.selectedIndex].value;
        var inputCompanyName = x.options[x.selectedIndex].label;
        var MoDule = 'workflow/';
        var Functional = 'AdminBypassSaveLog';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        var oRequestParameter = { InputParameter: { "Username": inputAdminUsername, "EmployeeCode": inputAccount, "CompanyCode": inputCompanyCode, "Description": description, "IPAddress": $scope.data_ip, "ClientName": client_name } };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.status = response.data;
        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });

    }

    $scope.companySelectedList = [];

    $scope.GetBypassCompanyList = function GetBypassCompanyList() {
        var MoDule = 'workflow/';
        var Functional = 'GetBypassCompanyList';
        var URL = SERVER + MoDule + Functional;

        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {
            $scope.BypassCompanyList = response.data;

            for (i = 0; i < $scope.BypassCompanyList.length; i++) {
                var temp = {
                    CompanyName: '',
                    CompanyCode: '',
                    Selected: false
                }

                temp.CompanyName = $scope.BypassCompanyList[i].CompanyName;
                temp.CompanyCode = $scope.BypassCompanyList[i].CompanyCode;
                $scope.companySelectedList.push(temp);
            }
            $scope.companySelectedList[0].Selected = true;
        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });

    }

    getLockInBlockSetting();
    $scope.GetBypassCompanyList();

});
