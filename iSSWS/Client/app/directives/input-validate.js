﻿(function () {
    "use strict";
    angular.module('DESS')
        .directive('numbersOnly', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attr, ngModelCtrl) {
                    function fromUser(text) {
                        if (text) {
                            var transformedInput = text.replace(/[^0-9-]/g, '');
                            if (transformedInput !== text) {
                                ngModelCtrl.$setViewValue(transformedInput);
                                ngModelCtrl.$render();
                            }
                            return transformedInput;
                        }
                        return undefined;
                    }
                    ngModelCtrl.$parsers.push(fromUser);
                }
            };
        })
        .directive('decimalOnly', ['CONFIG', function (CONFIG) {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attr, ngModelCtrl) {
                    function fromUser(text) {
                        var text = text + '';
                        if (text) {
                            var str = text.replace(/[^0-9-.]/g, '');
                            var regexp = /^\d{1,30}(\.\d{1,2})?$/;

                            if (!regexp.test(str)) {
                                if (str.split('.').length > 2 || str.substring(str.length - 1, str.length) !== '.')
                                    str = str.substring(0, str.length - 1);
                            }
                            if (parseFloat(str) > CONFIG.MAX_MONEY_VALUE) {
                                str = str.substring(0, str.length - 1);
                            }

                            if (str !== text) {
                                ngModelCtrl.$setViewValue(str);
                                ngModelCtrl.$render();
                            }
                            return str;
                        }
                        return undefined;
                    }
                    ngModelCtrl.$parsers.push(fromUser);
                }
            };
        }]);
})();