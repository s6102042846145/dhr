﻿
/* Creator: Jirawat Jannet
 * Usage  :
 * <plant-selector 
 *      ng-model=" your model (array obj) " 
 *      text=" text variable " 
 *      items=" all plant data items " 
 *      disabled=" disable control (boolean) "
 *      ng-change=" on select item change ( event(selec item datas) ) "
 *      ng-delete=" on delte item ( event(item) ) "></plant-selector>
 * */

(function () {
    "use strict";
    angular.module('DESS')
        .directive('dAutoComplete', function () {
            return {
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    data: '=ngModel',
                    title: '=',
                    placeHolder: '=',

                    valueName: '=',
                    showName: '=',

                    text: '=',
                    items: '=',
                    disabled: '=',
                    ngChange: '&'
                },
                templateUrl: 'views/directives/commons/d-auto-complete.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$mdDialog',
                    function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $mdDialog) {

                        $scope.showDialog = false;
                        $scope.searchTxt = '';
                        $scope.showText = '';
                        $scope.tmpData = '';
                        $scope.tmpDataItem = null;
                        $scope.guid = '';

                        $scope.$watch('data', function (newvalue, oldvalue, scope) {
                            resetShowText();
                        });

                        var init = function () {
                            $scope.tmpData = angular.copy($scope.data);
                            $scope.guid = uuidv4();
                            resetShowText();
                        };

                        var resetShowText = function () {
                            if ($scope.items && $scope.items.length > 0) {
                                var sd = $scope.items.filter(function (i) { return i[$scope.valueName] === $scope.data; });
                                if (sd && sd.length > 0) {
                                    $scope.showText = sd[0][$scope.showName];
                                    $scope.tmpDataItem = sd[0];
                                } else {
                                    $scope.showText = '';
                                }
                            } else {
                                $scope.showText = '';
                            }
                        };

                        // #region Event button

                        $scope.showModal = function () {
                            if ($scope.disabled !== true) {
                                if ($('.d-dialog-full-screen')) {
                                    $('.d-dialog-full-screen').addClass('d-auto-complete-dialog');
                                }
                                $scope.tmpData = angular.copy($scope.data);
                                $scope.showDialog = true;
                                scrollToSelect();
                            }
                        };


                        $scope.hideModal = function () {
                            $scope.searchTxt = '';
                            $scope.tmpData = '';
                            $scope.tmpDataItem = null;
                            resetShowText();
                            if ($('.d-dialog-full-screen')) {
                                $('.d-dialog-full-screen').removeClass('d-auto-complete-dialog');
                            }
                            $scope.showDialog = false;
                        };

                        $scope.submit = function () {
                            $scope.data = angular.copy($scope.tmpData);
                            resetShowText();
                            $scope.showDialog = false;

                            if (typeof $scope.ngChange === 'function') {
                                $scope.ngChange({ newvalue: $scope.tmpData, newitem: $scope.tmpDataItem });
                            }
                        };

                        // #endregion

                        // #region  List funcs

                        $scope.selectItem = function (item) {
                            $scope.tmpData = angular.copy(item[$scope.valueName]);
                            $scope.tmpDataItem = angular.copy(item);
                        };

                        $scope.getShowTextHTML = function (item) {
                            var txt = angular.copy(item[$scope.showName]);
                            var ltxt = item[$scope.showName].toLowerCase();
                            var ind = ltxt.indexOf($scope.searchTxt.toLowerCase());

                            var html = '';
                            if (ind > 0) {
                                html = txt.substring(0, ind) + '<i>' + txt.substring(ind, ind + $scope.searchTxt.length) + '</i>' + txt.substring(ind + $scope.searchTxt.length, txt.length);
                            } else {
                                html = '<i>' + txt.substring(ind, ind + $scope.searchTxt.length) + '</i>' + txt.substring(ind + $scope.searchTxt.length, txt.length);
                            }

                            return html;
                        };

                        // #endregion

                        var scrollToSelect = function () {
                            if ($('#' + $scope.guid)) {
                                var e = $('#' + $scope.guid).find("[data-code='" + $scope.tmpData + "']");
                                if (e) {
                                    $($('#' + $scope.guid)).scrollTo(e);
                                }
                            }
                        };
                        

                        var uuidv4 = function () {
                            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                                return v.toString(16);
                            });
                        };

                        init();
                    }],
                link: function (scope, element, attrs, controller) {

                }
            };
        });
})();