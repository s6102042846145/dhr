﻿(function () {
    "use strict";
    angular.module('DESS')
        .directive('treeView', function ($compile) {
            return {
                //restrict: 'E',
                //require: '?ngModel',
                scope: {
                    localNodes: '=model',
                    localClick: '&click',
                    "myDirectiveFn": "="
                },
                link: function ($scope, tElement, tAttrs, transclude) {
                    var maxLevels = (angular.isUndefined(tAttrs.maxlevels)) ? 10 : tAttrs.maxlevels; 
                    var hasCheckBox = (angular.isUndefined(tAttrs.checkbox)) ? false : true;
                    $scope.showItems = [];

                    $scope.showHide = function (ulId) {
                        var hideThis = document.getElementById(ulId);
                        var showHide = angular.element(hideThis).attr('class');
                        angular.element(hideThis).attr('class', (showHide === 'show' ? 'hide' : 'show'));
                    }
                    $scope.changeIcon = function (ulId) {
                        var hideThis = document.getElementById(ulId);
                        var showHide = angular.element(hideThis).attr('class');
                        return showHide === 'show' ? true : false;
                    }

                    $scope.showIcon = function (node) {
                        if (!angular.isUndefined(node.children)) return true;
                    }

                    $scope.checkIfChildren = function (node) {
                        if (!angular.isUndefined(node.children)) return true;
                    }

                    /////////////////////////////////////////////////
                    /// SELECT ALL CHILDRENS
                    function parentCheckChange(item) {
                        for (var i in item.children) {
                            item.children[i].checked = item.checked;
                            //console.log('child = ', item.children[i].id);
                            if (item.checked == false) {
                                if (!angular.isUndefined(item.children[i].id)) {
                                    var index = $scope.selected.indexOf(item.children[i].id);
                                    $scope.selected.splice(index, 1);
                                    $scope.level.splice(index, 1);
                                    //console.log('splice by parentCheckChange() = ', item.children[i].id);
                                }
                            } else {
                                if (!angular.isUndefined(item.children[i].id)) {
                                    var dup = 0;
                                    //console.log('$scope.selected parentCheck = ', $scope.selected);
                                    //console.log('item.children[i].id will push = ', item.children[i].id.toString());
                                    var itemSelected = item.children[i].id.toString();
                                    for (var j = 0; j < $scope.selected.length; j++) {
                                        if (itemSelected == $scope.selected[j]) {
                                            dup = 1;
                                        }
                                    }
                                    if (dup == 0) {
                                        $scope.selected.push(item.children[i].id);
                                        //console.log('push by parentCheckChange() = ', item.children[i].id);
                                    }
                                }
                            }
                            //$scope.selected.push(item.children[i].id);
                            //$scope.level.push(item.children[i].level);

                            if (item.children[i].children) {
                                parentCheckChange(item.children[i]);
                            }
                        }
                    }

                    $scope.selected = [];
                    $scope.level = [];

                    $scope.checkChange = function (node) {
                        if (node.children) {
                            parentCheckChange(node);
                        }
                        //alert(node.id + ' | ' + node.name + ' | ' + node.checked);
                        if (node.checked) {
                            //console.log(node.checked);
                            var dup = 0;
                            //console.log('$scope.selected checkChange = ', $scope.selected);
                            //console.log('node.id = ', node.id.toString());
                            var itemSelected = node.id.toString();
                            for (var j = 0; j < $scope.selected.length; j++) {
                                if (itemSelected == $scope.selected[j]) {
                                    dup = 1;
                                }
                            }
                            if (dup == 0) {
                                $scope.selected.push(node.id);
                                $scope.level.push(node.level);
                                //console.log('push by checkChange() = ', node.id);
                            }

                            //console.log('selected checked = ', $scope.selected);
                            //console.log('level checked = ', $scope.level);
                            //console.log('level check = ', node.level);
                        } else {
                            //splice from arr
                            //console.log('selected uncheck before delete = ', $scope.selected);
                            var index = $scope.selected.indexOf(node.id);
                            $scope.selected.splice(index, 1);
                            $scope.level.splice(index, 1);
                            //console.log('splice by checkChange() = ', node.id);

                            for (var i = 0; i < $scope.level.length; i++) {
                                if (node.level < $scope.level[i]) {
                                    var index = $scope.level.indexOf(node.level);
                                    $scope.selected.splice(index, 1);
                                    $scope.level.splice(index, 1);
                                }
                            }

                            //$scope.selected;
                            //console.log('selected uncheck = ', $scope.selected);
                            //console.log('level uncheck = ', $scope.level);
                            //console.log('level uncheck = ', node.level);
                        }
                    }

                    //parse selected id to Controller
                    $scope.somethingHappend = function () {
                        $scope.myDirectiveFn($scope.selected);

                    }
                    /////////////////////////////////////////////////

                    function renderTreeView(collection, level, max) {
                        var text = '';
                        text += '<li ng-repeat="n in ' + collection + '" >';
                        text += '<span ng-show=showIcon(n) class="show-hide" ng-click=showHide(n.id)><i class="fa fa-minus-square" ng-show=changeIcon(n.id)></i><i class="fa fa-plus-square" ng-hide=changeIcon(n.id)></i></span>';
                        text += '<span ng-show=!showIcon(n) style="padding-right: 6px"></span>';

                        if (hasCheckBox) {
                            text += '<input name="checkNode[]" class="tree-checkbox" type="checkbox" ng-model="n.checked" ng-click="somethingHappend()" ng-change="checkChange(n)">';
                        }

                        //text += '<span class="edit" ng-click=localClick({node:n})><i class="fa fa-pencil"></i></span>'

                        text += '<label>{{n.name}}</label>';

                        if (level < max) {
                            text += '<ul id="{{n.id}}" class="hide" ng-if=checkIfChildren(n)>' + renderTreeView('n.children', level + 1, max) + '</ul></li>';
                        } else {
                            text += '</li>';
                        }

                        return text;
                    }// end renderTreeView();

                    try {
                        var text = '<ul class="tree-view-wrapper">';
                        text += renderTreeView('localNodes', 1, maxLevels);
                        text += '</ul>';
                        tElement.html(text);
                        $compile(tElement.contents())($scope);
                    }
                    catch (err) {
                        tElement.html('<b>ERROR!!!</b> - ' + err);
                        $compile(tElement.contents())($scope);
                    }

                }//end link
            }; //end return
        })

})();

//$$scope.nodes = [
//    {
//        id: 2, name: '1ª Habilitação', checked: true,
//        entity: [
//            {
//                entidade: {
//                    img: 'http://placehold.it/500',
//                    obrigatorio: true
//                }
//            }
//        ],

//        children: [
//            { id: 3, name: 'Level2 - A', checked: true },
//            { id: 4, name: 'Level2 - B', checked: true }
//        ]
//    },

//    {
//        id: 3, name: 'Veículo', checked: true,
//        entity: [
//            {
//                entidade: {
//                    img: 'http://placehold.it/500',
//                    obrigatorio: true
//                }
//            }
//        ],

//        children: [
//            { id: 3, name: 'Level2 - A', checked: true },
//            { id: 4, name: 'Level2 - B', checked: true },
//            {
//                id: 5, name: 'Level2 - C', checked: true, children: [
//                    { id: 6, name: 'Level3 - A', checked: false },
//                    {
//                        id: 7, name: 'Level3 - B', checked: false, children: [
//                            { id: 8, name: 'Level4 - A', checked: false }
//                        ]
//                    }
//                ]
//            }
//        ]
//    },
//    {
//        id: 4, name: 'Habilitação', checked: true,
//        entity: [
//            {
//                entidade: {
//                    img: 'http://placehold.it/500',
//                    obrigatorio: true
//                }
//            }
//        ],

//        children: [
//            { id: 3, name: 'Level2 - A', checked: true },
//            { id: 4, name: 'Level2 - B', checked: true },
//            {
//                id: 5, name: 'Level2 - C', checked: true, children: [
//                    { id: 6, name: 'Level3 - A', checked: false },
//                    {
//                        id: 7, name: 'Level3 - B', checked: false, children: [
//                            { id: 8, name: 'Level4 - A', checked: false }
//                        ]
//                    }
//                ]
//            }
//        ]
//    },
//];