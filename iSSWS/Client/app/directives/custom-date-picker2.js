﻿(function () {
    "use strict";
    angular.module('DESS')
        .directive('myDatePickerEnd', function () {
            return {
                restrict: 'A',
                require: '?ngModel',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {
                    $scope.employee = getToken(CONFIG.USER);
                    $scope.ln = "th";
                    $scope.YearCurrent;
                    $scope.holidayList = [];
                    $scope.holidayListMonth = [];
                    $scope.holidayListYear = [];
                    $scope.holidayData = {
                        day: '',
                        month: '',
                        year: '',
                        name: ''
                    };
                    //flag call to function getholiday
                    $scope.isCallMonth = false,
                        $scope.isCallYear = false;

                    if ($scope.employee.Language !== "TH") {
                        $scope.ln = "en";
                    }
                    $scope.isCallMonthFunction = false;
                    $scope.isCallYearFunction = false;

                    $scope.GetHolidayByYearMonth = function (Month, Year) {

                        //check duplicate month and year in holidayList
                        if ($scope.holidayListMonth.indexOf(Month) == -1) {
                            $scope.isCallMonthFunction = true;
                            $scope.holidayListMonth.push(Month);
                        } else {
                            $scope.isCallMonthFunction = false;
                        }
                        if ($scope.holidayListYear.indexOf(Year) == -1) {
                            $scope.isCallYearFunction = true;
                            $scope.holidayListYear.push(Year);
                        } else {
                            $scope.isCallYearFunction = false;
                        }
                        if ($scope.isCallMonthFunction == true) {
                            if (Month !== "" && Month !== null && Month !== undefined && Month !== 0) {
                                var URL = CONFIG.SERVER + 'HRTM/GetHolidayByYearMonth';
                                $scope.employeeData = getToken(CONFIG.USER);
                                var oRequestParameter = {
                                    InputParameter: { 'Month': Month, 'Year': Year }
                                    , CurrentEmployee: $scope.employeeData
                                    , Requestor: $scope.requesterData
                                    , Creator: getToken(CONFIG.USER)
                                };
                                $scope.isCallMonth = false;
                                $http({
                                    method: 'POST',
                                    url: URL,
                                    data: oRequestParameter
                                }).then(function successCallback(response) {
                                    $scope.MasterDataHolidayList = response.data.HolidayList;
                                    //console.log('HolidayList ' + Month + ' ', response.data.HolidayList);
                                    //if ($scope.MasterDataHolidayList.length > 0) {
                                    var j = 0;
                                    var i = 0;
                                    var checkDuplicate = true;
                                    for (i = 0; i < $scope.MasterDataHolidayList.length; i++) {
                                        $scope.holidayData = {
                                            day: $scope.MasterDataHolidayList[i].Date,
                                            month: Month,
                                            year: $scope.MasterDataHolidayList[i].Year,
                                            name: $scope.MasterDataHolidayList[i].HolidayName
                                        };
                                        if ($scope.holidayList.length == 0) {
                                            checkDuplicate = true;
                                        }
                                        //check duplicate holiday name in lists
                                        for (j = 0; j < $scope.holidayList.length; j++) {
                                            if ($scope.MasterDataHolidayList[i].HolidayName === $scope.holidayList[j].name && Year === Number($scope.holidayList[j].year)) {
                                                checkDuplicate = false;
                                            }
                                        }
                                        if (checkDuplicate === true) {
                                            $scope.holidayList.push($scope.holidayData);
                                            checkDuplicate = true;
                                        } else {
                                            checkDuplicate = true;
                                        }
                                    }
                                    //console.log('holidayList.push = ', $scope.holidayList);
                                })
                            }
                        }
                    }

                }],
                link: function ($scope, element, attrs, ngModelController) {
                    // Private variables
                    $scope.holidayList = [];
                    var datepickerFormat = 'dd/mm/yyyy',
                        momentFormat = 'DD/MM/YYYY',
                        datepicker,
                        tempMonth = "",
                        tempYear = "";

                    // Init date picker and get objects http://bootstrap-datepicker.readthedocs.org/en/release/index.html
                    datepicker = element.datepicker({
                        autoclose: true,
                        keyboardNavigation: false,
                        todayHighlight: true,
                        //daysOfWeekDisabled: "0,6",
                        format: datepickerFormat,
                        language: $scope.ln,
                        beforeShowDay: function (date) {
                            //set month, year
                            if (tempMonth == "") {
                                tempMonth = date.getMonth() + 1;
                                tempYear = date.getFullYear();
                                $scope.isCallMonth = true;
                                $scope.isCallYear = true;
                            }
                            if (tempMonth !== date.getMonth() + 1) {
                                tempMonth = date.getMonth() + 1;
                                $scope.isCallMonth = true;
                            }
                            if (tempYear !== date.getFullYear()) {
                                tempYear = date.getFullYear();
                                $scope.isCallYear = true;
                            }

                            if ($scope.isCallMonth === true && $scope.isCallYear === true) {
                                $scope.GetHolidayByYearMonth((tempMonth), (tempYear));
                            }

                            if (date.getMonth() + 1 == (new Date()).getMonth() + 1) {
                                //get holiday by current month
                                //console.log('nonthNow ', date.getMonth()+1);
                                if ($scope.holidayList.length > 0) {
                                    //console.log('checkholiday');
                                    var i = 0;
                                    for (i = 0; i < $scope.holidayList.length; i++) {
                                        if ($scope.holidayList[i].day == date.getDate() && $scope.holidayList[i].month == date.getMonth() + 1) {
                                            return {
                                                tooltip: $scope.holidayList[i].name,
                                                classes: 'bg-holiday disabled-date'
                                            };
                                        }
                                    }
                                }
                            } else {
                                //console.log('nonthNext ', date.getMonth()+1);
                                //get holiday by month
                                if ($scope.holidayList.length > 0) {
                                    //console.log('checkholiday');
                                    var i = 0;
                                    for (i = 0; i < $scope.holidayList.length; i++) {
                                        if ($scope.holidayList[i].day == date.getDate() && $scope.holidayList[i].month == date.getMonth() + 1) {
                                            return {
                                                tooltip: $scope.holidayList[i].name,
                                                classes: 'bg-holiday disabled-date'
                                            };
                                        }
                                    }
                                }
                            }

                        },
                        //datesDisabled: ['15/06/2020']
                    });
                    //elPicker = datepicker.data('datepicker').picker;

                    // Adjust offset on show
                    //datepicker.on('show', function (evt) {
                    //elPicker.css('left', parseInt(elPicker.css('left')) + +attrs.offsetX);
                    //elPicker.css('top', parseInt(elPicker.css('top')) + +attrs.offsetY);
                    //});

                    // Only watch and format if ng-model is present https://docs.angularjs.org/api/ng/type/ngModel.NgModelController
                    if (ngModelController) {
                        // So we can maintain time
                        var lastModelValueMoment;

                        ngModelController.$formatters.push(function (modelValue) {
                            //
                            // Date -> String
                            //

                            // Get view value (String) from model value (Date)
                            var viewValue,
                                m = moment(modelValue);
                            if (modelValue && m.isValid()) {
                                // Valid date obj in model
                                lastModelValueMoment = m.clone(); // Save date (so we can restore time later)
                                viewValue = m.format(momentFormat);
                            } else {
                                // Invalid date obj in model
                                lastModelValueMoment = undefined;
                                viewValue = undefined;
                            }

                            // Update picker
                            element.datepicker('update', viewValue);

                            // Update view
                            return viewValue;
                        });

                        ngModelController.$parsers.push(function (viewValue) {
                            //
                            // String -> Date
                            //

                            // Get model value (Date) from view value (String)
                            var modelValue,
                                m = moment(viewValue, momentFormat, true);
                            if (viewValue && m.isValid()) {
                                // Valid date string in view
                                if (lastModelValueMoment) { // Restore time
                                    m.hour(lastModelValueMoment.hour());
                                    m.minute(lastModelValueMoment.minute());
                                    m.second(lastModelValueMoment.second());
                                    m.millisecond(lastModelValueMoment.millisecond());
                                }
                                modelValue = m.toDate();
                            } else {
                                // Invalid date string in view
                                modelValue = undefined;
                            }

                            // Update model
                            return modelValue;
                        });

                        /*datepicker.on('changeDate', function (evt) {
                            // Only update if it's NOT an <input> (if it's an <input> the datepicker plugin trys to cast the val to a Date)
                            if (evt.target.tagName !== 'INPUT') {
                                ngModelController.$setViewValue(moment(evt.date).format(momentFormat)); // $seViewValue basically calls the $parser above so we need to pass a string date value in
                                ngModelController.$render();
                            }
                        });*/
                    }
                }//end link
            }; //end return
        })

})();