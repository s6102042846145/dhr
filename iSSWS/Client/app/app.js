﻿(function () {
    angular.module('DESS'
        , ['ngRoute', 'ngSanitize', 'datatables', 'ui.select', 'ngMaterial', 'selectize', 'ngMessages', 'daterangepicker', 'naif.base64', 'ui.bootstrap', 'ckeditor', 'multipleSelect'])
        .constant('CONFIG', {
            //SERVER: 'https://dessweb-test.pttdigital.com/DHR_UI_DEV/',
            SERVER: 'http://localhost:5555/',
            USER: 'com.pttdigital.dess.authorizeUser',
            ANNOUNCEMENT: 'com.pttdigital.dess.announcement',
            MENU: 'com.pttdigital.dess.menu',
            PAGE_HOME: '/frmViewContent/101',
            //Nattawat S. Add new 
            PATHCOOKIES: 'com.pttdigital.dess.pathcookie', 
            POSITION_SELECTED: 'com.pttdigital.dess.selectedPosition', 
            FILE_SETTING: {
                //ALLOW_FILETYPE: new RegExp(/^([a-zA-Z0-9._+-])+.(([gif png jpg jpeg bmp png pdf doc xls ppt zip rar docx xlsx txt pptx]{3}))/i),
                ALLOW_FILETYPE: 'gif png jpg jpeg bmp png pdf doc xls ppt docx xlsx txt pptx',
                ALLOW_FILE: new RegExp(/[!@#$%^&*<>\/:"|?+]/),
                ALLOW_FILENAME_LENGTH: 50,
                ALLOW_FILESIZE: 10485760,
                DIVIDE_FILESIZE: 1048576
                // 1GB = 1073741824
            },
            MAX_MONEY_VALUE: 10000000,
            MAX_DECIMAL_LENGTH: 2
            //PROFILE_SETTING:{
            //    POSITION: '',
            //}
        }).filter("nl2br", function ($filter) {
            return function (data) {
                if (!data) return data;
                return data.replace(/\n\r?/g, '<br />');
            };
        }).filter('unique', function () {

            return function (arr, field) {
                var o = {}, i, l = arr.length, r = [];
                for (i = 0; i < l; i += 1) {
                    o[arr[i][field]] = arr[i];
                }
                for (i in o) {
                    r.push(o[i]);
                }
                return r;
            };
        })
        .factory('iosService', function () {
            return {
                do: function (openDialog) {

                    if (this.isDo()) {
                        if (openDialog) {
                            console.log('hide top button')
                            $("nav").css('visibility', 'hidden');
                        } else {
                            $("nav").css('visibility', 'visible');
                        }
                    }
                },
                isDo: function () {
                    var width = window.innerWidth > 0 ? window.innerWidth : screen.width;
                    return width <= 764 && this.isIOS();
                },
                isIOS: function () {
                    var iDevices = [
                        'iPad Simulator',
                        'iPhone Simulator',
                        'iPod Simulator',
                        'iPad',
                        'iPhone',
                        'iPod'
                    ];

                    if (navigator.platform) {
                        while (iDevices.length > 0) {
                            if (navigator.platform === iDevices.pop()) { return true; }
                        }
                    }

                    return false;

                }
            };
        })
        .directive('selectOnClick', ['$window', function ($window) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.on('click', function () {
                        if (!$window.getSelection().toString()) {
                            // Required for mobile Safari
                            $(this).select();
                        }
                    });
                }
            };
        }]).directive('numberInput', function ($filter) {
            return {
                require: 'ngModel',
                link: function (scope, elem, attrs, ngModelCtrl) {

                    ngModelCtrl.$formatters.push(function (modelValue) {
                        return setDisplayNumber(modelValue, true);
                    });

                    // it's best to change the displayed text using elem.val() rather than
                    // ngModelCtrl.$setViewValue because the latter will re-trigger the parser
                    // and not necessarily in the correct order with the changed value last.
                    // see http://radify.io/blog/understanding-ngmodelcontroller-by-example-part-1/
                    // for an explanation of how ngModelCtrl works.
                    ngModelCtrl.$parsers.push(function (viewValue) {
                        setDisplayNumber(viewValue);
                        return setModelNumber(viewValue);
                    });

                    // occasionally the parser chain doesn't run (when the user repeatedly 
                    // types the same non-numeric character)
                    // for these cases, clean up again half a second later using "keyup"
                    // (the parser runs much sooner than keyup, so it's better UX to also do it within parser
                    // to give the feeling that the comma is added as they type)
                    elem.bind('keyup focus', function () {
                        setDisplayNumber(elem.val());
                    });

                    function setDisplayNumber(val, formatter) {
                        var valStr, displayValue;

                        if (typeof val === 'undefined') {
                            return 0;
                        }

                        valStr = val.toString();
                        displayValue = valStr.replace(/,/g, '').replace(/[A-Za-z]/g, '');
                        displayValue = parseFloat(displayValue);
                        displayValue = (!isNaN(displayValue)) ? displayValue.toString() : '';

                        // handle leading character -/0
                        if (valStr.length === 1 && valStr[0] === '-') {
                            displayValue = valStr[0];
                        } else if (valStr.length === 1 && valStr[0] === '0') {
                            displayValue = '';
                        } else {
                            displayValue = $filter('number')(displayValue);
                        }

                        // handle decimal
                        if (!attrs.integer) {
                            if (displayValue.indexOf('.') === -1) {
                                if (valStr.slice(-1) === '.') {
                                    displayValue += '.';
                                } else if (valStr.slice(-2) === '.0') {
                                    displayValue += '.0';
                                } else if (valStr.slice(-3) === '.00') {
                                    displayValue += '.00';
                                }
                            } // handle last character 0 after decimal and another number
                            else {
                                if (valStr.slice(-1) === '0') {
                                    displayValue += '0';
                                }
                            }
                        }

                        if (attrs.positive && displayValue[0] === '-') {
                            displayValue = displayValue.substring(1);
                        }

                        if (typeof formatter !== 'undefined') {
                            return (displayValue === '') ? 0 : displayValue;
                        } else {
                            elem.val((displayValue === '0') ? '' : displayValue);
                        }
                    }

                    function setModelNumber(val) {
                        var modelNum = val.toString().replace(/,/g, '').replace(/[A-Za-z]/g, '');
                        modelNum = parseFloat(modelNum);
                        modelNum = (!isNaN(modelNum)) ? modelNum : 0;
                        if (modelNum.toString().indexOf('.') !== -1) {
                            modelNum = Math.round((modelNum + 0.00001) * 100) / 100;
                        }
                        if (attrs.positive) {
                            modelNum = Math.abs(modelNum);
                        }
                        return modelNum;
                    }
                }
            };
        })
        .config(function ($mdThemingProvider) {
            $mdThemingProvider.theme('default').primaryPalette('blue', {
                'default': '400',
                'hue-2': '500'
            });
        }).directive('phoneInput', ['$filter', '$browser', function ($filter, $browser) {
            return {
                require: 'ngModel',
                link: function ($scope, $element, $attrs, ngModelCtrl) {
                    var listener = function () {
                        var value = $element.val().replace(/[^0-9]/g, '');
                        $element.val($filter('tel')(value, false));
                    };

                    // This runs when we update the text field
                    ngModelCtrl.$parsers.push(function (viewValue) {
                        return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
                    });

                    // This runs when the model gets updated on the scope directly and keeps our view in sync
                    ngModelCtrl.$render = function () {
                        $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
                    };

                    $element.bind('change', listener);
                    $element.bind('keydown', function (event) {
                        var key = event.keyCode;
                        // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                        // This lets us support copy and paste too
                        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
                            return;
                        }
                        $browser.defer(listener); // Have to do this or changes don't get picked up properly
                    });

                    $element.bind('paste cut', function () {
                        $browser.defer(listener);
                    });
                }

            };
        }]).filter('tel', function () {
            return function (tel) {
                if (!tel) { return ''; }

                var value = tel.toString().trim().replace(/^\+/, '');

                if (value.match(/[^0-9]/)) {
                    return tel;
                }

                var country, city, number;

                switch (value.length) {
                    case 1:
                    case 2:
                    case 3:
                        city = value;
                        break;

                    default:
                        city = value.slice(0, 3);
                        number = value.slice(3);
                }

                if (number) {
                    if (number.length > 3) {
                        number = number.slice(0, 3) + '-' + number.slice(3, 7);
                    }
                    else {
                        number = number;
                    }

                    return (city + "-" + number).trim();
                }
                else {
                    return city;
                }

            };
        });
})();


var rx = /INPUT|SELECT|TEXTAREA/i;
$(document).bind("keydown keypress", function (e) {
    if (e.which == 8) { // 8 == backspace
        if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly) {
            e.preventDefault();
        }
    }
});

String.format = function () {
    // The string containing the format items (e.g. "{0}")
    // will and always has to be the first argument.
    var theString = arguments[0];

    // start with the second argument (i = 1)
    for (var i = 1; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}

isArrayNotNull = function (datas) {
    return datas && datas.length > 0;
};

var templateFunction = function () {
    return (
        '<div>' +
        '<input type="text" ng-model="data" class="data-selection" />' +
        '</div>'
    );
};