﻿
        function setUpPushNotification() {
            //alert('calling push init');
            if (typeof(PushNotification) != 'undefined') {
                try {
                    var push = PushNotification.init({
                        "android": {
                            "senderID": "648020613372"
                        },
                        "ios": {
                            "sound": true,
                            "vibration": true,
                            "badge": true
                        },
                        "windows": {}
                    });
                    //alert('after init');

                    push.on('registration', function (data) {
                        //alert('data: ' + data);
                        //alert('registration event: ' + data.registrationId);

                        var oldRegId = localStorage.getItem('registrationId');
                        if (oldRegId !== data.registrationId) {
                            // Save new registration ID
                            localStorage.setItem('registrationId', data.registrationId);
                            //
                            //$("#good").html = data.registrationId;
                            // Post registrationId to your app server as the value has changed
                        }

                        UpdateRegistrationID(data.registrationId);

                        console.log('console:' + data.registrationId);
                        toApplication();
                        //$("#good").html = data.registrationId;
                    });

                    push.on('error', function (e) {
                        alert("push error = " + e.message + ':' + e.data);
                    });

                    push.on('notification', function (data) {
                        //alert('notification event:'+data.message+':'+data.title);
                        try {
                            navigator.notification.alert(
                                data.message,         // message
                                onalert,                 // callback
                                data.title,           // title
                                'Ok'                  // buttonName
                            );
                        }
                        catch (err) {
                            alert(err.message);
                        }
                    });
                } catch (err) {
                    alert(err.message);
                }
            }
        }

        function setUpPushNotification2() {
            //alert('calling push init');
            if (typeof (PushNotification) != 'undefined') {
                try {
                    var push = PushNotification.init({
                        "android": {
                            "senderID": "648020613372"
                        },
                        "ios": {
                            "sound": true,
                            "vibration": true,
                            "badge": true
                        },
                        "windows": {}
                    });
                    //alert('after init');

                    push.on('registration', function (data) {
                        //alert('data: ' + data);
                        //alert('registration event: ' + data.registrationId);

                        var oldRegId = localStorage.getItem('registrationId');
                        if (oldRegId !== data.registrationId) {
                            // Save new registration ID
                            localStorage.setItem('registrationId', data.registrationId);
                            //
                            //$("#good").html = data.registrationId;
                            // Post registrationId to your app server as the value has changed
                        }

                        UpdateRegistrationID(data.registrationId);

                        console.log('console:' + data.registrationId);
                        //toApplication();
                        //$("#good").html = data.registrationId;
                    });

                    push.on('error', function (e) {
                        alert("push error = " + e.message + ':' + e.data);
                    });

                    push.on('notification', function (data) {
                        //alert('notification event:'+data.message+':'+data.title);
                        try {
                            navigator.notification.alert(
                                data.message,         // message
                                onalert,                 // callback
                                data.title,           // title
                                'Ok'                  // buttonName
                            );
                        }
                        catch (err) {
                            alert(err.message);
                        }
                    });
                } catch (err) {
                    alert(err.message);
                }
            }
        }

        function onalert() {
            //alert('eiei');
        }

        function UpdateRegistrationID(_RegistrationID) {
            //// check username, password with webservice
            var URL = 'http://testessmobile.pttict.com/workflow/UpdateRegistrationID/';

            var oRegistration = { RegistrationID: _RegistrationID };
            //window.localStorage.removeItem('com.pttdigital.ess.authorizeUser');
            //$.ajax({
            //    type: 'POST',
            //    url: URL,
            //    data: oRegistration
            //}).then(function successCallback(response) {
            //    // Success
            //    console.log('UpdateRegistrationID : ', _RegistrationID);

            //}, function errorCallback(response) {
            //    // Error
            //    console.log('error UpdateRegistrationID : ', response);
            //});

            $.ajax({
                type: 'POST',
                data: oRegistration,
                url: URL,
                success:
                    function (data, textStatus, XMLHttpRequest) {
                        // Success
                        console.log('UpdateRegistrationID : ', _RegistrationID);
                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        // Error
                        console.log('error UpdateRegistrationID : ', response);
                    }
            });

        }
