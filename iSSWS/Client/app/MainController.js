﻿(function () {
    angular.module('DESS')
        .controller('MainController', ['$rootScope', '$scope', '$http', '$document', '$window', '$route', '$location', 'CONFIG', '$timeout', '$mdDialog', '$routeParams', function ($rootScope, $scope, $http, $document, $window, $route, $location, CONFIG, $timeout, $mdDialog, $routeParams) {
            //Nattawat S. 2021-01-25
            $scope.PositionSelected = getToken(CONFIG.POSITION_SELECTED);
            $scope.CurrentEmployeeAllPosition = getToken(CONFIG.USER).AllPosition;
            $scope.CurrentEmployeeLanguage = getToken(CONFIG.USER).Language;
            $scope.SelectedPosition = angular.copy($scope.CurrentEmployeeAllPosition[0]);
            //End add
            $scope.isMove = false;
            $window.onscroll = function () {
                if ($window.scrollY === 0) {
                    if (document.getElementsByClassName("md-virtual-repeat-container").length > 0) {
                        var elementExists = document.getElementsByClassName("md-virtual-repeat-container")[0].classList.contains("ng-hide");
                        if (!elementExists) {
                            $scope.isMove = true;
                        } else {
                            $scope.isMove = false;
                        }
                    }
                    else {
                        $scope.isMove = false;
                    }
                } else {
                    $scope.isMove = true;
                }
                $scope.$apply();

                // add for go to top button
                scrollFunction();
            };

            $scope.gototopButton = document.getElementById("dess_ontopButton");

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    $scope.gototopButton.style.display = "block";
                } else {
                    $scope.gototopButton.style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            $scope.topFunction = function () {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }

            $scope.key_lightbox = false;

            var qa_type = $location.search().type;

            if (qa_type == 1) {
                $scope.key_lightbox = true;
            } else {
                $scope.key_lightbox = false;
            }
            $scope.isExpendMainMenu = false;

            $scope.toggleExpendMainMenu = function () {
                $scope.isExpendMainMenu = !$scope.isExpendMainMenu;
                if ($scope.isExpendMainMenu) {
                    $scope.iconIsExpendMainMenu = '_x';
                } else {
                    $scope.iconIsExpendMainMenu = '';
                }
                return;
            }

            $scope.toggleExpendMainMenuClose = function () {
                if ($scope.isExpendMainMenu) {
                    $scope.isExpendMainMenu = false;
                    $scope.iconIsExpendMainMenu = '';
                }
                return;
            }

            $scope.isExpendMainMenuLogout = false;
            $scope.toggleExpendMainMenuLogout = function () {
                $scope.isExpendMainMenuLogout = !$scope.isExpendMainMenuLogout;
            }

            //var golf2min = 'real';
            //golf2min();

            $scope.runtime = Date.now();
            $scope.loader = {
                enable: false,
                childRequestEnable: false
            };
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.PassingDocument = { document: null };
            $scope.MODULE_SETTING = CONFIG.MODULE_SETTING;
            $scope.PAGE_SETTING = CONFIG.PAGE_SETTING;
            $scope.PROFILE_SETTING = { POSITIONID: '' };
            $scope.CONFIG_SERVER = CONFIG.SERVER;

            // Please search all text int project 'RECEIPT_LIMIT_DATE_09182017' to find the assignment line
            $scope.RECEIPT_LIMIT_DATE = {
                RP_LIMIT_MINDATE: 0,
                RP_LIMIT_MAXDATE: 0,
                GE_LIMIT_MINDATE: 0,
                GE_LIMIT_MAXDATE: 0,
                GE_ALLOW_SELECTDATE_OUTOFPERIOD: false,
                RP_ALLOW_SELECTDATE_OUTOFPERIOD: false,
                GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: false,
                RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: false,
            }
            $scope.documentState = {
                isDirty: false
            };
            $scope.start_date_for_apply_perdiem_distribution = '2016-12-01T00:00:00.000';
            $scope.blockAction = {
                isBLock: false,
                blockMessage: ''
            };

            $scope.class = "red";
            $scope.changeClass = function () {
                if ($scope.class === "red")
                    $scope.class = "blue";
                else
                    $scope.class = "red";
            };
            $scope.menuCtrl = {
                isHideMenu: true
            }
            $scope.toggleMenu = function () {
                $scope.menuCtrl.isHideMenu = !$scope.menuCtrl.isHideMenu

            }
            var focus_guide = $("body").guide();
            $scope.foucus_element = function (tagId, text) {
                focus_guide = $("body").guide();
                focus_guide.addStep(tagId, text);
                focus_guide.start();
                $(tagId).click(function () {
                    focus_guide.clear();
                });
            }

            var CurrentCompanyTheme = localStorage.getItem('CurrentCompanyTheme');
            if (CurrentCompanyTheme != null) {
                var fileref = document.createElement("link")
                fileref.setAttribute("rel", "stylesheet")
                fileref.setAttribute("type", "text/css")
                fileref.setAttribute("href", CurrentCompanyTheme)
                if (typeof fileref != "undefined")
                    document.getElementsByTagName("head")[0].appendChild(fileref)
            }



            $scope.flowCurrent = function (flowItems) {
                if (flowItems != null) {
                    var status = 0;
                    for (i = 0; i < flowItems.length; i++) {
                        if (flowItems[i].IsCurrentItem) {
                            flowItems[i].STATUS_FLOW_ITEM = 1;
                            status = 2;
                            continue;
                        }
                        else if (i == flowItems.length - 1) {
                            status = 3;
                        }
                        flowItems[i].STATUS_FLOW_ITEM = angular.copy(status);

                    }
                }
                return flowItems;
            }
            /* Text Description */

            $scope.Text = null;
            $scope.RequestorText = null;
            $scope.getAllTextDescription = function () {
                $scope.Text = null;
                var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.Text = response.data;
                        $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                    }
                }, function errorCallback(response) {
                    // Error
                    $scope.Text = null;
                    console.log('error MainController TextDescription.', response);
                });
            };
            $scope.getAllTextDescription();

            $scope.getTextByCompany = function (oRequestor) {
                var oRequestParameter = { InputParameter: {}, Requestor: oRequestor }
                var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.RequestorText = response.data;
                    }
                }, function errorCallback(response) {
                    // Error
                    $scope.RequestorText = angular.copy($scope.Text);
                });
            };

            /* !Text Description */

            /* Expense Receipt */

            $scope.SystemInitialReady = false;
            $scope.systemMasterData = {};
            $scope.initialSystem = function () {
                $scope.systemMasterData = {};
                $scope.SystemInitialReady = true;
                var oRequestParameter = { InputParameter: { Module: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'workflow/InitialSystemByModule/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.systemMasterData.IsAccountUser = response.data.IsAccountUser;
                        $scope.systemMasterData.ExpenseModel = response.data.ExpenseModel;
                    }
                    $scope.SystemInitialReady = true;
                }, function errorCallback(response) {
                    // Error
                    $scope.SystemInitialReady = false;
                    alert('Please refresh page again.');
                });
            };
            $scope.initialSystem();

            /* Expense Receipt */


            $scope.changeReceiveMail = function (language) {

                var URL = CONFIG.SERVER + 'workflow/GetEmployeeData';
                var oRequestParameter = { InputParameter: { "Language": language }, CurrentEmployee: getToken(CONFIG.USER) };

                // change EmployeeData
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    window.localStorage.removeItem(CONFIG.USER);
                    window.localStorage.setItem(CONFIG.USER, JSON.stringify(response.data.EmployeeData));
                    console.log('success changeReceiveMail.', getToken(CONFIG.USER));

                }, function errorCallback(response) {
                    // Error
                    console.log('error changeLanguage.', response);
                });
            };

            $scope.changeLanguage = function (language) {
                //var employeeData = getToken(CONFIG.USER);
                //var URL = CONFIG.SERVER + 'workflow/GetEmployeeData/' + language;

                var URL = CONFIG.SERVER + 'workflow/GetEmployeeData';
                var oRequestParameter = { InputParameter: { "Language": language }, CurrentEmployee: getToken(CONFIG.USER) };

                // change EmployeeData
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    window.localStorage.removeItem(CONFIG.USER);
                    window.localStorage.setItem(CONFIG.USER, JSON.stringify(response.data.EmployeeData));
                    console.log('success changeLanguage.', getToken(CONFIG.USER));

                    // Reload Menu
                    var args = {};

                    // $timeout
                    $scope.getAllTextDescription();
                    $scope.initialSystem();

                    $rootScope.$broadcast('onReloadMenu', args);


                }, function errorCallback(response) {
                    // Error
                    console.log('error changeLanguage.', response);
                });
            };

            $scope.goBack = function () {
                $window.history.back();
            };


            $scope.Orgsettings = {
                OrgUnitShortText: ''
            };


            $scope.getOrgUnitShortText = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetOrgUnit/';
                var oRequestParameter = {
                    InputParameter: { "ObjectID": $scope.employeeData.OrgUnit }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success

                    $scope.objOrgUnit = response.data;
                    if (angular.isDefined($scope.objOrgUnit) && $scope.objOrgUnit != null) {
                        if ($scope.employeeData.Language == 'EN') {
                            if ($scope.objOrgUnit.ShortTextEn !== null && $scope.objOrgUnit.ShortTextEn !== '') {
                                $scope.Orgsettings.OrgUnitShortText = '( ' + $scope.objOrgUnit.ShortTextEn + ' )';
                            }
                        }
                        else {
                            if ($scope.objOrgUnit.ShortText !== null && $scope.objOrgUnit.ShortText !== '') {
                                $scope.Orgsettings.OrgUnitShortText = '( ' + $scope.objOrgUnit.ShortText + ' )';
                            }
                        }
                    }
                }, function errorCallback(response) {
                });
            };
            $scope.getOrgUnitShortText();


            $scope.getImageProxy = function (urlImage) {
                var urlImage = window.encodeURIComponent(urlImage);
                var URL = CONFIG.SERVER + 'workflow/GetProfileImageProxy/?Param1=' + urlImage;
                return URL;
            };
            $scope.ImageProfilePath = "";
            $scope.getImageProfileURL = function (employeeid) {
                if (employeeid === undefined || employeeid === null) return "";
                return $scope.ImageProfilePath + employeeid.substring(2) + ".jpg";
            }
            $scope.getVersionSystem = function () {
                $scope.CurrentVersion = "0.0.1";

                var URL = CONFIG.SERVER + 'Share/GetCurrentVersion/';
                $http({
                    method: 'GET',
                    url: URL
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.CurrentVersion = response.data;
                    }
                }, function errorCallback(response) {
                    // Error
                    $scope.RequestorText = angular.copy($scope.Text);
                });


            }
            $scope.getVersionSystem();
            /*
            $scope.getImageProfilePath = function () {
                var URL = CONFIG.SERVER + 'workflow/GetImageProfilePath/';
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.ImageProfilePath = response.data;
                    }
                }, function errorCallback(response) {
                    // Error
                    $scope.ImageProfilePath = "";
                        console.log('error GetImageProfilePath.');
                });
            };
            */

            // $scope.getImageProfilePath();

            /* helper method */

            $scope.parseDateStringToObj = function (dateString) {
                if (typeof (dateString) != 'undefined' && dateString != '') {
                    var from = dateString.split("/");
                    var dateObj = new Date(from[2], from[1] - 1, from[0]); // (year,month,date)
                    return dateObj;
                }
                return new Date();
            };

            $scope.getLocalDateString = function (dateString) {
                if (typeof (dateString) != 'undefined' && typeof (dateString) == 'string' && dateString != '') {
                    return dateString + '+07:00';
                }
                return '';
            };

            $scope.showText = function (sourceText) {
                if (angular.isDefined(sourceText) || sourceText == null || sourceText == '') {
                    return '-'
                }
                return sourceText;
            };

            $scope.zeroPaddingText = function (sourceText, digit) {
                var str = '';
                for (var i = 0; i < digit; i++) {
                    str += '0';
                }
                sourceText = str + sourceText.toString();
                return sourceText.substr((-1 * digit), digit);
            };

            $scope.roundDecimal = function (number) {
                var checkNumber = Number(number);
                if (isNaN(checkNumber)) {
                    return 0;
                } else {
                    return Math.round(checkNumber * 100) / 100;
                }
            };

            Array.prototype.sum = function (prop) {
                var total = 0
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (angular.isString(this[i][prop])) {
                        total += $scope.roundDecimal(parseFloat(this[i][prop].replace(',', '')));
                    }
                    else {
                        total += $scope.roundDecimal(parseFloat(this[i][prop]));
                    }
                }
                return total
            };

            Array.prototype.isZero = function (prop) {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (angular.isString(this[i][prop])) {
                        if (this[i][prop].replace(',', '') <= 0) return true;
                    }
                    else {
                        if (this[i][prop] <= 0) return true;
                    }
                }
                return ret;
            };

            Array.prototype.nullProp = function (prop) {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (!this[i][prop]) return true;
                }
                return ret;
            };

            Array.prototype.PropHasValue = function (prop) {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i][prop]) return true;
                }
                return ret;
            };

            Array.prototype.hasNull = function () {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (!this[i]) return true;
                }
                return ret;
            };

            Array.prototype.findIndexWithAttr = function (prop, value) {
                var ret = -1;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop] == value)
                            return i;
                }
                return ret;
            };

            Array.prototype.duplicate = function (value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i] == value) return true;
                }
                return false;
            };

            Array.prototype.duplicateProp = function (prop, value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop] == value)
                            return true;
                }
                return false;
            };

            Array.prototype.duplicatePropOfProp = function (prop, propOfProp, value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop])
                            if (this[i][prop][propOfProp] == value)
                                return true;
                }
                return false;
            };

            Array.prototype.propIndexOf = function (prop, val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop].toLowerCase().indexOf(val) > -1)
                            return i;
                }
                return -1;
            };

            Array.prototype.likeIndexOf = function (val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i].toLowerCase().indexOf(val) > -1)
                        return i;
                }
                return -1;
            };

            Array.prototype.likeWithMappingIndexOf = function (func, val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (func[this[i]].toLowerCase().indexOf(val) > -1)
                        return i;
                }
                return -1;
            };

            $scope.MathRounding = function (amount) {
                return Math.round(amount * 100) / 100;
            };

            $scope.MathRoundingToString = function (amount) {
                return (Math.round(amount * 100) / 100).toFixed(2);
            };

            /* !helper method */

            // document ready
            angular.element(document).ready(function () {
                $('body').on('keypress', 'input[type="number"]', function (evt) {
                    //alert(this.value);
                    //alert($(this).val());
                    //alert($(evt.target).val());
                    var key = evt.which;
                    if (!evt.ctrlKey) {
                        if (!((key >= 48 && key <= 57) || key == 0 || key == 8 || key == 46)) {
                            evt.preventDefault();
                        }
                    }
                });

                //$('body').on('change', 'input[type="number"]', function (evt) {
                //    $(this).val(parseFloat($(this).val()).toFixed(2));
                //});

                $('body').on('keypress', 'input[type="text"].only-number', function (evt) {
                    var key = evt.which;
                    if (!evt.ctrlKey) {
                        if (!((key >= 48 && key <= 57) || key == 0 || key == 8)) {
                            evt.preventDefault();
                        }
                    }
                });

                $('body').on('keypress', 'input[type="text"].only-decimal', function (evt) {
                    var key = evt.which;
                    if (!evt.ctrlKey) {
                        if (!((key >= 48 && key <= 57) || key == 0 || key == 8 || key == 46)) {
                            evt.preventDefault();
                        }
                    }
                });

                $('body').on('click', 'div[data-toggle="collapse2"]', function (evt) {
                    $(this).next('div.collapse').slideToggle();
                });

            });

            $scope.$on('$viewContentLoaded', function () {
                $window.scrollTo(0, 0);
            });

            $scope.$on("$locationChangeStart", function (event, next, current) {
                console.log('$locationChangeStart');
                // prevent user accidentally leave edit document page
                if ($scope.documentState.isDirty) {
                    event.preventDefault();
                    var confirm = $mdDialog.confirm()
                        .title($scope.Text['SYSTEM']['CONFIRM_REDIRECT_TITLE'])
                        .textContent($scope.Text['SYSTEM']['CONFIRM_REDIRECT_DETAIL'])
                        .ok('OK')
                        .cancel('Cancel');
                    $mdDialog.show(confirm).then(function (result) {
                        $scope.documentState.isDirty = false;
                        var cutoff = CONFIG.SERVER + 'Client/index.html#!';

                        //แก้ภาษาต่างดาวที่ link ตอนดูผู้ใต้บังคับบัญชา

                        if (next.split('/')[next.split('/').length - 2].includes("%")) {
                            var txt = next.split('/')[next.split('/').length - 2];
                            next = next.replace(txt, decodeURIComponent(txt))
                        }
                        if (next.split('/')[next.split('/').length - 3].includes("%")) {
                            var txt = next.split('/')[next.split('/').length - 3];
                            next = next.replace(txt, decodeURIComponent(txt))
                        }

                        var newNext = next.replace(cutoff, '');
                        //console.debug(newNext);
                        $location.path(newNext);
                    }, function () {
                        event.preventDefault();
                    });
                }

            });

            $scope.scrollToTop = function () {
                $('#main_scroll').animate({ scrollTop: 0 }, 'fast');
            }

            $scope.setBlockAction = function (isBLock, blockMessage) {
                $scope.blockAction.isBLock = isBLock;
                $scope.blockAction.blockMessage = blockMessage;
            }


            //GUID gen
            $scope.guid = function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            }

            $scope.limMaxArr = function (arr, max) {
                var tempArr = []; var tempArrIn = angular.copy(arr);
                if (tempArrIn.length > max) { tempArr = tempArrIn.splice(0, max); } else { return arr; }
                return tempArr
            }

            $scope.FILELIMIT_DESCRIPTION = function () {
                var oResult = String.format($scope.Text['SYSTEM']['FILE_LIMIT']
                    , (CONFIG.FILE_SETTING.ALLOW_FILESIZE / CONFIG.FILE_SETTING.DIVIDE_FILESIZE)
                    , CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH);
                return oResult;
            }
            $scope.AllowUploadFile = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
            }
            $scope.AllowUploadFileType = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
            }
            $scope.AllowFileNameLength = function (fileNameLength) {
                return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
            }
            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            }
            $scope.AllowFileZeroSize = function (fileSize) {
                return fileSize > 0;
            }
            //Pariyaporn P. Add 2021-03-10 for validate display footer
            // window.localStorage.setItem('currentPageIsSwitchPositon', 'SwitchPosition');
            //var currentPage = window.localStorage.getItem('currentPageIsSwitchPositon');
            //if (currentPage == null || currentPage) {
            //    $scope.hideFooter = true;
            //} else {
            //    $scope.hideFooter = false;
            //}

            //Nattawat S. Add 2021-01-25
            $scope.selectPositionAfterLogin = function () {
                alert($scope.SelectedPosition);
                var Position = $scope.SelectedPosition;
                var NewPositionID = Position.ObjectID;
                var NewPositionName = $scope.CurrentEmployeeLanguage == 'EN' ? Position.TextEn : Position.Text;

                $scope.PROFILE_SETTING.POSITIONID = NewPositionID;
                $scope.profile = getToken(CONFIG.USER);
                $scope.profile.PositionID = NewPositionID;
                $scope.profile.Position = NewPositionName;
                $scope.profile.RequesterPositionID = NewPositionID;

                //$scope.PositionSelected = true;
                window.localStorage.setItem(CONFIG.USER, JSON.stringify($scope.profile));
                window.localStorage.setItem(CONFIG.POSITION_SELECTED, true);
                $rootScope.isShowSwitPositionPage = true;
                window.location.reload();

            };

        }]);

})();