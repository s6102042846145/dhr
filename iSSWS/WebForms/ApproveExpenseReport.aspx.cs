﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;
using System.IO;

namespace iSSWS.WebForms
{
    /// <summary>
    /// รายงานการอนุมัติเอกสาร
    /// </summary>
    public partial class ApproveExpenseReport : ReportPage
    {       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExportData();
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetApproveExpenseReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetApproveExpenseReport(this.TravelReportParams).Tables[0].Rows);
            }

            return ds.Tables[0];
        }
        public void ExportExcel()
        {
            try
            {
                DataTable dt = GetData();

                string attachment = "attachment; filename=ApproveExpenseReport.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "\t";
                String ExpenseTypeName = "";
                if (this.TravelReportParams.ExpenseTypeID == 0)
                    ExpenseTypeName = "ค่าใช้จ่ายเดินทาง";
                else
                    ExpenseTypeName = "ค่าใช้จ่ายเบ็ดเตล็ดสำหรับเดินทาง";

                Response.Write("\t\t\t\t\t\t\t\tรายงานการอนุมัติเอกสาร\n");
                Response.Write("\t\t\t\t\t\t\t\t" + dt.Rows[0]["CompanyName"] + "\n");
                Response.Write("\t\t\t\t\t\t\t\tรายงาน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + "\n");

                if (this.TravelReportParams.ClientCompanyID <= 0)
                    Response.Write("\t\tClient Company\t\t-\n");
                else
                    Response.Write("\t\tClient Company\t\t" + this.TravelReportParams.ClientCompanyName + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.OrgUnitID))
                    Response.Write("\t\tรหัสหน่วยงาน \t\t-\t");
                else
                    Response.Write("\t\tรหัสหน่วยงาน \t\t" + this.TravelReportParams.OrgUnitID + "\t");

                if (DateTime.MinValue == this.TravelReportParams.SubmitBeginDate)
                    Response.Write("\t\t\t\t\t\tวันที่สร้างเอกสาร\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\tวันที่สร้างเอกสาร\t\t" + this.TravelReportParams.SubmitBeginDate + "\t\tถึง\t" + this.TravelReportParams.SubmitEndDate + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.RequestorName))
                    Response.Write("\t\tรหัสพนักงาน\t\t-\t");
                else
                    Response.Write("\t\tรหัสพนักงาน\t\t" + this.TravelReportParams.RequestorName + "\t");

                if (DateTime.MinValue == this.TravelReportParams.ApproveBeginDate)
                    Response.Write("\t\t\t\t\t\tวันที่อนุมัติ \t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\tวันที่อนุมัติ \t\t" + this.TravelReportParams.ApproveBeginDate + "\t\tถึง\t" + this.TravelReportParams.ApproveEndDate + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.PositionID))
                    Response.Write("\t\tตำแหน่ง \t\t-\n");
                else
                    Response.Write("\t\tตำแหน่ง \t\t" + this.TravelReportParams.Position + "\n");
                
                if (string.IsNullOrEmpty(this.TravelReportParams.CostCenterCodeBegin))
                    Response.Write("\t\tรหัสศูนย์ต้นทุน\t\t-\n");
                else
                    Response.Write("\t\tรหัสศูนย์ต้นทุน\t\t'" + this.TravelReportParams.CostCenterCodeBegin + "\n");

                if(string.IsNullOrEmpty(this.TravelReportParams.IOID))
                    Response.Write("\t\tรหัสงบประมาณ\t\t-\n");
                else
                    Response.Write("\t\tรหัสงบประมาณ\t\t'" + this.TravelReportParams.IOID + "\n");

                Response.Write("\t\tประเภทค่าใช้จ่าย\t\t" + ExpenseTypeName + "\n");
                Response.Write("\n\n");
                //Gend Columns

                //Response.Write("No." + tab);
                Response.Write("Client Company" + tab + tab);
                Response.Write("เลขที่สัญญา" + tab);
                Response.Write("เลขที่เอกสาร" + tab);
                Response.Write("สถานะ" + tab);
                Response.Write("วันที่ขออนุมัติ" + tab);
                Response.Write("ชื่อพนักงาน" + tab + tab);
                Response.Write("วัตถุประสงค์" + tab);
                Response.Write("กลุ่มค่าใช้จ่าย" + tab);
                Response.Write("รายการ" + tab);
                Response.Write("ผู้ขาย/ผู้จ้าง" + tab);
                Response.Write("รหัสศุนย์ต้นทุน" + tab);
                Response.Write("จำนวนเงิน" + tab);
                Response.Write("ผู้มีอำนาจอนุมัติ" + tab);
                Response.Write("วันที่อนุมัติ" + tab);
                Response.Write("BudgetHolder" + tab);
                Response.Write("\n");

                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    Response.Write(dr["ContractClientCompanyAbbrv"] + tab);
                    Response.Write(dr["ContractClientCompanyName"] + tab);
                    Response.Write("'" + dr["ContractNo"] + tab);
                    Response.Write(dr["RequestNo"] + tab);
                    Response.Write(dr["Status"] + tab);
                    Response.Write(dr["SubmitDate"] + tab);
                    Response.Write(dr["EmployeeID"] + tab);
                    Response.Write(dr["RequestName"] + tab);
                    Response.Write(dr["Detail"].ToString().Replace("\n", "") + tab);
                    Response.Write(dr["ExpenseGroupRemark"] + tab);
                    Response.Write(dr["ExpenseType"] + tab);
                    Response.Write(dr["VendorName"] + tab);
                    if (dr["IsHeader"].ToString() == "1")
                    {
                        Response.Write("รวม" + tab);
                        Response.Write(String.Format("{0:#,###,##0.00}", dr["ExpenseAmount"]) + tab);
                    }
                    else
                    {
                        Response.Write("'" + dr["CostcenterCode"] + tab);
                        Response.Write(String.Format("{0:#,###,##0.00}", dr["ExpenseAmount"]) + tab);
                    }
                    Response.Write(dr["LastApproveName"] + tab);
                    Response.Write(dr["LastApproveDate"] + tab);
                    Response.Write(dr["BudgetHolder"] + tab);
                    
                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {

            }
        }
        public void ExportData()
        {
            string fileRdlc = "";
            string tableName = "ApproveExpenseReport";
            string filename = "ApproveExpenseReport";

            if (this.TravelReportParams.RequestorCompanyCode == "0030")
                fileRdlc = "ApproveExpenseReport";
            else
                fileRdlc = "ApproveExpensePTTReport";

            HRTRManagement oHRTR = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);
            string rootPath = oHRTR.FILE_ROOTPATH;
            byte[] ar;
            ar = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode).saveFileReport(fileRdlc, tableName, rootPath, this.TravelReportParams);

            MemoryStream stream = new MemoryStream();
            stream.Write(ar, 0, ar.Length);

            if(this.TravelReportParams.ExportType == "EXCEL")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            else if(this.TravelReportParams.ExportType == "PDF")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".pdf");
            Response.ContentType = "application/octectstream";
            Response.BinaryWrite(ar);
            Response.End();
        }
    }
}