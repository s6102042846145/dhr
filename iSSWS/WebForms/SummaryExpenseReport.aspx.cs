﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;
using System.Text.RegularExpressions;
using System.IO;

namespace iSSWS.WebForms
{
    /// <summary>
    /// รายงานสรุปข้อมูลค่าใช้จ่ายเดินทาง
    /// </summary>
    public partial class SummaryExpenseReport : ReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExportData();
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetSummaryExpenseReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetSummaryExpenseReport(this.TravelReportParams).Tables[0].Rows);
            }
            return ds.Tables[0];
        }
        void ExportExcel()
        {
            try
            {
                DataTable dt = GetData();
                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment; filename=SummaryExpenseReport.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentEncoding = System.Text.Encoding.Unicode;
                    Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                    Response.ContentType = "application/vnd.ms-excel";
                    string tab = "\t";

                    Response.Write("\t\t\t\t\t\t\t\tรายงานสรุปค่าใช้จ่ายเดินทาง\n");
                    Response.Write("\t\t\t\t\t\t\t\t" + dt.Rows[0]["CompanyName"] + "\n");
                    Response.Write("\t\t\t\t\t\t\t\tรายงาน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + "\n");

                    if (this.TravelReportParams.ClientCompanyID <= 0)
                        Response.Write("\t\tClient Company\t\t-\n");
                    else
                        Response.Write("\t\tClient Company\t\t" + this.TravelReportParams.ClientCompanyName + "\n");

                    if (string.IsNullOrEmpty(this.TravelReportParams.OrgUnitID))
                        Response.Write("\t\tรหัสหน่วยงาน \t\t-\n");
                    else
                        Response.Write("\t\tรหัสหน่วยงาน \t\t" + this.TravelReportParams.OrgUnitID + "\n");

                    if (string.IsNullOrEmpty(this.TravelReportParams.RequestorID))
                        Response.Write("\t\tรหัสพนักงาน \t\t-\t");
                    else
                        Response.Write("\t\tรหัสพนักงาน \t\t" + this.TravelReportParams.RequestorName + "\t");

                    if(this.TravelReportParams.TravelBeginDate == DateTime.MinValue)
                        Response.Write("\t\t\tช่วงเวลาเดินทาง \t\t-\t\tถึง\t-\n");
                    else
                        Response.Write("\t\t\tช่วงเวลาเดินทาง \t\t" + this.TravelReportParams.TravelBeginDate.ToString("dd/MM/yyyy") + "\t\tถึง\t" + this.TravelReportParams.TravelEndDate.ToString("dd/MM/yyyy") + "\n");

                    if (string.IsNullOrEmpty(this.TravelReportParams.PositionID))
                        Response.Write("\t\tตำแหน่ง \t\t-\t");
                    else
                        Response.Write("\t\tตำแหน่ง \t\t" + this.TravelReportParams.Position + "\t");

                    Response.Write("\n\n");
                    //Gend Columns

                    Response.Write("ClientCompany" + tab + tab);
                    Response.Write("เลขที่สัญญา" + tab);
                    Response.Write("ชื่อพนักงาน" + tab + tab);
                    Response.Write("เลขที่เอกสาร" + tab);
                    Response.Write("หน่วยงาน" + tab + tab);
                    Response.Write("ผู้อนุมัติ" + tab);
                    Response.Write("สถานที่เดินทาง" + tab);
                    Response.Write("ช่วงเวลาเดินทาง" + tab);
                    Response.Write("จำนวนเงิน (ที่ขออนุมัติ)" + tab);
                    Response.Write("จำนวนเงิน (ค่าใช้จ่าย)" + tab);
                    Response.Write("สถานะ" + tab);
                    Response.Write("\n");

                    int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        Response.Write(dr["ContractClientCompanyAbbrv"] + tab);
                        Response.Write(dr["ContractClientCompanyName"] + tab);
                        Response.Write("'" + dr["ContractNo"] + tab);
                        Response.Write(dr["EmployeeID"] + tab);
                        Response.Write(dr["EmployeeName"] + tab);
                        Response.Write(dr["RequestNo"] + tab);
                        Response.Write("'" + dr["OrgUnitID"] + tab);
                        Response.Write(ValidateWithOutHTML(dr["OrgUnit"].ToString()) + tab);
                        Response.Write(dr["LastApproveName"] + tab);
                        Response.Write(dr["Place"] + tab);
                        Response.Write(dr["TravelBeginDate"] + " - " + dr["TravelEndDate"] + tab);
                        Response.Write(string.Format("{0:#,###,##0.00}",dr["EstimateAmount"]) + tab);
                        Response.Write(string.Format("{0:#,###,##0.00}",dr["ActualAmount"]) + tab);
                        Response.Write(dr["Status"] + tab);
                        Response.Write("\n");
                    }
                    Response.End();
                }
            }
            catch (Exception ex)
            {

            }
        }
        void ExportData()
        {
            string fileRdlc = "";
            string tableName = "SummaryExpenseReport";
            string filename = "SummaryExpenseReport";

            if (this.TravelReportParams.RequestorCompanyCode == "0030")
                fileRdlc = "SummaryExpenseReport";
            else
                fileRdlc = "SummaryExpensePTTReport";

            HRTRManagement oHRTR = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);
            string rootPath = oHRTR.FILE_ROOTPATH;
            byte[] ar;
            ar = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode).saveFileReport(fileRdlc, tableName, rootPath, this.TravelReportParams);

            MemoryStream stream = new MemoryStream();
            stream.Write(ar, 0, ar.Length);

            if(this.TravelReportParams.ExportType == "EXCEL")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            else if(this.TravelReportParams.ExportType == "PDF")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".pdf");
            Response.ContentType = "application/octectstream";
            Response.BinaryWrite(ar);
            Response.End();
        }
        String ValidateWithOutHTML(String strText)
        {
            String strData = "";
            try
            {
                string pattern = "<(.|\n\t)+?>";
                string opattern = "\t";
                strData = Regex.Replace(strText, pattern, " ");
                strData = Regex.Replace(strData, opattern, " ");
            }
            catch (Exception ex)
            {

            }
            return strData;
        }
    }
}