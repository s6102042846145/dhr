﻿using DESS.API.DB;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class JobPayrollServiceManager
    {
        #region Constructor
        public JobPayrollServiceManager()
        {

        }
        #endregion Constructor

        #region MultiCompany Framework
        private string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "DHR.JOB";
            }
        }
        public static JobPayrollServiceManager CreateInstance(string oCompanyCode)
        {
            JobPayrollServiceManager oJobOrgServiceManager = new JobPayrollServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oJobOrgServiceManager;
        }
        #endregion
        public string TARGETMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TARGETMODE");
            }
        }


        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"DESS.API.{Mode.ToUpper()}");
            string typeName = string.Format($"DESS.API.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            return oReturn;
        }

        internal IPayrollDataService JobPayrollDataService
        {
            get
            {
                Type oType = GetService(TARGETMODE, "PayrollDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IPayrollDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
    }
}
