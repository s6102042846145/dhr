﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using DHR.HR.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class APIJobTimeManagement
    {
        #region Constructor
        public APIJobTimeManagement()
        {

        }
        #endregion Constructor

        #region MultiCompany Framework

        private static string MoDuleID = "DHR.JOB";

        private string CompanyCode { get; set; }

        public static APIJobTimeManagement CreateInstance(string oCompanyCode)
        {
            APIJobTimeManagement oAPIJobTimeManagement = new APIJobTimeManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIJobTimeManagement;
        }
        #endregion MultiCompany Framework

        public ResponseModel<TMCalenHoliday> GetAllTMCalenHoliday(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMCalenHoliday(startDate, endDate);
        }

        public string SaveAllTMCalenHoliday(List<TMCalenHoliday> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMCalenHoliday(data);
        }

        public ResponseModel<TMSetupHoliday> GetAllTMSetupHoliday(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMSetupHoliday(startDate, endDate);
        }

        public string SaveAllTMSetupHoliday(List<TMSetupHoliday> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMSetupHoliday(data);
        }

        public ResponseModel<TMPlanWorkTime> GetAllTMPlanWorkTime(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMPlanWorkTime(startDate, endDate);
        }

        public string SaveAllTMPlanWorkTime(List<TMPlanWorkTime> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMPlanWorkTime(data);
        }

        public ResponseModel<TMTimeRecType> GetAllTMTimeRecType(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMTimeRecType(startDate, endDate);
        }

        public string SaveAllTMTimeRecType (List<TMTimeRecType> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMTimeRecType(data);
        }

        public ResponseModel<TMLeaveType> GetAllTMLeaveType(string startDate,string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMLeaveType(startDate, endDate);
        }

        public string SaveAllTMLeaveType(List<TMLeaveType> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMLeaveType(data);
        }

        public ResponseModel<TMWorkingType> GetAllTMWorkingType(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMWorkingType(startDate, endDate);
        }

        public string SaveAllTMWorkingType(List<TMWorkingType> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMWorkingType(data);
        }

        public ResponseModel<TMLeaveQuotaType> GetAllTMLeaveQuotaType(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMLeaveQuotaType(startDate, endDate);
        }

        public string SaveAllTMLeaveQuotaType(List<TMLeaveQuotaType> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMLeaveQuotaType(data);
        }

        public ResponseModel<TMEmpGroup> GetAllTMEmpGroup(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMEmpGroup(startDate, endDate);
        }

        public string SaveAllTMEmpGroup(List<TMEmpGroup> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMEmpGroup(data);
        }

        public ResponseModel<TMPersonalArea> GetAllTMPersonalArea(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMPersonalArea(startDate, endDate);
        }

        public string SaveAllTMPersonalArea(List<TMPersonalArea> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMPersonalArea(data);
        }

        public ResponseModel<TMBreakSchedule> GetAllTMBreakSchedule(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMBreakSchedule(startDate, endDate);
        }

        public string SaveAllTMBreakSchedule(List<TMBreakSchedule> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMBreakSchedule(data);
        }

        public ResponseModel<TMDailyWorkSchedule> GetAllTMDailyWorkSchedule(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMDailyWorkSchedule(startDate, endDate);
        }

        public string SaveAllTMDailyWorkSchedule(List<TMDailyWorkSchedule> data)

        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMDailyWorkSchedule(data);
        }

        public ResponseModel<TMPeriodWorkSchedule> GetAllTMPeriodWorkSchedule(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMPeriodWorkSchedule(startDate, endDate);
        }

        public string SaveAllTMPeriodWorkSchedule(List<TMPeriodWorkSchedule> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMPeriodWorkSchedule(data);
        }

        public ResponseModel<TMWorkSchedule> GetAllTMWorkSchedule(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMWorkSchedule(startDate, endDate);
        }

        public string SaveAllTMWorkSchedule(List<TMWorkSchedule> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMWorkSchedule(data);
        }

        public ResponseModel<TMWorkScheduleRule> GetAllTMWorkScheduleRule(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMWorkScheduleRule(startDate, endDate);
        }

        public string SaveAllTMWorkScheduleRule(List<TMWorkScheduleRule> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMWorkScheduleRule(data);
        }

        public ResponseModel<TMSubsititutionType> GetAllTMSubsititutionType(string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMSubsititutionType(startDate, endDate);
        }

        public string SaveAllTMSubsititutionType(List<TMSubsititutionType> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMSubsititutionType(data);
        }

        public ResponseModel<TMLeaveInf> GetAllTMLeaveInf(string startDate,string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMLeaveInf(startDate, endDate);
        }

        public string SaveAllTMLeaveInf(List<TMLeaveInf> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMLeaveInf(data);
        }

        public ResponseModel<TMTimeAttend> GetAllTMTimeAttend (string startDate, string endDate)
        {
            return APITimeManagement.CreateInstance(CompanyCode).GetDHRTMTimeAttend(startDate, endDate);
        }

        public string SaveAllTMTimeAttendance (List<TMTimeAttend> data)
        {
            return JobTimeServiceManager.CreateInstance(CompanyCode).JobTimeDataService.SaveAllTMTimeAttend(data);
        }
    }
}
