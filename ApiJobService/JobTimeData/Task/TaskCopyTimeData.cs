﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.JOB.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;

namespace DESS.JOB
{
    public class TaskCopyTimeData : ITaskWorker
    {
        public TaskCopyTimeData()
        {

        }
        public string CompanyCode { get; set ; }
        public int TaskID { get ; set ; }
        public int JobID { get; set; }
        public string ConfigName { get; set; }

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        private string DefaultEndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }



        public void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            string oWorkAction = string.Format($"Try to copy D-HR Data {ConfigName} to DB");
            Console.WriteLine(oWorkAction);

            APIJobTimeManagement oApiJobTimeData = APIJobTimeManagement.CreateInstance(CompanyCode);

            switch (ConfigName.ToUpper())
            {
                case "CALENHOLIDAY":
                    List<TMCalenHoliday> calenHoliday = oApiJobTimeData.GetAllTMCalenHoliday(DefaultBeginDate, DefaultEndDate).Data;
                    if (calenHoliday != null && calenHoliday.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {calenHoliday.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMCalenHoliday(calenHoliday);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {calenHoliday.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {calenHoliday.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {calenHoliday.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "SETUPHOLIDAY":
                    List<TMSetupHoliday> setupHoliday = oApiJobTimeData.GetAllTMSetupHoliday(DefaultBeginDate, DefaultEndDate).Data;
                    if (setupHoliday != null && setupHoliday.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {setupHoliday.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMSetupHoliday(setupHoliday);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                       // EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {setupHoliday.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {setupHoliday.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {setupHoliday.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "PLANWORKTIME":
                    List<TMPlanWorkTime> planWorkTime = oApiJobTimeData.GetAllTMPlanWorkTime(DefaultBeginDate, DefaultEndDate).Data;
                    if (planWorkTime != null && planWorkTime.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {planWorkTime.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMPlanWorkTime(planWorkTime);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {planWorkTime.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {planWorkTime.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {planWorkTime.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "TIMERECTYPE":
                    List<TMTimeRecType> timeRecType = oApiJobTimeData.GetAllTMTimeRecType(DefaultBeginDate, DefaultEndDate).Data;
                    if (timeRecType != null && timeRecType.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {timeRecType.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMTimeRecType(timeRecType);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {timeRecType.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {timeRecType.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {timeRecType.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "LEAVETYPE":
                    List<TMLeaveType> leaveType = oApiJobTimeData.GetAllTMLeaveType(DefaultBeginDate, DefaultEndDate).Data;
                    if (leaveType != null && leaveType.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {leaveType.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMLeaveType(leaveType);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {leaveType.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {leaveType.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {leaveType.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "WORKINGTYPE":
                    List<TMWorkingType> workingType = oApiJobTimeData.GetAllTMWorkingType(DefaultBeginDate, DefaultEndDate).Data;
                    if (workingType != null && workingType.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {workingType.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMWorkingType(workingType);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {workingType.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {workingType.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {workingType.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "LEAVEQUOTATYPE":
                    List<TMLeaveQuotaType> leaveQuotaType = oApiJobTimeData.GetAllTMLeaveQuotaType(DefaultBeginDate, DefaultEndDate).Data;
                    if (leaveQuotaType != null && leaveQuotaType.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {leaveQuotaType.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMLeaveQuotaType(leaveQuotaType);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {leaveQuotaType.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {leaveQuotaType.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {leaveQuotaType.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "EMPGROUP":
                    List<TMEmpGroup> empGroup = oApiJobTimeData.GetAllTMEmpGroup(DefaultBeginDate, DefaultEndDate).Data;
                    if (empGroup != null && empGroup.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {empGroup.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMEmpGroup(empGroup);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {empGroup.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {empGroup.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {empGroup.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "PERSONALAREA":
                    List<TMPersonalArea> personalArea = oApiJobTimeData.GetAllTMPersonalArea(DefaultBeginDate, DefaultEndDate).Data;
                    if (personalArea != null && personalArea.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {personalArea.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMPersonalArea(personalArea);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {personalArea.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {personalArea.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {personalArea.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "BREAKSCHEDULE":
                    List<TMBreakSchedule> breakSchedule = oApiJobTimeData.GetAllTMBreakSchedule(DefaultBeginDate, DefaultEndDate).Data;
                    if (breakSchedule != null && breakSchedule.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {breakSchedule.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMBreakSchedule(breakSchedule);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                    //    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {breakSchedule.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {breakSchedule.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {breakSchedule.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "DAILYWORKSCHEDULE":
                    List<TMDailyWorkSchedule> dailyWorkSchedule = oApiJobTimeData.GetAllTMDailyWorkSchedule(DefaultBeginDate, DefaultEndDate).Data;
                    if (dailyWorkSchedule != null && dailyWorkSchedule.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {dailyWorkSchedule.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMDailyWorkSchedule(dailyWorkSchedule);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {dailyWorkSchedule.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {dailyWorkSchedule.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {dailyWorkSchedule.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "PERIODWORKSCHEDULE":
                    List<TMPeriodWorkSchedule> periodWorkSchedule = oApiJobTimeData.GetAllTMPeriodWorkSchedule(DefaultBeginDate, DefaultEndDate).Data;
                    if (periodWorkSchedule != null && periodWorkSchedule.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {periodWorkSchedule.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMPeriodWorkSchedule(periodWorkSchedule);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {periodWorkSchedule.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {periodWorkSchedule.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {periodWorkSchedule.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "WORKSCHEDULE":
                    List<TMWorkSchedule> workSchedule = oApiJobTimeData.GetAllTMWorkSchedule(DefaultBeginDate, DefaultEndDate).Data;
                    if (workSchedule != null && workSchedule.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {workSchedule.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMWorkSchedule(workSchedule);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {workSchedule.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {workSchedule.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {workSchedule.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "WORKSCHEDULERULE":
                    List<TMWorkScheduleRule> workScheduleRule = oApiJobTimeData.GetAllTMWorkScheduleRule(DefaultBeginDate, DefaultEndDate).Data;
                    if (workScheduleRule != null && workScheduleRule.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {workScheduleRule.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMWorkScheduleRule(workScheduleRule);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {workScheduleRule.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {workScheduleRule.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {workScheduleRule.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "SUBSITITUTIONTYPE":
                    List<TMSubsititutionType> subsititutionType = oApiJobTimeData.GetAllTMSubsititutionType(DefaultBeginDate, DefaultEndDate).Data;
                    if (subsititutionType != null && subsititutionType.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {subsititutionType.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMSubsititutionType(subsititutionType);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {subsititutionType.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {subsititutionType.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {subsititutionType.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "LEAVE":
                    List<TMLeaveInf> leaveInf = oApiJobTimeData.GetAllTMLeaveInf(DefaultBeginDate, DefaultEndDate).Data;
                    if (leaveInf != null && leaveInf.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {leaveInf.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMLeaveInf(leaveInf);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {leaveInf.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {leaveInf.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {leaveInf.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;

                case "ATTENDANCE":
                    List<TMTimeAttend> timeAttend = oApiJobTimeData.GetAllTMTimeAttend(DefaultBeginDate, DefaultEndDate).Data;
                    if (timeAttend != null && timeAttend.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {timeAttend.Count()} recored(s) completed");
                        ErrMsg = oApiJobTimeData.SaveAllTMTimeAttendance(timeAttend);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {timeAttend.Count()}"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {timeAttend.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {timeAttend.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }

                    break;
            }
        }
    }
}
