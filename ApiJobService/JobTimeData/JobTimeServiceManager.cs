﻿using DESS.API.DB;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace DESS.JOB
{
    public class JobTimeServiceManager
    {
        #region Constructor
        public JobTimeServiceManager()
        {

        }
        #endregion

        #region MultiCompany Framework

        private string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "DHR.JOB";
            }
        }

        public static JobTimeServiceManager CreateInstance(string oCompanyCode)
        {
            JobTimeServiceManager oJobTimeServiceManager = new JobTimeServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oJobTimeServiceManager;
        }

        public string TARGETMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TARGETMODE");
            }
        }

        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"DESS.API.{Mode.ToUpper()}");
            string typeName = string.Format($"DESS.API.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);

            return oReturn;
        }

        internal ITimeDataService JobTimeDataService
        {
            get
            {
                Type oType = GetService(TARGETMODE, "TimeDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ITimeDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
        #endregion
    }
}
