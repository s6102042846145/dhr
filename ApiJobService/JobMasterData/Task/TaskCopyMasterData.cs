﻿using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.JOB.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DESS.JOB
{
    public class TaskCopyMasterData : ITaskWorker
    {
        public TaskCopyMasterData()
        {

        }
        public string CompanyCode { get; set; }
        public int TaskID { get; set; }
        public string ConfigName { get; set; }

        private string MasterBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "MASTERBEGINDATEFORMAT");
            }
        }

        private string MasterEndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "MASTERENDDATEFORMAT");
            }
        }

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        private string DefaultEndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        public void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }


            string oWorkAction = string.Format($"Try to copy D-HR Data {ConfigName} to DB");
            Console.WriteLine(oWorkAction);
            APIJobMasterDataManagement oApiJobMasterData = APIJobMasterDataManagement.CreateInstance(CompanyCode);

            switch (ConfigName.ToUpper())
            {
                case "MASTERDATACODE":
                    var tmpDataCode = oApiJobMasterData.GetAllMasterDataCode(MasterBeginDate, MasterEndDate);
                    List<MasterDataCode> dataCode = tmpDataCode == null ? new List<MasterDataCode>() : tmpDataCode.Data;
                    if (dataCode != null && dataCode.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {dataCode.Count()} record(s) completed.");
                        ErrMsg = oApiJobMasterData.SaveAllMasterDataCode(dataCode);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {dataCode.Count()} recordd(s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {dataCode.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {dataCode.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }

                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;

                case "MASTERDATAVALUE":
                    var tmpDataValue = oApiJobMasterData.GetAllMasterDataValue(MasterBeginDate, MasterEndDate);
                    List<MasterDataValue> dataValue = tmpDataValue == null ? new List<MasterDataValue>() : tmpDataValue.Data;
                    if (dataValue != null && dataValue.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {dataValue.Count()} record(s) completed.");
                        ErrMsg = oApiJobMasterData.SaveAllMasterDataValue(dataValue);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {dataValue.Count()} record(s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {dataValue.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {dataValue.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }

                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;

                case "BANKMASTER":
                    APIJobPayrollManagement oApiJobPayrollData = APIJobPayrollManagement.CreateInstance(CompanyCode);
                    var tmpBankMaster = oApiJobPayrollData.GetAllBankMaster(DefaultBeginDate, DefaultEndDate);
                    List<PYBankMaster> bankMaster = tmpBankMaster == null ? new List<PYBankMaster>() : tmpBankMaster.Data;
                    if (bankMaster != null && bankMaster.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {bankMaster.Count()} record(s) completed.");
                        ErrMsg = oApiJobPayrollData.SaveAllBankMaster(bankMaster);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {bankMaster.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {bankMaster.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }

                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;
            }
        }
    }
}
