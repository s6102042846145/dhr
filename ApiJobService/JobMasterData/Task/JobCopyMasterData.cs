﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.JOB.INTERFACE;
using ESS.JOB.DATACLASS;
using ESS.WORKFLOW;

namespace DESS.JOB
{
    public class JobCopyMasterData : IJobWorker
    {
        public JobCopyMasterData()
        {

        }
        public List<JobParam> Params { get; set ; }
        public string CompanyCode { get ; set ; }

        public List<ITaskWorker> LoadTasks()
        {
            if(string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            List<ITaskWorker> returnValue = new List<ITaskWorker>();

            TaskCopyMasterData task;

            task = new TaskCopyMasterData();
            task.ConfigName = "MasterDataCode";
            task.TaskID = 1;

            returnValue.Add(task);

            task = new TaskCopyMasterData();
            task.TaskID = 2;
            task.ConfigName = "MasterDataValue";
            returnValue.Add(task);

            task = new TaskCopyMasterData();
            task.TaskID = 3;
            task.ConfigName = "BankMaster";
            returnValue.Add(task);

            return returnValue;
        }
    }
}
