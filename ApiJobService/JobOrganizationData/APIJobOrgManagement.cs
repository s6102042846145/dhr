﻿using DHR.HR.API.Model;
using DHR.HR.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Shared;

namespace DESS.JOB
{
    public class APIJobOrgManagement
    {
        #region Constructor
        public APIJobOrgManagement()
        {

        }
        #endregion Constructor

        #region MultiCompany Framework
        private static string ModuleID = "DHR.JOB";
        public string CompanyCode { get; set; }

        public static APIJobOrgManagement CreateInstance(string oCompanyCode)
        {
            APIJobOrgManagement oAPIJobOrgManagement = new APIJobOrgManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAPIJobOrgManagement;
        }

        #endregion MultiCompany FrameWork

        #region API Unused

        //public ResponseModel<BelongTo> GetAllBelongTo (string startDate,string endDate)
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRBelongTo(startDate, endDate);

        //}

        //public string SaveAllBelongTo(List<BelongTo> data , string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllBelongTo(data, "");
        //}

        //public ResponseModel<ComSecondment> GetAllComSecondment (string startDate, string endDate )
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRComSecondment(startDate, endDate);
        //}

        //public string SaveAllComSecondment(List<ComSecondment> data ,string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllComSecondment(data, Profile);
        //}

        //public ResponseModel<Company> GetAllCompany (string startDate , string endDate)
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRCompany(startDate, endDate);
        //}

        //public string SaveAllCompany (List<Company> data ,string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllCompany(data, Profile);
        //}

        //public ResponseModel<FiscalYear> GetAllFiscalYear (string startDate, string endDate )
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRFiscalYear(startDate, endDate);
        //}

        //public string SaveAllFiscalYear(List<FiscalYear> data, string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllFiscalYear(data, Profile);
        //}

        //public ResponseModel<Position> GetAllPosition (string startDate , string endDate)
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRPosition(startDate, endDate);
        //}

        //public string SaveAllPosition (List<Position> data , string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllPosition(data, Profile);
        //}

        //public ResponseModel<ReportTo> GetAllReportTo(string startDate,string endDate)
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRReportTo(startDate, endDate);
        //}

        //public string SaveAllReportTo (List<ReportTo> data , string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllReportTo(data, Profile);
        //}

        //public ResponseModel<Unit> GetAllUnit (string startDate , string endDate )
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRUnit(startDate, endDate);
        //}

        //public string SaveAllUnit(List<Unit> data , string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllUnit(data, Profile);
        //}

        //public ResponseModel<SolidLine> GetAllSolidLine(string startDate,string endDate)
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRSolidLine(startDate, endDate);
        //}

        //public string SaveAllSolidLine(List<SolidLine> data, string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllSolidLine(data, Profile);
        //}

        //public ResponseModel<DotedLine> GetAllDotedLine(string startDate ,string endDate)
        //{
        //    return APIOrgManagement.CreateInstance(CompanyCode).GetDHRDotedLine(startDate, endDate);
        //}
        //public string SaveAllDotedLine(List<DotedLine> data, string Profile)
        //{
        //    return JobOrgServiceManager.CreateInstance(CompanyCode).JobOrgService.SaveAllDotedLine(data, Profile);
        //}
        #endregion

    }
}
