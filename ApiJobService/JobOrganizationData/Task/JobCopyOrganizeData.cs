﻿using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.JOB;
using ESS.JOB.INTERFACE;
using ESS.JOB.DATACLASS;


namespace DESS.JOB
{
    public class JobCopyOrganizeData : IJobWorker
    {
        public JobCopyOrganizeData()
        {

        }
       
        public string CompanyCode { get ; set ; }
        public List<JobParam> Params { get ; set ; }
      

        public List<ITaskWorker> LoadTasks()
        {
            if(string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyOrganizeData task;

            task = new TaskCopyOrganizeData();
            task.TaskID = 1;
            task.ConfigName = "BelongTo";
            returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 2;
            task.ConfigName = "ComSecondment";
            returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 3;
            task.ConfigName = "Company";
            returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 4;
            task.ConfigName = "Fiscalyear";
            returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 5;
            task.ConfigName = "Position";
            returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 6;
            task.ConfigName = "SolidLine"; // For Reporting To
            returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 7;
            task.ConfigName = "DotedLine"; // For Reporting To
            returnValue.Add(task);

            //Not use No service from DHR  2-Jul-2020
            //task = new TaskCopyOrganizeData();
            //task.TaskID = 8;
            //task.ConfigName = "ReportTo";
            //returnValue.Add(task);

            task = new TaskCopyOrganizeData();
            task.TaskID = 9;
            task.ConfigName = "Unit";
            returnValue.Add(task);


            return returnValue;
        }

       
    }
}
