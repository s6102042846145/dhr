﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using DHR.HR.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class APIJobPersonalManagement
    {
        #region Constructor
        public APIJobPersonalManagement()
        {

        }
        #endregion Constructor
        #region MultiCompany Framework

        private static string MoDuleID = "DHR.JOB";
        // private readonly object APIPersonalMangetment;

        private string CompanyCode { get; set; }

        public static APIJobPersonalManagement CreateInstance(string oCompanyCode)
        {
            APIJobPersonalManagement oAPIJobPersonalManagement = new APIJobPersonalManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIJobPersonalManagement;
        }
        #endregion

        #region Personal
        //INFOTYPE0182
        public ResponseModel<PAPersonalInf> GetAllPersonalInf(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPersonalInf(startDate, endDate);
        }
        public string SaveAllPersonalInf(List<PAPersonalInf> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllPAPersonalInf(data);
        }

        //INFOTYPE0001
        public ResponseModel<PAInfoPersonal> GetAllInfoPersonal(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAInfoPersonal(startDate, endDate);
        }

        //INFOTYPE0001
        public string SaveAllInfoPersonal(List<PAInfoPersonal> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllPAInfoPersonal(data);
        }
      

        #endregion Personal

        #region Organization
        public ResponseModel<PAOrganizationInf> GetAllOrganizationInf(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAOrganizationInf(startDate, endDate);
        }

        public string SaveAllOrganizationInf(List<PAOrganizationInf> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllPAOrganizationInf(data);
        }

        #endregion Organization


        #region WorkCalendar
        //เลิกใช้แล้ว Pariyaporn 20200327
        //public ResponseModel<PAWorkCalendar> GetAllWorkCalendar(string startDate,string endDate)
        //{
        //    return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAWorkCalendar(startDate, endDate);
        //}

        //public string SaveAllWorkCalendar(List<PAWorkCalendar> data)
        //{
        //    return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllWorkCalendar(data);
        //}
        #endregion WorkCalendar

        #region SeconmentInInf
        public ResponseModel<PASecondmentInf> GetAllSecondmentInf(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPASecondmentInf(startDate, endDate);
        }

        public string SaveAllSecondmentInf(List<PASecondmentInf> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllSecondmentInf(data);
        }
        #endregion

        #region ContactInf
        public ResponseModel<PAContactInf> GetAllContactInf(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAContactInf(startDate, endDate);
        }

        public string SaveAllContactInf(List<PAContactInf> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllContactInf(data);
        }
        #endregion

        #region Position
        public ResponseModel<PAPosition> GetAllPosition(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPosition(startDate, endDate);
        }

        public string SaveAllPosition(List<PAPosition> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllPosition(data);
        }
        #endregion


        #region ActingPosition
        public ResponseModel<PAActingPosition> GetAllActingPosition(string startDate, string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAActingPosition(startDate, endDate);
        }

        public string SaveAllActingPosition(List<PAActingPosition> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllActingPosition(data);
        }
        #endregion

        #region ImportanceDateInf
        public ResponseModel<PAImportanceDateInf> GetAllImportanceDateInf(string startDate,string endDate)
        {
            return APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAImportanceDateInf(startDate, endDate);
        }

        public string SaveAllImportanceDateInf(List<PAImportanceDateInf> data)
        {
            return JobPersonServiceManager.CreateInstance(CompanyCode).JobPersonalDataService.SaveAllImportanceDateInf(data);
        }

        #endregion
    }
}
