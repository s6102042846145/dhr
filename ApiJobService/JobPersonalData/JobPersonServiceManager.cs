﻿using DESS.API.DB;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class JobPersonServiceManager
    {
        #region Constructor
        public JobPersonServiceManager()
        {

        }
        #endregion

        #region MultiCompany Framework
        private string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "DHR.JOB";
            }
        }

        public static JobPersonServiceManager CreateInstance(string oCompanyCode)
        {
            JobPersonServiceManager oJobPersonServiceManager = new JobPersonServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oJobPersonServiceManager;
        }

        public string TARGETMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TARGETMODE");
            }
        }

        private Type GetService(string Mode,string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"DESS.API.{Mode.ToUpper()}");
            string typeName = string.Format($"DESS.API.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);

            return oReturn;
        }

        internal IPersonalDataService JobPersonalDataService
        {
            get
            {
                Type oType = GetService(TARGETMODE, "PersonalDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IPersonalDataService)Activator.CreateInstance(oType, CompanyCode);
                }   
            }
        }

        #endregion
    }
}
