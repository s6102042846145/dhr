﻿using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.JOB.INTERFACE;
using ESS.JOB.DATACLASS;

namespace DESS.JOB
{
    public class JobCopyPersonalData : IJobWorker
    {
        public JobCopyPersonalData()
        {

        }
        public List<JobParam> Params { get ; set; }
        public string CompanyCode { get ; set ; }

        public List<ITaskWorker> LoadTasks()
        {
            if(string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyPersonalData task;

            int iTaskid = 1;
            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "OrganizationInf";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "PersonalInf";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "InfPersonal";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "SecondmentInf";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "ContactInf";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "Position";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "ActingPosition";
            returnValue.Add(task);

            task = new TaskCopyPersonalData();
            task.TaskID = iTaskid++;
            task.ConfigName = "ImportanceDateInf";
            returnValue.Add(task);

            return returnValue;
        }
    }
}
