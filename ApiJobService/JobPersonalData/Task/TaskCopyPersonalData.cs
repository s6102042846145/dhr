﻿using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.JOB.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class TaskCopyPersonalData : ITaskWorker
    {
        public TaskCopyPersonalData()
        {

        }
        public string CompanyCode { get; set; }
        public int TaskID { get; set; }

        public string ConfigName { get; set; }

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        private string DefaultEndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }


        public void Run()
        {
            if(string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            string oWorkAction = string.Format($"Try to copy D-HR Data {ConfigName} to DB");
            Console.WriteLine(oWorkAction);

            APIJobPersonalManagement oApiJobPersonalData = APIJobPersonalManagement.CreateInstance(CompanyCode);

            switch(ConfigName.ToUpper())
            {
                case "ORGANIZATIONINF":
                    var tmpOrganization = oApiJobPersonalData.GetAllOrganizationInf(DefaultBeginDate, DefaultEndDate);
                    List<PAOrganizationInf> organizationinf = tmpOrganization == null ? new List<PAOrganizationInf>() : tmpOrganization.Data;
                    if(organizationinf != null && organizationinf.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {organizationinf.Count()} recored(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllOrganizationInf(organizationinf);
                        oWorkAction = oWorkAction + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {organizationinf.Count()}"), oWorkAction, true);
                     
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {organizationinf.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {organizationinf.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);

                    }

                    break;

                case "PERSONALINF": //INFOTYPE 0182
                    var tmpPersonalInf = oApiJobPersonalData.GetAllPersonalInf(DefaultBeginDate, DefaultEndDate);
                    List<PAPersonalInf> personalinf = tmpPersonalInf == null ? new List<PAPersonalInf>() : tmpPersonalInf.Data;
                    if (personalinf != null && personalinf.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {personalinf.Count()} record(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllPersonalInf(personalinf);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {personalinf.Count()} record (s)"), oWorkAction, true);
                        if(string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {personalinf.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {personalinf.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;

                case "INFPERSONAL":
                    var tmpInfoPersonal = oApiJobPersonalData.GetAllInfoPersonal(DefaultBeginDate, DefaultEndDate);
                    List<PAInfoPersonal> infoPersonal = tmpInfoPersonal == null ? new List<PAInfoPersonal>() : tmpInfoPersonal.Data; //INFOTYPE0001
                    if (infoPersonal != null && infoPersonal.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {infoPersonal.Count()} record(s) completed");
                        
                        ErrMsg = oApiJobPersonalData.SaveAllInfoPersonal(infoPersonal);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {infoPersonal.Count()} record (s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {infoPersonal.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {infoPersonal.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;
                case "SECONDMENTINF":
                    List<PASecondmentInf> secondmentinf = oApiJobPersonalData.GetAllSecondmentInf(DefaultBeginDate, DefaultEndDate).Data;
                    if (secondmentinf != null && secondmentinf.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {secondmentinf.Count()} record(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllSecondmentInf(secondmentinf);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {secondmentinf.Count()} record (s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {secondmentinf.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {secondmentinf.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;
                case "CONTACTINF":
                    List<PAContactInf> contactinf = oApiJobPersonalData.GetAllContactInf(DefaultBeginDate, DefaultEndDate).Data;
                    if (contactinf != null && contactinf.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {contactinf.Count()} record(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllContactInf(contactinf);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {contactinf.Count()} record (s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {contactinf.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {contactinf.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;

                case "POSITION":
                    List<PAPosition> position = oApiJobPersonalData.GetAllPosition(DefaultBeginDate, DefaultEndDate).Data;
                    if (position != null && position.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {position.Count()} record(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllPosition(position);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                     //   EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {position.Count()} record (s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {position.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {position.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;

                case "ACTINGPOSITION":
                    List<PAActingPosition> actingposition = oApiJobPersonalData.GetAllActingPosition(DefaultBeginDate, DefaultEndDate).Data;
                    if (actingposition != null && actingposition.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {actingposition.Count()} record(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllActingPosition(actingposition);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {actingposition.Count()} record (s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {actingposition.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {actingposition.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;

                //ImportanceDateInf
                case "IMPORTANCEDATEINF":
                    List<PAImportanceDateInf> importancedateinf = oApiJobPersonalData.GetAllImportanceDateInf(DefaultBeginDate, DefaultEndDate).Data;
                    if (importancedateinf != null && importancedateinf.Count() > 0)
                    {
                        string ErrMsg = string.Empty;
                        Console.WriteLine($"Load {importancedateinf.Count()} record(s) completed");
                        ErrMsg = oApiJobPersonalData.SaveAllImportanceDateInf(importancedateinf);
                        oWorkAction = oApiJobPersonalData + " " + ErrMsg;
                      //  EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format($"Copy {importancedateinf.Count()} record (s)"), oWorkAction, true);
                        if (string.IsNullOrEmpty(ErrMsg))
                        {
                            Console.WriteLine($"Save {importancedateinf.Count()} record(s) completed.");
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {importancedateinf.Count()} record(s)"), "Success " + ConfigName);//Comment by Koissares 20201126
                        }
                        else
                        {
                            Console.WriteLine($"Found error {ErrMsg} please check!!");
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No record To Save");
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                    }
                    break;
            }
        }
    }
}
