#region Using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using ESS.DATA.FILE;
using ESS.DATA.INTERFACE;
using ESS.EMPLOYEE;
using ESS.PORTALENGINE;
using ESS.PORTALENGINE.DATACLASS;
using ESS.SECURITY;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.AUTHORIZATION;
using PTTCHEM.WORKFLOW;
using ESS.UTILITY.EXTENSION;
using ESS.DELEGATE.DATACLASS;
using ESS.EMPLOYEE.CONFIG.OM;
using System.IO;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using System.Xml;
using ESS.UTILITY.CONVERT;
using Newtonsoft.Json.Linq;

#endregion Using

namespace ESS.WORKFLOW.DB
{

    public class WorkflowServiceImpl : AbstractWorkflowService
    {

        #region Constructor
        public WorkflowServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }
        #endregion 
        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, WorkflowServiceImpl> Cache = new Dictionary<string, WorkflowServiceImpl>();
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }
        public static string ModuleID = "ESS.WORKFLOW.DB";
        #endregion MultiCompany  Framework
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string WORKFLOWSERVICE_CONNSTR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WORKFLOWSERVICE_CONNSTR");
            }
        }

        private string MongoDBConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MONGODB_CONNSTR");
            }
        }
        private string MongoDBDatabase
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MONGODB_DATABASENAME");
            }
        }


        //AddBy: Ratchatawan W. (2016-06-24)
        private string GetConnection(string CompanyCode)
        {
            return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
        }

        private string Language = "TH";
        private CultureInfo DefaultCultureInfo
        {
            get
            {
                return CultureInfo.GetCultureInfo(ShareDataManagement.LookupCache(CompanyCode, "ESS.GLOBAL","CULTUREINFO"));
            }
        }

        #region " Object Parse "

        #region " Flow "

        private Flow ParseToFlow(DataRow dr)
        {
            Flow Item = Flow.CreateInstance(CompanyCode);
            Item.ID = (int)dr["FlowID"];
            Item.Code = (string)dr["FlowCode"];
            Item.CreateItemID = (int)dr["CreateItem"];
            Item.ContinueItemID = (int)dr["ContinueItemID"];
            Item.EditItemID = (int)dr["EditItemID"];
            Item.CancelItemID = (int)dr["CancelItemID"];
            Item.OwnerViewItemID = (int)dr["OwnerViewItem"];
            if (!dr.IsNull("DataOwnerItem"))
            {
                Item.DataOwnerItemID = (int)dr["DataOwnerItem"];
            }
            return Item;
        }

        #endregion " Flow "

        #region " FlowItem "

        private FlowItem ParseToFlowItem(DataRow dr)
        {
            FlowItem Item = FlowItem.CreateInstance(CompanyCode);
            Item.FlowID = (int)dr["FlowID"];
            Item.ID = (int)dr["FlowItemID"];
            Item.Code = (string)dr["FlowItemCode"];
            Item.StateID = (int)dr["FlowItemStateID"];
            Item.IsCanOverwrite = (bool)dr["IsCanOverWrite"];
            Item.IsCheckFlowAfterOverwrite = (bool)dr["IsCheckFlowAfterOverwrite"];
            Item.IsCheckDataAfterOverwrite = (bool)dr["IsCheckDataAfterOverwrite"];
            if (dr.Table.Columns.Contains("GroupName") && !dr.IsNull("GroupName"))
            {
                Item.GroupName = (string)dr["GroupName"];
            }
            return Item;
        }

        #endregion " FlowItem "

        #region " State "

        private State ParseToState(DataRow dr)
        {
            State Item = State.CreateInstance(CompanyCode);
            Item.ID = (int)dr["StateID"];
            Item.Code = (string)dr["StateCode"];
            Item.IsEndPoint = (bool)dr["IsFinished"];
            Item.IsCanAction = (bool)dr["IsCanAction"];
            return Item;
        }

        #endregion " State "

        #region " FlowItemAction "

        private FlowItemAction ParseToFlowItemAction(DataRow dr)
        {
            int i = -1;
            FlowItemAction Item = FlowItemAction.CreateInstance(CompanyCode);
            Item.ID = (int)dr["FlowItemActionID"];
            Item.Code = (string)dr["FlowItemActionCode"];
            Item.FlowID = (int)dr["FlowID"];
            Item.FlowItemID = (int)dr["FlowItemID"];
            Item.NextItemCode = (string)dr["NextItemCode"];
            Item.NextItemID = (int)dr["NextItemID"];
            Item.ActionTypeID = (int)dr["ActionTypeID"];
            Item.AutomaticActionTypeID = (int)dr["AutomaticActionTypeID"];
            Item.IsCanOverwrite = (bool)dr["IsCanOverwrite"];
            Item.IsCheckDataAfterOverwrite = (bool)dr["IsCheckDataAfterOverwrite"];
            Item.IsCheckFlowAfterOverwrite = (bool)dr["IsCheckFlowAfterOverwrite"];
            Item.AutomaticStampRequestorTobeApprover = (bool)dr["AutomaticStampRequestorTobeApprover"];
            Item.StampRequestorTobeApprover = (bool)dr["StampRequestorTobeApprover"];

            i = -1;
            int.TryParse(dr["NotifyReceipients"].ToString(), out i);
            Item.NotifyReceipients = i;
            Item.NotifyReceipients_CCRequestor = (bool)dr["NotifyReceipients_CCRequestor"];
            Item.NotifyReceipients_CCCurrent = (bool)dr["NotifyReceipients_CCCurrent"];
            Item.NotifyReceipients_CCAllActionInPass = (bool)dr["NotifyReceipients_CCAllActionInPass"];
            Item.NotifyReceipients_CCSpecial = (bool)dr["NotifyReceipients_CCSpecial"];
            Item.NotifyReceipients_CCSpecialRole = dr["NotifyReceipients_CCSpecialRole"].ToString();

            i = -1;
            int.TryParse(dr["NotifyCurrent"].ToString(), out i);
            Item.NotifyCurrent = i;
            Item.NotifyCurrent_CCRequestor = (bool)dr["NotifyCurrent_CCRequestor"];
            Item.NotifyCurrent_CCAllActionInPass = (bool)dr["NotifyCurrent_CCAllActionInPass"];
            Item.NotifyCurrent_CCSpecial = (bool)dr["NotifyCurrent_CCSpecial"];
            Item.NotifyCurrent_CCSpecialRole = dr["NotifyCurrent_CCSpecialRole"].ToString();

            i = -1;
            int.TryParse(dr["NotifyRequestor"].ToString(), out i);
            Item.NotifyRequestor = i;
            Item.NotifyRequestor_CCCurrent = (bool)dr["NotifyRequestor_CCCurrent"];
            Item.NotifyRequestor_CCAllActionInPass = (bool)dr["NotifyRequestor_CCAllActionInPass"];
            Item.NotifyRequestor_CCSpecial = (bool)dr["NotifyRequestor_CCSpecial"];
            Item.NotifyRequestor_CCSpecialRole = dr["NotifyRequestor_CCSpecialRole"].ToString();

            i = -1;
            int.TryParse(dr["NotifySpecial"].ToString(), out i);
            Item.NotifySpecial = i;
            Item.NotifySpecial_CCRequestor = (bool)dr["NotifySpecial_CCRequestor"];
            Item.NotifySpecial_CCCurrent = (bool)dr["NotifySpecial_CCCurrent"];
            Item.NotifySpecial_CCAllActionInPass = (bool)dr["NotifySpecial_CCAllActionInPass"];
            Item.NotifySpecial_CCSpecial = (bool)dr["NotifySpecial_CCSpecial"];
            Item.NotifySpecial_CCSpecialRole = dr["NotifySpecial_CCSpecialRole"].ToString();

            i = -1;
            int.TryParse(dr["NotifyAllActionInPass"].ToString(), out i);
            Item.NotifyAllActionInPass = i;
            Item.NotifyAllActionInPass_CCRequestor = (bool)dr["NotifyAllActionInPass_CCRequestor"];
            Item.NotifyAllActionInPass_CCCurrent = (bool)dr["NotifyAllActionInPass_CCCurrent"];
            Item.NotifyAllActionInPass_CCSpecial = (bool)dr["NotifyAllActionInPass_CCSpecial"];
            Item.NotifyAllActionInPass_CCSpecialRole = dr["NotifyAllActionInPass_CCSpecialRole"].ToString();

            i = -1;
            int.TryParse(dr["NotifyAutomaticAction"].ToString(), out i);
            Item.NotifyAutomaticAction = i;
            Item.NotifyAutomaticAction_CCRequestor = (bool)dr["NotifyAutomaticAction_CCRequestor"];
            Item.NotifyAutomaticAction_CCCurrent = (bool)dr["NotifyAutomaticAction_CCCurrent"];
            Item.NotifyAutomaticAction_CCAllActionInPass = (bool)dr["NotifyAutomaticAction_CCAllActionInPass"];
            Item.NotifyAutomaticAction_CCSpecial = (bool)dr["NotifyAutomaticAction_CCSpecial"];
            Item.NotifyAutomaticAction_CCSpecialRole = dr["NotifyAutomaticAction_CCSpecialRole"].ToString();

            Item.SpecialRole = dr["SpecialRole"].ToString();

            i = -1;
            int.TryParse(dr["NotifyRequestorWithGotoNextItem"].ToString(), out i);
            Item.NotifyRequestorWithGotoNextItem = i;
            Item.NotifyRequestorWithGotoNextItem_CCCurrent = (bool)dr["NotifyRequestorWithGotoNextItem_CCCurrent"];
            Item.NotifyRequestorWithGotoNextItem_CCAllActionInPass = (bool)dr["NotifyRequestorWithGotoNextItem_CCAllActionInPass"];
            Item.NotifyRequestorWithGotoNextItem_CCSpecial = (bool)dr["NotifyRequestorWithGotoNextItem_CCSpecial"];
            Item.NotifyRequestorWithGotoNextItem_CCSpecialRole = dr["NotifyRequestorWithGotoNextItem_CCSpecialRole"].ToString();

            return Item;
        }

        #endregion " FlowItemAction "

        #region " ActionType "

        private ActionType ParseToActionType(DataRow dr)
        {
            ActionType Item = ActionType.CreateInstance(CompanyCode);
            Item.ID = (int)dr["ActionTypeID"];
            Item.Code = (string)dr["ActionTypeCode"];
            Item.IsMarkCompleted = (bool)dr["IsMarkCompleted"];
            Item.IsMarkCancelled = (bool)dr["IsMarkCancelled"];
            Item.IsMarkWaitForEdit = (bool)dr["IsMarkWaitForEdit"];
            Item.IsRaiseAutomaticAction = (bool)dr["IsRaiseAutomaticAction"];
            return Item;
        }

        #endregion " ActionType "

        #region " ActionTypeParam "

        private ActionTypeParam ParseToActionTypeParam(DataRow dr)
        {
            ActionTypeParam Item = new ActionTypeParam();
            Item.ID = (int)dr["ParamID"];
            Item.Code = (string)dr["ParamCode"];
            Item.Type = (ParameterType)Enum.Parse(typeof(ParameterType), (string)dr["ParamType"]);
            return Item;
        }

        #endregion " ActionTypeParam "

        #region " FlowParamter "

        private FlowParameter ParseToFlowParameter(DataRow dr)
        {
            FlowParameter Item = new FlowParameter();
            Item.ID = (int)dr["FlowParameterID"];
            Item.Code = (string)dr["FlowParameterCode"];
            Item.Type = (ParameterType)Enum.Parse(typeof(ParameterType), (string)dr["FlowParameterType"]);
            return Item;
        }

        #endregion " FlowParamter "

        #region " RequestType "

        private RequestType ParseToRequestType(DataRow dr)
        {
            RequestType Item = new RequestType();
            Item.ParseToObject(dr);

            return Item;
        }

        #endregion " RequestType "

        #region " RequestTypeSchema "

        private RequestTypeSchema ParseToRequestTypeSchema(DataRow dr)
        {
            RequestTypeSchema Item = new RequestTypeSchema();
            Item.ID = (int)dr["SchemaID"];
            Item.Code = (string)dr["SchemaCode"];
            Item.Type = (ParameterType)Enum.Parse(typeof(ParameterType), (string)dr["SchemaType"]);
            return Item;
        }

        #endregion " RequestTypeSchema "

        #region " RequestTypeKey "

        private RequestTypeKey ParseToRequestTypeKey(DataRow dr)
        {
            RequestTypeKey Item = new RequestTypeKey();
            Item.ID = (int)dr["KeyItemID"];
            Item.Code = (string)dr["KeyItemCode"];
            Item.Category = (RequestTypeKeyCategory)Enum.Parse(typeof(RequestTypeKeyCategory), (string)dr["KeyItemCategory"]);
            Item.Length = (int)dr["KeyItemLength"];
            return Item;
        }

        #endregion " RequestTypeKey "

        #region " RequestTypeSetting "

        private RequestTypeSetting ParseToRequestTypeSetting(DataRow dr)
        {
            RequestTypeSetting Item = RequestTypeSetting.CreateInstance(CompanyCode);
            Item.RequestTypeID = (int)dr["RequestTypeID"];
            Item.VersionID = (int)dr["VersionID"];
            Item.ID = (int)dr["KeyID"];
            Item.Code = (string)dr["KeyCode"];
            Item.FlowID = (int)dr["FlowID"];
            return Item;
        }


        #endregion " RequestTypeSetting "
        #region " RequestTypeParam "

        private RequestTypeParam ParseToRequestTypeParam(DataRow dr)
        {
            RequestTypeParam Item = new RequestTypeParam();
            Item.ID = (int)dr["ParamID"];
            Item.Mode = (ParameterMode)Enum.Parse(typeof(ParameterMode), (string)dr["ParamType"]);
            Item.Value = (string)dr["ParamValue"];
            return Item;
        }

        #endregion " RequestTypeParam "

        #region " RequestFlow "

        public RequestFlow ParseToRequestFlow(DataRow dr)
        {
            RequestFlow RF = new RequestFlow();
            if ((bool)dr["IsSystem"])
            {
                WorkflowManagement oManagement = WorkflowManagement.CreateInstance(this.CompanyCode);
                RF.ActionBy = oManagement.GetCommonText("SYSTEM", WorkflowPrinciple.Current.UserSetting.Language, "WORKFLOW");
            }
            else if ((bool)dr["IsExternalUser"])
            {
                RF.ActionBy = (string)dr["ActionByCode"] + " " + (string)dr["ActionBy"];
                RF.ActionByCode = (string)dr["ActionByCode"];
                RF.ActionByPosition = (string)dr["ActionByPosition"];
                RF.ActionByPositionName = (string)dr["ActionByPositionName"];
                RF.ActionByCompanyCode = (string)dr["ActionByCompanyCode"];
            }
            else
            {
                EmployeeData emp = new EmployeeData((string)dr["ActionByCode"], (string)dr["ActionByPosition"], (string)dr["ActionByCompanyCode"], (DateTime)dr["ActionDate"]);
                RF.ActionBy = emp.EmployeeID + " " + emp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                RF.ActionByCode = (string)dr["ActionByCode"];
                RF.ActionByPosition = emp.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                RF.ActionByPositionName = emp.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                RF.ActionByCompanyCode = (string)dr["ActionByCompanyCode"];

                EmployeeData emp2 = new EmployeeData((string)dr["ReceipientCode"], (string)dr["ReceipientPositon"], (string)dr["ReceipientCompanyCode"], (DateTime)dr["ActionDate"]);
                RF.Receipient = emp2.EmployeeID + " " + emp2.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                RF.ReceipientCode = (string)dr["ReceipientCode"];
                RF.ReceipientPosition = emp2.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                RF.ReceipientPositionName = emp2.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                RF.ReceipientCompanyCode = (string)dr["ReceipientCompanyCode"];
            }
            RF.ActionDate = (DateTime)dr["ActionDate"];
            RF.ActionCode = (string)dr["ActionCode"];
            RF.Comment = (string)dr["Comment"];
            RF.IsSystem = (bool)dr["IsSystem"];
            if (dr.Table.Columns.Contains("GroupName"))
            {
                if (!dr.IsNull("GroupName"))
                {
                    RF.GroupName = (string)dr["GroupName"];
                }
            }
            return RF;
        }

        #endregion " RequestFlow "

        #region " RequestDocument "

        private void ParseToRequestDocument(DataRow dr, RequestDocument Item)
        {
            Item.RequestID = (int)dr["RequestID"];
            Item.CreatedDate = (DateTime)dr["CreatedDate"];
            Item.CreatorNo = (string)dr["CreatorNo"];
            Item.CreatorPosition = (string)dr["CreatorPosition"];
            Item.CreatorCompanyCode = (string)dr["CreatorCompanyCode"];
            Item.CurrentFlowItemID = (int)dr["FlowItemID"];
            Item.CurrentItemID = (int)dr["ItemID"];
            Item.AuthorizeStep = (int)dr["AuthorizeStep"];
            Item.FlowID = (int)dr["FlowID"];
            Item.KeyMaster = (string)dr["KeyMaster"];
            Item.RequestNo = (string)dr["RequestNo"];
            Item.RequestorNo = (string)dr["RequestorNo"];
            Item.RequestorPosition = (string)dr["RequestorPosition"];
            Item.RequestorCompanyCode = (string)dr["RequestorCompanyCode"];
            Item.RequestTypeID = (int)dr["RequestTypeID"];
            Item.DocversionID = (int)dr["DocVersionID"];
            if (dr.IsNull("SubmitDate"))
            {
                Item.SubmittedDate = DateTime.MinValue;
            }
            else
            {
                Item.SubmittedDate = (DateTime)dr["SubmitDate"];
            }
            if (dr.IsNull("DocumentDate"))
            {
                Item.DocumentDate = DateTime.MinValue;
            }
            else
            {
                Item.DocumentDate = (DateTime)dr["DocumentDate"];
            }
            Item.IsCancelled = (bool)dr["IsCancelled"];
            Item.IsCompleted = (bool)dr["IsCompleted"];
            Item.IsWaitForEdit = (bool)dr["IsWaitForEdit"];
            if (dr.Table.Columns.Contains("IsOwnerView"))
            {
                Item.IsOwnerView = (bool)dr["IsOwnerView"];
            }
            if (dr.Table.Columns.Contains("LastActionDate"))
            {
                Item.LastActionDate = (DateTime)dr["LastActionDate"];
            }
            Item.FileSetID = (int)dr["FileSetID"];
            Item.AuthorizeDelegate = (string)dr["AuthorizeDelegate"];
            Item.CompanyCode = (string)dr["RequestorCompanyCode"];
            Item.Creator = new EmployeeData(Item.CreatorNo, Item.CreatorPosition, Item.CreatorCompanyCode, Item.DocumentDate);
            Item.Requestor = new EmployeeData(Item.RequestorNo, Item.RequestorPosition, Item.RequestorCompanyCode, Item.DocumentDate);
            //Item.Creator.CompanyCode = (string)dr["CreatorCompanyCode"];
            //Item.Requestor.CompanyCode = (string)dr["RequestorCompanyCode"];
            Item.ReferRequestNo = (string)dr["ReferRequestNo"];
           
        }

        #endregion " RequestDocument "

        #region " RequestApplication "

        private RequestApplication ParseToRequestApplication(DataRow dr)
        {
            RequestApplication Item = new RequestApplication();
            Item.ID = (int)dr["ApplicationID"];
            Item.Code = (string)dr["ApplicationCode"];
            Item.ShowCounter = (bool)dr["ShowCounter"];
            Item.IconClass = (string)dr["IconClass"];
            return Item;
        }

        #endregion " RequestApplication "

        #region " RequestApplication "

        private ApplicationSubject ParseToApplicationSubject(DataRow dr)
        {
            ApplicationSubject Item = new ApplicationSubject();
            Item.ID = (int)dr["SubjectID"];
            Item.Code = (string)dr["SubjectCode"];
            Item.CompanyCode = CompanyCode;
            Item.IconClass = (string)dr["IconClass"];
            return Item;
        }

        #endregion " RequestApplication "

        #region " Receipient "

        private Receipient ParseToReceipient(DataRow dr)
        {
            Receipient Item = new Receipient();// Receipient.CreateInstance(CompanyCode);
            Item.Code = (string)dr["ReceipientCode"];
            Item.CompanyCode = (string)dr["CompanyCode"];
            Item.Type = (ReceipientType)dr["ReceipientType"].ToString()[0];
            return Item;
        }

        #endregion " Receipient "

        #region " RequestBox "

        private RequestBox ParseToRequestBox(DataRow dr)
        {
            RequestBox Item = new RequestBox();
            Item.ID = (int)dr["BoxID"];
            Item.Code = (string)dr["BoxCode"];
            Item.IsCanDelegate = (bool)dr["IsCanDelegate"];
            Item.IsOwnerView = (bool)dr["IsOwnerView"];
            Item.IsDataOwnerView = (bool)dr["IsDataOwnerView"];
            Item.CompanyCode = CompanyCode;
            return Item;
        }

        #endregion " RequestBox "

        #region " AuthorizationModel "

        private AuthorizationModel ParseToAuthorizationModel(DataRow dr)
        {
            AuthorizationModel oModel = new AuthorizationModel();//WorkflowManagement.CreateInstance(CompanyCode);
            oModel.ParseToObject(dr);
            return oModel;
        }

        #endregion " AuthorizationModel "

        #endregion " Object Parse "

        #region " Override Methods "

        #region " Flow "

        #region " GetAllFlows "

        public override List<Flow> GetAllFlows()
        {
            if (!WorkflowPrinciple.Current.IsInRole("APPDEVELOPER"))
            {
                throw new NoAuthorizeException("GetAllFlows");
            }
            List<Flow> oReturn = new List<Flow>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from Flow", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOW");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToFlow(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllFlows "

        #region " GetFlow "

        public override Flow GetFlow(int FlowID)
        {
            Flow oReturn = new Flow();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("sp_FlowGetByID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_FlowID", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOW");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToFlow(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetFlow "

        #region " GetFlowItems "

        public override List<FlowItem> GetFlowItems(int FlowID)
        {
            if (!WorkflowPrinciple.Current.IsInRole("APPDEVELOPER"))
            {
                throw new NoAuthorizeException("GetFlowItems");
            }
            List<FlowItem> oReturn = new List<FlowItem>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from FlowItem where FlowID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOWITEM");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToFlowItem(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetFlowItems "

        #region " GetAllStates "

        public override List<State> GetAllStates()
        {
            if (!WorkflowPrinciple.Current.IsInRole("APPDEVELOPER"))
            {
                throw new NoAuthorizeException("GetAllStates");
            }
            List<State> oReturn = new List<State>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from State", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("STATE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToState(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllStates "

        #region " GetState "

        public override State GetState(int StateID)
        {
            State oReturn = new State();// State.CreateInstance(CompanyCode);
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from State Where StateID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = StateID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("STATE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToState(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetState "

        #region " GetStateResponse "

        public override ReceipientList GetStateResponse(int StateID)
        {
            ReceipientList oReturn = new ReceipientList();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select ResponseType As ReceipientType, ResponseCode As ReceipientCode from StateResponse where StateID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = StateID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("STATERESPONSE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToReceipient(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetStateResponse "

        #region " GetAllPossibleAction "

        public override List<FlowItemAction> GetAllPossibleAction(int FlowID, int FlowItemID)
        {
            List<FlowItemAction> oReturn = new List<FlowItemAction>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);

            SqlCommand oCommand = new SqlCommand("sp_GetFlowItemAction", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@ID", FlowID);
            oCommand.Parameters.AddWithValue("@ItemID", FlowItemID);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOWITEMACTION");
           
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToFlowItemAction(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllPossibleAction "

        #region " GetFlowItem "

        public override FlowItem GetFlowItem(int FlowID, int FlowItemID)
        {
            FlowItem oReturn = FlowItem.CreateInstance(CompanyCode);
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from FlowItem where FlowID = @ID and FlowItemID = @ItemID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ItemID", SqlDbType.Int);
            oParam.Value = FlowItemID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOWITEM");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToFlowItem(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetFlowItem "

        #region " GetActionType "

        public override ActionType GetActionType(int ActionTypeID)
        {
            ActionType oReturn = ActionType.CreateInstance(CompanyCode);
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from ActionType Where ActionTypeID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ActionTypeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ACTIONTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToActionType(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetActionType "

        #region " GetAllActionTypes "

        public override List<ActionType> GetAllActionTypes()
        {
            List<ActionType> oReturn = new List<ActionType>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from ActionType", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ACTIONTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToActionType(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllActionTypes "

        #region " GetActionTypeParameters "

        public override List<ActionTypeParam> GetActionTypeParameters(int ActionTypeID)
        {
            List<ActionTypeParam> oReturn = new List<ActionTypeParam>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from ActionTypeParam where ActionTypeID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ActionTypeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ACTIONTYPEPARAM");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToActionTypeParam(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetActionTypeParameters "

        #region " GetActionTypeReceipients "

        public override ReceipientList GetActionTypeReceipients(int ActionTypeID)
        {
            ReceipientList oReturn = new ReceipientList();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from ActionTypeReceipient where ActionTypeID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ActionTypeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ACTIONTYPERECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToReceipient(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetActionTypeReceipients "

        #region " GetFlowParameter "

        public override List<FlowParameter> GetFlowParameter(int FlowID)
        {
            List<FlowParameter> oReturn = new List<FlowParameter>();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select * from FlowParameter where FlowID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOWPARAMETER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToFlowParameter(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetFlowParameter "

        #region " GetMappingParameter "

        public override FlowParameter GetMappingParameter(int FlowID, int FlowItemID, int FlowItemActionID, int ParamID, string NextItemCode)
        {
            FlowParameter oReturn = new FlowParameter();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select A.* from FlowParameter A inner join FlowItemActionParam B on B.FlowID = A.FlowID AND B.NextItemCode=@NextItemCode And B.FlowParamID = A.FlowParameterID where B.FlowID = @ID And B.FlowItemID = @ItemID And B.FlowItemActionID = @ActionID And B.ParamID = @ParamID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ItemID", SqlDbType.Int);
            oParam.Value = FlowItemID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ActionID", SqlDbType.Int);
            oParam.Value = FlowItemActionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ParamID", SqlDbType.Int);
            oParam.Value = ParamID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@NextItemCode", SqlDbType.VarChar);
            oParam.Value = NextItemCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOWPARAMETER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToFlowParameter(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetMappingParameter "

        #region " GetMappingAutomaticParameter "

        public override FlowParameter GetMappingAutomaticParameter(int FlowID, int FlowItemID, int FlowItemActionID, int ParamID, string NextItemCode)
        {
            FlowParameter oReturn = new FlowParameter();
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select A.* from FlowParameter A inner join FlowItemAutomaticActionParam B on B.FlowID = A.FlowID AND B.NextItemCode=@NextItemCode And B.FlowParamID = A.FlowParameterID where B.FlowID = @ID And B.FlowItemID = @ItemID And B.FlowItemActionID = @ActionID And B.ParamID = @ParamID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ItemID", SqlDbType.Int);
            oParam.Value = FlowItemID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ActionID", SqlDbType.Int);
            oParam.Value = FlowItemActionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ParamID", SqlDbType.Int);
            oParam.Value = ParamID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@NextItemCode", SqlDbType.VarChar);
            oParam.Value = NextItemCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FLOWPARAMETER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToFlowParameter(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetMappingAutomaticParameter "

        #endregion " Flow "

        #region " RequestType "

        #region " GetAllRequestTypes "

        public override List<RequestType> GetAllRequestTypes()
        {
            if (!WorkflowPrinciple.Current.IsInRole("APPDEVELOPER"))
            {
                throw new NoAuthorizeException("GetAllRequestTypes");
            }
            List<RequestType> oReturn = new List<RequestType>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select *,dbo.GetSummary(requesttypeid,versionid,@lang) As SummaryText from V_RequestTypeCurrentVersion", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@lang", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.Current.UserSetting.Language;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestType(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllRequestTypes "

        #region " GetRequestTypeSchemas "

        public override List<RequestTypeSchema> GetRequestTypeSchemas(int RequestTypeID, int VersionID)
        {
            List<RequestTypeSchema> oReturn = new List<RequestTypeSchema>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from RequestSchema where RequestTypeID = @ID and VersionID = @VersionID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTSCHEMA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestTypeSchema(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeSchemas "

        #region " GetRequestTypeKeys "

        public override List<RequestTypeKey> GetRequestTypeKeys(int RequestTypeID, int VersionID)
        {
            List<RequestTypeKey> oReturn = new List<RequestTypeKey>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from RequestTypeKey where RequestTypeID = @ID and VersionID = @VersionID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPEKEY");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestTypeKey(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeKeys "

        #region " GetRequestTypeSettings "

        public override List<RequestTypeSetting> GetRequestTypeSettings(int RequestTypeID, int VersionID)
        {
            if (!WorkflowPrinciple.Current.IsInRole("APPDEVELOPER"))
            {
                throw new NoAuthorizeException("GetRequestTypeSettings");
            }
            List<RequestTypeSetting> oReturn = new List<RequestTypeSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from RequestTypeFlowSetting where RequestTypeID = @ID and VersionID = @VersionID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPESETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestTypeSetting(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeSettings "

        #region " GetRequestTypeSetting "

        public override RequestTypeSetting GetRequestTypeSetting(int RequestTypeID, int VersionID, string flowKey)
        {
            RequestTypeSetting oReturn = RequestTypeSetting.CreateInstance(CompanyCode);
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from LoadRequestTypeSetting(@ID,@VersionID,@flowKey)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@flowKey", SqlDbType.VarChar);
            oParam.Value = flowKey;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPESETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToRequestTypeSetting(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        public override RequestTypeSetting GetRequestTypeSettingByFlowID(int RequestTypeID, int VersionID, int FlowID)
        {
            RequestTypeSetting oReturn = RequestTypeSetting.CreateInstance(CompanyCode);
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from [LoadRequestTypeSettingByFlowID](@ID,@VersionID,@flowid)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@flowid", SqlDbType.Int);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPESETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToRequestTypeSetting(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeSetting "

        #region " GetRequestTypeParameters "

        public override List<RequestTypeParam> GetRequestTypeParameters(int RequestTypeID, int VersionID, int KeyID)
        {
            List<RequestTypeParam> oReturn = new List<RequestTypeParam>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from RequestTypeParam where RequestTypeID = @ID and VersionID = @VersionID and KeyID = @KeyID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@KeyID", SqlDbType.Int);
            oParam.Value = KeyID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPEPARAM");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestTypeParam(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeParameters "

        #region " GetRequestType "

        #region " GetRequestType(int) "

        public override RequestType GetRequestType(int RequestTypeID)
        {
            RequestType oReturn = new RequestType();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select *,dbo.GetSummary(requesttypeid,versionid,@lang) As SummaryText from V_RequestTypeCurrentVersion where RequestTypeID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@lang", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.Current.UserSetting.Language;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToRequestType(dr);
                oReturn.CompanyCode = this.CompanyCode;
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestType(int) "

        #region " GetRequestType(int,DateTime) "

        public override RequestType GetRequestType(int RequestTypeID, DateTime CheckDate,string LanguageCode)
        {
            RequestType oReturn = new RequestType();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select *,dbo.GetSummary(requesttypeid,versionid,@lang) As SummaryText,dbo.GetSummary(requesttypeid,versionid,'TH') As SummaryTextTH,dbo.GetSummary(requesttypeid,versionid,'EN') As SummaryTextEN from V_RequestType where RequestTypeID = @ID and @CheckDate between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@lang", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToRequestType(dr);
                oReturn.CompanyCode = this.CompanyCode;
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestType(int,DateTime) "

        #endregion " GetRequestType "

        #region " CalculateFlow "

        public override int CalculateFlow(int RequestTypeID, int VersionID, string FlowKey)
        {
            int returnValue;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select dbo.CalculateFlow(@RequestTypeID,@VersionID,@FlowKey)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestTypeID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@VersionID", SqlDbType.Int);
            oParam.Value = VersionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@FlowKey", SqlDbType.VarChar);
            oParam.Value = FlowKey;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            returnValue = (int)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return returnValue;
        }

        #endregion " CalculateFlow "

        #region " GetRequestTypeOwner "

        public override ReceipientList GetRequestTypeOwner(int RequestTypeID)
        {
            ReceipientList oReturn = new ReceipientList();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select OwnerType As ReceipientType, OwnerCode As ReceipientCode, @CompanyCode as CompanyCode from RequestTypeOwner Where RequestTypeID = @RequestTypeID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestTypeID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CompanyCode", SqlDbType.VarChar);
            oParam.Value = CompanyCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToReceipient(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeOwner "

        #region " GetRequestTypeResponse "

        public override List<UserRoleResponseSetting> GetRequestTypeResponse(int RequestTypeID)
        {
            List<UserRoleResponseSetting> oReturn = new List<UserRoleResponseSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from RequestTypeRoleResponse where RequestTypeID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RESPONSE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                UserRoleResponseSetting item = new UserRoleResponseSetting();
                item.UserRole = (string)dr["UserRole"];
                item.IncludeSub = (bool)dr["IncludeSub"];
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestTypeResponse "

        #region " GetRequestTypeFileSet "

        public override FileAttachmentSet GetRequestTypeFileSet(string employeeID, int requestTypeID, string requestSubType)
        {
            FileAttachmentSet oReturn = FileAttachmentSet.CreateFileSet();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select FileSetID from RequestTypeFileSet where EmployeeID = @EmpID and RequestTypeID = @RequestTypeID and RequestSubType = @ReqSubType", oConnection);
            SqlParameter oParam;

            try
            {
                oConnection.Open();

                oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
                oParam.Value = employeeID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@RequestTypeID", SqlDbType.Int);
                oParam.Value = requestTypeID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@ReqSubType", SqlDbType.VarChar);
                oParam.Value = requestSubType;
                oCommand.Parameters.Add(oParam);

                SqlDataReader oReader = oCommand.ExecuteReader();
                while (oReader.Read())
                {
                    int fileSetID = oReader.GetInt32(0);
                    oReturn = LoadFileAttachment(fileSetID);
                }

                oReader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Load error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion " GetRequestTypeFileSet "

        #endregion " RequestType "

        #region " Request "

        #region " LoadInData "
        public override DataTable LoadInfoData(string RequestNo, int RequestID, int RequestTypeID, int RequestTypeVersionID, int DocversionID)
        {
            DataTable oReturn = new DataTable();
            //Get data from mongo db
            MongoClient client = new MongoClient(MongoDBConnStr);
            IMongoDatabase database = client.GetDatabase(this.MongoDBDatabase);
            var collectionData = database.GetCollection<BsonDocument>(string.Format("Data.{0}.V{1}",RequestTypeID.ToString(), RequestTypeVersionID.ToString()));
            var filter = Builders<BsonDocument>.Filter.Eq("RequestID", RequestID) & Builders<BsonDocument>.Filter.Eq("RequestNo", RequestNo) & Builders<BsonDocument>.Filter.Eq("DocversionID", DocversionID);
            BsonDocument doc = collectionData.Find(filter).FirstOrDefault();
            string strData = doc.GetElement("Data").Value.ToString();
            if (!string.IsNullOrEmpty(strData))
                oReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(strData);
            oReturn.TableName = "REQUESTINFO";
            return oReturn;
        }

        private string ParseBSonToJSon(BsonDocument oBsonDocument)
        {
            var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict }; // key part
            return oBsonDocument.ToJson(jsonWriterSettings);
        }

        private BsonDocument JSonToBSon(string jSonString)
        {
            return MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jSonString);
        }

        #endregion " LoadInData "

        #region " GetRequestText "

        public override string GetRequestText(string Category, string Language, string Code)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetRequestText(@Category,@Language) where TextCode = @TextCode", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@Category", SqlDbType.VarChar);
            oParam.Value = Category;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TextCode", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TEXTTABLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = (string)dr["TextDescription"];
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetRequestText "

        #region " GetSpecialReceipient "

        public override ReceipientList GetSpecialReceipient(string EmployeeID, string Position, string Code, DateTime CheckDate)
        {
            ReceipientList oReturn = new ReceipientList();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetSpecialReceipient(@EmployeeID, @Position, @Code, @CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Position", SqlDbType.VarChar);
            oParam.Value = Position;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Code", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToReceipient(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetSpecialReceipient "

        #region " GetLastProcess "

        public override RequestFlow GetLastProcess(string RequestNo)
        {
            RequestFlow oReturn = new RequestFlow();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetLastProcess(@RequestNo)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTFLOW");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToRequestFlow(dr);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetLastProcess "

        #region " GetPassedProcess "

        public override List<RequestFlow> GetPassedProcess(string RequestNo)
        {
            List<RequestFlow> oReturn = new List<RequestFlow>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetPassedProcess(@RequestNo)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTFLOW");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            RequestFlow oldItem = null;
            bool isHaveAnother = false;
            foreach (DataRow dr in oTable.Rows)
            {
                RequestFlow item = this.ParseToRequestFlow(dr);
                //ModifiedBy: Ratchatawan W. (2017-05-14)
                ////ModifiedBy: Ratchatawan W. (2012-01-25)
                ////if (oldItem == null || ((item.ActionBy != oldItem.ActionBy || item.ActionCode != oldItem.ActionCode) && !(item.ActionCode == "EDIT" && oldItem.ActionCode == "CREATE")))
                //if (oldItem == null || !((item.ActionBy == oldItem.ActionBy) && (item.ActionCode == "EDIT" && oldItem.ActionCode == "CREATE")))
                //{
                    oReturn.Add(item);
                //    oldItem = item;
                //}
                //if (!isHaveAnother && item.ActionCode != "CREATE" && item.ActionCode != "EDIT")
                //{
                //    isHaveAnother = true;
                //}
            }
            oTable.Dispose();
            if (isHaveAnother)
            {
                oReturn.RemoveAt(0);
            }
            return oReturn;
        }

        #endregion " GetPassedProcess "

        #region " UpdateDocument "

        public override void UpdateDocument(RequestDocument Document, FlowItemAction action)
        {
            UpdateDocument(Document, action, null, null);
        }

        #endregion " UpdateDocument "

        #region " UpdateDocument "
        string _languageCode = string.Empty;
        private void UpdateDocument(RequestDocument Document, FlowItemAction action, SqlConnection Connection, SqlTransaction tx)
        {
            DataTable oDelegate = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetDelegationInfo(Document.RequestID);

            string ConnectionString = GetConnection(Document.RequestorCompanyCode);
            int __requestID = -1;
            int __requestFlowItemID = -1;
            int __currentDocVersionID = -1;
            bool __isNew = false;
            SqlConnection oConnection;
            if (Connection == null)
            {
                oConnection = new SqlConnection(ConnectionString);
                oConnection.Open();
            }
            else
            {
                oConnection = Connection;
            }

            #region " set environment "

            #region " RequestDocument "

            SqlCommand oCommand = new SqlCommand("select * from RequestDocument where RequestNo = @RequestNo", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = Document.RequestNo;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            SqlTransaction __tx;
            if (tx == null)
            {
                __tx = oConnection.BeginTransaction();
            }
            else
            {
                __tx = tx;
            }
            oCommand.Transaction = __tx;
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            #endregion " RequestDocument "

            #endregion " set environment "

            try
            {
                #region " RequestDocument "

                DataRow oDataRow;
                if (oTable.Rows.Count > 0)
                {
                    oDataRow = oTable.Rows[0];
                    if ((string)oDataRow["KeyMaster"] != Document.KeyMaster)
                    {
                        throw new Exception("KeyMaster changed. Maybe have some people update before you.");
                    }
                    __requestID = (int)oDataRow["RequestID"];
                }
                else
                {
                    __isNew = true;
                    oDataRow = oTable.NewRow();
                    __requestID = -1;
                    oDataRow["RequestID"] = -1;
                    oDataRow["CreatedDate"] = Document.CreatedDate;
                    oDataRow["CreatorNo"] = Document.CreatorNo;
                    oDataRow["CreatorPosition"] = Document.CreatorPosition;
                    oDataRow["CreatorCompanyCode"] = Document.Creator.CompanyCode;
                }
                oDataRow["RequestNo"] = Document.RequestNo;
                oDataRow["RequestTypeID"] = Document.RequestTypeID;
                oDataRow["RequestorNo"] = Document.RequestorNo;
                oDataRow["RequestorPosition"] = Document.RequestorPosition;
                oDataRow["RequestorCompanyCode"] = Document.Requestor.CompanyCode;
                oDataRow["ReferRequestNo"] = Document.ReferRequestNo;
                if (Document.SubmittedDate == DateTime.MinValue)
                {
                    oDataRow["SubmitDate"] = DBNull.Value;
                }
                else
                {
                    oDataRow["SubmitDate"] = Document.SubmittedDate;
                }
                if (Document.DocumentDate == DateTime.MinValue)
                {
                    oDataRow["DocumentDate"] = DBNull.Value;
                }
                else
                {
                    oDataRow["DocumentDate"] = Document.DocumentDate;
                }
                oDataRow["FlowID"] = Document.FlowID;
                oDataRow["FlowItemID"] = Document.CurrentFlowItemID;
                oDataRow["FlowItemStateID"] = Document.CurrentFlowItemStateID;
                oDataRow["FlowItemCode"] = Document.CurrentFlowItem.Code;
                oDataRow["ItemID"] = Document.CurrentItemID;
                oDataRow["AuthorizeStep"] = Document.AuthorizeStep;
                oDataRow["IsCompleted"] = Document.IsCompleted;
                oDataRow["IsCancelled"] = Document.IsCancelled;
                oDataRow["IsWaitForEdit"] = Document.IsWaitForEdit;
                Random oRandom = new Random();
                Document.KeyMaster = oRandom.Next(9999).ToString("0000");
                oDataRow["KeyMaster"] = Document.KeyMaster;
                oDataRow["FileSetID"] = Document.FileSetID;
                oDataRow["AuthorizeDelegate"] = Document.AuthorizeDelegate;


                if (oDataRow.RowState == DataRowState.Detached)
                {
                    oTable.Rows.Add(oDataRow);
                }
                //Update Document
                oAdapter.Update(oTable);

                if (__requestID == -1)
                {
                    oCommand.CommandText = "select IsNull(Max(RequestID),0) + 1 From RequestDocument";
                    __requestID = (int)oCommand.ExecuteScalar();
                    oDataRow["RequestID"] = __requestID;
                    oAdapter.Update(oTable);
                }

                #endregion " RequestDocument "

                #region " FlowItemAction "


                __currentDocVersionID = Document.DocversionID;
                oCommand.Parameters.Clear();
                oCommand.CommandText = "select * from RequestFlow where RequestID = @RequestID";
                oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                oParam.Value = __requestID;
                oCommand.Parameters.Add(oParam);
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oTable = new DataTable("REQUESTFLOW");
                oAdapter.FillSchema(oTable, SchemaType.Source);

                oDataRow = oTable.NewRow();
                oDataRow["RequestID"] = __requestID;
                oDataRow["ItemID"] = -1;
                oDataRow["ReceipientCode"] = WorkflowPrinciple.CurrentIdentity.EmployeeID;
                oDataRow["ReceipientPositon"] = WorkflowPrinciple.CurrentIdentity.CurrentPosition;
                oDataRow["ReceipientCompanyCode"] = WorkflowPrinciple.CurrentIdentity.CompanyCode;
                #region Get real receipient (If this request has been actioned by Delegator)
                if (oDelegate.Rows.Count > 0)
                {
                    foreach (DataRow dr in oDelegate.Rows)
                    {
                        oDataRow["ReceipientCode"] = dr["EmployeeID"].ToString();
                        oDataRow["ReceipientPositon"] = dr["PositionID"].ToString();
                        oDataRow["ReceipientCompanyCode"] = dr["CompanyCode"].ToString();
                    }
                }
                #endregion
                oDataRow["ActionBy"] = WorkflowPrinciple.CurrentIdentity.EmployeeID;
                oDataRow["ActionByPosition"] = WorkflowPrinciple.CurrentIdentity.CurrentPosition;
                oDataRow["ActionByCompanyCode"] = WorkflowPrinciple.CurrentIdentity.CompanyCode;
                oDataRow["IsSystem"] = WorkflowPrinciple.Current.UserSetting.Employee.IsSystem;
                oDataRow["IsExternalUser"] = WorkflowPrinciple.Current.UserSetting.Employee.IsExternalUser;
                if (action == null)
                {
                    oDataRow["ActionCode"] = __isNew ? "CREATE" : "EDIT";
                    oDataRow["FlowID"] = Document.FlowID;
                    FlowItem oFlowItem = GetFlowItem(Document.FlowID, Document.CurrentFlowItemID);
                    oDataRow["FlowItemID"] = Document.CurrentFlowItemID;
                    oDataRow["FlowItemStateID"] = Document.CurrentFlowItemStateID;
                    oDataRow["FlowItemCode"] = Document.CurrentFlowItem.Code;
                }
                else if (action is FlowItemExceptionAction)
                {
                    oDataRow["ActionCode"] = "EXCEPTION";
                    oDataRow["ActionBy"] = "#SYSTEM";
                }
                else
                {
                    oDataRow["ActionCode"] = action.Code;
                    oDataRow["FlowID"] = action.FlowID;
                    FlowItem oFlowItem = GetFlowItem(action.FlowID, action.FlowItemID);
                    oDataRow["FlowItemID"] = oFlowItem.FlowItemID;
                    oDataRow["FlowItemStateID"] = oFlowItem.StateID;
                    oDataRow["FlowItemCode"] = oFlowItem.FlowItemCode;
                }
                oDataRow["ActionDate"] = DateTime.Now;
                oDataRow["DocVersionID"] = __currentDocVersionID;
                if (Document.Comment == null)
                {
                    oDataRow["Comment"] = "";
                }
                else
                {
                    oDataRow["Comment"] = Document.Comment;
                }
                if (Document.Comment2 == null)
                {
                    oDataRow["Comment2"] = "";
                }
                else
                {
                    oDataRow["Comment2"] = Document.Comment2;
                }

                if (oDataRow.Table.Columns.Contains("GroupName"))
                {
                    oDataRow["GroupName"] = Document.GroupName;
                }
                oTable.Rows.Add(oDataRow);
                oAdapter.Update(oTable);
                oCommand.CommandText = "select IsNull(Max(ItemID),0) + 1 From RequestFlow Where RequestID = @RequestID ";
                __requestFlowItemID = (int)oCommand.ExecuteScalar();
                Document.CurrentItemID = __requestFlowItemID;
                oDataRow["ItemID"] = __requestFlowItemID;
                oAdapter.Update(oTable);

                //AddBy: Ratchatawan W. (2017-09-07) ��������ʴ������ż��͹��ѵԷ���繷�ҹ���ǡѺ����͹��ѵ�
                if (action != null)
                {
                    if (action.StampRequestorTobeApprover)
                    {
                        DataRow oDataRow2 = oTable.NewRow();
                        oDataRow2.ItemArray = oTable.Rows[0].ItemArray.Clone() as object[];
                        /* START Fix Bug092 : ������վ�ѡ�ҹ�ҷӡ�����ҧ�͡���᷹ Requestor �������ö͹��ѵԵ���ͧ�� �к� Stamp ���ͧ͢������ҧ᷹�繤� APPROVE �͡��� ��觷��١��ͧ��ͧ�繪��ͧ͢ Requestor �� �Ţ� �����ҧ �͡���᷹��� */
                        oDataRow2["ReceipientCode"] = Document.Requestor.EmployeeID;
                        oDataRow2["ReceipientPositon"] = Document.Requestor.PositionID;
                        oDataRow2["ReceipientCompanyCode"] = Document.Requestor.CompanyCode;
                        oDataRow2["ActionBy"] = Document.Requestor.EmployeeID;
                        oDataRow2["ActionByPosition"] = Document.Requestor.PositionID;
                        oDataRow2["ActionByCompanyCode"] = Document.Requestor.CompanyCode;
                        /* END */
                        oDataRow2["ActionCode"] = "APPROVE";
                        oDataRow2["ActionDate"] = DateTime.Now.AddSeconds(1);
                        oDataRow2["ItemID"] = -1;
                        oTable.Rows.Clear();
                        oTable.Rows.Add(oDataRow2);
                        oAdapter.Update(oTable);

                        oCommand.CommandText = "select IsNull(Max(ItemID),0) + 1 From RequestFlow Where RequestID = @RequestID ";
                        __requestFlowItemID = (int)oCommand.ExecuteScalar();
                        Document.CurrentItemID = __requestFlowItemID;
                        oDataRow2["ItemID"] = __requestFlowItemID;
                        oAdapter.Update(oTable);
                    }
                }

                //AddBy: Ratchatawan W. (2017-09-07) ��������ʴ������ż��͹��ѵԷ���繷�ҹ���ǡѺ����͹��ѵ�
                if (action != null)
                {
                    if (action.IsGotoNextItem && action.AutomaticStampRequestorTobeApprover)
                    {
                        DataRow oDataRow2 = oTable.NewRow();
                        oDataRow2.ItemArray = oTable.Rows[0].ItemArray.Clone() as object[];
                        oDataRow2["ReceipientCode"] = Document.Requestor.EmployeeID;
                        oDataRow2["ReceipientPositon"] = Document.Requestor.PositionID;
                        oDataRow2["ReceipientCompanyCode"] = Document.Requestor.CompanyCode;
                        oDataRow2["ActionDate"] = DateTime.Now.AddSeconds(1);
                        oDataRow2["ActionBy"] = Document.Requestor.EmployeeID;
                        oDataRow2["ActionByPosition"] = Document.Requestor.PositionID;
                        oDataRow2["ActionByCompanyCode"] = Document.Requestor.CompanyCode;
                        oDataRow2["ActionCode"] = "APPROVE";
                        oDataRow2["ItemID"] = -1;
                        oTable.Rows.Clear();
                        oTable.Rows.Add(oDataRow2);
                        oAdapter.Update(oTable);

                        oCommand.CommandText = "select IsNull(Max(ItemID),0) + 1 From RequestFlow Where RequestID = @RequestID ";
                        __requestFlowItemID = (int)oCommand.ExecuteScalar();
                        Document.CurrentItemID = __requestFlowItemID;
                        oDataRow2["ItemID"] = __requestFlowItemID;
                        oAdapter.Update(oTable);
                    }
                }

                oCommand.Parameters.Clear();
                oCommand.CommandText = "Update RequestDocument Set ItemID = @ID where RequestID = @RequestID";
                oParam = new SqlParameter("@ID", SqlDbType.Int);
                oParam.Value = __requestFlowItemID;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                oParam.Value = __requestID;
                oCommand.Parameters.Add(oParam);

                oCommand.ExecuteNonQuery();

                #endregion " FlowItemAction "

                #region " Receipients "

                oCommand.Parameters.Clear();
                oCommand.CommandText = "select * from RequestFlowReceipient where RequestID = @RequestID and ItemID = @ItemID";
                oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                oParam.Value = __requestID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@ItemID", SqlDbType.Int);
                oParam.Value = __requestFlowItemID;
                oCommand.Parameters.Add(oParam);

                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oTable = new DataTable("REQUESTFLOW");
                oAdapter.FillSchema(oTable, SchemaType.Source);

                for (int index = 0; index < Document.Receipients.Count; index++)
                {
                    Receipient RCP = Document.Receipients[index];
                    oDataRow = oTable.NewRow();
                    oDataRow["RequestID"] = __requestID;
                    oDataRow["ItemID"] = __requestFlowItemID;
                    oDataRow["ReceipientID"] = index;
                    oDataRow["ReceipientType"] = ((char)RCP.Type).ToString();
                    oDataRow["ReceipientCode"] = RCP.Code;
                    oDataRow["CompanyCode"] = RCP.CompanyCode;
                    oTable.Rows.Add(oDataRow);
                }

                oAdapter.Update(oTable);

                #endregion " Receipients "

                #region " RequestFlowMultipleReceipient "
                if (Document.RequestFlowMultipleReceipientList.Count > 0)
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "delete from RequestFlowMultipleReceipient where RequestID = @RequestID";
                    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                    oParam.Value = __requestID;
                    oCommand.Parameters.Add(oParam);
                    oCommand.ExecuteNonQuery();

                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "select * from RequestFlowMultipleReceipient where RequestID = @RequestID";
                    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                    oParam.Value = __requestID;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oCB = new SqlCommandBuilder(oAdapter);
                    oTable = new DataTable("REQUESTFLOW");
                    oAdapter.FillSchema(oTable, SchemaType.Source);


                    for (int index = 0; index < Document.RequestFlowMultipleReceipientList.Count; index++)
                    {
                        RequestFlowMultipleReceipient RCPM = Document.RequestFlowMultipleReceipientList[index];
                        oDataRow = oTable.NewRow();
                        oDataRow["RequestID"] = __requestID;
                        oDataRow["ApproverID"] = RCPM.ApproverID;
                        oDataRow["ApproverCompanyCode"] = RCPM.ApproverCompanyCode;
                        oDataRow["ActionCode"] = RCPM.ActionCode;
                        if (RCPM.ActionDate > DateTime.MinValue)
                            oDataRow["ActionDate"] = RCPM.ActionDate;
                        oDataRow["ActionBy"] = RCPM.ActionBy;
                        oDataRow["ActionByPosition"] = RCPM.ActionByPosition;
                        oDataRow["ActionByCompanyCode"] = RCPM.ActionByCompanyCode;
                        oDataRow["Comment"] = RCPM.Comment;
                        oDataRow["ApproverGroup"] = RCPM.ApproverGroup;
                        oTable.Rows.Add(oDataRow);
                    }

                    oAdapter.Update(oTable);
                }
                else
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "delete from RequestFlowMultipleReceipient where RequestID = @RequestID";
                    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                    oParam.Value = __requestID;
                    oCommand.Parameters.Add(oParam);
                    oCommand.ExecuteNonQuery();
                }

                #endregion " Receipients "

                #region " RequestFlowMultipleReceipient_External "
                //if (Document.RequestFlowMultipleReceipient_ExternalList.Count > 0)
                //{
                //    oCommand.Parameters.Clear();
                //    oCommand.CommandText = "delete from RequestFlowMultipleReceipient_External where RequestID = @RequestID";
                //    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                //    oParam.Value = __requestID;
                //    oCommand.Parameters.Add(oParam);
                //    oCommand.ExecuteNonQuery();

                //    oCommand.Parameters.Clear();
                //    oCommand.CommandText = "select * from RequestFlowMultipleReceipient_External where RequestID = @RequestID";
                //    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                //    oParam.Value = __requestID;
                //    oCommand.Parameters.Add(oParam);

                //    oAdapter = new SqlDataAdapter(oCommand);
                //    oCB = new SqlCommandBuilder(oAdapter);
                //    oTable = new DataTable("REQUESTFLOW");
                //    oAdapter.FillSchema(oTable, SchemaType.Source);


                //    for (int index = 0; index < Document.RequestFlowMultipleReceipient_ExternalList.Count; index++)
                //    {
                //        RequestFlowMultipleReceipient_External RCPM = Document.RequestFlowMultipleReceipient_ExternalList[index];
                //        oDataRow = oTable.NewRow();
                //        oDataRow["RequestID"] = __requestID;
                //        oDataRow["ApproverID"] = RCPM.ApproverID;
                //        oDataRow["ActionCode"] = RCPM.ActionCode;
                //        if (RCPM.ActionDate > DateTime.MinValue)
                //            oDataRow["ActionDate"] = RCPM.ActionDate;
                //        oDataRow["ActionBy"] = RCPM.ActionBy;
                //        oDataRow["Comment"] = RCPM.Comment;
                //        oDataRow["ApproverGroup"] = RCPM.ApproverGroup;
                //        oDataRow["Operation"] = RCPM.Operation;
                //        oDataRow["Sequence"] = RCPM.Sequence;
                //        oDataRow["IsNotify"] = RCPM.IsNotify;
                //        oDataRow["IsCurrent"] = RCPM.IsCurrent;
                //        oDataRow["ApproverName"] = RCPM.ApproverName;
                //        oDataRow["ApproverEmail"] = RCPM.ApproverEmail;
                //        oDataRow["ContractID"] = RCPM.ContractID;
                //        oDataRow["OperationFromContract"] = RCPM.OperationFromContract;
                //        oTable.Rows.Add(oDataRow);
                //    }

                //    oAdapter.Update(oTable);
                //}
                //else
                //{
                //    oCommand.Parameters.Clear();
                //    oCommand.CommandText = "delete from RequestFlowMultipleReceipient_External where RequestID = @RequestID";
                //    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                //    oParam.Value = __requestID;
                //    oCommand.Parameters.Add(oParam);
                //    oCommand.ExecuteNonQuery();
                //}

                #endregion " Receipients "

                #region "ReceipientSnapShot"
                oCommand.Parameters.Clear();
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.CommandText = "sp_ResolveReceipient";
                oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                oParam.Value = __requestID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@ItemID", SqlDbType.Int);
                oParam.Value = __requestFlowItemID;
                oCommand.Parameters.Add(oParam);

                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                DataTable dtReceipientForOtherCompany = new DataTable("ReceipientForOtherCompany");
                oAdapter.Fill(dtReceipientForOtherCompany);

                 //If this request have any receipient in others company,we also Insert receipient snapshot in others company
               // WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).ResolveReceipientToOtherCompany(dtReceipientForOtherCompany);
                #endregion

                if (tx == null)
                {
                    __tx.Commit();
                }
            }
            catch (Exception e)
            {
                if (tx == null)
                {
                    __tx.Rollback();
                }
                throw new Exception("UpdateDocument failed!", e);
            }
            finally
            {
                oCB.Dispose();
                oAdapter.Dispose();
                if (Connection == null)
                {
                    oConnection.Close();
                    oConnection.Dispose();
                }
                oCommand.Dispose();
                oTable.Dispose();
            }

            
        }

        public override DataTable GetRecipientByItemID(int RequestID, int ItemID)
        {
            oSqlManage["ProcedureName"] = "sp_RequestDocumentGetRecipientByItemID";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestID"] = RequestID;
            oParams["@p_ItemID"] = ItemID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
        }

        public override DataTable GetDelegationInfo(int RequestID)
        {

            oSqlManage["ProcedureName"] = "sp_DelegateAuthorizeCheck";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@EmployeeID"] = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            if (!WorkflowPrinciple.Current.UserSetting.Employee.IsExternalUser)
            {
                oParams["@EmployeePositionID"] = WorkflowPrinciple.Current.UserSetting.Employee.PositionID;
            }
            oParams["@RequestID"] = RequestID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
        }

        public override void ResolveReceipientToOtherCompany(DataRow dr)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_ResolveReceipientToOtherCompany", oConnection);
            oCommand.Parameters.Add(new SqlParameter("@RequestID", SqlDbType.Int)).Value = (int)dr["RequestID"];
            oCommand.Parameters.Add(new SqlParameter("@RequestCompanyCode", SqlDbType.VarChar)).Value = (string)dr["RequestCompanyCode"];
            oCommand.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.Int)).Value = (int)dr["ItemID"];
            oCommand.Parameters.Add(new SqlParameter("@ReceipientID", SqlDbType.Int)).Value = (int)dr["ReceipientID"];
            oCommand.Parameters.Add(new SqlParameter("@SnapshotID", SqlDbType.Int)).Value = (int)dr["SnapshotID"];
            oCommand.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.VarChar)).Value = (string)dr["EmployeeID"];
            oCommand.Parameters.Add(new SqlParameter("@PositionID", SqlDbType.VarChar)).Value = (string)dr["PositionID"];
            oCommand.Parameters.Add(new SqlParameter("@CompanyCode", SqlDbType.VarChar)).Value = (string)dr["CompanyCode"];
            oCommand.Parameters.Add(new SqlParameter("@IsMark", SqlDbType.Bit)).Value = (bool)dr["IsMark"];
            oCommand.Parameters.Add(new SqlParameter("@MarkDate", SqlDbType.DateTime)).Value = (DateTime)dr["MarkDate"];
            oCommand.ExecuteNonQuery();
        }
     
        #endregion " UpdateDocument "

        //AddBy: Ratchatawan W. (2016-Mar-15)
        protected void oDoc_ResolveText(RequestTextSolverEventArgs e)
        {   
            if (e.ClassName != "")
            {
                switch (e.ClassName.Substring(1).ToUpper())
                {
                    case "YEARMONTH":
                        DateTime yearmonthDate = DateTime.MinValue;
                        DateTime.TryParseExact(e.Value.ToString(), "yyyyMM", DefaultCultureInfo, DateTimeStyles.None, out yearmonthDate);
                        if (yearmonthDate != DateTime.MinValue)
                        {
                            e.ResolveText = string.Format("{0} {1}", WorkflowManagement.CreateInstance(CompanyCode).GetCommonText("MONTH", Language, yearmonthDate.Month.ToString("00")), yearmonthDate.Year.ToString("0000"));
                            e.IsHandler = true;
                        }
                        break;
                    case "TIME":
                        TimeSpan time1;
                        DateTime time_dateBuffer;
                        if (e.Value.GetType() == typeof(TimeSpan))
                        {
                            time1 = (TimeSpan)e.Value;
                            time_dateBuffer = DateTime.Now.Date.Add(time1);
                            e.ResolveText = string.Format("{0}", time_dateBuffer.ToString("HH:mm"));
                            e.IsHandler = true;
                        }
                        else if (e.Value.GetType() == typeof(string))
                        {
                            TimeSpan.TryParse((string)e.Value, out time1);
                            try
                            {
                                time_dateBuffer = DateTime.Now.Date.Add(time1);
                                e.ResolveText = string.Format("{0}", time_dateBuffer.ToString("HH:mm"));
                                e.IsHandler = true;
                            }
                            catch
                            {
                                e.ResolveText = "";
                                e.IsHandler = true;
                            }
                        }
                        break;
                    case "DECIMAL":
                        e.IsHandler = true;
                        e.ResolveText = ((decimal)e.Value).ToString("#,##0.00");
                        break;
                    case "LOWER": //AddBy: Ratchatawan W. (27 Oct 2016)
                        e.IsHandler = true;
                        e.ResolveText = e.Value.ToString().ToLower();
                        break;
                    case "UPPER": //AddBy: Ratchatawan W. (27 Oct 2016)
                        e.IsHandler = true;
                        e.ResolveText = e.Value.ToString().ToUpper();
                        break;
                }
                switch (e.ClassName.ToUpper())
                {
                    case "LOWER": //AddBy: Ratchatawan W. (27 Oct 2016)
                        e.IsHandler = true;
                        e.ResolveText = e.Value.ToString().ToLower();
                        break;
                    case "UPPER": //AddBy: Ratchatawan W. (27 Oct 2016)
                        e.IsHandler = true;
                        e.ResolveText = e.Value.ToString().ToUpper();
                        break;
                }
            }
        }
        public override string GetRequestTextOniService3(string Category, string Language, string Code)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetRequestText(@Category,@Language) where TextCode = @TextCode", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@Category", SqlDbType.VarChar);
            oParam.Value = Category;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TextCode", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TEXTTABLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = (string)dr["TextDescription"];
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<RequestDocument> GetRequestDocumentByRequestTypeID(int RequestTypeID)
        {
            List<RequestDocument> oResult = new List<RequestDocument>();
            oSqlManage["ProcedureName"] = "sp_RequestDocumentGetByRequestTypeID_Lucifer";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestTypeID"] = RequestTypeID;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<RequestDocument>(oSqlManage, oParams));
            return oResult;
        }

        public override void UpdateRequestTypeSummarySnapshot(List<RequestDocument> oRequestDocumentList, int RequestTypeID)
        {
            RequestType rt = GetRequestType(RequestTypeID, DateTime.Now, "TH");
            PortalSetting oSetting = PortalEngineManagement.CreateInstance(CompanyCode).GetPortalSetting(rt);
            IDataService dataservice;

            foreach (RequestDocument Document in oRequestDocumentList)
            {

                #region set service
                try
                {
                    dataservice = GetDataService(oSetting.DataAssembly, oSetting.DataClass);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    throw new Exception("Can't create data's service");
                }
                Document.SetDataService(dataservice);
                //doc.Comment = ServiceManager.HRTMBuffer.GetRemarkText(Language);
                #endregion

                try
                {
                    Document.RequestorCompanyCode = this.CompanyCode;
                    string strSummaryEN, strSummaryTH;
                    Document.ResolveText += new RequestTextSolverHandler(oDoc_ResolveText);
                    strSummaryTH = Document.SummaryTextTH;
                    strSummaryEN = Document.SummaryTextEN;
                    WorkflowManagement.CreateInstance(CompanyCode).RequestTypeSummarySave(Document.RequestID, Document.CurrentItemID, strSummaryTH, strSummaryEN);
                }
                catch (Exception ex)
                { }

            }

        }

        public override void AutoRejectRequest_WaitForPDMSStatus(RequestDocument oDoc, FlowItemAction oAction, EmployeeData oEmpData)
        {
            PortalSetting oSetting = PortalEngineManagement.CreateInstance(oDoc.RequestorCompanyCode).GetPortalSetting(oDoc.RequestType);

            try
            {
                IDataService dataservice = GetDataService(oSetting.DataAssembly, oSetting.DataClass);
                oDoc.Comment = "Auto reject to requestor";
                oDoc.SetDataService(dataservice);

                RequestActionResult oRequestActionResult = oDoc.Process(oAction, oEmpData);
                if (oRequestActionResult.GoToNextItem)
                {
                    UpdatePDMSRequestWaitForReturnToRequestor(oDoc.RequestNo, "COMPLETED");
                }
            }
            catch (Exception)
            {
                UpdatePDMSRequestWaitForReturnToRequestor(oDoc.RequestNo, "NO_PDMS_STATUS");
            }

        }


        #region " LoadDocument "

        public override RequestDocument LoadDocument(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView)
        {
            RequestDocument oReturn = new RequestDocument(); ;
            oSqlManage["ProcedureName"] = "sp_RequestDocumentGetByRequestNo";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestNo"] = RequestNo;
            if(!string.IsNullOrEmpty(KeyMaster))
                oParamRequest["@KeyMaster"] = KeyMaster;

            DataTable oTable = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);
            foreach(DataRow dr in oTable.Rows)
                this.ParseToRequestDocument(dr, oReturn);

            if (oReturn.RequestID > -1)
            {
                bool lFound = false;
                if (!IsDataOwnerView)
                {
                    SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                    SqlCommand oCommand = new SqlCommand("Select * From V_RequestReceipient where RequestID = @ReqID ", oConnection);
                    SqlParameter oParam;
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = oReturn.RequestID;
                    oCommand.Parameters.Add(oParam);
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

                    oTable = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    //Created By: Ratchatawan W. (2012-04-24)
                    //Remark: Get all receipient in this request (all in past and next)
                    oCommand.CommandText = "Select * From V_RequestReceipientAll where RequestID = @ReqID ";
                    DataTable dtAllReceipient = new DataTable("REQUESTFLOWALLRECEIPIENT");
                    oAdapter.Fill(dtAllReceipient);

                    oConnection.Close();

                    bool lCheckDelegate = false;
                    /* CommentBy: Ratchatawan W. (2012-04-24)
                    List<EmployeeData> DelegatePersons = WorkflowPrinciple.Current.UserSetting.Employee.GetDelegatePersons();
                    lCheckDelegate = oReturn.CurrentFlowItem.StateID == 1 && DelegatePersons.Count > 0;
                    */

                    //Created By: Ratchatawan W. (2012-04-24)
                    //Remark: Check delegate person in this request
                    List<EmployeeData> DelegatePersons = new List<EmployeeData>();
                    lCheckDelegate = true;// CheckDelegateAuthorize(__reqid, WorkflowPrinciple.Current.UserSetting.Employee);

                    //Comment By: Ratchatawan W. (2012-04-24)
                    //foreach (DataRow dr in oTable.Rows)

                    //Created By: Ratchatawan W. (2012-04-24)
                    foreach (DataRow dr in dtAllReceipient.Rows)
                    {
                        Receipient RC = this.ParseToReceipient(dr);
                        RC.RequestID = oReturn.RequestID;

                        //Created By: Ratchatawan W. (2012-04-24)
                        //Remark: Add current receipient in ReceipientsList of this request document
                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow drReceipient in oTable.Rows)
                            {
                                if (!oReturn.Receipients.Exists(obj => obj.Code == RC.Code && obj.CompanyCode == RC.CompanyCode && obj.Type == RC.Type))
                                {
                                    if (dr["ReceipientCode"].Equals(drReceipient["ReceipientCode"]))
                                        oReturn.Receipients.Add(RC);
                                }
                            }
                            //else
                            //{
                            //    if (dr["ReceipientCode"].ToString() == "#CREATOR")
                            //    {
                            //        oReturn.Receipients.Add(RC);s
                            //    }

                            //    if (dr["ReceipientCode"].ToString() == "#REQUESTOR")
                            //    {
                            //        oReturn.Receipients.Add(RC);
                            //    }
                            //}
                        }
                        //Comment By: Ratchatawan w. (2012-04-24)
                        //oReturn.Receipients.Add(RC);

                        if (lCheckDelegate)
                        {
                            DelegatePersons.Add(WorkflowPrinciple.Current.UserSetting.Employee);
                            lFound = true;
                            //Comment By: Ratchatawan w. (2012-04-24)
                            //if (CheckIsAuthorize(RC, oReturn, DelegatePersons))
                            //{
                            //lFound = true;
                            //}
                        }
                        else
                        {
                            if (CheckIsAuthorize(RC, oReturn))
                            {
                                lFound = true;
                            }
                        }
                    }
                    oTable.Dispose();
                    oAdapter.Dispose();
                    oConnection.Dispose();
                    oCommand.Dispose();
                }
                if (!lFound)
                {
                    if (IsDataOwnerView)
                    {
                        Receipient RC = new Receipient();//Receipient.CreateInstance(CompanyCode);
                        RC.Type = ReceipientType.SPECIAL;
                        RC.Code = "#DATAOWNER";
                        RC.RequestID = oReturn.RequestID;
                        oReturn.IsDataOwnerView = true;
                        lFound = CheckIsAuthorize(RC, oReturn);
                    }
                    //else if (CheckTrainingData(WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID, RequestNo) && IsOwnerView)
                    //{
                    //    oReturn.IsOwnerView = true;
                    //    lFound = true;
                    //}
                    else if (IsOwnerView)
                        lFound = true;
                    else if ((oReturn.CreatorNo == WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID || oReturn.RequestorNo == WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))// && IsOwnerView)
                    {
                        oReturn.IsOwnerView = true;
                        lFound = true;
                    }
                    if (!lFound)
                    {
                        throw new WorkflowException(string.Format("You are not authorize for request '{0}'", RequestNo));
                    }

                   
                }
            }
            else
            {
                throw new WorkflowException(string.Format("Can't load request '{0}'", RequestNo));
            }
            
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-12-27)
        public override RequestDocument LoadDocumentReadonly(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView)
        {
            RequestDocument oReturn = new RequestDocument(); ;
            oSqlManage["ProcedureName"] = "sp_RequestDocumentGetByRequestNo";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestNo"] = RequestNo;
            if (!string.IsNullOrEmpty(KeyMaster))
                oParamRequest["@KeyMaster"] = KeyMaster;

            DataTable oTable = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);
            foreach (DataRow dr in oTable.Rows)
                this.ParseToRequestDocument(dr, oReturn);

            int __reqid = oReturn.RequestID;
            if (__reqid <= -1)
            {
                throw new WorkflowException(string.Format("Can't load request '{0}'", RequestNo));
            }

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("Select * From V_RequestReceipient where RequestID = @ReqID", oConnection);
            SqlParameter oParam;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable dtAllReceipient = new DataTable("REQUESTFLOWALLRECEIPIENT");
            oParam = new SqlParameter("@ReqID", SqlDbType.Int);
            oParam.Value = __reqid;
            oCommand.Parameters.Add(oParam);

            oAdapter.Fill(dtAllReceipient);

            foreach (DataRow dr in dtAllReceipient.Rows)
            {
                Receipient RC = this.ParseToReceipient(dr);
                RC.RequestID = oReturn.RequestID;
                oReturn.Receipients.Add(RC);
            }

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oReturn;
        }


        public override RequestDocument LoadDocument(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView, EmployeeData Requestor)
        {
            RequestDocument oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            oCommand = new SqlCommand("select top 1 * from RequestDocument where RequestNo = @RequestNo", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@KeyMaster", SqlDbType.VarChar);
            oParam.Value = KeyMaster;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            int __reqid = -1;
            foreach (DataRow dr in oTable.Rows)
            {
                __reqid = (int)dr["RequestID"];
                oReturn = new RequestDocument(KeyMaster == "");
                this.ParseToRequestDocument(dr, oReturn);
                break;
            }
            oTable.Dispose();

            if (__reqid > -1)
            {
                bool lFound = false;
                if (!IsDataOwnerView)
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From V_RequestReceipient where RequestID = @ReqID ";
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = __reqid;
                    oCommand.Parameters.Add(oParam);

                    oTable = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    //Created By: Ratchatawan W. (2011-08-10)
                    //Remark: Get all receipient in this request (all in past and next)
                    oCommand.CommandText = "Select * From V_RequestReceipientAll where RequestID = @ReqID ";
                    DataTable dtAllReceipient = new DataTable("REQUESTFLOWALLRECEIPIENT");
                    oAdapter.Fill(dtAllReceipient);

                    oConnection.Close();

                    bool lCheckDelegate = false;
                    /* CommentBy: Ratchatawan W. (2011-08-23)
                    List<EmployeeData> DelegatePersons = WorkflowPrinciple.Current.UserSetting.Employee.GetDelegatePersons();
                    lCheckDelegate = oReturn.CurrentFlowItem.StateID == 1 && DelegatePersons.Count > 0;
                    */

                    //Created By: Ratchatawan W. (2011-08-23)
                    //Remark: Check delegate person in this request
                    List<EmployeeData> DelegatePersons = new List<EmployeeData>();
                    lCheckDelegate = true;//CheckDelegateAuthorize(__reqid,WorkflowPrinciple.Current.UserSetting.Employee);

                    //Comment By: Ratchatawan W. (2011-08-10)
                    //foreach (DataRow dr in oTable.Rows)

                    //Created By: Ratchatawan W. (2011-08-10)
                    foreach (DataRow dr in dtAllReceipient.Rows)
                    {
                        Receipient RC = this.ParseToReceipient(dr);
                        RC.RequestID = oReturn.RequestID;
                        //Created By: Ratchatawan W. (2011-08-10)
                        //Remark: Add current receipient in ReceipientsList of this request document
                        if (oTable.Rows.Count > 0)
                        {
                            if (oTable.Select("ReceipientCode = '" + dr["ReceipientCode"].ToString() + "'").Length > 0)
                                oReturn.Receipients.Add(RC);
                        }
                        //Comment By: Ratchatawan w. (2011-08-10)
                        //oReturn.Receipients.Add(RC);

                        if (lCheckDelegate)
                        {
                            DelegatePersons.Add(Requestor);
                            lFound = true;
                            //Comment By: Ratchatawan w. (2011-08-23)
                            //if (CheckIsAuthorize(RC, oReturn, DelegatePersons))
                            //{
                            //lFound = true;
                            //}
                        }
                        else
                        {
                            if (CheckIsAuthorize(RC, oReturn, Requestor))
                            {
                                lFound = true;
                            }
                        }
                    }
                    oTable.Dispose();
                }
                if (!lFound)
                {
                    if (IsDataOwnerView)
                    {
                        Receipient RC = new Receipient();// Receipient.CreateInstance(CompanyCode);
                        RC.Type = ReceipientType.SPECIAL;
                        RC.Code = "#DATAOWNER";
                        RC.RequestID = oReturn.RequestID;
                        oReturn.IsDataOwnerView = true;
                        lFound = CheckIsAuthorize(RC, oReturn);
                    }
                    else if (IsOwnerView)
                        lFound = true;
                    else if ((oReturn.CreatorNo == Requestor.EmployeeID || oReturn.RequestorNo == Requestor.EmployeeID))// && IsOwnerView)
                    {
                        oReturn.IsOwnerView = true;
                        lFound = true;
                    }
                    if (!lFound)
                    {
                        throw new WorkflowException(string.Format("You are not authorize for request '{0}'", RequestNo));
                    }
                }
            }
            else
            {
                throw new WorkflowException(string.Format("Can't load request '{0}'", RequestNo));
            }
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oReturn;
        }

        public override RequestDocument LoadDocumentWithOutCheckAuthorize(string strRequestNo)
        {
            RequestDocument oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            oCommand = new SqlCommand("select top 1 A.*,flow.DocVersionID  from RequestDocument A  JOIN RequestFLow flow ON flow.RequestID = A.RequestID and A.ItemID = flow.ItemID where A.RequestNo = @RequestNo  ", oConnection);
            
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = strRequestNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@KeyMaster", SqlDbType.VarChar);
            oParam.Value = "";
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            int __reqid = -1;
            foreach (DataRow dr in oTable.Rows)
            {
                __reqid = (int)dr["RequestID"];
                oReturn = new RequestDocument();
                this.ParseToRequestDocument(dr, oReturn);
                break;
            }
            oTable.Dispose();

            if (__reqid > -1)
            {
                bool IsDataOwnerView = false;
                if (!IsDataOwnerView)
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From V_RequestReceipient where RequestID = @ReqID ";
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = __reqid;
                    oCommand.Parameters.Add(oParam);

                    oTable = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable);
                    oConnection.Close();

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Receipient RC = this.ParseToReceipient(dr);
                        oReturn.Receipients.Add(RC);
                    }
                    oTable.Dispose();
                }
            }
            else
            {
                throw new WorkflowException(string.Format("Can't load request '{0}'", strRequestNo));
            }
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oReturn;
        }
        #endregion " LoadDocument "

       

        #region " CheckAuthorize "

        protected override Receipient CheckAuthorize(RequestDocument requestDocument)
        {
            Receipient oReturn = null;
            bool lReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select top 1 * from RequestDocument where RequestNo = @RequestNo and KeyMaster = @KeyMaster", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = requestDocument.RequestNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@KeyMaster", SqlDbType.VarChar);
            oParam.Value = requestDocument.KeyMaster;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            try
            {
                oConnection.Open();
                oAdapter.Fill(oTable);
                oConnection.Close();

                lReturn = oTable.Rows.Count > 0;
                int __reqid = -1;
                foreach (DataRow dr in oTable.Rows)
                {
                    __reqid = (int)dr["RequestID"];
                    break;
                }
                if (!lReturn)
                {
                    throw new Exception("KeyMaster incorrect");
                }
                lReturn = false;
                if (requestDocument.IsDataOwnerView)
                {
                    lReturn = true;
                }
                else if (requestDocument.IsOwnerView)
                {
                    if ((WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == requestDocument.CreatorNo) || (WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == requestDocument.RequestorNo))
                    {
                        if ((WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == requestDocument.CreatorNo))
                        {
                            oReturn = new Receipient(ReceipientType.EMPLOYEE, requestDocument.CreatorNo, requestDocument.CreatorCompanyCode, requestDocument.RequestID);
                        }
                        else
                        {
                            oReturn = new Receipient(ReceipientType.EMPLOYEE, requestDocument.RequestorNo, requestDocument.RequestorCompanyCode, requestDocument.RequestID);
                        }
                        lReturn = true;
                    }
                }
                else
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From V_RequestReceipient where RequestID = @ReqID ";
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = __reqid;
                    oCommand.Parameters.Add(oParam);

                    oTable = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable);
                    oConnection.Close();

                    bool lCheckDelegate = false;
                    List<EmployeeData> DelegatePersons = new List<EmployeeData>();// WorkflowPrinciple.Current.UserSetting.Employee.GetDelegatePersons();
                   // lCheckDelegate = requestDocument.CurrentFlowItem.StateID == 1 && DelegatePersons.Count > 0;

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Receipient RC = this.ParseToReceipient(dr);
                        if (lCheckDelegate)
                        {
                            DelegatePersons.Add(WorkflowPrinciple.Current.UserSetting.Employee);
                            if (CheckIsAuthorize(RC, requestDocument, DelegatePersons))
                            {
                                oReturn = RC;
                                lReturn = true;
                                break;
                            }
                        }
                        else
                        {
                            if (CheckIsAuthorize(RC, requestDocument))
                            {
                                oReturn = RC;
                                lReturn = true;
                                break;
                            }
                        }
                    }
                    oTable.Dispose();
                }

                if (!lReturn)
                {
                    throw new Exception("No authorize for action this request");
                }
            }
            catch (Exception ex)
            {
                throw new WorkflowException(ex.Message);
            }
            finally
            {
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
            return oReturn;
        }

        protected override Receipient CheckAuthorize(RequestDocument requestDocument, EmployeeData TakeActionBy)
        {
            Receipient oReturn = null;
            bool lReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select top 1 * from RequestDocument where RequestNo = @RequestNo and KeyMaster = @KeyMaster", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = requestDocument.RequestNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@KeyMaster", SqlDbType.VarChar);
            oParam.Value = requestDocument.KeyMaster;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            try
            {
                oConnection.Open();
                oAdapter.Fill(oTable);
                oConnection.Close();

                lReturn = oTable.Rows.Count > 0;
                int __reqid = -1;
                foreach (DataRow dr in oTable.Rows)
                {
                    __reqid = (int)dr["RequestID"];
                    break;
                }
                if (!lReturn)
                {
                    throw new Exception("KeyMaster incorrect");
                }
                lReturn = false;
                if (requestDocument.IsDataOwnerView)
                {
                    lReturn = true;
                }
                else if (requestDocument.IsOwnerView)
                {
                    if ((TakeActionBy.EmployeeID == requestDocument.CreatorNo) || (TakeActionBy.EmployeeID == requestDocument.RequestorNo))
                    {
                        if ((TakeActionBy.EmployeeID == requestDocument.CreatorNo))
                        {
                            oReturn = new Receipient(ReceipientType.EMPLOYEE, requestDocument.CreatorNo, requestDocument.CreatorCompanyCode, requestDocument.RequestID);
                        }
                        else
                        {
                            oReturn = new Receipient(ReceipientType.EMPLOYEE, requestDocument.RequestorNo, requestDocument.RequestorCompanyCode, requestDocument.RequestID);
                        }
                        lReturn = true;
                    }
                }
                else
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From V_RequestReceipient where RequestID = @ReqID ";
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = __reqid;
                    oCommand.Parameters.Add(oParam);

                    oTable = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable);
                    oConnection.Close();

                    bool lCheckDelegate = false;
                    //List<EmployeeData> DelegatePersons = TakeActionBy.GetDelegatePersons();
                    // lCheckDelegate = requestDocument.CurrentFlowItem.StateID == 1 && DelegatePersons.Count > 0;
                    if (!TakeActionBy.IsExternalUser)
                        lCheckDelegate = CheckDelegateAuthorize(__reqid, TakeActionBy);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Receipient RC = this.ParseToReceipient(dr);
                        if (lCheckDelegate)
                        {
                           // DelegatePersons.Add(TakeActionBy);
                            RC.Type = ReceipientType.SPECIAL_DELEGATE;
                            RC.Code = TakeActionBy.EmpSubGroupForDelegate.ToString();
                            oReturn = RC;
                            lReturn = true;
                            break;
                            //if (CheckIsAuthorize(RC, requestDocument, DelegatePersons))
                            //{
                            //    oReturn = RC;
                            //    lReturn = true;
                            //    break;
                            //}
                        }
                        else
                        {
                            if (CheckIsAuthorize(RC, requestDocument, TakeActionBy))
                            {
                                oReturn = RC;
                                lReturn = true;
                                break;
                            }
                        }
                    }
                    oTable.Dispose();
                }

                if (!lReturn)
                {
                    throw new Exception("No authorize for action this request");
                }
            }
            catch (Exception ex)
            {
                throw new WorkflowException(ex.Message);
            }
            finally
            {
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
            return oReturn;
        }

        private bool CheckIsAuthorize(Receipient RC, RequestDocument requestDocument)
        {
            List<EmployeeData> EmployeeList = new List<EmployeeData>();
            EmployeeList.Add(WorkflowPrinciple.Current.UserSetting.Employee);
            return CheckIsAuthorize(RC, requestDocument, EmployeeList);
        }

        private bool CheckIsAuthorize(Receipient RC, RequestDocument requestDocument, List<EmployeeData> EmployeeList)
        {
            bool lReturn = false;
            EmployeeData oEmpByPos;
            foreach (Receipient rc1 in requestDocument.TranslateReceipient(RC))
            {
                switch (rc1.Type)
                {
                    case ReceipientType.EMPLOYEE:
                        foreach (EmployeeData item in EmployeeList)
                        {
                            if (rc1.Code == item.EmployeeID)
                            {
                                lReturn = true;
                                break;
                            }
                        }
                        break;

                    case ReceipientType.USERROLE:
                        foreach (EmployeeData item in EmployeeList)
                        {
                            UserSetting oSetting = new UserSetting(item.EmployeeID,item.CompanyCode);
                            if (oSetting.Roles.Contains(rc1.Code.ToUpper()))
                            {
                                lReturn = true;
                                break;
                            }
                        }
                        break;

                    case ReceipientType.POSITION:
                        //oEmpByPos = new EmployeeData().GetEmployeeByPositionID(rc1.Code);
                        oEmpByPos = EmployeeManagement.CreateInstance(rc1.CompanyCode).GetEmployeeDataByPosition(rc1.Code, requestDocument.SubmittedDate, WorkflowPrinciple.Current.UserSetting.Language);
                        foreach (EmployeeData item in EmployeeList)
                        {
                            if (oEmpByPos.EmployeeID == item.EmployeeID)
                            {
                                lReturn = true;
                                break;
                            }
                        }
                        break;

                    case ReceipientType.CUSTODIAN:
                        //oEmpByPos = new EmployeeData().GetEmployeeByPositionID(rc1.Code);
                        oEmpByPos = EmployeeManagement.CreateInstance(rc1.CompanyCode).GetPettyCustodianGetByCode(rc1.Code);
                        foreach (EmployeeData item in EmployeeList)
                        {
                            if (oEmpByPos.EmployeeID == item.EmployeeID)
                            {
                                lReturn = true;
                                break;
                            }
                        }
                        break;

                    case ReceipientType.EXTERNAL_USER:
                        oEmpByPos = new EmployeeData();
                        oEmpByPos.CompanyCode = rc1.CompanyCode;
                       // DataTable dt = EmployeeManagement.CreateInstance(requestDocument.CompanyCode).GetExternalUserbyID(rc1.Code, WorkflowPrinciple.Current.UserSetting.Language);
                        oEmpByPos.EmployeeID = rc1.Code;
                        foreach (EmployeeData item in EmployeeList)
                        {
                            if (oEmpByPos.EmployeeID == item.EmployeeID)
                            {
                                lReturn = true;
                                break;
                            }
                        }
                        break;

                    case ReceipientType.SPECIAL:
                        List<Receipient> list = requestDocument.GetSpecialReceipient(rc1.Code);
                        foreach (Receipient rcp in list)
                        {
                            if (rcp.Type == ReceipientType.EMPLOYEE)
                            {
                                foreach (EmployeeData item in EmployeeList)
                                {
                                    if (rcp.Code == item.EmployeeID)
                                    {
                                        lReturn = true;
                                        break;
                                    }
                                }
                            }
                            else if (rcp.Type == ReceipientType.POSITION)
                            {
                                oEmpByPos = EmployeeManagement.CreateInstance(rc1.CompanyCode).GetEmployeeDataByPosition(rcp.Code, DateTime.Now, WorkflowPrinciple.Current.UserSetting.Language);
                                foreach (EmployeeData item in EmployeeList)
                                {
                                    if (oEmpByPos.EmployeeID == item.EmployeeID)
                                    {
                                        lReturn = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                }
                if (lReturn)
                {
                    break;
                }
            }
            return lReturn;
        }

        //AddBy: Ratchatawan W. (2012-09-06)
        private bool CheckIsAuthorize(Receipient RC, RequestDocument requestDocument, EmployeeData Requestor)
        {
            List<EmployeeData> EmployeeList = new List<EmployeeData>();
            EmployeeList.Add(Requestor);
            EmployeeList.Add(WorkflowPrinciple.Current.UserSetting.Employee);
            return CheckIsAuthorize(RC, requestDocument, EmployeeList);
        }

        #endregion " CheckAuthorize "

        #region " SaveData "

        public override void SaveDataByPass(RequestDocument Document, DataTable Header, Object Additional)
        {
            SaveData(Document, Header, Additional, true);
        }

        public override void SaveData(RequestDocument Document, DataTable Header, Object Additional)
        {
            SaveData(Document, Header, Additional, false);
        }

        private void SaveData(RequestDocument Document, DataTable Header, Object Additional, bool isByPass)
        {
            int __requestID = -1;
            int __docVersionID = -1;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();

            #region " Find requestid "

            SqlCommand oCommand = new SqlCommand("select RequestID from RequestDocument where RequestNo = @RequestNo", oConnection, tx);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = Document.RequestNo;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            oAdapter.Fill(oTable);
            if (oTable.Rows.Count > 0)
            {
                __requestID = (int)oTable.Rows[0]["RequestID"];
            }
            oTable.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            #endregion " Find requestid "

            try
            {
                if (__requestID == -1)
                {
                    Document.CurrentItemID = 0;
                }
                if (!isByPass)
                {
                    UpdateDocument(Document, null, oConnection, tx);
                }
                if (__requestID == -1)
                {
                    oCommand = new SqlCommand("select RequestID from RequestDocument where RequestNo = @RequestNo", oConnection, tx);
                    oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
                    oParam.Value = Document.RequestNo;
                    oCommand.Parameters.Add(oParam);
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("REQUESTDOCUMENT");
                    oAdapter.Fill(oTable);
                    if (oTable.Rows.Count > 0)
                    {
                        __requestID = (int)oTable.Rows[0]["RequestID"];
                    }
                    oTable.Dispose();
                    oAdapter.Dispose();
                    oCommand.Dispose();
                }
                if (Document.HasFileAttached)
                {
                    FileAttachmentSet oSet = Document.LoadFileAttached();
                    oSet.RequestNo = Document.RequestNo;
                    oSet.RequestID = __requestID;
                    oSet.CompanyCode = Document.RequestorCompanyCode;
                    //if (oSet.Count > 0)
                    SaveFileAttachment(oSet, oConnection, tx);
                    Document.FileSetID = oSet.FileSetID;
                }

                Document.RequestID = __requestID;
                Document.DocversionID += 1;

                #region Save HeaderData and AdditionalData to MongoDB
                InsertOrUpdateDataInMongoDB("Data", Document, Newtonsoft.Json.JsonConvert.SerializeObject(Header));
                InsertOrUpdateDataInMongoDB("AdditionalData", Document, Newtonsoft.Json.JsonConvert.SerializeObject(Additional));
                #endregion

                oCommand.Parameters.Clear();
                oCommand.CommandText = "Update RequestFlow set DocVersionID = @VersionID where RequestID = @RequestID and ItemID = @ItemID";
                oParam = new SqlParameter("@VersionID", SqlDbType.VarChar);
                oParam.Value = Document.DocversionID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@RequestID", SqlDbType.VarChar);
                oParam.Value = __requestID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@ItemID", SqlDbType.VarChar);
                oParam.Value = Document.CurrentItemID;
                oCommand.Parameters.Add(oParam);

                oCommand.ExecuteNonQuery();
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("Save error", ex);
            }
            finally
            {
                oConnection.Close();
                oCB.Dispose();
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }

            #region "RequestTypeSummarySnapshot"
            try
            {

                Document.Data = Header;
                string strSummaryEN, strSummaryTH;
                Document.ResolveText += new RequestTextSolverHandler(oDoc_ResolveText);
                strSummaryTH = Document.SummaryTextTH;
                strSummaryEN = Document.SummaryTextEN;
                WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).RequestTypeSummarySave(__requestID, Document.CurrentItemID, strSummaryTH, strSummaryEN);

            }
            catch (Exception ex)
            { throw new Exception("Update RequestType Summary failed!", ex); }
            #endregion
        }

        #endregion " SaveData "

        #region " LoadAdditionalData "

        private void InsertOrUpdateDataInMongoDB(string CollectionName,RequestDocument Document,string JSonString)
        {
            BsonDocument newBsonDocument;
            MongoClient client = new MongoClient(this.MongoDBConnStr);
            IMongoDatabase database = client.GetDatabase(this.MongoDBDatabase);

            //Save Headerdata to MongoDB Collection Data.{RequestTypeID}.V{RequestTypeVersionID}
            var collectionData = database.GetCollection<BsonDocument>(string.Format("{0}.{1}.V{2}", CollectionName, Document.RequestTypeID.ToString(), Document.RequestType.VersionID.ToString()));
            var filter = Builders<BsonDocument>.Filter.Eq("RequestNo", Document.RequestNo) & Builders<BsonDocument>.Filter.Eq("DocversionID", Document.DocversionID);
            var updoneresult = collectionData.UpdateOne(filter,
                            Builders<BsonDocument>.Update.Set(CollectionName, JSonString));
            if (updoneresult.MatchedCount <= 0)
            {
                newBsonDocument = new BsonDocument();
                newBsonDocument.InsertAt(0, new BsonElement(CollectionName, JSonString));
                newBsonDocument.InsertAt(0, new BsonElement("DocversionID", Document.DocversionID));
                newBsonDocument.InsertAt(0, new BsonElement("RequestNo", Document.RequestNo));
                newBsonDocument.InsertAt(0, new BsonElement("RequestID", Document.RequestID));
                collectionData.InsertOne(newBsonDocument);
            }
        }

        public override Object LoadAdditionalData(string RequestNo, int RequestID, int RequestTypeID, int RequestTypeVersionID, int DocversionID)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("RequestID", RequestID) & Builders<BsonDocument>.Filter.Eq("RequestNo", RequestNo) & Builders<BsonDocument>.Filter.Eq("DocversionID", DocversionID);
            //Get data from mongo db
            MongoClient client = new MongoClient(this.MongoDBConnStr);

            IMongoDatabase database = client.GetDatabase(this.MongoDBDatabase);
            var collection = database.GetCollection<BsonDocument>(string.Format("AdditionalData.{0}.V{1}", RequestTypeID.ToString(), RequestTypeVersionID.ToString()));
            BsonDocument doc = collection.Find(filter).FirstOrDefault();
            string strAdditional = doc.GetElement("AdditionalData").Value.ToString();

            return JObject.Parse(strAdditional);
        }

        #endregion " LoadAdditionalData "

        #region " GetDisplayName "

        public override string GetDisplayName(ReceipientType type, string code)
        {
            string oReturn = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = null;
            SqlParameter oParam;
            switch (type)
            {
                case ReceipientType.EMPLOYEE:
                    //oCommand = new SqlCommand("select name from v_current_infotype0001 where EmployeeID = @ID", oConnection);
                    oCommand = new SqlCommand("select name from v_PA_Assignment_current where EmployeeID = @ID", oConnection);
                    oParam = new SqlParameter("@ID", SqlDbType.VarChar);
                    oParam.Value = code;
                    oCommand.Parameters.Add(oParam);
                    oConnection.Open();
                    oReturn = (string)oCommand.ExecuteScalar();
                    oConnection.Close();
                    break;

                case ReceipientType.POSITION:
                    EmployeeData oEmp = new EmployeeData().GetEmployeeByPositionID(code);
                    oReturn = string.Format("{0}-{1}", oEmp.ActionOfPosition.Text, oEmp.OrgAssignment.Name);
                    break;

                case ReceipientType.USERROLE:
                    oReturn = RequestText.CreateInstance(CompanyCode).LoadText("USERROLENAME", WorkflowPrinciple.Current.UserSetting.Language, code);
                    break;

                case ReceipientType.SPECIAL:
                    switch (code)
                    {
                        case "#TEST":
                            break;

                        default:
                            oReturn = code;
                            break;
                    }
                    break;
            }
            if (oCommand != null)
            {
                oCommand.Dispose();
            }
            oConnection.Dispose();
            return oReturn;
        }

        #endregion " GetDisplayName "

        #endregion " Request "

        #region " Application "

        #region " GetAuthorizeApplications "

        public override List<RequestApplication> GetAuthorizeApplications()
        {
            List<RequestApplication> oReturn = new List<RequestApplication>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetAuthorizeApplication(@ID) order by ApplicationID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("APPLICATION");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestApplication(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizeApplications "

        #region " GetAuthorizeApplicationSubject "

        public override List<ApplicationSubject> GetAuthorizeApplicationSubject(int ApplicationID)
        {
            List<ApplicationSubject> oReturn = new List<ApplicationSubject>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetAuthorizeSubject(@AppID,@ID)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@AppID", SqlDbType.Int);
            oParam.Value = ApplicationID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("SUBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToApplicationSubject(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizeApplicationSubject "

        #region " GetApplicationSubjectResponse "

        public override List<UserRoleResponseSetting> GetApplicationSubjectResponse(int SubjectID)
        {
            List<UserRoleResponseSetting> oReturn = new List<UserRoleResponseSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from ApplicationSubjectResponse where SubjectID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = SubjectID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("SUBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                UserRoleResponseSetting item = new UserRoleResponseSetting();
                item.UserRole = (string)dr["UserRole"];
                item.IncludeSub = (bool)dr["IncludeSub"];
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetApplicationSubjectResponse "

        #region " LoadApplicationSubject "

        public override ApplicationSubject LoadApplicationSubject(int SubjectID)
        {
            ApplicationSubject oReturn = new ApplicationSubject();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from ApplicationSubject Where SubjectID = @SubjectID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@SubjectID", SqlDbType.Int);
            oParam.Value = SubjectID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("SUBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToApplicationSubject(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " LoadApplicationSubject "

        #region " LoadApplicationSubjectAuthorize "

        public override List<string> LoadApplicationSubjectAuthorize(int SubjectID)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select UserRole from ApplicationPermission Where ObjectID = @SubjectID and ObjectType = 'SUBJECT'", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@SubjectID", SqlDbType.Int);
            oParam.Value = SubjectID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("SUBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add((string)dr["UserRole"]);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " LoadApplicationSubjectAuthorize "

        #endregion " Application "

        #region " GetCurrentDate "

        protected override DateTime GetCurrentDate()
        {
            return DateTime.Now;
        }

        #endregion " GetCurrentDate "

        #region " Box "

        #region " GetRequestDocumentList "
        public override List<RequestDocument> GetRequestDocumentList(string BoxCode, string EmployeePositionID, int ItemPerPage, int Page, RequestBoxFilter Filter)
        {
            RequestBox oBox = GetRequestBox(BoxCode);
            List<RequestDocument> lstRequestDocument = new List<RequestDocument>();
            RequestDocument requestDocument;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetRequestInBox", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@BoxID", SqlDbType.Int);
            oParam.Value = oBox.ID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmployeePositionID", SqlDbType.VarChar);
            oParam.Value = EmployeePositionID;
            oCommand.Parameters.Add(oParam);
            //Filter
            oParam = new SqlParameter("@RequestTypeID", SqlDbType.Int);
            oParam.Value = Filter.RequestTypeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@RequestorNo", SqlDbType.VarChar);
            oParam.Value = Filter.RequestorNo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
            oParam.Value = Filter.AdminGroup;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@CreatorNo", SqlDbType.VarChar);
            oParam.Value = Filter.CreatorNo;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");

            oConnection.Open();

            int nStartIndex = ItemPerPage * (Page - 1);
            int nMaxRecord = ItemPerPage;

            if (ItemPerPage > 0 && Page > 0)
            {
                oAdapter.Fill(nStartIndex, nMaxRecord, oTable);
            }
            else
            {
                oAdapter.Fill(oTable);
            }

            oConnection.Close();

            #region Parse Table to RequestDocumentObject
            DataTable oTable1;
            int __reqid;

            foreach (DataRow dr in oTable.Rows)
            {
                __reqid = (int)dr["RequestID"];
                requestDocument = new RequestDocument();


                this.ParseToRequestDocument(dr, requestDocument);
                lstRequestDocument.Add(requestDocument);

                if (__reqid > -1)
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From RequestFlowReceipient where RequestID = @RequestID and ItemID = @CurrentItemID";
                    oCommand.CommandType = CommandType.Text;
                    oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                    oParam.Value = requestDocument.RequestID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@CurrentItemID", SqlDbType.Int);
                    oParam.Value = requestDocument.CurrentItemID;
                    oCommand.Parameters.Add(oParam);

                    oTable1 = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable1);
                    oConnection.Close();

                    foreach (DataRow dr1 in oTable1.Rows)
                    {
                        requestDocument.Receipients.Add(this.ParseToReceipient(dr1));
                    }

                    oTable1.Dispose();
                }
            }
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            #endregion

            return lstRequestDocument;
        }
        public override List<RequestDocument> GetRequestDocumentList(string BoxCode, int ItemPerPage, int Page, RequestBoxFilter Filter)
        {
            string empid = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            RequestBox oBox = GetRequestBox(BoxCode);
            List<RequestDocument> oReturn = new List<RequestDocument>();
            RequestDocument oItem;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter oParam;
            SqlParameter oParamEmpID;
            if (Filter == null || Filter.RequestTypeID == -1)
            {
                if (Filter == null || Filter.RequestorNo == "")
                {
                    if (Filter == null || Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID order by A.LastActionDate desc", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup order by A.LastActionDate desc", oConnection);
                        }

                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }

                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);
                }
                else
                {
                    if (Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID where A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID where A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }

                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }

                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@RequestorNo", SqlDbType.VarChar);
                    oParam.Value = Filter.RequestorNo;
                    oCommand.Parameters.Add(oParam);
                }
            }
            else
            {
                if (Filter.RequestorNo == "")
                {
                    if (Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID where B.RequestTypeID = @RequestTypeID order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID where B.RequestTypeID = @RequestTypeID order by A.LastActionDate desc", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID order by A.LastActionDate desc", oConnection);
                        }

                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }
                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@requesttypeid", SqlDbType.Int);
                    oParam.Value = Filter.RequestTypeID;
                    oCommand.Parameters.Add(oParam);
                }
                else
                {
                    if (Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID where RequestTypeID = @RequestTypeID and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID where RequestTypeID = @RequestTypeID and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox_MyWork(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                            oCommand = new SqlCommand("select B.*,A.IsOwnerView, A.LastActionDate from GetRequestInBox(@empid,@boxcode) A inner join RequestDocument B on B.RequestID = A.RequestID inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID and A.RequestorNo = @RequestorNo order by A.LastActionDate desc", oConnection);
                        }
                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }
                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@requesttypeid", SqlDbType.Int);
                    oParam.Value = Filter.RequestTypeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@RequestorNo", SqlDbType.VarChar);
                    oParam.Value = Filter.RequestorNo;
                    oCommand.Parameters.Add(oParam);
                }
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            DataTable oTable1;
            List<EmployeeData> oDelegate = new List<EmployeeData>();
            if (oBox.IsCanDelegate)
            {
                oDelegate = WorkflowPrinciple.Current.UserSetting.Employee.GetDelegatePersons();
            }
            oDelegate.Insert(0, WorkflowPrinciple.Current.UserSetting.Employee);
            List<string> loadList = new List<string>();
            oConnection.Open();
            int nStartIndex = ItemPerPage * (Page - 1);
            int nMaxRecord = ItemPerPage;
            foreach (EmployeeData empDelegate in oDelegate)
            {
                if (loadList.Contains(empDelegate.EmployeeID))
                {
                    continue;
                }
                loadList.Add(empDelegate.EmployeeID);
                if (Filter != null && Filter.DelegateView != "" && Filter.DelegateView != empDelegate.EmployeeID)
                {
                    continue;
                }
                oParamEmpID.Value = empDelegate.EmployeeID;
                if (ItemPerPage > 0 && Page > 0)
                {
                    oAdapter.Fill(nStartIndex, nMaxRecord, oTable);
                }
                else
                {
                    oAdapter.Fill(oTable);
                }
                if (oTable.Rows.Count >= ItemPerPage)
                {
                    break;
                }
                nStartIndex = 0;
                nMaxRecord = ItemPerPage - oTable.Rows.Count;
            }
            oConnection.Close();

            //Created By: Ratchatawan w. (2012-04-24)
            //Show request's document that this user have permission to delegate
            if (oBox.Code == "DELEGATE")
                oTable = GetRequestInDelegateBox(Filter);

            int __reqid;
            foreach (DataRow dr in oTable.Rows)
            {
                __reqid = (int)dr["RequestID"];
                oItem = new RequestDocument();
                this.ParseToRequestDocument(dr, oItem);
                oReturn.Add(oItem);
                if (__reqid > -1)
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From RequestFlowReceipient where RequestID = @ReqID and ItemID = @CurrentItemID";
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = __reqid;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@CurrentItemID", SqlDbType.Int);
                    oParam.Value = oItem.CurrentItemID;
                    oCommand.Parameters.Add(oParam);

                    oTable1 = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable1);
                    oConnection.Close();

                    foreach (DataRow dr1 in oTable1.Rows)
                    {
                        oItem.Receipients.Add(this.ParseToReceipient(dr1));
                    }

                    oTable1.Dispose();
                }
            }
            oTable.Dispose();

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        #endregion " GetRequestDocumentList "

        #region " CountItemInbox "

        public override int CountItemInbox(string BoxCode, RequestBoxFilter Filter)
        {
            string empid = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            int oReturn = 0;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter oParam;
            SqlParameter oParamEmpID;
            RequestBox oBox = GetRequestBox(BoxCode);

            if (Filter == null || Filter.RequestTypeID == -1)
            {
                if (Filter == null || Filter.RequestorNo == "")
                {
                    if (Filter == null || Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode)", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode)", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup ", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup ", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup ", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup ", oConnection);
                        }
                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }

                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);
                }
                else
                {
                    if (Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) where RequestorNo = @RequestorNo", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) where RequestorNo = @RequestorNo", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestorNo = @RequestorNo", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestorNo = @RequestorNo", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestorNo = @RequestorNo", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestorNo = @RequestorNo", oConnection);
                        }
                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }
                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@RequestorNo", SqlDbType.VarChar);
                    oParam.Value = Filter.RequestorNo;
                    oCommand.Parameters.Add(oParam);
                }
            }
            else
            {
                if (Filter.RequestorNo == "")
                {
                    if (Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) where RequestTypeID = @RequestTypeID ", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) where RequestTypeID = @RequestTypeID ", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID ", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID ", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID ", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID ", oConnection);
                        }
                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }
                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@requesttypeid", SqlDbType.Int);
                    oParam.Value = Filter.RequestTypeID;
                    oCommand.Parameters.Add(oParam);
                }
                else
                {
                    if (Filter.AdminGroup == null || Filter.AdminGroup == "")
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) where RequestTypeID = @RequestTypeID And RequestorNo = @RequestorNo", oConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) where RequestTypeID = @RequestTypeID And RequestorNo = @RequestorNo", oConnection);
                        }
                    }
                    else
                    {
                        if (oBox.IsDataOwnerView && !oBox.IsOwnerView)
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID And B.RequestorNo = @RequestorNo", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount_MyWork(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID And B.RequestorNo = @RequestorNo", oConnection);
                        }
                        else
                        {
                            //oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_current_infotype0001 C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID And B.RequestorNo = @RequestorNo", oConnection);
                            oCommand = new SqlCommand("select Count(RequestID) from dbo.GetRequestInBoxForCount(@empid,@boxcode) as B inner join v_PA_Assignment_current C on C.EmployeeID = B.RequestorNo where C.AdminGroup = @AdminGroup and B.RequestTypeID = @RequestTypeID And B.RequestorNo = @RequestorNo", oConnection);
                        }
                        oParam = new SqlParameter("@AdminGroup", SqlDbType.VarChar);
                        oParam.Value = Filter.AdminGroup;
                        oCommand.Parameters.Add(oParam);
                    }
                    oParamEmpID = new SqlParameter("@empid", SqlDbType.VarChar);
                    oParamEmpID.Value = empid;
                    oCommand.Parameters.Add(oParamEmpID);

                    oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
                    oParam.Value = BoxCode;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@requesttypeid", SqlDbType.Int);
                    oParam.Value = Filter.RequestTypeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@RequestorNo", SqlDbType.VarChar);
                    oParam.Value = Filter.RequestorNo;
                    oCommand.Parameters.Add(oParam);
                }
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            List<EmployeeData> oDelegate = new List<EmployeeData>();
            if (oBox.IsCanDelegate)
            {
                oDelegate = WorkflowPrinciple.Current.UserSetting.Employee.GetDelegatePersons();
            }
            oDelegate.Insert(0, WorkflowPrinciple.Current.UserSetting.Employee);
            List<string> loadList = new List<string>();
            foreach (EmployeeData empDelegate in oDelegate)
            {
                if (loadList.Contains(empDelegate.EmployeeID))
                {
                    continue;
                }
                loadList.Add(empDelegate.EmployeeID);
                if (Filter != null && Filter.DelegateView != "" && Filter.DelegateView != empDelegate.EmployeeID)
                {
                    continue;
                }
                oParamEmpID.Value = empDelegate.EmployeeID;
                oConnection.Open();
                oReturn += (int)oCommand.ExecuteScalar();
                oConnection.Close();
            }

            oConnection.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        #endregion " CountItemInbox "

        #region " GetAuthorizeBoxes "

        public override List<RequestBox> GetAuthorizeBoxes(string BoxCategoryCode)
        {
            List<RequestBox> oReturn = new List<RequestBox>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetAuthorizeBox(@ID,@CategoryCode)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CategoryCode", SqlDbType.VarChar);
            oParam.Value = BoxCategoryCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTBOX");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestBox(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<RequestBox> GetAuthorizeBoxes(string BoxCategoryCode, string EmployeeID)
        {
            List<RequestBox> oReturn = new List<RequestBox>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //ModifiedBy: Ratchatawan W. (2011-01-05)
            //SqlCommand oCommand = new SqlCommand("select * from GetAuthorizeBox(@ID,@CategoryCode) order by OrderID", oConnection);

            SqlCommand oCommand = new SqlCommand("sp_AuthorizeBoxGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            //oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            //oParam = new SqlParameter("@CategoryCode", SqlDbType.VarChar);
            oParam = new SqlParameter("@p_BoxCategoryCode", SqlDbType.VarChar);
            oParam.Value = BoxCategoryCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTBOX");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestBox(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizeBoxes "

        #region " GetAvailableRequestType "

        public override List<RequestType> GetAvailableRequestType(string BoxCode)
        {
            List<RequestType> oReturn = new List<RequestType>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetAvailableRequestType(@boxcode)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@boxcode", SqlDbType.VarChar);
            oParam.Value = BoxCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(this.ParseToRequestType(dr));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAvailableRequestType "

        #region " GetRequestBox "

        public override RequestBox GetRequestBox(string BoxCode)
        {
            RequestBox oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from Box where BoxCode = @BoxCode", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@BoxCode", SqlDbType.VarChar);
            oParam.Value = BoxCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTBOX");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = ParseToRequestBox(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetRequestBox "

        #endregion " Box "

        #region " AuthorizationModel "

        #region " GetAllAuthorizationModel "

        public override List<AuthorizationModel> GetAllAuthorizationModel()
        {
            if (!WorkflowPrinciple.Current.IsInRole("APPDEVELOPER"))
            {
                throw new NoAuthorizeException("GetAllAuthorizationModel");
            }
            List<AuthorizationModel> oReturn = new List<AuthorizationModel>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from AuthorizeModel", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AUTHMODEL");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                AuthorizationModel oModel = new AuthorizationModel();// AuthorizationModel.CreateInstance(CompanyCode);
                oModel.ParseToObject(dr);
                oReturn.Add(oModel);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllAuthorizationModel "

        #region " GetAuthorizationModelKey "

        public override List<AuthorizationModelKey> GetAuthorizationModelKey(int ModelID)
        {
            List<AuthorizationModelKey> oReturn = new List<AuthorizationModelKey>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from AuthorizeModelKey where AuthModelID = @ID", oConnection);
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ModelID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AUTHMODELKEY");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                AuthorizationModelKey oModel = new AuthorizationModelKey();
                oModel.ParseToObject(dr);
                oReturn.Add(oModel);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizationModelKey "

        #region " GetAuthorizationModelSet "

        public override List<AuthorizationModelSet> GetAuthorizationModelSet(int ModelID)
        {
            List<AuthorizationModelSet> oReturn = new List<AuthorizationModelSet>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from AuthorizeModelSet where AuthModelID = @ID", oConnection);
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ModelID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AUTHMODELSET");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                AuthorizationModelSet oModel = new AuthorizationModelSet();
                oModel.ParseToObject(dr);
                oReturn.Add(oModel);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizationModelSet "

        #region " GetAllAuthorizationModelCondition "

        public override List<AuthorizationModelSetCondition> GetAllAuthorizationModelCondition(int ModelID, int SetID)
        {
            List<AuthorizationModelSetCondition> oReturn = new List<AuthorizationModelSetCondition>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from AuthorizeModelSetCondition where AuthModelID = @ID and AuthModelSetID = @SetID", oConnection);
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ModelID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SetID", SqlDbType.Int);
            oParam.Value = SetID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AUTHMODELVALUE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                AuthorizationModelSetCondition oCondition = new AuthorizationModelSetCondition();
                oCondition.ParseToObject(dr);
                oReturn.Add(oCondition);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAllAuthorizationModelCondition "

        #region " GetAuthorizationModel "

        public override AuthorizationModel GetAuthorizationModel(int ModelID)
        {
            AuthorizationModel oReturn = AuthorizationModel.CreateInstance(CompanyCode);
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from AuthorizeModel Where AuthModelID = @ID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ModelID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AUTHMODEL");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = AuthorizationModel.CreateInstance(CompanyCode);
                oReturn.ParseToObject(dr);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizationModel "

        #region " GetAuthorizationModelStep "

        public override Receipient GetAuthorizationModelStep(int ModelID, int StepID)
        {
            Receipient oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from dbo.GetAuthorizationModelStepReceipient(@ModelID,@StepID)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ModelID", SqlDbType.Int);
            oParam.Value = ModelID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@StepID", SqlDbType.Int);
            oParam.Value = StepID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = new Receipient();//Receipient.CreateInstance(CompanyCode);
                oReturn = this.ParseToReceipient(dr);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetAuthorizationModelStep "

        #region " CheckIsInRole "

        public override bool CheckIsInRole(string EmployeeID, Receipient RCP, string Requestor, DateTime CheckDate)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select dbo.IsInRole( @empid , @type , @code , @requestor , @checkdate ) IsInRole", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@empid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@type", SqlDbType.VarChar);
            oParam.Value = RCP.Type;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@code", SqlDbType.VarChar);
            oParam.Value = RCP.Code;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@requestor", SqlDbType.VarChar);
            oParam.Value = Requestor;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@checkdate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TEMP");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = (bool)dr["IsInRole"];
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " CheckIsInRole "

        #region " CalculateAuthorize "

        public override List<AuthorizationModelSet> CalculateAuthorize(int ModelID, List<AuthorizationModelKey> list, List<object> values)
        {
            if (values.Count < list.Count)
            {
                throw new Exception("KEY and VALUE numbers not match");
            }
            List<AuthorizationModelSet> oReturn = new List<AuthorizationModelSet>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from AuthorizeModelSet A where AuthModelID = @ID", oConnection);
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = ModelID;
            oCommand.Parameters.Add(oParam);
            for (int index = 0; index < list.Count; index++)
            {
                AuthorizationModelKey key = list[index];
                string paramName = string.Format("@Param{0}", index.ToString("00"));
                string valueString1, valueString2;
                switch (key.ValueType)
                {
                    case ParameterType.DECIMAL:
                        valueString1 = "convert(decimal,Value1)";
                        valueString2 = "convert(decimal,Value2)";
                        break;

                    default:
                        valueString1 = "Value1";
                        valueString2 = "Value2";
                        break;
                }
                switch (key.ValueOperator.ToUpper())
                {
                    case "=*":
                        oCommand.CommandText += string.Format(" and exists (select * from authorizeModelSetCondition where AuthModelID = A.AuthModelID and AuthModelSetID = A.AuthModelSetID and AuthConditionID = {0} and ({1} = {2} or '*' = {2}))", index, paramName, valueString1);
                        break;

                    case "<=":
                        oCommand.CommandText += string.Format(" and exists (select * from authorizeModelSetCondition where AuthModelID = A.AuthModelID and AuthModelSetID = A.AuthModelSetID and AuthConditionID = {0} and ({1} <= {2})) ", index, paramName, valueString1);
                        break;

                    case "BW":
                        oCommand.CommandText += string.Format(" and exists (select * from authorizeModelSetCondition where AuthModelID = A.AuthModelID and AuthModelSetID = A.AuthModelSetID and AuthConditionID = {0} and ({1} between {2} and {3})) ", index, paramName, valueString1, valueString2);
                        break;

                    case "IN":
                        oCommand.CommandText += string.Format(" and exists (select * from authorizeModelSetCondition where AuthModelID = A.AuthModelID and AuthModelSetID = A.AuthModelSetID and AuthConditionID = {0} and (charindex({2}+',',{1}) > 0)) ", index, paramName, valueString1, valueString2);
                        break;

                    default:
                        oCommand.CommandText += string.Format(" and exists (select * from authorizeModelSetCondition where AuthModelID = A.AuthModelID and AuthModelSetID = A.AuthModelSetID and AuthConditionID = {0} and ({1} = {2})) ", index, paramName, valueString1);
                        break;
                }

                oParam = new SqlParameter();
                oParam.ParameterName = paramName;
                switch (key.ValueType)
                {
                    case ParameterType.BOOLEAN:
                        oParam.SqlDbType = SqlDbType.Bit;
                        break;

                    case ParameterType.DATETIME:
                        oParam.SqlDbType = SqlDbType.DateTime;
                        break;

                    case ParameterType.DECIMAL:
                        oParam.SqlDbType = SqlDbType.Decimal;
                        break;

                    case ParameterType.INTEGER:
                        oParam.SqlDbType = SqlDbType.Int;
                        break;

                    default:
                        oParam.SqlDbType = SqlDbType.VarChar;
                        break;
                }
                oParam.Value = values[index];
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AUTHMODELSET");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                AuthorizationModelSet oModel = new AuthorizationModelSet();
                oModel.ParseToObject(dr);
                oReturn.Add(oModel);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " CalculateAuthorize "

        #endregion " AuthorizationModel "

        #region " FileAttachment "


        public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream =
                   new System.IO.FileStream(_FileName, System.IO.FileMode.Create,
                                            System.IO.FileAccess.Write);
                // Writes a block of bytes to this stream using data from
                // a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}",
                                  _Exception.ToString());
            }

            // error occured, return false
            return false;
        }

        private void SaveFileAttachment(FileAttachmentSet fileSet, SqlConnection oConnection, SqlTransaction tx)
        {
            string rootPath = WorkflowManagement.CreateInstance(fileSet.CompanyCode).FILE_ROOTPATH;
            string filePath = rootPath + fileSet.CompanyCode + "/" + fileSet.RequestNo+"/";
           // string fileFullPath = "D:/" + filePath+"/";
            bool exists = System.IO.Directory.Exists(filePath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(filePath); 
            }


            SqlCommand oCommand_Set = new SqlCommand("select * from FileSet where FileSetID = @ID", oConnection, tx);
            SqlCommand oCommand = new SqlCommand("select * from FileAttachment where FileSetID = @ID", oConnection, tx);
            SqlCommand commandDeleteFileAttachment = new SqlCommand("Delete from FileAttachment where FileSetID = @ID", oConnection, tx);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = fileSet.FileSetID;
            oCommand_Set.Parameters.Add(oParam);

            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = fileSet.FileSetID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = fileSet.FileSetID;
            commandDeleteFileAttachment.Parameters.Add(oParam);
            commandDeleteFileAttachment.ExecuteNonQuery();

            SqlDataAdapter oAdapter_Set = new SqlDataAdapter(oCommand_Set);
            SqlCommandBuilder oCB_Set = new SqlCommandBuilder(oAdapter_Set);
            DataTable oTable_Set = new DataTable("FILESET");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("ATTACHMENT");

            try
            {
                oAdapter_Set.FillSchema(oTable_Set, SchemaType.Source);
                oAdapter_Set.Fill(oTable_Set);

                DataRow oRow_Set;
                if (oTable_Set.Rows.Count == 0)
                {
                    oRow_Set = oTable_Set.NewRow();
                }
                else
                {
                    oRow_Set = oTable_Set.Rows[0];
                }

                oRow_Set["FileSetID"] = fileSet.FileSetID;
                oRow_Set["OwnerUser"] = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
                if (fileSet.RequestID == -1)
                {
                    oRow_Set["RequestID"] = DBNull.Value;
                }
                else
                {
                    oRow_Set["RequestID"] = fileSet.RequestID;
                }

                if (oRow_Set.RowState == DataRowState.Detached)
                {
                    oTable_Set.Rows.Add(oRow_Set);
                }

                oAdapter_Set.Update(oTable_Set);

                if (fileSet.FileSetID == -1)
                {
                    oCommand_Set.CommandText = "select IsNull(Max(FileSetID),-1) + 1 As FileSetID From FileSet";
                    fileSet.FileSetID = (int)oCommand_Set.ExecuteScalar();
                    oRow_Set["FileSetID"] = fileSet.FileSetID;
                    oAdapter_Set.Update(oTable_Set);
                }

                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);



                List<string> oFilesOld = Directory.GetFiles(filePath).ToList<string>();
                List<string> oDiffFileForDelete = oFilesOld.Where(old => fileSet.Count(newf => newf.FileName.Equals(Path.GetFileName(old))) <= 0).ToList();
                oDiffFileForDelete.ForEach(current =>
                {
                    if (File.Exists(current))
                    {
                        File.Delete(current);
                    }
                });
                

                for (int index = 0; index < fileSet.Count; index++)
                {
                    FileAttachment item = fileSet[index];
                    DataRow oRow = oTable.NewRow();
                    oRow["FileSetID"] = fileSet.FileSetID;
                    oRow["FileID"] = index;

                    //oRow["FileName"] = item.FileName;
                    //oRow["FileType"] = item.FileType;
                    //oRow["FileData"] = "0x00";
                    
                    if (item.Data != null) {
                        //Add By Nipon Supap 2018-01-26 (Support-005) Number Index
                        string nFileName = item.FileName.Substring(0, item.FileName.IndexOf('.'));
                        string nFileType = item.FileName.Substring(item.FileName.IndexOf('.') + 1, item.FileName.Length - item.FileName.IndexOf('.') - 1);
                        string nIdentity = GetPad2SolveStupidThaiFilenameProblemOnAL11(fileSet.RequestNo);
                        item.FileName = nFileName + "-" + nIdentity + "." + nFileType;
                        oRow["FileName"] = item.FileName;
                        oRow["FileType"] = item.FileType;
                        ByteArrayToFile(filePath + item.FileName, item.Data);
                    }
                    else
                    {
                        oRow["FileName"] = item.FileName;
                        oRow["FileType"] = item.FileType;
                    }
                    oRow["FilePath"] = fileSet.CompanyCode + "/" + fileSet.RequestNo;
                    oTable.LoadDataRow(oRow.ItemArray, false);
                }

                oAdapter.Update(oTable);

                oCommand = new SqlCommand("Update RequestDocument set FileSetID = @FileSetID where RequestID = @RequestID", oConnection, tx);
                oParam = new SqlParameter("@FileSetID", SqlDbType.Int);
                oParam.Value = fileSet.FileSetID;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                oParam.Value = fileSet.RequestID;
                oCommand.Parameters.Add(oParam);

                oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new Exception("Save error", ex);
            }
            finally
            {
                oCB.Dispose();
                oAdapter.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }


        private void SaveFileAttachment_BU(FileAttachmentSet fileSet, SqlConnection oConnection, SqlTransaction tx)
        {
            SqlCommand oCommand_Set = new SqlCommand("select * from FileSet where FileSetID = @ID", oConnection, tx);
            SqlCommand oCommand = new SqlCommand("select * from FileAttachment where FileSetID = @ID", oConnection, tx);
            SqlCommand commandDeleteFileAttachment = new SqlCommand("Delete from FileAttachment where FileSetID = @ID", oConnection, tx);
            SqlParameter oParam;
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = fileSet.FileSetID;
            oCommand_Set.Parameters.Add(oParam);

            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = fileSet.FileSetID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = fileSet.FileSetID;
            commandDeleteFileAttachment.Parameters.Add(oParam);
            commandDeleteFileAttachment.ExecuteNonQuery();

            SqlDataAdapter oAdapter_Set = new SqlDataAdapter(oCommand_Set);
            SqlCommandBuilder oCB_Set = new SqlCommandBuilder(oAdapter_Set);
            DataTable oTable_Set = new DataTable("FILESET");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("ATTACHMENT");

            try
            {
                oAdapter_Set.FillSchema(oTable_Set, SchemaType.Source);
                oAdapter_Set.Fill(oTable_Set);

                DataRow oRow_Set;
                if (oTable_Set.Rows.Count == 0)
                {
                    oRow_Set = oTable_Set.NewRow();
                }
                else
                {
                    oRow_Set = oTable_Set.Rows[0];
                }

                oRow_Set["FileSetID"] = fileSet.FileSetID;
                oRow_Set["OwnerUser"] = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
                if (fileSet.RequestID == -1)
                {
                    oRow_Set["RequestID"] = DBNull.Value;
                }
                else
                {
                    oRow_Set["RequestID"] = fileSet.RequestID;
                }

                if (oRow_Set.RowState == DataRowState.Detached)
                {
                    oTable_Set.Rows.Add(oRow_Set);
                }

                oAdapter_Set.Update(oTable_Set);

                if (fileSet.FileSetID == -1)
                {
                    oCommand_Set.CommandText = "select IsNull(Max(FileSetID),-1) + 1 As FileSetID From FileSet";
                    fileSet.FileSetID = (int)oCommand_Set.ExecuteScalar();
                    oRow_Set["FileSetID"] = fileSet.FileSetID;
                    oAdapter_Set.Update(oTable_Set);
                }

                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);


                for (int index = 0; index < fileSet.Count; index++)
                {
                    FileAttachment item = fileSet[index];
                    DataRow oRow = oTable.NewRow();
                    oRow["FileSetID"] = fileSet.FileSetID;
                    oRow["FileID"] = index;
                    oRow["FileName"] = item.FileName;
                    oRow["FileType"] = item.FileType;
                    oRow["FileData"] = item.Data;
                    oTable.LoadDataRow(oRow.ItemArray, false);
                }

                oAdapter.Update(oTable);

                oCommand = new SqlCommand("Update RequestDocument set FileSetID = @FileSetID where RequestID = @RequestID", oConnection, tx);
                oParam = new SqlParameter("@FileSetID", SqlDbType.Int);
                oParam.Value = fileSet.FileSetID;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@RequestID", SqlDbType.Int);
                oParam.Value = fileSet.RequestID;
                oCommand.Parameters.Add(oParam);

                oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new Exception("Save error", ex);
            }
            finally
            {
                oCB.Dispose();
                oAdapter.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }
        public override void SaveFileAttachment(FileAttachmentSet fileSet)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            try
            {
                this.SaveFileAttachment(fileSet, oConnection, tx);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("Save error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
            }
        }

        public override FileAttachmentSet LoadFileAttachment(int FileSetID)
        {
            FileAttachmentSet oReturn = FileAttachmentSet.CreateFileSet();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            oConnection.Open();
            SqlCommand oCommand_Set = new SqlCommand("select * from FileSet where FileSetID = @ID", oConnection);
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FileSetID;
            oCommand_Set.Parameters.Add(oParam);
            SqlDataAdapter oAdapter_Set = new SqlDataAdapter(oCommand_Set);
            DataTable oTable_Set = new DataTable("ATTACHMENTSET");

            SqlCommand oCommand = new SqlCommand("select * from FileAttachment where FileSetID = @ID", oConnection);
            oParam = new SqlParameter("@ID", SqlDbType.Int);
            oParam.Value = FileSetID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ATTACHMENT");

            try
            {
                oAdapter_Set.FillSchema(oTable_Set, SchemaType.Source);
                oAdapter_Set.Fill(oTable_Set);

                if (oTable_Set.Rows.Count > 0)
                {
                    oReturn.FileSetID = FileSetID;
                }

                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                for (int index = 0; index < oTable.Rows.Count; index++)
                {
                    FileAttachment item = new FileAttachment();
                    DataRow oRow = oTable.Rows[index];
                    item.FileSetID = (int)FileSetID;
                    item.FileID = (int)oRow["FileID"];
                    item.FileName = (string)oRow["FileName"];
                    item.FileType = (string)oRow["FileType"];
                    //item.Data = (byte[])oRow["FileData"][FilePath]
                    item.FilePath = oRow["FilePath"].ToString();
                    oReturn.Add(item);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Load error", ex);
            }
            finally
            {
                oConnection.Close();
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
            return oReturn;
        }

        #endregion " FileAttachment "

        #region " GetUserResponse "

        public override List<EmployeeData> GetUserResponse(string Role, EmployeeData emp)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //Modified By: Ratchatawan W. (2012-01-25)
            //SqlCommand oCommand = new SqlCommand("select UserID as EmployeeID from GetUserResponse(@UserRole,@AdminGroup)", oConnection);
            SqlCommand oCommand = new SqlCommand("sp_UserResponseGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_UserRole", SqlDbType.VarChar);
            oParam.Value = Role;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_AdminGroup", SqlDbType.VarChar);
            oParam.Value = emp.OrgAssignment.AdminGroup;
            oCommand.Parameters.Add(oParam);

            //Add By: Ratchatawan W. (2012-01-25)
            oParam = new SqlParameter("@p_Organization", SqlDbType.VarChar);
            oParam.Value = emp.OrgAssignment.OrgUnit;
            oCommand.Parameters.Add(oParam);

            //Add By: Ratchatawan W. (2017-02-14)
            oParam = new SqlParameter("@p_BusinessArea", SqlDbType.VarChar);
            oParam.Value = emp.OrgAssignment.BusinessArea;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERRESPONSE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"],"",(string)dr["CompanyCode"],DateTime.Now));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserResponse "

        #region " GetRequestFlag "

        public override List<RequestFlag> GetRequestFlag(string RequestNo)
        {
            List<RequestFlag> oResult = new List<RequestFlag>();
            oSqlManage["ProcedureName"] = "sp_RequestFlagGet";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestNo"] = RequestNo;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<RequestFlag>(oSqlManage, oParams));
            return oResult;
        }

        #endregion " GetRequestFlag "

        #region " SaveRequestFlag "

        public override void SaveRequestFlag(List<RequestFlag> lstFlag)
        {
            if (lstFlag.Count > 0)
            {
                // Delete
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                oConnection.Open();
                SqlTransaction oTransaction = oConnection.BeginTransaction();
                try
                {
                    SqlCommand oCommand = new SqlCommand("sp_RequestFlagDelete", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_RequestNo", lstFlag[0].RequestNo);
                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();

                    foreach (RequestFlag oFlag in lstFlag)
                    {
                        // Save
                        oCommand = new SqlCommand("sp_RequestFlagSave", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_RequestNo", oFlag.RequestNo);
                        oCommand.Parameters.AddWithValue("@p_FlagID", oFlag.FlagID);
                        oCommand.Parameters.AddWithValue("@p_FlagComment", oFlag.FlagComment);
                        oCommand.ExecuteNonQuery();
                        oCommand.Dispose();
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    throw ex;
                }
                oConnection.Close();
            }
        }

        public override void ClearFlagSuggestionFromAccount(string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_RequestFlagDelete";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = RequestNo;
            oParamRequest["@p_FlagID"] = (int)RequestFlag.WAITFORREVERSE;
            DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);

            oParamRequest["@p_RequestNo"] = RequestNo;
            oParamRequest["@p_FlagID"] = (int)RequestFlag.WAITFORREVERSEANDREJECT;
            DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }

        #endregion " SaveRequestFlag "

        #endregion " Override Methods "

        #region " GetMailTemplate "
        public override string GetMailTemplate(int FlowID, int FlowItemID, int ID, string LanguageCode)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select IsNull(dbo.GetMailText(@FlowID,@FlowItemID,@FlowItemActionID,@Language),'') ", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@FlowID", SqlDbType.VarChar);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@FlowItemID", SqlDbType.VarChar);
            oParam.Value = FlowItemID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@FlowItemActionID", SqlDbType.VarChar);
            oParam.Value = ID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            returnValue = (string)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }
        public override string GetMailTemplateByID(int MailID, string LanguageCode)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select IsNull(dbo.GetMailTextByID(@MailID,@Language),'') ", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@MailID", SqlDbType.VarChar);
            oParam.Value = MailID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            returnValue = (string)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }
        public override string GetMailSubject(int FlowID, int FlowItemID, int ID, string LanguageCode)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlCommand oCommand = new SqlCommand("select IsNull(dbo.GetMailSubject(@FlowID,@FlowItemID,@FlowItemActionID,@Language),'') ", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@FlowID", SqlDbType.VarChar);
            oParam.Value = FlowID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@FlowItemID", SqlDbType.VarChar);
            oParam.Value = FlowItemID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@FlowItemActionID", SqlDbType.VarChar);
            oParam.Value = ID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            returnValue = (string)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }

        #endregion " GetMailTemplate "

        #region " GetMailSubject "

        public override string GetMailSubjectByID(int MailID, string LanguageCode)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select IsNull(dbo.GetMailSubjectByID(@MailID,@Language),'') ", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@MailID", SqlDbType.VarChar);
            oParam.Value = MailID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            returnValue = (string)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }

        #endregion " GetMailSubject "

        public override EmployeeData GetProjectManager(string ProjectRole)
        {
            EmployeeData oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select *  from v_project_structure where ProjectCode = @ProjectRole", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ProjectRole", SqlDbType.VarChar);
            oParam.Value = ProjectRole;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERRESPONSE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = new EmployeeData((string)dr["ProjectManager"]);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<string> GetAllAdminGroup()
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //SqlCommand oCommand = new SqlCommand("select distinct(admingroup) AdminGroup from v_current_infotype0001 order by adminGroup", oConnection);
            SqlCommand oCommand = new SqlCommand("select distinct(admingroup) AdminGroup from v_PA_Assignment_current order by adminGroup", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ADMINGROUP");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add((string)dr["AdminGroup"]);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override FileAttachmentSet LoadMasterFileAttachment(EmployeeData employeeData, int requestTypeID, string subType)
        {
            FileAttachmentSet oReturn = FileAttachmentSet.CreateFileSet();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            oConnection.Open();
            SqlCommand oCommand = new SqlCommand("SELECT FileAttachment.*  FROM FileAttachment " +
                                                "inner join FileSet on FileAttachment.FileSetID = FileSet.FileSetID " +
                                                "where FileSet.RequestID = @RequestID and FileSet.OwnerUser = @OwnerUser", oConnection);
            oParam = new SqlParameter("@RequestID", SqlDbType.Int);
            oParam.Value = requestTypeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@OwnerUser", SqlDbType.VarChar);
            oParam.Value = employeeData.EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ATTACHMENT");

            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                for (int index = 0; index < oTable.Rows.Count; index++)
                {
                    FileAttachment item = new FileAttachment();
                    DataRow oRow = oTable.Rows[index];
                    item.FileID = (int)oRow["FileID"];
                    item.FileName = (string)oRow["FileName"];
                    item.FileType = (string)oRow["FileType"];
                    item.Data = (byte[])oRow["FileData"];
                    oReturn.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Load error", ex);
            }
            finally
            {
                oConnection.Close();
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
            return oReturn;
        }

        #region " GetResponseCode "

        public override List<string> GetResponseCode(string Role, EmployeeData emp)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from UserRoleResponseSnapShot where UserID = @EmpID and UserRole = @Role and @Now between BeginDate and EndDate", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = emp.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Role", SqlDbType.VarChar);
            oParam.Value = Role;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Now", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLERESPONSESNAPSHOT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add((string)dr["ResponseCode"]);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<string> GetResponseCodeByOrganizationOnly(string Role, EmployeeData Emp, string RootOrg)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from UserRoleResponseSnapShot where UserID = @EmpID and UserRole = @Role and @Now between BeginDate and EndDate AND ResponseType='O'", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = Emp.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Role", SqlDbType.VarChar);
            oParam.Value = Role;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Now", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLERESPONSESNAPSHOT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            string ResponseCode = string.Empty;
            foreach (DataRow dr in oTable.Rows)
            {
                ResponseCode = (string)dr["ResponseCode"] == "*" ? RootOrg : (string)dr["ResponseCode"];
                if (!oReturn.Exists(delegate(string s) { return s == ResponseCode; }))
                    oReturn.Add(ResponseCode);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetResponseCode "

        #region "Delegate"

        public override List<DelegateHeader> GetDelegateDataByCriteria(string EmployeeID, int Month, int Year)
        {
            List<DelegateHeader> oReturn = new List<DelegateHeader>();
            DataTable oHeader = new DataTable();
            DataTable oDetail = new DataTable();

            oSqlManage["ProcedureName"] = "sp_DelegateDataGetByMonth";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_EmployeeID"] = EmployeeID;
            oParams["@p_Month"] = Month;
            oParams["@p_Year"] = Year;
            oHeader = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);

            Dictionary<string, Object> oAllEmployeePossibleToDelegated = new Dictionary<string, object>();
            EmployeeData emp;
            DelegateHeader header;
            foreach (DataRow dr in oHeader.Rows)
            {
                oAllEmployeePossibleToDelegated = new Dictionary<string, object>();
                header = new DelegateHeader();
                header.ParseToObject(dr);
                emp = new EmployeeData(header.DelegateFrom, header.DelegateFromPosition, header.DelegateFromCompanyCode, DateTime.Now);
                header.DelegateFromName = emp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                header.DelegateFromPositionName = emp.CurrentPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                header.DelegateFromOrg = emp.CurrentOrganization.ObjectID;
                header.DelegateFromOrgName = emp.CurrentOrganization.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                foreach (OM_Organization info10 in emp.GetAllPositions(WorkflowPrinciple.Current.UserSetting.Language))
                    oAllEmployeePossibleToDelegated.Add(info10.ObjectID, GetDelegateToByEmployeePosition(header.DelegateFrom, info10.ObjectID));
                header.AllEmployeePossibleToDelegated = oAllEmployeePossibleToDelegated;

                emp = new EmployeeData(header.DelegateTo, header.DelegateToPosition, header.DelegateToCompanyCode, DateTime.Now);
                header.DelegateToName = emp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                header.DelegateToPositionName = emp.CurrentPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                header.DelegateToOrg = emp.CurrentOrganization.ObjectID;
                header.DelegateToOrgName = emp.CurrentOrganization.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                header.ImageUrl = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(header.DelegateToCompanyCode).PHOTO_PATH, header.DelegateTo.Substring(2));

                emp = new EmployeeData(header.CreatorNo, DateTime.Now);
                header.CreatorName = emp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                header.DetailList = new List<DelegateDetail>();
                
                oSqlManage["ProcedureName"] = "sp_DelegateDetailGetByID";
                oParams.Clear();
                oParams["@p_DelegateID"] = header.DelegateID;
                oDetail = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
                foreach (DataRow drDetail in oDetail.Rows)
                {
                    DelegateDetail delDetail = new DelegateDetail();
                    delDetail.ParseToObject(drDetail);
                    delDetail.RequestTypeName = PortalEngineManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetTextDescription("REQUESTTYPE", WorkflowPrinciple.Current.UserSetting.Language, drDetail["RequestTypeCode"].ToString());
                    header.DetailList.Add(delDetail);
                }

                oReturn.Add(header);
            }

            return oReturn;
        }

        public override List<EmployeeData> GetDelegateToByEmployeePosition(string EmployeeID, string PositionID)
        {
            List<EmployeeData> EmployeeDataList = new List<EmployeeData>();
            DataTable oResult = new DataTable();
            oSqlManage["ProcedureName"] = "sp_DelegateGetEmployeeOMByPosition";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_EmployeeID"] = EmployeeID;
            oParams["@p_PositionID"] = PositionID;
            oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);

            foreach (DataRow dr in oResult.Rows)
            {
                EmployeeData emp = EmployeeManagement.CreateInstance(this.CompanyCode).GetEmployeeDataByPosition(dr["ReceipientCode"].ToString(), DateTime.Now, WorkflowPrinciple.Current.UserSetting.Language);
                EmployeeDataList.Add(emp);
            }

            return EmployeeDataList;
        }

        // AddBY: Ratchatawan W. (2011-08-23)
        public override bool CheckDelegateAuthorize(int RequestID)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand delCommand = new SqlCommand("sp_CheckAuthorizeRequestInDelegateBox", oConnection);
            delCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;

            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            delCommand.Parameters.Add(oParam);

            //oParam = new SqlParameter("@EmployeePositionID", SqlDbType.VarChar);
            //oParam.Value = WorkflowPrinciple.CurrentIdentity.CurrentPosition;
            //delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = string.Empty;
            delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@RequestID", SqlDbType.Int);
            oParam.Value = RequestID;
            delCommand.Parameters.Add(oParam);

            SqlDataAdapter da = new SqlDataAdapter(delCommand);
            DataTable dt = new DataTable();
            oConnection.Open();
            da.Fill(dt);
            oConnection.Close();
            return ((int)dt.Rows.Count > 0);
        }

        public override bool CheckDelegateAuthorize(string RequestNo)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand delCommand = new SqlCommand("sp_CheckAuthorizeRequestInDelegateBox", oConnection);
            delCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;

            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            delCommand.Parameters.Add(oParam);

            //oParam = new SqlParameter("@EmployeePositionID", SqlDbType.VarChar);
            //oParam.Value = WorkflowPrinciple.CurrentIdentity.CurrentPosition;
            //delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@RequestID", SqlDbType.Int);
            oParam.Value = -1;
            delCommand.Parameters.Add(oParam);

            //oParam = new SqlParameter("@IsAllRequestType", SqlDbType.Bit);
            //oParam.Value = true;
            //delCommand.Parameters.Add(oParam);

            //oParam = new SqlParameter("@IsAllRequestID", SqlDbType.Bit);
            //oParam.Value = false;
            //delCommand.Parameters.Add(oParam);

            oConnection.Open();

            return ((int)delCommand.ExecuteNonQuery() > 0);
        }

        // AddBY: Ratchatawan W. (2011-08-23)
        public override DataTable GetRequestInDelegateBox(RequestBoxFilter filter)
        {
            DataTable dt = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetRequestInDelegateBox", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            oCommand.Parameters.Add(oParam);

            if (filter != null)
            {
                oParam = new SqlParameter("@FilterRequestTypeID", SqlDbType.Int);
                oParam.Value = filter.RequestTypeID;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@FilterRequestorNo", SqlDbType.VarChar);
                oParam.Value = filter.RequestorNo;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@FilterAdminGroup", SqlDbType.VarChar);
                oParam.Value = filter.AdminGroup;
                oCommand.Parameters.Add(oParam);
            }

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return dt;
        }
        public override bool GetAuthorizeMassApproveForMobile(int BoxID, string EmployeeID, string EmployeePositionID)
        {

            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AuthorizeMassApproveGetByBoxID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EmployeePositionID", SqlDbType.VarChar);
            oParam.Value = EmployeePositionID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BoxID", SqlDbType.Int);
            oParam.Value = BoxID;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            oReturn = Convert.ToInt32(oCommand.ExecuteScalar()) > 0;
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oReturn;
        }
        #endregion "Delegate"

        //AddBy: Ratchatawan W. (2012-04-30)
        public override bool GetAuthorizeMassApprove(string BoxCode, string EmployeeID, string EmployeePositionID)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AuthorizeMassApproveGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EmployeePositionID", SqlDbType.VarChar);
            oParam.Value = EmployeePositionID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BoxCode", SqlDbType.VarChar);
            oParam.Value = BoxCode;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            oReturn = Convert.ToInt32(oCommand.ExecuteScalar()) > 0;
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override void CreateTicket(string TicketID, string RequestNo, string TicketFor, string TicketForCompanyCode,bool IsExternalUser)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand();
            SqlParameter oParam;

            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();

            oCommand.Transaction = tx;
            oCommand.Connection = oConnection;

            oCommand.CommandText = "sp_CreateRequestTicket";
            oCommand.CommandType = CommandType.StoredProcedure;
            oParam = new SqlParameter("@p_TicketID", SqlDbType.VarChar);
            oParam.Value = TicketID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_TicketFor", SqlDbType.VarChar);
            oParam.Value = TicketFor;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_TicketForCompanyCode", SqlDbType.VarChar);
            oParam.Value = TicketForCompanyCode;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_IsExternalUser", SqlDbType.Bit);
            oParam.Value = IsExternalUser;
            oCommand.Parameters.Add(oParam);

            try
            {
                oCommand.ExecuteNonQuery();

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw ex;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override void UpdateRequestTypeFileSet(FileAttachmentSet NewFileSet)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            oConnection.Open();
            SqlCommand oCommand = new SqlCommand("Delete From FileAttachment where FileSetID = @FileSetID", oConnection);
            oParam = new SqlParameter("@FileSetID", SqlDbType.Int);
            oParam.Value = NewFileSet.FileSetID;
            oCommand.Parameters.Add(oParam);

            try
            {
                oCommand.ExecuteNonQuery();
                SaveFileAttachment(NewFileSet);
            }
            catch (Exception ex)
            {
                throw new Exception("Load error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }
        public override void RecallUpdateDocument(string RequestNo, string ActionBy, string ActionByPosition)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_TimesheetRecallUpdateDocument", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_ActionBy", SqlDbType.VarChar);
            oParam.Value = ActionBy;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_ActionByPosition", SqlDbType.VarChar);
            oParam.Value = ActionByPosition;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            oCommand.ExecuteNonQuery();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }
        public override int CountRequestBox(int BoxID, string EmployeeID)
        {

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            int result = 0;
            SqlCommand oCommand;
            SqlParameter oParam;
            {
                oCommand = new SqlCommand("sp_GetRequestInBoxCount_ForMobile", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                oParam = new SqlParameter("@BoxID", SqlDbType.Int);
                oParam.Value = BoxID;
                oCommand.Parameters.Add(oParam);
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oParam.Value = EmployeeID;
                oCommand.Parameters.Add(oParam);

                oConnection.Open();
                result = Convert.ToInt32(oCommand.ExecuteScalar());
                oConnection.Close();
            }

            oConnection.Dispose();
            oCommand.Dispose();
            return result;
        }

        private bool CheckIsAuthorizeForMobile(Receipient RC, RequestDocument requestDocument, EmployeeData CurrentEmployee)
        {
            List<EmployeeData> EmployeeList = new List<EmployeeData>();
            EmployeeList.Add(requestDocument.Requestor);
            EmployeeList.Add(CurrentEmployee);
            return CheckIsAuthorize(RC, requestDocument, EmployeeList);
        }

        protected override Receipient CheckAuthorizeForMobile(RequestDocument requestDocument, EmployeeData Requestor, EmployeeData CurrentEmployee)
        {
            Receipient oReturn = null;
            bool lReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select top 1 * from RequestDocument where RequestNo = @RequestNo and KeyMaster = @KeyMaster", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = requestDocument.RequestNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@KeyMaster", SqlDbType.VarChar);
            oParam.Value = requestDocument.KeyMaster;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("REQUESTDOCUMENT");
            try
            {
                oConnection.Open();
                oAdapter.Fill(oTable);
                oConnection.Close();

                lReturn = oTable.Rows.Count > 0;
                int __reqid = -1;
                foreach (DataRow dr in oTable.Rows)
                {
                    __reqid = (int)dr["RequestID"];
                    break;
                }
                if (!lReturn)
                {
                    throw new Exception("KeyMaster incorrect");
                }
                lReturn = false;
                if (requestDocument.IsDataOwnerView)
                {
                    lReturn = true;
                }
                else if (requestDocument.IsOwnerView)
                {
                    if ((CurrentEmployee.EmployeeID == requestDocument.CreatorNo) || (CurrentEmployee.EmployeeID == requestDocument.RequestorNo))
                    {
                        if ((CurrentEmployee.EmployeeID == requestDocument.CreatorNo))
                        {
                            oReturn = new Receipient(ReceipientType.EMPLOYEE, requestDocument.CreatorNo, requestDocument.CreatorCompanyCode, requestDocument.RequestID);
                        }
                        else
                        {
                            oReturn = new Receipient(ReceipientType.EMPLOYEE, requestDocument.RequestorNo, requestDocument.RequestorCompanyCode, requestDocument.RequestID);
                        }
                        lReturn = true;
                    }
                }
                else
                {
                    oCommand.Parameters.Clear();
                    oCommand.CommandText = "Select * From V_RequestReceipient where RequestID = @ReqID ";
                    oParam = new SqlParameter("@ReqID", SqlDbType.Int);
                    oParam.Value = __reqid;
                    oCommand.Parameters.Add(oParam);

                    oTable = new DataTable("REQUESTFLOWRECEIPIENT");
                    oConnection.Open();
                    oAdapter.Fill(oTable);
                    oConnection.Close();

                    bool lCheckDelegate = false;
                    List<EmployeeData> DelegatePersons = CurrentEmployee.GetDelegatePersons();
                    // lCheckDelegate = requestDocument.CurrentFlowItem.StateID == 1 && DelegatePersons.Count > 0;
                    lCheckDelegate = CheckDelegateAuthorize(__reqid, CurrentEmployee);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Receipient RC = this.ParseToReceipient(dr);
                        if (lCheckDelegate)
                        {
                            DelegatePersons.Add(CurrentEmployee);
                            RC.Type = ReceipientType.SPECIAL_DELEGATE;
                            oReturn = RC;
                            lReturn = true;
                            break;
                            //if (CheckIsAuthorize(RC, requestDocument, DelegatePersons))
                            //{
                            //    oReturn = RC;
                            //    lReturn = true;
                            //    break;
                            //}
                        }
                        else
                        {
                            if (CheckIsAuthorizeForMobile(RC, requestDocument, CurrentEmployee))
                            {
                                oReturn = RC;
                                lReturn = true;
                                break;
                            }
                        }
                    }
                    oTable.Dispose();
                }

                if (!lReturn)
                {
                    throw new Exception("No authorize for action this request");
                }
            }
            catch (Exception ex)
            {
                throw new WorkflowException(ex.Message);
            }
            finally
            {
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2016-Mar-15)
        public override DataSet GetRequestDocumentList(int BoxID, EmployeeData oEmpData, string Filter)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetRequestInBox_ForMobile", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@BoxID", SqlDbType.Int);
            oParam.Value = BoxID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = oEmpData.EmployeeID;
            oCommand.Parameters.Add(oParam);
            if (!oEmpData.IsExternalUser)
            {
                oParam = new SqlParameter("@EmployeePositionID", SqlDbType.VarChar);
                oParam.Value = oEmpData.PositionID;
                oCommand.Parameters.Add(oParam);
            }
            oParam = new SqlParameter("@LanguageCode", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.Current.UserSetting.Language;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet oDS = new DataSet("REQUESTDOCUMENT");

            oConnection.Open();
            oAdapter.Fill(oDS);
            oConnection.Close();

            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            // set flag
            DataTable oRequestFlag = oDS.Tables[3];
            foreach(DataRow oDoc in oDS.Tables[0].Rows)
            {
                DataRow[] oRowResult = oRequestFlag.Select(string.Format("RequestNo = '{0}'", oDoc["RequestNo"]));
                List<string> lstFlag = new List<string>();
                foreach( DataRow oRow in oRowResult)
                {
                    lstFlag.Add(oRow["FlagID"].ToString());
                }
                oDoc["FlagList"] = string.Join("/", lstFlag.ToArray());
                oDoc["CompanyShortName"] = ShareDataManagement.GetCompanyByCompanyCode(CompanyCode).Name;
            }
            return oDS;
        }

        public override void RequestTypeSummarySave(int RequestID, int ItemID, string SummaryTextTH, string SummaryTextEN)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestTypeSummarySave", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@RequestID", SqlDbType.Int);
            oParam.Value = RequestID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@ItemID", SqlDbType.Int);
            oParam.Value = ItemID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@SummaryTextTH", SqlDbType.VarChar);
            oParam.Value = SummaryTextTH;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@SummaryTextEN", SqlDbType.VarChar);
            oParam.Value = SummaryTextEN;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();
            oCommand.ExecuteNonQuery();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();
        }

        #region "Delegate"
        // AddBY: Ratchatawan W. (2011-08-23)
        public override bool CheckDelegateAuthorize(int RequestID, EmployeeData TakeActionBy)
        {
             DataTable oResult = new DataTable();
            oSqlManage["ProcedureName"] = "sp_DelegateAuthorizeCheck";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@EmployeeID"] = TakeActionBy.EmployeeID;
            oParams["@EmployeePositionID"] =  TakeActionBy.CurrentPosition.ObjectID;
            oParams["@RequestID"] =  RequestID;
            oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
            
            foreach(DataRow dr in oResult.Rows)
                TakeActionBy.EmpSubGroupForDelegate = Convert.ToDecimal(dr["EmpSubGroup"].ToString());

            return (oResult.Rows.Count > 0);
        }
        // AddBY: Ratchatawan W. (2011-08-23)
        public override DataTable GetRequestInDelegateBox()
        {
            DataTable dt = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand delCommand = new SqlCommand("sp_GetRequestInDelegateBox", oConnection);
            delCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.CurrentIdentity.EmployeeID;
            delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_RequestTypeID", SqlDbType.Int);
            oParam.Value = 0;
            delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_RequestID", SqlDbType.Int);
            oParam.Value = 0;
            delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IsAllRequestType", SqlDbType.Bit);
            oParam.Value = true;
            delCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IsAllRequestID", SqlDbType.Bit);
            oParam.Value = true;
            delCommand.Parameters.Add(oParam);

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(delCommand);
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            delCommand.Dispose();

            return dt;
        }
        private static IDataService GetDataService(string AssemblyName, string ClassName)
        {
            try
            {
                Assembly oAssembly = Assembly.Load(AssemblyName);
                Type oType = oAssembly.GetType(ClassName);
                IDataService oService = (IDataService)Activator.CreateInstance(oType);
                return oService;
            }
            catch (Exception ex)
            {
                throw new Exception("Can't get data service", ex);
            }
        }
        #endregion

        public override List<RequestFlowMultipleReceipient> GetRequestFlowMultipleReceipientList(int RequestID)
        {
            List<RequestFlowMultipleReceipient> oReturn = new List<RequestFlowMultipleReceipient>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestFlowMultipleReceipientListGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            //SqlParameter oParam;

            oCommand.Parameters.AddWithValue("@RequestID", RequestID);         

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();


            foreach (DataRow dr in oTable.Rows)
            {
                RequestFlowMultipleReceipient obj = new RequestFlowMultipleReceipient();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override List<RequestFlowMultipleReceipient_External> GetRequestFlowMultipleReceipient_ExternalList(int RequestID)
        {
            List<RequestFlowMultipleReceipient_External> oReturn = new List<RequestFlowMultipleReceipient_External>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestFlowMultipleReceipient_ExternalListGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            //SqlParameter oParam;

            oCommand.Parameters.AddWithValue("@RequestID", RequestID);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();


            foreach (DataRow dr in oTable.Rows)
            {
                RequestFlowMultipleReceipient_External obj = new RequestFlowMultipleReceipient_External();
                obj.ParseToObject(dr);
                oReturn.Add(obj);
            }
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        //AddBy: Ratchatawan W. (2016-06-19) Get possible action with selector action by criteria
        public override List<FlowItemAction> GetAllPossibleAction(int FlowID, int FlowItemID, RequestDocument Doc,EmployeeData ActionBy)
        {
            List<FlowItemAction> oReturn = new List<FlowItemAction>();
            FlowItemAction oFlowItemAction;
            SqlConnection oConnection = new SqlConnection(this.WORKFLOWSERVICE_CONNSTR);
            SqlConnection oCompanyConnection = new SqlConnection(this.BaseConnStr);
            try
            {
                //Get FlowItemActionSelector
                SqlCommand oCommand = new SqlCommand("sp_FlowItemActionSelectorGet", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Add(new SqlParameter("@FlowID", SqlDbType.Int)).Value = FlowID;
                oCommand.Parameters.Add(new SqlParameter("@FlowItemID", SqlDbType.Int)).Value = FlowItemID;

                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                DataSet ds = new DataSet("FlowItemActionSelector");
                oConnection.Open();
                oCompanyConnection.Open();
                oAdapter.Fill(ds);

                foreach (DataRow drSelector in ds.Tables[0].Rows)
                {
                    string FlowItemActionNextItemCode = "%";
                    bool FlowItemActionVisibility = true;
                    if (!string.IsNullOrEmpty(drSelector["ActionSelector"].ToString()))
                    {
                        //Check Store Exists in Company
                        string StoreName = drSelector["ActionSelector"].ToString();
                        string isExists = string.Format("SELECT COUNT(1) FROM sys.objects WHERE type = 'P' AND name = '{0}'", StoreName);
                        oCommand = new SqlCommand(isExists, oCompanyConnection);
                        oCommand.CommandType = CommandType.Text;
                        if (int.Parse(oCommand.ExecuteScalar().ToString()) > 0)
                        {
                            oCommand = new SqlCommand(drSelector["ActionSelector"].ToString(), oCompanyConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand(drSelector["ActionSelector"].ToString(), oConnection);
                        }


                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.Add(new SqlParameter("@RequestID", SqlDbType.Int)).Value = Doc.RequestID;
                        oCommand.Parameters.Add(new SqlParameter("@ActionBy", SqlDbType.VarChar)).Value = ActionBy.EmployeeID;
                        oCommand.Parameters.Add(new SqlParameter("@ActionByPosition", SqlDbType.VarChar)).Value = ActionBy.PositionID;
                        FlowItemActionNextItemCode = oCommand.ExecuteScalar().ToString();
                    }

                    if (!string.IsNullOrEmpty(drSelector["VisibilityCondition"].ToString()))
                    {
                        //Check Store Exists in Company
                        string StoreName = drSelector["VisibilityCondition"].ToString();
                        string isExists = string.Format("SELECT COUNT(1) FROM sys.objects WHERE type = 'P' AND name = '{0}'", StoreName);
                        oCommand = new SqlCommand(isExists, oCompanyConnection);
                        oCommand.CommandType = CommandType.Text;
                        if (int.Parse(oCommand.ExecuteScalar().ToString()) > 0)
                        {
                            oCommand = new SqlCommand(StoreName, oCompanyConnection);
                        }
                        else
                        {
                            oCommand = new SqlCommand(StoreName, oConnection);
                        }
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.Add(new SqlParameter("@RequestID", SqlDbType.Int)).Value = Doc.RequestID;
                        oCommand.Parameters.Add(new SqlParameter("@ActionBy", SqlDbType.VarChar)).Value = ActionBy.EmployeeID;
                        oCommand.Parameters.Add(new SqlParameter("@ActionByPosition", SqlDbType.VarChar)).Value = ActionBy.PositionID;
                        FlowItemActionVisibility = Convert.ToBoolean(oCommand.ExecuteScalar());
                    }

                    foreach (DataRow drAction in ds.Tables[1].Select(string.Format("FlowItemActionID={0} AND NextItemCode = '{1}'", drSelector["FlowItemActionID"].ToString(), FlowItemActionNextItemCode)))
                    {
                        oFlowItemAction = new FlowItemAction();
                        oFlowItemAction = ParseToFlowItemAction(drAction);
                        oFlowItemAction.IsVisible = FlowItemActionVisibility;
                        oReturn.Add(oFlowItemAction);
                    }
                }

                oConnection.Close();
                ds.Dispose();
                oCommand.Dispose();
                oAdapter.Dispose();
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            oConnection.Dispose();
            return oReturn;
        }
        public override string GetRequestNo(DateTime CreatedDate, string RequestTypePrefix, string ReferRequestNo)
        {
            string oReturn = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestNoGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_CompanyCode", CompanyCode);
            oCommand.Parameters.AddWithValue("@p_CreatedDate", CreatedDate);
            oCommand.Parameters.AddWithValue("@p_RequestTypePrefix", RequestTypePrefix);
            if (!string.IsNullOrEmpty(ReferRequestNo) && ReferRequestNo != "null") { oCommand.Parameters.AddWithValue("@p_ReferRequestNo", ReferRequestNo); }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();


            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = dr["RequestNo"].ToString();
            }
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }
        public override RequestBox GetRequestBox(int iBoxID)
        {
            RequestBox oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_BoxGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_BoxID", iBoxID);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();


            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = this.ParseToRequestBox(dr);
                break;
            }
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            return oReturn;
        }

        //Get other companycode that this employee have role response
        public override DataTable GetUserRoleResponseForOtherCompany(string EmployeeID,string CompanyCode)
        {
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_UserRoleResponseForOtherCompany", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_CompanyCode", CompanyCode);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
  
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            return oTable;
        }

        public override DataTable GetFlowItemActionPreference()
        {
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_FlowItemActionPreferenceGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            return oTable;
        }

        //Get lasted action in requestflow by 'Action Code'
        public override EmployeeData GetLastedActionByActionCode(int RequestID, string ActionCode)
        {
            EmployeeData oReturn = new EmployeeData();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestFlowLastedActionGetByActionCode", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add(new SqlParameter("@p_RequestID", SqlDbType.Int)).Value = RequestID;
            oCommand.Parameters.Add(new SqlParameter("@p_ActionCode", SqlDbType.VarChar)).Value = ActionCode;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn = new EmployeeData(dr["ActionBy"].ToString(), dr["ActionByPosition"].ToString(), dr["ActionByCompanyCode"].ToString(), DateTime.Now);

            return oReturn;
        }
        public override EmployeeData GetLastedActionByActionCode(string RequestNo, string ActionCode)
        {
            EmployeeData oReturn = new EmployeeData();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestFlowLastedActionGetByActionCode", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add(new SqlParameter("@p_RequestNo", SqlDbType.VarChar)).Value = RequestNo;
            oCommand.Parameters.Add(new SqlParameter("@p_ActionCode", SqlDbType.VarChar)).Value = ActionCode;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn = new EmployeeData(dr["ActionBy"].ToString(), dr["ActionByPosition"].ToString(), dr["ActionByCompanyCode"].ToString(), DateTime.Now);

            return oReturn;
        }
        public override List<ResponseLog> GetResponseLog(string oRequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_TR_ResponseLogGet";
            List<ResponseLog> oResult = new List<ResponseLog>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = oRequestNo;
            DataSet ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            if( ds != null && ds.Tables.Count > 0)
            {
                oResult = ds.Tables[0].ToList<ResponseLog>();
                if(oResult != null && oResult.Count>0)
                {
                    oResult.ForEach(current =>
                        {
                            current.ResponseLogDetail = ds.Tables[1].ToList<ResponseLogDetail>().FindAll(all => all.CreatedDate == current.CreatedDate && all.RequestNo == current.RequestNo);
                        });
                }
            }
            return oResult;
        }

        //Add by Nipon Supap 06-06-2017
        public override DataTable GetResponseManualLog(string oRequestNo)
        {
            DataTable oTable = new DataTable();
            try
            {                
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_TR_ResponseManualLogGet", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.AddWithValue("@p_RequestNo", oRequestNo);
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

                oConnection.Open();
                oAdapter.Fill(oTable);
                oConnection.Close();
                oConnection.Dispose();
                oAdapter.Dispose();
                oCommand.Dispose();
            }
            catch(Exception ex)
            {

            }
            return oTable;
        }
        //Add by Nipon Supap 13-09-2017
        public override String PDMSStatusGetForView(string oRequestNo)
        {
            string oReturn = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_TR_PDMSStatusGetForViewer", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestNo", oRequestNo);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();


            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = dr["ReturnStatus"].ToString();
            }
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }


        public override bool CheckStatusPDMSForRejectToRequestor(string oRequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_TR_PDMSCheckStatusForRejectToRequestor";
            DataTable oResult = new DataTable();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = oRequestNo;
            return DatabaseHelper.ExecuteScalarToBoolean(oSqlManage, oParamRequest);
            
        }

        public override EmployeeData GetLastedManagerByRequestNo(string RequestNo)
        {
            EmployeeData oReturn = new EmployeeData();
            oReturn.CompanyCode = this.CompanyCode;
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetLastedManagerByRequestNo", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestNo", RequestNo);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn = oReturn.GetEmployeeByPositionID(dr["ReceipientCode"].ToString());

            return oReturn;
        }

        public override DataTable GetRequestTypeKeySetting(int RequestTypeID)
        {
            oSqlManage["ProcedureName"] = "sp_RequestTypeKeySettingGet";
            DataTable oResult = new DataTable();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestTypeID"] = RequestTypeID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }


        public override List<Receipient> GetAllActionInPass(int RequestID,string EmployeeID)
        {
            List<Receipient> oResult = new List<Receipient>();
            oSqlManage["ProcedureName"] = "sp_AllActionInPassGet";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestID"] = RequestID;
            oParams["@p_EmployeeID"] = EmployeeID;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Receipient>(oSqlManage, oParams));
            return oResult;
        }


        public override DataTable ValidateTicket(string TicketID, string EmployeeID)
        {
            DataTable oResult = new DataTable();
            oSqlManage["ProcedureName"] = "sp_ValidateTicket";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_TicketID"] = TicketID;
            oParams["@p_CurrentEmployeeID"] = EmployeeID;
            oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
            return oResult;
        }

        public override DataTable ValidateTicketForExternalUser(string TicketID, string ExternalUserID)
        {
            DataTable oResult = new DataTable();
            oSqlManage["ProcedureName"] = "sp_ValidateTicketForExternalUser";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_TicketID"] = TicketID;
            oParams["@p_CurrentEmployeeID"] = ExternalUserID;
            oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
            return oResult;
        }

        public override DataTable GetAllRequestType(string LanguageCode)
        {
            DataTable oResult = new DataTable();
            oSqlManage["ProcedureName"] = "sp_RequestTypeGetAll";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_LanguageCode"] = LanguageCode;
            oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
            return oResult;
        }


        public override string SaveDelegateHeader(DelegateHeader header)
        {
            string ErrorMessage = string.Empty;
            try
            {
                oSqlManage["ProcedureName"] = "sp_DelegateDataSave";
                Dictionary<string, object> oParams = new Dictionary<string, object>();
                oParams["@p_RequestNo"] = header.RequestNo;
                oParams["@p_DelegateID"] = header.DelegateID;
                oParams["@p_DelegateFrom"] = header.DelegateFrom;
                oParams["@p_DelegateFromPosition"] = header.DelegateFromPosition;
                oParams["@p_DelegateFromCompanyCode"] = header.DelegateFromCompanyCode;
                oParams["@p_DelegateTo"] = header.DelegateTo;
                oParams["@p_DelegateToPosition"] = header.DelegateToPosition;
                oParams["@p_DelegateToCompanyCode"] = header.DelegateToCompanyCode;
                oParams["@p_BeginDate"] = header.BeginDate;
                oParams["@p_EndDate"] = header.EndDate;
                oParams["@p_Remark"] = header.Remark;
                oParams["@p_CreatorNo"] = header.CreatorNo;
                oParams["@p_CreatorCompanyCode"] = header.CreatorCompanyCode;
                oParams["@p_UpdaterNo"] = header.UpdaterNo;
                oParams["@p_UpdaterCompanyCode"] = header.UpdaterCompanyCode;
                
                string RequestTypeID = "";
                foreach (DelegateDetail detail in header.DetailList.FindAll(obj => obj.IsCheck))
                    RequestTypeID += detail.RequestTypeID.ToString() + ",";
                oParams["@p_RequestTypeID"] = RequestTypeID;

                DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
            }
            catch (Exception ex)
            {
                if (ex.Message == "DUPPLICATE")
                    ErrorMessage = PortalEngineManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetTextDescription("DELEGATE", WorkflowPrinciple.Current.UserSetting.Language, ex.Message);
                else
                    ErrorMessage = ex.Message;
            }
            return ErrorMessage;
        }

        public override void DeleteDelegateHeader(DelegateHeader header)
        {
            oSqlManage["ProcedureName"] = "sp_DelegateDataDelete";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_DelegateID"] = header.DelegateID;
            DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
        }

        public override void SaveRequestFlowForwardSnapshot(List<RequestFlowForwardSnapshot> ForwardFlowList)
        {
            oSqlManage["ProcedureName"] = "sp_RequestFlowForwardSnapshotSave";
            Dictionary<string, object> oParams = new Dictionary<string, object>();

            foreach (RequestFlowForwardSnapshot snap in ForwardFlowList)
            {
                oParams = new Dictionary<string, object>();
                oParams["@p_RowID"] = snap.RowID;
                oParams["@p_RequestID"] = snap.RequestID;
                oParams["@p_ItemID"] = snap.ItemID;
                oParams["@p_ActionCode"] = snap.ActionCode;
                oParams["@p_ReceipientCode"] = snap.ReceipientCode;
                oParams["@p_ReceipientPositon"] = snap.ReceipientPosition;
                oParams["@p_ReceipientCompanyCode"] = snap.ReceipientCompanyCode;
                oParams["@p_ReceipientPositionName"] = snap.ReceipientPositionName;
                oParams["@p_ReceipientName"] = snap.ReceipientName;
                oParams["@p_IsCurrentItem"] = snap.IsCurrentItem;
                oParams["@p_IsEndPoint"] = snap.IsEndPoint;
                oParams["@p_GroupName"] = snap.GroupName;
                oParams["@p_IsSystem"] = snap.IsSystem;
                DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParams);
            }
            
        }

        public override void DeleteRequestFlowForwardSnapshotByRequestID(int RequestID)
        {
            oSqlManage["ProcedureName"] = "sp_RequestFlowForwardSnapshotDelete";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestID"] = RequestID;
            DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
        }

        public override string GetLastedCommentByActionCode(int RequestID, string ActionCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetLastedCommentForActionCode";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestID"] = RequestID;
            oParams["@p_ActionCode"] = ActionCode;
            return DatabaseHelper.ExecuteScalar(oSqlManage, oParams);
        }

        public override string GetEditReasonByRequestNo(string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_GetEditReasonByRequestNo";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestNo"] = RequestNo;
            return DatabaseHelper.ExecuteScalar(oSqlManage, oParams);
        }

        public override string GetLastedActionByEmployeeID(int RequestID, string EmployeeID, string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetLastedActionByEmployeeID";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestID"] = RequestID;
            oParams["@p_EmployeeID"] = EmployeeID;
            oParams["@p_LanguageCode"] = LanguageCode;
            return DatabaseHelper.ExecuteScalar(oSqlManage, oParams);
        }

        public override DataTable GetActionInPassByRequestNo(string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_GetActionInPassByRequestNo";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_RequestNo"] = RequestNo;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParams);
        }

        public override string GetAuthorizeBoxByEmployeeID(string EmployeeID)
        {
            oSqlManage["ProcedureName"] = "sp_AuthorizeBoxGetByEmployeeID";
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_EmployeeID"] = EmployeeID;
            return DatabaseHelper.ExecuteScalar(oSqlManage, oParams);
        }


        public override bool CanApproveCoWorkflow(string RequestNo, string ActionBy, string ActionByPosition)
        {
            oSqlManage["ProcedureName"] = "sp_ApproverGetForCoWorkflow";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = RequestNo;
            oParamRequest["@p_ActionBy"] = ActionBy;
            oParamRequest["@p_ActionByPosition"] = ActionByPosition;
            string oResult = DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest);
            bool oFlgResult = (oResult == string.Empty) ? false : Convert.ToBoolean(oResult);
            return oFlgResult;
        }

        public override void InsertPDMSRequestWaitForReturnToRequestor(string Requestno, string ActionBy, string ActionByCompanyCode)
        {
            oSqlManage["ProcedureName"] = "sp_TR_PDMSRequestWaitForReturnToRequestorInsert";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = Requestno;
            oParamRequest["@p_ActionBy"] = ActionBy;
            oParamRequest["@p_ActionByCompanyCode"] = ActionByCompanyCode;

            DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }

        public override DataSet GetPDMSRequestWaitForReturnToRequestorForRunJob(string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_TR_PDMSRequestWaitForReturnToRequestorGetForRunJob";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(RequestNo))
            {
                oParamRequest["@p_RequestNo"] = RequestNo;
            }
            return DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
        }

        public override void UpdatePDMSRequestWaitForReturnToRequestor(string RequestNo, string Status)
        {
            oSqlManage["ProcedureName"] = "sp_TR_PDMSRequestWaitForReturnToRequestorUpdate";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = RequestNo;
            oParamRequest["@p_Status"] = Status;

            DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }

        public override void ResponseFlagSave(string RequestNo, string TransactionType, string ServiceURL, DateTime TransactionStartDate, DateTime ServiceFinishDate, string ServiceResult, string ServiceException, DateTime DocumentFinishDate, string DocumentException)
        {
            oSqlManage["ProcedureName"] = "sp_TR_ResponseFlagSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = RequestNo;
            oParamRequest["@p_TransactionType"] = TransactionType;
            oParamRequest["@p_ServiceURL"] = ServiceURL;
            oParamRequest["@p_TransactionStartDate"] = TransactionStartDate;
            if (ServiceFinishDate > DateTime.MinValue) { oParamRequest["@p_ServiceFinishDate"] = ServiceFinishDate; }
            oParamRequest["@p_ServiceResult"] = ServiceResult;
            oParamRequest["@p_ServiceException"] = ServiceException;
            if (DocumentFinishDate > DateTime.MinValue) { oParamRequest["@p_DocumentFinishDate"] = DocumentFinishDate; }
            if (!string.IsNullOrEmpty(DocumentException)) { oParamRequest["@p_DocumentException"] = DocumentException; }
            DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }

        public override bool CheckResponseFlag(string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_TR_CheckResponseFlag";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestNo"] = RequestNo;
            return DatabaseHelper.ExecuteScalarToBoolean(oSqlManage, oParamRequest);
        }

        public override DataSet GetPositionApprove(int BoxID,string EmployeeID,string PositionID,string Language)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetRequestInBox_ForMobile", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@BoxID", SqlDbType.Int);
            oParam.Value = BoxID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmployeePositionID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@LanguageCode", SqlDbType.VarChar);
            oParam.Value = WorkflowPrinciple.Current.UserSetting.Language;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet oDS = new DataSet("POSITION");

            oConnection.Open();
            oAdapter.Fill(oDS);
            oConnection.Close();

            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            return oDS;
        }


        public string GetPad2SolveStupidThaiFilenameProblemOnAL11(string RequestNo)
        {
            string nNumber = "";
            try
            {
                oSqlManage["ProcedureName"] = "sp_TR_GetPad2SolveStupidThaiFilenameProblemOnAL11";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_RequestNo"] = RequestNo;
                nNumber = DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {

            }
            return nNumber;
        }

        public override DataTable GetEmployeeByUserRole(int RequestID, string UserRole, string Language)
        {
            oSqlManage["ProcedureName"] = "sp_UserRoleResponseGetByRequestNo";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_RequestID"] = RequestID;
            oParamRequest["@p_UserRole"] = UserRole;
            oParamRequest["@p_Language"] = Language;
            return DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);
        }
    }
}