﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.CONFIG
{
    public class UserRoleResponseContent
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string UserRole { get; set; }
        public string RoleDesc { get; set; }
        public string ResponseType { get; set; }
        public string ResponseDesc { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseCodeDesc { get; set; }
        public string ResponseCompanyCode { get; set; }
        public string ResponseCompanyDesc { get; set; }
        public bool includeSub { get; set; }
    }
}
