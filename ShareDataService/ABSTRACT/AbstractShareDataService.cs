﻿using System;
using System.Collections.Generic;
using ESS.SHAREDATASERVICE.DATACLASS;
using ESS.SHAREDATASERVICE.INTERFACE;
using ESS.SHAREDATASERVICE.CONFIG;
using System.Data;

namespace ESS.SHAREDATASERVICE.ABSTRACT
{
    public abstract class AbstractShareDataService : IShareDataService
    {
        public AbstractShareDataService()
        {
        }

        #region IShareDataService Members

        public virtual string GetUserConfigurationByCompany(string oCompanycod)
        {
            throw new NotImplementedException();
        }

        public virtual List<Company> GetCompanyList()
        {
            throw new NotImplementedException();
        }
        public virtual Dictionary<string, string> GetModuleSettings(string oCompanyCode, string oClassName)
        {
            throw new NotImplementedException();
        }
        public virtual Dictionary<string, Dictionary<string, Dictionary<string, string>>> GetModuleSettings()
        {
            throw new NotImplementedException();
        }
        public virtual Company GetCompanyByCompanyCode(string sCompanyCode)
        {
            throw new NotImplementedException();
        }
        public virtual void CopyUserRoleAndResponse(string sCompanyCode)
        {
            throw new NotImplementedException();
        }
        public virtual Dictionary<string, string> GetDatabaseLocation()
        {
            throw new NotImplementedException();
        }

        public virtual System.Data.DataSet GetTextDescriptionByCompany(string CompanyCode, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<UserRoleClass> GetAllUserRole()
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetAlluserRoleEditor()
        {
            throw new NotImplementedException();
        }

        public virtual List<RoleClass> GetAllRole()
        {
            throw new NotImplementedException();
        }

        public virtual List<UserRoleResponseType> GetAllUserRoleResponseType(string oLanguage)
        {
            throw new NotImplementedException();
        }

        public virtual List<UserRoleResponse> GetUserRoleResponse(string EmployeeID, string CompanyCode)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveUserRoleResponse(List<UserRoleResponse> lstUserRoleResponse)
        {
            throw new NotImplementedException();
        }
        public virtual string MaintainAdminBypass(string Username, string Password, string Email, string Firstlogin, string ResponseID, string CompanyCodes,string CompanyName,string CurrentCompany)
        {
            throw new NotImplementedException();
        }
        public virtual string AdminBypassLogin(string Username, string Password , string Flag,string Company)
        {
            throw new NotImplementedException();
        }
        public virtual int AdminBypassCompany()
        {
            throw new NotImplementedException();
        }
        public virtual string AdminBypassResetPassword(string Username, string Flag,string Company,string CompanyName)
        {
            throw new NotImplementedException();
        }
        public virtual void AdminBypassSaveLog(string Username, string EmployeeCode,string CompanyCode,string Description, string IPAddress, string ClientName )
        {
            throw new NotImplementedException();
        }
        public virtual List<BypassCompany> GetBypassCompanyList()
        {
            throw new NotImplementedException();
        }

        public virtual object GetFlowIDByRequestTypeIDKeyCode(string RequestTypeID, string KeyCode)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}