﻿using System;
using System.Reflection;
using ESS.SHAREDATASERVICE.INTERFACE;

namespace ESS.SHAREDATASERVICE
{
    public class ServiceManager
    {
        #region Constructor

        public ServiceManager()
        {
        }

        #endregion Constructor

        private static Type GetService(string Mode)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.SHAREDATASERVICE.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.SHAREDATASERVICE.{0}.ShareDataServiceImpl", Mode.ToUpper());
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            return oReturn;
        }

        private static Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.SHAREDATASERVICE.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.SHAREDATASERVICE.{0}.{1}", Mode.ToUpper(), ClassName); //CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        public static IShareDataService DataService
        {
            get
            {
                IShareDataService service;
                Type oType = GetService("DB");
                service = (IShareDataService)Activator.CreateInstance(oType);
                return service;
            }
        }
    }
}