﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.EXCEPTION;
using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.HR.PA.DHR
{
    public class HRPAConfigServiceImpl : AbstractHRPAConfigService
    {
        #region Constructor
        public HRPAConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }
        #endregion

        #region Member
        private static string ModuleID = "ESS.HR.PA.DHR";
        private static string ModuleID_DB = "ESS.HR.PA.DB";
        private string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private string BaseConnStr_DB
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID_DB, "BASECONNSTR");
            }
        }

        public override List<PAConfiguration> GetPAConfigurationList()
        {
            throw new NotImplementedException();
        }

        public override List<TitleName> GetTitleList(string LanguageCode)
        {
            List<TitleName> oReturn = new List<TitleName>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr_DB))
            {
                DataTable oTable = new DataTable("TitleName");
                SqlCommand oCommand = null;
              
                try
                {
                    oCommand = new SqlCommand("sp_DHR_PA_Config", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_dataCode", "INAME_CODE");

                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    
                    oConnection.Open();
                    oAdapter.Fill(oTable);
                    oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                    foreach (DataRow dr in oTable.Rows)
                    {
                        TitleName item = new TitleName();
                        item.Key = dr["valueCode"].ToString();
                        item.Description = dr["valueName"].ToString();
                        oReturn.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    throw new DataServiceException("HRPA", "TitleName Error", ex.Message);
                }
                finally
                {
                    oTable.Dispose();
                    oConnection.Close();
                    oConnection.Dispose();
                }
                
            }

            return oReturn;
        }


        public TitleName GetTitle(string Code)
        {
            throw new NotImplementedException();
        }

        public TitleName GetTitle(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public void SaveTitleList(List<TitleName> data)
        {
            throw new NotImplementedException();
        }

        public List<PrefixName> GetPrefixNameList()
        {
            throw new NotImplementedException();
        }

        public PrefixName GetPrefixName(string Code)
        {
            throw new NotImplementedException();
        }

        public void SavePrefixNameList(List<PrefixName> data)
        {
            throw new NotImplementedException();
        }

        public List<NameFormat> GetNameFormatList()
        {
            throw new NotImplementedException();
        }

        public NameFormat GetNameFormat(string Code)
        {
            throw new NotImplementedException();
        }

        public void SaveNameFormatList(List<NameFormat> data)
        {
            throw new NotImplementedException();
        }

        public List<Gender> GetGenderList(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public List<Gender> GetGenderList()
        {
            throw new NotImplementedException();
        }

        public Gender GetGender(string Code)
        {
            throw new NotImplementedException();
        }

        public Gender GetGender(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public void SaveGenderList(List<Gender> data)
        {
            throw new NotImplementedException();
        }

        public List<Country> GetCountryList()
        {
            throw new NotImplementedException();
        }

        public List<Country> GetCountryList(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public Country GetCountry(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public List<Nationality> GetAllNationality(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public Nationality GetNationality(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public Country GetCountry(string Code)
        {
            throw new NotImplementedException();
        }

        public List<Region> GetRegionByCountryCode(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public void SaveCountryList(List<Country> data)
        {
            throw new NotImplementedException();
        }

        public List<MaritalStatus> GetMaritalStatusList()
        {
            throw new NotImplementedException();
        }

        public List<MaritalStatus> GetMaritalStatusList(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public MaritalStatus GetMaritalStatus(string Code)
        {
            throw new NotImplementedException();
        }

        public MaritalStatus GetMaritalStatus(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public void SaveMaritalStatusList(List<MaritalStatus> Data)
        {
            throw new NotImplementedException();
        }

        public List<Religion> GetReligionList(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public Religion GetReligion(string Code)
        {
            throw new NotImplementedException();
        }

        public Religion GetReligion(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public void SaveReligionList(List<Religion> Data)
        {
            throw new NotImplementedException();
        }

        public List<Language> GetLanguageList()
        {
            throw new NotImplementedException();
        }

        public Language GetLanguage(string Code)
        {
            throw new NotImplementedException();
        }

        public void SaveLanguageList(List<Language> Data)
        {
            throw new NotImplementedException();
        }

        public List<AddressType> GetAddressTypeList()
        {
            throw new NotImplementedException();
        }

        public List<AddressType> GetAddressTypeList(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public void SaveAddressTypeList(List<AddressType> Data)
        {
            throw new NotImplementedException();
        }

        //public List<Bank> GetBankList()
        //{
        //    throw new NotImplementedException();
        //}

        //public Bank GetBank(string Code)
        //{
        //    throw new NotImplementedException();
        //}

        //public void SaveBankList(List<Bank> Data)
        //{
        //    throw new NotImplementedException();
        //}

        public FamilyMember GetFamilyMember(string Code)
        {
            throw new NotImplementedException();
        }

        public void SaveFamilyMemberList(List<FamilyMember> Data)
        {
            throw new NotImplementedException();
        }

        public List<FamilyMember> GetFamilyMemberList()
        {
            throw new NotImplementedException();
        }

        public void SaveProvinceList(List<Province> Data)
        {
            throw new NotImplementedException();
        }

        public List<EducationLevel> GetAllEducationLevel()
        {
            throw new NotImplementedException();
        }

        public void SaveAllEducationLevel(List<EducationLevel> list)
        {
            throw new NotImplementedException();
        }

        public List<Vocation> GetAllVocation()
        {
            throw new NotImplementedException();
        }

        public void SaveAllVocation(List<Vocation> list)
        {
            throw new NotImplementedException();
        }

        public List<Institute> GetAllInstitute()
        {
            throw new NotImplementedException();
        }

        public void SaveAllInstitute(List<Institute> list)
        {
            throw new NotImplementedException();
        }

        public List<Honor> GetAllHonor()
        {
            throw new NotImplementedException();
        }

        public void SaveAllHonor(List<Honor> list)
        {
            throw new NotImplementedException();
        }

        public List<Branch> GetAllBranch()
        {
            throw new NotImplementedException();
        }

        public void SaveAllBranch(List<Branch> list)
        {
            throw new NotImplementedException();
        }

        public List<CompanyBusiness> GetAllCompanyBusiness()
        {
            throw new NotImplementedException();
        }

        public void SaveAllCompanyBusiness(List<CompanyBusiness> list)
        {
            throw new NotImplementedException();
        }

        public List<JobType> GetAllJobType()
        {
            throw new NotImplementedException();
        }

        public void SaveAllJobType(List<JobType> list)
        {
            throw new NotImplementedException();
        }

        public List<Certificate> GetAllCertificate()
        {
            throw new NotImplementedException();
        }

        public void SaveAllCertificate(List<Certificate> list)
        {
            throw new NotImplementedException();
        }

        public Branch GetBranchData(string BranchCode)
        {
            throw new NotImplementedException();
        }

        public EducationLevel GetEducationLevel(string EducationLevelCode)
        {
            throw new NotImplementedException();
        }

        public Certificate GetCertificateCode(string CertificateCode)
        {
            throw new NotImplementedException();
        }

        public Vocation GetVocation(string VocationCode)
        {
            throw new NotImplementedException();
        }

        public CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        {
            throw new NotImplementedException();
        }

        public JobType GetJobType(string JobCode)
        {
            throw new NotImplementedException();
        }

        public Institute GetInstitute(string InstituteCode)
        {
            throw new NotImplementedException();
        }

        public List<EducationGroup> GetAllEducationGroup()
        {
            throw new NotImplementedException();
        }

        public EducationGroup GetEducationGroupByCode(string EducationGroupCode)
        {
            throw new NotImplementedException();
        }

        public List<CourseDurationUnit> GetAllUnit()
        {
            throw new NotImplementedException();
        }

        public List<string> GetCertCodeFromEduLevelCode(string educationLevelCode)
        {
            throw new NotImplementedException();
        }

        public List<string> GetBranchFromEduLevelCode(string educationLevelCode)
        {
            throw new NotImplementedException();
        }

        public List<Relationship> GetAllRelationship()
        {
            throw new NotImplementedException();
        }

        public void SaveAllRelationship(List<Relationship> list)
        {
            throw new NotImplementedException();
        }

        public List<Province> GetProvinceByCountryCode(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode)
        {
            throw new NotImplementedException();
        }

        public bool ShowTimeAwareLink(int LinkID)
        {
            throw new NotImplementedException();
        }

        
        #endregion
    }
}
