﻿using DHR.HR.API.Model;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ESS.HR.PA
{
    public interface IPAPersonalInf
    {
        PAPersonalInf PAPersonalInf { get; set; }

    }
    public class PersonalInfoCenter : AbstractInfoType, IPAPersonalInf
    {
        public string CompanyCode { get; set; }
        public string TitleID { get; set; }

        public string TitleIDEn { get; set; }
        public string Prefix { get; set; }
        public string MilitaryTitle { get; set; }
        public string SecondTitle { get; set; }
        public string AcademicTitle { get; set; }
        public string MedicalTitle { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Initial { get; set; }
        public string FullName { get; set; }
        public string FullNameEN { get; set; }
        public string NickName { get; set; }
        public string NameFormat { get; set; }
        public string InameEnValue { get; set; }
        public string FirstNameEn { get; set; }
        public string MiddleNameEn { get; set; }
        public string LastNameEn { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string BirthPlace { get; set; }
        public string BirthCity { get; set; }
        public string Nationality { get; set; }
        public string RaceValue { get; set; }
        public string Religion { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? MaritalDate { get; set; }
        public DateTime? DivorceDate { get; set; }
        public DateTime? PassAwayDate { get; set; }
        public int? NoOfChild { get; set; }
        public string Language { get; set; }
        public string Remark { get; set; }
        public string SecondNationality { get; set; }
        public string ThirdNationality { get; set; }
        public string ModifyType { get; set; }
        public override string InfoType
        {
            get
            {
                return "0002";
            }
        }

        public string IDCard { get; set; }
        public DateTime? IDCardIssueDate { get; set; }
        public DateTime? IDCardExpiredDate { get; set; }
        public string LicenseID { get; set; }
        public DateTime? LicenceIssueDate { get; set; }
        public DateTime? LicenceExpiredDate { get; set; }
        public string Address { get; set; }

        public string BloodType { get; set; }
        public DateTime? BirthDate { get; set; }
        
        public PersonalInfoCenter()
        {
            CompanyCode = string.Empty;
            TitleID = string.Empty;
            Prefix = string.Empty;
            MilitaryTitle = string.Empty;
            SecondTitle = string.Empty;
            AcademicTitle = string.Empty;
            MedicalTitle = string.Empty;
            FirstName = string.Empty;
            MiddleName = string.Empty;
            LastName = string.Empty;
            Initial = string.Empty;
            FullName = string.Empty;
            NickName = string.Empty;
            NameFormat = string.Empty;
            InameEnValue = string.Empty;
            FirstNameEn = string.Empty;
            MiddleNameEn = string.Empty;
            LastNameEn = string.Empty;
            DOB = default(DateTime);
            Gender = string.Empty;
            BirthPlace = string.Empty;
            BirthCity = string.Empty;
            Nationality = string.Empty;
            RaceValue = string.Empty;
            Religion = string.Empty;
            MaritalStatus = string.Empty;
            MaritalDate = null;
            DivorceDate = null;
            PassAwayDate = null;
            NoOfChild = default(int);
            Language = string.Empty;
            Remark = string.Empty;
            SecondNationality = string.Empty;
            ThirdNationality = string.Empty;
            IDCard = string.Empty;
            LicenseID = string.Empty;
            Address = string.Empty;
            ModifyType = string.Empty;
            BloodType = string.Empty;

            BirthDate = default(DateTime);
        }

        private PAPersonalInf __PAPersonalInf;
        public PAPersonalInf PAPersonalInf
        {
            get { return __PAPersonalInf; }
            set { __PAPersonalInf = value; }
        }
        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> data = new List<IInfoType>();
            //data.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetINFOTYPE0002List(EmployeeID1, EmployeeID2).ToArray());
            return data;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<PersonalInfoCenter> List = new List<PersonalInfoCenter>();
            foreach (IInfoType item in Data)
            {
                List.Add((PersonalInfoCenter)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SavePersonalInfo(EmployeeID1, EmployeeID2, List, Profile);
        }

        public override object LoadData(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetPersonalInfo(EmployeeID, BeginDate);
        }
    }
}

