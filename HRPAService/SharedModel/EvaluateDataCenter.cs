﻿using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA
{
    public class EvaluateDataCenter : AbstractObject
    {

        public EvaluateDataCenter()
        {
            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            StartDate = string.Empty;
            EndDate = string.Empty;
            Period = string.Empty;
            EvaluationTime = string.Empty;
            GradeCode = string.Empty;
            GradeValue = string.Empty;
            GradeTextTH = string.Empty;
            GradeTextEN = string.Empty;
            UpSalaryPercentage = default(decimal);
            VariableBonus = default(decimal);
            Score = default(decimal);
            Average = default(decimal);
            increaseSalary = default(decimal);
            actualBonus = default(decimal);
        }

        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Period { get; set; }
        public string EvaluationTime { get; set; }
        public string GradeCode { get; set; }
        public string GradeValue { get; set; }
        public string GradeTextTH { get; set; }
        public string GradeTextEN { get; set; }
        public decimal? UpSalaryPercentage { get; set; }
        public decimal? VariableBonus { get; set; }
        public decimal? Score { get; set; }
        public decimal? Average { get; set; }
        public decimal? increaseSalary { get; set; }
        public decimal? actualBonus { get; set; }

    }
}

