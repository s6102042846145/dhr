﻿using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA
{
    public class EmployeeDataCenter : AbstractObject
    {

        public EmployeeDataCenter()
        {
            CompanyCode = string.Empty;
            StartDate = string.Empty;
            EndDate = string.Empty;
            EmpCode = string.Empty;
            EmpNameTH = string.Empty;
            EmpNameEN = string.Empty;
            PosCode = string.Empty;
            PositionTextTH = string.Empty;
            PositionTextEN = string.Empty;
            UnitCode = string.Empty;
            UnitTextTh = string.Empty;
            UnitTextEN = string.Empty;

            UnitLevelValue = string.Empty;
            UnitLevelTextTH = string.Empty;
            UnitLevelTextEN = string.Empty;
            BandValue = string.Empty;
            BandTextTH = string.Empty;
            BandTextEN = string.Empty;
        }

        public string CompanyCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string EmpCode { get; set; }
        public string EmpNameTH { get; set; }
        public string EmpNameEN { get; set; }
        public string PosCode { get; set; }
        public string PositionTextTH { get; set; }
        public string PositionTextEN { get; set; }
        public string UnitCode { get; set; }
        public string UnitTextTh { get; set; }
        public string UnitTextEN { get; set; }
        public string UnitLevelValue { get; set; }
        public string UnitLevelTextTH { get; set; }
        public string UnitLevelTextEN { get; set; }
        public string BandValue { get; set; }
        public string BandTextTH { get; set; }
        public string BandTextEN { get; set; }


    }
}

