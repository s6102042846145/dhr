﻿using ESS.UTILITY.EXTENSION;
using System;

namespace ESS.HR.PA
{
    public class PersonalPunishmentCenter : AbstractObject
    {

        public string CompanyCode { get; set; }
        public DateTime Begindate { get; set; }
        public DateTime Enddate { get; set; }
        public string EmployeeID { get; set; }
        public string StatusCode { get; set; }
        public string StatusValue { get; set; }
        public string StartCode { get; set; }
        public string StartValue { get; set; }
        public string EvidenceCode { get; set; }
        public string EvidenceValue { get; set; }
        public string PunishmentCode { get; set; }
        public string PunishmentValue { get; set; }
        public string Detail { get; set; }
        public string CommandNo { get; set; }

        public string EvidenceDesc { get; set; }
        public string PunishmentDesc { get; set; }
        public string StartValueDesc { get; set; }
        public string StatusDesc { get; set; }

        public PersonalPunishmentCenter()
        {
            CompanyCode = string.Empty;
            Begindate = default(DateTime);
            Enddate = default(DateTime);
            EmployeeID = string.Empty;
            StatusCode = string.Empty;
            StatusValue = string.Empty;
            StartCode = string.Empty;
            StartValue = string.Empty;
            EvidenceCode = string.Empty;
            EvidenceValue = string.Empty;
            PunishmentCode = string.Empty;
            PunishmentValue = string.Empty;
            Detail = string.Empty;
            CommandNo = string.Empty;

            EvidenceDesc = string.Empty;
            PunishmentDesc = string.Empty;
            StartValueDesc = string.Empty;
        }

    }
}

