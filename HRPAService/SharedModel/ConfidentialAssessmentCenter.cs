﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.UTILITY.EXTENSION;
using System;

namespace ESS.HR.PA
{
    public class ConfidentialAssessmentCenter : AbstractInfoType
    {
        public string paEvId { get; set; }
        public string companyCode { get; set; }
        public string period { get; set; }
        public decimal? evaluationTime { get; set; }
        public string gradeCode { get; set; }
        public string gradeValue { get; set; }
        public decimal? upSalaryPercentage { get; set; }
        public decimal? variableBonus { get; set; }
        public decimal? score { get; set; }
        public decimal? average { get; set; }
        public decimal? increaseSalary { get; set; }
        public decimal? compenAmount { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }


        public override string InfoType
        {
            get { return "0022"; }
        }


        public ConfidentialAssessmentCenter()
        {
            paEvId = string.Empty;
            companyCode = string.Empty;
            period = string.Empty;
            evaluationTime = default(decimal);
            gradeCode = string.Empty;
            gradeValue = string.Empty;
            upSalaryPercentage = default(decimal);
            variableBonus = default(decimal);
            score = default(decimal);
            average = default(decimal);
            increaseSalary = default(decimal);
            compenAmount = default(decimal);
            actionType = string.Empty;
            serviceType = string.Empty;

        }
    }
}

