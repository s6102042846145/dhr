﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.UTILITY.EXTENSION;
using System;

namespace ESS.HR.PA
{
    public class ConfidentialPenaltyCenter : AbstractInfoType
    {        
        public string paPuId { get; set; }
        public string companyCode { get; set; }
        public string statusCode { get; set; }
        public string statusValue { get; set; }
        public string startCode { get; set; }
        public string startValue { get; set; }
        public string evidenceCode { get; set; }
        public string evidenceValue { get; set; }
        public string punishmentCode { get; set; }
        public string punishmentValue { get; set; }
        public string detail { get; set; }
        public string commandNo { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }
        public override string InfoType
        {
            get { return "0022"; }
        }


        public ConfidentialPenaltyCenter()
        {
            paPuId = string.Empty;
            companyCode = string.Empty;
            statusCode = string.Empty;
            statusValue = string.Empty;
            startCode = string.Empty;
            startValue = string.Empty;
            evidenceCode = string.Empty;
            evidenceValue = string.Empty;
            punishmentCode = string.Empty;
            punishmentValue = string.Empty;
            detail = string.Empty;
            commandNo = string.Empty;
            actionType = string.Empty;
            serviceType = string.Empty;
        }
    }
}

