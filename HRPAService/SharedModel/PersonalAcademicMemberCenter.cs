﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA
{
    public class PersonalAcademicMemberCenter : AbstractInfoType
    {
        public string CompanyCode { get; set; }
        public string Title { get; set; }
        public string SubtypeValue { get; set; }
        public string SubtypeTextTH { get; set; }
        public string SubtypeTextEN { get; set; }
        public string EmpCode { get; set; }
        public string RoleCode { get; set; }
        public string RoleValue { get; set; }
        public string RoleNameTH { get; set; }
        public string RoleNameEN { get; set; }
        public string StartDate { get; set; }
        public string FullNameTH { get; set; }
        public string FullNameEN { get; set; }
        public string PAReMemID { get; set; }
        public string PAReID { get; set; }
        public string ActionType { get; set; }
        public string ServiceType { get; set; }
        
        public string Remark { get; set; }





        public override string InfoType
        {
            get { return "0022"; }
        }


        public PersonalAcademicMemberCenter()
        {
            CompanyCode = string.Empty;
            PAReMemID = string.Empty;
            PAReID = string.Empty;
            Title = string.Empty;
            SubtypeValue = string.Empty;
            SubtypeTextTH = string.Empty;
            SubtypeTextEN = string.Empty;
            EmpCode = string.Empty;
            RoleCode = string.Empty;
            RoleValue = string.Empty;
            RoleNameTH = string.Empty;
            RoleNameEN = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
        }
    }
}
