﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.CONFIG
{
   public class MedicalTitle
    {
        public MedicalTitle()
        {
        }

        public string Key { get; set; }
        public string Description { get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("MEDICALTITLENAME_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}
