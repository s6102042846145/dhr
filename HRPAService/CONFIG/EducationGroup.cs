using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class EducationGroup : AbstractObject
    {
        public EducationGroup()
        {
        }
        public string EducationGroupCode { get; set; }
        public string EducationGroupDesc { get; set; }
        public string Description { get; set; }
    }
}