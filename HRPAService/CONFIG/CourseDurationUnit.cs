using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class CourseDurationUnit : AbstractObject
    {
		public CourseDurationUnit()
		{
			
		}
        public string Code { get; set; }
        public string Description { get; set; }
    }
}