using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class NationalitySortingName : IComparer<Nationality>
    {
        private CharCompare oCompare;

        public NationalitySortingName()
        {
            oCompare = new CharCompare();
        }

        #region IComparer<Nationality> Members

        public int Compare(Nationality x, Nationality y)
        {
            int result = 0;
            for (int i = 0; i < (x.NationalityName.Length < y.NationalityName.Length ? x.NationalityName.Length : y.NationalityName.Length); i++)
            {
                result = oCompare.Compare(x.NationalityName.ToCharArray()[i], y.NationalityName.ToCharArray()[i]);
                if (result == 1 || result == -1)
                    break;
            }
            return result;
        }

        #endregion IComparer<Nationality> Members
    }
}