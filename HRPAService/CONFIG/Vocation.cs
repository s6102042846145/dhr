using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Vocation : AbstractObject
    {
        public Vocation()
        {
        }
        public string VocationCode { get; set; }
        public string VocationText { get; set; }
    }
}