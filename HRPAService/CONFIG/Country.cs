using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class Country
    {
        public Country()
        {
        }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string Nationality{ get; set; }

        public override int GetHashCode()
        {
            string cCode = string.Format("CONTRY_{0}", CountryCode);
            return cCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}