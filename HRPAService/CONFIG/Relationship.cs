using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Relationship : AbstractObject
    {
        public Relationship()
        {
        }
        public string EducationLevel{ get; set; }
        public string CertificateCode{ get; set; }
    }
}