﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.CONFIG
{
    public class Child
    {
        public Child()
        {

        }

        public int ChildID { get; set; }
        public string ChildName { get; set; }
    }
}
