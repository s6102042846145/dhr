using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Branch : AbstractObject
    {
        //private string __educationLevelCode = "";
        //private string __branchCode = "";
        //private string __branchText = "";

        public Branch()
        {
        }
        public string EducationLevelCode { get; set; }
        public string BranchCode { get; set; }
        public string BranchText { get; set; }
        public string Description { get; set; }


    }
}