﻿using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class PAConfiguration : AbstractObject
    {
        public string CategoryName { get; set; }
        public string FieldName { get; set; }
        public int SeqNo { get; set; }
        public bool IsEditable { get; set; }
        public bool IsVisible { get; set; }
        public int GroupAttachFile { get; set; }
        public string GroupFileName { get; set; }
        public string Description { get; set; }
    }
}
