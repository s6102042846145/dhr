using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class ProvinceSortingName : IComparer<Province>
    {
        private CharCompare oCompare;

        public ProvinceSortingName()
        {
            oCompare = new CharCompare();
        }

        #region IComparer<Province> Members

        public int Compare(Province x, Province y)
        {
            int result = 0;
            for (int i = 0; i < (x.ProvinceName.Length < y.ProvinceName.Length ? x.ProvinceName.Length : y.ProvinceName.Length); i++)
            {
                result = oCompare.Compare(x.ProvinceName.ToCharArray()[i], y.ProvinceName.ToCharArray()[i]);
                if (result == 1 || result == -1)
                    break;
            }
            return result;
        }

        #endregion IComparer<Province> Members
    }
}