﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.UTILITY.EXTENSION;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace ESS.HR.PA.DATACLASS
{
    class PersonalAcademic : AbstractObject
    {
        private PersonalAcademicCenter __academicData = new PersonalAcademicCenter();
        public PersonalAcademicCenter AcademicData
        {
            get
            {
                return __academicData;
            }
            set
            {
                __academicData = value;
            }
        }

        private PersonalAcademicCenter __academicData_OLD = new PersonalAcademicCenter();
        public PersonalAcademicCenter AcademicData_OLD
        {
            get
            {
                return __academicData_OLD;
            }
            set
            {
                __academicData_OLD = value;
            }
        }
    }
}