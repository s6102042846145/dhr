﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
using DHR.HR.API.Model;

namespace ESS.HR.PA.DATACLASS
{
    public class ConfidentialAssessment : AbstractObject
    {
        private ConfidentialAssessmentCenter __confidential = new ConfidentialAssessmentCenter();
        public ConfidentialAssessmentCenter Confidential
        {
            get { return __confidential; }
            set { __confidential = value; }
        }

        private ConfidentialAssessmentCenter __confidentialOld = new ConfidentialAssessmentCenter();
        public ConfidentialAssessmentCenter ConfidentialOld
        {
            get { return __confidentialOld; }
            set { __confidentialOld = value; }

        }
    }
}