﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
namespace ESS.HR.PA.DATACLASS
{
    public class TrainingHisInfo : AbstractObject
    {
        private PersonalTrainingCenter __training = new PersonalTrainingCenter();
        public PersonalTrainingCenter Trainning
        {
            get { return __training; }
            set { __training = value; }
        }

        private PersonalTrainingCenter __trainingOld = new PersonalTrainingCenter();
        public PersonalTrainingCenter TrainningOld
        {
            get { return __trainingOld; }
            set { __trainingOld = value; }

        }
    }
}
