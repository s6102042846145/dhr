﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalFamily : AbstractObject
    {
        private PersonalFamilyCenter __familyData = new PersonalFamilyCenter();
        public PersonalFamilyCenter FamilyData
        {
            get
            {
                return __familyData;
            }
            set
            {
                __familyData = value;
            }
        }

        private PersonalFamilyCenter __familyData_OLD = new PersonalFamilyCenter();
        public PersonalFamilyCenter FamilyData_OLD
        {
            get
            {
                return __familyData_OLD;
            }
            set
            {
                __familyData_OLD = value;
            }
        }

    }
}
