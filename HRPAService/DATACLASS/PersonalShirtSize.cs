﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
using DHR.HR.API.Model;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalShirtSize : AbstractObject
    {
        private PAClothingSize __personal = new PAClothingSize();
        public PAClothingSize Personal
        {
            get { return __personal; }
            set { __personal = value; }
        }

        private PAClothingSize __personalOld = new PAClothingSize();
        public PAClothingSize PersonalOld
        {
            get { return __personalOld; }
            set { __personalOld = value; }

        }
    }
}
