﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class EvaluateSetting : AbstractObject
    {
        public int PeriodID { get; set; }
        public int Year { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UpdateByEmployeeID { get; set; }
        public string UpdateByPositionID { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
