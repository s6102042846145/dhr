﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class RoleSetting : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public bool ACCOUNTADMIN { get; set; }
        public bool ACMANAGER { get; set; }
        public bool BEADMIN { get; set; }
        public bool CUSTODIAN { get; set; }
        public bool LETTERADMIN { get; set; }
        public bool OMADMIN { get; set; }
        public bool PAADMIN { get; set; }
        public bool PYADMIN { get; set; }
        public bool SETTINGADMIN { get; set; }
        public bool SUPERADMIN { get; set; }
        public bool TEADMIN { get; set; }
        public bool TMADMIN { get; set; }
    }
}
