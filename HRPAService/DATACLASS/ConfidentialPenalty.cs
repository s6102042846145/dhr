﻿using DHR.HR.API.Model;
using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class ConfidentialPenalty : AbstractObject
    {
        private ConfidentialPenaltyCenter __confidential = new ConfidentialPenaltyCenter();
        public ConfidentialPenaltyCenter Confidential
        {
            get { return __confidential; }
            set { __confidential = value; }
        }

        private ConfidentialPenaltyCenter __confidentialOld = new ConfidentialPenaltyCenter();
        public ConfidentialPenaltyCenter ConfidentialOld
        {
            get { return __confidentialOld; }
            set { __confidentialOld = value; }

        }
    }
}