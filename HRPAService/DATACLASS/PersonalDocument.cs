﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    class PersonalDocument: AbstractObject
    {
        private PersonalDocumentCenter __documentData = new PersonalDocumentCenter();
        public PersonalDocumentCenter DocumentData
        {
            get
            {
                return __documentData;
            }
            set
            {
                __documentData = value;
            }
        }

        private PersonalDocumentCenter __documentData_OLD = new PersonalDocumentCenter();
        public PersonalDocumentCenter DocumentData_OLD
        {
            get
            {
                return __documentData_OLD;
            }
            set
            {
                __documentData_OLD = value;
            }
        }
    }
}
