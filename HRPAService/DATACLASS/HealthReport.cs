﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class HealthReport : AbstractObject
    {
        public int No { get; set; }
        public string Topic { get; set; }
        public string DetecStandard { get; set; }
        public string DetecUse { get; set; }
        public string Remark { get; set; }
    }
}
