﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class PhoneDirectory : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string ImagePath { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public string NickName { get; set; }
        public string OrgUnit { get; set; }
        public string OrgName { get; set; }
        public string Position { get; set; }
        public string PositionName { get; set; }
        public string HOMENUMBER { get; set; }
        public string OFFICENUMBER { get; set; }
        public string CELLPHONE { get; set; }
        public string LINEID { get; set; }
        public string MAIL { get; set; }
        public string PERSONALMAIL { get; set; }
        public string USERID { get; set; }
        public string EMERGENCYPERSON { get; set; }
        public string EMERGENCYPHONE { get; set; }
        public string ORG_SHORTTEXT_EN { get; set; }
        public string ORG_SHORTTEXT_TH { get; set; }
        public string PRIMARYWEBSITE { get; set; }
        public string SECONDARYWEBSITE { get; set; }

    }
}
