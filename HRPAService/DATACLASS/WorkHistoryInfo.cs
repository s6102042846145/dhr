﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
namespace ESS.HR.PA.DATACLASS
{
    public class WorkHistoryInfo : AbstractObject
    {
        private WorkHistoryCenter __workhistory = new WorkHistoryCenter();
        public WorkHistoryCenter WorkHistory
        {
            get { return __workhistory; }
            set { __workhistory = value; }
        }

        private WorkHistoryCenter __workhistoryOld = new WorkHistoryCenter();
        public WorkHistoryCenter WorkHistoryOld
        {
            get { return __workhistoryOld; }
            set { __workhistoryOld = value; }

        }
    }
}
