﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
using DHR.HR.API.Model;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalInfo : AbstractObject
    {
        private PAPersonalInf __personal = new PAPersonalInf();
        public PAPersonalInf Personal
        {
            get { return __personal; }
            set { __personal = value; }
        }

        private PAPersonalInf __personalOld = new PAPersonalInf();
        public PAPersonalInf PersonalOld
        {
            get { return __personalOld; }
            set { __personalOld = value; }

        }
    }
}
