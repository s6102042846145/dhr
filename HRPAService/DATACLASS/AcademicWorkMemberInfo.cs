﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
namespace ESS.HR.PA.DATACLASS
{
    public class AcademicWorkMemberInfo : AbstractObject
    {
        private PersonalAcademicMemberCenter __academicworkmember = new PersonalAcademicMemberCenter();
        public PersonalAcademicMemberCenter Academicworkmember
        {
            get { return __academicworkmember; }
            set { __academicworkmember = value; }
        }

        private PersonalAcademicMemberCenter __academicworkmemberOld = new PersonalAcademicMemberCenter();
        public PersonalAcademicMemberCenter AcademicworkmemberOld
        {
            get { return __academicworkmemberOld; }
            set { __academicworkmemberOld = value; }

        }
    }
}
