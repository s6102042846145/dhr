using DHR.HR.API.Model;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DATACLASS
{
    public class CommunicationData : AbstractObject
    {
        private string __Remark;
        private string __telephoneNo = "";
        private string __workPlace = "";
        private string __employeeID = "";
        private string __mobilePhone = "";
        private string __mobileNotPublic = "";
        private string __email = "";
        private string __homePhone = "";
        private string __privateEmail = "";
        private string __lineId = "";
        private string __primaryWebsite = "";
        private string __secondaryWebsite = "";

        public PAContactInf hdrContactInf { get; set; }

        public string TelephoneNo
        {
            get
            {
                if (string.IsNullOrEmpty(__telephoneNo) || __telephoneNo.Trim() == "")
                {
                    return string.Empty;
                }
                return __telephoneNo;
            }
            set
            {
                __telephoneNo = value;
            }
        }

        public string WorkPlace
        {
            get
            {
                if (string.IsNullOrEmpty(__workPlace) || __workPlace.Trim() == "")
                {
                    return string.Empty;
                }
                return __workPlace;
            }
            set
            {
                __workPlace = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public string MobilePhone
        {
            get
            {
                if (string.IsNullOrEmpty(__mobilePhone) || __mobilePhone.Trim() == "")
                {
                    return string.Empty;
                }
                return __mobilePhone;
            }
            set
            {
                __mobilePhone = value;
            }
        }

        public string MobileNotPublic
        {
            get
            {
                if (string.IsNullOrEmpty(__mobileNotPublic) || __mobileNotPublic.Trim() == "")
                {
                    return string.Empty;
                }
                return __mobileNotPublic;
            }
            set
            {
                __mobileNotPublic = value;
            }
        }

        public string Email
        {
            get
            {
                if (string.IsNullOrEmpty(__email) || __email.Trim() == "")
                {
                    return string.Empty;
                }
                return __email;
            }
            set
            {
                __email = value;
            }
        }

        public string PrivateEmail
        {
            get
            {
                if (string.IsNullOrEmpty(__privateEmail) || __privateEmail.Trim() == "")
                {
                    return string.Empty;
                }
                return __privateEmail;
            }
            set
            {
                __privateEmail = value;
            }
        }

        public string HomePhone
        {
            get
            {
                if (string.IsNullOrEmpty(__homePhone) || __homePhone.Trim() == "")
                {
                    return string.Empty;
                }
                return __homePhone;
            }
            set
            {
                __homePhone = value;
            }
        }

        public string LineID
        {
            get
            {
                if (string.IsNullOrEmpty(__lineId) || __lineId.Trim() == "")
                {
                    return "-";
                }
                return __lineId;
            }
            set
            {
                __lineId = value;
            }
        }

        public string Remark
        {
            get
            {
                if (string.IsNullOrEmpty(__Remark) || __Remark.Trim() == "")
                {
                    return string.Empty;
                }
                return __Remark;
            }
            set
            {
                __Remark = value;
            }
        }

        public string PrimaryWebsite
        {
            get
            {
                if (string.IsNullOrEmpty(__primaryWebsite) || __primaryWebsite.Trim() == "")
                {
                    return string.Empty;
                }
                return __primaryWebsite;
            }
            set
            {
                __primaryWebsite = value;
            }
        }

        public string SecondaryWebsite
        {
            get
            {
                if (string.IsNullOrEmpty(__secondaryWebsite) || __secondaryWebsite.Trim() == "")
                {
                    return string.Empty;
                }
                return __secondaryWebsite;
            }
            set
            {
                __secondaryWebsite = value;
            }
        }
    }
}