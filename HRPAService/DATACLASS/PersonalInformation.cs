﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalInformation : AbstractObject
    {
        private PersonalInfoCenter __personalData = new PersonalInfoCenter();
        public PersonalInfoCenter PersonalData
        {
            get
            {
                return __personalData;
            }
            set
            {
                __personalData = value;
            }
        }

        private PersonalInfoCenter __personalData_OLD = new PersonalInfoCenter();
        public PersonalInfoCenter PersonalData_OLD
        {
            get
            {
                return __personalData_OLD;
            }
            set
            {
                __personalData_OLD = value;
            }
        }
        //private INFOTYPE0002 __personalData = new INFOTYPE0002();
        //public INFOTYPE0002 PersonalData
        //{
        //    get
        //    {
        //        return __personalData;
        //    }
        //    set
        //    {
        //        __personalData = value;
        //    }
        //}

        //private INFOTYPE0002 __personalData_OLD = new INFOTYPE0002();
        //public INFOTYPE0002 PersonalData_OLD
        //{
        //    get
        //    {
        //        return __personalData_OLD;
        //    }
        //    set
        //    {
        //        __personalData_OLD = value;
        //    }
        //}

    }
}
