﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.UTILITY.EXTENSION;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace ESS.HR.PA.DATACLASS
{
    class PersonalAcademicMember : AbstractObject
    {
        private PersonalAcademicMemberCenter __academicMemberData = new PersonalAcademicMemberCenter();
        public PersonalAcademicMemberCenter AcademicMemberData
        {
            get
            {
                return __academicMemberData;
            }
            set
            {
                __academicMemberData = value;
            }
        }

        private PersonalAcademicMemberCenter __academicMemberData_OLD = new PersonalAcademicMemberCenter();
        public PersonalAcademicMemberCenter AcademicMemberData_OLD
        {
            get
            {
                return __academicMemberData_OLD;
            }
            set
            {
                __academicMemberData_OLD = value;
            }
        }
    }
}