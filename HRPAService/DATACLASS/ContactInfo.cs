﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class ContactInfo : AbstractObject
    {
        private PersonalCommunicationCenter __contData = new PersonalCommunicationCenter();
        public PersonalCommunicationCenter ContData
        {
            get
            {
                return __contData;
            }
            set
            {
                __contData = value;
            }
        }

        private PersonalCommunicationCenter __contDataNew = new PersonalCommunicationCenter();
        public PersonalCommunicationCenter ContDataNew
        {
            get
            {
                return __contDataNew;
            }
            set
            {
                __contDataNew = value;
            }
        }
    }
}
