﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class PAAttactFileSummary : AbstractObject
    {
        public int RequestTypeID { get; set; }
        public string TextCode { get; set; }
        public List<PAAttactFileRequestNo> ContentNo;
    }
}
