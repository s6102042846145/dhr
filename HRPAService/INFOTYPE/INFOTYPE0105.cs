﻿using System;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0105 : AbstractInfoType
    {
        public string EmployeeID { get; set; }
        public string CategoryCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DataText { get; set; }


        public INFOTYPE0105()
        {
        }

        public override string InfoType
        {
            get { return "0105"; }
        }
    }
}
