using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE9001 : AbstractObject
    {
        //private string __employeeID = "";
        //private DateTime __beginDate = DateTime.MinValue;
        //private decimal __salary = 0.0M;
        //private DateTime __endDate = DateTime.MinValue;
        //private string __positionName = "";
        //private string __empGroup = "";
        //private string __empSubGroup = "";
        //private string __section = "";
        //private string __division = "";
        //private string __department = "";
        //private string __function = "";
        //private string __companyName = "";

        public INFOTYPE9001()
        {
        }

        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }

        public decimal Salary { get; set; }

        public DateTime EndDate { get; set; }

        public string PositionName { get; set; }
        public string EmpGroup { get; set; }

        public string EmpSubGroup { get; set; }

        public string Section { get; set; }
        public string Division { get; set; }
        public string Department { get; set; }
        public string Function { get; set; }
        public string CompanyName { get; set; }
        //public static List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        //{
        //    return HRPAManagement.GetAllSalaryHistory(EmployeeID);
        //}
    }
}