using System.Collections.Generic;
using System.Data;
using ESS.HR.PA.CONFIG;

namespace ESS.HR.PA.INTERFACE
{
    public interface IHRPAConfigService
    {
        List<PAConfiguration> GetPAConfigurationList();
        DataSet GetPAConfigurationListAll();
        DataSet GetPAConfigurationListByCategoryName(string categoryName);
        List<TitleName> GetTitleList(string LanguageCode);
        List<TitleName> GetTitleEnList(string LanguageCode);
        TitleName GetTitle(string Code);
        TitleName GetTitle(string Code, string LanguageCode);
        void SaveTitleList(List<TitleName> data);

        List<PrefixName> GetPrefixNameList();
        List<PrefixName> GetPrefixNameList(string LanguageCode);
        PrefixName GetPrefixName(string Code);
        void SavePrefixNameList(List<PrefixName> data);

        List<SecondTitle> GetSecondTitleList();
        List<SecondTitle> GetSecondTitleList(string LanguageCode);
        SecondTitle GetSecondTitle(string Code);
        void SaveSecondTitleList(List<SecondTitle> data);

        List<MilitaryTitle> GetMilitaryTitleList();
        List<MilitaryTitle> GetMilitaryTitleList(string LanguageCode);
        MilitaryTitle GetMilitaryTitle(string Code);
        void SaveMilitaryTitleList(List<MilitaryTitle> data);

        List<AcademicTitle> GetAcademicTitleList();
        List<AcademicTitle> GetAcademicTitleList(string LanguageCode);
        AcademicTitle GetAcademicTitle(string Code);
        void SaveAcademicTitleList(List<AcademicTitle> data);

        List<MedicalTitle> GetMedicalTitleList();
        List<MedicalTitle> GetMedicalTitleList(string LanguageCode);
        MedicalTitle GetMedicalTitle(string Code);
        void SaveMedicalTitleList(List<MedicalTitle> data);


        List<NameFormat> GetNameFormatList();
        NameFormat GetNameFormat(string Code);
        void SaveNameFormatList(List<NameFormat> data);

        List<Gender> GetGenderList(string LanguageCode);
        List<Gender> GetGenderList();
        Gender GetGender(string Code);
        Gender GetGender(string Code, string LanguageCode);

        void SaveGenderList(List<Gender> data);

        List<Country> GetCountryList();

        //AddBy: Ratchatawan W. (2012-02-16)
        List<Country> GetCountryList(string LanguageCode);

        Country GetCountry(string Code, string LanguageCode);

        //AddBy: Ratchatawan W. (2012-02-17)
        List<Nationality> GetAllNationality(string LanguageCode);

        Nationality GetNationality(string Code, string LanguageCode);

        Country GetCountry(string Code);
        List<Region> GetRegionByCountryCode(string Code, string LanguageCode);

        void SaveCountryList(List<Country> data);

        List<MaritalStatus> GetMaritalStatusList();

        List<MaritalStatus> GetMaritalStatusList(string LanguageCode);

        MaritalStatus GetMaritalStatus(string Code);

        MaritalStatus GetMaritalStatus(string Code, string LanguageCode);

        void SaveMaritalStatusList(List<MaritalStatus> Data);

        List<Religion> GetReligionList(string LanguageCode);

        Religion GetReligion(string Code);

        Religion GetReligion(string Code, string LanguageCode);

        void SaveReligionList(List<Religion> Data);

        List<Language> GetLanguageList();

        Language GetLanguage(string Code);

        void SaveLanguageList(List<Language> Data);

        List<AddressType> GetAddressTypeList();

        List<AddressType> GetAddressTypeList(string LanguageCode);

        void SaveAddressTypeList(List<AddressType> Data);

        List<Bank> GetBankList(string LanguageCode);

        Bank GetBank(string Code);

        void SaveBankList(List<Bank> Data);

        FamilyMember GetFamilyMember(string Code);

        List<FamilyMember> GetFamilyMemberList();

        List<FamilyMember> GetFamilyMemberList(string LanguageCode);

        void SaveFamilyMemberList(List<FamilyMember> Data);

        void SaveProvinceList(List<Province> Data);

        List<EducationLevel> GetAllEducationLevel(string LanguageCode);

        void SaveAllEducationLevel(List<EducationLevel> list);

        void SaveAllEducationGroup(List<EducationGroup> list);

        List<Vocation> GetAllVocation();

        void SaveAllVocation(List<Vocation> list);

        List<Institute> GetAllInstitute();

        void SaveAllInstitute(List<Institute> list);

        List<Honor> GetAllHonor();
        List<Honor> GetAllHonor(string LanguageCode);
        void SaveAllHonor(List<Honor> list);

        List<Branch> GetAllBranch();
        List<Branch> GetAllBranch(string LanguageCode);

        void SaveAllBranch(List<Branch> list);

        List<CompanyBusiness> GetAllCompanyBusiness();

        void SaveAllCompanyBusiness(List<CompanyBusiness> list);

        List<JobType> GetAllJobType();

        void SaveAllJobType(List<JobType> list);

        List<Certificate> GetAllCertificate();

        void SaveAllCertificate(List<Certificate> list);

        Branch GetBranchData(string BranchCode);

        EducationLevel GetEducationLevel(string EducationLevelCode);

        Certificate GetCertificateCode(string CertificateCode);

        Vocation GetVocation(string VocationCode);

        CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode);

        JobType GetJobType(string JobCode);

        Institute GetInstitute(string InstituteCode);

        List<EducationGroup> GetAllEducationGroup();
        List<EducationGroup> GetAllEducationGroup(string LanguageCode);

        EducationGroup GetEducationGroupByCode(string EducationGroupCode);

        List<CourseDurationUnit> GetAllUnit();

        List<string> GetCertCodeFromEduLevelCode(string educationLevelCode);

        List<string> GetBranchFromEduLevelCode(string educationLevelCode);

        List<Relationship> GetAllRelationship();

        void SaveAllRelationship(List<Relationship> list);

        List<Province> GetProvinceByCountryCode(string Code, string LanguageCode);
        List<Province> GetProvinceAll();
        List<Province> GetProvinceAll(string LanguageCode);
        List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode);
        List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode);

        bool ShowTimeAwareLink(int LinkID);
        bool TransferDataDHRToDB(string oProcedureName, out string oMessage);
        List<Document> GetDocumentTypeAllForDocumentInfo(string LanguageCode);
        List<ValidityType> GetValidityTypeAllForDocumentInfo(string LanguageCode);
        List<AcademicSubType> GetAcademicSubTypeList(string LanguageCode);
        List<AcademicCategoryType> GetAcademicCategoryTypeList(string LanguageCode);
        List<AcademicRole> GetAcademicRoleList(string LanguageCode);
        List<Institute> GetAllInstitute(string LanguageCode);
        List<Certificate> GetAllCertificate(string LanguageCode);
    }
}