﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;


namespace ESS.HR.PA.DATASERVICE
{
    class AddressService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalAddress oPersonalAddrData = new PersonalAddress();
            string ParamAddressType = string.Empty;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] tempParam = CreateParam.Split('|');
                ParamAddressType = tempParam[1];
            }

            List<PersonalAddressCenter> personalAddr = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalAddress(Requestor.EmployeeID, DateTime.Now);
            List<PersonalAddressControl> oListPersonalAddressControl = new List<PersonalAddressControl>();
            PersonalAddressControl oPersonalAddressControl = new PersonalAddressControl();
            oPersonalAddressControl.Requestor = Requestor.EmployeeID;
            oPersonalAddressControl.Requestor_Effective = DateTime.Now;
            oPersonalAddressControl.FileCount = 1;

            oListPersonalAddressControl.Add(oPersonalAddressControl);

            List<PersonalAddressType> oListPersonalAddressType = new List<PersonalAddressType>();
            PersonalAddressType oPersonalAddressType = new PersonalAddressType();
            oPersonalAddressType.Key = ParamAddressType;
            oListPersonalAddressType.Add(oPersonalAddressType);

            oPersonalAddrData.PersonalAddressControl = oListPersonalAddressControl;
            oPersonalAddrData.PersonalAddressType = oListPersonalAddressType;

            oPersonalAddrData.PersonalAddr = personalAddr;
            oPersonalAddrData.PersonalAddrNew = personalAddr;
            return oPersonalAddrData;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalAddress oPersonalAddrData = JsonConvert.DeserializeObject<PersonalAddress>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = ""; 
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}
            PersonalAddress oPersonalAddrData = JsonConvert.DeserializeObject<PersonalAddress>(newData.ToString());
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            //add by Pariyaporn 20200522
            if (oPersonalAddrData.PersonalAddressType != null && oPersonalAddrData.PersonalAddressType.Count > 0)
            {
                if (oPersonalAddrData.PersonalAddressType[0].Key == "002")//ถ้าหากแก้ไขที่อยู่ตามทะเบียนบ้านต้องเป็น ADMIN_CREATE
                {
                    ReturnKey = "ADMIN_CREATE"; 
                }
            }
            //end
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            PersonalAddress oPersonalAddrData = JsonConvert.DeserializeObject<PersonalAddress>(newData.ToString());
            List<PersonalAddressControl> oControlRecord = oPersonalAddrData.PersonalAddressControl;
            if (oControlRecord == null || oControlRecord.Count == 0 || oControlRecord[0].Requestor_Effective.Equals(DateTime.MinValue))
            {
                throw new DataServiceException("HRPA", "RECORD_CONTROL_NOT_FOUND", "Can not find 'RECORDCONTROL' table");
            }

            if (NoOfFileAttached < 1 && Convert.ToInt32(oPersonalAddrData.PersonalAddressType[0].Key) != 2)
            {
                throw new DataServiceException("hrpa", "file_not_enough", "files are not enough");
            }

            string dataCategory = string.Empty;
            List<PersonalAddressCenter> oPersonalAddr = oPersonalAddrData.PersonalAddrNew;
            string AddrType = oPersonalAddrData.PersonalAddressType[0].Key;
            dataCategory = string.Format("HRPAPERSONALADDRESS_{0}", AddrType);
            //PersonalAddressCenter data;
            //comment by pariyaporn 20200522 useless code
            //for (int i = 1; i < oPersonalAddr.Count(); i++)
            //{
            //    data = new PersonalAddressCenter();
            //    data = oPersonalAddr[i];
            //    if (i == 2)
            //        HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

            //}
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }
        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_Address;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

        #region SaveExternalData Delegate
        public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string dataCategory = string.Empty;
            string subType = string.Empty;
            PersonalAddressCenter data;
            PersonalAddress oPersonalAddrData = JsonConvert.DeserializeObject<PersonalAddress>(Data.ToString());
            int AddrType = Convert.ToInt32(oPersonalAddrData.PersonalAddressType[0].Key);
            List<PersonalAddressCenter> pAddress = oPersonalAddrData.PersonalAddrNew;
            for (int i = 1; i < pAddress.Count; i++)
            {
                data = new PersonalAddressCenter();
                data = pAddress[i];
                dataCategory = string.Format("HRPAPERSONALADDRESS_{0}", AddrType);
                //subType = data.AddressType;
                subType = AddrType.ToString();
                HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            }


            if (State == "COMPLETED")
            {
                List<PersonalAddressControl> oControlRecord = oPersonalAddrData.PersonalAddressControl;
                List<PersonalAddressType> oKey = new List<PersonalAddressType>();
                oKey = oPersonalAddrData.PersonalAddressType;

                List<PersonalAddressCenter> oDataAddr = new List<PersonalAddressCenter>();
                oDataAddr = oPersonalAddrData.PersonalAddr;

                List<PersonalAddressCenter> oDataAddrNew = new List<PersonalAddressCenter>();
                oDataAddrNew = oPersonalAddrData.PersonalAddrNew;

                List<PersonalAddressCenter> list = new List<PersonalAddressCenter>();
                foreach (PersonalAddressCenter dr in oDataAddr)
                {
                    PersonalAddressCenter item = new PersonalAddressCenter();
                    item = dr;
                    if (!item.IsDelete)
                    {
                        item.BeginDate = oControlRecord[0].Requestor_Effective;
                        item.EndDate = DateTime.MaxValue;
                    }
                    item.EmployeeID = oControlRecord[0].Requestor;
                    oKey.FindAll(a => a.Key == item.AddressType);
                    if (oKey.Count > 0)
                    {
                        list.Add(item);
                    }
                }
                foreach (PersonalAddressCenter addrdata in oDataAddrNew)
                {
                    PersonalAddressCenter item = new PersonalAddressCenter();
                    item = addrdata;
                    if (!item.IsDelete)
                    {
                        item.BeginDate = oControlRecord[0].Requestor_Effective;
                        item.EndDate = DateTime.MaxValue;
                    }

                    item.EmployeeID = oControlRecord[0].Requestor;
                    oKey.FindAll(a => a.Key == item.AddressType);
                    if (oKey.Count > 0)
                    {
                        list.Add(item);
                    }
                }
                try
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalAddressData(list);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (Exception ex)
                {
                    string sError = string.Empty;
                    foreach (PersonalAddressCenter p_item in list)
                    {
                        if (!string.IsNullOrEmpty(p_item.Remark))
                        {
                            sError += p_item.Remark;
                        }
                    }
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("SaveExternalData Error" + " " + sError, true, ex);
                }
            }
        }


        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to DHR
            string dataCategory = string.Empty;
            string subType = string.Empty;


            PersonalAddress oPersonalAddrData = JsonConvert.DeserializeObject<PersonalAddress>(Data.ToString());
            string AddrType = oPersonalAddrData.PersonalAddressType[0].Key;
            //comment by pariyaporn 20200502 useless code
            //List<PersonalAddressCenter> pAddress = oPersonalAddrData.PersonalAddrNew;
            //PersonalAddressCenter data;
            //for (int i = 0; i < pAddress.Count; i++)
            //{
            //    data = new PersonalAddressCenter();
            //    data = pAddress[i];
            //    //dataCategory = string.Format("HRPAPERSONALADDRESS_{0}", data.AddressType);
            //    //subType = data.AddressType;

            //}

            dataCategory = string.Format("HRPAPERSONALADDRESS_{0}", AddrType);
            subType = AddrType.ToString();
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);

            if (State == "COMPLETED")
            {
                List<PersonalAddressControl> oControlRecord = oPersonalAddrData.PersonalAddressControl;
                List<PersonalAddressType> oKey = new List<PersonalAddressType>();
                oKey = oPersonalAddrData.PersonalAddressType;

                List<PersonalAddressCenter> oDataAddrNew = new List<PersonalAddressCenter>();
                oDataAddrNew = oPersonalAddrData.PersonalAddrNew;

                List<PersonalAddressCenter> list = new List<PersonalAddressCenter>();

                foreach (PersonalAddressCenter addrdata in oDataAddrNew)
                {
                    PersonalAddressCenter item = new PersonalAddressCenter();
                    item = addrdata;
                    if (!item.IsDelete)
                    {
                        item.BeginDate = oControlRecord[0].Requestor_Effective;
                        item.EndDate = DateTime.MaxValue;
                    }

                    item.EmployeeID = oControlRecord[0].Requestor;
                    //oKey.FindAll(a => a.Key == item.AddressType);
                    /*if (oKey.Count > 0)
                    {
                        list.Add(item);
                    }*/
                    for (int i = 0; i < oKey.Count; i++)
                    {
                        if (oKey[i].Key == item.AddressType)
                        {
                            list.Add(item);
                        }
                    }

                }
                try
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalAddressData(list);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (Exception ex)
                {
                    string sError = string.Empty;
                    foreach (PersonalAddressCenter p_item in list)
                    {
                        if (!string.IsNullOrEmpty(p_item.Remark))
                        {
                            sError += p_item.Remark;
                        }
                    }
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message + " " + sError, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("SaveExternalData Error" + " " + sError, true, ex);
                }
            }
        }

        #endregion SaveExternalData Delegate

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            return "";
        }
    }
}
