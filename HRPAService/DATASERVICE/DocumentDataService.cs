﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;


namespace ESS.HR.PA.DATASERVICE
{
    class DocumentDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalDocument oDocumentData = new PersonalDocument();
            string ParamActionType = string.Empty;
            string ParamDocumentType = string.Empty;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] tempParam = CreateParam.Split('|');
                ParamActionType = tempParam[0];
                ParamDocumentType = tempParam[1];
            }

            List<PersonalDocumentCenter> personalDocument = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalDocumentData(Requestor.EmployeeID);
            foreach (var item in personalDocument)
            {
                if (item.DocumentType == ParamDocumentType)
                {
                    oDocumentData.DocumentData = item;
                    oDocumentData.DocumentData_OLD = item;
                }
            }
            return oDocumentData;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalDocument oDocumentModel = JsonConvert.DeserializeObject<PersonalDocument>(Data.ToString());
            DataRow dr = Info.NewRow();

            dr["DOCUMENTTYPE"] = oDocumentModel.DocumentData.DocumentType;
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = "";
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            PersonalDocument oPersonalDocData = JsonConvert.DeserializeObject<PersonalDocument>(newData.ToString());

            if (NoOfFileAttached < 1 )
            {
                throw new DataServiceException("hrpa", "file_not_enough", "files are not enough");
            }

            string subType = oPersonalDocData.DocumentData.DocumentType;
            string dataCategory = string.Format("HRPAPERSONALDOCUMENT_{0}", subType);       
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }
        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_Address;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

        #region SaveExternalData Delegate
        public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string dataCategory = string.Empty;
            string subType = string.Empty;
            PersonalDocumentCenter data;
            PersonalDocument oPersonalDocData = JsonConvert.DeserializeObject<PersonalDocument>(Data.ToString());
            //int AddrType = Convert.ToInt32(oPersonalDocData.DocumentData.DocumentType);
            //List<PersonalAddressCenter> pAddress = oPersonalAddrData.PersonalAddrNew;
            //for (int i = 1; i < pAddress.Count; i++)
            //{
            //    data = new PersonalAddressCenter();
            //    data = pAddress[i];
            //    dataCategory = string.Format("HRPAPERSONALDOCUMENT_{0}", AddrType);
            //    //subType = data.AddressType;
            //    subType = AddrType.ToString();
            //    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            //}


            //if (State == "COMPLETED")
            //{
            //    List<PersonalAddressControl> oControlRecord = oPersonalAddrData.PersonalAddressControl;
            //    List<PersonalAddressType> oKey = new List<PersonalAddressType>();
            //    oKey = oPersonalAddrData.PersonalAddressType;

            //    List<PersonalAddressCenter> oDataAddr = new List<PersonalAddressCenter>();
            //    oDataAddr = oPersonalAddrData.PersonalAddr;

            //    List<PersonalAddressCenter> oDataAddrNew = new List<PersonalAddressCenter>();
            //    oDataAddrNew = oPersonalAddrData.PersonalAddrNew;

            //    List<PersonalAddressCenter> list = new List<PersonalAddressCenter>();
            //    foreach (PersonalAddressCenter dr in oDataAddr)
            //    {
            //        PersonalAddressCenter item = new PersonalAddressCenter();
            //        item = dr;
            //        if (!item.IsDelete)
            //        {
            //            item.BeginDate = oControlRecord[0].Requestor_Effective;
            //            item.EndDate = DateTime.MaxValue;
            //        }
            //        item.EmployeeID = oControlRecord[0].Requestor;
            //        oKey.FindAll(a => a.Key == item.AddressType);
            //        if (oKey.Count > 0)
            //        {
            //            list.Add(item);
            //        }
            //    }
            //    foreach (PersonalAddressCenter addrdata in oDataAddrNew)
            //    {
            //        PersonalAddressCenter item = new PersonalAddressCenter();
            //        item = addrdata;
            //        if (!item.IsDelete)
            //        {
            //            item.BeginDate = oControlRecord[0].Requestor_Effective;
            //            item.EndDate = DateTime.MaxValue;
            //        }

            //        item.EmployeeID = oControlRecord[0].Requestor;
            //        oKey.FindAll(a => a.Key == item.AddressType);
            //        if (oKey.Count > 0)
            //        {
            //            list.Add(item);
            //        }
            //    }
            //    try
            //    {
            //        HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalAddressData(list);
            //        HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
            //    }
            //    catch (Exception ex)
            //    {
            //        string sError = string.Empty;
            //        foreach (PersonalAddressCenter p_item in list)
            //        {
            //            if (!string.IsNullOrEmpty(p_item.Remark))
            //            {
            //                sError += p_item.Remark;
            //            }
            //        }
            //        HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
            //        throw new SaveExternalDataException("SaveExternalData Error" + " " + sError, true, ex);
            //    }
            //}
        }

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to DHR
            PersonalDocument oPersonalDocData = JsonConvert.DeserializeObject<PersonalDocument>(Data.ToString());
            string subType = oPersonalDocData.DocumentData.DocumentType;
            string dataCategory = string.Format("HRPAPERSONALDOCUMENT_{0}", subType);
            //subType = AddrType.ToString();
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);

            if (State == "COMPLETED")
            {
                try
                {
                    PersonalDocumentCenter oComDataNew = new PersonalDocumentCenter();
                    oComDataNew.CompanyCode = Requestor.CompanyCode;
                    oComDataNew.EmployeeID = Requestor.EmployeeID;
                    oComDataNew.DocumentNo = oPersonalDocData.DocumentData.DocumentNo;
                    oComDataNew.DocumentType = oPersonalDocData.DocumentData.DocumentType;
                    oComDataNew.IssueDate = oPersonalDocData.DocumentData.IssueDate;
                    oComDataNew.ExpiredDate = oPersonalDocData.DocumentData.ExpiredDate;
                    oComDataNew.ValidityOfEntry = oPersonalDocData.DocumentData.ValidityOfEntry;
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalDocumentData(oComDataNew);

                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
            }
        }

        #endregion SaveExternalData Delegate

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

    }
}
