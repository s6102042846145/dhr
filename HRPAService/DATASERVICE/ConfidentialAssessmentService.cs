﻿using DHR.HR.API.Model;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DATASERVICE
{
    class ConfidentialAssessmentService : AbstractDataService
    {
        public ConfidentialAssessmentService()
        { }


        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            ConfidentialAssessment Data = new ConfidentialAssessment();
            ConfidentialAssessmentCenter oReturn = new ConfidentialAssessmentCenter();
            try
            {
                if (!string.IsNullOrEmpty(CreateParam))
                {

                    string[] param = CreateParam.Split('|');

                    if (param.Count() > 0 && param[0] == "NEWID")
                    {
                        oReturn.EmployeeID = param[1];
                        oReturn.paEvId = null;
                        oReturn.BeginDate = new DateTime(1900, 1, 1, 0, 0, 0, 0);
                        oReturn.EndDate = new DateTime(9999, 12, 31, 0, 0, 0, 0);
                        Data.Confidential = oReturn;
                        Data.ConfidentialOld = oReturn;
                    }
                    else if (param.Count() > 2)
                    {
                        //DateTime BeginDate = DateTime.ParseExact(param[1].ToString(), "yyyy-MM-dd", null);
                        //DateTime EndDate = DateTime.ParseExact(param[2].ToString(), "yyyy-MM-dd", null);

                        //oReturn = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetEvaluationInfData(BeginDate, EndDate, Requestor.CompanyCode, param[3])
                        //.Where(x => x.paEvId == param[4]).FirstOrDefault();
                    //    oReturn = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetTrainingHistory(param[1])
                    //.Where(x => x.PaTrId == param[3]).FirstOrDefault();
                        oReturn = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetConfidentialAssessment(param[1])
                    .Where(x => x.paEvId == param[3]).FirstOrDefault();

                        Data.Confidential = oReturn;
                        Data.ConfidentialOld = oReturn;

                    }

                }
                else
                {
                    //oReturn.EmployeeID = null;
                    //oReturn.paEvId = null;
                    //oReturn.BeginDate = DateTime.Now;
                    //oReturn.EndDate = new DateTime(9999, 12, 31, 0, 0, 0, 0);
                    Data.Confidential = oReturn;
                    Data.ConfidentialOld = oReturn;

                }
            }
            catch(Exception e)
            {
                throw;
            }
            return Data;
        }

        //public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        //{
        //    Info.Rows.Clear();
        //    WorkHistoryInfo oWorkHistory = JsonConvert.DeserializeObject<WorkHistoryInfo>(Data.ToString());
        //    DataRow dr = Info.NewRow();
        //    Info.Rows.Add(dr);
        //}

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = "";
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }
        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            TrainingHisInfo oTrainning = JsonConvert.DeserializeObject<TrainingHisInfo>(Additional.ToString());
            return oTrainning.Trainning.CourseName;

        }


        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            ConfidentialAssessment oTrainning = JsonConvert.DeserializeObject<ConfidentialAssessment>(newData.ToString());

            string subType = GenerateRequestSubtype(Requestor, newData);
            string dataCategory = "PERSONAL ALASSESSMENT_" + subType + "_" + oTrainning.Confidential.paEvId;
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE");
            }
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveConfidentialAssessment_Data;
            oTrainning.Confidential.actionType = !string.IsNullOrEmpty(oTrainning.Confidential.paEvId) ? "U" : "I";
            oTrainning.Confidential.serviceType = "V";
            List<ConfidentialAssessmentCenter> list = new List<ConfidentialAssessmentCenter>();
            list.Add(oTrainning.Confidential);
            try
            {

                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateAssessment(list);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }

        }

        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_WorkHistory;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

        #region SaveExternalData Extension

        //public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        //{
        //    //Save data to sql / sap
        //    string subType = GenerateRequestSubtype(Requestor, Data);
        //    string dataCategory = "PERSONALEDUCATION_" + subType;
        //    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
        //    if (State == "COMPLETED")
        //    {
        //        try
        //        {
        //            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
        //            List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();

        //            educationList.Add(oEducationInfo.Education);
        //            if (!string.IsNullOrEmpty(oEducationInfo.EducationOld.EducationLevelCode) && (oEducationInfo.Education.EducationLevelCode == oEducationInfo.EducationOld.EducationLevelCode))
        //            {
        //                //update by deleting Old data education
        //                List<PersonalEducationCenter> educationUpdate = new List<PersonalEducationCenter>();
        //                educationUpdate.Add(oEducationInfo.EducationOld);
        //                educationUpdate.Add(oEducationInfo.Education);

        //                HRPAManagement.CreateInstance(Requestor.CompanyCode).DeleteInsertEducation(educationUpdate);
        //            }
        //            else
        //            {
        //                HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveEducation(educationList);
        //            }

        //            HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
        //        }
        //        catch (Exception ex)
        //        {
        //            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
        //            throw new SaveExternalDataException("Post data error", true, ex);
        //        }
        //    }
        //}

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            ConfidentialAssessment oData = JsonConvert.DeserializeObject<ConfidentialAssessment>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONAL ALASSESSMENT_" + subType + "_" + oData.Confidential.paEvId;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    oData.Confidential.actionType = !string.IsNullOrEmpty(oData.Confidential.paEvId) ? "U" : "I";
                    oData.Confidential.serviceType = "S";
                    List<ConfidentialAssessmentCenter> trainningList = new List<ConfidentialAssessmentCenter>();
                    trainningList.Add(oData.Confidential);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveConfidentialAssessment(trainningList);
                    //HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                    //("HROM_EXCEPTION", "OM_DATA_SAVE");
                }
            }

        }

        #endregion

    }
}
