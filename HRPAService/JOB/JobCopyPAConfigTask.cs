using ESS.EMPLOYEE;
using ESS.JOB.ABSTRACT;
using System;

namespace ESS.HR.PA.JOB
{
    public class JobCopyPAConfigTask : AbstractTaskWorker
    {
        #region Variable

        private const string SUCCESS = "SUCCESS";
        private const string FINISH = "FINISH";
        private const string ERROR = "ERROR";
        private const string ToMode = "DB";
        private string FromMode = string.Empty;
        private string Message = string.Empty;
        #endregion Variable

        public override void Run()
        {
            HRPAManagement oHRPAManagement = HRPAManagement.CreateInstance(CompanyCode);

            #region NAMEFORMAT

            try
            {
                Console.WriteLine(":: Copy data NAMEFORMAT");
                FromMode = HRPAServices.SVC(CompanyCode).PA_GetNameFormatList;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllNameFormat(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllNameFormat", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "NAMEFORMAT");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion NAMEFORMAT

            #region COUNTRY

            try
            {
                Console.WriteLine(":: Copy data COUNTRY");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllCountry;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllCountry(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllCountry", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Country", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Country " + Message, FromMode);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "COUNTRY");
                        Console.WriteLine(Message);
                        break;
                    default:
                      
                        break;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "COUNTRY");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion COUNTRY

            #region ADDRESSTYPE

            try
            {
                Console.WriteLine(":: Copy data ADDRESSTYPE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllAddressType;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllAddressType(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllAddressType", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_AddressType", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_AddressType", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "ADDRESSTYPE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "ADDRESSTYPE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion ADDRESSTYPE

            #region CERTIFICATE

            try
            {
                Console.WriteLine(":: Copy data CERTIFICATE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllCertificate;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllCertificate(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllCertificate", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Certificate", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Certificate", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "CERTIFICATE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "CERTIFICATE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion CERTIFICATE

            #region INFOTYPE0105_Mapping
            try
            {
                Console.WriteLine(":: Copy data INFOTYPE0105_Mapping");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllInfotype0105_Mapping;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_INFOTYPE0105_Mapping", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_INFOTYPE0105_Mapping", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "INFOTYPE0105_Mapping");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "INFOTYPE0105_Mapping");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion INFOTYPE0105_Mapping

            #region EDUCATIONGROUP

            try
            {
                Console.WriteLine("Copy data EDUCATIONGROUP");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllEducationGroup;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllEducationGroup(FromMode), ToMode);
                       // oHRPAManagement.SaveTo(oHRPAManagement.GetAllEducationGroup(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllEducationGroup", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_EducationGroup", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_EducationGroup", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "EDUCATIONGROUP");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "EDUCATIONGROUP");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion EDUCATIONGROUP

            #region EDUCATIONLEVEL

            try
            {
                Console.WriteLine("Copy data EDUCATIONLEVEL");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllEducationLevel;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllEducationLevel(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllEducationLevel", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_EducationLevel", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_EducationLevel", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "EDUCATIONLEVEL");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "EDUCATIONLEVEL");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion EDUCATIONLEVEL

            #region EMPGROUP

            try
            {
                Console.WriteLine(":: Copy data EMPGROUP");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllEmpGroup;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_EmpGroup", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_EmpGroup",Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "EMPGROUP");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "EMPGROUP");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion EMPGROUP

            #region TITLENAME  
            try
            {
                Console.WriteLine(":: Copy data TITLENAME");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllTitleName;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllTitleNameList(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllTitleNameList", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_TitleName", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_TitleName", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "TITLENAME");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "TITLENAME");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion TITLENAME

            #region TITLENAMEEN
            try
            {
                Console.WriteLine(":: Copy data TITLENAME_EN");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllTitleName;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllTitleNameList(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllTitleNameList", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_TitleNameEn", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_TitleName", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "TITLENAME_EN");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "TITLENAME_EN");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion TITLENAME

            #region PREFIXNAME
            try
            {
                Console.WriteLine(":: Copy data PREFIXNAME");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllPrefixName;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetPrefixNameList(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetPrefixNameList", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_PrefixName", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_PrefixName", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "PREFIXNAME");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "PREFIXNAME");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion PREFIXNAME

            #region MILITARYTITLE

            try
            {
                Console.WriteLine(":: Copy data MILITARYTITLE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllMilitaryTitle;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_MilitaryTitle", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_MilitaryTitle", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "MILITARYTITLE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "MILITARYTITLE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion MILITARYTITLE

            #region SECONDTITLE

            try
            {
                Console.WriteLine(":: Copy data SECONDTITLE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllSecondTitle;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_SecondTitle", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_SecondTitle", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "SECONDTITLE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "SECONDTITLE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion SECONDTITLE

            #region ACADEMICTITLE

            try
            {
                Console.WriteLine(":: Copy data ACADEMICTITLE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllAcademicTitle;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_AcademicTitle", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_AcademicTitle", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "ACADEMICTITLE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "ACADEMICTITLE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion ACADEMICTITLE

            #region MEDICALTITLE

            try
            {
                Console.WriteLine(":: Copy data MEDICALTITLE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllMedicalTitle;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_MedicalTitle", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_MedicalTitle", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "MEDICALTITLE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "MEDICALTITLE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion MEDICALTITLE

            #region INSTITUTE

            try
            {
                Console.WriteLine(":: Copy data INSTITUTE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllInstitute;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllInstitute(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllInstitute", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Institute", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Institute", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "INSTITUTE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "INSTITUTE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion INSTITUTE

            #region BRANCH

            try
            {
                Console.WriteLine(":: Copy data BRANCH");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllBranch;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllBranch(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllBranch", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Branch", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Branch", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "BRANCH");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "BRANCH");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion BRANCH            

            #region MARITALSTATUS

            try
            {
                Console.WriteLine(":: Copy data MARITALSTATUS");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllMaritalStatus;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllMaritalStatus("", FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllMaritalStatus", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_MaritalStatus", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_MaritalStatus", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "MARITALSTATUS");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "MARITALSTATUS");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion MARITALSTATUS

            #region NATIONALITY

            try
            {
                Console.WriteLine(":: Copy data NATIONALITY");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllNationaltity;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Nationality", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Nationality", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "NATIONALITY");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "NATIONALITY");

            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion NATIONALITY

            #region PROVINCE

            try
            {
                Console.WriteLine(":: Copy data PROVINCE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllProvince;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Province", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Province", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "PROVINCE");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "PROVINCE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion PROVINCE

            #region FAMILYMEMBER

            try
            {
                Console.WriteLine(":: Copy data FAMILYMEMBER");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllFamilyMember;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllFamilyMember(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllFamilyMember", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    default:
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_FamilyMember", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllFamilyMember", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "FAMILYMEMBER");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "FAMILYMEMBER");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion FAMILYMEMBER

            #region RELIGION

            try
            {
                Console.WriteLine(":: Copy data RELIGION");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllReligion;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllReligion(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllReligion", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Religion", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllReligion", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "RELIGION");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "RELIGION");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion RELIGION

            #region GENDER

            try
            {
                Console.WriteLine(":: Copy data GENDER");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllGender;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllGenderList(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllGenderList", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Gender", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllGenderList", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "GENDER");
                        Console.WriteLine(Message);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "GENDER");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion GENDER

            #region HONOR

            try
            {
                Console.WriteLine(":: Copy data HONOR");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllHonor;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Honor", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Honor", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "HONOR");
                        Console.WriteLine(Message);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "HONOR");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion HONOR

            #region LANGUAGE

            try
            {
                Console.WriteLine(":: Copy data LANGUAGE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_GetLanguageList;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllLanguage(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllLanguage", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "LANGUAGE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion LANGUAGE

            #region BANK

            try
            {
                Console.WriteLine(":: Copy data BANK");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllBank;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllBank(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllBank", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_Bank", out Message);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "sp_JobPAConfig_DHR_TO_Bank", Message);//Comment by Koissares 20201126
                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "BANK");
                        Console.WriteLine(Message);
                        break;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "BANK");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion BANK

            #region RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE

            try
            {
                Console.WriteLine("Copy data RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_GetAllRelationship;
                switch (FromMode.ToUpper())
                {
                    case "SAP":
                        oHRPAManagement.SaveTo(oHRPAManagement.GetAllRelationship(FromMode), ToMode);
                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "GetAllRelationship", FromMode + "->" + ToMode);//Comment by Koissares 20201126
                        Console.WriteLine(SUCCESS);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE

            #region DOCUMENTTYPE
            try
            {
                Console.WriteLine(":: Copy data DOCUMENTTYPE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllDocumentType;
                switch (FromMode.ToUpper())
                {
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_DocumentType", out Message);

                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "DOCUMENTTYPE");
                        Console.WriteLine(Message);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "DOCUMENTTYPE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion

            #region VALIDITYTYPE
            try
            {
                Console.WriteLine(":: Copy data VALIDITYTYPE");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllValidityType;
                switch (FromMode.ToUpper())
                {
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_ValidityType", out Message);

                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "VALIDITYTYPE");
                        Console.WriteLine(Message);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "VALIDITYTYPE");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion

            #region AcademicCategoryType
            try
            {
                Console.WriteLine(":: Copy data AcademicCategoryType");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllAcademicCategoryType;
                switch (FromMode.ToUpper())
                {
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_AcademicCategoryType", out Message);

                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "AcademicCategoryType");
                        Console.WriteLine(Message);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "AcademicCategoryType");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion

            #region AcademicSubType
            try
            {
                Console.WriteLine(":: Copy data AcademicSubType");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllAcademicSubType;
                switch (FromMode.ToUpper())
                {
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_AcademicSubType", out Message);

                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "AcademicSubType");
                        Console.WriteLine(Message);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "AcademicSubType");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion

            #region AcademicRole
            try
            {
                Console.WriteLine(":: Copy data AcademicRole");
                FromMode = HRPAServices.SVC(CompanyCode).PA_Copy_AllAcademicRole;
                switch (FromMode.ToUpper())
                {
                    case "DB":
                        oHRPAManagement.TransferDataDHRToDB(FromMode, "sp_JobPAConfig_DHR_TO_AcademicRole", out Message);

                        if (Message.ToUpper().Contains("ERROR"))
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, Message, "AcademicRole");
                        Console.WriteLine(Message);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
                EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, "AcademicRole");
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
            #endregion



        }
    }
}