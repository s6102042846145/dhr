﻿using ESS.SHAREDATASERVICE;

namespace ESS.HR.OM
{
    public class HROMServices
    {
        public HROMServices()
        {

        }

        private static string ModuleID = "ESS.OM.SVC";
        private string CompanyCode { get; set; }

        public static HROMServices SVC(string oCompanyCode)
        {
            HROMServices oHROMServices = new HROMServices()
            {
                CompanyCode = oCompanyCode
            };
            return oHROMServices;
        }

        public string GET_BAND_DLL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_BAND_DLL");
            }
        }

        public string GET_OMUNIT_BY_PERIODDATE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_OMUNIT_BY_PERIODDATE");
            }
        }
        public string GET_ORGANIZATION_BY_ORGID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ORGANIZATION_BY_ORGID");
            }
        }
        public string GET_ORGANIZATIONDATA_BY_ID_BEGDA_ENDDA
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ORGANIZATIONDATA_BY_ID_BEGDA_ENDDA");
            }
        }
        public string SAVE_ORGANIZATIONDATA
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ORGANIZATIONDATA");
            }
        }
        public string GET_ORGANIZATION_CHART_BY_HEAD_ORG
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ORGANIZATION_CHART_BY_HEAD_ORG");
            }
        }
        public string GET_OM_DATACONTENT
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_OM_DATACONTENT");
            }
        }
        public string GET_OMVALIDATE_SOURCE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_OMVALIDATE_SOURCE");
            }
        }

        #region Position

        public string UI_GET_OMPOSITION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UI_GET_OMPOSITION");
            }
        }
        #endregion

    }
}