﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    public class OrganizationChief
    {
        public string OrganizationID { get; set; }
        public string OrganizationLevel { get; set; }
        public string HeadUnitNameTH { get; set; }
        public string HeadUnitNameEN { get; set; }
        public string HeadUnitPositionTH { get; set; }
        public string HeadUnitPositionEN { get; set; }
    }
}
