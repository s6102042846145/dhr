﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    public class PositionMasterData
    {
        private PositionCenter _positionData = new PositionCenter();
        public PositionCenter PositionData
        {
            get
            {
                return _positionData;
            }
            set
            {
                _positionData = value;
            }
        }

        private PositionCenter _positionData_OLD = new PositionCenter();
        public PositionCenter PositionData_OLD
        {
            get
            {
                return _positionData_OLD;
            }
            set
            {
                _positionData_OLD = value;
            }
        }
    }
}
