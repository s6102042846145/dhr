﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.INTERFACE
{
   public interface IAuthenService
    {
        bool GetServicePermission(string ServiceName);
    }
}
