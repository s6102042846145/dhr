﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;


namespace ESS.HR.OM.DATASERVICE
{
    class OrganizationInactiveDataService : AbstractDataService
    {
        string oOrgID = String.Empty;
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {

            if (!String.IsNullOrEmpty(CreateParam))
            {
                oOrgID = CreateParam.Split('|')[0];
                //oReturn = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", oOrgID, DateTime.MinValue, DateTime.MaxValue);
            }

            object oReturn = new { 
                OrganizationID = oOrgID,
                LastEffectiveDate = DateTime.Now
            };
            return oReturn;
        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            string oOrganizationID = ((Newtonsoft.Json.Linq.JToken)newData).Root["OrganizationID"].ToString();
            DateTime oLastEffectiveDate = Convert.ToDateTime(((Newtonsoft.Json.Linq.JToken)newData).Root["LastEffectiveDate"].ToString()).Date;
            string oActionType = "C",
                oServiceType = "V",
                DataSource = HROMServices.SVC(Requestor.CompanyCode).GET_OMVALIDATE_SOURCE;
            OrganizationCenter oValidateOrg = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", oOrganizationID, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31)).LastOrDefault();

            #region Check MarkData
            string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
            string oCondition = "=";
            //Check Org
            string oDataCategory = String.Format("HROMORGANIZATION_{0}", oValidateOrg.OrganizationID);
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORG");

            //check Pos
            oDataCategory = String.Format("HROMPOSITION_{0}", oValidateOrg.OrganizationID);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORGPOS");
            #endregion

            try
            {
                oValidateOrg.EndDate = oLastEffectiveDate.Date;
                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSource).DataService.ValidateOrganizationData(oValidateOrg, oActionType, oServiceType);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                    //DataServiceException("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string organizationID = ((Newtonsoft.Json.Linq.JToken)Data).Root["OrganizationID"].ToString();
            DateTime oLastEffectiveDate = Convert.ToDateTime(((Newtonsoft.Json.Linq.JToken)Data).Root["LastEffectiveDate"].ToString()).Date;

            OrganizationCenter oReturn = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", organizationID, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31)).LastOrDefault();
            string dataCategoryOrg = string.Format("HROMORGANIZATION_{0}", oReturn.OrganizationID);

            if (State == "COMPLETED")
            {
                try
                {
                    oReturn.EndDate = oLastEffectiveDate.Date;
                    HROMManagement.CreateInstance(Requestor.CompanyCode).SaveOrganizationData(oReturn, "C", "S");
                    HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //DataServiceException("HROM_EXCEPTION", "OM_INACTIVE_SAVE");
                }
            }
            else
            {
                HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            }
        }

    }
}

