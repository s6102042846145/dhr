﻿
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ESS.HR.OM.DATASERVICE
{
    class OrganizationDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            OrganizationMasterData oReturn = new OrganizationMasterData();
            if (!String.IsNullOrEmpty(CreateParam))
            {
                string oOrgID = CreateParam.Split('|')[0];
                DateTime oBegDate = DateTime.ParseExact(CreateParam.Split('|')[1], "ddMMyyyy", null);
                DateTime oEndDate = DateTime.ParseExact(CreateParam.Split('|')[2], "ddMMyyyy", null);
                var orgStructure = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", oOrgID, oBegDate, oEndDate).FirstOrDefault();
                oReturn.OrganizationData = orgStructure;
                oReturn.OrganizationDataOLD = orgStructure;
            }
            else
            {
                DateTime oBegDate = new DateTime(DateTime.Now.Date.Year, 1, 1);
                DateTime oEndDate = new DateTime(9999, 12, 31);
                oReturn.OrganizationData = new OrganizationCenter();
                oReturn.OrganizationData.ID = null;
                oReturn.OrganizationData.CompanyCode = Requestor.CompanyCode;
                oReturn.OrganizationData.OrderNo = 0;
                oReturn.OrganizationDataOLD = oReturn.OrganizationData;
            }
            return oReturn;
        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            OrganizationMasterData Organization = JsonConvert.DeserializeObject<OrganizationMasterData>(newData.ToString());
            OrganizationCenter additional = Organization.OrganizationData;
            string oActionType = String.Empty,
                oServiceType = "V",
                DataSource = HROMServices.SVC(Requestor.CompanyCode).GET_OMVALIDATE_SOURCE;

            var tmpAmount = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", additional.OrganizationID, additional.BeginDate, additional.EndDate);
            if (tmpAmount.Count > 0 && !String.IsNullOrEmpty(additional.OrganizationID))
                oActionType = "U";
            else
                oActionType = "I";

            #region Check MarkData
            if (oActionType != "I") { 
                string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
                string oCondition = "=";
                //Check Org
                string oDataCategory = String.Format("HROMORGANIZATION_{0}", Organization.OrganizationData.OrganizationID);
                if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                    //&& oDataCategory != "HROMORGANIZATION_" // prevent multiple new unit
                    throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORG");

                //check Pos
                oDataCategory = String.Format("HROMPOSITION_{0}", Organization.OrganizationData.OrganizationID);
                oCondition = "%";
                if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                    //&& !String.IsNullOrEmpty(Organization.OrganizationData.OrganizationID)
                    throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORGPOS");
            }
            #endregion

            try
            {
                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSource).DataService.ValidateOrganizationData(additional, oActionType, oServiceType);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);

            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            OrganizationMasterData Organization = JsonConvert.DeserializeObject<OrganizationMasterData>(Data.ToString());
            string dataCategoryOrg = string.Format("HROMORGANIZATION_{0}", Organization.OrganizationDataOLD.OrganizationID);

            if (State == "COMPLETED")
            {
                try
                {
                    string oActionType = String.Empty;
                    //if new org return 'I'
                    if (HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", Organization.OrganizationData.OrganizationID, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31)).Count > 0
                        && !String.IsNullOrEmpty(Convert.ToString(Organization.OrganizationDataOLD.ID)))
                    {
                        oActionType = "U";
                    }
                    else
                    {
                        oActionType = "I";
                    }
                    //Flag 'S' for Save
                    HROMManagement.CreateInstance(Requestor.CompanyCode).SaveOrganizationData(Organization.OrganizationData, oActionType, "S");
                    HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
                }
                catch(DataServiceException dex){
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                        //("HROM_EXCEPTION", "OM_DATA_SAVE");
                }
            }
            else
            {
                HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            }
        }

    }
}
