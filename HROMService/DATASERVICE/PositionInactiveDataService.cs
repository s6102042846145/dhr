﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;

namespace ESS.HR.OM.DATASERVICE
{
    class PositionInactiveDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PositionPermanentDeleteMasterData PositionData = new PositionPermanentDeleteMasterData();
            List<PositionCenter> tmp = new List<PositionCenter>();
            if (!string.IsNullOrEmpty(CreateParam))
            {

                DateTime startDate = DateTime.ParseExact("1900-01-01", "yyyy-MM-dd", null);
                DateTime endDate = DateTime.ParseExact("9999-12-31", "yyyy-MM-dd", null);
                string poscode = CreateParam;
                tmp = HROMManagement.CreateInstance(Requestor.CompanyCode).GetPositionDataContent(startDate, endDate, string.Empty, string.Empty, poscode);
            }

            PositionData.PositionData = tmp;
            PositionData.PositionData_OLD = tmp;
            return PositionData;
        }
        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {

        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            string oExceptionError = string.Empty;
            string oErrorTextCode = string.Empty;
            string oErrorParam = string.Empty;
            PositionPermanentDeleteMasterData PositionData = JsonConvert.DeserializeObject<PositionPermanentDeleteMasterData>(newData.ToString());
            string dataCategory = string.Format("HROMPOSITION_{0}", PositionData.PositionData.FirstOrDefault().PosCode); //HROMPOSITION_POSCODE
            var PositionDataLastRecord = PositionData.PositionData.OrderByDescending(a => a.EndDate).FirstOrDefault();
            #region Check MarkData
            string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
            string oCondition = "=";
            //Check Org
            string oDataCategory = String.Format("HROMORGANIZATION_{0}", PositionDataLastRecord.UnitCode);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                && oDataCategory != "HROMORGANIZATION_")
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_UNITCODE");


            //check PosCode
            oDataCategory = String.Format("HROMPOSITION_{0}", PositionDataLastRecord.PosCode);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                && oDataCategory != "HROMPOSITION_")
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_POSCODE");
            #endregion

            if (RequestNo.ToUpper() == "DUMMY")
            {
                if (HROMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(dataCategory))
                {
                    throw new DataServiceException("HROM", "REQUEST_DUPLICATE", "Request duplicate");
                }
            }
            else
            {
                if (HROMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(dataCategory, RequestNo))
                {
                    throw new DataServiceException("HROM", "REQUEST_DUPLICATE", "Request duplicate");
                }
            }
            PositionDataLastRecord.ActionType = "C";
            bool result = ServiceManager.CreateInstance(Requestor.CompanyCode, "DHR").DataService.SaveWithValidateSolidLineData(PositionDataLastRecord, "V", out oExceptionError, out oErrorTextCode, out oErrorParam);

            if (!result)
            {
                if (!string.IsNullOrEmpty(oErrorTextCode))
                {
                    string[] param = !string.IsNullOrEmpty(oErrorParam) ? oErrorParam.Split('|') : null;
                    throw new DataServiceException("VALIDATION", oErrorTextCode, param);
                }
                else
                {
                    throw new DataServiceException("HROM_EXCEPTION", "POSITION_DOTED_VALIDATE");
                }

            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            PositionPermanentDeleteMasterData PositionData = JsonConvert.DeserializeObject<PositionPermanentDeleteMasterData>(Data.ToString());
            var PositionDataLastRecord = PositionData.PositionData.OrderByDescending(a => a.EndDate).FirstOrDefault();
            string oExceptionError = string.Empty;
            string oErrorTextCode = string.Empty;
            string oErrorParam = string.Empty;
            string dataCategory = string.Format("HROMPOSITION_{0}", PositionDataLastRecord.PosCode); //HROMPOSITION_DOTED_HEADPOSCODE_POSCODE
            HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    // save something to dhr
                    PositionDataLastRecord.ActionType = "C";
                    bool result = ServiceManager.CreateInstance(Requestor.CompanyCode, "DHR").DataService.SaveWithValidateSolidLineData(PositionDataLastRecord, "S", out oExceptionError, out oErrorTextCode, out oErrorParam);

                    if (!result)
                    {
                        if (!string.IsNullOrEmpty(oErrorTextCode))
                        {
                            string[] param = oErrorParam.Split('|');
                            throw new DataServiceException("VALIDATION", oErrorTextCode, param);
                        }
                        else
                        {
                            throw new DataServiceException("HROM_EXCEPTION", "POSITION_DOTED_VALIDATE");
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new DataServiceException("HROM_EXCEPTION", "SAVE_EXTERNAL_ERROR " + ex.Message);
                }
            }
        }

    }
}
