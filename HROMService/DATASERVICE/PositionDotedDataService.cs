﻿
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using System.Reflection;

namespace ESS.HR.OM.DATASERVICE
{
    class PositionDotedDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PositionDotedMasterData PositionData = new PositionDotedMasterData();
            PositionDotedCenter tmp = new PositionDotedCenter();
            // List<PositionDotedDetailCenter> tmp = new List<PositionDotedCenter>();
            if (!string.IsNullOrEmpty(CreateParam))
            {   //request_id = 1206
                //unit_code|pos_code|start_date|end_date
                string[] param = CreateParam.Split('|');
                string unitcode = string.Empty;
                string poscode = string.Empty;
                DateTime startDate = DateTime.Now;
                DateTime endDate = DateTime.Now;
                for (int i = 0; i < param.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            unitcode = param[i];
                            break;
                        case 1:
                            poscode = param[i];
                            break;
                        case 2:
                            startDate = DateTimeService.ConvertDateTimeURL(Requestor.CompanyCode, param[i]);
                            break;
                        case 3:
                            endDate = DateTimeService.ConvertDateTimeURL(Requestor.CompanyCode, param[i]);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(poscode))
                {

                    //get doted data list by pos_code

                }


                PositionData.PositionDotedData = tmp;
                PositionData.PositionDotedData_OLD = tmp;
            }
            return PositionData;
        }
        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PositionDotedMasterData PositionDotedData = JsonConvert.DeserializeObject<PositionDotedMasterData>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            PositionDotedMasterData oData = JsonConvert.DeserializeObject<PositionDotedMasterData>(newData.ToString());

            foreach (var item in oData.PositionDotedData.PositionDotedDetail)
            {
                string oExceptionError = string.Empty;
                string oErrorTextCode = string.Empty;
                string oErrorParam = string.Empty;
                if (!string.IsNullOrEmpty(item.ActionType))//เช็คว่า ActionType ต้องเกิดขึ้นถึงจะบันทึกข้อมูลลง MarkData
                {
                    string dataCategory = string.Format("HROMPOSITION_{0}_{1}", item.HeadPosCode, item.PosCode); //HROMPOSITION_HEADPOSCODE_POSCODE

                    #region Check MarkData
                    string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
                    string oCondition = "=";
                    //Check Org
                    string oDataCategory = String.Format("HROMORGANIZATION_{0}", item.UnitCode);
                    oCondition = "%";
                    if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                        && oDataCategory != "HROMORGANIZATION_") // prevent multiple new unit
                        throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_UNITCODE", item.HeadOfOrganizationName);

                    //check Head PosCode
                    oDataCategory = String.Format("HROMPOSITION_{0}", item.HeadPosCode);
                    oCondition = "%";
                    if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                        && !String.IsNullOrEmpty(item.HeadPosCode))
                        throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_HEAD_POSCODE");

                    //check PosCode
                    oDataCategory = String.Format("HROMPOSITION_{0}", item.PosCode);
                    oCondition = "%";
                    if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                        && !String.IsNullOrEmpty(item.HeadPosCode))
                        throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_POSCODE");
                    #endregion

                    if (RequestNo.ToUpper() == "DUMMY")
                    {
                        if (HROMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(dataCategory))
                        {
                            throw new DataServiceException("HROM", "REQUEST_DUPLICATE", "Request duplicate");
                        }
                    }
                    else
                    {
                        if (HROMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(dataCategory, RequestNo))
                        {
                            throw new DataServiceException("HROM", "REQUEST_DUPLICATE", "Request duplicate");
                        }
                    }
                }

                bool result = true;
                if(!string.IsNullOrEmpty(item.ActionType))
                    result = ServiceManager.CreateInstance(Requestor.CompanyCode, "DHR").DataService.SaveWithValidateDotedLineData(item, "V", out oExceptionError, out oErrorTextCode, out oErrorParam);

                if (!result)
                {
                    if (!string.IsNullOrEmpty(oErrorTextCode))
                    {
                        string[] param = !string.IsNullOrEmpty(oErrorParam) ? oErrorParam.Split('|') : null;
                        throw new DataServiceException("VALIDATION", oErrorTextCode, param);
                    }
                    else if (!string.IsNullOrEmpty(oExceptionError))
                    {
                        throw new DataServiceException("HROM_EXCEPTION", "POSITION_VALIDATE_OTHER", oExceptionError);
                    }
                    else
                    {
                        throw new DataServiceException("HROM_EXCEPTION", "POSITION_EXCEPTION");
                    }

                }
            }

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            PositionDotedMasterData oData = JsonConvert.DeserializeObject<PositionDotedMasterData>(Data.ToString());
            ValidateData(Data, Requestor, RequestNo, DateTime.Now, 0);
            foreach (var item in oData.PositionDotedData.PositionDotedDetail)
            {
                string oExceptionError = string.Empty;
                string oErrorTextCode = string.Empty;
                string oErrorParam = string.Empty;
                if (!string.IsNullOrEmpty(item.ActionType))//เช็คว่า ActionType ต้องเกิดขึ้นถึงจะบันทึกข้อมูลลง MarkData
                {
                    string dataCategory = string.Format("HROMPOSITION_{0}_{1}", item.HeadPosCode, item.PosCode); //HROMPOSITION_DOTED_HEADPOSCODE_POSCODE
                    HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
                    if (State == "COMPLETED")
                    {
                        try
                        {
                            //  save something to dhr
                            bool result = ServiceManager.CreateInstance(Requestor.CompanyCode, "DHR").DataService.SaveWithValidateDotedLineData(item, "S", out oExceptionError, out oErrorTextCode, out oErrorParam);

                            if (!result)
                            {
                                if (!string.IsNullOrEmpty(oErrorTextCode))
                                {
                                    string[] param = !string.IsNullOrEmpty(oErrorParam) ? oErrorParam.Split('|') : null;
                                    throw new DataServiceException("VALIDATION", oErrorTextCode, param);
                                }
                                else if (!string.IsNullOrEmpty(oExceptionError))
                                {
                                    throw new DataServiceException("HROM_EXCEPTION", "POSITION_VALIDATE_OTHER", oExceptionError);
                                }
                                else
                                {
                                    throw new DataServiceException("HROM_EXCEPTION", "POSITION_EXCEPTION");
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new DataServiceException("HROM_EXCEPTION", "SAVE_EXTERNAL_ERROR " + ex.Message);
                        }
                    }

                }
            }
        }

    }
}
