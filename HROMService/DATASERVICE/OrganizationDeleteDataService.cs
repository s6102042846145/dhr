﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;

namespace ESS.HR.OM.DATASERVICE
{
   class OrganizationDeleteDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            OrganizationCenter oReturn = new OrganizationCenter();
            if (!String.IsNullOrEmpty(CreateParam))
            {
                string oOrgID = CreateParam.Split('|')[0];
                DateTime oBegDate = DateTime.ParseExact(CreateParam.Split('|')[1], "ddMMyyyy", null);
                DateTime oEndDate = DateTime.ParseExact(CreateParam.Split('|')[2], "ddMMyyyy", null);
                oReturn = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", oOrgID, oBegDate, oEndDate).FirstOrDefault();
            }
            return oReturn;
        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {
            
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            OrganizationCenter Organization = JsonConvert.DeserializeObject<OrganizationCenter>(newData.ToString());
            string oActionType = "D",
                oServiceType = "V",
                DataSource = HROMServices.SVC(Requestor.CompanyCode).GET_OMVALIDATE_SOURCE;

            #region Check MarkData
            string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
            string oCondition = "=";
            //Check Org
            string oDataCategory = String.Format("HROMORGANIZATION_{0}", Organization.OrganizationID);
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORG");

            //check Pos
            oDataCategory = String.Format("HROMPOSITION_{0}", Organization.OrganizationID);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORGPOS");
            #endregion

            try
            {
                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSource).DataService.ValidateOrganizationData(Organization, oActionType, oServiceType);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                    //DataServiceException("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            OrganizationCenter Organization = JsonConvert.DeserializeObject<OrganizationCenter>(Data.ToString());
            string dataCategoryOrg = string.Format("HROMORGANIZATION_{0}", Organization.OrganizationID);
            
            if (State == "COMPLETED")
            {
                try
                {
                    HROMManagement.CreateInstance(Requestor.CompanyCode).SaveOrganizationData(Organization, "D", "S");
                    HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                        //DataServiceException("HROM_EXCEPTION", "OM_DELETE_SAVE");
                }
            }
            else
            {
                HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            }
        }

    }
}
