﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM
{
    public class PositionCenter : AbstractObject
    {
        
        public string ID { get; set; }
        public string CompanyCode { get; set; }
        public string HeadCode { get; set; }
        public string PosCode { get; set; }
        public string PosShortName { get; set; }
        public string PosName { get; set; }
        public string PosShortNameEN { get; set; }
        public string PosNameEN { get; set; }
        public string HeadOfUnitFlag { get; set; }
        public string BandCode { get; set; }
        public string BandValue { get; set; }
        public string BandValueTextTH { get; set; }
        public string BandValueTextEN { get; set; }
        public string UnitCode { get; set; }
        public string UnitTextTH { get; set; }
        public string UnitTextEN { get; set; }
        public string EmpOfPosTextTH { get; set; }
        public string EmpOfPosTextEN { get; set; }
        public string PositionTextTH { get; set; }
        public string PositionTextEN { get; set; }
        public string BandTextTH { get; set; }
        public string BandTextEN { get; set; }
        public string ActionType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public PositionCenter()
        {
            StartDate = default(DateTime);
            EndDate = default(DateTime);
            UpdateDate = default(DateTime);
        }
    }
}
