﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM
{
    public class PositionDotedCenter : AbstractObject
    {
        public string HeadData_UnitCode { get; set; }
        public string HeadData_HeadPosCode { get; set; }
        public DateTime? HeadData_StartDate { get; set; }
        public DateTime? HeadData_EndDate { get; set; }
       public List<PositionDotedDetailCenter> PositionDotedDetail { get; set; }
    }
}
