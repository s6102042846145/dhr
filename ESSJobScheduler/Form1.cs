using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;
using ESS.DATA.INTERFACE;
using ESS.JOB;
using ESS.MAIL.DATACLASS;
using ESS.WORKFLOW;

namespace ESS.JOBSCHEDULER
{
    public partial class Form1 : Form
    {
        private TreeNode __selectedNode;

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void eXITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButton1_Click(this, EventArgs.Empty);
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (__selectedNode == e.Node)
            {
                return;
            }
            __selectedNode = e.Node;
            this.listView1.Items.Clear();
            this.listView1.Columns.Clear();
            if (e.Node.Name == "NodeMail")
            {
                LoadMail();
            }
        }

        private void LoadMail()
        {
            foreach (PropertyInfo prop in typeof(WorkflowMail).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                this.listView1.Columns.Add(prop.Name);
            }
            //foreach (WorkflowMail mail in WorkflowMail.GetAllMail())
            //{
            //    ListViewItem oLVItem = null;
            //    foreach (ColumnHeader col in this.listView1.Columns)
            //    {
            //        PropertyInfo prop = mail.GetType().GetProperty(col.Text, BindingFlags.Public | BindingFlags.Instance);
            //        string valueText = "";
            //        if (prop.Name == "MailTo")
            //        {
            //            foreach (WorkflowMailTo mailTo in mail.MailTo)
            //            {
            //                valueText += mailTo.MailTo + ",";
            //            }
            //            valueText = valueText.TrimEnd(',');
            //        }
            //        else if (prop.CanRead && prop.CanWrite)
            //        {
            //            if (prop != null)
            //            {
            //                valueText = prop.GetValue(mail, null).ToString();
            //            }
            //            else
            //            {
            //                valueText = "";
            //            }
            //        }
            //        if (oLVItem == null)
            //        {
            //            oLVItem = this.listView1.Items.Add(valueText);
            //        }
            //        else
            //        {
            //            oLVItem.SubItems.Add(valueText);
            //        }
            //    }
            //    oLVItem.Tag = mail;
            //}
        }

        private void contextMail_Opening(object sender, CancelEventArgs e)
        {
            this.mnuSend.Enabled = false;
            if (this.listView1.SelectedItems.Count > 0)
            {
                if (this.listView1.SelectedItems[0].Tag is WorkflowMail)
                {
                    //WorkflowMail mail = (WorkflowMail)this.listView1.SelectedItems[0].Tag;
                    this.mnuSend.Enabled = true;
                }
            }
        }

        private void mnuSend_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                if (this.listView1.SelectedItems[0].Tag is WorkflowMail)
                {
                    WorkflowMail mail = (WorkflowMail)this.listView1.SelectedItems[0].Tag;
                    mail.Send();
                }
            }
        }

        private void btnTestRunJob_Click(object sender, EventArgs e)
        {
            JobManagement.CreateInstance("0000").RunJob();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //WorkflowIdentity iden = WorkflowIdentity.CreateInstance("Dummy").GetIdentity("10480058", "taiw1981","", "");
            //WorkflowPrinciple principle = new WorkflowPrinciple(iden);
            //System.Threading.Thread.CurrentPrincipal = principle;
            //RequestBoxFilter oFilter = new RequestBoxFilter();
            //oFilter.RequestTypeID = 1;

            //SqlDataAdapter oAdapter = new SqlDataAdapter("select * from RequestDocument where IsCompleted = 1 and RequestTypeID = 1 and CreatedDate > '2009-08-20'", "server=schhronline;uid=hronlineagent;pwd=hrweb;database=iService2_Prd");
            //DataTable oTable = new DataTable();
            //oAdapter.Fill(oTable);
            //oAdapter.Dispose();
            //System.IO.StreamWriter sw = System.IO.File.CreateText("ot.txt");
            //foreach (DataRow dr in oTable.Rows)
            //{
            //    string reqNo = (string)dr["RequestNo"];
            //    string companyCode = (string)dr["RequestorCompanyCode"];
            //    RequestDocument doc = RequestDocument.LoadDocument(reqNo, companyCode,"", false, true);
            //    ESS.HR.TM.DATASERVICE.DailyOTService oService = new ESS.HR.TM.DATASERVICE.DailyOTService();
            //    doc.SetDataService((IDataService)oService);
            //    //List<DailyOT> dList = doc.Additional.Tables["DAILYOT"].ToList<DailyOT>();
            //    //foreach (DataRow dr1 in doc.Additional.Tables["DAILYOT"].Rows)
            //    //{
            //    //    DailyOT dot = new DailyOT();
            //    //    dot.ParseToObject(dr1);
            //    //    if (dot.OTItemTypeID == 0)
            //    //    {
            //    //        sw.WriteLine(dot.ToString() + "|" + dot.OTItemTypeID.ToString());
            //    //    }
            //    //}
            //}
            //sw.Close();
            //sw.Dispose();
        }
    }
}