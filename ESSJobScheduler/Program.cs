using System;
using System.Windows.Forms;
using ESS.JOB;
using ESS.WORKFLOW;
using ESS.SHAREDATASERVICE;
using System.Collections.Generic;
using ESS.SHAREDATASERVICE.DATACLASS;
using System.Configuration;
using System.Reflection;

namespace ESS.JOBSCHEDULER
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Console.WriteLine("ESS Job Scheduler V1.0");
            if (args.Length > 0)
            {
                try
                {
                    if (args[0].ToUpper() == "RUNJOB")
                    {
                        // ESS.JobScheduler.exe runjob => check and run job on every active company
                        if (args.Length == 1)
                        {
                            List<Company> lstCompany = ShareDataManagement.GetCompanyList();
                            foreach(Company oCompany in lstCompany)
                            {
                                RunJob(oCompany.CompanyCode, -1);
                            }
                        }
                        // ESS.JobScheduler.exe runjob 0013 => check and run job on every active company
                        else if (args.Length == 2)
                        {
                            string sCompanyCode = args[1];
                            RunJob(sCompanyCode, -1);
                        }
                        // ESS.JobScheduler.exe runjob 0013 1
                        else if (args.Length >= 3)
                        {
                            string sCompanyCode = args[1];
                            int iJobID = int.Parse(args[2]);
                            RunJob(sCompanyCode, iJobID);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Pass invalid argument");
                        Console.WriteLine("Syntax: ESS.JobScheduler.exe runjob [CompanyCode] [JobID]");
                        Console.WriteLine("Example:");
                        Console.WriteLine("   ESS.JobScheduler.exe                => start a scheduler windows (not working)");
                        Console.WriteLine("   ESS.JobScheduler.exe runjob         => check and run job on every active company");
                        Console.WriteLine("   ESS.JobScheduler.exe runjob 0013    => check and run job on company 0013");
                        Console.WriteLine("   ESS.JobScheduler.exe runjob 0013 1  => run job 1 on company 0013");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }

        private static void RunJob(string sCompanyCode, int iJobID)
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            WorkflowIdentity iden = WorkflowIdentity.CreateInstance(sCompanyCode).GetIdentity("MYADMIN", "", sCompanyCode, true, "");
            WorkflowPrinciple principle = new WorkflowPrinciple(iden);
            System.Threading.Thread.CurrentPrincipal = principle;
            Console.WriteLine("Company Code: " + sCompanyCode);
            if (iJobID >= 0)
            {
                JobManagement.CreateInstance(sCompanyCode).RunJob(iJobID);
            }
            else
            {
                JobManagement.CreateInstance(sCompanyCode).RunJob();
            }
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Console.WriteLine(e.Exception.ToString());
        }
    }
}