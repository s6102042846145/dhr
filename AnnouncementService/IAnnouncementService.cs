using System;
using System.Collections.Generic;
using System.Text;
using ESS.ANNOUNCEMENT;
using System.Data;
using ESS.ANNOUNCEMENT.DATACLASS;

namespace ESS.ANNOUNCEMENT
{
    public interface IAnnouncementService
    {
        List<AnnouncementData> GetAnnoncementList();
        DataTable GetData();
        void Update(AnnouncementData data);
        void Insert(AnnouncementData data);
        void Delete(int id);
        AnnouncementData AnnouncementGetByID(int id);
        List<AnnouncementData> AnnouncementGetAll();
        List<AnnouncementData> AnnouncementGetActive();
        DataTable AnnouncementCheckDupplicate(AnnouncementData data);
        void AnnouncementSave(AnnouncementData data);
    }
}
