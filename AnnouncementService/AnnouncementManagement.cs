#region Using

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.DATA.FILE;
using ESS.SHAREDATASERVICE;

#endregion Using

namespace ESS.ANNOUNCEMENT
{
    public class AnnouncementManagement
    {
        #region Constructor
        private AnnouncementManagement()
        {

        }
        #endregion 

        #region MultiCompany  Framework
        private static string ModuleID = "ESS.ANNOUNCEMENT";

        public string CompanyCode { get; set; }

        public static AnnouncementManagement CreateInstance(string oCompanyCode)
        {
            AnnouncementManagement oAnnouncementManagement = new AnnouncementManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAnnouncementManagement;
        }
        #endregion MultiCompany  Framework

        #region Properties
        public CultureInfo oCultureInfo 
        { 
            get 
            { 
                return CultureInfo.GetCultureInfo(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CultureInfo")); 
            } 
        }
        public string SqlConnectionString
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BaseConStr");
            }
        }
        #endregion Properties

        #region Function
        //public List<AnnouncementData> GetAnnoncementList()
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnoncementList();
        //}

        public void acceptAnnouncement(string employeeID, string announcementID, string statusType)
        {
            string DataSource = ANNOUNCEMENTServices.SVC(CompanyCode).SAVE_EMPLOYEE_ACCEPTANNOUNCEMENT;
            ServiceManager.CreateInstance(CompanyCode, DataSource).AnnouncementService.acceptAnnouncement(employeeID, announcementID, statusType);
        }

        public void announcementSave(AnnouncementData announcement)
        {
            string DataSource = ANNOUNCEMENTServices.SVC(CompanyCode).SAVE_ANNOUNCEMENT;
            ServiceManager.CreateInstance(CompanyCode, DataSource).AnnouncementService.announcementSave(announcement);
        }

        public void announcementDelete(int announcementID)
        {
            string DataSource = ANNOUNCEMENTServices.SVC(CompanyCode).SET_DELETE_ANNOUNCEMENT_ID;
            ServiceManager.CreateInstance(CompanyCode, DataSource).AnnouncementService.announcementDelete(announcementID);
        }

        public void announcementUpdateState(int oID, string oState)
        {
            string DataSource = ANNOUNCEMENTServices.SVC(CompanyCode).SET_ANNOUNCEMENT_UPDATE_STATE;
            ServiceManager.CreateInstance(CompanyCode, DataSource).AnnouncementService.announcementUpdateState(oID, oState);
        }

        public List<AnnouncementData> GetAnnoncementList()
        {
            string DataSource = ANNOUNCEMENTServices.SVC(CompanyCode).GET_ANNOUNCEMENTLIST;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).AnnouncementService.GetAnnoncementList();
        }

        public void announcementDeleteImage(string oImageName)
        {

        }

        public void announcementUploadImage(FileAttachment oAnnounce)
        {
            //Nattawat S. 
            //string strPathAnnounce = ShareDataManagement.LookupCache(CompanyCode, "ESS.ANNOUNCEMENT", "IMAGE_PATH");
            string strPathAnnounce = ShareDataManagement.LookupCache(CompanyCode, "ESS.ANNOUNCEMENT", "IMAGE_PATH") + CompanyCode +"/";
            string strRootPath = System.Web.HttpContext.Current.Server.MapPath(strPathAnnounce);
            //EMPLOYEE_PHOTO_PATH

            FileAttachment newatt = new FileAttachment();
            int FileID = 0;
            newatt.FileID = FileID + 1;
            newatt.FileSetID = oAnnounce.FileSetID;
            newatt.FileName = oAnnounce.FileName;
            newatt.FileType = oAnnounce.FileType;
            newatt.Data = oAnnounce.Data;


            string filePath = strRootPath;
            // string fileFullPath = "D:/" + filePath+"/";
            bool exists = System.IO.Directory.Exists(filePath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(filePath);
            }

            if (newatt.Data != null)
            {

                try
                {
                    // Open file for reading
                    System.IO.FileStream _FileStream =
                       new System.IO.FileStream(filePath + newatt.FileName, System.IO.FileMode.Create,
                                                System.IO.FileAccess.Write);
                    // Writes a block of bytes to this stream using data from
                    // a byte array.
                    _FileStream.Write(newatt.Data, 0, newatt.Data.Length);

                    // close file stream
                    _FileStream.Close();
                }
                catch (Exception _Exception)
                {
                    // Error
                    Console.WriteLine("Exception caught in process: {0}",
                                      _Exception.ToString());
                }
            }
        }

        public void announcementDeleteByRequestNo(string RequestNo)
        {
            string DataSource = ANNOUNCEMENTServices.SVC(CompanyCode).SET_DELETE_ANNOUNCEMENT_ID;
            ServiceManager.CreateInstance(CompanyCode, DataSource).AnnouncementService.announcementDeleteByRequestNo(RequestNo);
        }

        #endregion Function

    }
}