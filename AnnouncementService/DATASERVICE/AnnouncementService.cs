﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.DATA.ABSTRACT;
using ESS.DATA;
using ESS.EMPLOYEE;
using System.Globalization;
using ESS.ANNOUNCEMENT.DATACLASS;
using Newtonsoft.Json;

namespace ESS.ANNOUNCEMENT.DATASERVICE
{
    public class AnnouncementService : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");

        //public string ImagePath
        //{
        //    get
        //    {
        //        return ServiceManager.ANNOUNCEMENT_PATH;
        //    }
        //}

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            AnnouncementData oAnnouncementData = JsonConvert.DeserializeObject<AnnouncementData>(Data.ToString());
            AnnouncementManagement oAnnouncementManagement = AnnouncementManagement.CreateInstance(Requestor.CompanyCode);
            if (State == "DRAFT")
            {
                //announce is edit state
                if (oAnnouncementData.ID > -1)
                {
                    string oState = RequestNo;
                    oAnnouncementManagement.announcementUpdateState(oAnnouncementData.ID, oState);
                }
                //announce upload image
                if (oAnnouncementData.FileAttach.Data != null)
                {
                    oAnnouncementManagement.announcementUploadImage(oAnnouncementData.FileAttach);
                }
            }
            else if (State == "CANCELLED")
            {
                //announce is edit state
                if (oAnnouncementData.ID > -1)
                {
                    string oState = "COMPLETED";
                    oAnnouncementManagement.announcementUpdateState(oAnnouncementData.ID , oState);
                }
                //announce delete image
            }
            else if (State == "COMPLETED")
            {
                oAnnouncementData.RequestNo = RequestNo;
                oAnnouncementData.Type = "COMPLETED";
                oAnnouncementManagement.announcementSave(oAnnouncementData);
            }
            
        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            AnnouncementData announces = new AnnouncementData();

            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] param = CreateParam.Split('|');
                List<AnnouncementData> tmp = new List<AnnouncementData>();
                tmp = AnnouncementManagement.CreateInstance(Requestor.CompanyCode).GetAnnoncementList().Where(x => x.ID == Convert.ToInt16(param[1])).ToList();
                tmp[0].FileAttach = new DATA.FILE.FileAttachment();
                announces = tmp[0];
            }
            else
            {
                announces.ID = -1;
                announces.Name = string.Empty;
                announces.Content = string.Empty;
                announces.Image = string.Empty;
                announces.Priority = 0;
                announces.NewFlagDate = DateTime.Now;
                announces.BeginDate = DateTime.Now;
                announces.EndDate = DateTime.Now;
                announces.RequestNo = "";
                announces.Type = "INPROGRESS";
                announces.Acceptable = false;
                announces.FileAttach = new DATA.FILE.FileAttachment();
            }

            return announces;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {
            //throw new NotImplementedException();
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //throw new NotImplementedException();
        }
    }
}
