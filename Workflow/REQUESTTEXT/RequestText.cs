using System.Collections.Generic;
using System.Reflection;
using ESS.SHAREDATASERVICE;
namespace ESS.WORKFLOW
{
    public class RequestText
    {
        #region Constructor
        private RequestText()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, RequestText> Cache = new Dictionary<string, RequestText>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static RequestText CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new RequestText()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            RequestText oRequestText = new RequestText()
            {
                CompanyCode = oCompanyCode
            };
            return oRequestText;
        }
        #endregion MultiCompany  Framework

        public string LoadText(string Category, string Language, string Code)
        {
            string cReturn = ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestText(Category, Language, Code);
            if (cReturn == "")
            {
                cReturn = string.Format("{0}_{1}_{2}", Language, Category, Code);
            }
            return cReturn;
        }
    }
}