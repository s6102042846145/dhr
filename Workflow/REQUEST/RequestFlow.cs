using System;
using System.Collections.Generic;

namespace ESS.WORKFLOW
{
    public class RequestFlow
    {
        public RequestFlow()
        {
        }
        public string ActionBy { get; set; }
        public string ActionByCode { get; set; }
        public string ActionByPosition { get; set; }
        public string ActionByCompanyCode { get; set; }
        public string ActionByPositionName { get; set; }
        public string Receipient { get; set; }
        public string ReceipientCode { get; set; }
        public string ReceipientPosition { get; set; }
        public string ReceipientCompanyCode { get; set; }
        public string ReceipientPositionName { get; set; }

        public DateTime ActionDate { get; set; }
        public string ActionCode { get; set; }
        public bool IsCurrentItem { get; set; }
        public bool IsEndPoint { get; set; }
        public string Comment { get; set; }
        public string Detail { get; set; }
        public string GroupName { get; set; }
        public bool IsSystem { get; set; }
        public bool IsExternalUser { get; set; }
        public int FlowID { get; set; }
        public int FlowItemID { get; set; }
        public string OptionalStatus { get; set; }
        public string Comment2 { get; set; }
    }
}