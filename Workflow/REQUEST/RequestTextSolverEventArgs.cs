using System;

namespace ESS.WORKFLOW
{
    public delegate void RequestTextSolverHandler(RequestTextSolverEventArgs e);
    public class RequestTextSolverEventArgs : EventArgs
    {
        public RequestTextSolverEventArgs(string className, object value)
        {
            ClassName = className;
            Value = value;
            ResolveText = Value.ToString();
        }

        public string ClassName { get; private set; }
        public object Value { get; private set; }
        public string ResolveText { get; set; }
        public bool IsHandler { get; set; }
    }
}