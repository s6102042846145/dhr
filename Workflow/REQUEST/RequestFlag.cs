using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW
{
    public class RequestFlag : AbstractObject
    {
        public RequestFlag()
        {
        }

        public string RequestNo { get; set; }
        public int FlagID { get; set; }
        public string FlagComment { get; set; }

        public static int URGENT = 1;
        public static int CASH_ADVANCE = 2;
        public static int VAT7 = 3;
        public static int WAITFORREVERSE = 4;
        public static int WAITFORREVERSEANDREJECT = 5;
        public static int REVERSEANDREJECT = 6;
        public static int REVERSECOMPLETEANDWAITFORREJECT = 7;
    }
}