using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Text;

namespace PTTCHEM.WORKFLOW
{
    public class RequestFlowMultipleReceipient_External : AbstractObject
    {
        public RequestFlowMultipleReceipient_External()
        {
        }

        public int RequestID { get; set; }
        public string ApproverID { get; set; }
        public string ActionCode { get; set; }
        public DateTime ActionDate { get; set; }
        public string ActionBy { get; set; }
        public string Comment { get; set; }
        public string ApproverGroup { get; set; }
        public string Operation { get; set; }
        public int Sequence { get; set; }   
        public bool IsNotify { get; set; }
        public bool IsCurrent { get; set; }

        public string ApproverName { get; set; }
        public string ApproverEmail { get; set; }
        public int ContractID { get; set; }
        public string OperationFromContract { get; set; }
    }
}
