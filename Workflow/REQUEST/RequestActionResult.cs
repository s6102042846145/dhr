using PTTCHEM.WORKFLOW;
using System.Collections.Generic;
namespace ESS.WORKFLOW
{
    public class RequestActionResult
    {
        private ReceipientList __receipients = new ReceipientList();
        private bool __isCompleted = false;
        private bool __isCancelled = false;
        private bool __isWaitForEdit = false;
        private bool __goToNextItem = false;
        private int __authorizeStep = -1;
        private string __detail = "";
        private string __authorizeDelegate = "";
        private bool __isRestartFlow = false;
        private string __groupName = "";
        public int AuthorizeModelID { get; set; }
        public List<RequestFlowMultipleReceipient> RequestFlowMultipleReceipientList { get; set; }
        public List<RequestFlowMultipleReceipient_External> RequestFlowMultipleReceipient_ExternalList { get; set; }

        public RequestActionResult()
        {
        }

        public bool IsRestartFlow
        {
            get
            {
                return __isRestartFlow;
            }
            set
            {
                __isRestartFlow = value;
            }
        }

        public ReceipientList Receipients
        {
            get
            {
                return __receipients;
            }
            set
            {
                __receipients = value;
            }
        }

        public string AuthorizeDelegate
        {
            get
            {
                return __authorizeDelegate;
            }
            set
            {
                __authorizeDelegate = value;
            }
        }

        public bool IsCompleted
        {
            get
            {
                return __isCompleted;
            }
            set
            {
                __isCompleted = value;
            }
        }

        public bool IsCancelled
        {
            get
            {
                return __isCancelled;
            }
            set
            {
                __isCancelled = value;
            }
        }

        public bool IsWaitForEdit
        {
            get
            {
                return __isWaitForEdit;
            }
            set
            {
                __isWaitForEdit = value;
            }
        }

        public bool GoToNextItem
        {
            get
            {
                return __goToNextItem;
            }
            set
            {
                __goToNextItem = value;
            }
        }

        public int AuthorizeStep
        {
            get
            {
                return __authorizeStep;
            }
            set
            {
                __authorizeStep = value;
            }
        }

        public string Detail
        {
            get
            {
                return __detail;
            }
            set
            {
                __detail = value;
            }
        }

        public string GroupName
        {
            get
            {
                return __groupName;
            }
            set
            {
                __groupName = value;
            }
        }

        public int FlowID { get; set; }

        public bool IsAutomatic { get; set; }
    }
}