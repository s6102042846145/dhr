using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.DATA.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class RequestAction : IActionData
    {
        #region Constructor
        private RequestAction()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, RequestAction> Cache = new Dictionary<string, RequestAction>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static RequestAction CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new RequestAction()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            RequestAction oRequestAction = new RequestAction()
            {
                CompanyCode = oCompanyCode
            };
            return oRequestAction;
        }
        #endregion MultiCompany  Framework

        public string Code { get; set; }

        public int ActionTypeID { get; set; }

        public ActionType ActionType
        {
            get
            {
                //if (__actionType == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetActionType(this.ActionTypeID);
                //}
                //return __actionType;
            }
        }

    }
}