using System.Collections.Generic;
using ESS.DATA.FILE;
using ESS.WORKFLOW;
using ESS.SHAREDATASERVICE;
using System.Reflection;

namespace ESS.FILE
{
    public class FileAttachmentSystemManagement
    {
        #region Constructor
        public string CompanyCode { get; set; }

        public static FileAttachmentSystemManagement CreateInstance(string oCompanyCode)
        {
            FileAttachmentSystemManagement oFileAttachmentSystemManagement = new FileAttachmentSystemManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oFileAttachmentSystemManagement;
        }

        #endregion 
        public void SaveFileSet(FileAttachmentSet fileSet)
        {
            WORKFLOW.ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.SaveFileAttachment(fileSet);
        }

        public FileAttachmentSet LoadFileSet(int FileSetID)
        {
            return WORKFLOW.ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.LoadFileAttachment(FileSetID);
        }

        public FileAttachmentSet LoadFileSet(int FileSetID, string CompanyCode)
        {
            return WORKFLOW.ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.LoadFileAttachment(FileSetID);
        }

    }
}