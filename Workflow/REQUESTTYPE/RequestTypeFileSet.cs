using ESS.DATA.FILE;
using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW
{
    public class RequestTypeFileSet : AbstractObject
    {
        private string __employeeID = "";
        private int __requestTypeID = -1;
        private int __fileSetID = -1;
        private string __requestSubType = "";

        public RequestTypeFileSet()
        {
        }
        public string EmployeeID { get; set; }
        public int RequestTypeID { get; set; }
        public int FileSetID{ get; set; }
        public string RequestSubType { get; set; }
        public static FileAttachmentSet GetRequestTypeFileSet(string employeeID, int requestTypeID, string requestSubType)
        {
            return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).WorkflowService.GetRequestTypeFileSet(employeeID, requestTypeID, requestSubType);
        }
        public static void UpdateRequestTypeFileSet(FileAttachmentSet NewFileSet)
        {
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).WorkflowService.UpdateRequestTypeFileSet(NewFileSet);
        }
    }
}