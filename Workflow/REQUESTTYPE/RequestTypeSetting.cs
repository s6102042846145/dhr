using System.Collections.Generic;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    public class RequestTypeSetting : BaseCodeObject
    {
        #region Constructor
        private RequestTypeSetting()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, RequestTypeSetting> Cache = new Dictionary<string, RequestTypeSetting>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static RequestTypeSetting CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new RequestTypeSetting()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            RequestTypeSetting oRequestTypeSetting = new RequestTypeSetting()
            {
                CompanyCode = oCompanyCode
            };
            return oRequestTypeSetting;
        }
        #endregion MultiCompany  Framework

        public int RequestTypeID { get; set; }
        public int VersionID { get; set; }
        public int FlowID { get; set; }
        public Flow Flow
        {
            get
            {
                //if (__flow == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFlow(FlowID);
                //}
                //return __flow;
            }
        }

        public List<RequestTypeParam> Parameters
        {
            get
            {
                //if (__params == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeParameters(this.RequestTypeID, this.VersionID, this.ID);
                //}
                //return __params;
            }
        }
    }
}