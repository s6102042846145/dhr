using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class RequestType : AbstractObject
    {
        #region Constructor
        public RequestType()
        {
            this.ID = -1;
        }
        #endregion

        #region MultiCompany  Framework
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";

        public static RequestType CreateInstance(string oCompanyCode)
        {
            RequestType oRequestType = new RequestType()
            {
                CompanyCode = oCompanyCode
            };
            return oRequestType;
        }
       
        #endregion MultiCompany  Framework

        protected override void SetDataRowToObject(PropertyInfo prop, DataRow dr)
        {
            if (prop.Name == "ID")
            {
                this.ID = (int)dr["RequestTypeID"];
            }
            else if (prop.Name == "Code")
            {
                this.Code = (string)dr["RequestTypeCode"];
            }
            else if (prop.Name == "Prefix")
            {
                this.Prefix = (string)dr["RequestTypePrefix"];
            }
            base.SetDataRowToObject(prop, dr);
        }

        public List<RequestType> GetAllRequestType()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllRequestTypes();
        }

        public RequestType GetRequestType(int RequestTypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestType(RequestTypeID);
        }

        public List<UserRoleResponseSetting> LoadUserResponse()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeResponse(this.ID);
        }

        public string Name(string LanguageCode)
        {
            return RequestText.CreateInstance(CompanyCode).LoadText("REQUESTTYPE", LanguageCode, this.Code);
        }

        public ReceipientList RequestTypeOwner
        {
            get
            {
                //if (__owner == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeOwner(this.ID);
                //}
                //return __owner;
            }
        }

        public string Code { get; set; }

        public int ID { get; set; }

        #region " Properties "

        public bool CanSave { get; set; }
        public bool ShowComment { get; set; }

        public string Prefix { get; set; }

        public int VersionID { get; set; }
        public string VersionCode { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<RequestTypeSchema> Schemas
        {
            get
            {
                //if (__schemas == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeSchemas(this.ID, this.VersionID);
                //}
                //return __schemas;
            }
        }

        public List<RequestTypeKey> Keys
        {
            get
            {
                //if (__keys == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeKeys(this.ID, this.VersionID);
                //}
                //return __keys;
            }
        }

        public List<RequestTypeSetting> Settings
        {
            get
            {
                //if (__settings == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeSettings(this.ID, this.VersionID);
                //}
                //return __settings;
            }
        }

        public string SummaryText { get; set; }

        public string SummaryTextTH { get; set; }

        public string SummaryTextEN { get; set; }
        #endregion " Properties "

        public DataTable GenerateDataTable()
        {
            DataTable oTable = new DataTable("REQUESTINFO");
            foreach (RequestTypeSchema sc in this.Schemas)
            {
                DataColumn oDC = new DataColumn(sc.Code);
                switch (sc.Type)
                {
                    case ParameterType.BOOLEAN:
                        oDC.DataType = typeof(bool);
                        oDC.DefaultValue = false;
                        break;

                    case ParameterType.DATETIME:
                        oDC.DataType = typeof(DateTime);
                        oDC.DefaultValue = DBNull.Value;
                        break;

                    case ParameterType.DECIMAL:
                        oDC.DataType = typeof(Decimal);
                        oDC.DefaultValue = 0.00M;
                        break;

                    case ParameterType.INTEGER:
                        oDC.DataType = typeof(int);
                        oDC.DefaultValue = 0;
                        break;

                    case ParameterType.STRING:
                        oDC.DataType = typeof(string);
                        oDC.DefaultValue = "";
                        break;
                }
                oTable.Columns.Add(oDC);
            }
            return oTable;
        }
    }
}