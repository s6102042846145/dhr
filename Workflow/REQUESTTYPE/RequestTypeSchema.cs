using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    public class RequestTypeSchema : BaseCodeObject
    {
        private ParameterType __type = ParameterType.STRING;

        public RequestTypeSchema()
        {
        }

        public ParameterType Type
        {
            get
            {
                return __type;
            }
            set
            {
                __type = value;
            }
        }
    }
}