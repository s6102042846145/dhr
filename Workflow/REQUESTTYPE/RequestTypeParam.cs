using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    public class RequestTypeParam : BaseObject
    {
        private ParameterMode __mode;
        private string __value;

        public RequestTypeParam()
        {
        }

        public ParameterMode Mode
        {
            get
            {
                return __mode;
            }
            set
            {
                __mode = value;
            }
        }

        public string Value
        {
            get
            {
                return __value;
            }
            set
            {
                __value = value;
            }
        }
    }
}