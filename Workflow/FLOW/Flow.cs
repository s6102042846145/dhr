using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class Flow : BaseCodeObject
    {
        #region Constructor
        public Flow()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, Flow> Cache = new Dictionary<string, Flow>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static Flow CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new Flow()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            Flow oFlow = new Flow()
            {
                CompanyCode = oCompanyCode
            };
            return oFlow;
        }
        #endregion MultiCompany  Framework


        #region " Property "

        public List<FlowItem> Items
        {
            get
            {
                //if (__Items == null)
                //{
                    return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFlowItems(this.ID);
                //}
                //return __Items;
            }
        }

        public List<FlowParameter> Parameters
        {
            get
            {
                //if (__Params == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFlowParameter(this.ID);
                //}
                //return __Params;
            }
        }

        public int ContinueItemID { get; set; }
        public int EditItemID { get; set; }

        public int CancelItemID { get; set; }
        public int CreateItemID { get; set; }
        public int OwnerViewItemID { get; set; }
        public int DataOwnerItemID { get; set; }

        public FlowItem DataOwnerView
        {
            get
            {
                return FlowItem.CreateInstance(CompanyCode).GetFlowItem(this.ID, this.DataOwnerItemID);
            }
        }

        public FlowItem OwnerView
        {
            get
            {
                return FlowItem.CreateInstance(CompanyCode).GetFlowItem(this.ID, this.OwnerViewItemID);
            }
        }
        public int TimeoutItemID { get; set; }

        #endregion " Property "

        public override int GetHashCode()
        {
            string cCode = string.Format("Flow_{0}", this.ID);
            return cCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj.GetHashCode() == this.GetHashCode();
        }
        #region " Method "

        public List<Flow> GetAllFlows()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllFlows();
        }

        public Flow GetFlow(int FlowID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFlow(FlowID);
        }
        #endregion " Method "
    }
}