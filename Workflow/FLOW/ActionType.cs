using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class ActionType : BaseCodeObject
    {
        #region Constructor
        private ActionType()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, ActionType> Cache = new Dictionary<string, ActionType>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static ActionType CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new ActionType()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            ActionType oActionType = new ActionType()
            {
                CompanyCode = oCompanyCode
            };
            return oActionType;
        }
        #endregion MultiCompany  Framework
        public bool IsMarkCompleted{ get; set; }

        public bool IsMarkCancelled{ get; set; }

        public bool IsMarkWaitForEdit{ get; set; }

        public bool IsRaiseAutomaticAction { get; set; }

        public override bool Equals(object obj)
        {
            try
            {
                return ((ActionType)obj).ID == this.ID;
            }
            catch
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            string cCode = "ActionType" + this.ID.ToString();
            return cCode.GetHashCode();
        }

        public List<ActionTypeParam> Parameters
        {
            get
            {
                //if (__params == null)
                //{
                    return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetActionTypeParameters(this.ID);
                //}
                //return __params;
            }
        }
        public List<ActionType> GetAllActionTypes()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllActionTypes();
        }
        private IRequestAction GetCustomisedAction()
        {
            Type oReturn = null;
            string typeName = string.Format("{0}.REQUESTACTION.{1}",ModuleID, this.Code.ToUpper());
            oReturn = Type.GetType(typeName);
            if (oReturn != null)
            {
                return (IRequestAction)Activator.CreateInstance(oReturn);
            }
            else
            {
                return null;
            }
        }
        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params,bool IsSimulation)
        {
            RequestActionResult oResult;

            IRequestAction oAction = this.GetCustomisedAction();
            if (oAction == null)
            {
                throw new Exception(string.Format("Can't find customised action class for ActionType '{0}'", this.Code));
            }
            oResult = oAction.DoAction(Document, ActionBy, Params, IsSimulation);
            return oResult;
        }
    }
}