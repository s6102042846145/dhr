using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.DATA.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class FlowItemAction : BaseCodeObject, IActionData
    {
        #region Constructor
        public FlowItemAction()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, FlowItemAction> Cache = new Dictionary<string, FlowItemAction>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static FlowItemAction CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new FlowItemAction()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            FlowItemAction oFlowItemAction = new FlowItemAction()
            {
                CompanyCode = oCompanyCode
            };
            return oFlowItemAction;
        }
        #endregion MultiCompany  Framework


        public int FlowID { get; set; }
        public int FlowItemID { get; set; }
        public int FlowItemActionID	{ get; set; }
        public string FlowItemActionCode { get; set; }
        public string NextItemCode { get; set; }
        public int NextItemID { get; set; }
        public int ActionTypeID { get; set; }
        public int AutomaticActionTypeID { get; set; }
        public int NotifyReceipients { get; set; }
        public int NotifyRequestor { get; set; }
        public int NotifyCurrent { get; set; }
        public int NotifySpecial { get; set; }
        public int NotifyAllActionInPass { get; set; }

        public int NotifyAutomaticAction { get; set; }

        public string SpecialRole { get; set; }

        public int NotifyRequestorWithGotoNextItem { get; set; }
        public bool NotifyReceipients_CCRequestor { get; set; }
        public bool NotifyReceipients_CCCurrent { get; set; }
        public bool NotifyReceipients_CCSpecial { get; set; }
        public bool NotifyReceipients_CCAllActionInPass { get; set; }
        public string NotifyReceipients_CCSpecialRole { get; set; }
        public bool NotifyRequestor_CCCurrent { get; set; }
        public bool NotifyRequestor_CCSpecial { get; set; }
        public bool NotifyRequestor_CCAllActionInPass { get; set; }
        public string NotifyRequestor_CCSpecialRole { get; set; }
        public bool NotifyRequestorWithGotoNextItem_CCCurrent { get; set; }
        public bool NotifyRequestorWithGotoNextItem_CCSpecial { get; set; }
        public bool NotifyRequestorWithGotoNextItem_CCAllActionInPass { get; set; }
        public string NotifyRequestorWithGotoNextItem_CCSpecialRole { get; set; }
        public bool NotifyCurrent_CCRequestor { get; set; }
        public bool NotifyCurrent_CCSpecial { get; set; }
        public bool NotifyCurrent_CCAllActionInPass { get; set; }
        public string NotifyCurrent_CCSpecialRole { get; set; }
        public bool NotifySpecial_CCRequestor { get; set; }
        public bool NotifySpecial_CCCurrent { get; set; }
        public bool NotifySpecial_CCSpecial { get; set; }
        public bool NotifySpecial_CCAllActionInPass { get; set; }
        public string NotifySpecial_CCSpecialRole { get; set; }
        public bool NotifyAllActionInPass_CCRequestor { get; set; }
        public bool NotifyAllActionInPass_CCCurrent { get; set; }
        public bool NotifyAllActionInPass_CCSpecial { get; set; }
        public string NotifyAllActionInPass_CCSpecialRole { get; set; }
        public bool NotifyAutomaticAction_CCRequestor { get; set; }
        public bool NotifyAutomaticAction_CCCurrent { get; set; }
        public bool NotifyAutomaticAction_CCSpecial { get; set; }
        public bool NotifyAutomaticAction_CCAllActionInPass { get; set; }
        public string NotifyAutomaticAction_CCSpecialRole { get; set; }
        public bool AutomaticStampRequestorTobeApprover { get; set; }
        public bool StampRequestorTobeApprover { get; set; }
        public bool IsVisible { get; set; }

        public bool IsAutomaticAction { get; set; }

        public bool IsGotoNextItem { get; set; }

        public bool IsCanOverwrite { get; set; }
        public bool IsCheckFlowAfterOverwrite { get; set; }
        public bool IsCheckDataAfterOverwrite { get; set; }

        public FlowItem NextItem
        {
            get
            {
                //if (__nextItem == null)
                //{
                    return FlowItem.CreateInstance(CompanyCode).GetFlowItem(this.FlowID, this.NextItemID);
                //}
                //return __nextItem;
            }
        }

        public ActionType ActionType
        {
            get
            {
                //if (__actionType == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetActionType(ActionTypeID);
                //}
                //return __actionType;
            }
        }

        public ActionType AutomaticActionType
        {
            get
            {
                //if (__automaticActionType == null && __automaticActionTypeID != -1)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetActionType(AutomaticActionTypeID);
                //}
                //return __automaticActionType;
            }
        }

        public List<FlowItemAction> GetAllPosibleAction(int FlowID, int FlowItemID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllPossibleAction(FlowID, FlowItemID);
        }

        public FlowParameter GetMappingParameter(int ParamID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMappingParameter(FlowID, FlowItemID, ID, ParamID, NextItemCode);
        }

        public FlowParameter GetMappingAutomaticParameter(int ParamID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMappingAutomaticParameter(FlowID, FlowItemID, ID, ParamID, NextItemCode);
        }

        public string GetMailTemplate(int MailID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMailTemplateByID(MailID, LanguageCode);
        }

        public string GetMailSubject(int MailID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMailSubjectByID(MailID, LanguageCode);
        }
        internal string GetMailTemplate(string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMailTemplate(FlowID, FlowItemID, ID, LanguageCode);
        }
        internal string GetMailSubject(string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMailSubject(FlowID, FlowItemID, ID, LanguageCode);
        }

    }
}