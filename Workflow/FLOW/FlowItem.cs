using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class FlowItem : BaseCodeObject
    {
        #region Constructor
        private FlowItem()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, FlowItem> Cache = new Dictionary<string, FlowItem>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static FlowItem CreateInstance(string oCompanyCode)
        {
            FlowItem oFlowItem = new FlowItem()
            {
                CompanyCode = oCompanyCode
            };
            return oFlowItem;
        }
        #endregion MultiCompany  Framework

        public int StateID { get; set; }
        public string FlowItemCode { get; set; }
        public int FlowItemID { get; set; }

        [DefaultValue(-1)]
        public int FlowID { get; set; }
        public int CancelActionTypeID { get; set; }
        public int EditActionTypeID { get; set; }
        public bool IsCanOverwrite { get; set; }
        public bool IsCheckFlowAfterOverwrite { get; set; }
        public bool IsCheckDataAfterOverwrite { get; set; }
        public bool IsCanRequestForEdit { get; set; }
        public bool IsCanRequestForCancel { get; set; }
        public State State
        {
            get
            {
                //if (__state == null)
                //{
                    return State.CreateInstance(CompanyCode).GetState(this.StateID);
                //}
                //return __state;
            }
        }
        public string GroupName { get; set; }
        public bool IsCheckTimout { get; set; }
        public bool IsCanOverwriteForMobile { get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("FlowItem_{0}_{1}", this.FlowID, this.ID);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            try
            {
                return ((FlowItem)obj).GetHashCode() == this.GetHashCode();
            }
            catch
            {
                return false;
            }
        }

        //public List<FlowItemAction> PossibleActions
        //{
        //    get
        //    {
        //        //if (__possibleActions == null)
        //        //{
        //        return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllPossibleAction(this.FlowID, this.ID);
        //        //}
        //        //return __possibleActions;
        //    }
        //}

        public List<FlowItemAction> PossibleActions(RequestDocument Doc, EmployeeData ActionBy)
        {
            return ServiceManager.CreateInstance(Doc.RequestorCompanyCode).WorkflowService.GetAllPossibleAction(this.FlowID, this.ID, Doc, ActionBy);
        }



        public FlowItem GetFlowItem(int FlowID, int FlowItemID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFlowItem(FlowID, FlowItemID);
        }

    }
}