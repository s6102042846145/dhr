using System;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class ActionTypeParam : BaseCodeObject
    {
        private ParameterType __type;

        public ActionTypeParam()
        {
        }

        public ParameterType Type
        {
            get
            {
                return __type;
            }
            set
            {
                __type = value;
            }
        }
    }
}