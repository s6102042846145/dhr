using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW.AUTHORIZATION
{
    public class AuthorizationModelKey : AbstractObject
    {
        public AuthorizationModelKey()
        {
        }

        public int AuthModelID { get; set; }
        public int AuthConditionID { get; set; }
        public AuthorizationModelKeyMode AuthConditionMode { get; set; }
        public string AuthConditionCode { get; set; }
        public ParameterType ValueType { get; set; }
        public string ValueOperator { get; set; }
    }
}