using System.ComponentModel;
using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW.AUTHORIZATION
{
    public class AuthorizationModelSet : AbstractObject
    {

        private int __nextStep = -1;

        public AuthorizationModelSet()
        {
        }

        public int AuthModelID { get; set; }
        public int AuthModelSetID { get; set; }
        public string Detail { get; set; }
        [DefaultValue(-1)]
        public int NextStep { get; set; }

    }
}