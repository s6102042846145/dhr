using System.Collections.Generic;
using ESS.EMPLOYEE;

//CreateBy: Ratchatawan W. (2012-11-09)
namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOCUSTOMMANAGER : IRequestAction
    {
        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            string positionID = string.Empty;
            if (Params.ContainsKey("REPORTTO"))
            {
                positionID = Params["REPORTTO"];
            }
            RequestActionResult oReturn = new RequestActionResult();
            oReturn.GoToNextItem = true;

            Receipient rcp = new Receipient(ReceipientType.POSITION, positionID, Document.RequestorCompanyCode, Document.RequestID);
            oReturn.Receipients.Add(rcp);
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}