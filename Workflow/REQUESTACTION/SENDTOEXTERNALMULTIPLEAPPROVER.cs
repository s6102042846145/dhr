﻿using ESS.EMPLOYEE;
using PTTCHEM.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOEXTERNALMULTIPLEAPPROVER : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            List<RequestFlowMultipleReceipient_External> RequestFlowMultipleReceipientList = new List<RequestFlowMultipleReceipient_External>();
            RequestFlowMultipleReceipient_External oRequestFlowMultipleReceipient;
            if (Params.ContainsKey("EXTERNALMULTIPLEAPPROVER"))
            {
                foreach (string item in Params["EXTERNALMULTIPLEAPPROVER"].Split('|')) //Group1,Operator^EmployeeID1,EmployeeID2|Group2,Operator^EmployeeID3,EmployeeID4
                {
                    int Sequence = 1;
                    foreach (string approver_id in item.Split('^')[1].Split(','))
                    {
                        string ApproverGroup = item.Split('^')[0].Split(',')[0];
                        string Operation = item.Split('^')[0].Split(',')[1];
                        int ContractID = 0;
                        string OperationFromContract = Operation;
                        if (item.Split('^')[0].Split(',').Length > 2)
                        {
                            int.TryParse(item.Split('^')[0].Split(',')[2], out ContractID);
                            OperationFromContract = item.Split('^')[0].Split(',')[3];
                        }

                        if (Operation == "AND" && Sequence == 1)
                        {
                            Receipient rcp = new Receipient(ReceipientType.EXTERNAL_USER, approver_id, Document.RequestorCompanyCode, Document.RequestID);
                            oReturn.Receipients.Add(rcp);
                        }
                        else if (Operation == "OR")
                        {
                            Receipient rcp = new Receipient(ReceipientType.EXTERNAL_USER, approver_id, Document.RequestorCompanyCode, Document.RequestID);
                            oReturn.Receipients.Add(rcp);
                        }

                        if (!RequestFlowMultipleReceipientList.Exists(obj => obj.RequestID == Document.RequestID && obj.ApproverID == approver_id && obj.ApproverGroup == ApproverGroup))
                        {
                            DataTable dtExternal = EmployeeManagement.CreateInstance(Document.RequestorCompanyCode).GetExternalUserbyID(approver_id, "TH");
                            foreach (DataRow dr in dtExternal.Rows)
                            {
                                oRequestFlowMultipleReceipient = new RequestFlowMultipleReceipient_External();
                                oRequestFlowMultipleReceipient.RequestID = Document.RequestID;
                                oRequestFlowMultipleReceipient.ApproverID = approver_id;
                                oRequestFlowMultipleReceipient.ApproverName = dr["Name"].ToString();
                                oRequestFlowMultipleReceipient.ApproverEmail = dr["Email"].ToString();
                                oRequestFlowMultipleReceipient.ContractID = ContractID;
                                oRequestFlowMultipleReceipient.OperationFromContract = OperationFromContract;
                                oRequestFlowMultipleReceipient.ApproverGroup = ApproverGroup;
                                oRequestFlowMultipleReceipient.Operation = Operation;
                                oRequestFlowMultipleReceipient.Sequence = Sequence;
                                RequestFlowMultipleReceipientList.Add(oRequestFlowMultipleReceipient);
                            }
                        }
                        Sequence++;
                    }
                }
            }

            Document.RequestFlowMultipleReceipient_ExternalList = RequestFlowMultipleReceipientList;
            oReturn.RequestFlowMultipleReceipient_ExternalList = Document.RequestFlowMultipleReceipient_ExternalList;
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }
    }
}
