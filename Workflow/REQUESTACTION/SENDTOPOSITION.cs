using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOPOSITION : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            string positionID = "";
            if (Params.ContainsKey("POSITIONID"))
            {
                positionID = Params["POSITIONID"];
            }
            RequestActionResult oReturn = new RequestActionResult();
            oReturn.GoToNextItem = true;
            Receipient rcp = new Receipient(ReceipientType.POSITION, positionID, Document.RequestorCompanyCode, Document.RequestID);
            oReturn.Receipients.Add(rcp);
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }

    }
}
