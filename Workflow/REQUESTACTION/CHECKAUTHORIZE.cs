using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.WORKFLOW.AUTHORIZATION;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class CHECKAUTHORIZE : IRequestAction
    {
        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            List<RequestActionResult> Result = new List<RequestActionResult>();
            RequestActionResult oReturn = new RequestActionResult();
            Receipient rcp;
            foreach (string strAuth in Params["AUTHORIZEMODEL"].ToString().Split(','))
            {
                oReturn = new RequestActionResult();
                int AuthorizeModelID = -1;
                //if (Params.ContainsKey("AUTHORIZEMODEL"))
                //{
                int.TryParse(strAuth, out AuthorizeModelID);
                // }
                int AuthorizeStepID = Document.AuthorizeStep;
                oReturn.AuthorizeModelID = AuthorizeModelID;
                bool isEnd = false;
                while (!isEnd)
                {
                    oReturn = new RequestActionResult();
                    isEnd = true;
                    AuthorizationModel oModel = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetModel(AuthorizeModelID);

                    List<AuthorizationModelSet> auths;
                    if (ActionBy == null || ActionBy.EmployeeID == Document.Requestor.EmployeeID)
                    {
                        auths = new List<AuthorizationModelSet>();
                    }
                    else
                    {
                        auths = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).CalculateAuthorize(ActionBy, Document, oModel.AuthModelID, oModel.Keys);
                    }
                    if (auths.Count > 0 && auths[0].NextStep == -1)
                    {
                        oReturn.GoToNextItem = true;
                        oReturn.IsCompleted = true;
                        oReturn.Detail = auths[0].Detail;
                    }
                    else
                    {
                        oReturn.GoToNextItem = false;
                        oReturn.IsCompleted = false;
                        if (auths.Count > 0 && auths[0].NextStep > -1)
                        {
                            oReturn.AuthorizeStep = auths[0].NextStep;
                        }
                        else
                        {
                            oReturn.AuthorizeStep = AuthorizeStepID + 1;
                        }
                        rcp = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetStepReceipient(oModel.AuthModelID, oReturn.AuthorizeStep);
                        if (rcp != null)
                        {
                            oReturn.Receipients.Add(rcp);
                        }
                        if (oReturn.Receipients.Count > 0)
                        {
                            if (ActionBy == null && oReturn.Receipients.IsCanAction(Document, Document.Requestor))
                            {
                                isEnd = false;
                            }
                            //else if (oReturn.Receipients.IsCanAction(Document, ActionBy))
                            //{
                            //    isEnd = false;
                            //}
                        }
                        AuthorizeStepID = oReturn.AuthorizeStep;
                    }
                }

                Result.Add(oReturn);
            }


            RequestActionResult oReturn2 = new RequestActionResult();
            if (Result.Exists(obj => !obj.IsCompleted))
            {
                oReturn2 = Result.Find(obj => !obj.IsCompleted);
            }
            else
            {
                oReturn2 = Result.Find(obj => obj.IsCompleted);
                oReturn2.AuthorizeStep = Document.AuthorizeStep + 1;
            }

            return oReturn2;
        }

        #endregion IRequestAction Members
    }
}