using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOCUSTODIAN : IRequestAction
    {
        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            if (Params.ContainsKey("PETTYCODE"))
            {
                foreach (string PettyCode in Params["PETTYCODE"].Split(','))
                {
                    Receipient rcp = new Receipient(ReceipientType.CUSTODIAN, PettyCode, Document.RequestorCompanyCode, Document.RequestID);
                    oReturn.Receipients.Add(rcp);
                }
            }

            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}