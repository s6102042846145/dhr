using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.WORKFLOW.AUTHORIZATION;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOAUTHORIZEWITHCUSTOMBAND : IRequestAction
    {
        public SENDTOAUTHORIZEWITHCUSTOMBAND()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            int AuthorizeModelID = -1;
            string startBand = "";
            if (Params.ContainsKey("AUTHORIZEMODEL"))
            {
                int.TryParse(Params["AUTHORIZEMODEL"], out AuthorizeModelID);
            }
            if (Params.ContainsKey("STARTBAND"))
            {
                startBand = Params["STARTBAND"];
            }
            AuthorizationModel oModel = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetModel(AuthorizeModelID);
            RequestActionResult oReturn = new RequestActionResult();
            oReturn.GoToNextItem = true;
            oReturn.AuthorizeStep = 0;
            Receipient rcp;
            EmployeeData emp = new EmployeeData();
            ReceipientList list;
            bool lFound = false;
            do
            {
                rcp = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetStepReceipient(oModel.AuthModelID, oReturn.AuthorizeStep);
                if (rcp == null)
                {
                    break;
                }
                list = Document.TranslateReceipient(rcp);
                foreach (Receipient rcpItem in list)
                {
                    switch (rcpItem.Type)
                    {
                        case ReceipientType.EMPLOYEE:
                            emp = new EmployeeData(rcpItem.Code, Document.DocumentDate);
                            break;

                        case ReceipientType.POSITION:
                            emp = emp.GetEmployeeByPositionID(rcpItem.Code, Document.DocumentDate);
                            break;

                        default:
                            continue;
                    }
                    if (emp != null)
                    {
                        if (emp.EmpGroup != "" && startBand.CompareTo(emp.EmpGroup) <= 0)
                        {
                            lFound = true;
                            oReturn.Receipients.Add(rcp);
                            break;
                        }
                    }
                }
                if (lFound)
                {
                    break;
                }
                oReturn.AuthorizeStep++;
            } while (!lFound);

            if (oReturn.Receipients.Count > 0 && oReturn.Receipients.IsCanAction(Document, Document.Requestor))
            {
                oReturn.Receipients.Clear();
            }
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}