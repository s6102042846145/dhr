using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOSPECIAL_DELEGATE : IRequestAction
    {
        public SENDTOSPECIAL_DELEGATE()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            string specialcode = "";
            string authorizeDelegate = "";
            if (Params.ContainsKey("SPECIALCODE"))
            {
                specialcode = Params["SPECIALCODE"];
            }
            if (Params.ContainsKey("AUTHORIZEDELEGATE"))
            {
                authorizeDelegate = Params["AUTHORIZEDELEGATE"];
            }
            RequestActionResult oReturn = new RequestActionResult();
            Receipient rcp = new Receipient(ReceipientType.SPECIAL_DELEGATE, specialcode, Document.RequestorCompanyCode, Document.RequestID);
            oReturn.Receipients.Add(rcp);
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            oReturn.AuthorizeDelegate = authorizeDelegate;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}