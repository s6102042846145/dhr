﻿using PTTCHEM.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class CHECKAUTHORIZEEXTERNALMULTIPLE : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();

            oReturn.GoToNextItem = true;
            oReturn.IsCompleted = true;
            int CurrentSequence = 1;
            if (Document.RequestFlowMultipleReceipient_ExternalList.Exists(obj => obj.ApproverID == ActionBy.EmployeeID))
            {
                List<RequestFlowMultipleReceipient_External> Current = Document.RequestFlowMultipleReceipient_ExternalList.FindAll(obj => obj.ApproverID == ActionBy.EmployeeID);
                foreach (RequestFlowMultipleReceipient_External cur in Current)
                {
                    if (cur.Operation == "AND")
                    {
                        cur.ActionBy = ActionBy.EmployeeID;
                        cur.ActionDate = DateTime.Now;
                        cur.ActionCode = "APPROVE";
                        CurrentSequence = cur.Sequence;
                    }
                    else
                    {
                        foreach (RequestFlowMultipleReceipient_External repInGroup in Document.RequestFlowMultipleReceipient_ExternalList.FindAll(obj => obj.ApproverGroup == cur.ApproverGroup || obj.ApproverID == cur.ApproverID))
                        {
                            repInGroup.ActionBy = ActionBy.EmployeeID;
                            repInGroup.ActionDate = DateTime.Now;
                            repInGroup.ActionCode = "APPROVE";
                        }
                    }
                }
            }

            if (Document.RequestFlowMultipleReceipient_ExternalList.Exists(obj => obj.ActionDate <= DateTime.MinValue))
            {
                foreach (RequestFlowMultipleReceipient_External existsing in Document.RequestFlowMultipleReceipient_ExternalList.FindAll(obj => obj.ActionDate <= DateTime.MinValue))
                {
                    if (existsing.Operation == "AND")
                    {
                        if (existsing.Sequence == (CurrentSequence + 1))
                        {
                            Receipient rcp = new Receipient(ReceipientType.EXTERNAL_USER, existsing.ApproverID, Document.RequestorCompanyCode, Document.RequestID);
                            oReturn.Receipients.Add(rcp);
                        }
                    }
                    else
                    {
                        Receipient rcp = new Receipient(ReceipientType.EXTERNAL_USER, existsing.ApproverID, Document.RequestorCompanyCode, Document.RequestID);
                        oReturn.Receipients.Add(rcp);
                    }
                }
                oReturn.GoToNextItem = false;
                oReturn.IsCancelled = false;
                oReturn.IsCompleted = false;
                oReturn.IsWaitForEdit = false;
            }
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.RequestFlowMultipleReceipient_ExternalList = Document.RequestFlowMultipleReceipient_ExternalList;

            return oReturn;
        }
    }
}
