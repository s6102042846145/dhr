﻿using ESS.WORKFLOW.AUTHORIZATION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOLASTEDAPPROVER : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            int AuthorizeModelID = -1;
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            if (Params.ContainsKey("AUTHORIZEMODEL"))
            {
                foreach (string strAuth in Params["AUTHORIZEMODEL"].ToString().Split(','))
                {
                    AuthorizeModelID = -1;
                    int.TryParse(strAuth, out AuthorizeModelID);

                    AuthorizationModel oModel = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetModel(AuthorizeModelID);
                    Receipient rcp = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetStepReceipient(oModel.AuthModelID, Document.AuthorizeStep - 1);
                    oReturn.Receipients.Add(rcp);
                    if (oReturn.Receipients.Count > 0)
                        break;
                }
            }
            
            oReturn.GoToNextItem = true;
            return oReturn;
        }
    }
}
