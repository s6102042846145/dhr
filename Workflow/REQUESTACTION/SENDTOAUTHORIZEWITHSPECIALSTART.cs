using System;
using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOAUTHORIZEWITHSPECIALSTART : IRequestAction
    {
        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            string employeeID = "";
            if (Params.ContainsKey("EMPLOYEEID"))
            {
                employeeID = Params["EMPLOYEEID"];
            }
            RequestActionResult oReturn = new RequestActionResult();
            EmployeeData emp = new EmployeeData();
            oReturn.GoToNextItem = true;
            if (!emp.ValidateEmployeeID(employeeID))
            {
                throw new Exception(string.Format("EmployeeID '{0}' not found", employeeID));
            }
            Receipient rcp = new Receipient(ReceipientType.EMPLOYEE, employeeID, Document.RequestorCompanyCode, Document.RequestID);
            oReturn.Receipients.Add(rcp);
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            oReturn.AuthorizeStep = 0;
            oReturn.AuthorizeDelegate = employeeID;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}