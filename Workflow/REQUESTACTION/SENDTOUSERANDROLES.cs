﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOUSERANDROLES : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            Receipient rcp;

            if (IsSimulation)
            {
                if (Params.ContainsKey("USERROLE"))
                {
                    foreach (string adminrole in Params["USERROLE"].Split(','))
                    {
                        rcp = new Receipient(ReceipientType.USERROLE, adminrole, Document.RequestorCompanyCode, Document.RequestID);
                        oReturn.Receipients.Add(rcp);
                    }
                }
            }
            else
            {

                rcp = new Receipient(ReceipientType.EMPLOYEE, ActionBy.EmployeeID, ActionBy.CompanyCode, Document.RequestID);
                oReturn.Receipients.Add(rcp);
            }

            if (Params.ContainsKey("USERROLE_CUSTOM"))
            {
                foreach (string adminrole in Params["USERROLE_CUSTOM"].Split(','))
                {
                    rcp = new Receipient(ReceipientType.USERROLE, adminrole, Document.RequestorCompanyCode, Document.RequestID);
                    oReturn.Receipients.Add(rcp);
                }
            }

            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }
    }
}
