﻿using ESS.EMPLOYEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOLASTEDACTION : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            Receipient rcp;

            if (IsSimulation)
            {
                if (Params.ContainsKey("USERROLE"))
                {
                    foreach (string adminrole in Params["USERROLE"].Split(','))
                    {
                        rcp = new Receipient(ReceipientType.USERROLE, adminrole, Document.RequestorCompanyCode, Document.RequestID);
                        oReturn.Receipients.Add(rcp);
                    }
                }
            }
            else
            {
                if (Params.ContainsKey("ACTIONCODE"))
                {
                    EmployeeData emp = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetLastedActionByActionCode(Document.RequestID, Params["ACTIONCODE"]);
                    rcp = new Receipient(ReceipientType.EMPLOYEE, emp.EmployeeID, emp.CompanyCode, Document.RequestID);
                    oReturn.Receipients.Add(rcp);
                }
            }

            oReturn.GoToNextItem = true;
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }
    }
}
