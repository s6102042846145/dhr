using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class APPROVEMULTIPLE : IRequestAction
    {
        public APPROVEMULTIPLE()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            foreach (Receipient rcp in Document.Receipients)
            {
                ReceipientList list = Document.TranslateReceipient(rcp);
                if (list.Count > 0 && !list.IsCanAction(Document, ActionBy))
                {
                    oReturn.Receipients.Add(rcp);
                }
            }
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = oReturn.Receipients.Count == 0;
            oReturn.GroupName = "MULTIPLE";

            return oReturn;
        }

        #endregion IRequestAction Members
    }
}