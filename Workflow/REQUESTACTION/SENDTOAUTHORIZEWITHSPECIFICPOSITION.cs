﻿using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.WORKFLOW.AUTHORIZATION;

namespace ESS.WORKFLOW.REQUESTACTION
{
    /// <summary>
    /// ActionType: สำหรับการใช้ manager ข้ามสายการอนุมัติ โดยการส่งรหัส Position ของสายงานที่ต้องการไปแทน
    /// เช่น พนักงาน A ต้องการให้นายของพนักงาน B อนุมัติ จะต้องส่ง Position ของพนักงาน B ไปด้วย
    /// </summary>
    public class SENDTOAUTHORIZEWITHSPECIFICPOSITION : IRequestAction
    {
        public SENDTOAUTHORIZEWITHSPECIFICPOSITION() 
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            int AuthorizeModelID = -1;
            if (Params.ContainsKey("AUTHORIZEMODEL") && Params.ContainsKey("POSITIONID"))
            {
                foreach (string strAuth in Params["AUTHORIZEMODEL"].ToString().Split(','))
                {
                    AuthorizeModelID = -1;
                    int.TryParse(strAuth, out AuthorizeModelID);

                    AuthorizationModel oModel = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetModel(AuthorizeModelID);
                    Receipient rcp = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetStepReceipient(oModel.AuthModelID, 0);
                    rcp.Code = rcp.Code + ":" + Params["POSITIONID"];
                    oReturn.Receipients.Add(rcp);
                    break;
                }
            }
            oReturn.GoToNextItem = true;
            oReturn.AuthorizeStep = 0;
            return oReturn;
        }

        #endregion IRequestAction Memberss
    }
}
