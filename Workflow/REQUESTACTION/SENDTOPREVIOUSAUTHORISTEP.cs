using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.WORKFLOW.AUTHORIZATION;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOPREVIOUSAUTHORISTEP : IRequestAction
    {
        public SENDTOPREVIOUSAUTHORISTEP()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            oReturn.AuthorizeStep = Document.AuthorizeStep - 1;
            int manger_r_number = oReturn.AuthorizeStep + 1;
            Receipient rcp = new Receipient(ReceipientType.SPECIAL, "#MANAGER_R_" + manger_r_number.ToString("00"), Document.CompanyCode, Document.RequestID);
            oReturn.Receipients.Add(rcp);
            oReturn.GoToNextItem = true;
           
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}