using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Xml;
using ESS.DATA.FILE;
using ESS.EMPLOYEE;
using ESS.MAIL.DATACLASS;
using ESS.SECURITY;
using ESS.WORKFLOW.AUTHORIZATION;
using ESS.WORKFLOW.INTERFACE;
using PTTCHEM.WORKFLOW;
using ESS.DELEGATE.DATACLASS;
using ESS.DATA.EXCEPTION;

namespace ESS.WORKFLOW
{
    public abstract class AbstractWorkflowService : IWorkflowService
    {
        #region " GetAction "

        protected IRequestAction GetAction(string ActionCode)
        {
            IRequestAction oReturn;
            Assembly oAssembly = Assembly.Load("ESS.WORKFLOW");
            Type oType = oAssembly.GetType("ESS.WORKFLOW.ACTION." + ActionCode.ToUpper());
            oReturn = (IRequestAction)Activator.CreateInstance(oType);
            return oReturn;
        }

        #endregion " GetAction "

        #region " GetCurrentDate "

        protected abstract DateTime GetCurrentDate();

        #endregion " GetCurrentDate "

        #region " ProcessAction "

        private RequestActionResult ProcessAction(RequestDocument Document, FlowItemAction Action, EmployeeData ActionBy, bool IsSimulation)
        {
            WorkflowManagement oWorkflowManagement = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode);
            RequestTypeSetting RTS;
            RequestActionResult oResult;

            if (Action.IsCheckFlowAfterOverwrite)
                RTS = oWorkflowManagement.GetRequestTypeSetting(Document);
            else
                RTS = oWorkflowManagement.GetRequestTypeSettingWithoutCheckFlow(Document);

            if (RTS.RequestTypeID == -1)
            {
                throw new Exception("Not found flowsetting");
            }
            if (Action.FlowID != RTS.FlowID)
            {
                oResult = new RequestActionResult();
                oResult.FlowID = RTS.FlowID;
                oResult.IsRestartFlow = true;
                oResult.AuthorizeStep = 0;
                return oResult;
            }
            Dictionary<string, string> paramList = new Dictionary<string, string>();
            foreach (ActionTypeParam ATP in Action.ActionType.Parameters)
            {
                FlowParameter FP = Action.GetMappingParameter(ATP.ID);
                if (FP.ID == -1)
                {
                    throw new Exception(string.Format("Not found mapping parameter {0}_{1}_{2}_{3}", Action.FlowID, Action.FlowItemID, Action.ID, ATP.ID));
                }
                if(!RTS.Parameters.Exists(obj => obj.ID == FP.ID))
                    throw new Exception(string.Format("Not found mapping parameter no. {0} in Flow {1}_{2}_{3}_{4}", FP.ID ,Action.FlowID, Action.FlowItemID, Action.ID, ATP.ID));
                RequestTypeParam param = RTS.Parameters.Find(obj => obj.ID == FP.ID);
                switch (param.Mode)
                {
                    case ParameterMode.DATA:
                        paramList.Add(ATP.Code, Document.Data.Rows[0][param.Value].ToString());
                        break;

                    case ParameterMode.SETTING:
                        paramList.Add(ATP.Code, param.Value);
                        break;

                    case ParameterMode.REQUEST:
                        PropertyInfo oProp = Document.GetType().GetProperty(param.Value, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        if (oProp != null)
                        {
                            paramList.Add(ATP.Code, oProp.GetValue(Document, null).ToString());
                        }
                        break;
                }
            }
            oResult = Action.ActionType.DoAction(Document, ActionBy, paramList, IsSimulation);
            if (oResult.GoToNextItem && Action.ActionType.IsRaiseAutomaticAction)
            {
                Document.AuthorizeStep = oResult.AuthorizeStep;
                paramList = new Dictionary<string, string>();
                if (Action.AutomaticActionType == null)
                {
                    throw new Exception(string.Format("Automatic action for {0}_{1}_{2}", Action.FlowID, Action.FlowItemID, Action.ID));
                }
                foreach (ActionTypeParam ATP in Action.AutomaticActionType.Parameters)
                {
                    FlowParameter FP = Action.GetMappingAutomaticParameter(ATP.ID);
                    if (FP.ID == -1)
                    {
                        throw new Exception("Not found mapping automatic parameter");
                    }
                    if (RTS.Parameters.Count >= FP.ID)
                    {
                        if (!RTS.Parameters.Exists(obj => obj.ID == FP.ID))
                            throw new Exception(string.Format("Not found mapping parameter no. {0} in Flow {1}_{2}_{3}_{4}", FP.ID, Action.FlowID, Action.FlowItemID, Action.ID, ATP.ID));
                        RequestTypeParam param = RTS.Parameters.Find(obj => obj.ID == FP.ID);
                        switch (param.Mode)
                        {
                            case ParameterMode.DATA:
                                paramList.Add(ATP.Code, Document.Data.Rows[0][param.Value].ToString());
                                break;

                            case ParameterMode.SETTING:
                                paramList.Add(ATP.Code, param.Value);
                                break;

                            case ParameterMode.REQUEST:
                                PropertyInfo oProp = Document.GetType().GetProperty(param.Value, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                                if (oProp != null)
                                {
                                    paramList.Add(ATP.Code, oProp.GetValue(Document, null).ToString());
                                }
                                break;
                        }
                    }

                }

                List<RequestFlowMultipleReceipient> tmp = oResult.RequestFlowMultipleReceipientList;
                oResult = Action.AutomaticActionType.DoAction(Document, ActionBy, paramList, IsSimulation);
                oResult.RequestFlowMultipleReceipientList = tmp;
                oResult.IsAutomatic = true;
            }

            return oResult;
        }

        public virtual bool CanApproveCoWorkflow(string RequestNo, string ActionBy, string ActionByPosition)
        {
            throw new NotImplementedException();
        }

        #endregion " ProcessAction "

        #region " UpdateDocument "

        public abstract void UpdateDocument(RequestDocument Document, FlowItemAction action);

        #endregion " UpdateDocument "

        #region " XML "

        protected string ParseToXML(DataSet DS)
        {
            StringWriter tw = new StringWriter();
            DS.WriteXml(tw, XmlWriteMode.WriteSchema);
            string cData = tw.ToString();
            tw.Close();
            tw.Dispose();
            return cData;
        }

        protected DataSet ParseFromXML(string xmlString)
        {
            DataSet oReturn = new DataSet();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);
            StringReader tr = new System.IO.StringReader(xml.InnerXml);
            oReturn.ReadXml(tr);
            return oReturn;
        }

        #endregion " XML "

        #region " RequestText "

        public abstract string GetRequestText(string Category, string Language, string Code);

        #endregion " RequestText "

        #region IWorkflowService Members

        #region " User "

        //public abstract UserSetting GetUserSetting(string EmployeeID);

        //public abstract List<string> GetUserRole(string EmployeeID);

        //public abstract UserRoleSetting GetUserRoleSetting(string UserRole);

        //public abstract List<EmployeeData> GetUserResponse(string Role, string AdminGroup);

        #endregion " User "

        #endregion IWorkflowService Members

        #region " Flow "

        #region " GetAllFlows "

        public abstract List<Flow> GetAllFlows();

        #endregion " GetAllFlows "

        #region " GetFlow "

        public abstract Flow GetFlow(int FlowID);

        #endregion " GetFlow "

        #region " GetFlowItems "

        public abstract List<FlowItem> GetFlowItems(int FlowID);

        #endregion " GetFlowItems "

        #region " GetFlowItem "

        public abstract FlowItem GetFlowItem(int FlowID, int FlowItemID);

        #endregion " GetFlowItem "

        #region " GetAllStates "

        public abstract List<State> GetAllStates();

        #endregion " GetAllStates "

        #region " GetState "

        public abstract State GetState(int StateID);

        #endregion " GetState "

        #region " GetStateResponse "

        public abstract ReceipientList GetStateResponse(int StateID);

        #endregion " GetStateResponse "

        #region " GetAllPossibleAction "

        public abstract List<FlowItemAction> GetAllPossibleAction(int FlowID, int FlowItemID);

        #endregion " GetAllPossibleAction "

        #region " GetActionType "

        public abstract ActionType GetActionType(int ActionTypeID);

        #endregion " GetActionType "

        #region " GetAllActionTypes "

        public abstract List<ActionType> GetAllActionTypes();

        #endregion " GetAllActionTypes "

        #region " GetActionTypeParameters "

        public abstract List<ActionTypeParam> GetActionTypeParameters(int ActionTypeID);

        #endregion " GetActionTypeParameters "

        #region " GetActionTypeReceipients "

        public abstract ReceipientList GetActionTypeReceipients(int ActionTypeID);

        #endregion " GetActionTypeReceipients "

        #region " GetFlowParameter "

        public abstract List<FlowParameter> GetFlowParameter(int FlowID);

        #endregion " GetFlowParameter "

        #region " GetMappingParameter "

        public abstract FlowParameter GetMappingParameter(int FlowID, int FlowItemID, int FlowItemActionID, int ParamID,string NextItemCode);

        #endregion " GetMappingParameter "

        #region " GetMappingAutomaticParameter "

        public abstract FlowParameter GetMappingAutomaticParameter(int FlowID, int FlowItemID, int FlowItemActionID, int ParamID, string NextItemCode);

        #endregion " GetMappingAutomaticParameter "

        #endregion " Flow "

        #region " RequestType "

        #region " GetRequestTypeResponse "

        public virtual List<UserRoleResponseSetting> GetRequestTypeResponse(int RequestTypeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetRequestTypeResponse "

        #region " GetRequestType "

        #region " GetRequestType(int) "

        public abstract RequestType GetRequestType(int RequestTypeID);

        #endregion " GetRequestType(int) "

        #region " GetRequestType(int,DateTime) "

        public abstract RequestType GetRequestType(int RequestTypeID, DateTime CheckDate, string LanguageCode);

        #endregion " GetRequestType(int,DateTime) "

        #endregion " GetRequestType "

        #region " GetAllRequestTypes "

        public abstract List<RequestType> GetAllRequestTypes();

        #endregion " GetAllRequestTypes "

        #region " GetRequestTypeSchemas "

        public abstract List<RequestTypeSchema> GetRequestTypeSchemas(int RequestTypeID, int VersionID);

        #endregion " GetRequestTypeSchemas "

        #region " GetRequestTypeKeys "

        public abstract List<RequestTypeKey> GetRequestTypeKeys(int RequestTypeID, int VersionID);

        #endregion " GetRequestTypeKeys "

        #region " GetRequestTypeSettings "

        public abstract List<RequestTypeSetting> GetRequestTypeSettings(int RequestTypeID, int VersionID);

        #endregion " GetRequestTypeSettings "

        #region " GetRequestTypeSetting "

        public abstract RequestTypeSetting GetRequestTypeSetting(int RequestTypeID, int VersionID, string FlowKey);

        public abstract RequestTypeSetting GetRequestTypeSettingByFlowID(int RequestTypeID, int VersionID, int FlowID);

        #endregion " GetRequestTypeSetting "

        #region " GetRequestTypeParameters "

        public abstract List<RequestTypeParam> GetRequestTypeParameters(int RequestTypeID, int VersionID, int KeyID);

        #endregion " GetRequestTypeParameters "

        #region " CalculateFlow "

        public abstract int CalculateFlow(int RequestTypeID, int VersionID, string FlowKey);

        #endregion " CalculateFlow "

        #region " GetRequestTypeOwner "

        public virtual ReceipientList GetRequestTypeOwner(int RequestTypeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetRequestTypeOwner "

        #endregion " RequestType "

        #region " Request "

        #region " GetSpecialReceipient "

        public abstract ReceipientList GetSpecialReceipient(string EmployeeID, string Position, string Code, DateTime CheckDate);

        #endregion " GetSpecialReceipient "

        #region " LoadAdditionalData "
        public abstract Object LoadAdditionalData(string RequestNo, int RequestID, int RequestTypeID, int RequestTypeVersionID, int DocversionID);

        #endregion " LoadAdditionalData "

        #region " LoadInfoData "

        public abstract DataTable LoadInfoData(string RequestNo, int RequestID, int RequestTypeID, int RequestTypeVersionID, int DocversionID);

        #endregion " LoadInfoData "

        #region " CreateRequest "
        public RequestDocument CreateRequest(EmployeeData Requestor, int RequestTypeID, Dictionary<string, string> Params)
        {
            RequestType ReqType = this.GetRequestType(RequestTypeID);
            //if (Requestor.EmployeeID != WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID)
            //{
            //    //Check userrole response for this request type id;
            //    List<UserRoleResponseSetting> settings = ReqType.LoadUserResponse();
            //    if (!WorkflowPrinciple.Current.UserSetting.Employee.IsUserInResponse(settings, Requestor.EmployeeID, Requestor.CheckDate))
            //    {
            //        throw new NoAuthorizeException("CreateRequest");
            //    }
            //}
            System.Globalization.CultureInfo oCL = new System.Globalization.CultureInfo("en-US");
            RequestDocument oDoc = RequestDocument.CreateInstance(Requestor.CompanyCode);
            // 0 : companycode 4 digit
            // 1 : year 2 digit
            // 2 : running 7 digit
            // 3 : requesttype 2 digit
            // 4 : requesttype running 2 digit
            Random oRandom = new Random();
            string cRandomStr = oRandom.Next(999).ToString("000");
            oDoc.RequestID = 0;
            //oDoc.RequestNo = string.Format("{0}{1}{2}{3}", ReqType.Prefix.PadRight(2, 'Z').Substring(0, 2), Requestor.EmployeeID.PadLeft(8, '0'), DateTime.Now.ToString("yyyyMMddHHmmssfff", oCL), cRandomStr);
            oDoc.RequestTypeID = RequestTypeID;
            oDoc.RequestorNo = Requestor.EmployeeID;
            oDoc.RequestorPosition = Requestor.PositionID;
            oDoc.RequestorCompanyCode = Requestor.CompanyCode;
            oDoc.Requestor = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode,DateTime.Now);
            oDoc.CreatorNo = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oDoc.CreatorPosition = WorkflowPrinciple.Current.UserSetting.Employee.CurrentPosition.ObjectID;
            oDoc.CreatorCompanyCode = WorkflowPrinciple.Current.UserSetting.Employee.CompanyCode;
            oDoc.Creator = new EmployeeData(oDoc.CreatorNo, oDoc.CreatorPosition, oDoc.CreatorCompanyCode, DateTime.Now);
            oDoc.CreatedDate = GetCurrentDate();
            oDoc.KeyMaster = oRandom.Next(9999).ToString("0000");
            oDoc.IsWaitForEdit = true;
            
            if (Params.ContainsKey("ReferRequestNo"))
            {
                oDoc.ReferRequestNo = Params["ReferRequestNo"];
            }
            oDoc.RequestNo = "DUMMY"; // GetRequestNo(oDoc.CompanyCode, oDoc.CreatedDate, ReqType.Prefix, oDoc.ReferRequestNo);

            return oDoc;
        }

        public virtual string GetRequestNo(DateTime CreatedDate, string RequestTypePrefix, string ReferRequestNo)
        {
            throw new NotImplementedException();
        }
        #endregion " CreateRequest "

        #region " GetLastProcess "

        public abstract RequestFlow GetLastProcess(string RequestNo);

        #endregion " GetLastProcess "

        #region " GetPassedProcess "

        public abstract List<RequestFlow> GetPassedProcess(string RequestNo);

        #endregion " GetPassedProcess "

        #region " SimulateProcess "

        public List<RequestFlow> SimulateProcess(RequestDocument Document)
        {
            List<RequestFlow> oReturn = new List<RequestFlow>();
            //1. Get passed process :: append to ResultList
            oReturn.AddRange(GetPassedProcess(Document.RequestNo));
            //2. GetCurrentFlowItem
            FlowItem CurrentItem;
            CurrentItem = Document.CurrentFlowItem;
            RequestFlow RF = null;
            EmployeeData ActionBy = WorkflowPrinciple.Current.UserSetting.Employee;

            #region " Current item "
            if (!CurrentItem.State.IsEndPoint)
            {
                if (CurrentItem.State.Code == "CR")
                {
                    oReturn[oReturn.Count - 1].IsCurrentItem = true;
                    oReturn[oReturn.Count - 1].OptionalStatus = "NULL";
                }
                else
                {
                    if(CurrentItem.State.Code == "ED" || CurrentItem.State.Code == "CN")
                    {
                        Receipient Creator = new Receipient(ReceipientType.SPECIAL, "#CREATOR", Document.RequestorCompanyCode, Document.RequestID);
                        Document.Receipients.Add(Creator);
                        Receipient Requestor = new Receipient(ReceipientType.SPECIAL, "#REQUESTOR", Document.CreatorCompanyCode, Document.RequestID);
                        Document.Receipients.Add(Requestor);
                    }
                    ReceipientList list1 = new ReceipientList();
                    foreach (Receipient rcp in Document.TranslateList(Document.Receipients))
                    {
                        if (rcp.Type == ReceipientType.SPECIAL)
                        {
                            rcp.CheckDate = Document.SubmittedDate;
                            list1.AddRange(Document.GetSpecialReceipient(rcp.Code));
                        }
                        else if (rcp.Type == ReceipientType.POSITION)
                        {
                            rcp.CheckDate = Document.SubmittedDate;
                            list1.Add(rcp);
                        }
                        else
                        {
                            list1.Add(rcp);
                        }
                    }

                    RF = new RequestFlow();
                    RF.ActionBy = list1.ToString();
                    RF.ActionByPosition = list1.ToPositionString();
                    if (list1.Count > 0)
                    {
                        RF = new RequestFlow();
                        RF.GroupName = CurrentItem.GroupName;
                        RF.ActionBy = list1.ToString();
                        RF.ActionByPosition = list1.ToPositionString();

                        ReceipientList oReceipientList = list1;
                        foreach (Receipient rep in oReceipientList)
                        {
                            if (ActionBy == null)
                            {
                                ActionBy = new EmployeeData();
                                ActionBy.CompanyCode = rep.CompanyCode;
                            }

                            if (rep.Type == ReceipientType.POSITION)
                            {
                                ActionBy.CompanyCode = rep.CompanyCode;
                                ActionBy = ActionBy.GetEmployeeByPositionID(rep.Code, Document.SubmittedDate, WorkflowPrinciple.Current.UserSetting.Language);
                            }
                            else
                            {
                                if (rep.Type == ReceipientType.USERROLE)
                                {
                                    List<EmployeeData> userRoleResponse = UserRoleManager.CreateInstance(Document.CompanyCode).GetUserResponse(rep.Code, Document.Requestor);
                                    if (userRoleResponse.Count > 0)
                                        ActionBy = userRoleResponse[0];
                                    else
                                        ActionBy = new EmployeeData() { CompanyCode = Document.CompanyCode };
                                }
                                else
                                {
                                    ActionBy = new EmployeeData(rep.Code, "", rep.CompanyCode, DateTime.Now);
                                }
                            }

                            RF.ActionCode = CurrentItem.Code;
                            RF.IsCurrentItem = false;
                        }
                    }

                    RF.ActionCode = CurrentItem.Code;
                    RF.OptionalStatus = string.Empty;
                    RF.IsCurrentItem = true;
                    RF.GroupName = CurrentItem.GroupName;
                    oReturn.Add(RF);
                }
            }
            else
            {
                RF = new RequestFlow();
                RF.ActionCode = CurrentItem.Code;
                RF.IsCurrentItem = true;
                RF.OptionalStatus = string.Empty;

                oReturn.Add(RF);
            }

            #endregion " Current item "

            int count = 0;
            FlowItemAction oDefaultAction;
            // SAVE LAST DATA
            int nAuthStep = Document.AuthorizeStep;
            string cAuthorizeDelegate = Document.AuthorizeDelegate;
            string cGroupName = Document.GroupName;
            ReceipientList saveList = new ReceipientList();
            saveList.AddRange(Document.Receipients.ToArray());
            // *********************
            ActionBy = ActionBy == null ? Document.Requestor : ActionBy;
            while (!CurrentItem.State.IsEndPoint && count < 10 && CurrentItem.PossibleActions(Document, ActionBy).Count > 0)
            {
                //3. GetDefaultAction ( index 0 in this version )
                oDefaultAction = CurrentItem.PossibleActions(Document, ActionBy)[0];
                count++;
                //5. Receive ReqActionResult
                RequestActionResult oResult = this.ProcessAction(Document, oDefaultAction, ActionBy, true);

                if (oResult.RequestFlowMultipleReceipientList != null)
                Document.RequestFlowMultipleReceipientList = oResult.RequestFlowMultipleReceipientList;

                Document.AuthorizeStep = oResult.AuthorizeStep;
                Document.AuthorizeDelegate = oResult.AuthorizeDelegate;
                Document.GroupName = oResult.GroupName;
                Document.Receipients.Clear();
                Document.Receipients.AddRange(oResult.Receipients.ToArray());
                ReceipientList list = Document.TranslateList(oResult.Receipients);


                if (oDefaultAction.StampRequestorTobeApprover)
                {
                    RF = new RequestFlow();
                    RF.GroupName = string.Empty;
                    RF.ActionByCompanyCode = Document.Requestor.CompanyCode;
                    RF.ActionBy = Document.Requestor.EmployeeID+" "+Document.Requestor.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                    RF.ActionByPosition = Document.Requestor.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                    RF.ActionCode = "APPROVE";
                    oReturn.Add(RF);
                }


                if (oDefaultAction.AutomaticStampRequestorTobeApprover && oResult.GoToNextItem)
                {
                    RF = new RequestFlow();
                    RF.GroupName = string.Empty;
                    RF.ActionByCompanyCode = Document.Requestor.CompanyCode;
                    RF.ActionBy = Document.Requestor.EmployeeID + " " + Document.Requestor.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                    RF.ActionByPosition = Document.Requestor.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                    RF.ActionCode = "APPROVE";
                    oReturn.Add(RF);
                }

                //7. if not ReqActionResult.NextItem.isEndPoint :: repeat step 3 - 7
                if (oResult.IsRestartFlow)
                {
                    //Document.CalculateFlow();
                    Document.FlowID = oResult.FlowID;
                    Document.AuthorizeStep = -1;
                    CurrentItem = FlowItem.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetFlowItem(Document.FlowID, Document.Flow.CreateItemID);
                    ActionBy = Document.Requestor;
                }
                else if (oResult.GoToNextItem)
                {
                    CurrentItem = oDefaultAction.NextItem;
                }

                if (list.Count > 0 || CurrentItem.State.IsEndPoint)
                {

                    if (list.Count > 0)
                    {
                        RF = new RequestFlow();
                        RF.GroupName = CurrentItem.GroupName;
                        RF.ActionBy = list.ToString();
                        RF.ActionByPosition = list.ToPositionString();

                        ReceipientList oReceipientList = list;
                        foreach (Receipient rep in oReceipientList)
                        {
                            if (ActionBy == null)
                            {
                                ActionBy = new EmployeeData();
                                ActionBy.CompanyCode = rep.CompanyCode;
                            }

                            if (rep.Type == ReceipientType.POSITION)
                            {
                                ActionBy.CompanyCode = rep.CompanyCode;
                                ActionBy = ActionBy.GetEmployeeByPositionID(rep.Code, Document.SubmittedDate, WorkflowPrinciple.Current.UserSetting.Language);
                                if (ActionBy != null)
                                {
                                    RF.ActionByCode = ActionBy.EmployeeID;
                                }
                                else
                                {
                                    ActionBy = new EmployeeData(RF.ActionByCode);
                                }

                                if (string.IsNullOrEmpty(ActionBy.EmployeeID))
                                {
                                    string[] args = new string[1] { rep.Code };
                                    throw new DataServiceException("FLOWEXCEPTION", "NO_POSITION", args);
                                }
                            }
                            else if (rep.Type == ReceipientType.EXTERNAL_USER)
                            {
                                ActionBy.CompanyCode = rep.CompanyCode;
                                DataTable dt = EmployeeManagement.CreateInstance(Document.CompanyCode).GetExternalUserbyID(rep.Code, WorkflowPrinciple.Current.UserSetting.Language);
                                ActionBy.EmployeeID = rep.Code;

                                if (dt.Rows.Count <= 0)
                                {
                                    string[] args = new string[1] { rep.Code };
                                    throw new DataServiceException("FLOWEXCEPTION", "NO_EXTERNALUSER", args);
                                }
                            }
                            else if (rep.Type == ReceipientType.CUSTODIAN)
                            {
                                ActionBy = EmployeeManagement.CreateInstance(Document.CompanyCode).GetPettyCustodianGetByCode(rep.Code);
                                ActionBy.CompanyCode = rep.CompanyCode;

                                if (string.IsNullOrEmpty(ActionBy.EmployeeID))
                                {
                                    string[] args = new string[1]{rep.Code};
                                    throw new DataServiceException("FLOWEXCEPTION", "NO_CUSTODIAN", args);
                                }
                            }
                            else
                            {
                                RF.ActionByCode = rep.Code;
                                if (rep.Type == ReceipientType.USERROLE)
                                {
                                    // RF.ActionBy = ShowUserRoleDescription(list);
                                    //  RF.ActionByPosition = string.Empty;
                                    List<EmployeeData> userRoleResponse = UserRoleManager.CreateInstance(Document.CompanyCode).GetUserResponse(rep.Code, Document.Requestor);
                                    if (userRoleResponse.Count > 0)
                                        ActionBy = userRoleResponse[0];
                                    else
                                    {
                                        string[] args = new string[1] { rep.Code };
                                        throw new DataServiceException("FLOWEXCEPTION", "NO_USERROLE", args);
                                    }
                                }
                                else
                                {
                                    ActionBy = new EmployeeData(rep.Code,"",rep.CompanyCode,DateTime.Now);
                                    if (string.IsNullOrEmpty(ActionBy.EmployeeID))
                                    {
                                        string[] args = new string[1] { rep.Code };
                                        throw new DataServiceException("FLOWEXCEPTION", "NO_RECEIPIENT", args);
                                    }
                                    // RF.ActionByPosition += ActionBy.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language) + ",";
                                }
                            }

                            RF.ActionCode = CurrentItem.Code;
                            RF.IsCurrentItem = false;
                            RF.Detail = oResult.Detail;


                        }
                        //RF.ActionByPosition = RF.ActionByPosition.TrimEnd(',');
                        oReturn.Add(RF);
                    }

                    if (CurrentItem.State.IsEndPoint)
                    {
                        RF = new RequestFlow();
                        RF.GroupName = CurrentItem.GroupName;
                        RF.ActionCode = CurrentItem.Code;
                        RF.IsCurrentItem = false;
                        RF.Detail = oResult.Detail;
                        oReturn.Add(RF);
                    }
                }

            }

            if (RF != null && CurrentItem != null && CurrentItem.State != null && CurrentItem.State.IsEndPoint)
            {
                RF.IsEndPoint = true;
            }

            // restore authorize step to currentItem after use in simulation
            Document.AuthorizeStep = nAuthStep;
            Document.AuthorizeDelegate = cAuthorizeDelegate;
            Document.GroupName = cGroupName;
            Document.Receipients.Clear();
            Document.Receipients.AddRange(saveList.ToArray());
            //8. Return ResultList
            return oReturn;
        }

        public List<RequestFlowForwardSnapshot> SimulateForwardProcess(RequestDocument Document)
        {
        //    List<RequestFlowForwardSnapshot> oReturn = new List<RequestFlowForwardSnapshot>();
        //    FlowItem CurrentItem;
        //    CurrentItem = Document.CurrentFlowItem;
        //    RequestFlowForwardSnapshot RF = null;
        //    EmployeeData ActionBy = WorkflowPrinciple.Current.UserSetting.Employee;

        //    #region " Current item "

        //    if (!CurrentItem.State.IsEndPoint)
        //    {
        //        //if (CurrentItem.State.Code == "CR")
        //        //{
        //        //    oReturn[oReturn.Count - 1].IsCurrentItem = true;
        //        //}
        //        //else
        //        //{
        //            if (CurrentItem.State.Code == "ED")
        //            {
        //                Receipient Creator = new Receipient(ReceipientType.SPECIAL, "#CREATOR", Document.RequestorCompanyCode);
        //                Document.Receipients.Add(Creator);
        //                Receipient Requestor = new Receipient(ReceipientType.SPECIAL, "#REQUESTOR", Document.CreatorCompanyCode);
        //                Document.Receipients.Add(Requestor);
        //            }
        //            ReceipientList list1 = new ReceipientList();
        //            foreach (Receipient rcp in Document.TranslateList(Document.Receipients))
        //            {
        //                if (rcp.Type == ReceipientType.SPECIAL)
        //                {
        //                    list1.AddRange(Document.GetSpecialReceipient(rcp.Code));
        //                }
        //                else
        //                {
        //                    list1.Add(rcp);
        //                }
        //            }

        //            RF = new RequestFlowForwardSnapshot();
        //            RF.ActionBy = list1.ToString();
        //            if (list1.Count > 0)
        //            {
        //                if (list1[0].Type == ReceipientType.POSITION)
        //                {
        //                    ActionBy = new EmployeeData();
        //                    ActionBy.CompanyCode = list1[0].CompanyCode;
        //                    ActionBy = ActionBy.GetEmployeeByPositionID(list1[0].Code, DateTime.Now, WorkflowPrinciple.Current.UserSetting.Language);
        //                    if (ActionBy != null)
        //                    {
        //                        RF.ActionByCode = ActionBy.EmployeeID;
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        RF.ActionByPosition = ActionBy.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
        //                        //AddBy: Ratchatawan W. (2012-02-21)
        //                        //RF.ActionBy = string.Format("{0}::{1}", RF.ActionByPosition, ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
        //                        RF.ActionBy = string.Format("{0} {1}", ActionBy.EmployeeID, ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
        //                    }
        //                    else
        //                    {
        //                        RF.ActionByCode = list1[0].Code;
        //                        ActionBy = new EmployeeData(RF.ActionByCode);
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        RF.ActionByPosition = string.Empty;
        //                    }
        //                }
        //                else
        //                {
        //                    RF.ActionByCode = list1[0].Code;
        //                    if (list1[0].Type == ReceipientType.USERROLE)
        //                    {
        //                        RF.ActionBy = ShowUserRoleDescription(list1);
        //                        RF.ActionByPosition = string.Empty;
        //                        List<EmployeeData> userRoleResponse = UserRoleManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetUserResponse(list1[0].Code, Document.Requestor);
        //                        if (userRoleResponse.Count > 0)
        //                        {
        //                            //string strPosition = string.Empty;
        //                            //string strEmployee = string.Empty;
        //                            //int index = 1;
        //                            foreach (EmployeeData emp in userRoleResponse)
        //                            {
        //                                ActionBy = emp;
        //                                //strPosition = ActionBy.ActionOfPosition.Text;
        //                                //strEmployee += ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
        //                                //if (index < userRoleResponse.Count)
        //                                //    strEmployee += ",";
        //                                //index++;
        //                            }
        //                            //RF.ActionBy = string.Format("{0}::{1}", strPosition, strEmployee);
        //                        }
        //                        else
        //                        {
        //                            ActionBy = new EmployeeData() { CompanyCode = Document.CompanyCode };
        //                        }
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        //RF.ActionByPosition = ActionBy.ActionOfPosition.Text;//list[0].DisplayName;
        //                    }
        //                    else
        //                    {
        //                        ActionBy = new EmployeeData(RF.ActionByCode, "", list1[0].CompanyCode, DateTime.Now);
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        RF.ActionByPosition = ActionBy.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
        //                    }
        //                }
        //            }
        //            RF.ActionCode = CurrentItem.Code;
        //            RF.IsCurrentItem = true;
        //            RF.GroupName = CurrentItem.GroupName;
        //            oReturn.Add(RF);
        //        //}
        //    }
        //    else
        //    {
        //        RF = new RequestFlow();
        //        RF.ActionCode = CurrentItem.Code;
        //        RF.IsCurrentItem = true;
        //        oReturn.Add(RF);
        //    }

        //    #endregion " Current item "

        //    int count = 0;
        //    FlowItemAction oDefaultAction;
        //    // SAVE LAST DATA
        //    int nAuthStep = Document.AuthorizeStep;
        //    string cAuthorizeDelegate = Document.AuthorizeDelegate;
        //    string cGroupName = Document.GroupName;
        //    ReceipientList saveList = new ReceipientList();
        //    saveList.AddRange(Document.Receipients.ToArray());
        //    // *********************
        //    ActionBy = ActionBy == null ? Document.Requestor : ActionBy;
        //    while (!CurrentItem.State.IsEndPoint && count < 10 && CurrentItem.PossibleActions(Document, ActionBy).Count > 0)
        //    {
        //        //3. GetDefaultAction ( index 0 in this version )
        //        oDefaultAction = CurrentItem.PossibleActions(Document, ActionBy)[0];
        //        count++;
        //        //5. Receive ReqActionResult
        //        RequestActionResult oResult = this.ProcessAction(Document, oDefaultAction, ActionBy, true);

        //        if (oResult.RequestFlowMultipleReceipientList != null)
        //            Document.RequestFlowMultipleReceipientList = oResult.RequestFlowMultipleReceipientList;
        //        //6. Append ResultList
        //        //ReceipientList list = new ReceipientList();
        //        //foreach (Receipient rcp in Document.TranslateList(oResult.Receipients))
        //        //{
        //        //    if (rcp.Type == ReceipientType.SPECIAL)
        //        //    {
        //        //        list.AddRange(Document.GetSpecialReceipient(rcp.Code));
        //        //    }
        //        //    else
        //        //    {
        //        //        list.Add(rcp);
        //        //    }
        //        //}
        //        Document.AuthorizeStep = oResult.AuthorizeStep;
        //        Document.AuthorizeDelegate = oResult.AuthorizeDelegate;
        //        Document.GroupName = oResult.GroupName;
        //        Document.Receipients.Clear();
        //        Document.Receipients.AddRange(oResult.Receipients.ToArray());
        //        ReceipientList list = Document.TranslateList(oResult.Receipients);

        //        //7. if not ReqActionResult.NextItem.isEndPoint :: repeat step 3 - 7
        //        if (oResult.IsRestartFlow)
        //        {
        //            Document.CalculateFlow();
        //            CurrentItem = FlowItem.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetFlowItem(Document.FlowID, Document.Flow.CreateItemID);
        //            ActionBy = Document.Requestor;
        //        }
        //        else if (oResult.GoToNextItem)
        //        {
        //            CurrentItem = oDefaultAction.NextItem;
        //        }

        //        if (list.Count > 0 || CurrentItem.State.IsEndPoint)
        //        {

        //            if (list.Count > 0)
        //            {
        //                RF = new RequestFlow();
        //                RF.GroupName = CurrentItem.GroupName;
        //                RF.ActionBy = list.ToString();

        //                if (ActionBy == null)
        //                    ActionBy = new EmployeeData();
        //                if (list[0].Type == ReceipientType.POSITION)
        //                {
        //                    ActionBy = ActionBy.GetEmployeeByPositionID(list[0].Code, DateTime.Now, WorkflowPrinciple.Current.UserSetting.Language);
        //                    if (ActionBy != null)
        //                    {
        //                        RF.ActionByCode = ActionBy.EmployeeID;
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        RF.ActionByPosition = ActionBy.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
        //                        //RF.ActionBy = string.Format("{0} {1}::{2}",ActionBy.EmployeeID, ActionBy.ActionOfPosition.Text, ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
        //                        RF.ActionBy = string.Format("{0} {1}", ActionBy.EmployeeID, ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
        //                    }
        //                    else
        //                    {
        //                        RF.ActionByCode = list[0].Code;
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        RF.ActionByPosition = string.Empty;
        //                        ActionBy = new EmployeeData(RF.ActionByCode);
        //                        // RF.ActionBy = string.Format("{0} {1}::{2}", ActionBy.EmployeeID, ActionBy.ActionOfPosition.Text, ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
        //                        RF.ActionBy = string.Format("{0} {1}", ActionBy.EmployeeID, ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
        //                    }
        //                }
        //                else
        //                {
        //                    RF.ActionByCode = list[0].Code;
        //                    if (list[0].Type == ReceipientType.USERROLE)
        //                    {
        //                        RF.ActionBy = ShowUserRoleDescription(list);
        //                        RF.ActionByPosition = string.Empty;
        //                        List<EmployeeData> userRoleResponse = UserRoleManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetUserResponse(list[0].Code, Document.Requestor);
        //                        if (userRoleResponse.Count > 0)
        //                        {
        //                            //string strPosition = string.Empty;
        //                            //string strEmployee = string.Empty;
        //                            //int index = 1;
        //                            foreach (EmployeeData emp in userRoleResponse)
        //                            {
        //                                ActionBy = emp;
        //                                break;
        //                                //strPosition = ActionBy.ActionOfPosition.Text;
        //                                //strEmployee += ActionBy.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
        //                                //if (index < userRoleResponse.Count)
        //                                //    strEmployee += ",";
        //                                //index++;
        //                            }
        //                            //RF.ActionBy = string.Format("{0}::{1}", strPosition, strEmployee);
        //                        }
        //                        else
        //                        {
        //                            ActionBy = new EmployeeData() { CompanyCode = Document.CompanyCode };
        //                        }
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        //RF.ActionByPosition = ActionBy.ActionOfPosition.Text;//list[0].DisplayName;
        //                    }
        //                    else
        //                    {
        //                        ActionBy = new EmployeeData(RF.ActionByCode);
        //                        //AddBy: Ratchatawan W. (2011-09-01)
        //                        RF.ActionByPosition = ActionBy.ActionOfPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
        //                    }
        //                }

        //                //RF.ActionCode = CurrentItem.PossibleActions[oDefaultAction.ID].NextItem.Code;
        //                RF.ActionCode = CurrentItem.Code;
        //                RF.IsCurrentItem = false;
        //                RF.Detail = oResult.Detail;
        //                oReturn.Add(RF);
        //            }

        //            if (CurrentItem.State.IsEndPoint)
        //            {
        //                RF = new RequestFlow();
        //                RF.GroupName = CurrentItem.GroupName;
        //                RF.ActionCode = CurrentItem.Code;
        //                RF.IsCurrentItem = false;
        //                RF.Detail = oResult.Detail;
        //                oReturn.Add(RF);
        //            }
        //        }
        //        else
        //        {
        //            RF = null;
        //            ActionBy = null;
        //        }
        //    }

        //    if (RF != null && CurrentItem != null && CurrentItem.State != null && CurrentItem.State.IsEndPoint)
        //    {
        //        RF.IsEndPoint = true;
        //    }

        //    // restore authorize step to currentItem after use in simulation
        //    Document.AuthorizeStep = nAuthStep;
        //    Document.AuthorizeDelegate = cAuthorizeDelegate;
        //    Document.GroupName = cGroupName;
        //    Document.Receipients.Clear();
        //    Document.Receipients.AddRange(saveList.ToArray());
        //    //8. Return ResultList
        //    return oReturn;
            return new List<RequestFlowForwardSnapshot>();
        }

        private string ShowUserRoleDescription(ReceipientList List)
        {
            string Description = string.Empty;
            foreach (Receipient rep in List)
            {
                Description += ","+string.Format("{0}", WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCommonText("USERROLENAME", WorkflowPrinciple.Current.UserSetting.Language, rep.Code));
            }

            return Description.TrimStart(','); ;
        }

        #endregion " SimulateProcess "

        #region " GetMailTemplate "
        public abstract string GetMailTemplate(int FlowID, int FlowItemID, int ID, string LanguageCode);

        public abstract string GetMailTemplateByID(int MailID, string LanguageCode);

        public abstract string GetMailSubjectByID(int MailID, string LanguageCode);

        public abstract string GetMailSubject(int FlowID, int FlowItemID, int ID, string LanguageCode);


        #endregion " GetMailTemplate "

        public void GenerateMailTo(Dictionary<int, List<WorkflowMailTo>> list, Receipient rcp, bool IsCC, RequestDocument Document,int TemplateMailID)
        {
            List<WorkflowMailTo> receipients;
            WorkflowMailTo item;
            EmployeeData oEmp = new EmployeeData();
            UserSetting us;
            bool IsAdd = true;

            switch (rcp.Type)
            {
                case ReceipientType.EMPLOYEE:
                    oEmp = new EmployeeData(rcp.Code,"",rcp.CompanyCode,DateTime.Now);
                    us = oEmp.GetUserSetting(rcp.Code);
                    if (!list.ContainsKey(TemplateMailID))
                    {
                        receipients = new List<WorkflowMailTo>();
                        list.Add(TemplateMailID, receipients);
                    }
                    item = new WorkflowMailTo();
                    item.MailTo = us.Employee.EmailAddress;
                    item.MailToDisplay = us.Employee.AlternativeNameForMail(us.Language);
                    item.IsCC = IsCC;
                    item.CompanyCode = rcp.CompanyCode;
                    item.EmployeeID = rcp.Code;
                    item.SentTo = us.Employee.EmployeeID;
                    item.LanguageCode = us.Language;
                     IsAdd = true;
                    foreach (WorkflowMailTo mailto in list[TemplateMailID]){if (mailto.EmployeeID == rcp.Code && mailto.LanguageCode == item.LanguageCode) { IsAdd = false; }}if (IsAdd) { list[TemplateMailID].Add(item); }

                    if (SHAREDATASERVICE.ShareDataManagement.IsTest)
                    {
                        us.Language = us.Language == "EN" ? "TH" : "EN";
                        item = new WorkflowMailTo();
                        item.MailTo = us.Employee.EmailAddress;
                        item.MailToDisplay = us.Employee.AlternativeNameForMail(us.Language);
                        item.IsCC = IsCC;
                        item.CompanyCode = rcp.CompanyCode;
                        item.EmployeeID = rcp.Code;
                        item.SentTo = us.Employee.EmployeeID;
                        item.LanguageCode = us.Language;
                        foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code && mailto.LanguageCode == item.LanguageCode) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }
                    }

                   

                    break;
                case ReceipientType.POSITION:
                    oEmp.CompanyCode = Document.RequestorCompanyCode;
                    oEmp = oEmp.GetEmployeeByPositionID(rcp.Code);
                    UserSetting us1 = oEmp.GetUserSetting(oEmp.EmployeeID);
                    if (!list.ContainsKey(TemplateMailID))
                    {
                        receipients = new List<WorkflowMailTo>();
                        list.Add(TemplateMailID, receipients);
                    }
                    item = new WorkflowMailTo();
                    item.MailTo = us1.Employee.EmailAddress;
                    item.MailToDisplay = us1.Employee.AlternativeNameForMail(us1.Language);
                    item.IsCC = IsCC;
                    item.CompanyCode = rcp.CompanyCode;
                    item.EmployeeID = oEmp.EmployeeID;
                    item.SentTo = us1.Employee.EmployeeID;
                    item.LanguageCode = us1.Language;
                     IsAdd = true;
                    foreach (WorkflowMailTo mailto in list[TemplateMailID]){if (mailto.EmployeeID == rcp.Code) { IsAdd = false; }}if (IsAdd) { list[TemplateMailID].Add(item); }

                    if (SHAREDATASERVICE.ShareDataManagement.IsTest)
                    {
                        us1.Language = us1.Language == "EN" ? "TH" : "EN";
                        item = new WorkflowMailTo();
                        item.MailTo = us1.Employee.EmailAddress;
                        item.MailToDisplay = us1.Employee.AlternativeNameForMail(us1.Language);
                        item.IsCC = IsCC;
                        item.CompanyCode = rcp.CompanyCode;
                        item.EmployeeID = oEmp.EmployeeID;
                        item.SentTo = us1.Employee.EmployeeID;
                        item.LanguageCode = us1.Language;
                        foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code && mailto.LanguageCode == item.LanguageCode) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }
                    }

                   

                    //If this position have Delegate person
                    List<EmployeeData> oDelegateList = oEmp.GetDelegateEmployeeForSentMail(oEmp.EmployeeID, rcp.Code, Document.RequestTypeID, DateTime.Now);
                    foreach (EmployeeData delegateEmp in oDelegateList)
                    {
                        UserSetting us3 = delegateEmp.GetUserSetting(delegateEmp.EmployeeID);
                        item = new WorkflowMailTo();
                        item.MailTo = us3.Employee.EmailAddress;
                        item.MailToDisplay = us3.Employee.AlternativeNameForMail(us3.Language);
                        item.IsCC = IsCC;
                        item.CompanyCode = rcp.CompanyCode;
                        item.EmployeeID = delegateEmp.EmployeeID;
                        item.SentTo = us3.Employee.EmployeeID;
                        item.LanguageCode = us3.Language;
                        IsAdd = true;
                        foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }

                        if (SHAREDATASERVICE.ShareDataManagement.IsTest)
                        {
                            us3.Language = us3.Language == "EN" ? "TH" : "EN";
                            item = new WorkflowMailTo();
                            item.MailTo = us3.Employee.EmailAddress;
                            item.MailToDisplay = us3.Employee.AlternativeNameForMail(us3.Language);
                            item.IsCC = IsCC;
                            item.CompanyCode = rcp.CompanyCode;
                            item.EmployeeID = delegateEmp.EmployeeID;
                            item.SentTo = us3.Employee.EmployeeID;
                            item.LanguageCode = us3.Language;
                            foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code && mailto.LanguageCode == item.LanguageCode) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }
                        }

                        
                    }

                    break;
                case ReceipientType.USERROLE:
                    List<EmployeeData> userRoleResponse = UserRoleManager.CreateInstance(Document.RequestorCompanyCode).GetUserResponse(rcp.Code, Document.Requestor);
                    foreach (EmployeeData emp in userRoleResponse)
                    {
                        us = emp.GetUserSetting(emp.EmployeeID);
                        if (!list.ContainsKey(TemplateMailID))
                        {
                            receipients = new List<WorkflowMailTo>();
                            list.Add(TemplateMailID, receipients);
                        }
                        item = new WorkflowMailTo();
                        item.MailTo = us.Employee.EmailAddress;
                        if (WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).SHOW_USERROLE_IN_MAIL_SUBJECT)
                        {
                            string RoleName = GetRequestText("USERROLENAME", us.Language, rcp.Code);
                            if (string.IsNullOrEmpty(RoleName))
                                item.MailToDisplay = string.Format("({0}) {1}", rcp.Code, us.Employee.AlternativeNameForMail(us.Language));
                            else
                                item.MailToDisplay = string.Format("({0}) {1}", RoleName, us.Employee.AlternativeNameForMail(us.Language)); 
                        }
                        else
                            item.MailToDisplay = string.Format("{0}", us.Employee.AlternativeNameForMail(us.Language)); 
                        item.IsCC = IsCC;
                        item.CompanyCode = rcp.CompanyCode;
                        item.EmployeeID = us.Employee.EmployeeID;
                        item.SentTo = us.Employee.EmployeeID;
                        item.LanguageCode = us.Language;

                        IsAdd = true;
                        foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }

                        if (SHAREDATASERVICE.ShareDataManagement.IsTest)
                        {
                            us.Language = us.Language == "EN" ? "TH" : "EN";
                            item = new WorkflowMailTo();
                            item.MailTo = us.Employee.EmailAddress;
                            item.MailToDisplay = us.Employee.AlternativeNameForMail(us.Language);
                            item.IsCC = IsCC;
                            item.CompanyCode = rcp.CompanyCode;
                            item.EmployeeID = us.Employee.EmployeeID;
                            item.SentTo = us.Employee.EmployeeID;
                            item.LanguageCode = us.Language;
                            foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code && mailto.LanguageCode == item.LanguageCode) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }
                        }

                       
                    }
                    break;
                case ReceipientType.EXTERNAL_USER:
                    if (!list.ContainsKey(TemplateMailID))
                    {
                        receipients = new List<WorkflowMailTo>();
                        list.Add(TemplateMailID, receipients);
                    }

                    DataTable dtExternal = EmployeeManagement.CreateInstance(rcp.CompanyCode).GetExternalUserSnapshotbyRequestID(Document.RequestID,int.Parse(rcp.Code));
                    foreach (DataRow dr in dtExternal.Rows)
                    {
                        item = new WorkflowMailTo();
                        item.MailTo = dr["Email"].ToString();
                        item.MailToDisplay = dr["Name"].ToString();
                        item.IsCC = IsCC;
                        item.IsExternalUser = true;
                        item.CompanyCode = rcp.CompanyCode;
                        item.EmployeeID = rcp.Code;
                        item.SentTo = rcp.Code;
                        item.LanguageCode = "TH";
                        IsAdd = true;
                        foreach (WorkflowMailTo mailto in list[TemplateMailID]) { if (mailto.EmployeeID == rcp.Code && mailto.LanguageCode == item.LanguageCode) { IsAdd = false; } } if (IsAdd) { list[TemplateMailID].Add(item); }
                    }

                    break;
            }
        }

        #region " ProcesssRequest "

        public RequestActionResult ProcessRequest(RequestDocument requestDocument, FlowItemAction action)
        {
            Receipient rcpThatHaveAuthorize;

            #region " Validate Request "

            rcpThatHaveAuthorize = this.CheckAuthorize(requestDocument);
            //if (requestDocument.FlowID != action.FlowID)
            //{
            //    throw new WorkflowException("FlowID not equal maybe have some error");
            //}
            if (requestDocument.SubmittedDate == DateTime.MinValue)
            {
                requestDocument.SubmittedDate = GetCurrentDate();
            }

            #endregion " Validate Request "

            requestDocument.CopyReceipient();

            #region " Process & Update "

            // find Position or Employee that currentUser have authorize
            EmployeeData ActionBy = null;
            if (rcpThatHaveAuthorize != null)
            {
                foreach (Receipient item in requestDocument.TranslateReceipient(rcpThatHaveAuthorize))
                {
                    if (item.Type == ReceipientType.POSITION)
                    {
                        ActionBy = ActionBy.GetEmployeeByPositionID(item.Code);
                    }
                    else
                    {
                        ActionBy = WorkflowPrinciple.Current.UserSetting.Employee;
                    }
                }
            }

            RequestActionResult oResult;
            ReceipientList rcpList;
            bool lBreak = false;
            do
            {
                oResult = ProcessAction(requestDocument, action, ActionBy, false);
                if (oResult.IsRestartFlow)
                {
                    requestDocument.CalculateFlow();
                    requestDocument.CurrentFlowItemID = requestDocument.Flow.CreateItemID;
                    rcpList = requestDocument.TranslateList(oResult.Receipients);
                    lBreak = true;
                }
                else
                {
                    if (oResult.GoToNextItem)
                    {
                        requestDocument.CurrentFlowItemID = action.NextItemID;
                    }
                    rcpList = requestDocument.TranslateList(oResult.Receipients);
                    requestDocument.AuthorizeStep = oResult.AuthorizeStep;

                    if (rcpList.Count == 0)
                    {
                        if (oResult.IsCancelled || oResult.IsCompleted || oResult.IsWaitForEdit)
                        {
                            lBreak = true;
                        }
                        else if (!action.NextItem.State.IsEndPoint && action.NextItem.PossibleActions(requestDocument, ActionBy).Count > 0)
                        {
                            action = action.NextItem.PossibleActions(requestDocument, ActionBy)[0];
                        }
                        else
                        {
                            lBreak = true;
                        }
                    }
                }
            } while (rcpList.Count == 0 && !lBreak);
            requestDocument.Receipients.Clear();
            requestDocument.Receipients.AddRange(oResult.Receipients);
            requestDocument.IsCancelled = oResult.IsCancelled;
            requestDocument.IsCompleted = oResult.IsCompleted;
            requestDocument.IsWaitForEdit = oResult.IsWaitForEdit;
            requestDocument.AuthorizeDelegate = oResult.AuthorizeDelegate;
            requestDocument.GroupName = oResult.GroupName;

            #endregion " Process & Update "

            //bool isSendMail = true;

            //if (isSendMail)
            //{
            //    #region " Notify Receipient "
            //    Dictionary<string, List<WorkflowMailTo>> list = new Dictionary<string, List<WorkflowMailTo>>();
            //    string cMailBody;

            //    #region " NotifyReceipients "
            //    if (action.NotifyReceipients)
            //    {
            //        foreach (Receipient rcp in requestDocument.TranslateList(requestDocument.Receipients))
            //        {
            //            this.GenerateMailTo(list, rcp, false);
            //        }
            //    }
            //    #endregion

            //    #region " NotifyRequestor "
            //    if (action.NotifyRequestor)
            //    {
            //        Receipient rcp;
            //        rcp = new Receipient(ReceipientType.EMPLOYEE, requestDocument.RequestorNo);
            //        this.GenerateMailTo(list, rcp, false);

            //        if (requestDocument.CreatorNo != requestDocument.RequestorNo)
            //        {
            //            rcp = new Receipient(ReceipientType.EMPLOYEE, requestDocument.CreatorNo);
            //            this.GenerateMailTo(list, rcp, false);
            //        }
            //    }
            //    #endregion

            //    #region " NotifyCurrent "
            //    if (action.NotifyCurrent)
            //    {
            //        foreach (Receipient rcp in requestDocument.TranslateList(requestDocument.BackupReceipients))
            //        {
            //            this.GenerateMailTo(list, rcp, false);
            //        }
            //    }
            //    #endregion

            //    #region " send mail "
            //    foreach (string cLangCode in list.Keys)
            //    {
            //        cMailBody = requestDocument.FillInText(action.GetMailTemplate(cLangCode), action, WorkflowPrinciple.Current.UserSetting.Employee, cLangCode);
            //        WorkflowMail oMailMessage = new WorkflowMail();
            //        oMailMessage.MailFrom = WorkflowPrinciple.Current.UserSetting.Employee.EmailAddress;
            //        //oMailMessage.MailFromDisplay = WorkflowPrinciple.Current.UserSetting.Employee.AlternativeName(cLangCode);
            //        foreach (WorkflowMailTo rcp in list[cLangCode])
            //        {
            //            oMailMessage.MailTo.Add(rcp);
            //        }
            //        if (oMailMessage.MailTo.Count == 0)
            //        {
            //            continue;
            //        }
            //        oMailMessage.MailSubject = requestDocument.FillInText(action.GetMailSubject(cLangCode), action, WorkflowPrinciple.Current.UserSetting.Employee, cLangCode);
            //        oMailMessage.MailBody = cMailBody;
            //        oMailMessage.Send();
            //    }
            //    #endregion

            //    #endregion
            //}

            return oResult;
        }

        public RequestActionResult ProcessRequest(RequestDocument requestDocument, FlowItemAction action, EmployeeData TakeActionBy)
        {
            Receipient rcpThatHaveAuthorize;
            #region " Validate Request "
            rcpThatHaveAuthorize = this.CheckAuthorize(requestDocument, TakeActionBy);
            //if (requestDocument.FlowID != action.FlowID)
            //{
            //    throw new WorkflowException("FlowID not equal maybe have some error");
            //}
           // if (requestDocument.SubmittedDate == DateTime.MinValue)
            if (action.Code == "SUBMIT")
            {
                requestDocument.SubmittedDate = GetCurrentDate();
            }

            #endregion

            requestDocument.CopyReceipient();

            #region " Process & Update "

            // find Position or Employee that currentUser have authorize
            EmployeeData ActionBy = null;
            if (rcpThatHaveAuthorize != null)
            {
                foreach (Receipient item in requestDocument.TranslateReceipient(rcpThatHaveAuthorize))
                {
                    ActionBy = new EmployeeData();
                    if (item.Type == ReceipientType.POSITION)
                    {
                        ActionBy = ActionBy.GetEmployeeByPositionID(item.Code);
                        //CommentBy:Ratchatawan W. (2016/07/31) 
                        //if (ActionBy.EmployeeID == Requestor.EmployeeID)
                        //    ActionBy = Requestor;
                    }
                    else
                    {
                        ActionBy = TakeActionBy;
                    }
                }

                if (rcpThatHaveAuthorize.Type == ReceipientType.SPECIAL_DELEGATE)
                {
                    ActionBy = TakeActionBy;
                    ActionBy.EmpSubGroupForDelegate = decimal.Parse(rcpThatHaveAuthorize.Code);
                }
            }

            RequestActionResult oResult;
            ReceipientList rcpList;
            bool lBreak = false;
            do
            {
                oResult = ProcessAction(requestDocument, action, ActionBy, false);
                if (oResult.IsRestartFlow)
                {
                    requestDocument.AuthorizeStep = 0;
                    //requestDocument.CalculateFlow();
                    //requestDocument.CurrentFlowItemID = requestDocument.Flow.CreateItemID;

                    requestDocument.FlowID = oResult.FlowID;
                    if (action.NextItem.Code == "WAIT_FOR_EDIT")
                        requestDocument.CurrentFlowItemID = requestDocument.Flow.EditItemID;
                    else if (action.NextItem.Code == "DRAFT")
                        requestDocument.CurrentFlowItemID = requestDocument.Flow.CreateItemID;
                    else if (action.NextItem.Code == "CANCELLED")
                        requestDocument.CurrentFlowItemID = requestDocument.Flow.CancelItemID;
                    else
                    {
                        requestDocument.CurrentFlowItemID = 0;
                        FlowItemAction oDefaultAction = requestDocument.CurrentFlowItem.PossibleActions(requestDocument, ActionBy)[0];
                        oResult = this.ProcessAction(requestDocument, oDefaultAction, ActionBy, true);
                        requestDocument.CurrentFlowItemID = oDefaultAction.NextItemID;
                        action.NotifyReceipients = oDefaultAction.NotifyReceipients;
                        action.NotifyCurrent = oDefaultAction.NotifyCurrent;
                        action.NotifyRequestor = oDefaultAction.NotifyRequestor;
                        action.NotifyAllActionInPass = oDefaultAction.NotifyAllActionInPass;
                        action.NotifyAutomaticAction = oDefaultAction.NotifyAutomaticAction;
                    }

                    rcpList = requestDocument.TranslateList(oResult.Receipients);
                    lBreak = true;
                }
                else
                {
                    if (oResult.GoToNextItem)
                    {
                        requestDocument.CurrentFlowItemID = action.NextItemID;
                    }
                    rcpList = requestDocument.TranslateList(oResult.Receipients);
                    requestDocument.AuthorizeStep = oResult.AuthorizeStep;

                    if (rcpList.Count == 0)
                    {
                        if (oResult.IsCancelled || oResult.IsCompleted || oResult.IsWaitForEdit)
                        {
                            lBreak = true;
                        }
                        else if (!action.NextItem.State.IsEndPoint && action.NextItem.PossibleActions(requestDocument, ActionBy).Count > 0)
                        {
                            action = action.NextItem.PossibleActions(requestDocument, ActionBy)[0];
                        }
                        else
                        {
                            lBreak = true;
                        }
                    }
                }
            } while (rcpList.Count == 0 && !lBreak);
            requestDocument.Receipients.Clear();
            requestDocument.Receipients.AddRange(oResult.Receipients);
            requestDocument.IsCancelled = oResult.IsCancelled;
            requestDocument.IsCompleted = oResult.IsCompleted;
            requestDocument.IsWaitForEdit = oResult.IsWaitForEdit;
            requestDocument.AuthorizeDelegate = oResult.AuthorizeDelegate;
            requestDocument.GroupName = oResult.GroupName;
            #endregion
            return oResult;
        }


        #endregion " ProcesssRequest "

        #region " CheckAuthorize "
        protected abstract Receipient CheckAuthorize(RequestDocument requestDocument);
        //AddBy: Ratchatawan W. (2012-09-06)
        protected abstract Receipient CheckAuthorize(RequestDocument requestDocument, EmployeeData Requestor);

        #endregion " CheckAuthorize "

        #region " LoadDocument "

        public RequestDocument LoadDocument(string RequestNo, string KeyMaster)
        {
            return LoadDocument(RequestNo, KeyMaster, false, false);
        }

        public RequestDocument LoadDocument(string RequestNo)
        {
            return LoadDocument(RequestNo, string.Empty, true, false);
        }

        public abstract RequestDocument LoadDocument(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView);
        public abstract RequestDocument LoadDocument(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView, EmployeeData Requestor);

        //AddBy: Ratchatawan W. (2012-12-27)
        public virtual RequestDocument LoadDocumentReadonly(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual RequestDocument LoadDocumentWithOutCheckAuthorize(string strRequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " LoadDocument "

        #region " UpdateDocument "

        public void UpdateDocument(RequestDocument requestDocument)
        {
            UpdateDocument(requestDocument, null);
        }

        #endregion " UpdateDocument "

        #region " SaveData "

        public abstract void SaveData(RequestDocument Document, DataTable Header, Object Additional);

        #endregion " SaveData "

        #region " SaveDataByPass "

        public abstract void SaveDataByPass(RequestDocument requestDocument, DataTable Header, Object Additional);

        #endregion " SaveDataByPass "

        #region " GetRequestDocumentList "

        public List<RequestDocument> GetRequestDocumentList(string BoxCode)
        {
            return GetRequestDocumentList(BoxCode, -1, -1, null);
        }

        public abstract List<RequestDocument> GetRequestDocumentList(string BoxCode, int ItemPerPage, int Page, RequestBoxFilter Filter);
        public abstract List<RequestDocument> GetRequestDocumentList(string BoxCode, string EmployeePositionID, int ItemPerPage, int Page, RequestBoxFilter Filter);
        //AddBy: Ratchatawan W. (2011-09-09)
        public abstract DataSet GetRequestDocumentList(int BoxID, EmployeeData employeeData, string Filter);
        public abstract string GetRequestTextOniService3(string Category, string Language, string Code);
        public abstract void UpdateRequestTypeSummarySnapshot(List<RequestDocument> oRequestDocumentList, int RequestTypeID);


        #endregion " GetRequestDocumentList "

        #region " GetDisplayName "

        public abstract string GetDisplayName(ReceipientType type, string code);

        #endregion " GetDisplayName "

        #endregion " Request "

        #region " Application "

        #region " GetAuthorizeApplications "

        public abstract List<RequestApplication> GetAuthorizeApplications();

        #endregion " GetAuthorizeApplications "

        #region " GetAuthorizeRequestTypes "

        public abstract List<ApplicationSubject> GetAuthorizeApplicationSubject(int ApplicationID);

        #endregion " GetAuthorizeRequestTypes "

        #region " GetApplicationSubjectResponse "

        public abstract List<UserRoleResponseSetting> GetApplicationSubjectResponse(int SubjectID);

        #endregion " GetApplicationSubjectResponse "

        #region " LoadApplicationSubject "

        public abstract ApplicationSubject LoadApplicationSubject(int SubjectID);

        #endregion " LoadApplicationSubject "

        #region " LoadApplicationSubjectAuthorize "

        public virtual List<string> LoadApplicationSubjectAuthorize(int SubjectID)
        {
            throw new Exception("The method 'List<string> LoadApplicationSubjectAuthorize(int SubjectID)' is not implemented.");
        }

        #endregion " LoadApplicationSubjectAuthorize "

        #endregion " Application "

        #region " Box "

        #region " GetAuthorizeBoxes "

        public abstract List<RequestBox> GetAuthorizeBoxes(string BoxCategoryCode);

        public abstract List<RequestBox> GetAuthorizeBoxes(string BoxCategoryCode, string EmployeeID);

        #endregion " GetAuthorizeBoxes "

        #region " GetBoxMode "

        public virtual bool GetBoxMode(string BoxCategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetBoxMode "

        #region " CountItemInbox "

        public abstract int CountItemInbox(string BoxCode, RequestBoxFilter Filter);

        #endregion " CountItemInbox "

        #region " GetAvailableRequestType "

        public abstract List<RequestType> GetAvailableRequestType(string BoxCode);

        #endregion " GetAvailableRequestType "

        #region " GetRequestBox "

        public abstract RequestBox GetRequestBox(string BoxCode);

        #endregion " GetRequestBox "

        #endregion " Box "

        #region " AuthorizationModel "

        #region " AuthorizationModel "

        #region " GetAllAuthorizationModel "

        public virtual List<AuthorizationModel> GetAllAuthorizationModel()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetAllAuthorizationModel "

        #endregion " AuthorizationModel "

        #region " GetAuthorizationModelKey "

        public virtual List<AuthorizationModelKey> GetAuthorizationModelKey(int ModelID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetAuthorizationModelKey "

        #region " GetAuthorizationModelSet "

        public virtual List<AuthorizationModelSet> GetAuthorizationModelSet(int ModelID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetAuthorizationModelSet "

        #region " GetAllAuthorizationModelCondition "

        public virtual List<AuthorizationModelSetCondition> GetAllAuthorizationModelCondition(int ModelID, int SetID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetAllAuthorizationModelCondition "

        #endregion " AuthorizationModel "

        #region " GetAuthorizationModel "

        public virtual AuthorizationModel GetAuthorizationModel(int ModelID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetAuthorizationModel "

        #region " GetAuthorizationModelStep "

        public virtual Receipient GetAuthorizationModelStep(int ModelID, int StepID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetAuthorizationModelStep "

        #region " CheckIsInRole "

        public virtual bool CheckIsInRole(string EmployeeID, Receipient RCP, string Requestor, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " CheckIsInRole "

        #region " CalculateAuthorize "

        public virtual List<AuthorizationModelSet> CalculateAuthorize(int ModelID, List<AuthorizationModelKey> list, List<object> values)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " CalculateAuthorize "

        #region " FileAttachment "

        #region " SaveFileAttachment "

        public abstract void SaveFileAttachment(FileAttachmentSet fileSet);

        #endregion " SaveFileAttachment "

        #region " LoadFileAttachment "

        public abstract FileAttachmentSet LoadFileAttachment(int FileSetID);

        #endregion " LoadFileAttachment "

        #endregion " FileAttachment "

        #region IWorkflowService Members

        public virtual List<EmployeeData> GetUserResponse(string Role, EmployeeData emp)
        {
            throw new Exception("The method 'List<EmployeeData> GetUserResponse(string Role, string AdminGroup)' is not implemented.");
        }

        public virtual  List<RequestFlag> GetRequestFlag(string RequestNo)
        {
            throw new Exception("The method RequestFlag GetRequestFlag(string RequestNo) is not implemented.");
        }

        public virtual void SaveRequestFlag(List<RequestFlag> lstFlag)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-09-12
        public virtual FileAttachmentSet GetRequestTypeFileSet(string employeeID, int requestTypeID, string requestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual FileAttachmentSet LoadMasterFileAttachment(EmployeeData employeeData, int requestTypeID, string subType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-09-12

        public virtual List<string> GetResponseCode(string Role, EmployeeData Emp)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IWorkflowService Members

        //public virtual List<EmployeeData> GetUserResponse(string Role, EmployeeData emp)
        //{
        //    throw new Exception("The method 'List<EmployeeData> GetUserResponse(string Role, string AdminGroup)' is not implemented.");
        //}

        #region " GetProjectManager "

        public virtual EmployeeData GetProjectManager(string ProjectRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetAllAdminGroup()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion " GetProjectManager "

        //AddBy: Ratchatawan W. (2012-02-27)
        public virtual List<string> GetResponseCodeByOrganizationOnly(string Role, EmployeeData Emp, string RootOrg)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-04-24)

        #region Delegate
        //AddBy: Ratchatawan W. (2011-08-23)
        public abstract bool CheckDelegateAuthorize(int RequestID);
        //AddBy: Ratchatawan W. (2012-09-06)
        public abstract bool CheckDelegateAuthorize(int RequestID, EmployeeData Requestor);
        //AddBy: Ratchatawan W. (2011-08-23)
        public abstract bool CheckDelegateAuthorize(string RequestNo);
        //AddBy: Ratchatawan W. (2011-08-23)

        public virtual DataTable GetRequestInDelegateBox(RequestBoxFilter Filter)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion Delegate

        //AddBy: Ratchatawan W. (2012-04-30)
        public virtual bool GetAuthorizeMassApprove(string BoxCode, string EmployeeID, string EmployeePositionID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateTicket(string TicketID, string RequestNo, string TicketFor, string TicketForCompanyCode, bool IsExternalUser)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateRequestTypeFileSet(FileAttachmentSet NewFileSet)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public RequestActionResult ProcessRequestForMobile(RequestDocument requestDocument, FlowItemAction action, EmployeeData Requestor, EmployeeData CurrentEmployee)
        {
            Receipient rcpThatHaveAuthorize;
            #region " Validate Request "
            rcpThatHaveAuthorize = this.CheckAuthorizeForMobile(requestDocument, Requestor, CurrentEmployee);
            //if (requestDocument.FlowID != action.FlowID)
            //{
            //    throw new WorkflowException("FlowID not equal maybe have some error");
            //}
            if (requestDocument.SubmittedDate == DateTime.MinValue)
            {
                requestDocument.SubmittedDate = GetCurrentDate();
            }
            #endregion

            requestDocument.CopyReceipient();

            #region " Process & Update "

            // find Position or Employee that currentUser have authorize
            EmployeeData ActionBy = null;
            if (rcpThatHaveAuthorize != null)
            {
                foreach (Receipient item in requestDocument.TranslateReceipient(rcpThatHaveAuthorize))
                {
                    if (item.Type == ReceipientType.POSITION)
                    {
                        ActionBy = ActionBy.GetEmployeeByPositionID(item.Code);
                        if (ActionBy.EmployeeID == Requestor.EmployeeID)
                            ActionBy = Requestor;
                    }
                    else
                    {
                        ActionBy = Requestor;
                    }
                }
            }

            RequestActionResult oResult;
            ReceipientList rcpList;
            bool lBreak = false;
            do
            {
                oResult = ProcessAction(requestDocument, action, ActionBy, false);
                if (oResult.IsRestartFlow)
                {
                    requestDocument.CalculateFlow();
                    requestDocument.CurrentFlowItemID = requestDocument.Flow.CreateItemID;
                    rcpList = requestDocument.TranslateList(oResult.Receipients);
                    lBreak = true;
                }
                else
                {
                    if (oResult.GoToNextItem)
                    {
                        requestDocument.CurrentFlowItemID = action.NextItemID;
                    }
                    rcpList = requestDocument.TranslateList(oResult.Receipients);
                    requestDocument.AuthorizeStep = oResult.AuthorizeStep;

                    if (rcpList.Count == 0)
                    {
                        if (oResult.IsCancelled || oResult.IsCompleted || oResult.IsWaitForEdit)
                        {
                            lBreak = true;
                        }
                        else if (!action.NextItem.State.IsEndPoint && action.NextItem.PossibleActions(requestDocument, ActionBy).Count > 0)
                        {
                            action = action.NextItem.PossibleActions(requestDocument, ActionBy)[0];
                        }
                        else
                        {
                            lBreak = true;
                        }
                    }
                }
            } while (rcpList.Count == 0 && !lBreak);
            requestDocument.Receipients.Clear();
            requestDocument.Receipients.AddRange(oResult.Receipients);
            requestDocument.IsCancelled = oResult.IsCancelled;
            requestDocument.IsCompleted = oResult.IsCompleted;
            requestDocument.IsWaitForEdit = oResult.IsWaitForEdit;
            requestDocument.AuthorizeDelegate = oResult.AuthorizeDelegate;
            requestDocument.GroupName = oResult.GroupName;
            #endregion
            return oResult;
        }
        public virtual void RecallUpdateDocument(string RequestNo, string ActionBy, string ActionByPosition)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        protected abstract Receipient CheckAuthorizeForMobile(RequestDocument requestDocument, EmployeeData Requestor, EmployeeData CurrentEmployee);

        public abstract bool GetAuthorizeMassApproveForMobile(int BoxID, string EmployeeID, string EmployeePositionID);

        public virtual int CountRequestBox(int BoxID, string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public abstract void RequestTypeSummarySave(int RequestID, int ItemID, string SummaryTextTH, string SummaryTextEN);

        public abstract DataTable GetRequestInDelegateBox();

        public abstract List<PTTCHEM.WORKFLOW.RequestFlowMultipleReceipient> GetRequestFlowMultipleReceipientList(int RequestID);

        public abstract List<RequestFlowMultipleReceipient_External> GetRequestFlowMultipleReceipient_ExternalList(int RequestID);

        public abstract List<FlowItemAction> GetAllPossibleAction(int FlowID, int FlowItemID, RequestDocument Doc, EmployeeData ActionBy);

        public abstract RequestBox GetRequestBox(int iBoxID);

        public abstract void ResolveReceipientToOtherCompany(DataRow dr);

        public abstract DataTable GetUserRoleResponseForOtherCompany(string EmployeeID, string CompanyCode);

        public abstract DataTable GetFlowItemActionPreference();

        public abstract EmployeeData GetLastedActionByActionCode(int RequestID, string ActionCode);
        public abstract EmployeeData GetLastedManagerByRequestNo(string RequestNo);
        
        public virtual List<ResponseLog> GetResponseLog(string oRequestNo)
        {
            throw new NotImplementedException();
        }
        public virtual DataTable GetResponseManualLog(string oRequestNo)
        {
            throw new NotImplementedException();
        }
        public virtual String PDMSStatusGetForView(string oRequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetRequestTypeKeySetting(int RequestTypeID)
        {
            throw new NotImplementedException();
        }


        public virtual EmployeeData GetLastedActionByActionCode(string RequestNo, string ActionCode)
        {
            throw new NotImplementedException();
        }


        public virtual List<Receipient> GetAllActionInPass(int RequestID, string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual DataTable ValidateTicket(string TicketID, string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual List<EmployeeData> GetDelegateToByEmployeePosition(string EmployeeID, string PositionID)
        {
            throw new NotImplementedException();
        }

        public virtual List<DelegateHeader> GetDelegateDataByCriteria(string EmployeeID, int Month, int Year)
        {
            throw new NotImplementedException();
        }


        public virtual DataTable GetAllRequestType(string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetDelegationInfo(int RequestID)
        {
            throw new NotImplementedException();
        }

        public virtual string SaveDelegateHeader(DelegateHeader header)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteDelegateHeader(DelegateHeader header)
        {
            throw new NotImplementedException();
        }


        public virtual void DeleteRequestFlowForwardSnapshotByRequestID(int RequestID)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveRequestFlowForwardSnapshot(List<RequestFlowForwardSnapshot> ForwardFlowList)
        {
            throw new NotImplementedException();
        }


        public virtual string GetLastedCommentByActionCode(int RequestID, string ActionCode)
        {
            throw new NotImplementedException();
        }


        public virtual string GetLastedActionByEmployeeID(int RequestID, string EmployeeID, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetActionInPassByRequestNo(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetRecipientByItemID(int RequestID, int ItemID)
        {
            throw new NotImplementedException();
        }


        public virtual string GetEditReasonByRequestNo(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<RequestDocument> GetRequestDocumentByRequestTypeID(int RequestTypeID)
        {
            throw new NotImplementedException();
        }


        public virtual string GetAuthorizeBoxByEmployeeID(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable ValidateTicketForExternalUser(string TicketID, string ExternalUserID)
        {
            throw new NotImplementedException();
        }
        public virtual bool CheckStatusPDMSForRejectToRequestor(string oRequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void InsertPDMSRequestWaitForReturnToRequestor(string Requestno, string ActionBy, string ActionByCompanyCode)
        {
            throw new NotImplementedException();
        }
        public virtual void UpdatePDMSRequestWaitForReturnToRequestor(string RequestNo, string Status)
        {
            throw new NotImplementedException();
        }

        public virtual DataSet GetPDMSRequestWaitForReturnToRequestorForRunJob(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void AutoRejectRequest_WaitForPDMSStatus(RequestDocument oDoc, FlowItemAction oAction, EmployeeData oEmpData)
        {
            throw new NotImplementedException();
        }

        public virtual void ClearFlagSuggestionFromAccount(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void ResponseFlagSave(string RequestNo, string TransactionType, string ServiceURL, DateTime TransactionStartDate, DateTime ServiceFinishDate, string ServiceResult, string ServiceException, DateTime DocumentFinishDate, string DocumentException)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckResponseFlag(string RequestNo)
        {
            throw new NotImplementedException();
        }
        public virtual DataSet GetPositionApprove(int BoxID,string EmployeeID,string PositionID,string Language)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetEmployeeByUserRole(int RequestID, string UserRole, string Language)
        {
            throw new NotImplementedException();
        }

    }
}