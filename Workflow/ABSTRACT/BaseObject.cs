using System;
using System.ComponentModel;

namespace ESS.WORKFLOW.ABSTRACT
{
    [Serializable()]
    public class BaseObject
    {
        public BaseObject()
        {
        }
        [DefaultValue(-1)]
        public virtual int ID { get; set; }
    }
}