namespace ESS.WORKFLOW
{
    public class RequestBoxFilter
    {
        public RequestBoxFilter()
        {
        }
        public int RequestTypeID { get; set; }
        public string DelegateView { get; set; }
        public string RequestorNo { get; set; }
        public string AdminGroup { get; set; }
        public bool IsEmpty
        {
            get
            {
                return this.RequestorNo == "" && this.RequestTypeID == -1 && this.DelegateView == "" && this.AdminGroup == "";
            }
        }
        public object CreatorNo { get; set; }
    }
}