﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.DELEGATE.DATACLASS;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;
using System.Data;
using ESS.DATA.INTERFACE;
using ESS.DATA.ABSTRACT;
using System.Reflection;
using ESS.MAIL.DATACLASS;
using ESS.DATA.EXCEPTION;

namespace ESS.WORKFLOW
{
    public class UpdateRequestTypeSummaryTask : AbstractTaskWorker
    {
        List<RequestDocument> RequestDocumentList = new List<RequestDocument>();
        int RequestTypeID = 0;

        public UpdateRequestTypeSummaryTask(List<RequestDocument> _RequestDocumentList, int _RequestTypeID)
        {
            RequestDocumentList = _RequestDocumentList;
            RequestTypeID = _RequestTypeID;
        }

        public override void Run()
        {
            WorkflowManagement.CreateInstance(this.CompanyCode).UpdateRequestTypeSummarySnapshot(RequestDocumentList, RequestTypeID);
        }
    }
}
