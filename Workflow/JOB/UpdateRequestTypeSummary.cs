﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.EMPLOYEE;
using ESS.JOB;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;
using System.Data;

namespace ESS.WORKFLOW
{
    public class UpdateRequestTypeSummary : AbstractJobWorker
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public UpdateRequestTypeSummary()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> oReturn = new List<ITaskWorker>();
            List<string> oReqTypeID = new List<string>() { "110",
"105",
"116",
"108",
"103"};
            foreach (string strRequestTypeID in oReqTypeID)
            {
                UpdateRequestTypeSummaryTask otask = new UpdateRequestTypeSummaryTask(WorkflowManagement.CreateInstance(this.CompanyCode).GetRequestDocumentByRequestTypeID(int.Parse(strRequestTypeID)), int.Parse(strRequestTypeID));
                oReturn.Add(otask);
            }
            return oReturn;
        }
    }
}