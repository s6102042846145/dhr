﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace ESS.WORKFLOW
{
    public class ResponseLogDetail
    {
        public ResponseLogDetail()
        { 
        }
        public string RequestNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public int DetailID { get; set; }
        public string TxNo { get; set; }
        public string PostDocID { get; set; }
        public string ReverseDocID { get; set; }
        public string Message { get; set; }
        public string RawData { get; set; }
    }
}