using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    public class RequestApplication : BaseCodeObject
    {
        [DefaultValue(false)]
        public bool EnableForMobile { get; set; }
        public bool ShowCounter { get; set; }

        public string IconClass { get; set; }

        #region Constructor
        public RequestApplication()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, RequestApplication> Cache = new Dictionary<string, RequestApplication>();
        //public string CompanyCode { get; set; }
        //public static string ModuleID
        //{
        //    get
        //    {
        //        return "ESS.WORKFLOW";
        //    }
        //}
        //public static RequestApplication CreateInstance(string oCompanyCode)
        //{
        //    if (!Cache.ContainsKey(oCompanyCode))
        //    {
        //        Cache[oCompanyCode] = new RequestApplication()
        //        {
        //            CompanyCode = oCompanyCode
        //            ,
        //            Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
        //        };
        //    }
        //    return Cache[oCompanyCode];
        //}
        #endregion MultiCompany  Framework


    }
}