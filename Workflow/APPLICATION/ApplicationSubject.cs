using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class ApplicationSubject : BaseCodeObject
    {
        #region Constructor
        public ApplicationSubject()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, ApplicationSubject> Cache = new Dictionary<string, ApplicationSubject>();
        public string CompanyCode { get; set; }

        public string IconClass { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static ApplicationSubject CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new ApplicationSubject()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            ApplicationSubject oApplicationSubject = new ApplicationSubject()
            {
                CompanyCode = oCompanyCode
            };
            return oApplicationSubject;
        }
        #endregion MultiCompany  Framework

        #region ApplicationSubject
        public void CheckAuthorize()
        {
            List<string> roles = ServiceManager.CreateInstance(CompanyCode).WorkflowService.LoadApplicationSubjectAuthorize(this.ID);
            bool isAuth = false;
            foreach (string role in roles)
            {
                if (WorkflowPrinciple.Current.IsInRole(role))
                {
                    isAuth = true;
                    break;
                }
            }
            if (!isAuth)
            {
                throw new WorkflowException("NOT_AUTHORIZE");
            }
        }
        public List<UserRoleResponseSetting> LoadUserResponse(int SubjectID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetApplicationSubjectResponse(SubjectID);
        }

        public ApplicationSubject LoadApplicationSubject(int SubjectID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.LoadApplicationSubject(SubjectID);
        }

        #endregion
    }
}