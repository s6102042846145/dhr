﻿using ESS.DATA;
using ESS.HR.OM.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using SAP.Connector;
using SAPInterface;
using SAPInterfaceExt;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ESS.HR.OM.SAP
{
    public class HROMDataServiceImpl : AbstractHROMDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        #region Constructor
        public HROMDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.OM.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");
            }
        }

        #endregion Member




    }
}