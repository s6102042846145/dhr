using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.UTILITY.EXTENSION;
using ESS.WORKFLOW;
using System.Data;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class EmployeeData : AbstractObject
    {
        #region " Constructors "
        public EmployeeData()
            : this("", DateTime.Now)
        {
        }

        public EmployeeData(string employeeid)
            : this(employeeid, "", DateTime.Now)
        {
        }


        public EmployeeData(string employeeid, string positionid)
            : this(employeeid, positionid, DateTime.Now)
        {
        }

        public EmployeeData(string employeeid, string positionid, string companycode)
            : this(employeeid, positionid, companycode, DateTime.Now)
        {
        }


        public EmployeeData(string employeeid, DateTime checkDate)
            : this(employeeid, "", checkDate)
        {
        }

        public EmployeeData(string employeeid, string positionid, DateTime checkDate)
        {
            this.EmployeeID = employeeid;
            this.CheckDate = checkDate;
            this.PositionID = positionid;
            if (WorkflowPrinciple.Current != null)
            {
                this.CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            if (!string.IsNullOrEmpty(positionid))
            {
                this.ActionOfPosition = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, positionid, checkDate, currentSetting.Language);
            }
        }

        public EmployeeData(string employeeid, string positionid, string companycode, DateTime checkDate)
        {
            this.EmployeeID = employeeid;
            this.CheckDate = checkDate;
            this.CompanyCode = companycode;
            if (!string.IsNullOrEmpty(positionid))
            {
                this.ActionOfPosition = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, positionid, checkDate, currentSetting.Language);
                this.PositionID = positionid;
            }
        }

        #endregion " Constructors "

        #region " Basic Property "
        public List<INFOTYPE1000> AllPosition
        {
            get
            {
                return this.GetAllPositions(WorkflowPrinciple.Current.UserSetting.Language);
            }
        }
        public string IdentityCardNO
        {
            get
            {
                string oIdentityCardNo = string.Empty;
                List<INFOTYPE0185> oINFOTYPE0185 = EmployeeManagement.CreateInstance(CompanyCode).GetInfotype0185ByEmpID(WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID);
                if (oINFOTYPE0185 != null && oINFOTYPE0185.Count > 0)
                {
                    oIdentityCardNo = oINFOTYPE0185.Find(all => all.SubType == "01" || all.SubType == "06").CardID;
                }
                return oIdentityCardNo;
            }
        }
        public EmployeeSubAreaType EmpSubAreaType
        {
            //Each Corporate not same
            //GPSC Empower Style
            //get
            //{
            //    switch (this.OrgAssignment.SubArea.Substring(this.OrgAssignment.SubArea.Length - 2, 2))
            //    {
            //        case "01":
            //            return EmployeeSubAreaType.Flex;

            //        case "02":
            //            return EmployeeSubAreaType.Norm;

            //        case "03":
            //            return EmployeeSubAreaType.Turn;

            //        case "04":
            //            return EmployeeSubAreaType.Shift12;

            //        default:
            //            return new EmployeeSubAreaType();
            //    }
            //}

            // KVIS DHR Style
            get
            {
                switch (this.OrgAssignment.SubArea)
                {
                    case "F":
                        return EmployeeSubAreaType.Flex;

                    case "N":
                        return EmployeeSubAreaType.Norm;

                    case "T":
                        return EmployeeSubAreaType.Turn;

                    case "S":
                        return EmployeeSubAreaType.Shift12;

                    default:
                        return new EmployeeSubAreaType();
                }
            }
        }

        public string EmployeeID { get; set; }

        public DateSpecificData __dateSpecificData { get; set; }

        public string CompanyCode { get; set; }

        public bool IsExternalUser
        {
            get
            {
                return false;
                // return ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsExternalUser(this.EmployeeID);
            }
        }

        public DateTime CheckDate { get; set; }

        private string __PositionID;
        public string PositionID
        {
            get
            {
                if (string.IsNullOrEmpty(__PositionID))
                    return this.OrgAssignment.Position;
                else
                    return __PositionID;
            }
            set { __PositionID = value; }
        }
        private DataTable __monthlyWorkSchedule = null;
        public INFOTYPE0001 OrgAssignment
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0001(this.EmployeeID, this.CheckDate);
            }
        }

        public INFOTYPE0030 Secondment
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0030(this.EmployeeID, this.CheckDate); ;
            }
        }

        private INFOTYPE1000 __actionOfPostion = null;
        public INFOTYPE1000 ActionOfPosition
        {
            get
            {
                if (__actionOfPostion == null)
                    __actionOfPostion = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, this.OrgAssignment.Position, this.CheckDate.Date, currentSetting.Language);
                return __actionOfPostion;
            }
            set
            {
                __actionOfPostion = value;
            }
        }

        public INFOTYPE1000 CurrentPosition
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, this.PositionID, this.CheckDate.Date, currentSetting.Language);
            }
        }

        public INFOTYPE1000 Job
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetJobByPositionID(this.OrgAssignment.Position, CheckDate); ;
            }
        }


        //Commented by Koissares 20190926 Did not found any code assign value to JobLevel
        //public int JobLevel
        //{
        //    get
        //    {
        //        int level = 0;
        //        try
        //        {
        //            int length = Job.ShortText.Length;
        //            if (length >= 2)
        //                level = int.Parse(Job.ShortText.Substring(length - 2, 2));
        //        }
        //        catch (Exception e)
        //        {
        //            throw new Exception("Job Object has a invalid job level in short text", e);
        //        }
        //        return level;
        //    }
        //}

        public INFOTYPE1000 CurrentOrganization
        {
            get
            {
                if ((WorkflowPrinciple.CurrentIdentity.EmployeeID == this.EmployeeID))
                {
                    return OMManagement.CreateInstance(CompanyCode).GetOrganizationByPositionID(WorkflowPrinciple.CurrentIdentity.CurrentPosition, WorkflowPrinciple.Current.UserSetting.Language);
                }
                else
                {
                    return OMManagement.CreateInstance(CompanyCode).GetOrganizationByPositionID(this.ActionOfPosition == null ? this.PositionID : this.ActionOfPosition.ObjectID, WorkflowPrinciple.Current.UserSetting.Language);
                }
            }
        }

        public INFOTYPE1013 PositionBandLevel
        {
            get
            {
                //if (__infotype1013 == null && this.ActionOfPosition != null)
                //{
                //    __infotype1013 = ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(ActionOfPosition.ObjectID, this.CheckDate);
                //}
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(ActionOfPosition.ObjectID, this.CheckDate); ;
            }
        }

        public INFOTYPE1003 ApprovalData
        {
            get
            {
                //if (__infotype1003 == null)
                //{
                //    __infotype1003 = ServiceManager.CreateInstance(CompanyCode).OMService.GetApprovalData(ActionOfPosition.ObjectID, this.CheckDate);
                //}
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetApprovalData(ActionOfPosition.ObjectID, this.CheckDate); ;
            }
        }

        ////Comment by Koissares 20190926 sp_MonthlyWorkScheduleGet not found
        //public DataTable MonthlyWorkSchedule
        //{
        //    get
        //    {
        //        if (__monthlyWorkSchedule == null)
        //        {
        //            __monthlyWorkSchedule = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetMonthlyWorkSchedule(this.EmployeeID);
        //        }
        //        return __monthlyWorkSchedule;
        //    }
        //    set
        //    {
        //        __monthlyWorkSchedule = value;
        //    }
        //}

        public List<INFOTYPE0007> GetWorkSchedule(int Year, int Month)
        {
            return EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(this.EmployeeID, Year, Month);
        }

        #endregion " Basic Property "

        #region " Method "

        #region " GetInfoType "

        public Type GetInfotype(string code)
        {
            Type oType = ServiceManager.CreateInstance(CompanyCode).GetInfotype(code);
            return oType;
        }

        #endregion " GetInfoType "

        public List<EmployeeData> GetDelegatePersons()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDelegatePersons(EmployeeID);
        }

        public UserSetting GetUserSetting(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserSetting(EmployeeID);
        }

        public INFOTYPE0007 GetEmployeeWF(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0007(EmployeeID, CheckDate);
        }

        public string GetEmployeeIDFromUserID(string UserID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeIDFromUserID(UserID);
        }
        public string GetEmployeeIDFromUserID(string UserID, string oCompanyCode)
        {
            return ServiceManager.CreateInstance(oCompanyCode).EmployeeService.GetEmployeeIDFromUserID(UserID);
        }
        public bool ValidateEmployeeID(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateEmployeeID(EmployeeID);
        }

        //AddBy: Ratchatawan W. (2012-04-23)
        public bool ValidateManager(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateManager(EmployeeID);
        }

        public INFOTYPE1000 GetFirstRoot(string LanguageCode)
        {
            return OMManagement.CreateInstance(CompanyCode).GetOrgRoot(this.OrgAssignment.OrgUnit, 1, this.CheckDate, LanguageCode);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID)
        {
            return GetEmployeeByPositionID(PositionID, DateTime.Now);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, string LanguageCode)
        {
            return GetEmployeeByPositionID(PositionID, DateTime.Now, LanguageCode);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            return GetEmployeeByPositionID(PositionID, CheckDate, "");
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate, string LanguageCode)
        {
            EmployeeData oReturn;
            INFOTYPE1000 oPosition = ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, PositionID, CheckDate, LanguageCode);
            oReturn = OMManagement.CreateInstance(CompanyCode).GetEmployeeByPositionID(PositionID, CheckDate);
            if (oReturn != null)
                oReturn.ActionOfPosition = oPosition;
            return oReturn;
        }

        public EmployeeData FindManager(string ManagerCode)
        {
            return new EmployeeData();
            //return ServiceManager.CreateInstance(CompanyCode).EmployeeService.FindManager(this.EmployeeID,this.PositionID, ManagerCode, this.CheckDate,WorkflowPrinciple.Current.UserSetting.Language);
        }

        public List<EmployeeData> GetOMByEmployeePosition(string EmployeeID, string Position, DateTime CheckDate)
        {
            //return ServiceManager.CreateInstance(CompanyCode).GetOMByEmployeePosition(this.EmployeeID, Position, this.CheckDate);
            return EmployeeManagement.CreateInstance(CompanyCode).GetOMByEmployeePosition(EmployeeID, Position, CheckDate);
        }

        #region " GetUserRoles "

        public List<string> GetUserRoles()
        {
            if (currentSetting == null)
            {
                currentSetting = new EmployeeData().GetUserSetting(this.EmployeeID);
            }
            return currentSetting.Roles;
        }

        public List<string> GetUserRoles2()
        {
            if (currentSetting == null)
            {
                currentSetting = new EmployeeData().GetUserSetting(this.EmployeeID);
            }
            return currentSetting.Roles;
        }

        #endregion " GetUserRoles "

        #region " AlternativeName "

        public INFOTYPE1000 PositionData(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, this.OrgAssignment.Position, this.CheckDate, Language);
        }

        #endregion " AlternativeName "

        #region " Role action "

        public bool IsInRole(string role)
        {
            return GetUserRoles().Contains(role.ToUpper());
        }

        public bool IsInRole(List<string> roles)
        {
            foreach (string role in roles)
            {
                if (IsInRole(role))
                {
                    return true;
                }
            }
            return false;
        }

        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2013-03-05)
        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles, DateTime oCheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role, oCheckDate);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2013-03-0)
        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles, DateTime BeginDate, DateTime EndDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role, BeginDate, EndDate);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles, string EmployeeID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role, EmployeeID);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        public bool IsUserInResponse(List<UserRoleResponseSetting> roles, string EmployeeID)
        {
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    if (ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsUserInResponse(role, EmployeeID))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion " Role action "

        #endregion " Method "

        #region " Property for Flow "

        #region " Name "

        public string Name
        {
            get
            {
                if (OrgAssignment == null)
                {
                    if (EmployeeID.StartsWith("#"))
                    {
                        if (EmployeeID == "#SYSTEM")
                        {
                            return "WORKFLOW SYSTEM";
                        }
                        else
                        {
                            return EmployeeID;
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return OrgAssignment.Name;
                }
            }
        }

        #endregion " Name "

        #region " AlternativeName "

        public string AlternativeName(string Language)
        {
            if (Language.ToUpper() == "TH")
            {
                return Name;
            }
            else
            {
                //Pending for support EN version
                INFOTYPE0182 name = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0182(this.EmployeeID, Language);
                return name.AlternateName;
                //return Name;
            }
        }

        public string AlternativeNameForMail(string Language)
        {
            if (this.IsExternalUser)
            {
                return this.Name;
            }
            else
            {
                if (Language.ToUpper() == "TH")
                {
                    return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetNameFromINFOTYPE0002(this.EmployeeID);
                }
                else
                {
                    INFOTYPE0182 name = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0182(this.EmployeeID, Language);
                    return name.AlternateName;
                }
            }
        }

        #endregion " AlternativeNameWithTitle "
        public string AlternativeNameWithTitle(string Language)
        {
            if (Language.ToUpper() == "TH")
            {
                return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetNameWithTitleFromINFOTYPE0002(this.EmployeeID);
            }
            else
            {
                INFOTYPE0182 name = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0182(this.EmployeeID, Language);
                return name.AlternateName;
            }
        }
        #region "AlternativeNameWithTitle"

        #endregion

        #region Email
        public string EmailAddress
        {
            get
            {
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105(this.EmployeeID, "MAIL");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }
        #endregion

        #region MobileNo
        public string MobileNo
        {
            get
            {
                //CELLPHONE
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105(this.EmployeeID, "CELLPHONE");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }
        #endregion

        #region FaxNumber
        public string FaxNumber
        {
            get
            {
                //FAXNUMBER
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105(this.EmployeeID, "FAXNUMBER");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }

        #endregion

        #region HomeNumber
        public string HomeNumber
        {
            get
            {
                //HOMENUMBER
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105(this.EmployeeID, "HOMENUMBER");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }
        #endregion
        public string OfficeNumber
        {
            get
            {
                //OFFICENUMBER
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105(this.EmployeeID, "OFFICENUMBER");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }


        #region " EmpGroup "
        public string EmpGroup
        {
            get
            {
                if (this.PositionBandLevel == null)
                {
                    return "";
                }
                return this.PositionBandLevel.EmpGroup;
            }
        }

        #endregion " EmpGroup "


        #region " EmpSubGroup "

        public decimal EmpSubGroup
        {
            get
            {
                if (EmpSubGroupForDelegate > 0)
                    return EmpSubGroupForDelegate;
                Decimal retVal = 0;
                if (this.PositionBandLevel != null)
                    Decimal.TryParse(this.PositionBandLevel.EmpSubGroup, out retVal);
                return retVal;
            }
        }

        public int BrandNo
        {
            get
            {
                int retVal = 0;
                if (this.PositionBandLevel != null)
                    retVal = this.PositionBandLevel.BrandNo;
                return retVal;
            }
        }

        public decimal EmpSubGroupForDelegate { get; set; }

        #endregion " EmpSubGroup "

        #region " NextEmpGroup "

        public string NextEmpGroup
        {
            get
            {
                //if (__nextEmpGroup == null)
                //{
                //    INFOTYPE1000 nextPos = OMManagement.CreateInstance(CompanyCode).FindNextPosition(this.ActionOfPosition.ObjectID, this.CheckDate, "EN");
                //    __nextEmpGroup = ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(nextPos.ObjectID, this.CheckDate);
                //    return __nextEmpGroup.EmpGroup;
                //}
                try
                {
                    INFOTYPE1000 nextPos = OMManagement.CreateInstance(CompanyCode).FindNextPosition(this.ActionOfPosition.ObjectID, this.CheckDate, "EN");
                    //Nattawat S. Add new if-else
                    if (nextPos == null)
                        return "";
                    else
                        return ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(nextPos.ObjectID, this.CheckDate).EmpGroup;
                }
                catch(Exception ex)
                {
                    return "";
                }
                
            }
        }

        #endregion " NextEmpGroup "

        #region " IsManager "

        public bool IsManager
        {
            get
            {
                return this.IsHaveSubOrdinate && !this.ApprovalData.IsStaff;
            }
        }

        public bool IsSystem { get; set; }


        #endregion " IsManager "

        #region " IsHaveSubOrdinate "

        public bool IsHaveSubOrdinate
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).OMService.IsHaveSubOrdinate(ActionOfPosition.ObjectID, this.CheckDate);
            }
        }

        #endregion " IsHaveSubOrdinate "

        #region " UserRoles "

        public string UserRoles
        {
            get
            {
                string returnValue = "";
                foreach (string roleName in GetUserRoles())
                {
                    returnValue += string.Format("{0},", roleName);
                }
                return returnValue;
            }
        }

        #endregion " UserRoles "

        #endregion " Property for Flow "

        #region " Other properties "

        protected EmployeeData currentLogon
        {
            get
            {
                return currentSetting.Employee;
            }
        }

        protected UserSetting currentSetting
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new UserSetting("", "");
                }
                return WorkflowPrinciple.Current.UserSetting;
            }
            set
            {

            }
        }

        public string MultiLanguageName
        {
            get
            {
                if (currentSetting.Language == "TH")
                {
                    return OrgAssignment.Name;
                }
                else
                {
                    return AlternativeName(currentSetting.Language);
                }
            }
        }

        public DateSpecificData DateSpecific
        {
            get
            {
                if (__dateSpecificData == null)
                {
                    __dateSpecificData = EmployeeManagement.CreateInstance(CompanyCode).GetDateSpecificData(this.EmployeeID);
                }
                return __dateSpecificData;//EmployeeManagement.CreateInstance(CompanyCode).GetDateSpecificData(this.EmployeeID);
            }
        }

        public object PersonalData
        {
            get
            {
                Type type = GetInfotype("INFOTYPE0002");
                AbstractInfoType objectdata = (AbstractInfoType)Activator.CreateInstance(type);
                return objectdata.LoadData(EmployeeID, "", CheckDate, CheckDate);

                //return new INFOTYPE0002().LoadData(EmployeeID, "", CheckDate, CheckDate);//Added by Koissares 20190828
            }
        }


        public INFOTYPE1000 DepartmentData
        {
            get
            {
                return OMManagement.CreateInstance(CompanyCode).FindDepartment(this.OrgAssignment.OrgUnitData, currentSetting.Language);
            }
        }

        public List<INFOTYPE1000> GetAllPositions(string LanguageCode)
        {
            return OMManagement.CreateInstance(CompanyCode).GetAllPosition(this.EmployeeID, this.CheckDate, LanguageCode);
        }

        #endregion " Other properties "

        public List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDelegateEmployeeForSentMail(DelegateFromID, DelegateFromPositionID, RequestTypeID, CheckDate);
        }

        public DataTable ValidateData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateData(this.EmployeeID);
        }

        public DailyWS GetDailyWorkSchedule(DateTime date)
        {
            bool IncludeSubstitute = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CONFIG_CalendarUseSubstitute;
            return GetDailyWorkSchedule(date, IncludeSubstitute);

        }
        public DailyWS GetDailyWorkSchedule(DateTime date, bool includeSubstitute)
        {
            DailyWS oDWS = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWorkschedule(this.EmployeeID, date);

            if (includeSubstitute)
            {
                List<Substitution> substitutionList = EmployeeManagement.CreateInstance(CompanyCode).GetSubstitutionList(this.EmployeeID, date, date, true);

                if (substitutionList.Count > 0 && substitutionList[0].BeginDate <= date.Date && substitutionList[0].EndDate >= date.Date)
                {
                   
                    oDWS = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWorkschedule(substitutionList[0].DWSGroup, substitutionList[0].DWSCode, date.Date);
                }
            }
            return oDWS;
        }

        public DateTime GetLastWorkingDate()
        {
            DateTime lastWorkingDate = CheckDate.Date;
            while (true)
            {
                if (lastWorkingDate <= this.DateSpecific.HiringDate)
                    break;
                DailyWS dws = this.GetDailyWorkSchedule(lastWorkingDate);
                if (dws.IsHoliday || dws.IsDayOff)
                {
                    lastWorkingDate = lastWorkingDate.AddDays(-1);
                }
                else
                {
                    break;
                }
            }

            return lastWorkingDate;
        }

    }
}