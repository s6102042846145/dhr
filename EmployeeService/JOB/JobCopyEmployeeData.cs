using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class JobCopyEmployeeData : AbstractJobWorker
    {
        public JobCopyEmployeeData()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyEmployeeData task;

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0001";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetINFOTYPE0001List_EmployeeID1_EmployeeID2_CheckDate_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 0;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0002";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetINFOTYPE0002List_EmployeeID1_EmployeeID2_Profile; //EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 1;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0007";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetINFOTYPE0007List_EmployeeID1_EmployeeID2_Profile; //EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 2;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0105";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetINFOTYPE0105List_EmployeeID1_EmployeeID2_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 3;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0105_MAPPING";
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetINFOTYPE0105_MAPPING;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 4;
            returnValue.Add(task); 

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0182";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetINFOTYPE0182List_EmployeeID1_EmployeeID2_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 5;
            returnValue.Add(task);
          

            task = new TaskCopyEmployeeData();
            task.InfoType = "DATESPECIFIC";
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetDateSpecific;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 6;
            returnValue.Add(task);
            return returnValue;
        }
    }
}