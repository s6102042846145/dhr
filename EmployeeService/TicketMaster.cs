using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE
{
    public class TicketMaster : AbstractObject
    {

        public TicketMaster()
        {
        }
        public string EmployeeID{ get; set; }
        public string TicketClass { get; set; }
        public string TicketID { get; set; }
        public DateTime TicketExpired { get; set; }

    }
}