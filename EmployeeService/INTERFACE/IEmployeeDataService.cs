using System;
using System.Collections.Generic;
using System.Data;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.JOB;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG;

namespace ESS.EMPLOYEE.INTERFACE
{
    public interface IEmployeeDataService
    {
        #region " User "

        UserSetting GetUserSetting(string EmployeeID);

        List<string> GetUserRole(string EmployeeID);

        List<string> GetUserInRole(string UserRole);

        UserRoleSetting GetUserRoleSetting(string UserRole);

        List<EmployeeData> GetUserResponse(string Role, string AdminGroup);

        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting);

        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmployeeID);

        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime oCheckDate);

        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate, DateTime EndDate);

        bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID);

        bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID, DateTime checkDate);


        void SaveUserSetting(UserSetting userSetting);

        #endregion " User "

        bool ValidateActiveEmployeeForTravel(string EmployeeID, DateTime BeginTravelDate);

        List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID, DateTime CheckDate);

        string GetNameFromINFOTYPE0002(string EmployeeID);

        string GetNameWithTitleFromINFOTYPE0002(string EmployeeID);

        string GetEmployeeIDFromUserID(string UserID);

        bool ValidateEmployeeID(string EmployeeID);

        bool ValidateEmployeeID(string EmployeeID, DateTime CheckDate);

        EmployeeData FindManager(string EmployeeID, string PositionID, string ManagerCode, DateTime CheckDate, string LanguageCode);

        List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID);

        List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID1, string EmployeeID2);

        List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID1, string EmployeeID2, string Profile);

        List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile);

        CARDSETTING GetCardSetting(string EmployeeID);

        CARDSETTING GetCardSetting(string EmployeeID, DateTime CheckDate);

        List<CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate, string Profile);

        INFOTYPE0001 GetINFOTYPE0001(string EmployeeID);

        INFOTYPE0001 GetINFOTYPE0001(string EmployeeID, DateTime CheckDate);

        List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID);

        List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID, DateTime CheckDate);

        List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2);

        List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, string Profile);
        List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2, string Profile);
        List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile);

        void SaveINFOTYPE0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile);
        void SaveINFOTYPE0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> data, string profile);

        void SaveINFOTYPE0001(List<INFOTYPE0001> data, string RequestNo);

        INFOTYPE0182 GetINFOTYPE0182(string EmployeeID, string Language);

        void UpdateINFOTYPE0182(string EmployeeID, string Language, INFOTYPE0182 item);

        void SaveINFOTYPE0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile);

        List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID);

        List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2);

        List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2, string Profile);

        INFOTYPE0105 GetINFOTYPE0105(string EmployeeID, string SubType);

        List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string CategoryCode);

        List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID);
        List<INFOTYPE0027> GetInfotype0027List(string EmployeeID, DateTime CheckDate);
        List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2);

        List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2, string Profile);

        void SaveINFOTYPE0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile);

        //AddBy: Ratchatawan W. (2012-02-22)
        INFOTYPE0007 GetINFOTYPE0007(string EmployeeID, DateTime CheckDate);

        INFOTYPE0007 GetINFOTYPE0007(string EmployeeID, DateTime CheckDate, string Profile);

        List<INFOTYPE0007> GetINFOTYPE0007(string EmployeeID, int Year, int Month);

        List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2);

        List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, string Profile);
        List<INFOTYPE0027> GetInfotype0027List(string EmployeeID1, string EmployeeID2, string Profile);

        List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile);

        List<INFOTYPE0007> GetINFOTYPE0007List(int Year, int Month, string TimeEvaluateClass);

        List<INFOTYPE0007> GetINFOTYPE0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass);

        void SaveINFOTYPE0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile);
        void SaveInfotype0027(string EmployeeID1, string EmployeeID2, List<INFOTYPE0027> data, string profile);

        void SaveINFOTYPE0007(List<INFOTYPE0007> data, string RequestNo);

        void DeleteINFOTYPE0007(List<INFOTYPE0007> data, string RequestNo);

        List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        List<Substitution> GetInfotype2003_Log(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        List<Substitution> GetInfotype2003_LogList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<Substitution> GetInfotype2003_LogListInverse(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        List<Substitution> GetInfotype2003_POSTING(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<Substitution> GetInfotype2003_POSTINGInverse(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        List<EmployeeData> GetDelegatePersons(string EmployeeID);

        List<INFOTYPE0001> GetAllINFOTYPE0001(DateTime CheckDate);

        List<INFOTYPE0001> GetAllEmployeeName(string Language);

        List<INFOTYPE0001> GetAllEmployeeNameForUserRole(string Language);

        List<TaskCopyEmployeeConfig> GetTasks();

        List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID);

        void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile);

        List<WorkPlaceCommunication> GetWorkplaceData();

        void SaveWorkPlaceData(List<WorkPlaceCommunication> workplaceList, string Profile);

        DateSpecificData GetDateSpecific(string EmployeeID);

        DateSpecificData GetERPDateSpecific(string EmployeeID);

        List<DateSpecificData> GetDateSpecificList();

        string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode);

        List<INFOTYPE0001> GetMeEmpList(string EmployeeID);

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        DataTable GetEmployeeIDByOrgUnit(string OrgUnit);

        List<string> GetUserResponseType(string EmpID, string UserRole);

        DataTable GetRequestorPerformance(string EmpID);

        DataTable GetRequestorCompetency(string EmpID);

        //AddBy: Ratchatawan W. (2012-04-23)
        bool ValidateManager(string EmployeeID);

        //AddBy: Ratchatawan W. (2012-04-23)
        List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID);

        //AddBy: Ratchatawan W. (2012-08-08)
        List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate);

        //AddBy: Ratchatawan W. (2012-11-07)
        List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate);

        //AddBy: Ratchatawan W. (2012-12-24)
        INFOTYPE0032 GetINFOTYPE0032ByEmployeeID(string EmployeeID, DateTime CheckDate);

        List<INFOTYPE0032> GetINFOTYPE0032List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile);

        void SaveINFOTYPE0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile);

        //AddBy: Ratchatawan W. (2013-04-12)
        List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate);

        bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate);

        //AddBy: Nattawat S. 23052020
        DataSet GetUserInResponseForActionOfInsteadWithIncludeSub(string EmployeeID, int SubjectID, DateTime CheckDate, string FilterTEmpGroup, string FilterText);
        DataSet GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, string SearchText);
        DataTable GetUserInResponseForActionOfInsteadByResponseType(string ResponseType, string ResponseCode, string ResponseCompanyCode, bool IncludeSub, DateTime CheckDate,string SearchText);

        void SaveINFOTYPE0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile);

        INFOTYPE0030 GetINFOTYPE0030(string EmployeeID, DateTime CheckDate);

        List<INFOTYPE0030> GetINFOTYPE0030List(string EmployeeID1, string EmployeeID2, string Profile);

        List<INFOTYPE0185> GetInfotype0185(string EmployeeID, DateTime date);

        List<INFOTYPE0001> GetINFOTYPE0001AllList(string EmployeeID);

        string InsertINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182);

        void UpdateINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182);

        void InsertINFOTYPE0001(INFOTYPE0001 oINF1);

        void UpdateINFOTYPE0001(INFOTYPE0001 oINF1);

        void InsertINFOTYPE0105(INFOTYPE0105 oINF105);

        void UpdateINFOTYPE0105(INFOTYPE0105 oINF105);

        string InsertINFOTYPE0002And0182And0001And0105Data(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182, INFOTYPE0001 oINF1, INFOTYPE0105 oINF105);

        void DeleteINFOTYPE0002And0182And0001And0105And1001Data(string EmployeeID);

        void DeleteINFOTYPE0002And0182(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        void DeleteINFOTYPE0001(string EmployeeID, DateTime BeginDate);

        void DeleteINFOTYPE0105(string EmployeeID, DateTime BeginDate, string SubType);

        void Resign(string EmployeeID, DateTime EndDate);

        List<INFOTYPE0001> INFOTYPE0001GetAllEmployeeHistory(string Language);

        Dictionary<string, object> INFOTYPE0002And0182GetAllHistory(string EmployeeID);

        List<INFOTYPE0001> INFOTYPE0001GetAllHistory(string EmployeeID,string Language);

        List<INFOTYPE0105> INFOTYPE0105GetAllHistory(string EmployeeID);

        Dictionary<string, object> INFOTYPE0002And0182Get(string EmployeeID,DateTime BeginDate);

        INFOTYPE0001 INFOTYPE0001Get(string EmployeeID, DateTime BeginDate, string Language);

        INFOTYPE0105 INFOTYPE0105Get(string EmployeeID, DateTime BeginDate,string CategoryCode);

        List<INFOTYPE1000> INFOTYPE1000GetAllHistory();

        void DeleteINFOTYPE1000(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate);

        void InsertINFOTYPE1000(INFOTYPE1000 oINF1000);

        void UpdateINFOTYPE1000(INFOTYPE1000 oINF1000);

        INFOTYPE1000 INFOTYPE1000Get(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate);

        List<INFOTYPE1001> INFOTYPE1001GetAllHistory();

        void InsertINFOTYPE0002And0182And0001And0105And1001List(List<INFOTYPE0001> INF0001Lst, List<INFOTYPE0002> INF0002Lst, List<INFOTYPE0182> INF0182Lst, List<INFOTYPE0105> INF0105Lst, List<INFOTYPE1001> INF1001Lst);

        List<INFOTYPE1001> GetPositionForPerson(string NextObjectID);

        List<INFOTYPE1001> GetBelongToRelation();

        INFOTYPE1001 INFOTYPE1001Get(string ObjectType, string ObjectID, string NextObjectType, string NextObjectID, string Relation, DateTime BeginDate, DateTime EndDate);

        void DeleteINFOTYPE1001(INFOTYPE1001 oINF1001);

        void InsertINFOTYPE1001(INFOTYPE1001 oINF1001, INFOTYPE1001 oOldINF1001);

        void UpdateINFOTYPE1001(INFOTYPE1001 oINF1001);

        List<INFOTYPE1013> INFOTYPE1013GetAllHistory();

        void DeleteINFOTYPE1013(INFOTYPE1013 oINF1013);

        void InsertINFOTYPE1013(INFOTYPE1013 oINF1013);

        void UpdateINFOTYPE1013(INFOTYPE1013 oINF1013);

        INFOTYPE1013 INFOTYPE1013Get(string ObjectID, DateTime BeginDate, DateTime EndDate);

        DataTable GetOrganizationStructure(string EmployeeID, string PositionID);
		
		DataTable ValidateData(string EmployeeID);

        bool InsertActionLog(UTILITY.DATACLASS.ActionLog oActionLog);
        bool InsertJobActionLog(JobActionLog oActionLog);
        DataTable GetMonthlyWorkSchedule(string EmployeeID);

        List<INFOTYPE0001> GetAllINFOTYPE0001ForReport(DateTime oCheckDate, string oLanguage);

        DataTable GetExternalUserbyID(string UserID, string oLanguage);

        DataTable GetExternalUserSnapshotbyRequestID(int RequestID, int ApproverID);

        DataTable GetPettyCustodianGetByCode(string PettyCode);

        DataSet GetContractDetailByEmployeeID(string EmployeeID, string CostCenter, DateTime CheckDate);

        bool IsContractUser(string EmployeeID);
        bool IsExternalUser(string EmployeeID);

        #region Job Data from DHR

        string JobALL_DHR_TO_INFOTYPE0001(string EmployeeID1, string EmployeeID2);
        string JobALL_DHR_TO_INFOTYPE0002(string EmployeeID1, string EmployeeID2);
        string JobALL_DHR_TO_INFOTYPE0007(string EmployeeID1, string EmployeeID2);
        string JobALL_DHR_TO_INFOTYPE0027(string EmployeeID1, string EmployeeID2);
        string JobALL_DHR_TO_INFOTYPE0030(string EmployeeID1, string EmployeeID2);
        string JobALL_DHR_TO_INFOTYPE0105(string EmployeeID1, string EmployeeID2);
        string JobALL_DHR_TO_INFOTYPE0105_Mapping(out string oMessage);
        string JobALL_DHR_TO_INFOTYPE0182(string EmployeeID1, string EmployeeID2);
        string JobAll_DHR_TO_DateSpecificData();
        #endregion Job Data from DHR

        DataTable GetEmployeesList(string oLang);

        #region PINCODE
        bool VerifyPinCode(string EmployeeID, string oPincode);
        bool ExistPinCodeByEmployee(string EmployeeID);
        void ChangePinCode(string EmployeeID, string oPincode, string oNewPINcode);
        void RequestNEWPin(EmployeeData oEmp);
        bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID);
        void CreateNEWPin(string EmployeeID, string TicketID, string oPincode);
        #endregion

        DataTable GetFlowIDByRequestTypeIDKeyCode(string RequestTypeID);

        List<EmployeeData> GetOMByEmployeePosition(string EmployeeID, string Position, DateTime CheckDate);
        DataTable GetConfigSelectYear();
    }
}