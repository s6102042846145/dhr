﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.CONFIG
{
    public class AttachedFileConfiguration : AbstractObject
    {
        public string CategoryGroup { get; set; }
        public string CategoryType { get; set; }
        public string CategorySubType { get; set; }
        public int GroupAttachFile { get; set; }
        public string Description { get; set; }
    }
}
