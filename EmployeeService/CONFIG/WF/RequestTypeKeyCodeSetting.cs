﻿using System;
using ESS.UTILITY.EXTENSION;
using System.Collections.Generic;

namespace ESS.EMPLOYEE.CONFIG.WF
{
    public class RequestTypeKeyCodeSetting : AbstractObject
    {
        public string RequestTypeID { get; set; }
        public int Version { get; set; }
        public int Priority  { get; set; }
        public string KeyCode { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool? IsActionInsteadOf { get; set; }

    }

}