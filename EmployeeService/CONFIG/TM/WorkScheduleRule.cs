﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    public class WorkScheduleRule
    {
        public string EmpSubGroupForWorkSchedule { get; set; }
        public string PublicHolidayCalendar { get; set; }
        public string EmpSubAreaForWorkSchedule { get; set; }
        public string WSRule { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}
