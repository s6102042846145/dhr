using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    //[Serializable()]
    public class DailyWS : AbstractObject
    {
        public string DailyWorkscheduleGrouping { get; set; }

        private string __dailyWorkscheduleCode;
        public string DailyWorkscheduleCode
        {
            get
            {
                return this.IsHoliday ? "HOL" : __dailyWorkscheduleCode;
            }
            set
            {
                __dailyWorkscheduleCode = value;
            }
        }

        public string WorkscheduleClass { get; set; }
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal WorkHours { get; set; }

        public TimeSpan WorkBeginTime { get; set; }

        public long WorkBeginTimeTicks
        {
            get { return WorkBeginTime.Ticks; }
        }

        public TimeSpan WorkEndTime { get; set; }

        public long WorkEndTimeTicks
        {
            get { return WorkEndTime.Ticks; }
        }

        public TimeSpan NormalBeginTime { get; set; }


        public TimeSpan NormalEndTime { get; set; }

        public string BreakCode { get; set; }


        public bool IsDayOff
        {
            get
            {
                return WorkHours == 0.00m;
            }
        }

        public bool IsHoliday { get; set; }

        public bool IsDayOffOrHoliday
        {
            get
            {
                return this.IsDayOff || this.IsHoliday;
            }
        }

        public DateTime WorkBeginDateTime { get; set; }
       
        public DateTime WorkEndDateTime { get; set; }
        

        public bool IsAttendance0192 { get; set; }

        public bool IsNormOrTNorm1(string ValuationClass)
        {
            return ",1,2,".IndexOf(ValuationClass.Trim()) > -1;
        }

    }
}