using System;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class WFRuleSetting : AbstractObject
    {
        public WFRuleSetting()
        {
        }

        public string WFRule { get; set; }

        public TimeSpan NormalBeginTime { get; set; }
        public TimeSpan NormalEndTime { get; set; }

        public TimeSpan CutoffTime { get; set; }

        
    }
}