﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0002 : AbstractInfoType
    {
        public INFOTYPE0002()
        {
        }
        public override string InfoType
        {
            get
            {
                return "0002";
            }
        }

        public string TitleID { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Initial { get; set; }
        public string NickName { get; set; }
        public string NameFormat { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string BirthPlace { get; set; }
        public string BirthCity { get; set; }
        public string Nationality { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime MaritalEffectiveDate { get; set; }
        public int NoOfChild { get; set; }
        public string Religion { get; set; }
        public string Language { get; set; }
        public string Remark { get; set; }
        public string MilitaryTitle { get; set; }
        public string SecondTitle { get; set; }
        public string NamePrefix1 { get; set; }
        public string NamePrefix2 { get; set; }
        public string SecondNationality { get; set; }
        public string ThirdNationality { get; set; }
    }
}
