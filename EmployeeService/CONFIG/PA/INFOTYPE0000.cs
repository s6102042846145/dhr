using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0000 : AbstractInfoType
    {
        public INFOTYPE0000()
        {
        }

        public string EmploymentStatus { get; set; }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetINFOTYPE0000List(EmployeeID1, EmployeeID2, DateTime.Now.Date, Profile).ToArray());
            return oReturn;
        }

        public override string InfoType
        {
            get { return "0000"; }
        }
    }
}