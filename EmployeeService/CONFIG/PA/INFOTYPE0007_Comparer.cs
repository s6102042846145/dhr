using System.Collections.Generic;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    public class INFOTYPE0007_Comparer : IComparer<INFOTYPE0007>
    {
        public int Compare(INFOTYPE0007 obj1, INFOTYPE0007 obj2)
        {
            return obj1.BeginDate.CompareTo(obj2.BeginDate);
        }
    }
}