using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0007 : AbstractInfoType
    {
        private string __WSRule;
        private string __timeEvaClass;
        private string __remark;

        public INFOTYPE0007()
        {
        }

        public string WFRule
        {
            get
            {
                return __WSRule;
            }
            set
            {
                __WSRule = value;
            }
        }

        public string TimeEvaluateClass
        {
            get
            {
                return __timeEvaClass;
            }
            set
            {
                __timeEvaClass = value;
            }
        }

        public String Remark
        {
            get { return __remark; }
            set
            {
                __remark = value;
            }
        }

        public override string InfoType
        {
            get { return "0007"; }
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetINFOTYPE0007List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0007> List = new List<INFOTYPE0007>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0007)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0007(EmployeeID1, EmployeeID2, List, Profile);
        }


    }
}