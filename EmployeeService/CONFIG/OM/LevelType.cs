﻿namespace ESS.EMPLOYEE.CONFIG.OM
{
    public enum OrgLevelType
    {
        MAN1 = 10,
        MAN2 = 20,
        MAN3 = 30,
        MAN4 = 40,
        Phai = 50,   // ฝ่าย
        Suan = 60,   // ส่วน
        Noi = 70,    // หน่วย
        Panak = 80   // แผนก
    }
}