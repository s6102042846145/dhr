﻿using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    public class INFOTYPE1010 : AbstractObject
    {
        public INFOTYPE1010()
        {
        }

        public string ObjectID{ get; set; }
        public DateTime BeginDate{ get; set; }
        public DateTime EndDate{ get; set; }
        public string OrgLevel{ get; set; }

    }
}