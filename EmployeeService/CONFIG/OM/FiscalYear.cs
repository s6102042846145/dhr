﻿using System;
using ESS.UTILITY.EXTENSION;
using System.Collections.Generic;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    public class FiscalYear : AbstractObject
    {
        public FiscalYear()
        {
            startMonth = "01";
            endMonth = "12";
        }
        public string startMonth { get; set; }
        public string endMonth { get; set; }
        public string companyCode { get; set; }
    }

}