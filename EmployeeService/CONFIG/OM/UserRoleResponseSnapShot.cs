﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    public class UserRoleResponseSnapShot
    {
        public UserRoleResponseSnapShot(UserRoleResponse oResponse, INFOTYPE1000 oOrg)
        {
            // TODO: Complete member initialization
            this.EmployeeID = oResponse.EmployeeID;
            this.CompanyCode = oResponse.CompanyCode;
            this.UserRole = oResponse.UserRole;
            this.ResponseType = oResponse.ResponseType;
            this.ResponseCode = oOrg.ObjectID;
            this.ResponseCompanyCode = oResponse.ResponseCompanyCode;
            this.BeginDate = oOrg.BeginDate;
            this.EndDate = oOrg.EndDate;
        }

        public UserRoleResponseSnapShot(UserRoleResponse oResponse)
        {
            this.EmployeeID = oResponse.EmployeeID;
            this.CompanyCode = oResponse.CompanyCode;
            this.UserRole = oResponse.UserRole;
            this.ResponseType = oResponse.ResponseType;
            this.ResponseCode = oResponse.ResponseCode;
            this.ResponseCompanyCode = oResponse.ResponseCompanyCode;
            this.BeginDate = new DateTime(2000, 1, 1);
            this.EndDate = new DateTime(9999, 12, 31);
        }
        public string EmployeeID { get; set; }
        public string CompanyCode { get; set; }
        public string UserRole { get; set; }
        public string ResponseType { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseCompanyCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
