namespace ESS.EMPLOYEE.CONFIG.OM
{
    public enum ObjectType
    {
        P = 1, //Employee
        S = 2, //POSITION
        O = 3, //ORGANIZE
        C = 4, //JOB
        K = 5, //COSTCENTER
        Q = 6, //QUALIFICATION
        QK = 7 //QUALIFICATION CATALOUGUE
    }
}