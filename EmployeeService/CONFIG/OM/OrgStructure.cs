using System;
using System.Collections.Generic;
using ESS.SHAREDATASERVICE;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    public class OrgStructure
    {
	 #region Constructor
        private OrgStructure()
        {

        }
        #endregion 
	    #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, OrgStructure> Cache = new Dictionary<string, OrgStructure>();

        private static string ModuleID = "ESS.EMPLOYEE";

        public string CompanyCode { get; set; }

        public static OrgStructure CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new OrgStructure()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            OrgStructure oOrgStructure = new OrgStructure()
            {
                CompanyCode = oCompanyCode
            };
            return oOrgStructure;
        }
        #endregion MultiCompany  Framework
        public int SectionLevel  // ˹���
        {
            get
            {
                return int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SECTIONLEVEL"));//70;
            }
        }

        public int DivisionLevel
        {
            get
            {
                return int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DIVISIONLEVEL")); //5;
            }
        }

        public int DepartmentLevel
        {
            get
            {
                return int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEPARTMENTLEVEL")); //4;
            }
        }

        public int VCLevel
        {
            get
            {
                return int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VCLEVEL")); //2;
            }
        }

    }
}