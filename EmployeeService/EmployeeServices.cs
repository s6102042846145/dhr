﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace ESS.EMPLOYEE
{
    public class EmployeeServices
    {
        public EmployeeServices()
        {

        }
        private static string ModuleID = "ESS.EMPLOYEE.SVC";
        private string CompanyCode { get; set; }

        public static EmployeeServices SVC (string oCompanyCode)
        {
            EmployeeServices oEmployeeServices = new EmployeeServices()
            {
                CompanyCode = oCompanyCode
            };
            return oEmployeeServices;
        }


        //DateSpecificData GetDateSpecific(string EmployeeID);
        public string EMPLOYEE_GetDateSpecific_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DATESPECIFIC_EMPLOYEEID");
            }
        }

        //List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        public string EMPLOYEE_GetINFOTYPE0001List_EmployeeID1_EmployeeID2_CheckDate_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0001LIST_EMPLOYEEID1_EMPLOYEEID2_CHECKDATE_PROFILE");
            }
        }

        //List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2, string Profile)
        public string EMPLOYEE_GetINFOTYPE0002List_EmployeeID1_EmployeeID2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0002LIST_EMPLOYEEID1_EMPLOYEEID2_PROFILE");
            }
        }

        //List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, string Profile)
        public string EMPLOYEE_GetINFOTYPE0007List_EmployeeID1_EmployeeID2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0007LIST_EMPLOYEEID1_EMPLOYEEID2_PROFILE");
            }
        }

        //List<INFOTYPE0027> GetInfotype0027List(string EmployeeID1, string EmployeeID2, string Profile)
        public string EMPLOYEE_GetINFOTYPE0027List_EmployeeID1_EmployeeID2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0027LIST_EMPLOYEEID1_EMPLOYEEID2_PROFILE");
            }
        }

        // List<INFOTYPE0030> GetINFOTYPE0030List(string EmployeeID1, string EmployeeID2, string Profile)
        public string EMPLOYEE_GetINFOTYPE0030List_EmployeeID1_EmployeeID2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0030LIST_EMPLOYEEID1_EMPLOYEEID2_PROFILE");
            }
        }

        // List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2, string Profile)
        public string EMPLOYEE_GetINFOTYPE0105List_EmployeeID1_EmployeeID2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0105LIST_EMPLOYEEID1_EMPLOYEEID2_PROFILE");
            }
        }
        public string EMPLOYEE_GetINFOTYPE0105_MAPPING
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0105_MAPPING");
            }
        }

        //List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2, string Profile)
        public string EMPLOYEE_GetINFOTYPE0182List_EmployeeID1_EmployeeID2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0182LIST_EMPLOYEEID1_EMPLOYEEID2_PROFILE");
            }
        }

        //List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string SourceProfile)
        public string EMPLOYEE_GetMonthlyWorkscheduleList_Year_SourceProfile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MONTHLYWORKSCHEDULELIST_YEAR_SOURCEPROFILE");
            }
        }

        //List<DailyWS> GetDailyWorkscheduleList(string Profile)
        public string EMPLOYEE_GetDailyWorkscheduleList_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DAILYWORKSCHEDULELIST_PROFILE");
            }
        }

        public string EMPLOYEE_CHANGEPINCODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_CHANGEPINCODE");
            }
        }

        public string EMPLOYEE_VERIFY_PINCODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_VERIFY_PINCODE");
            }
        }

        public string EMPLOYEE_EXIST_PINCODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_EXIST_PINCODE");
            }
        }

        #region OM

        //List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile)
        public string EMPLOYEE_OM_GetAllObject_INFOTYPE1000_ObjId1_ObjId2_ObjectTypes_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLOBJECT_INFOTYPE1000_OBJID1_OBJID2_OBJECTTYPE_PROFILE");
            }
        }

        //List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        public string EMPLOYEE_OM_GetAllRelation_INFOTYPE1001_ObjId1_ObjId2_ObjectTypes_RelationCodes_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLRELATION_INFOTYPE1001_OBJID1_OBJID2_OBJECTTYPE_RELATIONCODES_PROFILE");
            }
        }

        //List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjID1, string ObjID2, string Profile)
        public  string EMPLOYEE_OM_GetAllOrgUnitLevel_INFOTYPE1010_ObjId1_ObjId2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLORGUNITLEVEL_INFOTYPE1010_OBJID1_OBJID2_PROFILE");
            }
        }

        //List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        public string EMPLOYEE_OM_GetAllPositionBandLevel_INFOTYPE1013_ObjId1_ObjId2_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLPOSITIONBANDLEVEL_INFOTYPE1013_OBJID1_OBJID2_PROFILE");
            }
        }
        #endregion OM


        public string MINPINCODE_LENGHT
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MINPINCODE_LENGHT");
            }
        }

        public string EMPLOYEE_REQUEST_PINCODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_REQUEST_PINCODE");
            }
        }

        public string EMPLOYEE_VALIDATE_TICKET
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_VALIDATE_TICKET");
            }
        }

        public string EMPLOYEE_CREATE_PINCODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_CREATE_PINCODE");
            }
        }



        #region MonthlyWS
        //IncludeSubstitute
        public bool EMPLOYEE_CONFIG_CalendarUseSubstitute
        {
            get
            {
                return Convert.ToBoolean(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONFIG_CALENDARUSESUBSTITUTE"));
            }
        }
        //MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month)
        public string EMPLOYEE_CONFIG_EmpSubGroupForWorkSchedule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONFIG_EMPSUBGROUPFORWORKSCHEDULE");
            }
        }
        //MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month)
        public string EMPLOYEE_CONFIG_EmpSubAreaForWorkSchedule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONFIG_EMPSUBAREAFORWORKSCHEDULE");
            }
        }
        //MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month)
        public string EMPLOYEE_CONFIG_PublicHolidayCalendar
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONFIG_PUBLICHOLIDAYCALENDAR");
            }
        }
        //MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month)
        public string EMPLOYEE_CONFIG_WorkScheduleRule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONFIG_WORKSCHEDULERULE");
            }
        }
        #endregion MonthlyWS

        #region DailyWS
        //DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate)
        public string EMPLOYEE_GetDailyWorkschedule_DailyGroup_DailyCode_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DAILYWORKSCHEDULE_DAILYGROUP_DAILYCODE_CHECKDATE");
            }
        }

        //DailyWS GetDailyWorkschedule(string EmployeeID, DateTime CheckDate)
        public string EMPLOYEE_GetDailyWorkschedule_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DAILYWORKSCHEDULE_EMPLOYEEID_CHECKDATE");
            }
        }

        //SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile)
        public string EMPLOYEE_SaveDailyWorkscheduleList_Data_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_DAILYWORKSCHEDULELIST_DATA_PROFILE");
            }
        }
        #endregion DailyWS

        #region PersonalSubAreaSetting

        //List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile)
        public string EMPLOYEE_GetPersonalSubAreaSettingList_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALSUBAREASETTINGLIST_PROFILE");
            }
        }

        //SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> Data, string Profile)
        public string EMPLOYEE_SavePersonalSubAreaSettingList_Data_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_PERSONALSUBAREASETTINGLIST_DATA_PROFILE");
            }
        }

        //PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea)
        public string EMPLOYEE_GetPersonalSubAreaSetting_PersonalArea_PersonalSubArea
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALSUBAREASETTING_PERSONALAREA_PERSONALSUBAREA");
            }
        }
        #endregion PersonalSubAreaSetting

        #region PersonalSubGroupSetting

        //List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string SourceProfile)
        public string EMPLOYEE_GetPersonalSubGroupSettingList_SourceProfile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALSUBGROUPSETTINGLIST_SOURCEPROFILE");
            }
        }

        //SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> Data, string TargetProfile)
        public string EMPLOYEE_SavePersonalSubGroupSettingList_Data_TargetProfile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_PERSONALSUBGROUPSETTINGLIST_DATA_TARGETPROFILE");
            }
        }

        #endregion PersonalSubGroupSetting

        #region Substitution
        
        //List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        public string SP1_EMPLOYEE_GetInfotype2003_EmployeeID_BeginDate_EndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SP1_GET_INFOTYPE2003_EMPLOYEEID_BEGINDATE_ENDDATE");
            }
        }

        //List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        public string SP2_EMPLOYEE_GetInfotype2003_EmployeeID_BeginDate_EndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SP2_GET_INFOTYPE2003_EMPLOYEEID_BEGINDATE_ENDDATE");
            }
        }

        //List<Substitution> GetInfotype2003_LogList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        public string EMPLOYEE_GetInfotype2003_LogList_EmployeeID_BeginDate_EndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE2003_LOGLIST_EMPLOYEEID_BEGINDATE_ENDDATE");
            }
        }

        //List<Substitution> GetInfotype2003_LogListInverse(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        public string EMPLOYEE_GetInfotype2003_LogListInverse_EmployeeID_BeginDate_EndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE2003_LOGLISTINVERSE_EMPLOYEEID_BEGINDATE_ENDDATE");
            }
        }
        #endregion Substitution

        #region WFRuleSetting
        //WFRuleSetting GetWFRuleSetting(string WFRule)
        public string EMPLOYEE_GetWFRuleSetting_WFRule
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_WFRULESETTING_WFRULE");
            }
        }
        #endregion WFRuleSetting 

        #region WorkScheduleRule
        //List<WorkScheduleRule> GetWorkScheduleRule(string Profile)
        public string EMPLOYEE_GetWorkScheduleRule_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_WORKSCHEDULERULE_PROFILE");
            }
        }
        #endregion WorkScheduleRule

        //string GetApiKeyRequest()
        public string EMPLOYEE_GetApiKeyRequest
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_APIKEYREQUEST");
            }
        }

        public string PUBLISH_DATE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "PUBLISHDATE", CompanyCode);
            }
        }

        public string EMPLOYEE_GetDateSpecific
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DATESPECIFIC");
            }
        }
        public string EMPLOYEE_GETFISCALYEAR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EMPLOYEE_GETFISCALYEAR");
            }
        }

        #region JOB

        public string EMPLOYEE_JOB_CopyEmployeeConfigFrom
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "JOB_COPYEMPLOYEECONFIG_FROM");
            }
        }

        public string EMPLOYEE_JOB_CopyEmployeeConfigTarget
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "JOB_COPYEMPLOYEECONFIG_TARGET");
            }
        }
        #endregion JOB
    }
}
