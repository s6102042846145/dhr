using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;
using ESS.EMPLOYEE.CONFIG;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.CONFIG.WF;
using ESS.EMPLOYEE.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;

namespace ESS.EMPLOYEE
{
    public class EmployeeManagement
    {
        #region Constructor
        private EmployeeManagement()
        {

        }
        #endregion
        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, EmployeeManagement> Cache = new Dictionary<string, EmployeeManagement>();
        public string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "ESS.EMPLOYEE";
            }
        }

        public static EmployeeManagement CreateInstance(string oCompanyCode)
        {
            EmployeeManagement oEmployeeManagement = new EmployeeManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oEmployeeManagement;
        }
        #endregion MultiCompany  Framework


        public int MAXIMUM_EMPSUBGROUP
        {
            get
            {
                int EmpSubGroup = 0;
                int.TryParse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXIMUM_EMPSUBGROUP"), out EmpSubGroup);
                return EmpSubGroup;
            }
        }

        public string ROOT_ORGANIZATION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ROOT_ORGANIZATION");
            }
        }

        public string PHOTO_PATH
        {
            get
            {
                //Nattawat S. 
                //return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PHOTO_PATH");
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PHOTO_PATH") + CompanyCode + "/";
            }
        }

        public bool VISIBLE_EMPLOYEE_LEVEL
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VISIBLE_EMPLOYEE_LEVEL"));
            }
        }

        public void CopyAllWorkPlaceData()
        {
            List<WorkPlaceCommunication> workplaceList;
            workplaceList = ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetWorkplaceData();
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveWorkPlaceData(workplaceList, "WORKFLOW");
        }


        public EmployeeData FindManager(string EmployeeID, string PositionID, string ManagerCode, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.FindManager(EmployeeID, PositionID, ManagerCode, CheckDate, LanguageCode);
        }


        public List<INFOTYPE0001> GetAllINFOTYPE0001(DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllINFOTYPE0001(CheckDate);
        }

        public List<INFOTYPE0001> GetAllEmployeeName(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllEmployeeName(Language);
        }

        public List<INFOTYPE0001> GetAllEmployeeNameForUserRole(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllEmployeeNameForUserRole(Language);
        }

        //AddBy: Nattawat S. 23052020
        public DataSet GetUserInResponseForActionOfInsteadWithIncludeSub(string EmployeeID, int SubjectID, DateTime CheckDate, string FilterEmpgroup, string FilterText)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponseForActionOfInsteadWithIncludeSub(EmployeeID, SubjectID, CheckDate, FilterEmpgroup, FilterText);
        }

        public DataSet GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, string SearchText)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponseForActionOfInstead(EmployeeID, SubjectID, CheckDate, SearchText);
        }

        public List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string SourceMode, string SourceProfile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(SourceMode).GetPersonalSubGroupSettingList(SourceProfile);
        }

        public void SaveTo(List<PersonalSubGroupSetting> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(TargetMode).SavePersonalSubGroupSettingList(Data, TargetProfile);
        }

        public void SaveINFOTYPE0001To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0001> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0001(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0002To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0002> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0002(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0007To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0007> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0007(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0027To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0027> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0027(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0030To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0030> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0030(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0105To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0105> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0105(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0182To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0182> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0182(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public PersonalSubGroupSetting GetSettingFromEmpSubGroup(string EmpGroup, string EmpSubGroup)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPersonalSubGroupSetting(EmpGroup, EmpSubGroup);
        }

        public List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserResponse(Role, AdminGroup);
        }

        public List<INFOTYPE0007> GetWorkSchedule(string Employee, int Year, int Month)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeePlannedWorkingTimeService.GetINFOTYPE0007List(Employee, Employee, Year, Month, "");
        }


        public INFOTYPE0001 GetINFOTYPE0001ByEmpID(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0001(EmployeeID);
        }

        public INFOTYPE0001 GetINFOTYPE0001ByEmpID(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0001(EmployeeID, CheckDate);
        }

        public List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode, Mode).DataService.GetINFOTYPE0001List(EmployeeID1, EmployeeID2, Profile);
            //return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetINFOTYPE0001List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetINFOTYPE0002List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetINFOTYPE0007List(EmployeeID1, EmployeeID2, Profile);
        }

        public INFOTYPE0007 GetINFOTYPE0007(string EmployeeID, DateTime checkDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0007(EmployeeID, checkDate);
        }

        public List<INFOTYPE0007> GetWorkScheduleWithEvalClass(DateTime BeginDate, DateTime EndDate, string TimeEvaluationClass)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetINFOTYPE0007List(BeginDate, EndDate, TimeEvaluationClass);
        }

        public List<INFOTYPE0027> GetINFOTYPE0027List(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0027List(EmployeeID, CheckDate);
        }
        public List<INFOTYPE0027> GetINFOTYPE0027List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0027List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0030> GetINFOTYPE0030List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetINFOTYPE0030List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetINFOTYPE0105List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetINFOTYPE0182List(EmployeeID1, EmployeeID2, Profile);
        }



        public List<INFOTYPE0185> GetInfotype0185ByEmpID(string EmployeeID)
        {
            return GetInfotype0185ByEmpID(EmployeeID, DateTime.Now);
        }

        public List<INFOTYPE0185> GetInfotype0185ByEmpID(string EmployeeID, DateTime date)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype0185(EmployeeID, date);
        }


        public List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Mode, string Profile = "DEFAULT")
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetPersonalSubAreaSettingList(Profile);
        }

        public void SaveTo(List<PersonalSubAreaSetting> Data, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SavePersonalSubAreaSettingList(Data, Profile);
        }

        public PersonalArea GetSettingFromEmpArea(string PersonalArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPersonalAreaSetting(PersonalArea);
        }

        public PersonalSubAreaSetting GetSettingFromEmpSubArea(string PersonalArea, string PersonalSubArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPersonalSubAreaSetting(PersonalArea, PersonalSubArea);
        }

        public DateSpecificData GetDateSpecificData(string EmployeeID)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetDateSpecific_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDateSpecific(EmployeeID);

        }

        public DateSpecificData GetERPDateSpecificData(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetDateSpecific(EmployeeID);
        }

        public List<MonthlyWS> GetAll(int Year, string SourceMode, string SourceProfile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(SourceMode).GetMonthlyWorkscheduleList(Year, SourceProfile);
        }

        public MonthlyWS GetCalendar(string EmployeeID, int Year, int Month)
        {
            bool includeSubstitute = true; // create config
            return GetCalendar(EmployeeID, Year, Month, includeSubstitute);
        }
        public MonthlyWS GetCalendar(string EmployeeID, int Year, int Month, bool includeSubstitute)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID,"",CompanyCode);
            DateTime firstday = new DateTime(Year, Month, 1);
            if (oEmp.DateSpecific.HiringDate < firstday)
            {
                oEmp = new EmployeeData(EmployeeID,"",CompanyCode, firstday);
            }
            else
            {
                oEmp = new EmployeeData(EmployeeID,"",CompanyCode, oEmp.DateSpecific.HiringDate);
            }
            string cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
            string cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
            string cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            if (includeSubstitute)
            {
                List<Substitution> substitutionList = GetSubstitutionList(EmployeeID, Year, Month, true);

                foreach (Substitution item in substitutionList)
                {
                    for (DateTime rundate = item.BeginDate; rundate <= item.EndDate && rundate < DateTime.MaxValue.Date; rundate = rundate.AddDays(1))
                    {
                        if (rundate.Year == Year && rundate.Month == Month)
                        {
                            substitutionDict.Add(rundate.Day, item);
                        }
                    }
                }
            }
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
               // EmployeeData oEmp1 = new EmployeeData(EmployeeID,"",CompanyCode, item.BeginDate);
                EmployeeData oEmp1 = new EmployeeData(EmployeeID, "", CompanyCode);
                MonthlyWS temp = GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));
                    if (substitutionDict.ContainsKey(index))
                    {
                        cDWSCode = substitutionDict[index].DWSCodeView;
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }

        public MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, int Year, int Month, bool includeSubstitute)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID, CheckDate);

            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";

            if (oEmp.OrgAssignment != null)
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<Substitution> substitutionList = new List<Substitution>();
            if (includeSubstitute)
                substitutionList = EmployeeManagement.CreateInstance(CompanyCode).GetSubstitutionList(EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                EmployeeData oEmp1 = new EmployeeData(EmployeeID, item.BeginDate);
                MonthlyWS temp = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    DateTime oCheckDate = new DateTime(Year, Month, index);
                    Substitution oSubstitution = substitutionList.Find(delegate (Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate <= oCheckDate && oCheckDate <= oSub.EndDate; });
                    if (oSubstitution != null)
                    {
                        //                    if (substitutionList.Exists(delegate(Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
                        //                    {
                        //                        Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }

        public MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month)
        {
            string EmpSubGroupForWorkSchedule = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CONFIG_EmpSubGroupForWorkSchedule;
            string EmpSubAreaForWorkSchedule = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CONFIG_EmpSubAreaForWorkSchedule;
            string PublicHolidayCalendar = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CONFIG_PublicHolidayCalendar;
            string WorkScheduleRule = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CONFIG_WorkScheduleRule;
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupForWorkSchedule, EmpSubAreaForWorkSchedule, PublicHolidayCalendar, WorkScheduleRule, Year, Month);
        }

        public MonthlyWS GetSimulateCalendar(string EmployeeID, int Year, int Month, string WFRule)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID);
            string cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
            string cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
            string cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            bool lFirst = true;
            DateTime dTemp = new DateTime(Year, Month, 1);
            EmployeeData oEmp1 = new EmployeeData(EmployeeID, dTemp);
            MonthlyWS temp = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, WFRule, Year, Month);
            oReturn.WorkScheduleRule = temp.WorkScheduleRule;
            if (lFirst)
            {
                oReturn.ValuationClass = temp.ValuationClass;
            }
            lFirst = false;
            int nStart = dTemp.Day;
            int nEnd = dTemp.AddMonths(1).AddDays(-1).Day;
            oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
            PropertyInfo oProp;
            Type oType = oReturn.GetType();
            for (int index = nStart; index <= nEnd; index++)
            {
                string cDayNo = index.ToString("00");
                string cDWSCode;
                oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                cDWSCode = (string)oProp.GetValue(temp, null);

                oProp.SetValue(oReturn, cDWSCode, null);

                oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
            }

            return oReturn;
        }

        public void SaveTo(int Year, List<MonthlyWS> list, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveMonthlyWorkscheduleList(Year, list, Profile);
        }

        public  DailyWS GetDailyWorkschedule(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(EmployeeID, CheckDate);
        }

        public DailyWS GetDailyWorkschedule(string DailyGroup, string WSCode, DateTime CheckDate)
        {
            if (WSCode == null)
            {
                throw new Exception("WSCode can't be null.May be check workschedule for this employee.");
            }
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(DailyGroup, WSCode, CheckDate);
        }

        public MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupForWorkSchedule, EmpSubAreaForWorkSchedule, PublicHolidayCalendar, WorkScheduleRule, Year, Month);
        }

        public List<DailyWS> GetAll(string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetDailyWorkscheduleList(Profile);
        }

        public void SaveTo(List<DailyWS> list, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveDailyWorkscheduleList(list, Profile);
        }
        public void SaveUserSetting(UserSetting oUserSetting)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveUserSetting(oUserSetting);
        }

        #region " PA "

        #region " Insert And Update "
        public string InsertINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0002And0182(oINF2, oINF182);
        }

        public void UpdateINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE0002And0182(oINF2, oINF182);
        }

        public void InsertINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0001(oINF1);
        }

        public void UpdateINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE0001(oINF1);
        }

        public void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0105(oINF105);
        }

        public void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE0105(oINF105);
        }

        public string InsertINFOTYPE0002And0182And0001And0105Data(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182, INFOTYPE0001 oINF1, INFOTYPE0105 oINF105)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0002And0182And0001And0105Data(oINF2, oINF182, oINF1, oINF105);
        }
        #endregion

        #region " Delete Employee Data "
        public void DeleteINFOTYPE0002And0182And0001And0105And1001Data(string EmployeeID)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0002And0182And0001And0105And1001Data(EmployeeID);
        }

        public void DeleteINFOTYPE0002And0182(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0002And0182(EmployeeID, BeginDate, EndDate);
        }

        public void DeleteINFOTYPE0001(string EmployeeID, DateTime BeginDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0001(EmployeeID, BeginDate);
        }

        public void DeleteINFOTYPE0105(string EmployeeID, DateTime BeginDate, string SubType)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0105(EmployeeID, BeginDate, SubType);
        }

        public void Resign(string EmployeeID, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.Resign(EmployeeID, EndDate);
        }
        #endregion

        #region " Get Employee Data "
        public List<INFOTYPE0001> INFOTYPE0001GetAllEmployeeHistory(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0001GetAllEmployeeHistory(Language);
        }

        public DataTable GetOrganizationStructure(string EmployeeID, string PositionID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetOrganizationStructure(EmployeeID, PositionID);
        }

        public Dictionary<string, object> INFOTYPE0002And0182GetAllHistory(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0002And0182GetAllHistory(EmployeeID);
        }

        public List<INFOTYPE0001> INFOTYPE0001GetAllHistory(string EmployeeID, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0001GetAllHistory(EmployeeID, Language);
        }

        public List<INFOTYPE0105> INFOTYPE0105GetAllHistory(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0105GetAllHistory(EmployeeID);
        }

        public Dictionary<string, object> INFOTYPE0002And0182Get(string EmployeeID, DateTime BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0002And0182Get(EmployeeID, BeginDate);
        }

        public INFOTYPE0001 INFOTYPE0001Get(string EmployeeID, DateTime BeginDate, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0001Get(EmployeeID, BeginDate, Language);
        }

        public INFOTYPE0105 INFOTYPE0105Get(string EmployeeID, DateTime BeginDate, string CategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0105Get(EmployeeID, BeginDate, CategoryCode);
        }
        #endregion  

        #region " Get Employee Data Selector "
        public List<INFOTYPE1000> GetGenderCheckBoxData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetGenderCheckBoxData();
        }

        public List<INFOTYPE1000> GetPrefixDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPrefixDropdownData();
        }

        public List<INFOTYPE1000> GetMaritalStatusDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMaritalStatusDropdownData();
        }

        public List<INFOTYPE1000> GetNationalityDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetNationalityDropdownData();
        }

        public List<INFOTYPE1000> GetLanguageDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetLanguageDropdownData();
        }

        public List<INFOTYPE1000> GetReligionDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetReligionDropdownData();
        }

        public List<INFOTYPE1000> GetEmpGroupDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetEmpGroupDropdownData();
        }

        public List<INFOTYPE1000> GetEmpSubGroupDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetEmpSubGroupDropdownData();
        }

        public List<INFOTYPE1000> GetSubTypeDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetSubTypeDropdownData();
        }

        public List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetCostCenterDropdownData(CheckDate);
        }

        public List<INFOTYPE1000> GetCostCenterByOrganize(string Organize)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetCostCenterByOrganize(Organize);
        }

        public List<INFOTYPE1000> GetAreaDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetAreaDropdownData();
        }

        public List<INFOTYPE1000> GetSubAreaDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetSubAreaDropdownData();
        }
        #endregion

        #endregion

        #region " OM "

        #region " Get Data "
        public List<INFOTYPE1000> INFOTYPE1000GetAllHistory()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1000GetAllHistory();
        }

        public INFOTYPE1000 INFOTYPE1000Get(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1000Get(ObjectType, ObjectID, BeginDate, EndDate);
        }

        public List<INFOTYPE1001> INFOTYPE1001GetAllHistory()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1001GetAllHistory();
        }

        public List<INFOTYPE1001> GetPositionForPerson(string NextObjectID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetPositionForPerson(NextObjectID);
        }

        public List<INFOTYPE1001> GetBelongToRelation()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetBelongToRelation();
        }

        public INFOTYPE1001 INFOTYPE1001Get(string ObjectType, string ObjectID, string NextObjectType, string NextObjectID, string Relation, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1001Get(ObjectType, ObjectID, NextObjectType, NextObjectID, Relation, BeginDate, EndDate);
        }

        public List<INFOTYPE1013> INFOTYPE1013GetAllHistory()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1013GetAllHistory();
        }

        public INFOTYPE1013 INFOTYPE1013Get(string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1013Get(ObjectID, BeginDate, EndDate);
        }
        #endregion

        #region " Delete "
        public void DeleteINFOTYPE1000(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE1000(ObjectType, ObjectID, BeginDate, EndDate);
        }

        public void DeleteINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE1001(oINF1001);
        }

        public void DeleteINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE1013(oINF1013);
        }
        #endregion

        #region " Insert and Update "
        public void InsertINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE1000(oINF1000);
        }

        public void UpdateINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE1000(oINF1000);
        }

        public void InsertINFOTYPE1001(INFOTYPE1001 oINF1001, INFOTYPE1001 oOldINF1001)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE1001(oINF1001, oOldINF1001);
        }

        public void InsertINFOTYPEAllForEmployee(List<INFOTYPE0001> INF0001Lst, List<INFOTYPE0002> INF0002Lst, List<INFOTYPE0182> INF0182Lst, List<INFOTYPE0105> INF0105Lst, List<INFOTYPE1001> INF1001Lst)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0002And0182And0001And0105And1001List(INF0001Lst, INF0002Lst, INF0182Lst, INF0105Lst, INF1001Lst);
        }

        public void UpdateINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE1001(oINF1001);
        }

        public void InsertINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE1013(oINF1013);
        }

        public void UpdateINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE1013(oINF1013);
        }
        #endregion

        public List<INFOTYPE1000> GetRelationSelectData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetRelationSelectData();
        }

        public List<INFOTYPE1000> GetRelationValidationSelectData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetRelationValidationSelectData();
        }

        public List<string> GetRelationValidationByRelation(string relation)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetRelationValidationByRelation(relation);
        }
        #endregion

        public INFOTYPE0105 GetINFOTYPE0105(string oEmployeeID, string oCategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105(oEmployeeID, oCategoryCode);
        }

        public List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string CategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0105ByCategoryCode(EmployeeID, CategoryCode);
        }

        public DataTable GetUserInResponseForActionOfInsteadByResponseType(string ResponseType, string ResponseCode, string ResponseCompanyCode, bool IncludeSub, DateTime CheckDate, string SearchText)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponseForActionOfInsteadByResponseType(ResponseType, ResponseCode, ResponseCompanyCode, IncludeSub, CheckDate, SearchText);
        }

        public EmployeeData GetEmployeeDataByPosition(string PositionID, DateTime CheckDate, string LanguageCode)
        {
            EmployeeData oReturn;
            INFOTYPE1000 oPosition = ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, PositionID, CheckDate, LanguageCode);
            oReturn = OMManagement.CreateInstance(CompanyCode).GetEmployeeByPositionID(PositionID, CheckDate);
            if (oReturn != null)
                oReturn.ActionOfPosition = oPosition;
            return oReturn;
        }

        public List<EmployeeData> GetOMByEmployeePosition(string EmployeeID,string Position,DateTime CheckDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetOMByEmployeePosition(EmployeeID, Position, CheckDate);
        }
        public bool InsertActionLog(object objOld, object objNew, string oAction, bool oStatus, string RequestNo = null)
        {
            bool flg = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.RequestNo = RequestNo;
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            flg = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.InsertActionLog(oActionLog);
            return flg;
        }

        public bool InsertJobActionLog(int oJobID,int oJobTypeID,string oLogDetail,string oLogRemark)
        {
            bool flg = false;
            JobActionLog oJobActionLog = new JobActionLog();
            oJobActionLog.JobID = oJobID;
            oJobActionLog.JobTypeID = oJobTypeID;
            oJobActionLog.LogData = oLogDetail;
            oJobActionLog.LogDate = DateTime.Now; //  generate from objData
            oJobActionLog.LogRemark = oLogRemark;

            flg = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.InsertJobActionLog(oJobActionLog);
            return flg;
        }
        public DataTable GetMonthlyWorkSchedule(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetMonthlyWorkSchedule(EmployeeID);
        }

        public List<INFOTYPE0182> GetINFOTYPE0182List(string oEmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetINFOTYPE0182List(oEmployeeID);
        }

        public List<INFOTYPE0001> GetAllINFOTYPE0001ForReport(DateTime oCheckDate, string oLanguage)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllINFOTYPE0001ForReport(oCheckDate, oLanguage);
        }

        public DataTable GetExternalUserbyID(string UserID, string oLanguage)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetExternalUserbyID(UserID, oLanguage);
        }

        public DataTable GetExternalUserSnapshotbyRequestID(int RequestID, int ApproverID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetExternalUserSnapshotbyRequestID(RequestID, ApproverID);
        }

        public EmployeeData GetPettyCustodianGetByCode(string PettyCode)
        {
            EmployeeData oEmp = new EmployeeData();
            DataTable dt = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetPettyCustodianGetByCode(PettyCode);
            if (dt.Rows.Count > 0)
            {
                oEmp = new EmployeeData(dt.Rows[0]["EmployeeID"].ToString(), DateTime.Now.Date);
            }
            return oEmp;
        }

        public DataSet GetContractDetailByEmployeeID(string EmployeeID, string CostCenter, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetContractDetailByEmployeeID(EmployeeID, CostCenter, CheckDate);
        }

        public bool IsContractUser(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsContractUser(EmployeeID);
        }

        public bool ValidateActiveEmployeeForTravel(string EmployeeID, DateTime BeginTravelDate)
        {
            return ServiceManager.CreateInstance(this.CompanyCode).EmployeeService.ValidateActiveEmployeeForTravel(EmployeeID, BeginTravelDate);
        }

        #region Job Data from DHR

        public string JobALL_DHR_TO_INFOTYPE0001(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0001(EmployeeID1, EmployeeID2);
        }

        public string JobALL_DHR_TO_INFOTYPE0002(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0002(EmployeeID1, EmployeeID2);
        }

        public string JobALL_DHR_TO_INFOTYPE0007(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0007(EmployeeID1, EmployeeID2);
        }

        public string JobALL_DHR_TO_INFOTYPE0027(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0027(EmployeeID1, EmployeeID2);
        }

        public string JobALL_DHR_TO_INFOTYPE0030(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0030(EmployeeID1, EmployeeID2);
        }

        public string JobALL_DHR_TO_INFOTYPE0105(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0105(EmployeeID1, EmployeeID2);
        }
        public string JobALL_DHR_TO_INFOTYPE0105_Mapping(string DataSource, out string oMessage)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0105_Mapping(out oMessage);
        }

        public string JobALL_DHR_TO_INFOTYPE0182(string EmployeeID1, string EmployeeID2, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobALL_DHR_TO_INFOTYPE0182(EmployeeID1, EmployeeID2);
        }

        public string JobALL_DHR_TO_DateSpecificData(string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).DataService.JobAll_DHR_TO_DateSpecificData();
        }

        #endregion Job Data from DHR

        #region Job Config from DHR

        public string JobAll_DHR_TO_PersonalSubAreaSetting(string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).ConfigService.JobALL_DHR_TO_PersonalSubAreaSetting();
        }

        public string JobALL_DHR_TO_PersonalSubGroupSetting(string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).ConfigService.JobALL_DHR_TO_PersonalSubGroupSetting();
        }

        public string JobALL_DHR_TO_MonthlyWorkschedule(int Year, string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).ConfigService.JobALL_DHR_TO_MonthlyWorkschedule(Year);
        }

        public string JobALL_DHR_TO_DailyWorkschedule(string DataSource)
        {
            return ServiceManager.CreateInstance(this.CompanyCode, DataSource).ConfigService.JobALL_DHR_TO_DailyWorkschedule();
        }

        #endregion Job Config from DHR


        public void ChangePinCode(string oEmployeeID, string oPincode, string oNewPinCode)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CHANGEPINCODE;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.ChangePinCode(oEmployeeID, oPincode, oNewPinCode);
        }

        public bool ValidatePinCode(string oEmployeeID, string oPincode)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_VERIFY_PINCODE;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.VerifyPinCode(oEmployeeID, oPincode);
        }

        public bool ExistPinCodeByEmployee(string oEmployeeID)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_EXIST_PINCODE;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.ExistPinCodeByEmployee(oEmployeeID);
        }

        public bool ValidateNewPasswordPolicy(string NewPassword, out string ErrorMessage)
        {

            int MINPINCODE = Convert.ToInt16(EmployeeServices.SVC(CompanyCode).MINPINCODE_LENGHT);
            bool isPass = false;
            ErrorMessage = string.Empty;
            if (NewPassword.Length < MINPINCODE)
            {
                ErrorMessage = "PINCODE must be at least " + MINPINCODE.ToString() + " characters in length";
            }
            else
            {
                //case specific policy
                //int iCondition = 0;
                //if(Regex.IsMatch(NewPassword, "[A-Z]")){ iCondition += 1; }
                //if(Regex.IsMatch(NewPassword, "[a-z]")){ iCondition += 1; }
                //if(Regex.IsMatch(NewPassword, "[0-9]")){ iCondition += 1; }
                //if(Regex.IsMatch(NewPassword, @"[!@#$%^&*()_+=\[{\]};:<>|./?,-]")){ iCondition += 1; }
                //if(iCondition >= 3){
                isPass = true;
                //}
                //else{
                //    ErrorMessage = "PINCODE must contain at least 3 of the following 4 types of characters: " + "\n" +
                //        "-UPPER case letters (i.e. A-Z) " + "\n" +
                //        "-lower case letters (i.e. a-z) " + "\n" +
                //        "-numbers (i.e. 0-9) " + "\n" +
                //        "-special characters (e.g. !@#$%^&*()_+=\\[{\\]};:<>|./?,-)";
                //}
            }
            return isPass;
        }

        public void RequestNEWPin(EmployeeData oEmp)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_REQUEST_PINCODE;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.RequestNEWPin(oEmp);
        }

        public bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_VALIDATE_TICKET;
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateTicket(EmployeeID, TicketClass, TicketID);
        }

        public void CreateNEWPin(string EmployeeID, string TicketID, string oPincode)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_CREATE_PINCODE;
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.CreateNEWPin(EmployeeID, TicketID, oPincode);
        }

        public List<AttachedFileConfiguration> GetAttachedFileConfigulationList(string CategoryGroup, string CategoryType)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAttachedFileConfigurationList(CategoryGroup, CategoryType);
        }

        public string GetYearsLimit()
        {
            return EmployeeServices.SVC(CompanyCode).PUBLISH_DATE;
        }

        public BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetBreakPattern(DWSGroup, BreakCode);
        }

        #region CONFIG TM

        public List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month)
        {
            return GetSubstitutionList(EmployeeID, Year, Month, true);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month, bool OnlyEffective)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return GetSubstitutionList(EmployeeID, date1, date2, OnlyEffective);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective)
        {
            List<Substitution> buffer = new List<Substitution>();
            if (!OnlyEffective)
            {
                buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            }
            // COMMENT SAP CLOSE SYSTEM 10-16-2020
            //buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            return buffer;
        }

        public List<PersonalSubGroupSetting> GetAllSetting(string Mode)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetPersonalSubGroupSettingList_SourceProfile;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPersonalSubGroupSettingList("DEFAULT");
        }


        public void SavePersonalSubGroupSettingListveTo(List<PersonalSubGroupSetting> Data, string TargetMode, string TargetProfile)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_SavePersonalSubGroupSettingList_Data_TargetProfile;
            ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.SavePersonalSubGroupSettingList(Data, TargetProfile);
        }

        public WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetWFRuleSetting_WFRule;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetWFRuleSetting(WFRule);
        }

        public List<WorkScheduleRule> GetWSRule(string SourceMode, string Profile)
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GetWorkScheduleRule_Profile;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetWorkScheduleRule(Profile);
        }

        #endregion CONFIG TM

        public bool ValidateEmployeeID(string EmployeeID)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.ValidateEmployeeID(EmployeeID);
        }

        public DataTable GetOffCycleTypeList(string oLang)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetOffCycleTypeList(oLang);
        }

        public DataTable GetEmployeesList(string oLang)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEmployeesList(oLang);
        }
        public DataTable GetFlowIDByRequestTypeIDKeyCode(string RequestTypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode, "DB").DataService.GetFlowIDByRequestTypeIDKeyCode(RequestTypeID);
        }

        public ESS.EMPLOYEE.CONFIG.OM.FiscalYear GetFiscalYear()
        {
            string DataSource = EmployeeServices.SVC(CompanyCode).EMPLOYEE_GETFISCALYEAR;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetFiscalYear();
        }

        public List<string> GetUserRoles(string EmployeeID)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode,DataSource).EmployeeService.GetUserRole(EmployeeID);
        }

        #region New Workflow by Nattawat S.
        public List<RequestTypeKeyCodeSetting> GetRequestFlowSettingByCriteria(int oRequestTypeID, int oVersion)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetRequestFlowSettingByCriteria(oRequestTypeID, oVersion);
        }
        #endregion

        public DataTable GetConfigSelectYear()
        {
            return ServiceManager.CreateInstance(CompanyCode,"DB").DataService.GetConfigSelectYear();
        }
    }
}