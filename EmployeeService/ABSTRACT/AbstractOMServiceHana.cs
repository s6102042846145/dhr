#region Using

using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.INTERFACE;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    public class AbstractOMServiceHana : IOMServiceHana
    {
        public AbstractOMServiceHana()
        {
        }

        public virtual List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        {
            throw new NotImplementedException();
        }
        public virtual List<INFOTYPE1513> GetAllJobIndex(string objId1, string objId2, string Profile)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjectID1, string ObjectID2, string Profile)
        {
            throw new NotImplementedException();
        }
    }
}