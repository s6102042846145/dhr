#region Using

using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.INTERFACE;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    public class AbstractOMService : IOMService
    {
        public AbstractOMService()
        {
        }
        #region IOMService Members
        public virtual List<ObjectTypeIDRange> GetObjectIDRange(ObjectType objectType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1513> GetAllJobIndex(string objId1, string objId2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetObjectData(ObjectType objectType, string objectID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes)
        {
            return GetAllObject(objectTypes, "DEFAULT");
        }

        public virtual List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, DateTime dtCheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Mode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllObject(List<ObjectType> objectTypes, List<INFOTYPE1000> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes)
        {
            return GetAllRelation(objectTypes, relationCodes, "DEFAULT");
        }

        public virtual List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes)
        {
            return GetAllRelation(objId1, objId2, objectTypes, relationCodes, "DEFAULT");
        }

        public virtual List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, List<INFOTYPE1001> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetRelationObjects(string ObjectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsHaveSubOrdinate(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1013> GetAllPositionBandLevel()
        {
            return GetAllPositionBandLevel("DEFAULT");
        }

        public virtual List<INFOTYPE1013> GetAllPositionBandLevel(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllPositionBandLevel(List<INFOTYPE1013> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1013 GetPositionBandLevel(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1003> GetAllApprovalData()
        {
            return GetAllApprovalData("DEFAULT");
        }

        public virtual List<INFOTYPE1003> GetAllApprovalData(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllApprovalData(List<INFOTYPE1003> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1003 GetApprovalData(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE1000> GetOrgUnit(string OrgID, bool IncludeSub)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllPositionForHrMaster(DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllPositionNoCheckDate(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetPositionByOrganize(string Organize, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetPositionByOrganizeForHRMaster(string Organize, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrgUnit(string PositionID, DateTime CheckDate, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrganizationByPositionID(string PositionID, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IOMService Members

        #region IOMService Members

        public virtual List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllJobIndex(List<INFOTYPE1513> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion IOMService Members

        //AddBy: Ratchatawan W. (2012-11-07)

        #region Function

        public virtual List<EmployeeData> GetEmployeeByOrg(string OrgID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjID1, string ObjID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOrgUnitLevel(List<INFOTYPE1010> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1010 GetLevelByOrg(string ObjectID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ObjectType> GetAllObjectType()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ObjectTypeIDRange> GetAllObjectTypeIDRange()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetAllRelationType()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<UserRoleResponse> GetUserRoleResponse()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveUserRoleResponseSnapshot(List<UserRoleResponseSnapShot> lstSnapshot)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void CreateSnapshot()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateRequestFlowReceipientSnapshot()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion Function

        public virtual void MapEmpSubGroup()
        {
            throw new NotImplementedException();
        }
        public virtual int GetBrandNoByEmpSubGroup(decimal EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region Job OM from DHR

        public virtual string JobALL_DHR_TO_INFOTYPE1000()
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE1001()
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE1010()
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE1013()
        {
            throw new NotImplementedException();
        }
        #endregion Job OM from DHR
    }
}