using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Security.Principal;
using ESS.EMPLOYEE;
using ESS.SECURITY;
using ESS.SHAREDATASERVICE;

namespace ESS.WORKFLOW
{
    public class WorkflowIdentity : IIdentity
    {
        #region MultiCompany  Framework
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.EMPLOYEE";
        public static WorkflowIdentity CreateInstance(string oCompanyCode)
        {
            WorkflowIdentity oWorkflowIdentity = new WorkflowIdentity()
            {
                CompanyCode = oCompanyCode
            };
            return oWorkflowIdentity;
        }
        #endregion MultiCompany  Framework

        protected WorkflowIdentity()
        {
        }

        #region " GetIdentity "

        public WorkflowIdentity GetIdentity(string username, string password, string Position)
        {
            return GetIdentity(username, password, false, Position);
        }

        public WorkflowIdentity GetIdentity(string domainName, string username, string password,string Position)
        {
            return GetIdentity(domainName, username, password, false, Position);
        }

        public WorkflowIdentity GetIdentity(string username, string password, bool IsSystemAccount, string Position)
        {
            //ModifiedBy: Ratchatawan W. (2012/01/04)
            return GetIdentity("", username, password, IsSystemAccount, Position);
        }

        public WorkflowIdentity GetIdentity(string domainName, string username, string password, bool IsSystemAccount, string Position)
        {
            string cEmpID = "";
            WorkflowIdentity iden = CreateInstance(CompanyCode);
            SecurityManagement oSecurityManagement = SecurityManagement.CreateInstance(CompanyCode);
            if (IsSystemAccount)
            {
                if (domainName != "")
                {
                    oSecurityManagement = SecurityManagement.CreateInstance(CompanyCode).CheckAuthorize("SYSTEM", string.Format(@"{0}\{1}", domainName, username), password);
                }
                else
                {
                    oSecurityManagement = SecurityManagement.CreateInstance(CompanyCode).CheckAuthorize("SYSTEM", username, password);
                }
            }
            else
            {
                if (domainName != "")
                {
                    oSecurityManagement = SecurityManagement.CreateInstance(CompanyCode).CheckAuthorize("EMPLOYEE", string.Format(@"{0}\{1}", domainName, username), password);
                }
                else
                {
                    oSecurityManagement = SecurityManagement.CreateInstance(CompanyCode).CheckAuthorize("EMPLOYEE", username, password);
                }
                cEmpID = new EmployeeData().GetEmployeeIDFromUserID(username);
                if (cEmpID == "")
                {
                    cEmpID = username;
                    //throw new Exception("username not found");
                }
            }
            if (oSecurityManagement.IsAuthorize && cEmpID == "")
            {
                cEmpID = username;
            }
            iden.AuthenticationType = oSecurityManagement.Mode;
            iden.IsAuthorize = oSecurityManagement.IsAuthorize;
            iden.Name = username;
            iden.EmployeeID = cEmpID;
            iden.TicketID = oSecurityManagement.TicketID;
            //AddBy: Ratchatawan W. (2011-08-30)
            iden.CurrentPosition = Position;
            return iden;
        }

        /// <summary>
        /// Get Identitity without Password, this method use for sigle sign-on
        /// </summary>
        public WorkflowIdentity GetIdentityWithoutPassword(EmployeeData empData)
        {
            WorkflowIdentity iden = CreateInstance(CompanyCode);
            if( empData != null )
            {
                SecurityManagement auth = SecurityManagement.CreateInstance(CompanyCode).CheckAuthorizeWithoutPassword("EMPLOYEE", empData.EmployeeID);
                iden.AuthenticationType = auth.Mode;
                iden.IsAuthorize = auth.IsAuthorize;
                iden.Name = empData.Name;
                iden.EmployeeID = empData.EmployeeID;
                iden.TicketID = auth.TicketID;
                iden.CurrentPosition = empData.PositionID;
                iden.IsSystem = empData.IsSystem;
            }
            return iden;
        }

        public WorkflowIdentity GetIdentityForExternalUser(string EmployeeID,string EmployeeName)
        {
            WorkflowIdentity iden = CreateInstance(CompanyCode);
            SecurityManagement auth = SecurityManagement.CreateInstance(CompanyCode).CheckAuthorizeForNoneEmployee("EMPLOYEE", EmployeeID);
            iden.AuthenticationType = auth.Mode;
            iden.IsAuthorize = auth.IsAuthorize;
            iden.Name = EmployeeName;
            iden.EmployeeID = EmployeeID;
            iden.TicketID = auth.TicketID;
            iden.CurrentPosition = "";
            iden.IsSystem = false;
            return iden;
        }

        #endregion " GetIdentity "

        #region IIdentity Members
        public bool IsAuthenticated { get; set; }
        public string Name { get; set; }
        #endregion IIdentity Members

        public string TicketID { get; set; }

        public string EmployeeID { get; set; }

        public string CurrentPosition { get; set; }
        public bool IsAuthorize { get; set; }
        public string AuthenticationType { get; set; }

        public bool IsSystem { get; set; }

        public bool ValidateDomain(string strDomain)
        {
            return SecurityManagement.CreateInstance(CompanyCode).ValidateDomain("EMPLOYEE", strDomain);
        }
    }
}