using System.Collections.Generic;

namespace ESS.EMPLOYEE
{
    public class EmployeeSortorByUnit_EmpID : IComparer<EmployeeData>
    {
        #region IComparer<EmployeeData> Members

        public int Compare(EmployeeData x, EmployeeData y)
        {
            int nReturn = string.Compare(x.OrgAssignment.OrgUnit, y.OrgAssignment.OrgUnit);
            if (nReturn == 0)
            {
                nReturn = string.Compare(x.EmployeeID, y.EmployeeID);
            }
            return nReturn;
        }

        #endregion IComparer<EmployeeData> Members
    }
}